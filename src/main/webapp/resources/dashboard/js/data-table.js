$(document).ready( function () {

    var myTable = jQuery("#datatable");
    var thead = myTable.find("thead");
    var thRows =  myTable.find("tr:has(th)");
    thead = jQuery("<thead></thead>").appendTo(myTable);
    var copy = thRows.clone(true).appendTo("thead");
    thRows.remove();

    var selec = jQuery("#idvalue");
    var selId =  myTable.find("th:has(select)");

  	var $tableBody = $('#datatable').find("thead");
    var $trLast = $tableBody.find("tr");
    var $trNew = $trLast.clone();
	var attribute = $trNew.attr('id', 'idvalue');
    $trLast.before(attribute);
	var table = $('#datatable').DataTable({
		"order": [[ 0, "asc" ]],
		"oLanguage": {
      "sLengthMenu": 'Display <select class="form-control">'+
        '<option value="10">10</option>'+
        '<option value="25">25</option>'+
        '<option value="50">50</option>'+
        '<option value="-1">All</option>'+
        '</select> records'
    },
        "dom": 'Blfrtip',
        "buttons": ['copy', 'csv', 'excel', 'pdf'],
        "colReorder": true
	});



    $('#clearBtn').click(function() {
         var table = $('#datatable').dataTable();
         $("select").each(function() { this.selectedIndex = 0 });
         table.fnFilterClear();
    });

    $("#datatable #idvalue th").each( function ( i ) {

		if ($(this).text() !== '') {
	        var isStatusColumn = (($(this).text() == 'Status') ? true : false);
			var select = $('<select><option value=""></option></select>')
	            .appendTo( $(this).empty() )
	            .on( 'change', function () {
	                var val = $(this).val();

                 table.column( i )
	                    .search( val ? '^'+$(this).val()+'$' : val, true, false )
	                    .draw();
	            } );
	 		
			// Get the Status values a specific way since the status is a anchor/image
			if (isStatusColumn) {
				var statusItems = [];
				table.column( i ).nodes().to$().each( function(d, j){
					var thisStatus = $(j).attr("data-filter");
					if($.inArray(thisStatus, statusItems) === -1) statusItems.push(thisStatus);
				} );
				
				statusItems.sort();

				$.each( statusItems, function(i, item){
				    select.append( '<option value="'+item+'">'+item+'</option>' );
				});

			}
            // All other non-Status columns (like the example)
			else {
				table.column( i ).data().unique().sort().each( function ( d, j ) {  
					select.append( '<option value="'+d+'">'+d+'</option>' );
		        } );	
			}
	        
		}
    } );

});
