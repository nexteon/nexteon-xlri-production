<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
   <%@include file="fragments/headerlibs.jsp" %>
   <body class="innerpage">
      <%@include file="fragments/header.jsp" %>
      <!-- Body Starts Here -->
      <main role="main">
         <div class="page-title">
            <h2>Programme Detail</h2>
         </div>
         <div class="content">
            <div class="container">
               <div class="row">
                  <div class="col-md-12">
                     <div class="whitebg">
                        <div class="clearfix"></div>
                        <div class="col-xs-12">
                           <h2>AWARD TRACK</h2>
                           <p>The awards in this track seeks to recognize and honor
                              exceptional entrepreneurs from various sectors. To encourage a
                              culture of entrepreneurship across the country and to highlight
                              the models of excellence for others to emulate and improve upon.
                              The Award Track winners will be identified through a process of
                              screening and jury selection.The target group for these awards
                              are Young Entrepreneurs of the country.
                           </p>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-12">
                           <h2>Eligibility To Participate</h2>
                           <ul>
                              <li>Entrepreneurs who registered their enterprise
                                 (Founder/co-founder of a Company or a Limited Liability
                                 Partnership firm as per the Indian Acts) should be below the
                                 age of 30 years as on 1<sup>st</sup> January of year of
                                 application. 5 years age relaxation will be given to candidates
                                 from SC, ST and Persons with Disability (PwD).
                              </li>
                              <li>The enterprise must have been registered at least 3
                                 years before the date of first advertisement of the current
                                 call.
                              </li>
                              <li>The candidate should be a First Generation
                                 Entrepreneur. The definition of a first generation entrepreneur
                                 for the scheme is “An entrepreneur who is not in the same line
                                 of business as his/her parent. They should have started the
                                 enterprise on their own initiative, idea or innovation”.
                              </li>
                              <li>In case of enterprises with more than one co-founders,
                                 at least one of the co-founders must adhere to the
                                 above-mentioned three criteria.
                              </li>
                              <li>Initial paid-up capital of the enterprise should not
                                 exceed INR 1 cr.
                              </li>
                              <li>In case of social enterprises, dependence on grant or
                                 subsidy should not be more than 25% of the working capital
                                 requirement in the year 2016-17.
                              </li>
                              <li>The enterprise must have mandatory industry
                                 certifications (example: FDA, CE, FSSAI, MSMEetc).
                              </li>
                           </ul>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-12">
                           <h2>Award Categories</h2>
                           <h4>Award categories in Goods</h4>
                           <ul>
                              <li>Textiles, leather and related goods</li>
                              <li>Agri, food and forestry products</li>
                              <li>Chemicals, pharma, bio and other processed materials</li>
                              <li>Engineering systems</li>
                              <li>Renewable energy and waste management</li>
                              <li>Handicrafts, traditional manufacturing & other goods</li>
                           </ul>
                           <div class="clearfix"></div>
                           <h4>Award categories in Goods</h4>
                           <ul>
                              <li>IT & ITES, Financial</li>
                              <li>Education</li>
                              <li>Healthcare</li>
                              <li>Hospitality</li>
                              <li>E-Commerce, logistics, transport & other services</li>
                           </ul>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-12">
                           <h2>Additionally awards will be presented to the best
                              entrepreneurs in the following categories
                           </h2>
                           <ul>
                              <li><strong>Social Enterprises</strong> - A business with
                                 the primary objective of social and/or environmental impact,
                                 and where at least 51% of working capital is generated through
                                 core activities of the enterprise, and its' surpluses are
                                 principally reinvested for that purpose in the business or in
                                 the community, rather than being driven by the need to maximize
                                 profit for shareholders and owners.
                              </li>
                              <li><strong>Women Entrepreneur</strong> - should
                                 individually/ collectively own 51% or more of the paid up
                                 capital in the enterprise.
                              </li>
                              <li><strong>Entrepreneur from SC/ST Category</strong> -
                                 Person(s) from SC/ST category should individually/ collectively
                                 own 51% or more of the paid up capital in the enterprises.
                              </li>
                              <li><strong>Entrepreneur from Persons with
                                 Disability (PwD) category</strong> - Person(s) with Disability should
                                 individually/ collectively own 51% or more of the paid up
                                 capital in the enterprise.
                              </li>
                              <li><strong>Entrepreneur from Difficult Areas</strong> -
                                 Major operation (production/research/processing etc.) should be
                                 located in the North Eastern States / Jammu & Kashmir / Islands
                                 / Backward Districts (as notified by GoI).
                              </li>
                              <li><strong>Barefoot Enterprises</strong> – Group
                                 Enterprises established as <strong><i>a)</i></strong> Producer
                                 Company, <strong><i>b)</i></strong> Co-operatives <strong><i>or
                                 c)</i></strong> Federation of SHGs,working primarily in marginal, poor and
                                 excluded places and contexts solving one or more
                                 social-economic problem.
                              </li>
                           </ul>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-12">
                           <h2>Eligibility for Awards (Barefoot Enterprises)-</h2>
                           <ul>
                              <li>Registration of Barefoot enterprise should not be less
                                 than three years old and more than ten years.
                              </li>
                              <li>Dependence on grant or subsidy should not be more than
                                 25% of the working capital requirement in the year 2016-17.
                              </li>
                           </ul>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-12">
                           <h2>The Award Winner Will Receive</h2>
                           <ul>
                              <li>Award money of INR 5 lakh each to the enterprise.</li>
                              <li>Trophy</li>
                              <li>Certificate</li>
                           </ul>
                        </div>
                        <table class="table">
                           <tr>
                              <th class="tabel-header">Number of awards in the Award
                                 Track
                              </th>
                           </tr>
                        </table>
                        <table class="table">
                           <tr>
                              <td rowspan=9>Awards</td>
                              <td>Goods Category</td>
                              <td>6</td>
                           </tr>
                           <tr>
                              <td>Services Category</td>
                              <td>5</td>
                           </tr>
                           <tr>
                              <td>Social Enterprises</td>
                              <td>1</td>
                           </tr>
                           <tr>
                              <td>Women Entrepreneurs</td>
                              <td>1</td>
                           </tr>
                           <tr>
                              <td>Entrepreneur/s from SC/ ST
                                 Category
                              </td>
                              <td>1</td>
                           </tr>
                           <tr>
                              <td>Entrepreneur/s from Persons with
                                 Disability (PwD) Category
                              </td>
                              <td>1</td>
                           </tr>
                           <tr>
                              <td>Difficult Areas – North East/ J &
                                 K/ Islands/ Backward area rural enterprises
                              </td>
                              <td>1</td>
                           </tr>
                           <tr>
                              <td>Barefoot Enterprises</td>
                              <td>1</td>
                           </tr>
                           <tr>
                              <td>Entrepreneur with Vocational
                                 Skills
                              </td>
                              <td>1</td>
                           </tr>
                        </table>
                        <table class="table">
                           <tr>
                              <th><strong>Total Awards in	Award Track</strong></td>
                              <th><strong>18</strong></td>
                           </tr>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </main>
      <!-- Body Ends Here -->
      <%@include file="fragments/footer.jsp" %>
   </body>
</html>