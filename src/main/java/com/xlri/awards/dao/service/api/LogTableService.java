package com.xlri.awards.dao.service.api;

import java.util.List;

import com.xlri.awards.dao.entity.LogTable;

public interface LogTableService {

	public boolean saveLog(LogTable logTable);
	public List<LogTable> getAllLogs();
	public List<LogTable> getLogsByUsername(String username);
	public List<LogTable> getLogsByIPAddress(String ipAddress);
	
}
