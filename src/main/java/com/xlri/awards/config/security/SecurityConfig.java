package com.xlri.awards.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{

	@Autowired
    private ApplicationAuthenticationProvider applicationAuthenticationProvider;
	
	 @Autowired
	    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
	        auth.authenticationProvider(new ApplicationAuthenticationProvider());
	    }

	 @Override
	    protected void configure(HttpSecurity http) throws Exception {

	        http.authorizeRequests()
	                .antMatchers("/","/bin/captcha","/bin/validateCaptcha","/nominatordata","/login","/signup","/resources*//**//**","/static*//**//**","/neas*//**//**","/otp/verify*//**//**","/forgetpassword*//**//**").permitAll()
	                .antMatchers("/newssectionget","/newssectionpost","/expertsection","/editexpert"
	                		,"/updateexpertsection","/allexperts","/allpartners","/partnersection","/view/nominator","/view/nominee"
	                		).hasAnyRole("ADMIN","SUPPORT","REGIONAL", "EXPERT")
	                .antMatchers("/basicdetails*//**//**","/secondcatreg*//**//**","/thirdcatreg*//**//**","/basicdetail*//**//**"
	                		,"/openfirstcat","/openseccat","/openthirdcat","/nominatorlanding","/nomineelanding"
	                		,"/secondcat/fileupload*//**").hasAnyRole("NOMINATOR")
	                .antMatchers("/secondcatreg*//**//**","/thirdcatreg*//**//**","/nomineelanding"
	                		,"/secondcat/fileupload*//**").hasAnyRole("NOMINEE")
	                .antMatchers("/dashboard","/userdata").hasAnyRole("SUPPORT","ADMIN", "REGIONAL", "EXPERT")
	                .antMatchers("/userpage").hasAnyRole("NOMINATOR","NOMINEE","ADMIN","REGIONAL","SUPPORT", "EXPERT")
	                .and()
	                .formLogin().usernameParameter("emailId")
	                .loginPage("/login").defaultSuccessUrl("/userpage").successHandler(customAuthenticationSuccessHandler()).failureUrl("/login?error=Invalid Username or Password.").failureHandler(customAuthenticationFailureHandler())
	                .and().logout().addLogoutHandler(customLogoutSuccessHandler()).logoutRequestMatcher(new AntPathRequestMatcher("/logout")).and().sessionManagement().invalidSessionUrl("/").sessionFixation().newSession()
	                .and().sessionManagement();
	        http.csrf().ignoringAntMatchers("/otp/resend*//**//**","/anotherbasicdetail","/neas/api/**","/saveseccat/**","/secondcat/fileupload*//**","/thirdnominee/**","/allexperts","/allpartners","/category/textile","/goods/category","/special/category/**","/basicdetail*//**//**");
	    }


	    @Override
	    protected  void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception{
	        authenticationManagerBuilder.authenticationProvider(applicationAuthenticationProvider);

	    }
	    
	    @Bean
	    public AuthenticationFailureHandler customAuthenticationFailureHandler() {
	        return new CustomAuthenticationFailureHandler();
	    }
	    
	    @Bean
	    public AuthenticationSuccessHandler customAuthenticationSuccessHandler() {
	        return new AuthenticationSuccessEventListener();
	    }
	    
	    @Bean
	    public CustomLogoutSuccessHandler customLogoutSuccessHandler() {
	        return new CustomLogoutSuccessHandler();
	    }
}
