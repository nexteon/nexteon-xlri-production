<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <%@include file="fragments/headerlibs.jsp" %>
    <body class="innerpage">
        <%@include file="fragments/header.jsp" %>
        <!-- Body Starts Here -->
        <div class="clearfix"></div>
        <main role="main">
            <div class="page-title">
                <h2>Basic Financial & Operational Information Form</h2>
            </div>
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="whitebg">
                            <div class="row corner-box">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="registrationNumber">Form Number:
                                                            <span class="heading-span">${formNumber}</span></label>
                                                    </div>
                                                </div>
                                            </div>
                                <div id="form1" class="cst-form left-form">
                                    <div class="form-body">
                                        <form action="/saveseccat/${encryptedPhone}" method="POST" role="form" enctype="multipart/form-data" id="seccatform" data-toggle="validator" >
                                            <input type="hidden" name="formNumber" value="${formNumber}">
                                            <!-- <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="enterprise-date">Date of Enterprise’s Incorporation (The day when nominee’s company/organization was registered.)</label>
                                                        <input type="date" name="enterpriseDate" id="enterprise-date" class="form-control" placeholder="Date of Enterprise’s Incorporation"  max="2014-03-31" min="2011-04-01" required data-required-error="Please fill the field.">
                                                        <div class="help-block with-errors"></div>
                                                        <input type="text" name="enterpriseDate" id="enterprise-date" class="form-control" placeholder="Date of Enterprise’s Incorporation" required>
                                                    </div>
                                                </div>
                                            </div> -->
                                            
                                            
                                            <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="enterprise-date">1. Date of Enterprise’s Incorporation (The day when nominee’s company/organization was registered.)</label>
                                                          <div class="row">  <div class="col-md-3">
                                                             <select name="date" class="form-control" id="yearidd" required>
                                                              <option value="" selected disabled>YYYY</option>
                                                              <c:forEach begin="1978" end="2018" step="1" var="d">
                                                               <option value="${d}">${d}</option>
                                                              </c:forEach>
                                                             </select>
                                                            </div>
                                                            
                                                            <div class="col-md-3">
                                                             <select name="month" class="form-control" id="monthidd" required>
                                                              <option value="" selected disabled>MM</option>
                                                              <option id="jan1" value="01">Jan</option>
                                                              <option id="feb1" value="02">Feb</option>
                                                              <option id="mar1" value="03">Mar</option>
                                                              <option id="apr1" value="04">Apr</option>
                                                              <option id="may1" value="05">May</option>
                                                              <option id="jun1" value="06">Jun</option>
                                                              <option id="jul1" value="07">Jul</option>
                                                              <option id="aug1" value="08">Aug</option>
                                                              <option id="sep1" value="09">Sep</option>
                                                              <option id="oct1" value="10">Oct</option>
                                                              <option id="nov1" value="11">Nov</option>
                                                              <option id="dec1" value="12">Dec</option>
                                                             </select>
                                                            </div>
                                                            
                                                            <div class="col-md-2">
                                                             <select name="day" class="form-control" id="dayidd" required>
                                                              <option value="" selected disabled>DD</option>
                                                              <option value="01">01</option>
                                                              <option value="02">02</option>
                                                              <option value="03">03</option>
                                                              <option value="04">04</option>
                                                              <option value="05">05</option>
                                                              <option value="06">06</option>
                                                              <option value="07">07</option>
                                                              <option value="08">08</option>
                                                              <option value="09">09</option>
                                                              <c:forEach begin="10" end="28" step="1" var="d">
                                                               <option value="${d}">${d}</option>
                                                              </c:forEach>
                                                              <option id="d29" style="display: none;" value="29">29</option>
											                 <option id="d30" style="display: none;" value="30">30</option>
											                 <option id="d31" style="display: none;" value="31">31</option>
                                                             </select>
                                                            </div>
                                                            <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            
                                            <input type="hidden" class="form-control" name="enterpriseDate" id="dateeidd">
                                            
                                            
                                            <div class="clearerfix"></div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="businessType">2. Type of Business Formation</label>
                                                        <select class="form-control" name="businessFormation" id="businessType" required>
                                                            <option value="" selected disabled>Select</option>
                                                            <option value="Company">Company</option>
                                                            <option value="Partnership">Partnership</option>
                                                            <option value="Limited Liability Partnership">Limited Liability Partnership</option>
                                                            <option value="Proprietaryship">Proprietorship</option>
                                                            <option value="Cooperative">Cooperative</option>
                                                            <option value="Producer Company">Producer Company</option>
                                                            <option value="Self Help group">Self Help group</option>
                                                            <option value="Joint Liability Group">Joint Liability Group</option>
                                                        </select>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearerfix"></div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="foundingDiv">
                                                        <strong class="main-td">2.1 Founding Team Members (Mention their name and credentials)</strong>
                                                        <input type="button"  value="+" onClick="aadTextBoxFT()" class="btn btn-warning">
                                                        <input type="button" value="-"  onClick="removeTextBoxFT()" class="btn btn-warning">
                                                        <div id="ft">
                                                            <div id="ft_child">
                                                                <div class="col-xs-6">
                                                                    <label class="" for="ft_name">Name/s </label>
                                                                    <input id="ft_name" type="text"   class="crt_assts_17  form-control"  >	
                                                                </div>
                                                                <div class="col-xs-6">
                                                                    <label class="" for="ft_designation">Designation</label>
                                                                    <input id="ft_designation" type="text"   class="crt_assts_17  form-control"  >
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="clearerfix"></div>
                                                        <div class="row">
                                                            <strong class="main-td">2.2 Other Promoters, if any (Mention their name and credentials) </strong>
                                                            <input type="button" value="+" onClick="aadTextBoxOP()"  class="btn btn-warning">
                                                            <input type="button" value="-"  onClick="removeTextBoxOP()" class="btn btn-warning">
                                                        </div>
                                                        <div id="op">
                                                            <div id="op_child">
                                                                <div class="col-xs-6">
                                                                    <label class="" for="ot_name">Name/s </label>
                                                                    <input id="ot_name" type="text"   class="crt_assts_17  form-control"  >
                                                                </div>
                                                                
                                                                
                                                                <div class="col-xs-6">
                                                                    <label class="" for="ot_designation">Designation</label>
                                                                    <input id="ot_designation" type="text"   class="crt_assts_17  form-control"  >
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearerfix"></div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="membersDiv" class="form-group">
                                                        <div class="col-md-6">
                                                            <label for="email">2.1 Total members in the cooperative /producer Company /SHG<input type="text" id="coopSgh" name="cooperSgh" class="form-control"></label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for="email">2.2 Number of members in the Management  Committee<input type="text" id="coopComittee" name="cooperComittee" class="form-control"></label>
                                                        </div>
                                                        <div class="clearerfix"></div>
                                                        <div class="give-padding"></div>
                                                        <div>
                                                            <strong class="main-td">2.3 Credentials of the key Office Bearers</strong>
                                                            <input type="button" value="+" onClick="aadTextBoxCB()"  class="btn btn-warning">
                                                            <input type="button" value="-"  onClick="removeTextBoxCB()" class="btn btn-warning">
                                                        </div>
                                                        <div class="clearerfix"></div>
                                                        <div id="cb">
                                                            <div id="cb_child">
                                                                <div class="col-xs-3">
                                                                    <label class="" for="cb_name">Name/s </label>
                                                                    <input id="cb_name" type="text"   class="crt_assts_17  form-control"  >
                                                                </div>
                                                                <div class="col-xs-3">
                                                                    <label class="" for="cb_designation">Designation</label>
                                                                    <input id="cb_designation" type="text"   class="crt_assts_17  form-control"  >
                                                                </div>
                                                                <div class="col-xs-3">
                                                                    <label class="" for="cb_phone">Phone</label>
                                                                    <input id="cb_phone" type="text"   class="crt_assts_17  form-control"  >
                                                                </div>
                                                                <div class="col-xs-3">
                                                                    <label class="" for="cb_email">Email</label>
                                                                    <input id="cb_email" type="text"   class="crt_assts_17  form-control"  >
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" id="foundJson" class="form-control" name="foundJson">
                                            <input type="hidden" id="promotorJson" class="form-control" name="promotorJson">
                                            <input type="hidden" id="bearerJson" class="form-control" name="bearerJson">
                                            <div class="clearerfix"></div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="" for=" moa">3. Memorandum of Association (MoA) (of the enterprise and/or of the co-producers)</label>
                                                        <div class="col-md-12">
													        <div class="input-file1">
													            <input id="moa" type="file" name="file1" class="form-control demoInputBox" onchange="ValidateSize(this)" accept="application/pdf,image/jpeg" >
													            <span id="file_error"></span>
													            <div id="myProgress1">
													                <div id="myBar1">0%</div>
													            </div>
													            <button class="upload-btn1 btn btn-primary" onclick="move()">Upload</button>
													            <a href="javascript:void(0)" class="close-file1 btn btn-primary">X</a>
													        </div>
													    </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="" for=" aoa">4. Articles of Association (AoA) (of the enterprise and/or of the co-producers)</label>
                                                        <div class="col-md-12">
													        <div class="input-file2">
													            <input id="aoa" type="file" name="file2" class="form-control" onchange="ValidateSize(this)" accept="application/pdf,image/jpeg">
													            <div id="myProgress2">
													                <div id="myBar2">0%</div>
													            </div>
													            <button class="upload-btn2 btn btn-primary" onclick="move()">Upload</button>
													            <a href="javascript:void(0)" class="close-file2 btn btn-primary">X</a>
													        </div>
													    </div>
                                                    </div>
                                                </div> 
                                            </div>
                                            <div class="clearerfix"></div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="email">
                                                            5. Organization Registration Certificate*
                                                        </label> 
                                                        <div class="col-md-12">
													        <div class="input-file3">
													            <input type="file" class="form-control" onchange="ValidateSize(this)" id="orfile" name="file3" multiple accept="application/pdf,image/jpeg" required>
													            <div id="myProgress3">
													                <div id="myBar3">0%</div>
													            </div>
													            <button class="upload-btn3 btn btn-primary" onclick="move()">Upload</button>
													            <a href="javascript:void(0)" class="close-file3 btn btn-primary">X</a>
													        </div>
													        <div class="help-block with-errors"></div>
													    </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearerfix"></div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="email">
                                                            6. Does your enterprise have any relevant industry certificate?
                                                        </label>
                                                        <input type="radio" name="redioCert" value="yes"> Yes
                                                        <input type="radio" name="redioCert" value="no" checked> No
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearerfix"></div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group" id="industryCert">
                                                        <label for="email">
                                                            6.1 If yes, please  provide the industry  certificate
                                                        </label>
           
                                                        <div class="col-md-12">
													        <div class="input-file4">
													            <input type="file" class="form-control" id="inFile" name="file4" onchange="ValidateSize(this)" accept="application/pdf,image/jpeg" multiple>
													            <div id="myProgress4">
													                <div id="myBar4">0%</div>
													            </div>
													            <button class="upload-btn4 btn btn-primary" onclick="move()">Upload</button>
													            <a href="javascript:void(0)" class="close-file4 btn btn-primary">X</a>
													        </div>
													    </div>
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="clearerfix"></div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="email">
                                                            7. Basic Financial Details of the Enterprise -
                                                        </label>
                                                        <table class="table table-bordered" id="basicFinancial">
                                                            <tr>
                                                                <th><b>Key Financial Data/Financial Year</b></th>
                                                                <th>2014-15</th>
                                                                <th>2015-16</th>
                                                                <th>2016-17</th>
                                                                <th>2017-18</th>
                                                            </tr>
                                                            <tr>
																<td>Revenue  of the Enterprise</td>
																<td ><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="Revenue14" class="form-control onlyDigit" name="reve2014" placeholder="Only Digit" onkeyup="reve2014_word.innerHTML=convertNumberToWords(this.value)"><div id="reve2014_word"></div></div></td>
																<td ><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="Revenue15" class="form-control onlyDigit" name="reve2015" placeholder="Only Digit" onkeyup="reve2015_word.innerHTML=convertNumberToWords(this.value)"><div id="reve2015_word"></div></div></td>
																<td ><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="Revenue16" class="form-control onlyDigit" name="reve2016" placeholder="Only Digit" onkeyup="reve2016_word.innerHTML=convertNumberToWords(this.value)"><div id="reve2016_word"></div></div></td>
																<td ><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="Revenue17" class="form-control onlyDigit" name="reve2017" placeholder="Only Digit" onkeyup="reve2017_word.innerHTML=convertNumberToWords(this.value)"><div id="reve2017_word"></div></div></td>
															</tr>
															<tr>
																<td>Net Assets</td>
																<td ><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="NetAssert14" class="form-control onlyDigit" name="netAssert2014" placeholder="Only Digit" onkeyup="netAssert2014_word.innerHTML=convertNumberToWords(this.value)"><div id="netAssert2014_word"></div></div></td>
																<td ><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="NetAssert15" class="form-control onlyDigit" name="netAssert2015" placeholder="Only Digit" onkeyup="netAssert2015_word.innerHTML=convertNumberToWords(this.value)"><div id="netAssert2015_word"></div></div></td>
																<td ><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="NetAssert16" class="form-control onlyDigit" name="netAssert2016" placeholder="Only Digit" onkeyup="netAssert2016_word.innerHTML=convertNumberToWords(this.value)"><div id="netAssert2016_word"></div></div></td>
																<td ><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="NetAssert17" class="form-control onlyDigit" name="netAssert2017" placeholder="Only Digit" onkeyup="netAssert2017_word.innerHTML=convertNumberToWords(this.value)"><div id="netAssert2017_word"></div></div></td>
															</tr>
															<tr>
																<td>Net Profit</td>
																<td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="NetProfit14" class="form-control onlyDigit" name="netProfit2014" placeholder="Only Digit" onkeyup="netProfit2014_word.innerHTML=convertNumberToWords(this.value)"><div id="netProfit2014_word"></div></div></td>
																<td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="NetProfit15" class="form-control onlyDigit" name="netProfit2015" placeholder="Only Digit" onkeyup="netProfit2015_word.innerHTML=convertNumberToWords(this.value)"><div id="netProfit2015_word"></div></div></td>
																<td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="NetProfit16" class="form-control onlyDigit" name="netProfit2016" placeholder="Only Digit" onkeyup="netProfit2016_word.innerHTML=convertNumberToWords(this.value)"><div id="netProfit2016_word"></div></div></td>
																<td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="NetProfit17" class="form-control onlyDigit" name="netProfit2017" placeholder="Only Digit" onkeyup="netProfit2017_word.innerHTML=convertNumberToWords(this.value)"><div id="netProfit2017_word"></div></div></td>
															</tr>
															<tr>
																<td>Number of Employees</td>
																<td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="employee14" class="form-control onlyDigit" name="employee2014" placeholder="Only Digit" onkeyup="employee2014_word.innerHTML=convertNumberToWords(this.value)"><div id="employee2014_word"></div></div></td>
																<td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="employee15" class="form-control onlyDigit" name="employee2015" placeholder="Only Digit" onkeyup="employee2015_word.innerHTML=convertNumberToWords(this.value)"><div id="employee2015_word"></div></div></td>
																<td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="employee16" class="form-control onlyDigit" name="employee2016" placeholder="Only Digit" onkeyup="employee2016_word.innerHTML=convertNumberToWords(this.value)"><div id="employee2016_word"></div></div></td>
																<td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="employee17" class="form-control onlyDigit" name="employee2017" placeholder="Only Digit" onkeyup="employee2017_word.innerHTML=convertNumberToWords(this.value)"><div id="employee2017_word"></div></div></td>
															</tr>
															<tr>
																<td>Number of Customers</td>
																<td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="customers14" class="form-control onlyDigit" name="customers2014" placeholder="Only Digit" onkeyup="customers2014_word.innerHTML=convertNumberToWords(this.value)"><div id="customers2014_word"></div></div></td>
																<td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="customers15" class="form-control onlyDigit" name="customers2015" placeholder="Only Digit" onkeyup="customers2015_word.innerHTML=convertNumberToWords(this.value)"><div id="customers2015_word"></div></div></td>
																<td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="customers16" class="form-control onlyDigit" name="customers2016" placeholder="Only Digit" onkeyup="customers2016_word.innerHTML=convertNumberToWords(this.value)"><div id="customers2016_word"></div></div></td>
																<td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="customers17" class="form-control onlyDigit" name="customers2017" placeholder="Only Digit" onkeyup="customers2017_word.innerHTML=convertNumberToWords(this.value)"><div id="customers2017_word"></div></div></td>
															</tr>
															<tr>
																<td>Total Cost of Goods/Services Sold</td>
																<td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="costGoods14" class="form-control onlyDigit" name="costGoods2014"  placeholder="Only Digit" onkeyup="costGoods2014_word.innerHTML=convertNumberToWords(this.value)"><div id="costGoods2014_word"></div></div></td>
																<td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="costGoods15" class="form-control onlyDigit" name="costGoods2015"  placeholder="Only Digit" onkeyup="costGoods2015_word.innerHTML=convertNumberToWords(this.value)"><div id="costGoods2015_word"></div></div></td>
																<td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="costGoods16" class="form-control onlyDigit" name="costGoods2016" placeholder="Only Digit" onkeyup="costGoods2016_word.innerHTML=convertNumberToWords(this.value)"><div id="costGoods2016_word"></div></div></td>
																<td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="costGoods17" class="form-control onlyDigit" name="costGoods2017" placeholder="Only Digit" onkeyup="costGoods2017_word.innerHTML=convertNumberToWords(this.value)"><div id="costGoods2017_word"></div></div></td>
															</tr>
                                                        </table>
                                                        <input type="hidden" id="totalBasicFinance" class="form-control" name="totalBasicFinance" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearerfix"></div>
                                            <div class="form-group">
                                                <h4>Declaration</h4>
                                            </div>
                                            <div class="form-group">
                                                <label class="checkbox-inline"><input type="checkbox" required >Nominee/Entrepreneur declares that I have not received the NEA Awards in any of the previous editions.</label>
                                            	<div class="help-block with-errors"></div>
                                            </div>
                                            <div class="form-group">
                                                <label class="checkbox-inline"><input type="checkbox" required >Nominee/Entrepreneur is a First Generation Entrepreneur. The definition of a first
                                                    generation entrepreneur for the scheme is "An entrepreneur who is not in the same line of
                                                    business as his/her parent. They should have started the enterprise on their own initiative,
                                                    idea or innovation".</label>
                                            	<div class="help-block with-errors"></div>
                                            </div>
                                            <div class="form-group">
                                                <label class="checkbox-inline"><input type="checkbox" required >The enterprise is duly registered.</label>
                                            	<div class="help-block with-errors"></div>
                                            </div>
                                            <div class="form-group">
                                                <label class="checkbox-inline"><input type="checkbox" required >Initial investment of the enterprise is between INR 1lakh up to INR 10 lakh.</label>
                                            	<div class="help-block with-errors"></div>
                                            </div>
                                            <div class="form-group">
                                                <label class="checkbox-inline"><input type="checkbox" required >By participating in the National Entrepreneurship Awards 2018, the nominee agrees to
                                                    receive mobile SMS, phone calls and email communication.</label>
                                            	<div class="help-block with-errors"></div>
                                            </div>
                                            <div class="form-group">
                                                <label class="checkbox-inline"><input type="checkbox" required >I / We hereby declare that all the information provided by me / us is true and correct to
                                                    the best of my/our knowledge. Any false information provided in the context of National
                                                    Entrepreneurship Awards Scheme by the nominee may result in the immediate eliminat ion
                                                    from the Award process.</label>
                                            	<div class="help-block with-errors"></div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-md-12 align-center">
                                                <input type="hidden" name="currentpath" value=""/>
                                                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                                    <button type="submit" id="submitForm" class="btn btn-primary">SUBMIT APPICATION</button>
                                                </div>
                                            </div>                                           
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end of class whitebg-->
                    </div>
                </div>
            </div>
        </main>
        <c:if test="${(not empty errorMsg) or (not empty successMsg)}">
            <div class="modal fade" id="errorModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <c:if test="${ not empty errorMsg }">
                                <h4 class="modal-title">Error</h4>
                            </c:if>
                            <c:if test="${ not empty successMsg }">
                                <h4 class="modal-title">Success</h4>
                            </c:if>
                        </div>
                        <div class="modal-body">
                            <p>
                                <c:if test="${ not empty errorMsg }">
                                    ${ errorMsg }
                                </c:if>
                                <c:if test="${ not empty successMsg }">
                                    ${ successMsg }
                                </c:if>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    
                </div>
            </div>
            <script>
                $(document).ready(function(){
                    if($('#errorModal').length > 0){
                        $('#errorModal').modal('show');
                    }
                });
            </script>
        </c:if>
        
        
        <c:if test="${(not empty errorMsg) or (not empty successMsg)}">
            <div class="modal fade" id="successModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <c:if test="${ not empty errorMsg }">
                                <h4 class="modal-title">Error</h4>
                            </c:if>
                            <c:if test="${ not empty successMsg }">
                                <h4 class="modal-title">Success</h4>
                            </c:if>
                        </div>
                        <div class="modal-body">
                            <p>
                                <c:if test="${ not empty errorMsg }">
                                    ${ errorMsg }
                                </c:if>
                                <c:if test="${ not empty successMsg }">
                                    ${ successMsg }
                                </c:if>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    
                </div>
            </div>
        </c:if>
        
        <!-- Body Ends Here -->
        <%@include file="fragments/footer.jsp" %>
        <script>
        function ValidateSize(file) {
            var FileSize = file.files[0].size / 1024 / 1024; // in MB
            var ext = $('#moa').val().split('.').pop().toLowerCase();
            var ext1 = $('#aoa').val().split('.').pop().toLowerCase();
            var ext2 = $('#orfile').val().split('.').pop().toLowerCase();
            var ext3 = $('#inFile').val().split('.').pop().toLowerCase();
            if (FileSize > 2.5) {
            	
                alert('File size exceeds 2.5 MB');
                $(file).val(''); //for clearing with Jquery
            	
            } else {
            	if ($.inArray(ext, ['pdf','jpg','jpeg']) == -1 && ext != ""){
            		alert('File format not supported, please upload jpeg/jpg/pdf only.');
            		$(file).val('');
            }
            	else if ($.inArray(ext1, ['pdf','jpg','jpeg']) == -1 && ext1 != ""){
            		alert('File format not supported, please upload jpeg/jpg/pdf only.');
            		$(file).val('');
            }
            	else if ($.inArray(ext2, ['pdf','jpg','jpeg']) == -1 && ext2 != ""){
            		alert('File format not supported, please upload jpeg/jpg/pdf only.');
            		$(file).val('');
            }
            	else if ($.inArray(ext3, ['pdf','jpg','jpeg']) == -1 && ext3 != ""){
            		alert('File format not supported, please upload jpeg/jpg/pdf only.');
            		$(file).val('');
            }
            	else{
            		if($(file).attr('id')=='moa'){
                 	   move1();
                    }
                    if($(file).attr('id')=='aoa'){
                 	   move2();
                    }
                    if($(file).attr('id')=='orfile'){
                 	   move3();
                    }
                    if($(file).attr('id')=='inFile'){
                 	   move4();
                    }
            	}
            	
            }
        }
        
        function move1() {
        	$("#myProgress1").show();
        	var e = document.getElementById("myBar1"),
        		l = 10,
        		n = setInterval(function() {
        			l >= 100 ? clearInterval(n) : (l++, e.style.width = l + "%", e.innerHTML = 1 * l + "%")
        		}, 50);
        	$(".close-file1").show();
        }


        $(".close-file1").on('click',function() {
        	var e = $("#moa");
        	$(".close-file1").hide(), $("#myProgress1").hide(), e.replaceWith(e.val("").clone(!0)), $(".input-file1").removeClass("adsadas")
        });

        function move2() {
        	$("#myProgress2").show();
        	var e = document.getElementById("myBar2"),
        		l = 10,
        		n = setInterval(function() {
        			l >= 100 ? clearInterval(n) : (l++, e.style.width = l + "%", e.innerHTML = 1 * l + "%")
        		}, 50);
        	$(".close-file2").show();
        }


        $(".close-file2").on('click',function() {
        	var e = $("#aoa");
        	$(".close-file2").hide(), $("#myProgress2").hide(), e.replaceWith(e.val("").clone(!0)), $(".input-file2").removeClass("adsadas")
        });

        function move3() {
        	$("#myProgress3").show();
        	var e = document.getElementById("myBar3"),
        		l = 10,
        		n = setInterval(function() {
        			l >= 100 ? clearInterval(n) : (l++, e.style.width = l + "%", e.innerHTML = 1 * l + "%")
        		}, 50);
        	$(".close-file3").show();
        }


        $(".close-file3").on('click',function() {
        	var e = $("#orfile");
        	$(".close-file3").hide(), $("#myProgress3").hide(), e.replaceWith(e.val("").clone(!0)), $(".input-file3").removeClass("adsadas")
        });


        function move4() {
        	$("#myProgress4").show();
        	var e = document.getElementById("myBar4"),
        		l = 10,
        		n = setInterval(function() {
        			l >= 100 ? clearInterval(n) : (l++, e.style.width = l + "%", e.innerHTML = 1 * l + "%")
        		}, 50);
        	$(".close-file4").show();
        }


        $(".close-file4").on('click',function() {
        	var e = $("#inFile");
        	$(".close-file4").hide(), $("#myProgress4").hide(), e.replaceWith(e.val("").clone(!0)), $(".input-file4").removeClass("adsadas")
        });
        
        
            var i=1,j=1,k=1,l=1,m=1,n=1,o=1,p=1;
            function aadTextBoxFT()
            {
            	$("#ft").append("<div id=ft_child"+i+"> <div class='col-xs-6'><label class='' for='ft_name'>Name/s </label><input id='ft_name"+(i++)+"' type='text'   class='crt_assts_17  form-control' ></div><div class='col-xs-6'><label class='' for='ft_designation'>Designation</label><input id='ft_designation"+(j++)+"'  type='text'   class='crt_assts_17  form-control' ></div></div>");
                
            }
            
            function removeTextBoxFT()
            {
                var idc = "ft_child"+(--i);
                document.getElementById('ft').removeChild(document.getElementById(idc));
                j--;
            }
            
            function aadTextBoxOP()
            {
            	$("#op").append("<div id=op_child"+k+"><div class='col-xs-6'><label class='' for='ot_name'>Name/s </label><input id='ot_name"+(k++)+"' type='text'   class='crt_assts_17  form-control' ></div><div class='col-xs-6'><label class='' for='ot_designation'>Designation</label><input id='ot_designation"+(l++)+"' type='text'   class='crt_assts_17  form-control' ></div></div>");
                
            }
            
            function removeTextBoxOP()
            {
                var idc = "op_child"+(--k);
                document.getElementById('op').removeChild(document.getElementById(idc));
                l--;
            }
            function aadTextBoxCB()
            {
            	$("#cb").append("<div id=cb_child"+m+"><div class='col-xs-3'><label class='' for='cb_name'>Name/s </label><input id='cb_name"+(m++)+"' type='text'   class='crt_assts_17  form-control' ></div><div class='col-xs-3'><label class='' for='cb_designation'>Designation</label><input id='cb_designation"+(n++)+"' type='text'   class='crt_assts_17  form-control' ></div><div class='col-xs-3'><label class='' for='cb_phone'>Phone</label><input id='cb_phone"+(o++)+"' type='text'   class='crt_assts_17  form-control' ></div><div class='col-xs-3'><label class='' for='cb_email'>Email</label><input id='cb_email"+(p++)+"' type='email'   class='crt_assts_17  form-control' ></div></div>");
                
            }
            
            function removeTextBoxCB()
            {
                var idc = "cb_child"+(--m);
                document.getElementById('cb').removeChild(document.getElementById(idc));
                n--;
                o--;
                p--;
            }
            
            
            $(document).ready(function(){
                $('#foundingDiv').hide();
                $('#membersDiv').hide();
               /*  $.validator.addMethod(
                	    "maxfilesize",
                	    function (value, element) {
                	        if (this.optional(element) || ! element.files || ! element.files[0]) {
                	            return true;
                	        } else {
                	            return element.files[0].size <= 1024 * 1024 * 2.5;
                	        }
                	    },
                	    'The file size can not exceed 2.5 MB.'
                	); */
                	
                
                $("#industryCert").hide();
                $("input[name='redioCert']").click(function(){
                    var radioValue = $("input[name='redioCert']:checked").val();
                    if(radioValue == 'yes'){
                        $("#industryCert").show();
                    }
                    if(radioValue == 'no'){
                        $("#industryCert").hide();
                    }
                });
                
                
                $('#businessType').change(function(){
                    if($('#businessType').val() === 'Company' || $('#businessType').val() === 'Partnership' ||
                        $('#businessType').val() === 'Limited Liability Partnership' || $('#businessType').val() === 'Proprietaryship'){
                            $('#foundingDiv').show();
                            $('#membersDiv').hide();
                        }
                    if($('#businessType').val() === 'Cooperative' || $('#businessType').val() === 'Producer Company' || $('#businessType').val() === 'SHG'
                    || $('#businessType').val() === 'JLG'){
                        $('#foundingDiv').hide();
                        $('#membersDiv').show();
                    }
                });
                
                
                $('#seccatform').on("submit",function(e){
                    
                    var jsonTableData = [];
                    $('#basicFinancial').find('tbody tr').each(function(index){
                        var $td = $(this).find('input');
                        var newObj = {};
                        $td.each(function(i){
                            
                            var key,val;
                            
                            key = $td.eq(i).attr("name");
                            val = $td.eq(i).val();
                            
                            newObj[key] = val;
                            
                        });
                        jsonTableData.push(newObj); 
                    });
                    
                    $('input[name=totalBasicFinance]').val(JSON.stringify(jsonTableData));
                    console.log(JSON.stringify(jsonTableData));
                    
                    
                    var jsonFT = [{}],jsonOP = [{}],jsonCB = [{}];
                    
                    var x = $('#ft').find('input');
                    var y = $('#op').find('input');
                    var z = $('#cb').find('input');
                    
                    var j=0,k=0,l=0;
                    for(var i=0;i<x.length;i=i+2){
                        var jsonx = {};
                        jsonx.name = x[i].value;
                        jsonx.desg = x[i+1].value;
                        jsonFT[j++]=jsonx;
                    }
                    for(var i=0;i<y.length;i=i+2){
                        var jsonx = {};
                        jsonx.name = y[i].value;
                        jsonx.desg = y[i+1].value;
                        jsonOP[k++]=jsonx;
                    }
                    for(var i=0;i<z.length;i=i+4){
                        var jsonx = {};
                        jsonx.name = z[i].value;
                        jsonx.desg = z[i+1].value;
                        jsonx.phone = z[i+2].value;
                        jsonx.email = z[i+3].value;
                        jsonCB[l++]=jsonx;
                    }
                   document.getElementById('foundJson').value = JSON.stringify(jsonFT);
                   document.getElementById('promotorJson').value = JSON.stringify(jsonOP);
                   document.getElementById('bearerJson').value = JSON.stringify(jsonCB);
                   return;
                    
                });
            });
                
                
           
        
            $('.input-file1 #myProgress1').hide();
            $('.upload-btn1').hide();
            $('.close-file1').hide();

            $('.input-file2 #myProgress2').hide();
            $('.upload-btn2').hide();
            $('.close-file2').hide();

            $('.input-file3 #myProgress3').hide();
            $('.upload-btn3').hide();
            $('.close-file3').hide();

            $('.input-file4 #myProgress4').hide();
            $('.upload-btn4').hide();
            $('.close-file4').hide();


            /* $('#moa').change(move1);
            $('#aoa').change(move2);
            $('#orfile').change(move3);
            $('#inFile').change(move4); */

            

                
                
        
            function convertNumberToWords(amount) {
        		var words = new Array();
        		words[0] = '';
        		words[1] = 'One';
        		words[2] = 'Two';
        		words[3] = 'Three';
        		words[4] = 'Four';
        		words[5] = 'Five';
        		words[6] = 'Six';
        		words[7] = 'Seven';
        		words[8] = 'Eight';
        		words[9] = 'Nine';
        		words[10] = 'Ten';
        		words[11] = 'Eleven';
        		words[12] = 'Twelve';
        		words[13] = 'Thirteen';
        		words[14] = 'Fourteen';
        		words[15] = 'Fifteen';
        		words[16] = 'Sixteen';
        		words[17] = 'Seventeen';
        		words[18] = 'Eighteen';
        		words[19] = 'Nineteen';
        		words[20] = 'Twenty';
        		words[30] = 'Thirty';
        		words[40] = 'Forty';
        		words[50] = 'Fifty';
        		words[60] = 'Sixty';
        		words[70] = 'Seventy';
        		words[80] = 'Eighty';
        		words[90] = 'Ninety';
        		amount = amount.toString();
        		var atemp = amount.split(".");
        		var number = atemp[0].split(",").join("");
        		var n_length = number.length;
        		var words_string = "";
        		if (n_length <= 9) {
        			var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        			var received_n_array = new Array();
        			for (var i = 0; i < n_length; i++) {
        				received_n_array[i] = number.substr(i, 1);
        			}
        			for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
        				n_array[i] = received_n_array[j];
        			}
        			for (var i = 0, j = 1; i < 9; i++, j++) {
        				if (i == 0 || i == 2 || i == 4 || i == 7) {
        					if (n_array[i] == 1) {
        						n_array[j] = 10 + parseInt(n_array[j]);
        						n_array[i] = 0;
        					}
        				}
        			}
        			value = "";
        			for (var i = 0; i < 9; i++) {
        				if (i == 0 || i == 2 || i == 4 || i == 7) {
        					value = n_array[i] * 10;
        				} else {
        					value = n_array[i];
        				}
        				if (value != 0) {
        					words_string += words[value] + " ";
        				}
        				if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
        					words_string += "Crores ";
        				}
        				if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
        					words_string += "Lakhs ";
        				}
        				if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
        					words_string += "Thousand ";
        				}
        				if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
        					words_string += "Hundred and ";
        				} else if (i == 6 && value != 0) {
        					words_string += "Hundred ";
        				}
        			}
        			words_string = words_string.split("  ").join(" ");
        		}
        		return words_string;
        	}
            
            $(document).ready(function () {
          	  	$(".onlyDigit").keypress(function (e) {
          		 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
          				   return false;
          		}
          	   });
          	});
            
            $(document).on("change", "#Revenue14", function() {
            	var fix_assets_sum_17 = 0;
            	$(".fix_assts_17").each(function(){
            	    fix_assets_sum_17 += +$(this).val();
            	});
            	$(".tot_fix_assts_17").val(fix_assets_sum_17);
            });
            
            function validate() {
            	$("#moa").html("");
            	$(".demoInputBox").css("border-color","#F0F0F0");
            	var file_size = $('#file')[0].files[0].size;
            	if(file_size>2621440) {
            		$("#moa").html("File size is greater than 2.5MB");
            		$(".demoInputBox").css("border-color","#FF0000");
            		return false;
            	} 
            	return true;
            }
        </script>
        
    </body>
</html>