<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<header>
   <div class="header-top">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-6">
               <div class="top-header-lft">
                  <sec:authorize access="isAnonymous()">
                     <a href="/login"><i class="fa fa-lock" aria-hidden="true"></i> Login</a>
                     <a href="/signup"><i class="fa fa-user" aria-hidden="true"></i> Register Now</a>
                  </sec:authorize>
                  <sec:authorize access="hasRole('NOMINATOR')">
                     <sec:authentication var="name" property="principal" />
                     <a href="/nominatorlanding">
                        <i class="fa fa-user-circle-o" aria-hidden="true"></i> Welcome, 
                        <span id="userFullName"></span>
                     </a>
                     <a href="/logout">Logout</a>
                  </sec:authorize>
                  <sec:authorize access="hasRole('NOMINEE')">
                     <sec:authentication var="name" property="principal" />
                     <a href="/nomineelanding">
                        <i class="fa fa-user-circle-o" aria-hidden="true"></i> Welcome, 
                        <span id="userFullName"></span>
                     </a>
                     <a href="/logout">Logout</a>
                  </sec:authorize>
                  <sec:authorize access="hasRole('ADMIN')">
                     <sec:authentication var="name" property="principal" />
                     <a href="/userdata">
                        <i class="fa fa-user-circle-o" aria-hidden="true"></i> Welcome, 
                        <span id="userFullName"></span>
                     </a>
                     <a href="/logout">Logout</a>
                  </sec:authorize>
                  <sec:authorize access="hasRole('SUPPORT')">
                     <sec:authentication var="name" property="principal" />
                     <a href="/userdata">
                        <i class="fa fa-user-circle-o" aria-hidden="true"></i> Welcome, 
                        <span id="userFullName"></span>
                     </a>
                     <a href="/logout">Logout</a>
                  </sec:authorize>
                  <sec:authorize access="hasRole('REGIONAL')">
                     <sec:authentication var="name" property="principal" />
                     <a href="/userdata">
                        <i class="fa fa-user-circle-o" aria-hidden="true"></i> Welcome, 
                        <span id="userFullName"></span>
                     </a>
                     <a href="/logout">Logout</a>
                  </sec:authorize>
                  <sec:authorize access="hasRole('EXPERT')">
                     <sec:authentication var="name" property="principal" />
                     <a href="/userdata">
                        <i class="fa fa-user-circle-o" aria-hidden="true"></i> Welcome, 
                        <span id="userFullName"></span>
                     </a>
                     <a href="/logout">Logout</a>
                  </sec:authorize>
                  
                  <a href="https://www.facebook.com/Youth4Enterprise/" target="_blank" rel="noopener noreferrer"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                  <a href="https://twitter.com/Youth4Entrprise" target="_blank" rel="noopener noreferrer"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
              </div>
            </div>
            <div class="col-md-6">
               <div class="top-header-rft">
                  <div class="aembootstrap-gv-rmds-skp">
                     <a href="javascript:;" title="Skip To Main Content">Skip To Main Content</a>
                  </div>
                  <div class="aembootstrap-gv-rmds-fnt">
                     <a href="javascript:void(0)" id="aembootstrap-gv-rmds-fnt-in" title="Increase Font Size">A+</a>
                     <a href="javascript:void(0)" id="aembootstrap-gv-rmds-fnt-df" title="Default Font Size">A</a>
                     <a href="javascript:;" id="aembootstrap-gv-rmds-fnt-dec" title="Decrease Font Size">A-</a>
                  </div>
                  <div class="aembootstrap-gv-rmds-colr">
                     <a href="javascript:;" id="aembootstrap-gv-rmds-col-o" class="bootstrap-gv-rmds-col" data-bg="#a0140c" data-fg="#fff" title="Dark Theme">Dark</a>
                     <a href="javascript:;" id="aembootstrap-gv-rmds-col-t" class="bootstrap-gv-rmds-col" data-bg="#000000" data-fg="#ffffff" title="light theme">Light</a>
                     <a href="javascript:;" id="aembootstrap-gv-rmds-col-th" class="bootstrap-gv-rmds-col" data-bg="#ffffff" data-fg="#000000" title="Default Theme">Default</a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--end of class header-top-->
   <div class="clearfix"></div>
   <div class="header-bottom">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-2 logo-center">
               <h1 class="logo"> <a href="#"><img src="/resources/images/logo.png" alt="Logo"></a></h1>
            </div>
            <div class="col-md-12">
               <nav class="navbar navbar-inverse" role="navigation">
                  <div class="container-fluid">
                     <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                     </div>
                     <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav">
                           <li><a href="/">Home</a></li>
                           <li class="dropdown dropdown-inline">
                              <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-animations="zoomIn zoomIn zoomIn zoomIn" aria-expanded="false" href="#">About NEA</a>
                              <ul class="dropdown-menu" role="menu">
                                 <li><a href="/neas/about/overview">Overview</a></li>
                                 <li><a href="/neas/about/award">Award Categories</a></li>
                                 <li><a href="/neas/about/eligibility">Eligibility 1</a></li>
				 <li><a href="/neas/about/eligibility/second">Eligibility 2</a></li>
                                 <li><a href="/neas/about/reward">Rewards</a></li>
                              </ul>
                           </li>
                           <li><a href="/neas/guidelines">General Rules</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                           <li class="dropdown dropdown-inline">
                              <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-animations="zoomIn zoomIn zoomIn zoomIn" aria-expanded="false" href="#">Past Awards</a>
                              <ul class="dropdown-menu" role="menu">      
                                 <li class="dropdown">
                                    <a href="#">NEA 2017 <span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdownhover-right">
                                       <li><a href="/neas/past/2017/jury">Jury</a>
                                       <li class="dropdown">
                                          <a href="#">Winners <span class="caret"></span></a>
                                          <ul class="dropdown-menu">
                                             <li>
                                                <a href="/neas/past/2017/winner/award">Award Track</a>
                                             </li>
                                             <li>
                                                <a href="/neas/past/2017/winner/recognition">Recognition Track</a>
                                             </li>
                                          </ul>
                                       </li>
                                    </ul>
                                 </li>
                                 <li class="dropdown">
                                    <a href="#">NEA 2016 <span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdownhover-right">
                                       <li><a href="/neas/past/2016/jury">Jury</a></li>
                                       <li class="dropdown">
                                          <a href="#">Winners <span class="caret"></span></a>
                                          <ul class="dropdown-menu">
                                             <li>
                                                <a href="/neas/past/2016/winner/award">Award Track</a>
                                             </li>
                                             <li>
                                                <a href="/neas/past/2016/winner/recognition">Recognition Track</a>
                                             </li>
                                          </ul>
                                       </li>
                                    </ul>
                                 </li>
                              </ul>
                           </li>
                           <li><a href="/neas/scheme">FAQ's</a></li>
                           <li><a href="/neas/contact">Contact Us</a></li>
                        </ul>
                     </div>
                  </div>
               </nav>
            </div>
         </div>
      </div>
   </div>
</header>