package com.xlri.awards.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;

@Entity
public class ThirdCatNominee {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String contact;
	private String enterpriseDate;
	@Type(type="text")
	private String foundingMember;
	@Type(type="text")
	private String otherPromoter;
	@Type(type="text")
	private String balanceJson;
	@Type(type="text")
	private String incomeJson;
	@Type(type="text")
	private String balanceSheet;
	@Type(type="text")
	private String profitLossStatement;
	@Type(type="text")
	private String cashFlowStatement;
	@Type(type="text")
	private String moaDoc;
	@Type(type="text")
	private String aoaDoc;
	@Type(type="text")
	private String industryCert;
	@Type(type="text")
	private String orgCert;
	private String formNumber;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getEnterpriseDate() {
		return enterpriseDate;
	}
	public void setEnterpriseDate(String enterpriseDate) {
		this.enterpriseDate = enterpriseDate;
	}
	public String getFoundingMember() {
		return foundingMember;
	}
	public void setFoundingMember(String foundingMember) {
		this.foundingMember = foundingMember;
	}
	public String getOtherPromoter() {
		return otherPromoter;
	}
	public void setOtherPromoter(String otherPromoter) {
		this.otherPromoter = otherPromoter;
	}
	public String getBalanceSheet() {
		return balanceSheet;
	}
	public void setBalanceSheet(String balanceSheet) {
		this.balanceSheet = balanceSheet;
	}
	public String getProfitLossStatement() {
		return profitLossStatement;
	}
	public void setProfitLossStatement(String profitLossStatement) {
		this.profitLossStatement = profitLossStatement;
	}
	public String getCashFlowStatement() {
		return cashFlowStatement;
	}
	public void setCashFlowStatement(String cashFlowStatement) {
		this.cashFlowStatement = cashFlowStatement;
	}
	public String getMoaDoc() {
		return moaDoc;
	}
	public void setMoaDoc(String moaDoc) {
		this.moaDoc = moaDoc;
	}
	public String getIndustryCert() {
		return industryCert;
	}
	public void setIndustryCert(String industryCert) {
		this.industryCert = industryCert;
	}
	public String getOrgCert() {
		return orgCert;
	}
	public void setOrgCert(String orgCert) {
		this.orgCert = orgCert;
	}
	public String getIncomeJson() {
		return incomeJson;
	}
	public void setIncomeJson(String incomeJson) {
		this.incomeJson = incomeJson;
	}
	public String getBalanceJson() {
		return balanceJson;
	}
	public void setBalanceJson(String balanceJson) {
		this.balanceJson = balanceJson;
	}
	public String getAoaDoc() {
		return aoaDoc;
	}
	public void setAoaDoc(String aoaDoc) {
		this.aoaDoc = aoaDoc;
	}
	public String getFormNumber() {
		return formNumber;
	}
	public void setFormNumber(String formNumber) {
		this.formNumber = formNumber;
	}
	
	
	
	
	
}
