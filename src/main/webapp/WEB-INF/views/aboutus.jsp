<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
   <%@include file="fragments/headerlibs.jsp" %>
   <body class="innerpage">
      <%@include file="fragments/header.jsp" %>
      <!-- Body Starts Here -->
      <main role="main">
         <div class="page-title">
            <h2>OVERVIEW</h2>
         </div>
         <div class="content">
            <div class="container">
               <div class="row">
                  <div class="col-md-12">
                     <div class="whitebg">
                        <div class="clearfix"></div>
                        <div class="col-xs-12 pull-left">
                           <p>As an important step to catalyse a cultural shift in youth for entrepreneurship, the Ministry of Skill Development and Entrepreneurship (MSDE) has instituted the National Entrepreneurship Awards (NEA) to recognise and honour outstanding young first generation Entrepreneurs and their Ecosystem Builders for their outstanding contribution in entrepreneurship development.
                           </p>
                           <p>A total of 43 Awards under NEA2018 have been carefully considered to enable participation of young achievers below the age of 40 years and their ecosystem builders across sectors, geographies and socio-economic background through the process of nomination. The purpose is to highlight models of excellence for others to emulate and improve upon.
                           </p>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-12 pull-left">
                           <h2 class="inner-heading blue-hdg">SUMMARY OF AWARD CATEGORIES</h2>
                           <div class="table-responsive">
                           <table class="table table-bordered">
                              <tbody>
                                 <tr>
                                    <td colspan="4">
                                       <p><strong>Award Category</strong></p>
                                    </td>
                                    <td >
                                       <p><strong>Types of Awards under each Category </strong></p>
                                    </td>
                                    <td >
                                       <p><strong>No. of Awards</strong></p>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td rowspan="6" >
                                       <p><strong>A</strong></p>
                                    </td>
                                    <td rowspan="6">
                                       <p><strong>Enterprise Category</strong></p>
                                    </td>
                                    <td rowspan="2" >
                                       <p><strong>A1 </strong></p>
                                    </td>
                                    <td rowspan="2" >
                                       <p><strong>Initial investment up to </strong><strong>₹</strong><strong>1 L</strong></p>
                                    </td>
                                    <td >
                                       <p>Sector Awards</p>
                                    </td>
                                    <td >
                                       <p>9</p>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td >
                                       <p>Special Category Awards</p>
                                    </td>
                                    <td >
                                       <p>4</p>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td rowspan="2" >
                                       <p><strong>A2 </strong></p>
                                    </td>
                                    <td rowspan="2">
                                       <p><strong>Initial investment above </strong><strong>₹</strong><strong>1 L to </strong><strong>₹</strong><strong>&nbsp; </strong><strong>10 L</strong></p>
                                    </td>
                                    <td >
                                       <p>Sector Awards</p>
                                    </td>
                                    <td>
                                       <p>9</p>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td >
                                       <p>Special Category Awards</p>
                                    </td>
                                    <td >
                                       <p>4</p>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td rowspan="2" >
                                       <p><strong>A3 </strong></p>
                                    </td>
                                    <td rowspan="2" >
                                       <p><strong>Initial investment above </strong><strong>₹</strong><strong> 10 L to </strong><strong>₹</strong> <strong>1 Cr</strong></p>
                                    </td>
                                    <td >
                                       <p>Sector Awards</p>
                                    </td>
                                    <td>
                                       <p>9</p>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td >
                                       <p>Special Category Awards</p>
                                    </td>
                                    <td >
                                       <p>4</p>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td >
                                       <p><strong>B</strong></p>
                                    </td>
                                    <td colspan="4" >
                                       <p><strong>Eco-system Builder Category </strong></p>
                                    </td>
                                    <td >
                                       <p>4</p>
                                    </td>
                                 </tr>
                                 <tr class="tfoot-blue">
                                    <td colspan="5" >
                                       <p><strong>Total Awards</strong></p>
                                    </td>
                                    <td >
                                       <p><strong>43</strong></p>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                       </div> </div>
                        <div class="col-xs-12 pull-left">
                           <h2 class="inner-heading blue-hdg">IMPLEMENTATION MECHANISM</h2>
                           <p>Candidates are being mobilised through nominations, and undergoing a three-level rigorous evaluation process, wherein, at the last step the National Jury of eminent persons drawn from academia/research, industry, social sector, banking etc selects the winners.  To ensure excellence of the National Award, the Jury has the power to not award any applicant in a given category, if applications are found unsatisfactory.
                           </p>
                        </div>
                        <div class="col-xs-12 pull-left">
                           <h2 class="inner-heading blue-hdg">AWARD CEREMONY</h2>
                           <p>The winners will be awarded in a high profile Award Ceremony in third week of December 2018.
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </main>
      <!-- Body Ends Here -->
      <%@include file="fragments/footer.jsp" %>
   </body>
</html>