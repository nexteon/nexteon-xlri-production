 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
   <%@include file="fragments/headerlibs.jsp" %>
   <body class="innerpage">
      <%@include file="fragments/header.jsp" %>
      <!-- Body Starts Here -->
      <div class="clearfix"></div>
      <main role="main">
         <div class="page-title">
            <h2>Financial & Operational Information Form</h2>
         </div>
         <div class="content">
            <div class="container">
               <div class="row">
                  <div class="col-md-12">
                     <div class="whitebg form-new-css">
                     <div class="row corner-box">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="registrationNumber">Form Number:
                                                            <span class="heading-span">${formNumber}</span></label>
                                                    </div>
                                                </div>
                                            </div>
                        <form role="form" action="/thirdnominee/${encryptedContact}" method="post" enctype="multipart/form-data" data-toggle="validator" id="submitForm">
                        <input type="hidden" name="formNumber" value="${formNumber}">
                           <h4>Submission of Financial & Operational Information by Nominee (Investment between INR 10 lakh to INR 1 crore)</h4>
	                        <%@ include file="thirdbasicdetails.jsp" %>
	                        <%@ include file="thirdincomesheet.jsp" %>
	                        <%@ include file="thirdfileupload.jsp" %>
	                        <div class="row">
                                <div class="col-md-12 align-center">
                                <input type="hidden" name="currentpath" value=""/>
                                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                    <button type="submit" id="submitForm" class="btn btn-primary">SUBMIT APPICATION</button>
                                </div>
                            </div>
                        </form>
                     </div>
                  </div>
                  <!--end of class whitebg-->
               </div>
            </div>
         </div>
      </main>
      <!-- Body Ends Here -->
      <%@include file="fragments/footer.jsp" %>
<script>
var i=1,j=1,k=1,l=1,m=1,n=1,o=1,p=1;
function aadTextBoxFT()
{
	$("#ft").append("<div id=ft_child"+i+"> <div class='col-xs-6'><label class='' for='ft_name'>Name/s </label><input id='ft_name"+(i++)+"' name='foundingName' type='text'   class='ft_crt_assts_17  form-control' ></div><div class='col-xs-6'><label class='' for='ft_designation'>Designation</label><input id='ft_designation"+(j++)+"' name='foundingDesignation' type='text'   class='ft_crt_assts_17  form-control' ></div></div>");

}

function removeTextBoxFT()
{
	var idc = "ft_child"+(--i);
	document.getElementById('ft').removeChild(document.getElementById(idc));
	j--;
}

function aadTextBoxOP()
{
	$("#op").append("<div id=op_child"+k+"><div class='col-xs-6'><label class='' for='ot_name'>Name/s </label><input id='ot_name"+(k++)+"' name='otherName' type='text'   class='ft_crt_assts_17  form-control' ></div><div class='col-xs-6'><label class='' for='ot_designation'>Designation</label><input id='ot_designation"+(l++)+"' name='otherDesignation' type='text'   class='ft_crt_assts_17  form-control' ></div></div>");

}

function removeTextBoxOP()
{
	var idc = "op_child"+(--k);
	document.getElementById('op').removeChild(document.getElementById(idc));
	l--;
}

function ValidateSize(file) {
    var FileSize = file.files[0].size / 1024 / 1024; // in MB
    var ext = $('#balance_sheet').val().split('.').pop().toLowerCase();
    var ext1 = $('#profit_loss_statement').val().split('.').pop().toLowerCase();
    var ext2 = $('#cash_flow_statement').val().split('.').pop().toLowerCase();
    var ext3 = $('#moa').val().split('.').pop().toLowerCase();
    var ext4 = $('#aoa').val().split('.').pop().toLowerCase();
    var ext5 = $('#mandatory_industry_certifications').val().split('.').pop().toLowerCase();
    var ext6 = $('#organization_registration_certificate').val().split('.').pop().toLowerCase();
    if (FileSize > 2.5) {
    	
        alert('File size exceeds 2.5 MB');
        $(file).val(''); //for clearing with Jquery
    	
    } else {
    	if ($.inArray(ext, ['pdf','jpg','jpeg']) == -1 && ext != ""){
    		alert('File format not supported, please upload jpeg/jpg/pdf only.');
    		$(file).val('');
    }
    	else if ($.inArray(ext1, ['pdf','jpg','jpeg']) == -1 && ext1 != ""){
    		alert('File format not supported, please upload jpeg/jpg/pdf only.');
    		$(file).val('');
    }
    	else if ($.inArray(ext2, ['pdf','jpg','jpeg']) == -1 && ext2 != ""){
    		alert('File format not supported, please upload jpeg/jpg/pdf only.');
    		$(file).val('');
    }
    	else if ($.inArray(ext3, ['pdf','jpg','jpeg']) == -1 && ext3 != ""){
    		alert('File format not supported, please upload jpeg/jpg/pdf only.');
    		$(file).val('');
    }
    	else if ($.inArray(ext4, ['pdf','jpg','jpeg']) == -1 && ext4 != ""){
    		alert('File format not supported, please upload jpeg/jpg/pdf only.');
    		$(file).val('');
    }
    	else if ($.inArray(ext5, ['pdf','jpg','jpeg']) == -1 && ext5 != ""){
    		alert('File format not supported, please upload jpeg/jpg/pdf only.');
    		$(file).val('');
    }
    	else if ($.inArray(ext6, ['pdf','jpg','jpeg']) == -1 && ext6 != ""){
    		alert('File format not supported, please upload jpeg/jpg/pdf only.');
    		$(file).val('');
    }
    	else{
    		if($(file).attr('id')=='balance_sheet'){
         	   move1();
            }
            if($(file).attr('id')=='profit_loss_statement'){
         	   move2();
            }
            if($(file).attr('id')=='cash_flow_statement'){
         	   move3();
            }
            if($(file).attr('id')=='moa'){
         	   move4();
            }
            if($(file).attr('id')=='aoa'){
          	   move5();
             }
             if($(file).attr('id')=='mandatory_industry_certifications'){
          	   move6();
             }
             if($(file).attr('id')=='organization_registration_certificate'){
          	   move7();
             }
    	}
    	
    }
}

function move1() {
	$("#myProgress1").show();
	var e = document.getElementById("myBar1"),
		l = 10,
		n = setInterval(function() {
			l >= 100 ? clearInterval(n) : (l++, e.style.width = l + "%", e.innerHTML = 1 * l + "%")
		}, 50);
	$(".close-file1").show();
}


$(".close-file1").click(function() {
	var e = $("#balance_sheet");
	$(".close-file1").hide(), $("#myProgress1").hide(), e.replaceWith(e.val("").clone(!0)), $(".input-file1").removeClass("adsadas")
});

function move2() {
	$("#myProgress2").show();
	var e = document.getElementById("myBar2"),
		l = 10,
		n = setInterval(function() {
			l >= 100 ? clearInterval(n) : (l++, e.style.width = l + "%", e.innerHTML = 1 * l + "%")
		}, 50);
	$(".close-file2").show();
}


$(".close-file2").click(function() {
	var e = $("#profit_loss_statement");
	$(".close-file2").hide(), $("#myProgress2").hide(), e.replaceWith(e.val("").clone(!0)), $(".input-file2").removeClass("adsadas")
});

function move3() {
	$("#myProgress3").show();
	var e = document.getElementById("myBar3"),
		l = 10,
		n = setInterval(function() {
			l >= 100 ? clearInterval(n) : (l++, e.style.width = l + "%", e.innerHTML = 1 * l + "%")
		}, 50);
	$(".close-file3").show();
}


$(".close-file3").click(function() {
	var e = $("#cash_flow_statement");
	$(".close-file3").hide(), $("#myProgress3").hide(), e.replaceWith(e.val("").clone(!0)), $(".input-file3").removeClass("adsadas")
});


function move4() {
	$("#myProgress4").show();
	var e = document.getElementById("myBar4"),
		l = 10,
		n = setInterval(function() {
			l >= 100 ? clearInterval(n) : (l++, e.style.width = l + "%", e.innerHTML = 1 * l + "%")
		}, 50);
	$(".close-file4").show();
}


$(".close-file4").click(function() {
	var e = $("#moa");
	$(".close-file4").hide(), $("#myProgress4").hide(), e.replaceWith(e.val("").clone(!0)), $(".input-file4").removeClass("adsadas")
});

function move5() {
	$("#myProgress5").show();
	var e = document.getElementById("myBar5"),
		l = 10,
		n = setInterval(function() {
			l >= 100 ? clearInterval(n) : (l++, e.style.width = l + "%", e.innerHTML = 1 * l + "%")
		}, 50);
	$(".close-file5").show();
}


$(".close-file5").click(function() {
	var e = $("#aoa");
	$(".close-file5").hide(), $("#myProgress5").hide(), e.replaceWith(e.val("").clone(!0)), $(".input-file5").removeClass("adsadas")
});

function move6() {
	$("#myProgress6").show();
	var e = document.getElementById("myBar6"),
		l = 10,
		n = setInterval(function() {
			l >= 100 ? clearInterval(n) : (l++, e.style.width = l + "%", e.innerHTML = 1 * l + "%")
		}, 50);
	$(".close-file6").show();
}


$(".close-file6").click(function() {
	var e = $("#mandatory_industry_certifications");
	$(".close-file6").hide(), $("#myProgress6").hide(), e.replaceWith(e.val("").clone(!0)), $(".input-file6").removeClass("adsadas")
});


function move7() {
	$("#myProgress7").show();
	var e = document.getElementById("myBar7"),
		l = 10,
		n = setInterval(function() {
			l >= 100 ? clearInterval(n) : (l++, e.style.width = l + "%", e.innerHTML = 1 * l + "%")
		}, 50);
	$(".close-file7").show();
}


$(".close-file7").click(function() {
	var e = $("#organization_registration_certificate");
	$(".close-file7").hide(), $("#myProgress7").hide(), e.replaceWith(e.val("").clone(!0)), $(".input-file7").removeClass("adsadas")
});

$(document).ready(function(){

	$('#submitForm').on("click",function(e){
		
		var jsonTableData = [];
  		$('#basicFinancial').find('tbody tr').each(function(index){
  		    var $td = $(this).find('input');
  		  	var newObj = {};
  		    $td.each(function(i){
  		    	 
  		    	 var key,val;
				
  		    	 key = $td.eq(i).data("name");
  		    	 val = $td.eq(i).val();
  		    	 
  		    	newObj[key] = val;
  		    	 	
  		    });
  		  jsonTableData.push(newObj); 
  		});
  		
  		$('input[name=totalBasicFinance]').val(JSON.stringify(jsonTableData));
  		console.log(JSON.stringify(jsonTableData));
				
  		var jsonBalanceTable = [];
  		$('#balance-details').find('tbody tr').each(function(index){
  		    var $td = $(this).find('input');
  		  	var newObj = {};
  		    $td.each(function(i){
  		    	 
  		    	 var key,val;
				
  		    	 key = $td.eq(i).data("name");
  		    	 val = $td.eq(i).val();
  		    	 
  		    	newObj[key] = val;
  		    	 	
  		    });
  		  jsonBalanceTable.push(newObj); 
  		});
  		
  		$('input[name=balanceJson]').val(JSON.stringify(jsonBalanceTable));
  		console.log(JSON.stringify(jsonBalanceTable));
  		
		
		var jsonFT = [{}],jsonOP = [{}];
		
		var x = $('#ft').find('input');
		var y = $('#op').find('input');
		
	    var j=0,k=0,l=0;
	    for(var i=0;i<x.length;i=i+2){
	      	var jsonx = {};
	      	jsonx.name = x[i].value;
	        jsonx.desg = x[i+1].value;
	        jsonFT[j++]=jsonx;
	    }
	    for(var i=0;i<y.length;i=i+2){
	      	var jsonx = {};
	      	jsonx.name = y[i].value;
	        jsonx.desg = y[i+1].value;
	        jsonOP[k++]=jsonx;
	    }

	    $('input[name=foundingJson]').val(JSON.stringify(jsonFT));
		console.log(JSON.stringify(jsonFT));
		$('input[name=promotorJson]').val(JSON.stringify(jsonOP));
		console.log(JSON.stringify(jsonOP));
		return;
	});
	
});

/* Current assets 17-18   */
$(document).on("change", ".crt_assts_17", function() {
var current_assets_sum_17 = 0;
$(".crt_assts_17").each(function(){
    current_assets_sum_17 += +$(this).val();
});
$(".tot_crt_assts_17").val(current_assets_sum_17);
});
    
/* Current assets 16-17   */
$(document).on("change", ".crt_assts_16", function() {
var current_assets_sum_16 = 0;
$(".crt_assts_16").each(function(){
    current_assets_sum_16 += +$(this).val();
});
$(".tot_crt_assts_16").val(current_assets_sum_16);
});
    
/* Current assets 15-16   */
$(document).on("change", ".crt_assts_15", function() {
var current_assets_sum_15 = 0;
$(".crt_assts_15").each(function(){
    current_assets_sum_15 += +$(this).val();
});
$(".tot_crt_assts_15").val(current_assets_sum_15);
});

/* Current assets 14-15   */
$(document).on("change", ".crt_assts_14", function() {
var current_assets_sum_14 = 0;
$(".crt_assts_14").each(function(){
    current_assets_sum_14 += +$(this).val();
});
$(".tot_crt_assts_14").val(current_assets_sum_14);
});
    
    
    
/* Fixed assets 17-18   */
$(document).on("change", ".fix_assts_17", function() {
var fix_assets_sum_17 = 0;
$(".fix_assts_17").each(function(){
    fix_assets_sum_17 += +$(this).val();
});
$(".tot_fix_assts_17").val(fix_assets_sum_17);
});
    
 /* Fixed assets 16-17   */   
$(document).on("change", ".fix_assts_16", function() {
var fix_assets_sum_16 = 0;
$(".fix_assts_16").each(function(){
    fix_assets_sum_16 += +$(this).val();
});
$(".tot_fix_assts_16").val(fix_assets_sum_16);
});
    
/* Current assets 15-16   */
$(document).on("change", ".fix_assts_15", function() {
var fix_assets_sum_15 = 0;
$(".fix_assts_15").each(function(){
    fix_assets_sum_15 += +$(this).val();
});
$(".tot_fix_assts_15").val(fix_assets_sum_15);
});
    
/* total for Fixed and current assets   */  
$(document).on('change', 'input', function() {        
var primaryincome17 = $(".tot_crt_assts_17").val();
var otherincome17 = $(".tot_fix_assts_17").val();
var totalincome17 = parseInt(primaryincome17) + parseInt(otherincome17);
if(!isNaN(totalincome17)){	
	$(".total_assts_17").val(totalincome17);//total_assts_17
	total_assts_17_word.innerHTML=convertNumberToWords(totalincome17);
}
    
var primaryincome16 = $(".tot_crt_assts_16").val();
var otherincome16 = $(".tot_fix_assts_16").val();
var totalincome16 = parseInt(primaryincome16) + parseInt(otherincome16);
if(!isNaN(totalincome16)){	
	$(".total_assts_16").val(totalincome16);
	total_assts_16_word.innerHTML=convertNumberToWords(totalincome16);
}  

var primaryincome15 = $(".tot_crt_assts_15").val();
var otherincome15 = $(".tot_fix_assts_15").val();
var totalincome15 = parseInt(primaryincome15) + parseInt(otherincome15);    
if(!isNaN(totalincome15)){
	$(".total_assts_15").val(totalincome15);
	total_assts_15_word.innerHTML=convertNumberToWords(totalincome15);
}

var primaryincome14 = $(".tot_crt_assts_14").val();
var otherincome14 = $(".tot_fix_assts_14").val();
var totalincome14 = parseInt(primaryincome14) + parseInt(otherincome14);    
if(!isNaN(totalincome14)){
	$(".total_assts_14").val(totalincome14);
	total_assts_14_word.innerHTML=convertNumberToWords(totalincome14);
}    
});

// Set focus for 
$( ".tot_fix_assts_14" ).change(function() {
	$( ".current_short_term_liabilities_17" ).focus();
});

$( ".long_term_liabilities_14 " ).change(function() {
	$( ".shareholders_equity_17" ).focus();
});

/* total for Net Worth by MANISH */  

// validation to allow only 
		$(document).ready(function () {
	  //called when key is pressed in textbox
	  	$(".onlyDigit").keypress(function (e) {
		 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
				   return false;
		}
	   });
	});

//-------
		function convertNumberToWords(amount) {
		var words = new Array();
		words[0] = '';
		words[1] = 'One';
		words[2] = 'Two';
		words[3] = 'Three';
		words[4] = 'Four';
		words[5] = 'Five';
		words[6] = 'Six';
		words[7] = 'Seven';
		words[8] = 'Eight';
		words[9] = 'Nine';
		words[10] = 'Ten';
		words[11] = 'Eleven';
		words[12] = 'Twelve';
		words[13] = 'Thirteen';
		words[14] = 'Fourteen';
		words[15] = 'Fifteen';
		words[16] = 'Sixteen';
		words[17] = 'Seventeen';
		words[18] = 'Eighteen';
		words[19] = 'Nineteen';
		words[20] = 'Twenty';
		words[30] = 'Thirty';
		words[40] = 'Forty';
		words[50] = 'Fifty';
		words[60] = 'Sixty';
		words[70] = 'Seventy';
		words[80] = 'Eighty';
		words[90] = 'Ninety';
		amount = amount.toString();
		var atemp = amount.split(".");
		var number = atemp[0].split(",").join("");
		var n_length = number.length;
		var words_string = "";
		if (n_length <= 9) {
			var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
			var received_n_array = new Array();
			for (var i = 0; i < n_length; i++) {
				received_n_array[i] = number.substr(i, 1);
			}
			for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
				n_array[i] = received_n_array[j];
			}
			for (var i = 0, j = 1; i < 9; i++, j++) {
				if (i == 0 || i == 2 || i == 4 || i == 7) {
					if (n_array[i] == 1) {
						n_array[j] = 10 + parseInt(n_array[j]);
						n_array[i] = 0;
					}
				}
			}
			value = "";
			for (var i = 0; i < 9; i++) {
				if (i == 0 || i == 2 || i == 4 || i == 7) {
					value = n_array[i] * 10;
				} else {
					value = n_array[i];
				}
				if (value != 0) {
					words_string += words[value] + " ";
				}
				if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
					words_string += "Crores ";
				}
				if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
					words_string += "Lakhs ";
				}
				if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
					words_string += "Thousand ";
				}
				if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
					words_string += "Hundred and ";
				} else if (i == 6 && value != 0) {
					words_string += "Hundred ";
				}
			}
			words_string = words_string.split("  ").join(" ");
		}
		return words_string;
	}
//-------
/*
$(document).on('change', 'input', function() {        
var primaryincome17 = $(".total_assts_17").val();
var otherincome17 = $(".total_liabilities_17").val();
var totalincome17 = parseInt(primaryincome17) - parseInt(otherincome17);    
$(".net_worth_17").val(totalincome17); 
    
var primaryincome16 = $(".total_assts_16").val();
var otherincome16 = $(".total_liabilities_16").val();
var totalincome16 = parseInt(primaryincome16) - parseInt(otherincome16);    
$(".net_worth_16").val(totalincome16);
    
var primaryincome15 = $(".total_assts_15").val();
var otherincome15 = $(".total_liabilities_15").val();
var totalincome15 = parseInt(primaryincome15) - parseInt(otherincome15);    
$(".net_worth_15").val(totalincome15);

var primaryincome15 = $(".total_assts_14").val();
var otherincome15 = $(".total_liabilities_14").val();
var totalincome15 = parseInt(primaryincome15) - parseInt(otherincome15);    
$(".net_worth_14").val(totalincome15);
    
});
*/

// ADDED ON 27-09-18 
/* total for Total Liabilities   */  
$(document).on('change', 'input', function() {        
var primaryincome17 = $(".current_short_term_liabilities_17").val();
var otherincome17 = $(".long_term_liabilities_17").val();
var totalincome17 = parseInt(primaryincome17) + parseInt(otherincome17);
if(!isNaN(totalincome17)){	
	$(".total_liabilities_17").val(totalincome17);
	total_liabilities_17_word.innerHTML=convertNumberToWords(totalincome17);
}	
 

//
var pincome17 = $(".total_assts_17").val();
var oincome17 = $(".total_liabilities_17").val();
var tncome17 = parseInt(pincome17) - parseInt(oincome17);    
if(!isNaN(tncome17)){	
	$(".net_worth_17").val(tncome17);
	if(tncome17 < 0){
		net_worth_17_word.innerHTML="Negative "+convertNumberToWords(Math.abs(tncome17));
	}else{
		net_worth_17_word.innerHTML=convertNumberToWords(tncome17);
	}
}
//
    
var primaryincome16 = $(".current_short_term_liabilities_16").val();
var otherincome16 = $(".long_term_liabilities_16").val();
var totalincome16 = parseInt(primaryincome16) + parseInt(otherincome16);   
if(!isNaN(totalincome16)){	
	$(".total_liabilities_16").val(totalincome16);
	total_liabilities_16_word.innerHTML=convertNumberToWords(totalincome16);
}
//
var pincome16 = $(".total_assts_16").val();
var oincome16 = $(".total_liabilities_16").val();
var tncome16 = parseInt(pincome16) - parseInt(oincome16);    
if(!isNaN(tncome16)){	
	$(".net_worth_16").val(tncome16);
	if(tncome16 < 0){
		net_worth_16_word.innerHTML="Negative "+convertNumberToWords(Math.abs(tncome16));
	}else{
		net_worth_16_word.innerHTML=convertNumberToWords(tncome16);
	}
}
//    

var primaryincome15 = $(".current_short_term_liabilities_15").val();
var otherincome15 = $(".long_term_liabilities_15").val();
var totalincome15 = parseInt(primaryincome15) + parseInt(otherincome15);    
if(!isNaN(totalincome15)){	
	$(".total_liabilities_15").val(totalincome15);
	total_liabilities_15_word.innerHTML=convertNumberToWords(totalincome15);
}

//
var pincome15 = $(".total_assts_15").val();
var oincome15 = $(".total_liabilities_15").val();
var tncome15 = parseInt(pincome15) - parseInt(oincome15);    
if(!isNaN(tncome15)){	
	$(".net_worth_15").val(tncome15);
	if(tncome15 < 0){
		net_worth_15_word.innerHTML="Negative "+convertNumberToWords(Math.abs(tncome15));
	}else{
		net_worth_15_word.innerHTML=convertNumberToWords(tncome15);
	}
}
//

var primaryincome14 = $(".current_short_term_liabilities_14").val();
var otherincome14 = $(".long_term_liabilities_14").val();
var totalincome14 = parseInt(primaryincome14) + parseInt(otherincome14);    
if(!isNaN(totalincome14)){	
	$(".total_liabilities_14").val(totalincome14);
	total_liabilities_14_word.innerHTML=convertNumberToWords(totalincome14);
}

//
var pincome14 = $(".total_assts_14").val();
var oincome14 = $(".total_liabilities_14").val();
var tncome14 = parseInt(pincome14) - parseInt(oincome14);    
if(!isNaN(tncome14)){	
	$(".net_worth_14").val(tncome14);
	if(tncome14 < 0){
		net_worth_14_word.innerHTML="Negative "+convertNumberToWords(Math.abs(tncome14));
	}else{
		net_worth_14_word.innerHTML=convertNumberToWords(tncome14);
	}
	
}
//
});
/* ------ End by Manish ---*/

var form = document.getElementById('balance-sheet'); // form has to have ID: <form id="formID">
form.noValidate = true;
form.addEventListener('submit', function(event) { // listen for form submitting
    if (!event.target.checkValidity()) {
        event.preventDefault(); // dismiss the default functionality
        alert('Please, fill all form field'); // error message
    }
}, false);

</script>

<!--input type range script  -->
<script>
$('input[type="range"]').on('input', function() {

var control = $(this),
controlMin = control.attr('min'),
controlMax = control.attr('max'),
controlVal = control.val(),
controlThumbWidth = control.data('thumbwidth');

var range = controlMax - controlMin;

var position = ((controlVal - controlMin) / range) * 100;
var positionOffset = Math.round(controlThumbWidth * position / 100) - (controlThumbWidth / 2);
var output = control.next('output');

output
.css('left', 'calc(' + position + '% - ' + positionOffset + 'px)')
.text(controlVal);

});  
/* negative number script   */ 
$("#balance-sheet").keypress(function(event) {
if ( event.which == 45 || event.which == 189 ) {
  event.preventDefault();
}
});


$('.input-file1 #myProgress1').hide();
$('.upload-btn1').hide();
$('.close-file1').hide();

$('.input-file2 #myProgress2').hide();
$('.upload-btn2').hide();
$('.close-file2').hide();

$('.input-file3 #myProgress3').hide();
$('.upload-btn3').hide();
$('.close-file3').hide();

$('.input-file4 #myProgress4').hide();
$('.upload-btn4').hide();
$('.close-file4').hide();

$('.input-file5 #myProgress5').hide();
$('.upload-btn5').hide();
$('.close-file5').hide();

$('.input-file6 #myProgress6').hide();
$('.upload-btn6').hide();
$('.close-file6').hide();

$('.input-file7 #myProgress7').hide();
$('.upload-btn7').hide();
$('.close-file7').hide();



/* $('#balance_sheet').change(move1);
$('#profit_loss_statement').change(move2);
$('#cash_flow_statement').change(move3);
$('#moa').change(move4);
$('#aoa').change(move5);
$('#mandatory_industry_certifications').change(move6);
$('#organization_registration_certificate').change(move7); */


</script>
</body>
</html>