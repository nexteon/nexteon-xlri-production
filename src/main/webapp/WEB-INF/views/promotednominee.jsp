<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">
<%@include file="dashboard/headerlibs.jsp"%>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
	<div class="page-wrapper">
		<%@include file="dashboard/header.jsp"%>



		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<!-- BEGIN CONTENT BODY -->
			<div class="page-content">
				<!-- BEGIN PAGE HEADER-->

				<!-- BEGIN PAGE BAR -->
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li><a href="/userdata">Home</a> <i class="fa fa-circle"></i>
						</li>
						<li><span>Promoted Nominee(By Regional)</span></li>
					</ul>
					<div class="page-toolbar">
						<div id="dashboard-report-range"
							class="pull-right tooltips btn btn-sm" data-container="body"
							data-placement="bottom"
							data-original-title="Change dashboard date range">
							<i class="icon-calendar"></i>&nbsp; <span
								class="thin uppercase hidden-xs"></span>&nbsp; <i
								class="fa fa-angle-down"></i>
						</div>
					</div>
				</div>
				<!-- END PAGE BAR -->
				<!-- BEGIN PAGE TITLE-->
				<h1 class="page-title">Promoted Nominee(By Regional)</h1>
				<!-- END PAGE TITLE-->
				<!-- END PAGE HEADER-->
				<!-- BEGIN DASHBOARD STATS 1-->

				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet light portlet-fit bordered">
							
								</div>
								<div class="table-responsive promoted-nominee-by-regional">
								<div class="total_record">Total Count:
                    <div id="filter-count1"></div>
                </div>
								
								<table class="table table-striped table-bordered table-hover table-checkable order-column dataTable" id="example" class="display">
                                            <thead>
                         <tr>
                         					<th>Id</th>
                         					<th>Nominee Name</th>
											<th>Email Id</th>
											<th>Ranking</th>
											<th>Contact</th>
											<th>Award Category</th>
											<th>Enterprise Category</th>
											<th>State</th>
											<th>Download</th>
                          
                       </tr>
                     </thead>
                                            
                                            <tbody>
                                            </tbody>
                                  <tfoot>
               						<tr id="filters">
                         					<th>Id</th>
                         					<th>Nominee Name</th>
											<th>Email Id</th>
											<th>Ranking</th>
											<th>Contact</th>
											<th>Award Category</th>
											<th>Enterprise Category</th>
											<th>State</th>
											<th class="btn-aln-center"><a href="/createzip/fieldvisit/allpromoted" class="btn btn-primary">Download All</a></th>
                      				 </tr>
               					</tfoot>
    
                                             </table>
                                             </div>
                                             
                                             
                                             
														</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>




			</div>
		

	<!-- END QUICK NAV -->
	<%@include file="dashboard/footer.jsp"%>
	<%@include file="dashboard/datatablescript.jsp" %>
    
	<script>
	$('#example').DataTable( {
    	  "ajax": {
    	  "url": "/neas/api/promoted/data",
    	  "type" : 'POST',
    	"scrollX": true,
    	  "dataSrc": ""
    	  },
    	  "columns": [
			{ "data": "id" },
    	  { "data": "nomineename" },
    	  { "data": "emailid" },
    	  { "data": "rank" },
    	  { "data": "contactnumber" },
    	  { "data": "awardcategory" },
    	  { "data": "enterprisecategory" },
    	  { "data": "state" },
    	  { "data": "file" },
    	  ],dom: 'Bfrtip',
        buttons: [
                  'copyHtml5',
                  'excelHtml5',
                  'csvHtml5',
                  'pdfHtml5'
              ],  initComplete: function () {
    	      	  this.api().columns().every(function () {
    	      	  var column = this;

    	      	 
    	      	  var select = $('<select><option value=""></option></select>')
    	      	  .appendTo($("#filters").find("th").eq(column.index()))
    	      	  .on('change', function () {
    	      	  var val = $.fn.dataTable.util.escapeRegex(
    	      	  $(this).val()); 

    	      	  column.search(val ? '^' + val + '$' : '', true, false)
    	      	  .draw();
    	      	  }); 
    	      	  //console.log(select);
    	      	  column.data().unique().sort().each(function (d, j) {
    	      	  select.append('<option value="' + d + '">' + d + '</option>')
    	      	  });
    	      	  });
    	      	  }
    	      	  } );
	</script>
	<%@include file="dashboard/disablewarning.jsp" %>
	
	<style>
	.promoted-nominee-by-regional .total_record {
    display: none;
    font-size: 20px;
    text-align: center;
}
.promoted-nominee-by-regional tfoot tr th:nth-child(1) select,.promoted-nominee-by-regional tfoot tr th:nth-child(9) select
{
display:none;
}
.promoted-nominee-by-regional tfoot tr th select
{ width:100%;
}
.btn-aln-center
{
    text-align: center;
}
.btn-aln-center .btn
{
    margin: 0 auto;
    display: inline-block;
    margin-top: 4px;
}
	</style>
	
	<script>
	$(document).ready(function() {
	 $('body').change(function(){
         $('.promoted-nominee-by-regional #filter-count1 .dataTables_info').remove();
         $(".promoted-nominee-by-regional .dataTables_info").clone().appendTo(".promoted-nominee-by-regional #filter-count1");
              $('.total_record').show();
         });
	 });
	</script>
</body>
</html>
