<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<footer>
        <div class="footer-top">
            <div class="container">

                <div class="row">


                    <div class="col-md-12 footer-col2">

                        <ul>
                            <li><a href="/">Home</a></li>
                            <li><a href="/neas/about/overview">Overview</a></li>
                            <li><a href="/neas/about/award">Award Categories</a></li>
                            <li><a href="/neas/about/eligibility">Eligibility</a></li>
                            <li><a href="/neas/about/reward">Rewards</a></li>
                            <li><a href="/neas/guidelines">General Rules</a></li>
                            <li><a href="/neas/past/2017/jury">Jury 2017</a></li>
                            <li><a href="/neas/past/2017/winner/award">Award Track 2017</a></li>
							<li><a href="/neas/past/2017/winner/recognition">Recognition Track 2017</a></li>
							<li><a href="/neas/past/2016/jury">Jury 2016</a></li>
							<li><a href="/neas/past/2016/winner/award">Award Track 2016</a></li>
							<li><a href="/neas/past/2016/winner/recognition">Recognition Track 2016</a></li>
							<li><a href="/neas/scheme">FAQ's</a></li>
                            <li><a href="/neas/contact">Contact Us</a></li>
                        </ul>
                    </div>
                    
                    </div>
                    <div class="row">
                    <div class="col-md-3">
                        <ul class="fa-bottom">
                            <li><a href="https://www.facebook.com/Youth4Enterprise/" target="_blank" rel="noopener noreferrer"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                            <li><a href="https://twitter.com/Youth4Entrprise" target="_blank" rel="noopener noreferrer"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                       </ul>
                    </div>
                    <div class="col-md-6 help-center text-center">
                        <h4>Helpline Number</h4>
                            <ul>
                            <li><i class="fa fa-user" aria-hidden="true"></i> Ms. Richa</li>
                                <li><i class="fa fa-envelope" aria-hidden="true"></i> help.nea@ettech.com</li>
                                <li><i class="fa fa-phone" aria-hidden="true"></i> +91-9717211060</li>
                            </ul>
                      
                        </div>
                    <div class="col-md-3 web-last pull-right text-right">
                    	<a href="javascript:;">Webpage Last Updated on : 17-10-2018</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="copy-rights">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 align-center">
                        <p>&copy; 2018 MINISTRY OF SKILL DEVELOPMENT AND ENTREPRENEURSHIP. All rights reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    
    <!-- <div class="modal fade onload-popup" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="onload">

        <div class="modal-dialog">

            
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body text-center">
                  <img src="/static/images/images/neas-about.jpg" alt="Banner"/>
                </div>
                
            </div>

        </div>
    </div>-->
<%@include file="footerlibs.jsp" %>


<script type="text/javascript">
$(document).ready(function () {
    $.ajax({
        url: "/neas/api/username",
        type : 'POST',
        data: 'email=${name}',
        success: function (response) {
           $('#userFullName').html(response);
        }
    });
});
</script>