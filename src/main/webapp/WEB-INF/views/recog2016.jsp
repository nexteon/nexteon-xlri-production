<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<%@include file="fragments/headerlibs.jsp" %>
<body class="innerpage">

<%@include file="fragments/header.jsp" %>
<!-- Body Starts Here -->

<main role="main">
	<div class="page-title">
		<h2>NEA RECOGNITION AWARD 2016</h2>
	</div>
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="whitebg">
						<div class="row">
                           <div class="column">
                              <div class="card">
                                 <img src="/static/images/neaswinner/recog/2016/winner1.jpg" alt="Shri.K.N.Janardhan">
                                 <div>
                                    <h2>Shri.K.N.Janardhan</h2>
                                    <p class="title">Award Category: Entrepreneruship Education</p>
                                    <p>National Centre for Excellence of RSETIs</p>
                                 </div>
                              </div>
                           </div>
                           <div class="column">
                              <div class="card">
                                 <img src="/static/images/neaswinner/recog/2016/winner2.jpg" alt="Shri.R.M.P Jawahar">
                                 <div>
                                    <h2>Shri.R.M.P Jawahar</h2>
                                    <p class="title">Award Category: Incubation</p>
                                    <p>Tiruchirappalli Regional Engineering College-Science Technology Entrepreneurs Park (TREC-STEP)</p>
                                 </div>
                              </div>
                           </div>
                           <div class="column">
                              <div class="card">
                                 <img src="/static/images/neaswinner/recog/2016/winner3.jpg" alt="Shri.Pradeep Gupta">
                                 <div>
                                    <h2>Shri.Pradeep Gupta</h2>
                                    <p class="title">Award Category: Mentor-Private</p>
                                 </div>
                              </div>
                           </div>
                           <div class="column">
                              <div class="card">
                                 <img src="/static/images/neaswinner/recog/2016/winner4.jpg" alt="Shri.Harkesh Mittal">
                                 <div>
                                    <h2>Shri.Harkesh Mittal</h2>
                                    <p class="title">Award Category: Mentor- Government</p>
                                 </div>
                              </div>
                           </div>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>


<!-- Body Ends Here -->
<%@include file="fragments/footer.jsp" %>
</body>
</html>