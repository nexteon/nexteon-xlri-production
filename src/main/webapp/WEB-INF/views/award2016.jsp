<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
   <%@include file="fragments/headerlibs.jsp" %>
   <body class="innerpage">
      <%@include file="fragments/header.jsp" %>
      <!-- Body Starts Here -->
      <main role="main">
         <div class="page-title">
            <h2>NEA AWARDS 2016</h2>
         </div>
         <div class="content awr-2016">
            <div class="container">
               <div class="row">
                  <div class="col-md-12">
                     <div class="whitebg">
                        <div class="winner-section">
                           <div class="award-hdg text-center">
                              <h3 class="blue-hdg">Agri, Food, & Forestry Products</h3>
                              <a href="#"><img src="/static/images/neaswinner/2016/logo/wow.jpg" alt="wow"/></a>
                              <h4>Wow Momo Foods Private Limited</h4>
                           </div>
                           <div class="award-images text-center">
                              <ul>
                                 <li>
                                    <img src="/static/images/neaswinner/2016/wow1.jpg" alt="Mr.Sagar Jagdish Daryani"/>
                                    <p>Mr.Sagar Jagdish Daryani</p>
                                 </li>
                                 <li>
                                    <img src="/static/images/neaswinner/2016/wow2.jpg" alt="Mr.Binod Kimar Homagai"/>
                                    <p>Mr.Binod Kimar Homagai</p>
                                 </li>
                                 <li>
                                    <img src="/static/images/neaswinner/2016/wow3.jpg" alt="Mr.Shah Miftaur"/>
                                    <p>Mr.Shah Miftaur</p>
                                 </li>
                              </ul>
                           </div>
                        </div>
                        <div class="winner-section">
                           <div class="award-hdg text-center">
                              <h3 class="blue-hdg">Chemicals, Pharma, Bio and other processed material</h3>
                              <a href="http://www.saraldesigns.in/contact.php"><img src="/static/images/neaswinner/2016/logo/saral.jpg" alt="Saral Designs"/></a>
                              <h4>Saral Design Solutions Private Limited</h4>
                           </div>
                           <div class="award-images text-center">
                              <ul>
                                 <li>
                                    <img src="/static/images/neaswinner/2016/saral1.jpg" alt="Ms.Suhani Mohan"/>
                                    <p>Ms.Suhani Mohan</p>
                                 </li>
                                 <li>
                                    <img src="/static/images/neaswinner/2016/saral2.jpg" alt="Mr.Kartik Mehta"/>
                                    <p>Mr.Kartik Mehta</p>
                                 </li>
                              </ul>
                           </div>
                        </div>
                        <div class="winner-section">
                           <div class="award-hdg text-center">
                              <h3 class="blue-hdg">E-commerce, Logistics, Transport & other Services</h3>
                              <a href="https://jetsetgo.in/"><img src="/static/images/neaswinner/2016/logo/jetset.jpg" alt="JetSetGo Aviation Services Pvt Ltd"/></a>
                              <h4>JetSetGo Aviation Services Pvt Ltd</h4>
                           </div>
                           <div class="award-images text-center">
                              <ul>
                                 <li>
                                    <img src="/static/images/neaswinner/2016/jetset1.jpg" alt="Mr.Kartik Mehta"/>
                                    <p>Ms.Kanika Tekriwal</p>
                                 </li>
                              </ul>
                           </div>
                        </div>
                        <div class="winner-section">
                           <div class="award-hdg text-center">
                              <h3 class="blue-hdg">Engineering Services</h3>
                              <a href=""><img src="/static/images/neaswinner/2016/logo/swadha.jpg" alt="Swadha Energies Private Ltd."/></a>
                              <h4>Swadha Energies Private Ltd.</h4>
                           </div>
                           <div class="award-images text-center">
                              <ul>
                                 <li>
                                    <img src="/static/images/neaswinner/2016/swadha1.jpg" alt="Mr.Ankit Poddar"/>
                                    <p>Mr.Ankit Poddar</p>
                                 </li>
                                 <li>
                                    <img src="/static/images/neaswinner/2016/swadha2.jpg" alt="Mr.Dinesh Natarajan"/>
                                    <p>Mr.Dinesh Natarajan</p>
                                 </li>
                                 <li>
                                    <img src="/static/images/neaswinner/2016/swadha3.jpg" alt="Mr. Sreejith"/>
                                    <p>Mr. Sreejith</p>
                                 </li>
                              </ul>
                           </div>
                        </div>
                        <div class="winner-section">
                           <div class="award-hdg text-center">
                              <h3 class="blue-hdg">IT&ITES, Financial</h3>
                              <a href="http://lucideus.com/"><img src="/static/images/neaswinner/2016/logo/lucideus.jpg" alt="Lucideus Tech Private Ltd."/></a>
                              <h4>Lucideus Tech Private Ltd.</h4>
                           </div>
                           <div class="award-images text-center">
                              <ul>
                                 <li>
                                    <img src="/static/images/neaswinner/2016/lucideus1.jpg" alt="Mr. Saket Modi"/>
                                    <p>Mr. Saket Modi</p>
                                 </li>
                                 <li>
                                    <img src="/static/images/neaswinner/2016/lucideus2.jpg" alt="Mr. Vidit Baxi"/>
                                    <p>Mr. Vidit Baxi</p>
                                 </li>
                                 <li>
                                    <img src="/static/images/neaswinner/2016/lucideus3.jpg" alt="Mr. Rahul Tyagi"/>
                                    <p>Mr. Rahul Tyagi</p>
                                 </li>
                              </ul>
                           </div>
                        </div>
                        <div class="winner-section">
                           <div class="award-hdg text-center">
                              <h3 class="blue-hdg">SC/ST</h3>
                              <a href="http://jeevanksh.com/"><img src="/static/images/neaswinner/2016/logo/jeev.jpg" alt="Jeev Anksh Eco Products Private Ltd."/></a>
                              <h4>Jeev Anksh Eco Products Private Ltd.</h4>
                           </div>
                           <div class="award-images text-center">
                              <ul>
                                 <li>
                                    <img src="/static/images/neaswinner/2016/jeev1.jpg" alt="Mr. Gunajit Brahma"/>
                                    <p>Mr. Gunajit Brahma</p>
                                 </li>
                                 <li>
                                    <img src="/static/images/neaswinner/2016/jeev2.jpg" alt="Mr. Rohit Bajaj"/>
                                    <p>Mr. Rohit Bajaj</p>
                                 </li>
                              </ul>
                           </div>
                        </div>
                        <div class="winner-section">
                           <div class="award-hdg text-center">
                              <h3 class="blue-hdg">Women</h3>
                              <a href="http://svtechengineering.com/"><img src="/static/images/neaswinner/2016/logo/svengg.jpg" alt="S V Engineering and Consultancy Services Ltd."/></a>
                              <h4>S V Engineering and Consultancy Services Ltd.</h4>
                           </div>
                           <div class="award-images text-center">
                              <ul>
                                 <li>
                                    <img src="/static/images/neaswinner/2016/svengg1.jpg" alt="Mrs. Ashwini Manoj Kadam"/>
                                    <p>Mrs. Ashwini Manoj Kadam</p>
                                 </li>
                                 <li>
                                    <img src="/static/images/neaswinner/2016/svengg2.jpg" alt="Mr. Manoj Ravsaheb Kadam"/>
                                    <p>Mr. Manoj Ravsaheb Kadam</p>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </main>
      <!-- Body Ends Here -->
      <%@include file="fragments/footer.jsp" %>
   </body>
</html>