package com.xlri.awards.dao.service.impl;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;

import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.xlri.awards.dao.service.api.HttpClientPostService;

@Service
public class HttpClientPostServiceImpl implements HttpClientPostService {

	private static Logger LOGGER = LoggerFactory.getLogger(HttpClientPostServiceImpl.class);

	@Override
	public JSONObject postToURL(String postUrl, JSONObject json) {

		String output = null;
		HttpURLConnection conn = null;
		String response = "";
		try {
			URL url = new URL(postUrl);
			LOGGER.info("Opening connection for URL");
			conn= (HttpURLConnection) url.openConnection(); 
			conn.setDoOutput(true);
			conn.setInstanceFollowRedirects(false);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setRequestProperty("charset", "utf-8");
			conn.setUseCaches(false);
			StringBuilder urlParameters = new StringBuilder("");
			Iterator<String> keys = json.keys();
			while (keys.hasNext()) {
				String key = keys.next();
				urlParameters.append(key + "=" + json.get(key).toString() + "&");
			}
			urlParameters.deleteCharAt(urlParameters.length() - 1);
			try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
				wr.write(urlParameters.toString().getBytes());
			}

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			while ((output = br.readLine()) != null) {
				response += output;
			}
			LOGGER.info("Result: " + response);
			return new JSONObject(response);
		} catch (MalformedURLException e) {
			LOGGER.error("Error in post: " + e);
		} catch (Exception e) {
			LOGGER.error("Error in post: " + e);
		} finally {
			conn.disconnect();
		}
		return null;
	}

}
