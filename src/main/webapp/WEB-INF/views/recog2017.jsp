<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<%@include file="fragments/headerlibs.jsp" %>
<body class="innerpage">

<%@include file="fragments/header.jsp" %>
<!-- Body Starts Here -->

<main role="main">
	<div class="page-title">
		<h2>NEA RECOGNITION AWARD 2017</h2>
	</div>
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="whitebg">
						<div class="row">
                           <div class="column">
                              <div class="card">
                                 <img src="/static/images/neaswinner/recog/2017/winner1.jpg" alt="C-Camp">
                                 <div>
                                    <h2>C-Camp</h2>
                                    <p class="title">Award Category: Government</p>
                                    <p>C-Camp</p>
                                 </div>
                              </div>
                           </div>
                           <div class="column">
                              <div class="card">
                                 <img src="/static/images/neaswinner/recog/2017/winner2.jpg" alt="IKP Knowledge Park, Hyderabad">
                                 <div>
                                    <h2>IKP Knowledge Park, Hyderabad</h2>
                                    <p class="title">Award Category: Private</p>
                                    <p>IKP Knowledge Park, Hyderabad</p>
                                 </div>
                              </div>
                           </div>
                           <div class="column">
                              <div class="card">
                                 <img src="/static/images/neaswinner/recog/2017/winner3.jpg" alt="Dr.G.Subba Ramaiah">
                                 <div>
                                    <h2>Dr.G.Subba Ramaiah</h2>
                                    <p class="title">Award Category: Mentor-Private</p>
                                 </div>
                              </div>
                           </div>
                           <div class="column">
                              <div class="card">
                                 <img src="/static/images/neaswinner/recog/2017/winner4.jpg" alt="Dr.Renu Swaroop">
                                 <div>
                                    <h2>Dr.Renu Swaroop</h2>
                                    <p class="title">Award Category: Mentor-Private</p>
                                 </div>
                              </div>
                           </div>
                           <div class="column">
                              <div class="card">
                                 <img src="/static/images/neaswinner/recog/2017/winner5.jpg" alt="Mr.Vishwanathan">
                                 <div>
                                    <h2>Mr.Vishwanathan</h2>
                                    <p class="title">Award Category: Mentor- Government</p>
                                 </div>
                              </div>
                           </div>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>


<!-- Body Ends Here -->
<%@include file="fragments/footer.jsp" %>
</body>
</html>