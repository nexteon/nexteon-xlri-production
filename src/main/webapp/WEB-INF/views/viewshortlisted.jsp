<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="com.xlri.awards.common.StateUtil" %>
<!DOCTYPE html>
<html lang="en">
	<link href="/resources/css/select2.min.css" rel="stylesheet" />
    <link href="/resources/css/select2-placeholder-transition.css" rel="stylesheet" />
    <%@include file="dashboard/headerlibs.jsp" %>
	
	<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white" id="page-top">
		<div class="page-wrapper">
			<%@include file="dashboard/header.jsp" %>
			<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<!-- BEGIN CONTENT BODY -->
				<div class="page-content">
					<!-- BEGIN PAGE HEADER-->
					<!-- BEGIN PAGE BAR -->
					<div class="page-bar">
						<ul class="page-breadcrumb">
							<li>
								<a href="/userdata">Home</a>
								<i class="fa fa-circle"></i>
							</li>
							<li>
								<span>View Shortlisted Candidate Data</span>
							</li>
						</ul>
					</div>
					<h1 class="page-title">View Shortlisted Candidate Data</h1>
					<div class="row">
						<div class="col-xs-12">
							<c:if test ="${isAdmin || isRegional}">
								<div class="row">
									<div class="col-md-12">
										<div class="portlet light bordered">
											<div class="portlet-title">
												<div class="caption font-dark">
													<i class="icon-settings font-dark"></i>
													<span class="caption-subject bold uppercase">Shortlisted Nominees</span>
												</div>
												<a href="#" id="refresh-page1" class="btn btn-info">Reset Filters</a>
											</div>
											<div class="portlet-body">
												<div class="table-responsive">
													<div class="total_record2">
														Total Count:
														<div id="filter-count2"></div>
													</div>
													<table class="table table-striped table-bordered table-hover table-checkable order-column dataTable" id="example1" class="display">
														<thead>
															<tr>
																<th>S. No.</th>
																<th>View</th>
																<th>Shortlist Status</th>
																<th>Score</th>
																<th>Updated On</th>
																<th>Registration Number</th>
																<th>Nominee Name</th>
																<th>Email Id</th>
																<th>Contact Number</th>
																<th>Award Track</th>
																<th>Award Category</th>
																<th>State</th>
																<th>Social Category</th>
																<th>File Uploaded</th>
															</tr>
														</thead>
														<tfoot>
															<tr id="filters2">
																<th>S. No.</th>
																<th></th>
																<th>Shortlist Status</th>
																<th>Score</th>
																<th>Updated On</th>
																<th>Registration Number</th>
																<th>Nominee Name</th>
																<th>Email Id</th>
																<th>Contact Number</th>
																<th>Award Track</th>
																<th>Award Category</th>
																<th>State</th>
																<th>Social Category</th>
																<th>File Uploaded</th>
															</tr>
														</tfoot>
														<tbody>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</c:if>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Upload Field Report</h4>
					</div>
					<div class="modal-body">
						<form action="" method="post" enctype="multipart/form-data" id="field-report-upload">
							<div class="row">
								<div class="col-sm-12">
									<div class="row">
										<div class="col-lg-12">
											<label>Nominee Email Id: </label>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group">
												<input required type="text" class="form-control" id="mailTo" name="mailTo" placeholder="Mail To" value="" readonly>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="row">
										<div class="col-lg-12">
											<label>Upload File: </label>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group">
												<input required type="file" id='subject' class="form-control" name="subject" placeholder="Subject">
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<button class="btn btn-primary btnsubmit-cnf" type="submit">Upload</button>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button data-dismiss="modal" type="button" class="btn btn-default">Close</button>
					</div>
				</div>
			</div>
		</div>

		<!-- END QUICK NAV -->
		<%@include file="dashboard/footer.jsp"%>
     	<%@include file="dashboard/datatablescript.jsp" %>

		<c:if test ="${isAdmin || isRegional}">
			<script type="text/javascript" >
			$(document).ready(function(){	
			$('#example1').DataTable( {
				    	  "ajax": {
				    	  "url": "/neas/api/upload/show/shortlisted",
				    	  "type" : 'POST',
				    	"scrollX": true,
				    	  "dataSrc": ""
				    	  },
				    	 "stateSave": true,
				    	  "columns": [
				    	  { "data": "id" },
				    	  { "data": "view" },
				    	  { "data": "shortliststatus" },
				    	  { "data": "score" },
				    	  { "data": "scoreupdated" },
				    	  { "data": "registrationnumber" },
				    	  { "data": "nomineename" },
				    	  { "data": "emailid" },
				    	  { "data": "contactnumber" },
				    	  { "data": "awardcategory" },
				    	  { "data": "enterprisecategory" },
					   	  { "data": "state" },
					   	  { "data": "socialcategory" },
					   	  { "data": "fileuploaded" },
				    	  ],dom: 'Bfrtip',
				        buttons: [
				                  'copyHtml5',
				                  'excelHtml5',
				                  'csvHtml5',
				                  'pdfHtml5'
				              ], initComplete: function () {
				    	  this.api().columns().every(function () {
				    	  var column = this;
				
				    	 
				    	  var select = $('<select><option value=""></option></select>')
				    	  .appendTo($("#filters2").find("th").eq(column.index()))
				    	  .on('change', function () {
				    	  var val = $.fn.dataTable.util.escapeRegex(
				    	  $(this).val()); 
				
				    	  column.search(val ? '^' + val + '$' : '', true, false)
				    	  .draw();
				    	  }); 
				    	  column.data().unique().sort().each(function (d, j) {
				    	  select.append('<option value="' + d + '">' + d + '</option>')
				    	  });
				    	  });
				    	  }
				    	  } );
			
					});
			</script>
		</c:if>
	</body>
</html>