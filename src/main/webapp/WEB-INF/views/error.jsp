<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" isErrorPage="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<%@include file="fragments/headerlibs.jsp" %>
<body class="innerpage">

<%@include file="fragments/header.jsp" %>
<!-- Body Starts Here -->

<main role="main">
	<div class="page-title">
		<h2>Error Page</h2>
	</div>
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="whitebg">
						<div id="form1" class="cst-form">
						<div class="text-center not-found"><h2>Oops!</h2>
							    <h3>Something went wrong.</h3>
							    <p>
								Maybe you are looking for a wrong page or you don't have permission to access the page.
						        </p>
						        </div>
                         </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<!-- Body Ends Here -->
<%@include file="fragments/footer.jsp" %>
	<script>
		$(document).ready(function() {
			$('[data-toggle="tooltip"]').tooltip();
			if($('#errorModal').length > 0){
				$('#errorModal').modal('show');
			}
		});
	</script>
</body>
</html>