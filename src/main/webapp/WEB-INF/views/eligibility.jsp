<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
   <%@include file="fragments/headerlibs.jsp" %>
   <body class="innerpage">
      <%@include file="fragments/header.jsp" %>
      <!-- Body Starts Here -->
      <main role="main">
         <div class="page-title">
            <h2>ELIGIBILITY</h2>
         </div>
         <div class="content">
            <div class="container">
               <div class="row">
                  <div class="col-md-12">
                     <div class="whitebg eligibilitypg">
                        <div class="clearfix"></div>
                        <div class="col-md-12 pull-left">
			<h4 class="blue-hdg">Eligibility for 'Enterprise Award' Category</h4>
                           <p>Considering the uniqueness of all Award categories, separate eligibility criteria for each category are given below.</p>
                           <div class="table-responsive"><table class="table table-bordered">
                              <tbody>
                                 <tr>
                                    <td colspan="2">
                                       <p><strong>Award Category</strong></p>
                                    </td>
                                    <td>
                                       <p><strong>Common Criteria</strong></p>
                                    </td>
                                    <td>
                                       <p><strong>Award Category Specific Criteria</strong></p>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p><strong>A1 </strong></p>
                                    </td>
                                    <td>
                                       <p><strong>Initial investment up to </strong><strong>₹</strong><strong>1 L</strong></p>
                                    </td>
                                    <td rowspan="4">
                                        <p><strong>1.</strong> Entrepreneur&rsquo;s age should be below the age of 40 years as on April 1<sup>st</sup> 2018.</p>
                                        <p><strong>2.</strong> The candidate should be a First-Generation Entrepreneur/s. The definition of a first-generation entrepreneur for the scheme is &ldquo;An entrepreneur who is not in the same line of business as his/her parent. They should have started the enterprise on their own initiative, idea or innovation&rdquo;.</p>
                                        <!--<p><strong>3.</strong> The enterprise must be a young enterprise started within last 6 years, i.e., on or after April 1<sup>st</sup> 2012</p>-->
                                        <p><strong><a href="/neas/about/eligibility/second">Click here for Details of the Key Eligibility Indicators for the Common Criteria</strong></a></p>
                                    </td>
                                    <td>
                                        <p><strong>1.</strong> The initial investment of the enterprise should not exceed ₹ 1 Lakh</p>
                                        <p><strong>2.</strong> The enterprise may be a formal or an informal entity</p>
                                        <p><strong>3.</strong> The entrepreneur must be a founder/co-founder of an enterprise/sole proprietorship firm/ partnership/ own account enterprise</p>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p><strong>A2 </strong></p>
                                    </td>
                                    <td>
                                       <p><strong>Initial investment above </strong><strong>₹</strong><strong>1 L to </strong><strong>₹</strong><strong> 10 L</strong></p>
                                    </td>
                                    <td>
                                        <p><strong>1.</strong> The initial investment of the enterprise should be greater than ₹ 1 Lakh but should not exceed ₹ 10 Lakh</p>
                                        <p><strong>2.</strong> The Enterprise may be a formal or an informal entity, however, the Enterprise must provide evidence of its. Some of the examples of such evidence could be:</p>
                                        <p><strong>a.</strong> In the case of an informal entity: the entity can be a sole proprietorship firm/partnership/ own account enterprise but having one of the following: Bank Account, Registration with a Government/Non-Governmental Organizations</p>
                                        <p><strong>b.</strong> In the case of a formal entity: the entity can be registered under Companies or Cooperative Act, or a Partnership/Producer Company/ Limited Liability Partnership firm or an entity having a formal Memorandum of Association and/or Articles of Association</p>
                                        <p><strong>3.</strong> It is desirable but not mandated that the enterprise have some necessary industry certifications (example: CE, FSSAI, MSME, etc.) and/or a dedicated current or savings bank account, or registered with the Governmental and Non-Governmental organisation.</p>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p><strong>A3 </strong></p>
                                    </td>
                                    <td>
                                       <p><strong>Initial investment above </strong><strong>₹</strong><strong> 10 L to </strong><strong>₹</strong> <strong>1 Cr</strong></p>
                                    </td>
                                    <td>
                                        <p><strong>1.</strong> The initial investment of the enterprise should be greater than ₹ 10 Lakh but should not exceed ₹ 1 Cr. In addition, the enterprise should fulfil the following two criteria:</p>
                                        <p><strong>a.</strong> The enterprise must mandatorily have any one of statutory documents (Audited Balance Sheet or P&amp;L Account, Incorporation Certificate, etc.) or trade-specific registrations (example: CE, FSSAI, MSME, GST Registration, etc.)</p>
                                        <p><strong>b.</strong> The enterprise must not have defaulted to any lenders in the past 4 years (FY 14-15, 15-16, 16-17, 17-18) nor by its group entities or its promoters. No defaults and delays in paying the statutory dues.</p>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p><strong>For respective Award Category (A1/ A2/ A3) given above</strong></p>
                                    </td>
                                    <td>
                                       <p><strong>Special Category Awards</strong></p>
                                    </td>
                                    <td>
                                       <p>Such nominations will be evaluated separately (if they are already not shortlisted under respective Award categories (A1, A2 &amp; A3)).</p>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                           </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 pull-left">
                           <h4>Eligibility for 'Eco-System Builder Award' Category</h4>
                           <p>Eligibility criteria of Eco-System Builder Award Category for <strong>(a) Entrepreneurship Development Institutes/ Organisations; (b) Incubation Centres; (c) Mentors; (d) Promoters Rural Producer Group Enterprise</strong> should have completed a minimum six years of service in their respective field as on April 1st, 2018. They should be well established and recognised in their field and Entrepreneurship Promotion and Development. The trainees, incubates, mentees and enterprises (hereinafter referred to as candidates) supported by them should have made significant progress in their ventures due to the efforts of such Eco-System Builders. Such Eco-System Builders will have to provide references of such candidates.</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </main>
      <!-- Body Ends Here -->
      <%@include file="fragments/footer.jsp" %>
   </body>
</html>