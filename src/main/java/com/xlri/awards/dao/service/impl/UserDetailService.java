package com.xlri.awards.dao.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.xlri.awards.dao.entity.User;
import com.xlri.awards.dao.service.api.UserService;

@Service
public class UserDetailService implements UserDetailsService {

	@Autowired
	private UserService userService;
	
	@Autowired
    private LoginAttemptService loginAttemptService;
  
    @Autowired
    private HttpServletRequest request;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		String ip = getClientIP();
        if (loginAttemptService.isBlocked(ip)) {
            throw new RuntimeException("blocked");
        }
		
		User user = null;
		try {
			user = this.userService.getUserByUsername(username);
		} catch (Exception e) {
			e.printStackTrace();
		}
		UserConfig userDetail = null;
		if (user != null) {
			try {
				List<GrantedAuthority> authorities = getAuthorities(user.getRole());

				userDetail = new UserConfig(username, user.getPassword().toString(), user.getFullName(), authorities,
						true, true, true, true);
			} catch (Exception e) {
				System.out.println("Exception :" + e);
			}
		}

		return userDetail;

	}

	public List<GrantedAuthority> getAuthorities(String role) {
		List<GrantedAuthority> authList = getGrantedAuthorities(getRoles(role));
		return authList;
	}

	public List<String> getRoles(String role) {

		List<String> roles = new ArrayList<String>();
		if (role.equals("NOMINATOR")) {
			roles.add("ROLE_NOMINATOR");
		} else if (role.equals("ADMIN")) {
			roles.add("ROLE_ADMIN");
		} else if (role.equals("NOMINEE")) {
			roles.add("ROLE_NOMINEE");
		} else if (role.equals("REGIONAL")) {
			roles.add("ROLE_REGIONAL");
		} else if (role.equals("SUPPORT")) {
			roles.add("ROLE_SUPPORT");
		} else if (role.equals("EVALUATOR")) {
			roles.add("ROLE_EVALUATOR");
		} else if (role.equals("EXPERT")) {
			roles.add("ROLE_EXPERT");
		}
		return roles;
	}

	public static List<GrantedAuthority> getGrantedAuthorities(List<String> roles) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

		for (String role : roles) {
			authorities.add(new SimpleGrantedAuthority(role));
		}
		return authorities;
	}
	
	private String getClientIP() {
		RequestAttributes attribs = RequestContextHolder.getRequestAttributes();
		if (RequestContextHolder.getRequestAttributes() != null) {
		    HttpServletRequest request = ((ServletRequestAttributes) attribs).getRequest();
		    return request.getRemoteAddr();
		}
	    return null;
	}

}