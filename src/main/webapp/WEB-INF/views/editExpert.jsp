<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">
   <%@include file="dashboard/headerlibs.jsp" %>
   <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
   			<div class="page-wrapper">
          <%@include file="dashboard/header.jsp" %>
           


                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="/userdata">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>UpdateExpert</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> Update Expert</h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <!-- BEGIN DASHBOARD STATS 1-->



<!-- Form Container -->
   <div class="form-outer">
                <div class="form">
                    <form  action="/editexpertn" method="post">
                    	<input type = "hidden" name="id" value = "${expert.id}">
                        <div class="row">
                        <div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-12">Expert Name
									<span class="required"> * </span>
								</label>
								<div class="col-md-12">
									<input name="expertName" id="expertName" type="text" class="form-control" placeholder="Expert Name" required="" value="${expert.expertName}"/> 
									</div>
							</div>
						</div>
                   <%--      <div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-12">Email
									<span class="required"> * </span>
								</label>
								<div class="col-md-12">
									<input name="emailId" id="emailId" type="email" class="form-control" placeholder="Email Id"  required="" value="${expert.emailId}"/>
							</div>
						</div>
						</div>
					 --%>	
					 	<input name="emailId" id="emailId" type="hidden" class="form-control" placeholder="Email Id"  required="" value="${expert.emailId}"/>
					 	
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-12">Contact No
									<span class="required"> * </span>
								</label>
								<div class="col-md-12">
									<input name="contactNumber" id="contactNumber" type="tel" class="form-control" placeholder="Contact No" maxlength="10" pattern="[0-9]{10}" required="" value="${expert.contactNumber}"/> 
									</div>
							</div>
						</div>
						
                        <div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-12">Designation
									<span class="required"> * </span>
								</label>
								<div class="col-md-12">
									<input name="designation" id="designation" type="text" class="form-control" placeholder="Designation"  required="" value="${expert.designation}"/> 
									</div>
							</div>
						</div>
						
						<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-md-12">Specialization
								<span class="required"> * </span>
							</label>
							<div class="col-md-12">
								<select class="form-control" name="specialization" id="specialization" >
									<option value="">${expert.specialization}</option>
									<option value="Textiles, leather and related goods (Goods)">Textiles, leather and related goods (Goods)</option><option value="Agri, food and forestry products (Goods)">Agri, food and forestry products (Goods)</option><option value="Chemicals, pharma, bio and other processed materials (Goods)">Chemicals, pharma, bio and other processed materials (Goods)</option><option value="Engineering systems (Goods)">Engineering systems (Goods)</option><option value="Renewable energy and waste  (Goods)">Renewable energy and waste  (Goods)</option><option value="Handicrafts, traditional manufacturing &amp; other goods (Goods)">Handicrafts, traditional manufacturing &amp; other goods (Goods)</option><option value="IT &amp;ITES, Financial (Services)">IT &amp;ITES, Financial (Services)</option><option value="Education (Services)">Education (Services)</option><option value="Healthcare (Services)">Healthcare (Services)</option><option value="Hospitality (Services)">Hospitality (Services)</option><option value="E-Commerce, logistics, transport &amp; other services (Services)">E-Commerce, logistics, transport &amp; other services (Services)</option><option value="Social Enterprise">Social Enterprise</option><option value="Barefoot">Barefoot</option>
								</select>
							</div>
						</div>
						</div>
						
                        
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-12">Expert Address
									<span class="required"> * </span>
								</label>
								<div class="col-md-12">
									<input name="address" id="address" type="text" class="form-control" placeholder="Expert Address" value="${expert.address}"/> 
									</div>
							</div>
						</div>
						
						<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-md-12">Select Patrners
								<span class="required"> * </span>
							</label>
							<div class="col-md-12">
								<select class="form-control" name="partnerWith" id="partnerWith">
									<option value="">${expert.partnerWith}</option>
									<option value="Indian Institute of Technology Delhi">Indian Institute of Technology Delhi</option><option value="Indian Institute of Technology Kanpur">Indian Institute of Technology Kanpur</option><option value="Indian Institute of Technology Bombay">Indian Institute of Technology Bombay</option><option value="">Indian Institute of Technology Madras</option><option value="Indian Institute of Management Ahmedabad">Indian Institute of Management Ahmedabad</option><option value="XLRI Jamshedpur">XLRI Jamshedpur</option><option value="Tata Institute of Social Sciences (TISS), Mumbai">Tata Institute of Social Sciences (TISS), Mumbai</option>
								</select>
							</div>
						</div>
						 </div>             
                        
						<!-- <div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-12">Password
									<span class="required"> * </span>
								</label>
								<div class="col-md-12">
									<input name="password" id="password" type="password" class="form-control" placeholder="Password" /> 
									</div>
							</div>
						</div>	 -->
                        
                        <div class="col-md-12">
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<%-- <button type="submit" name="${_csrf.parameterName}" class="btn green" value="${_csrf.token}">Submit</button>
									 --%>
									 <input type="hidden" name="currentpath" value=""/>
									 <input type="hidden" name="${_csrf.parameterName}" class="btn green" value="${_csrf.token}"/>
									<input class="btn green" type="submit" name="Submit" value="Submit">
								</div>
							</div>
						</div>
						</div>
						
                        </div>
                    </form>
                   </div>
           </div>
         
         </div>
         </div>
         </div>
         
        <!-- END QUICK NAV -->
     <%@include file="dashboard/footer.jsp"%>
     </body>
         </html>
                    
