package com.xlri.awards.config.security;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.xlri.awards.dao.entity.LogTable;
import com.xlri.awards.dao.service.api.LogTableService;
import com.xlri.awards.dao.service.impl.LoginAttemptService;

@Component
public class AuthenticationSuccessEventListener extends SimpleUrlAuthenticationSuccessHandler {

	@Autowired
	private LoginAttemptService loginAttemptService;
	
	@Autowired
	private LogTableService logTableService;
	
	
	

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		setDefaultTargetUrl("/userpage");
		super.onAuthenticationSuccess(request, response, authentication);
		loginAttemptService.loginSucceeded(request.getRemoteAddr());
		LogTable logTable = new LogTable();
		logTable.setIpAddress(request.getRemoteAddr());
		logTable.setLoginType("Login Success");
		logTable.setTimestamp(new Date());
		logTable.setUsername(request.getParameter("emailId"));
		logTableService.saveLog(logTable);
		
	}



}