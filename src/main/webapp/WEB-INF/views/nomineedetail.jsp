<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="com.xlri.awards.common.StateUtil" %>
<!DOCTYPE html>
<html>
    <%@include file="fragments/headerlibs.jsp" %>
    <link rel="stylesheet" href="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css">
    <body class="innerpage">
        <%@include file="fragments/header.jsp" %>
        <!-- Body Starts Here -->
        <main role="main">
            <div class="page-title">
                <h2>NEAS 2018 Nomination Form</h2>
            </div>
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="whitebg">
						<div class="row corner-box">
							<div class="col-md-12">
								<div class="form-group">
									<label for="registrationNumber">Registration Number:
									<span class="heading-span">${registrationNumber}</span></label>
								</div>
							</div>
						</div>
						<div class="cst-form">
							<div class="circle-two">
								<ul>
									<li class="active-cir"><a href="#"><span>Part A</span></a>
									</li>
									<li><a href="#"><span>Part B</span></a></li>

								</ul>

							</div>

							<div class="form-header">
                                        <h4>PART A: Nominee's Basic Details</h4>
                                    </div>
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="awardCategory">Award Track<span class="mandate">*</span></label>
                                                    <select class="form-control" name="awardCategory" required data-required-error="Please fill in this field.">
                                                    	<option value="" selected disabled>Select</option>
                                                        <option value="enterprise">Enterprise Awards Track</option>
                                                        <option value="ecosystem">Ecosystem Builders Track</option>
                                                    </select>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="awardForm">
                                            <form action="/basicdetail" method="post" data-toggle="validator" role="form" id="awardbaseform">
                                                <input type="hidden" class="form-control" name="registrationNumber" value="${registrationNumber}" readonly="readonly">
                                                <input type = "hidden" value="enterprise" name = "awardCategory"/>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="enterpriseCategory">Award Category<span class="mandate">*</span></label>
                                                            <select class="form-control" name="enterpriseCategory" required data-required-error="Please fill in this field.">
                                                                <option value="" selected disabled>Select</option>
                                                                <option value="onelac">Investment upto ₹1 lakh</option>
                                                                <option value="uptoten">Investment between ₹1 lakh upto ₹10 lakh</option>
                                                                <option value="uptocrore">Investment between ₹10 lakh upto ₹1 crore</option>
                                                            </select>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            
                                                            <div id="onelaccat">
                                                                <label for="awardsub0">Award Sub Category<span class="mandate">*</span></label>
                                                                <select class="form-control" name="awardsub1" id="awardsub1">
                                                                	<option value="" selected disabled>Select</option>
                                                                    <option value="Textile, textile articles, leather and related goods">1. Textile, textile articles, leather and related goods</option>
                                                                    <option value="Agriculture, food processing and forestry – also includes vegetable products, vegetable fats, prepared foodstuffs and agri-tech">2. Agriculture, food processing and forestry – also includes vegetable products, vegetable fats, prepared foodstuffs and agri-tech</option>
                                                                    <option value="Chemicals, pharma, bio and others">3. Chemicals, pharma, bio and others</option>
                                                                    <option value="Retail Trade">4. Retail Trade</option>
                                                                    <option value="Renewable energy and waste management">5. Renewable energy and waste management</option>
                                                                    <option value="Handicrafts">6. Handicrafts</option>
                                                                    <option value="Healthcare">7. Healthcare</option>
                                                                    <option value="Hospitality, Tourism & Travel">8. Hospitality, Tourism & Travel</option>
                                                                    <option value="Logistics, transports, e-commerce & other services">9. Logistics, transports, e-commerce & other services</option>
                                                                </select>
                                                            </div>
                                                            <div id="uptotencat">
                                                                <label for="awardsub1">Award Sub Category<span class="mandate">*</span></label>
                                                                <select class="form-control" name="awardsub2" id="awardsub2">
                                                                	<option value="" selected disabled>Select</option>
                                                                    <option value="Textile, textile articles, leather and related goods">1. Textile, textile articles, leather and related goods</option>
                                                                    <option value="Agriculture, food processing and forestry – also includes vegetable products, vegetable fats, prepared foodstuffs and agri-tech">2. Agriculture, food processing and forestry – also includes vegetable products, vegetable fats, prepared foodstuffs and agri-tech</option>
                                                                    <option value="Chemicals, pharma, bio and others">3. Chemicals, pharma, bio and others</option>
                                                                    <option value="Engineering systems, base metals & articles, machinery & mechanical appliances and medical devices">4. Engineering systems, base metals & articles, machinery & mechanical appliances and medical devices</option>
                                                                    <option value="Renewable energy and waste management">5. Renewable energy and waste management</option>
                                                                    <option value="Handicrafts">6. Handicrafts</option>
                                                                    <option value="Healthcare">7. Healthcare</option>
                                                                    <option value="Hospitality, Tourism & Travel">8. Hospitality, Tourism & Travel</option>
                                                                    <option value="Logistics, transports, e-commerce & other services">9. Logistics, transports, e-commerce & other services</option>

                                                                </select>
                                                            </div>
                                                            <div id="uptocrorecat">
                                                                <label for="awardsub2">Award Sub Category<span class="mandate">*</span></label>
                                                                <select class="form-control" name="awardsub3" id="awardsub3">
                                                                	<option value="" selected disabled>Select</option>
                                                                    <option value="Textile, textile articles, leather and related goods">1. Textile, textile articles, leather and related goods</option>
                                                                    <option value="Agriculture, food processing and forestry – also includes vegetable products, vegetable fats, prepared foodstuffs and agri-tech">2. Agriculture, food processing and forestry – also includes vegetable products, vegetable fats, prepared foodstuffs and agri-tech</option>
                                                                    <option value="Chemicals, pharma, bio and others">3. Chemicals, pharma, bio and others</option>
                                                                    <option value="Engineering systems, base metals & articles, machinery & mechanical appliances and medical devices">4. Engineering systems, base metals & articles, machinery & mechanical appliances and medical devices</option>
                                                                    <option value="Renewable energy and waste management">5. Renewable energy and waste management</option>
                                                                    <option value="Handicrafts">6. Handicrafts</option>
                                                                    <option value="Healthcare">7. Healthcare</option>
                                                                    <option value="Hospitality, Tourism & Travel">8. Hospitality, Tourism & Travel</option>
                                                                    <option value="Logistics, transports, e-commerce & other services">9. Logistics, transports, e-commerce & other services</option>

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="specifyproduct">Please list about the product/service you offer<span class="mandate">*</span></label>
                                                            <textarea id="specifyproduct" name="specifyproduct" class="form-control" rows=5  maxlength="100" required data-required-error="Please fill in this field." onblur="check(this);"></textarea>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="nomineeName">Nominee/Entrepreneur’s Name<span class="mandate">*</span></label>
                                                            <input type="text" id="nomineeName" name="nomineeName" class="form-control" required data-required-error="Please fill in this field."/>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="dob">Date of Birth of the Nominee/Entrepreneur<span class="mandate">*</span></label>
                                                          <div class="row">  <div class="col-md-3">
                                                             <select name="date" class="form-control" id="yearid" required>
                                                              <option value="" selected disabled>YYYY</option>
                                                              <c:forEach begin="1960" end="2018" step="1" var="d">
                                                               <option value="${d}">${d}</option>
                                                              </c:forEach>
                                                             </select>
                                                            </div>
                                                            
                                                            <div class="col-md-3">
                                                             <select name="month" class="form-control" id="monthid" required>
                                                              <option value="" selected disabled>MM</option>
                                                              <option value="01">Jan</option>
                                                              <option value="02">Feb</option>
                                                              <option value="03">Mar</option>
                                                              <option value="04">Apr</option>
                                                              <option value="05">May</option>
                                                              <option value="06">Jun</option>
                                                              <option value="07">Jul</option>
                                                              <option value="08">Aug</option>
                                                              <option value="09">Sep</option>
                                                              <option value="10">Oct</option>
                                                              <option value="11">Nov</option>
                                                              <option value="12">Dec</option>
                                                             </select>
                                                            </div>
                                                            
                                                            <div class="col-md-2">
                                                             <select name="day" class="form-control" id="dayid" required>
                                                              <option value="" selected disabled>DD</option>
                                                              <option value="01">01</option>
                                                              <option value="02">02</option>
                                                              <option value="03">03</option>
                                                              <option value="04">04</option>
                                                              <option value="05">05</option>
                                                              <option value="06">06</option>
                                                              <option value="07">07</option>
                                                              <option value="08">08</option>
                                                              <option value="09">09</option>
                                                              <c:forEach begin="10" end="28" step="1" var="d">
                                                               <option value="${d}">${d}</option>
                                                              </c:forEach>
                                                              <option id="d29" style="display: none;" value="29">29</option>
											                 <option id="d30" style="display: none;" value="30">30</option>
											                 <option id="d31" style="display: none;" value="31">31</option>
                                                             </select>
                                                            </div>
                                                            <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>
                                                    </div>

													<input type="hidden" class="form-control" name="dob" id="dateeid">
                                                    
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="gender">Gender<span class="mandate">*</span></label>
                                                            <select class="form-control" name="gender"  required data-required-error="Please fill in this field.">
                                                                <option value="" selected disabled>Select</option>
                                                                <option value="m">Male(M)</option>
                                                                <option value="f">Female(F)</option>
                                                                <option value="t">Third Gender(T)</option>
                                                            </select>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="socialCategory">Social Category<span class="mandate">*</span></label> 
                                                            <select
                                                            class="form-control" name="socialCategory"  required data-required-error="Please fill in this field.">
                                                            	<option value="" selected disabled>Select</option>
                                                                <option value="gen">General</option>
                                                                <option value="sc">SC</option>
                                                                <option value="st">ST</option>
                                                            </select>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="pwd">Person with Disability (PwD)<span class="mandate">*</span></label>
                                                            <select class="form-control" name="pwd" id="pwd"  required data-required-error="Please fill in this field.">
                                                           		<option value="" selected disabled>Select</option>
                                                                <option value="no">No</option>
                                                                <option value="yes">Yes</option>
                                                            </select>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="speccat">Do you wish to nominate the candidate in the <span data-toggle="tooltip" title="“Please
															refer program guidelines for more information about the ‘Special Category’)">‘Special Category’</span> awards?<span class="mandate">*</span></label>
                                                            <select class="form-control" name="speccatopt" id="speccat">
                                                            	<option value="" selected disabled>Select</option>
                                                                <option value="no" selected>No</option>
                                                                <option value="yes">Yes</option>
                                                            </select>
                                                            <span class="chk-error mandate"> Please check at least one check-box </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" id="speccatid">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>If yes, please select the appropriate category from the list of following <span  data-toggle="tooltip" title="Please note that you can appear
															in maximum two categories but the award will be presented only in one category in which the candidate
															has scored the best.">'Special Category'</span> awards.</label>
                                                            <input type="checkbox" name="speccatval1" value="Women Category" class="special-category"/> Women Category<br/>
                                                            <input type="checkbox" name="speccatval2" value="SC/ST Category" class="special-category"/> Entrepreneur from SC/ST Category<br/>
                                                            <input type="checkbox" name="speccatval3" value="Difficult Category" class="special-category"/> Entrepreneur from <a href="javascript:void(0);" data-toggle="tooltip" title="Please provide the definition">'Difficult Area'</a><br/>
                                                            <input type="checkbox" name="speccatval4" value="PwD Category" class="special-category"/> Entrepreneur from PwD Category<br/>
                                                        </div>
                                                        <div><i style="color: #0074e1;font-size:14px;">(Please note that you can appear in maximum two 'Special Category' but the award will be presented only in one Special Category in which the candidate has scored the best.)</i></div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="enterprisename">Name of the Enterprise<span class="mandate">*</span></label>
                                                            <input type="text" id="enterprisename" name="enterpriseName" class="form-control" required data-required-error="Please fill in this field."/>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="address1">House No.</label>
                                                            <input type="text" class="form-control" name="address1">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="street">Street Name</label>
                                                            <input type="text" class="form-control" name="street">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="address2">Mohalla/Village</label>
                                                            <input type="text" class="form-control" name="address2">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="city">City/Town</label>
                                                            <input type="text" class="form-control" name="city">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="state">State<span class="mandate">*</span></label>
                                                            <select class="form-control" name="state" required data-required-error="Please fill in this field.">
                                                                <option value="" selected disabled>Select</option>
                                                                <c:set var="stateValues" value="<%=StateUtil.values()%>"/>
                                                                <c:forEach items="${stateValues}" var="state">
                                                                    <option value="${state}">${state.name}</option>
                                                                </c:forEach>
                                                            </select>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="landmark">Landmark</label>
                                                            <input type="text" class="form-control" name="landmark">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="pinCode">Pin Code<span class="mandate">*</span></label>
                                                            <input type="text" class="form-control onlyDigit" name="pinCode"  id="pinCode" class="form-control" placeholder="Pin No" pattern="[0-9]{6}" required data-required-error="Please fill in this field." data-pattern-error="Only six numeric digit allowed.">
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="web2">Website(If available)</label>
                                                            <input type="text" class="form-control" id="web2" name="website">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="emailId">Email ID<span class="mandate">*</span></label>
                                                            <input type="email" class="form-control" name="emailId" required data-required-error="Please fill in this field." data-remote="/neas/api/checkemail/nominee" data-remote-error="Email Id already registered"  pattern="^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$" data-pattern-error="Please provide a valid email id.">
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="contact">Contact Number<span class="mandate">*</span></label>
                                                            <input type="text" class="form-control" name="contactNumber"  required maxlength="10" pattern="[0-9]{10,10}" data-required-error="Please fill in this field." data-remote="/neas/api/checkcontact/nominee" data-remote-error="Contact Number already registered">
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="knowNominee">How do you know your Nominee?<span class="mandate">*</span></label>
                                                            <select class="form-control" name="knowNominee" id="knowNominee" aria-required="true" required data-required-error="Please fill in this field.">
                                                            	<option value="" selected disabled>Select</option>
                                                                <option value="Friend of mine">Friend of mine</option>
                                                                <option value="Family member">Family member</option>
                                                                <option value="Professional Network">Professional Network</option>
                                                                <option value="Mentor">Mentor</option>
                                                                <option value="Mentee or Student">Mentee or Student</option>
                                                                <option value="Investee">Investee</option>
                                                                <option value="other">Other (Please Specify)</option>
                                                            </select>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="other-fieldhide">
                                                                <label class="vis-hidden" for="other-field">Other</label>
                                                                <input type="text" name="otherNom" id="other-field" class="form-control" placeholder="Please Specify">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="currentpath" value=""/>
                                                    <input type="hidden" name="nxtval" id="nxtval" value="partAOnly">
                                                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                                </div>
                                                <div class="row align-center">
                                                    <div class="submit-btn">
                                                        <input class="btn btn-warning" id="awardsave" type="button" value="Save As Draft">
                                                        <input class="btn btn-primary" id="awardnext" type="button" name="Submit" value="Next">
                                                    </div>                                    
                                                </div>
                                                <!-- award end -->
                                            </form>
                                        </div>
                                        <div id="ecosystemForm">
                                            <form action="/basicdetail" method="post" data-toggle="validator" role="form" id="ecosystembaseform">
                                                <input type="hidden" class="form-control" name="registrationNumber" value="${registrationNumber}" readonly="readonly">
                                                <input type = "hidden" value="ecosystem" name = "awardCategory"/>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="enterprisename">Nominee's Institution's Name<span class="mandate">*</span></label>
                                                            <input type="text" id="enterprisename" name="enterpriseName" class="form-control" required data-required-error="Please fill in this field."/>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                </div> 
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="nomineeName">Nominee's Name<span class="mandate">*</span></label>
                                                            <input type="text" id="nomineeName" name="nomineeName" class="form-control"  required data-required-error="Please fill in this field."/>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="gender">Gender<span class="mandate">*</span></label>
                                                            <select class="form-control" name="gender" required data-required-error="Please fill in this field.">
                                                            	<option value="" selected disabled>Select</option>
                                                                <option value="m">Male(M)</option>
                                                                <option value="f">Female(F)</option>
                                                                <option value="t">Third Gender(T)</option>
                                                            </select>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="address1">House No.</label>
                                                            <input type="text" class="form-control" name="address1">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="street">Street Name</label>
                                                            <input type="text" class="form-control" name="street">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="address2">Mohalla/Village:</label>
                                                            <input type="text" class="form-control" name="address2">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="city">District/City</label>
                                                            <input type="text" class="form-control" name="city">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="state">State<span class="mandate">*</span></label>
                                                            <select class="form-control" name="state" required data-required-error="Please fill in this field.">
                                                                <option value="" selected disabled>Select</option>
                                                                <c:set var="stateValues" value="<%=StateUtil.values()%>"/>
                                                                <c:forEach items="${stateValues}" var="state">
                                                                    <option value="${state}">${state.name}</option>
                                                                </c:forEach>
                                                            </select>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="landmark">Landmark:</label>
                                                            <input type="text" class="form-control" name="landmark">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="pinCode">Pin Code<span class="mandate">*</span></label>
                                                            <input type="text" class="form-control onlyDigit" name="pinCode" placeholder="Pin No" pattern="[0-9]{6}" required data-required-error="Please fill in this field." data-pattern-error="Only six numeric digit allowed.">
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="web2">Website(If available)</label>
                                                            <input type="text" class="form-control" id="web2" name="website">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="emailId">Email ID<span class="mandate">*</span></label>
                                                            <input type="email" class="form-control" name="emailId" required data-required-error="Please fill in this field." data-remote="/neas/api/checkemail/nominee" data-remote-error="Email Id already registered" pattern="^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$" data-pattern-error="Please provide a valid email id.">
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="contact">Contact Number<span class="mandate">*</span></label>
                                                            <input type="text" class="form-control" name="contactNumber"  required pattern="[0-9]{10,10}" data-required-error="Please fill in this field." data-remote="/neas/api/checkcontact/nominee" data-remote-error="Contact Number already registered">
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h4>Please respond to the following questions (maximum of 150 words).</h4>
                                                        <h5>Note: 10 character in any one of the below question is mandatory.</h5>
                                                        <div class="help-block with-errors ecoques"></div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="ques1">1. Please describe the role the Nominee is playing as an Eco-System Contributor.</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <textarea maxlength="1000" id="ques1" class="form-control" name="roleNominee" onblur="check(this);"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="ques2">2. Please describe the impact the Nominee has created by playing their role as Mentor/Incubation Center/Entrepreneurship Development Institutes.</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <textarea maxlength="1000" id="ques2" class="form-control" name="impactNominee" onblur="check(this);"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="ques3">3. Please mention about the recognitions/awards the Nominee has received in the area of Entrepreneurship development.</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <textarea maxlength="1000" id="ques3" class="form-control" name="recognitionNominee" onblur="check(this);"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="ques4">4. Please mention about industry collaborations, Road shows/Bussiness Plan Competitions/Funding Events organised.</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <textarea maxlength="1000" id="ques4" class="form-control" name="collaborationNominee" onblur="check(this);"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="ques5">5. Any other special mentions?</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <textarea maxlength="1000" id="ques5" class="form-control" name="specMentionNominee" onblur="check(this);"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <input type="hidden" name="currentpath" value=""/>
                                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                                <div class="row align-center">
                                                    <div class="submit-btn">
                                                        <input class="btn btn-warning" id="ecosystemsave" type="button" value="Save As Draft">
                                                        <input class="btn btn-primary" id="ecosystemnext" type="button" name="Submit" value="Next">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <c:if test="${(not empty errorMsg) or (not empty successMsg)}">
            <div class="modal fade" id="errorModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <c:if test="${ not empty errorMsg }">
                                <h4 class="modal-title">Error</h4>
                            </c:if>
                            <c:if test="${ not empty successMsg }">
                                <h4 class="modal-title">Success</h4>
                            </c:if>
                        </div>
                        <div class="modal-body">
                            <p>
                                <c:if test="${ not empty errorMsg }">
                                    ${ errorMsg }
                                </c:if>
                                <c:if test="${ not empty successMsg }">
                                    ${ successMsg }
                                </c:if>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </c:if>
        
        
        
        
        
        <!-- Body Ends Here -->
        <%@include file="fragments/footer.jsp" %>
       
        <script>
            $(document).ready(function(){
            	$('.chk-error').hide();
            	
                $('#awardsave').click(function(){
                	
                	if(localStorage.getItem("awardbaseformData")){
                		userData = JSON.parse(localStorage.getItem("awardbaseformData"))
                	}else{
                		var userData ={}
                    	userData["status"] ="Pending"
                	}
                	userData["partA"] = $('#awardbaseform').serialize()
                	localStorage.setItem("awardbaseformData",JSON.stringify(userData))
                   // localStorage.setItem("awardbaseform",$('#awardbaseform').serialize());
                    $('#ecosystembaseformdata').trigger("reset");
                    localStorage.removeItem('ecosystembaseformdata');
                    alert("Form Saved..");
                });
                $('#ecosystemsave').click(function(){
                	if(localStorage.getItem("ecosystembaseformdata")){
                		userData = JSON.parse(localStorage.getItem("ecosystembaseformdata"))
                	}else{
                		var userData ={}
                    	userData["status"] ="Pending"
                	}
                	userData["partA"] = $('#ecosystembaseform').serialize()
                	localStorage.setItem("ecosystembaseformdata",JSON.stringify(userData))
                	
                	$('#awardbaseformData').trigger("reset");
                	localStorage.removeItem('awardbaseformData');
                    alert("Form Saved..");
                });
                $('#ques1,#ques2,#ques3,#ques4,#ques5').blur(function(){
                	if($('#ques1').val()==null || $('#ques2').val()==null || $('#ques3').val()==null || $('#ques4').val()==null || $('#ques5').val()==null){
                		$('.ecoques').html("Please provide answer to atleast 1 question.");
                	}
                });
            });
            
            
            $(document).ready(function() {
            	
            	$('#ecosystemnext').bind('click',function(){
            		 var validator = $("#ecosystembaseform").data("bs.validator");
                     validator.validate();

                     if (!validator.hasErrors()) 
                     {
                    	 var value1 = $('#ques1').val();
            	         var value2 = $('#ques2').val();
            	         var value3 = $('#ques3').val();
            	         var value4 = $('#ques4').val();
            	         var value5 = $('#ques5').val();
            	         
            	        var value1count = value1.trim().length;
            	        var value2count = value2.trim().length;
            	        var value3count = value3.trim().length;
            	        var value4count = value4.trim().length;
            	        var value5count = value5.trim().length;
            	        
                    	 if(value1count<10 && value2count<10 && value3count<10 && value4count<10 && value5count<10){
                    		 $('.ecoques').html("Please enter atleast 10 characters in any one of the questions.");
                    		 $('#ques1').focus();
                    	 }else{
                    		 $('.ecoques').html("");
                    	 if(localStorage.getItem("ecosystembaseformdata")){
                     		userData = JSON.parse(localStorage.getItem("ecosystembaseformdata"))
                 		}else{
                 			var userData ={}
                 			userData["status"] ="Pending"
                 		}
                 		
                     	userData["partA"] = $('#ecosystembaseform').serialize()
                     	localStorage.setItem("ecosystembaseformdata",JSON.stringify(userData))
                     	window.location.href = "/openecosystemcat";
               			userData["formtype"] = 'ecosystem'
                    	}
                     } else 
                     {
                         console.log(222)
                     }
            	})
            	
            	$('.special-category').on('click change', function(e) {
            		if($('.special-category:checked').length >=2){
            			$('.special-category').not(':checked').attr('disabled',true)
            		}else{
            			$('.special-category').not(':checked').removeAttr('disabled')
            		}
            	})
            	
            	$('#awardnext').bind('click',function(){
            	     var checkFlag = false;
            	     if($('#speccat').val() == "yes"){
            	    	if( $('.special-category:checked').length != 0 ){
            	    		 checkFlag = true
            	    	}else{
            	    		$('.chk-error').show();
            	    		$('html,body').animate({
            	    			scrollTop: $(".chk-error").offset().top-200},'slow');     
            	    			
            	    	}
            	     }else{
            	    	 checkFlag = true
            	    	 $('.chk-error').hide();
            	     }
            		
            		
            		 var validator = $("#awardbaseform").data("bs.validator");
                     validator.validate();

                     if (!validator.hasErrors() && checkFlag) 
                     {
                    	 if(localStorage.getItem("awardbaseformData")){
                     		userData = JSON.parse(localStorage.getItem("awardbaseformData"))
                 		}else{
                 			var userData ={}
                 			userData["status"] ="Pending"
                 		}
                 		
                     	userData["partA"] = $('#awardbaseform').serialize()
                     	
                     	localStorage.setItem("awardbaseformData",JSON.stringify(userData))
                     	
                     	
                 		if($('select[name="enterpriseCategory"]').val()=="onelac"){
                 			window.location.href = "/openfirstcat";
                 			userData["formtype"] = 'first'
                 		}else if($('select[name="enterpriseCategory"]').val()=="uptoten"){
                 			window.location.href = "/openseccat"
                 				userData["formtype"] = 'second'
                 		}else if($('select[name="enterpriseCategory"]').val()=="uptocrore"){
                 			window.location.href = "/openthirdcat"
                 				userData["formtype"] = 'third'
                 		}else{
                 			alert("please select form");
                 			
                 		} 
                     } else 
                     {
                         console.log(222)
                     }
            		
            		/* */
            	})
            	
            	
                $('[data-toggle="tooltip"]').tooltip();
                if($('#errorModal').length > 0){
                    $('#errorModal').modal('show');
                }
                $.ajax({
                    url: '/neas/api/checkecosystemcount',
                    type: 'POST',
                    success:function(data){
                        if(parseInt(data['eco']) <= 0){
                            $('select[name=awardCategory] option[value=ecosystem]').hide();
                        }
                        if(parseInt(data['ent']) <= 0){
                            $('select[name=awardCategory] option[value=enterprise]').hide();
                        }
                    }
                });
                
                
                $('#onelaccat').hide();
                $('#uptotencat').hide();
                $('#uptocrorecat').hide();
                $('select[name=enterpriseCategory]').change(function(){
                    if($(this).val()==='onelac'){
                        $('#onelaccat').show();
                        $('#uptotencat').hide();
                        $('#uptocrorecat').hide();
                    }if($(this).val()==='uptoten'){
                        $('#onelaccat').hide();
                        $('#uptotencat').show();
                        $('#uptocrorecat').hide();
                    }if($(this).val()==='uptocrore'){
                        $('#onelaccat').hide();
                        $('#uptotencat').hide();
                        $('#uptocrorecat').show();
                    }
                });
                $('#speccatid').hide();
                $('#awardForm').hide();
                $('#ecosystemForm').hide();
                $('select[name=awardCategory]').change(function(){
                    if($('select[name=awardCategory]').val() === 'enterprise'){
                        $('#awardForm').show();
                        $('#ecosystemForm').hide();
                    }
                    if($('select[name=awardCategory]').val() === 'ecosystem'){
                        $('#awardForm').hide();
                        $('#ecosystemForm').show();
                    }
                });
                $('#speccat').change(function(){
                    if($('#speccat').val() === 'no'){
                        $('#speccatid').hide();
                    }
                    if($('#speccat').val() === 'yes'){
                        $('#speccatid').show();
                    }
                });
                
                var limit = 2;
                $('.special-category').on('change', function(evt) {
                    if($(this).siblings(':checked').length >= limit) {
                        this.checked = false;
                    }
                });
                
                if(localStorage.getItem("awardbaseformData")!=null && localStorage.getItem("awardbaseformData")!=undefined){
                    var pairs, i, keyValuePair, key, value, map = {};
                    var query = JSON.parse(localStorage.getItem("awardbaseformData"))["partA"];
                    pairs = query.split('&');
                    for (i = 0; i < pairs.length; i += 1) {
                        keyValuePair = pairs[i].split('=');
                        key = decodeURIComponent(keyValuePair[0]);
                        value = (keyValuePair.length > 1) ? decodeURIComponent(keyValuePair[1]) : '';
                        value.replace('%20',' ');
                        if(key!=='_csrf'){
                        $('#awardbaseform [name="'+key+'"]').val(value);
                        if(key==='awardCategory' && value==="enterprise"){
                        	$('select[name="awardCategory"]').val("enterprise");
                            $('#awardForm').show();
                            $('#ecosystemForm').hide();
                        }
                        if(key==='speccatopt' && value==='yes'){
                            $('#speccatid').show();
                        }
                        if(key==='speccatval1'){
                        	$('input[name="speccatval1"][value="'+value+'"]').prop("checked",true);
                        }
                        if(key==='speccatval2'){
                        	$('input[name="speccatval2"][value="'+value+'"]').prop("checked",true);
                        }
                        if(key==='speccatval3'){
                        	$('input[name="speccatval3"][value="'+value+'"]').prop("checked",true);
                        }
                        if(key==='speccatval4'){
                        	$('input[name="speccatval4"][value="'+value+'"]').prop("checked",true);
                        }
                        if(key==='enterpriseCategory' && value==='onelac'){
                        	$('select[name="enterpriseCategory"]').val("onelac");
                            $('#onelaccat').show();
                            $('#uptotencat').hide();
                            $('#uptocrorecat').hide();
                        }
                        if(key==='enterpriseCategory' && value==='uptoten'){
                        	$('select[name="enterpriseCategory"]').val("uptoten");
                            $('#onelaccat').hide();
                            $('#uptotencat').show();
                            $('#uptocrorecat').hide();
                        }
                        if(key==='enterpriseCategory' && value==='uptocrore'){
                        	$('select[name="enterpriseCategory"]').val("uptocrore");
                            $('#onelaccat').hide();
                            $('#uptotencat').hide();
                            $('#uptocrorecat').show();
                        }
                        if(key==='awardsub'){
                        	$('select[name=awardsub]').parent().show();
                            $('select[name=awardsub]').val(value); 
                        }
                        if(key==='otherNom'){
                        	$('#other-field').parent().removeClass("other-fieldhide");
                        	$('#other-field').val(value);
                        }else{
                        	$('#other-field').parent().addClass("other-fieldhide");
                        }
                    }
                   }
                }else if(localStorage.getItem("ecosystembaseformdata")!=null || localStorage.getItem("ecosystembaseformdata")!=undefined){
                    var pairs, i, keyValuePair, key, value, map = {};
                    var query = JSON.parse(localStorage.getItem("ecosystembaseformdata"))["partA"];
                    pairs = query.split('&');
                    for (i = 0; i < pairs.length; i += 1) {
                        keyValuePair = pairs[i].split('=');
                        key = decodeURIComponent(keyValuePair[0]);
                        value = (keyValuePair.length > 1) ? decodeURIComponent(keyValuePair[1]) : '';
                        if(key!=='_csrf'){
                        $('#ecosystembaseform [name="'+key+'"]').val(value);
                        if(key==='awardCategory' && value ==="ecosystem"){
                        	$('select[name="awardCategory"]').val("ecosystem");
                            $('#awardForm').hide();
                            $('#ecosystemForm').show();
                        }
                    }
                    }
                }else{
                	
                }
                
            });
            $(document).ready(function() {
    			$("#awardbaseform,#ecosystembaseform").on("submit", function(e) {
    				if($("select[name=awardCategory]").val()==="enterprise"){
                        localStorage.setItem("awardbaseform",$('#awardbaseform').serialize());
                        $('#ecosystembaseform').trigger("reset");
                        alert("Form Saved..");
                    }
    				if($("select[name=awardCategory]").val()==="ecosystem"){
                    	localStorage.setItem("ecosystembaseform",$('#ecosystembaseform').serialize());
                    	$('#awardbaseform').trigger("reset");
                        alert("Form Saved..");
                    }
    				$(window).off("beforeunload");
    				return true;
    			});
    		});
            
            $("#ecosystemnext,#awardnext").on("click", function(e) {
 				$(window).off("beforeunload");
 				return true;
 			});
            
            $(window).on("beforeunload", function() {
    			return "You didn't save your form. All the data will be lost if you leave this page. Click Save as draft to save your data.";
    		});
            
            
            $(document).ready(function () {
    		  	$(".onlyDigit").keypress(function (e) {
    			 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
    					   return false;
    			}
    		   });
    		});
            
            $("#monthid, #yearid, #dayid").change(function(){
                $("#d29").hide(); $("#d30").hide(); $("#d31").hide();
                var m = document.getElementById("monthid");
                var y = document.getElementById("yearid");
                var d = document.getElementById("dayid");
                var iMonth = m.options[m.selectedIndex].value;
                var iYear = y.options[y.selectedIndex].value;
                var iday = d.options[d.selectedIndex].value;
                console.log("month-- "+ iMonth);
                console.log("year-- "+ iYear);
                console.log("result-- "+daysInMonth(iMonth -1, iYear));
                var dayVal = daysInMonth(iMonth -1, iYear);

                if(dayVal == 29){$("#d29").show();}
                if(dayVal == 30){$("#d29").show(); $("#d30").show();}
                if(dayVal == 31){$("#d29").show(); $("#d30").show(); $("#d31").show();}

                if(iMonth != "MM" && iYear != "yyyy" && iday != "dd"){
                 document.getElementById('dateeid').value = iday+"-"+iMonth+"-"+iYear;
                }

                });

                 function daysInMonth(iMonth, iYear)
                  {
                   return 32 - new Date(iYear, iMonth, 32).getDate();
                  }
                 
                 $(document).ready(function(){ 
                	    $('#submitForm').on("click",function(e){
                	        var validator = $("form").data("bs.validator");
                	         validator.validate(); 
                	         var value1 = $('.textarea1').val();
                	         var value2 = $('.textarea2').val();
                	         var value3 = $('.textarea3').val();
                	         var value4 = $('.textarea4').val();
                	        
                	        var regex = /\s+/gi;  
                	        
                	        
                	        var value1count = value1.trim().length;
                	        var value2count = value2.trim().length;
                	        var value3count = value3.trim().length;
                	        var value4count = value4.trim().length;         
                	        var totalcount = value1count + value2count + value3count + value4count;    
                	     console.log(totalcount);

                	    if(totalcount<10)
                	      {
                	          alert("min 10 words required");
                	          $('.textarea1').focusin();
                	      }
                	     
                	    });
                });
       	 </script>

    </body>
</html>