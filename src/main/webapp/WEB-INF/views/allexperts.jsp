<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">
<%@include file="dashboard/headerlibs.jsp"%>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
	<div class="page-wrapper">
		<%@include file="dashboard/header.jsp"%>



		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<!-- BEGIN CONTENT BODY -->
			<div class="page-content">
				<!-- BEGIN PAGE HEADER-->

				<!-- BEGIN PAGE BAR -->
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li><a href="/userdata">Home</a> <i class="fa fa-circle"></i>
						</li>
						<li><span>AllExperts</span></li>
					</ul>
					<div class="page-toolbar">
						<div id="dashboard-report-range"
							class="pull-right tooltips btn btn-sm" data-container="body"
							data-placement="bottom"
							data-original-title="Change dashboard date range">
							<i class="icon-calendar"></i>&nbsp; <span
								class="thin uppercase hidden-xs"></span>&nbsp; <i
								class="fa fa-angle-down"></i>
						</div>
					</div>
				</div>
				<!-- END PAGE BAR -->
				<!-- BEGIN PAGE TITLE-->
				<h1 class="page-title">All Experts</h1>
				<!-- END PAGE TITLE-->
				<!-- END PAGE HEADER-->
				<!-- BEGIN DASHBOARD STATS 1-->

				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet light portlet-fit bordered">
							<div class="portlet-title">
								
									</div>
								</div>
								<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover table-checkable order-column dataTable" id="example" class="display">
                                            <thead>
                         <tr>
                         					<th>Id</th>
                         					<th>Expert Name</th>
											<th>Email Id</th>
											<th>Contact Number</th>
											<th>Designation</th>
											<th>Partner Name</th>
											<th>Specialization</th>
										 	
                          
                       </tr>
                     </thead>
                                            
                                            <tbody>
                                            </tbody>
    
                                             </table>
                                             </div>
                                             
                                             
                                             
														</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>




			</div>
		

	<!-- END QUICK NAV -->
	<%@include file="dashboard/footer.jsp"%>
	<%@include file="dashboard/datatablescript.jsp" %>
    
	<script>
	$('#example').DataTable( {
    	  "ajax": {
    	  "url": "/allexperts",
    	  "type" : 'POST',
    	"scrollX": true,
    	  "dataSrc": ""
    	  },
    	  "columns": [
			{ "data": "id" },
    	  { "data": "expertName" },
    	  { "data": "emailId" },
    	  { "data": "contactNumber" },
    	  { "data": "designation" },
    	  { "data": "partnerWith" },
    	  { "data": "specialization" },
    	  ],dom: 'Bfrtip',
        buttons: [
                  'copyHtml5',
                  'excelHtml5',
                  'csvHtml5',
                  'pdfHtml5'
              ], 
               /* "columnDefs": [ {
            	    "targets": 7,
            	    "data": "id",
            	    "render": function ( data, type, row, meta ) {
            	      return '<a href="/editexpert?param='+data+'">Edit</a>';
            	    }
            	  } ], */ 
    	  } );
	</script>
	<%@include file="dashboard/disablewarning.jsp" %>
</body>
</html>
