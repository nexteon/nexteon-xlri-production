package com.xlri.awards.dao.service.impl;



import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlri.awards.dao.entity.NewsSection;
import com.xlri.awards.dao.service.api.NewsService;

@Repository
@Transactional
public class NewsServiceImpl implements NewsService{

	private static final Logger LOGGER = LoggerFactory.getLogger(NominatorServiceImpl.class);
	
	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public boolean saveNews(NewsSection newsSection){
		try{
			
		Session session = sessionFactory.openSession();
		session.getTransaction().begin();
		session.save(newsSection);
		session.getTransaction().commit();
		return true;
		}
		
		catch(Exception e){
			LOGGER.error(e.getMessage());
			return false;
			
		}
		
	}
	@Override
	public boolean updateNews(NewsSection newsSection){
		try{
			
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			session.update(newsSection);
			session.getTransaction().commit();
			return true;
		}
		catch(Exception e){
			LOGGER.error(e.getMessage());
			return false;
		}
	}
	
	
	@Override
	public List<NewsSection> getAllNews() {
		List<NewsSection> newsSectionList = null;
		try{
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			newsSectionList = session.createQuery("From NewsSection").getResultList();
			session.getTransaction().commit();
			return newsSectionList;
		}
		catch(Exception e){
			LOGGER.error(e.getMessage());
			return null;	
		}
	}
	
	@Override
	public List<NewsSection> getNewsDescFour(){
		try{
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<NewsSection> query = session.createQuery("SELECT n FROM NewsSection n order by n.createdOn desc");
			query.setMaxResults(4);
			session.getTransaction().commit();
			return query.getResultList();
		}
		catch(Exception e){
			LOGGER.error(e.getMessage());
			return null;	
		}
	}
	
	
}
