<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">
   <%@include file="dashboard/headerlibs.jsp" %>
   <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white" id="page-top" >
   			<div class="page-wrapper">
          <%@include file="dashboard/header.jsp" %>
           

				
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="/userdata">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Form View</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> Form View</h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <!-- BEGIN DASHBOARD STATS 1-->

        
		<!-- Form Container -->
		
		 <div class="row">
            <div class="col-md-12">
               <div class="portlet box blue form-display1 three-btn-pd">
                  <div class="portlet-title">
                     <div class="caption">
                        Nominator's Registration Number:  ${nominee.registrationNumber}
                     </div>
                  </div>
                  <div class="portlet-body">
                     <div class="col-xs-12 form-1-box">
                     <c:if test="${nominee.awardCategory == 'enterprise' }">
                     	<div class="row">
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>Award Track</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="Enterprise Award Track" readonly="">
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>Award Category</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                 	<c:if test="${nominee.enterpriseCategory == 'onelac'}">
                                 		<c:set var="entcat" value="Initial Investment upto Rs. 1 lakh"/>
                                 	</c:if>
                                 	<c:if test="${nominee.enterpriseCategory == 'uptoten'}">
                                 		<c:set var="entcat" value="Initial Investment between Rs. 1 lakh to Rs. 10 lakh"/>
                                 	</c:if>
                                 	<c:if test="${nominee.enterpriseCategory == 'uptocrore'}">
                                 		<c:set var="entcat" value="Initial Investment between Rs. 10 lakh to Rs. 1 Crore"/>
                                 	</c:if>
                                    <input type="text" class="form-control" value="${entcat}" readonly="">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>Award Sub Category</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.awardSub}" readonly="">
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>Please list about the product/service you offer</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.specifyproduct}" readonly="">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>Nominee's Name </label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.nomineeName}" readonly="">
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>Gender</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                 	<c:if test="${nominee.gender == 'm'}">
                                 		<c:set var="gender" value="Male"/>
                                 	</c:if>
                                 	<c:if test="${nominee.gender == 'f'}">
                                 		<c:set var="gender" value="Female"/>
                                 	</c:if>
                                 	<c:if test="${nominee.gender == 't'}">
                                 		<c:set var="gender" value="Third Gender"/>
                                 	</c:if>
                                    <input type="text" class="form-control" value="${gender}" readonly="">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>Date of Birth of the Nominee/Entrepreneur</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-4">
                                    <input type="text" class="form-control" value="${fn:substring(nominee.dob, 6, 10)}" readonly="">
                                 </div>
                                 <div class="col-lg-4">
                                    <input type="text" class="form-control" value="${fn:substring(nominee.dob, 3, 5)}" readonly="">
                                 </div>
                                 <div class="col-lg-4">
                                    <input type="text" class="form-control" value="${fn:substring(nominee.dob, 0, 2)}" readonly="">
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>Social Category</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                 	<c:if test="${nominee.socialCategory == 'gen'}">
                                 		<c:set var="socialCategory" value="General"/>
                                 	</c:if>
                                 	<c:if test="${nominee.socialCategory == 'sc'}">
                                 		<c:set var="socialCategory" value="SC"/>
                                 	</c:if>
                                 	<c:if test="${nominee.socialCategory == 'st'}">
                                 		<c:set var="socialCategory" value="ST"/>
                                 	</c:if>
                                    <input type="text" class="form-control" value="${socialCategory}" readonly="">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>Person with Disability (PwD)</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                 <c:choose>
                                 	<c:when test="${nominee.pwd}">
                                 		<c:set var="pwd" value="No"/>
                                 	</c:when>
                                 	<c:otherwise>
                                 		<c:set var="pwd" value="Yes"/>
                                 	</c:otherwise>
                                 	</c:choose>
                                    <input type="text" class="form-control" value="${pwd}" readonly="">
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>Do you wish to nominate the candidate in the 'Special Category' awards?</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                 <c:choose>
                                    <c:when test="${nominee.speccatopt}">
                                 		<c:set var="speccatopt" value="Yes"/>
                                 	</c:when>
                                 	<c:otherwise>
                                 		<c:set var="speccatopt" value="No"/>
                                 	</c:otherwise>
                                 </c:choose>
                                    <input type="text" class="form-control" value="${speccatopt}" readonly="">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>If yes, please select the appropriate category from the list of following 'Special Category' awards.</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.speccatval}" readonly="">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>Name of the Enterprise</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.enterpriseName}" readonly="">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>House No. </label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.address1}" readonly="">
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>Street Name</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.street}" readonly="">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>Mohalla/Village </label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.address2}" readonly="">
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>City/Town</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.city}" readonly="">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>State </label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.state}" readonly="">
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>Landmark</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.landmark}" readonly="">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>Pin Code</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.pinCode}" readonly="">
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-3">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>Website</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.website}" readonly="">
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-3">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>Email ID</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" name="emailId" id="emailId" class="form-control" value="${nominee.emailId}" readonly="">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>Contact Number</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.contact}" readonly="">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>How do you know your Nominee?</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.knowNominee}" readonly="">
                                 </div>
                              </div>
                           </div>
                        </div>
					</c:if>
					
		<!-- ECOSYSTEM -->			
					
					
					<c:if test="${nominee.awardCategory == 'ecosystem' }">
						<div class="row">
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>Award Track</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="Ecosystem Builder's Track" readonly="">
                                 </div>
                              </div>
                           </div>
                        </div>
                     	<div class="row">
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>Nominee's Institution's Name</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.enterpriseName }" readonly="">
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>Nominee's Name</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.nomineeName}" readonly="">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>Gender</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <c:if test="${nominee.gender == 'm'}">
                                 		<c:set var="gender" value="Male"/>
                                 	</c:if>
                                 	<c:if test="${nominee.gender == 'f'}">
                                 		<c:set var="gender" value="Female"/>
                                 	</c:if>
                                 	<c:if test="${nominee.gender == 't'}">
                                 		<c:set var="gender" value="Third Gender"/>
                                 	</c:if>
                                    <input type="text" class="form-control" value="${nominee.gender}" readonly="">
                                 </div>
                              </div>
                           </div>
                        </div>
                        
                        <div class="row">
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>House No. </label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.address1}" readonly="">
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>Street Name</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.street}" readonly="">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>Mohalla/Village </label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.address2}" readonly="">
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>City/Town</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.city}" readonly="">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>State </label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.state}" readonly="">
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>Landmark</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.landmark}" readonly="">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>Pin Code</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.pinCode}" readonly="">
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-3">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>Website</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.website}" readonly="">
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-3">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>Email ID</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" id="emailId" value="${nominee.emailId}" readonly="">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>Contact Number</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.contact}" readonly="">
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="row">
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>1. Please describe the role the Nominee is playing as an Eco-System Contributor.</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.roleNominee}" readonly="">
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>2. Please describe the impact the Nominee has created by playing their role as Mentor/Incubation Center/Entrepreneurship Development Institutes.</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.impactNominee}" readonly="">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>3. Please mention about the recognitions/awards the Nominee has received in the area of Entrepreneurship development.</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.recognitionNominee}" readonly="">
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>4. Please mention about industry collaborations, Road shows/Bussiness Plan Competitions/Funding Events organised.</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.collaborationNominee}" readonly="">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-6">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label>5. Any other special mentions?</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" value="${nominee.recognitionNominee}" readonly="">
                                 </div>
                              </div>
                           </div>
                        </div>
					</c:if>
					<div class="partbdata">
						
                     </div>
                     
                    <div class="filedata">
                    <c:if test="${nominee.awardCategory == 'enterprise' }">
                    <c:choose>
                    
                    <c:when test="${not empty secnom }">
                    	<a href="/createzip/secnominee/${secnomcontact}" class="btn btn-primary">Download Files</a>
                    </c:when>
                    <c:when test="${not empty thirdnom}">
                    	<a href="/createzip/thirdnominee/${thirdnomcontact}" class="btn btn-primary">Download Files</a>
                    </c:when>
                    <c:otherwise>
                    	
                    		<h4>File Not Uploaded yet</h4>
                    	
                    </c:otherwise>
                    </c:choose>
                    </c:if>
                    </div>
                     
                    <c:if test="${not empty secnom}"> 
	                    <div class="row">
	                    	<div class="col-lg-12">
		                    	<div class="col-sm-6">
	                              <div class="row">
	                                 <div class="col-lg-12">
	                                    <label>Type of Bussiness Formation</label>
	                                 </div>
	                              </div>
	                              <div class="row">
	                                 <div class="col-lg-12">
	                                    <input type="text" class="form-control" value="${secnom.businessFormation}" readonly="">
	                                 </div>
	                              </div>
	                           </div>
	                           <div class="col-sm-6">
	                              <div class="row">
	                                 <div class="col-lg-12">
	                                    <label>Founding Team Members (Mention their name and credentials)</label>
	                                 </div>
	                              </div>
	                              <div class="row">
	                                 <div class="col-lg-12">
	                                    <input type="text" class="form-control" value="${secnom.businessFormation}" readonly="">
	                                 </div>
	                              </div>
	                           </div>
	                    	</div>
	                    </div> 
	                    <div class="row">
	                    	<div class="col-lg-12">
		                    	<div class="col-sm-6">
	                              <div class="row">
	                                 <div class="col-lg-12">
	                                    <label>Type of Bussiness Formation</label>
	                                 </div>
	                              </div>
	                              <div class="row">
	                                 <div class="col-lg-12">
	                                    <input type="text" class="form-control" value="${secnom.businessFormation}" readonly="">
	                                 </div>
	                              </div>
	                           </div>
	                           <div class="col-sm-6">
	                              <div class="row">
	                                 <div class="col-lg-12">
	                                    <label>Founding Team Members (Mention their name and credentials)</label>
	                                 </div>
	                              </div>
	                              <div class="row">
	                                 <div class="col-lg-12">
	                                    <input type="text" class="form-control" value="${secnom.businessFormation}" readonly="">
	                                 </div>
	                              </div>
	                           </div>
	                    	</div>
	                    </div> 
                    </c:if>
                    
                    <c:if test="${not empty thirdnom}"> 
	                    <div class="row">
	                    	<div class="col-lg-12">
	                    		
	                    	</div>
	                    </div> 
                    </c:if>
                    
                     <div class="row">
                           <div class="col-sm-12">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <label> Status</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <input type="text" class="form-control" id="filledStatus" name="filledStatus" value="${nominee.filledStatus}" readonly="">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <c:if test="${nominee.filledStatus == 'PENDING'}">
                     <sec:authorize access="hasRole('SUPPORT')" var="isSupport">
                          
                     <div class="btn-group">
                     
					  <button data-toggle="modal" data-target="#myModal23" type="button" class="btn btn-primary red-bg" id="decline">Decline</button>
					  <button data-toggle="modal" data-target="#myModalPending" type="button" class="btn btn-primary blue-bg" id="accept">Pending</button>
					  <button data-toggle="modal" data-target="#myModal234" type="button" class="btn btn-primary green-bg" id="accept">Accept</button>
			
					</div>
					</sec:authorize>
					</c:if>
                  </div>
               </div>
               <button type="button" class="btn btn-warning" id="back-btn">BACK</button>
            </div>
         </div>
         
         <div id="myModal23" class="modal fade model-two" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body">
        <div class="portlet box blue form-display1">
             
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Registration Number:  ${nominee.registrationNumber}</div>
                                        
                                    </div>
                                    <div class="portlet-body">
                                     
											<div class="row">
												<div class="col-sm-12">

													<div class="row">
														<div class="col-lg-12">
															<label>Remark</label>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-12">
                                                            <textarea class="form-control" name="remark" id="remark" required></textarea>
														</div>
													</div>
                                                    
                                                    <div class="row">
														<div class="col-lg-12">
														<button  type="button" class="btn btn-primary" id="submit">Submit</button>
														</div>
													</div>
												</div>
											</div>
											</div>
                                    </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
         </div>
         
         <div id="myModal234" class="modal fade model-two" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body">
        <div class="portlet box blue form-display1">
             
                                    <div class="portlet-body">
                                     
											<div class="row">
												<div class="col-sm-12">

													<div class="row">
														<div class="col-lg-12">
															<label>Are you sure you are satisfied with the screening of ${nominee.nomineeName}( ${nominee.registrationNumber} ) and accept it for the next round of evaluation. You can not undo the action once accepted.</label>
														</div>
														<div class="col-md-12">
														<button id="final-acc" type="button" class="btn btn-primary" data-dismiss="modal">Yes, Proceed for Acceptance</button>
														<button type="button" class="btn btn-primary" data-dismiss="modal">No, Review the Nomination</button>
														</div>
													</div>
												</div>
											</div>
											</div>
                                    </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
         </div>
         
         <div id="myModal2345" class="modal fade model-two" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body">
        <div class="portlet box blue form-display1">
             
                                    <div class="portlet-body">
                                     
											<div class="row">
												<div class="col-sm-12">

													<div class="row">
														<div class="col-lg-12">
															<label>Are you sure you are satisfied with the screening of ${nominee.nomineeName}( ${nominee.registrationNumber} ) and reject it for the next round of evaluation. You can not undo the action once rejected.</label>
														</div>
														
														<div class="col-md-12">
														<button id="final-sub" type="button" class="btn btn-primary" data-dismiss="modal">Yes, Proceed for Rejection</button>
														<button type="button" class="btn btn-primary" data-dismiss="modal">No, Review the Nomination</button>
														</div>
													</div>
												</div>
											</div>
											</div>
                                    </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
         </div>
         
         <div id="myModalPending" class="modal fade model-two" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body">
        <div class="portlet box blue form-display1">
             
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Registration Number:  ${nominee.registrationNumber}</div>
                                        
                                    </div>
                                    <div class="portlet-body">
                                     
											<div class="row">
												<div class="col-sm-12">

													<div class="row">
														<div class="col-lg-12">
															<label>Remark</label>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-12">
                                                            <textarea class="form-control" name="remark" id="remark" required></textarea>
														</div>
													</div>
                                                    
                                                    <div class="row">
														<div class="col-lg-12">
														<button  type="button" class="btn btn-primary" id="pending">Submit</button>
														</div>
													</div>
												</div>
											</div>
											</div>
                                    </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
         </div>
         
         
         
         
         <div id="myModalPending-after" class="modal fade model-after" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body">
        <div class="portlet box blue form-display1">
             
                                    <div class="portlet-body">
                                     
											<div class="row">
												<div class="col-sm-12">

													<div class="row">
														<div class="col-lg-12">
															<label>Are you sure you are satisfied with the screening of ${nominee.nomineeName}( ${nominee.registrationNumber} ) and reject it for the next round of evaluation. You can not undo the action once rejected.</label>
														</div>
														
														<div class="col-md-12">
														<button id="pending-sub" type="button" class="btn btn-primary" data-dismiss="modal">Yes, Proceed for Pending</button>
														<button type="button" class="btn btn-primary" data-dismiss="modal">No, Review the Nomination</button>
														</div>
													</div>
												</div>
											</div>
											</div>
                                    </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
         </div>
         
        <!-- END QUICK NAV -->
     <%@include file="dashboard/footer.jsp"%>
   <style>
        .form-display1 .portlet-body{float:left;width:100%}
.portlet.box.blue{float:left;width:100%}
.col-xs-12.form-1-box{padding:15px;border:solid 1px #ccc;margin-bottom:30px}
.form-display1 .form-control{margin-bottom:15px}
.form-display1 label{font-weight:600;font-size:14px;padding-bottom:4px}
.formsecondpart{padding-top:15px!important}
.three-btn-pd .btn
{
    margin-right: 15px;
}
.three-btn-pd .btn-primary.green-bg {
    color: #fff;
    background-color: #3c763d;
    border-color: #3c763d;
}
.three-btn-pd .btn-primary.red-bg {
    color: #fff;
    background-color: #a94442;
    border-color: #a94442;
}
        
        </style>
     <script>
     $(document).ready(function(){
   		$('#back-btn').click(function(){
   			window.location.href="/userdata?nomineeback";
   		});
   	});
      	$(document).ready(function(){
      		
      		$('#final-acc').on('click',function(e) {
          		var email = document.getElementById("emailId");
                var emailId = email.value;
                var rmk = document.getElementById("remark");
                var remark = rmk.value;
                 	$.ajax({
                 		url:'/neas/api/support/aaction',
                 		type:'POST',
                 		data:'emailId='+emailId+'&remark='+remark,
                 		success:function(data){
                 			
                 		}
                 	});
                 	window.location = "/userdata";
            });
      		
      		
          	$('#submit').on('click',function(e) {
             	$('#myModal23').modal('hide');	
         		$('#myModal2345').modal('show');
        });
          	
		/*   pendind js  */
          	
          	$('#pending').on('click',function(e) {
             	$('#myModalPending').modal('hide');	
         		$('#myModalPending-after').modal('show');
        });
          	
          	$('#pending-sub').on('click',function(e) {
          		var email = document.getElementById("emailId");
                var emailId = email.value;
                var rmk = document.getElementById("remark");
                var remark = rmk.value;
                 	$.ajax({
                 		url:'/neas/api/support/paction',
                 		type:'POST',
                 		data:'emailId='+emailId+'&remark='+remark,
                 		success:function(data){
                 			
                 		}
                 	});
                 	window.location = "/userdata";
          	 });
          	
          	/*   pendind js end */
          	
          	$('#final-sub').on('click',function(e) {
          		var email = document.getElementById("emailId");
                var emailId = email.value;
          		var rmk = document.getElementById("remark");
                var remark = rmk.value;
             	$.ajax({
             		url:'/neas/api/support/daction',
             		type:'POST',
             		data:'emailId='+emailId+'&remark='+remark,
             		success:function(data){
             			
             		}
             	});
             	window.location = "/userdata";
        });
          	
          	var jsonData = {};
          	<c:set var='rates' value='${fn:replace(nominee.ratings,"\'","")}'/>
      		var partbdata = '${rates}';
      		if(partbdata.includes('"[')){
	      		var partbsplit = partbdata.split('"[');
	      		var partbas = partbsplit[1].split(']"');
	      		jsonData = JSON.parse(partbsplit[0]+'['+partbas[0]+']'+partbas[1]);
      		}else{
      			jsonData = JSON.parse(partbdata);
      		}
      		console.log(jsonData);
      		var html = "";
      		for (var key in jsonData) {
      		    if( jsonData.hasOwnProperty(key) ) {
      		    	if(key === 'Number of Enterpreneur References being provided'){
      		    		var tab = "<table class='table table-responsive table-bordered'><tr><th>Individual Name</th><th>Enterprise Name</th><th>Industry</th><th>Years in Operation</th><th>Mobile Number</th><th>Email</th><th>Website</th><th>Qunatum of funding received</th>";
      		    		for (var k in jsonData[key]) {
      		    			tab+="<tr>";
      		    			tab+="<td>"+jsonData[key][k]["Individual Name"]+"</td>";
      		    			tab+="<td>"+jsonData[key][k]["Enterprise Name"]+"</td>";
      		    			tab+="<td>"+jsonData[key][k]["Industry"]+"</td>";
      		    			tab+="<td>"+jsonData[key][k]["Years in Operation"]+"</td>";
      		    			tab+="<td>"+jsonData[key][k]["10 Digit Mobile Number"]+"</td>";
      		    			tab+="<td>"+jsonData[key][k]["Email"]+"</td>";
      		    			tab+="<td>"+jsonData[key][k]["Website"]+"</td>";
      		    			tab+="<td>"+jsonData[key][k]["Qunatum of funding received"]+"</td>";
     		    			tab+="</tr>";
      		    		}
      		    		html += tab;
      		    	}
      		    	else{
      		      html += '<div class="row">'+
                  			'<div class="col-sm-6">'+
                  				'<div class="row">'+
                     				'<div class="col-lg-12">'+
                        				'<label>'+ key +'</label>'+
                     				'</div>'+
                  				'</div>'+
                  				'<div class="row">'+
                     				'<div class="col-lg-12">'+
                        				'<input type="text" class="form-control" value="'+jsonData[key]+'" readonly="">'+
                        			'</div>'+
                        		'</div>'+
               				'</div>'+
            			'</div>';
      		    	}
      		    } 	
      		  }  
      		$('.partbdata').html(html);
      		
      		
      	});
      	
      	
      </script>
     </body>
         </html>