<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
        <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
            <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
                <!DOCTYPE html>
                <html lang="en">
                <link href="/resources/css/select2.min.css" rel="stylesheet" />
                <link href="/resources/css/select2-placeholder-transition.css" rel="stylesheet" />
                <%@include file="dashboard/headerlibs.jsp" %>



                    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white" id="page-top">
                        <div class="page-wrapper">
                            <%@include file="dashboard/header.jsp" %>



                                <!-- BEGIN CONTENT -->
                                <div class="page-content-wrapper">
                                    <!-- BEGIN CONTENT BODY -->
                                    <div class="page-content">
                                        <!-- BEGIN PAGE HEADER-->

                                        <!-- BEGIN PAGE BAR -->
                                        <div class="page-bar">
                                            <ul class="page-breadcrumb">
                                                <li>
                                                    <a href="/userdata">Home</a>
                                                    <i class="fa fa-circle"></i>
                                                </li>
                                                <li>
                                                    <span>Dashboard</span>
                                                </li>
                                            </ul>
                                            <div class="page-toolbar">
                                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                                    <i class="icon-calendar"></i>&nbsp;
                                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                                    <i class="fa fa-angle-down"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END PAGE BAR -->
                                        <!-- BEGIN PAGE TITLE-->
                                        <h1 class="page-title"> Regional Dashboard</h1>
                                        <!-- END PAGE TITLE-->
                                        <!-- END PAGE HEADER-->
                                        <!-- BEGIN DASHBOARD STATS 1-->
                                        <div class="row">
                                            <!--Start,  Following div is only used for Regional Dashbaord  -->
											<input  id="regionmail" name="regionmail"  type="hidden" value="${name}" readonly class="form-control" >
                                            <div class="row" ng-app="myApp" ng-controller="jsonCtrl">
                                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                    <div class="dashboard-stat dashboard-stat-v2 blue type-table1">
                                                        <div class="visual">
                                                            <i class="fa fa-comments"></i>
                                                        </div>
                                                        <div class="details">
                                                            <div class="number">
                                                                <span data-counter="counterup" data-value="1349" id="nominator-count"></span>
                                                            </div>
                                                            <div class="desc"> Nominator Count</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                    <div class="dashboard-stat dashboard-stat-v2 red type-table2">
                                                        <div class="visual">
                                                            <i class="fa fa-bar-chart-o"></i>
                                                        </div>
                                                        <div class="details">
                                                            <div class="number">
                                                                <span data-counter="counterup" data-value="12,5" id="nominee-count"></span></div>
                                                            <div class="desc"> Nominee Count</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					                                <div class="dashboard-stat dashboard-stat-v2 green type-table3">
					                                    <div class="visual">
                                                            <i class="fa fa-bar-chart-o"></i>
                                                        </div>
                                                        <div class="details">
                                                            <div class="number">
                                                                <span data-counter="counterup" data-value="1234" id="shortlisted-count"></span></div>
                                                            <div class="desc"> Shortlisted Count</div>
                                                        </div>
					                                </div>
                            					</div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <!-- END DASHBOARD STATS 1-->

                                        <input type="hidden" name="subCategory" value="" />

                                        <div class="row datatable">
                                            <div class="col-md-12">
                                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                                <div class="portlet light bordered">
                                                    <div class="portlet-title">
                                                        <div class="caption font-dark">
                                                            <i class="icon-settings font-dark"></i>
                                                            <span class="caption-subject bold uppercase"> Nominator Data Table</span>
                                                        </div>

                                                    </div>
                                                    <div class="portlet-body">
                                                        <div class="table-responsive">
                                                            <div class="total_record">Total Count:
                                                                <div id="filter-count1"></div>
                                                            </div>
                                                            <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable" id="example" class="display">
                                                                <thead>
                                                                    <tr>
                                                                        <th>ID</th>
                                                                        <th>View</th>
                                                                        <th>Send Email</th>
                                                                        <th>Registration Number</th>
                                                                        <th>Name</th>
                                                                        <th>Role</th>
                                                                        <th>Institute Name</th>
                                                                        <th>Email Id</th>
                                                                        <th>Contact Number</th>
                                                                        <th>Occupation</th>
                                                                        <th>Address</th>
                                                                        <th>City</th>
                                                                        <th>State</th>

                                                                    </tr>
                                                                </thead>

                                                                <tbody>

                                                                </tbody>

                                                                <tfoot>
                                                                    <tr id="filters">
                                                                        <th>ID</th>
                                                                        <th></th>
                                                                        <th></th>
                                                                        <th>Registration Number</th>
                                                                        <th>Name</th>
                                                                        <th>Role</th>
                                                                        <th>Institute Name</th>
                                                                        <th>Email Id</th>
                                                                        <th>Contact Number</th>
                                                                        <th>Occupation</th>
                                                                        <th>Address</th>
                                                                        <th>City</th>
                                                                        <th>State</th>
                                                                    </tr>
                                                                </tfoot>

                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END EXAMPLE TABLE PORTLET-->
                                            </div>
                                        </div>


                                        <!-- this is our second table -->
                                        <!--   <input type="hidden" name="subCategory" value=""/> -->

                                        <div class="row datatable2">
                                        <div class="col-md-12">
                                            <button class="enterprisebtn btn btn-primary">Assign for Ecosystem</button>
                                            <button class="ecosystembtn btn btn-primary">Assign for Enterprise</button>
                                            </div>
                                            
                                            <div class="col-md-12">
                                                <!--  BEGIN EXAMPLE TABLE PORTLET -->
                                                <div class="portlet light bordered">
                                                    <div class="portlet-title">
                                                        <div class="caption font-dark">
                                                            <i class="icon-settings font-dark"></i>
                                                            <span class="caption-subject bold uppercase"> Shortlisted Nominee Data</span>
                                                        </div>

                                                    </div>
                                                    <div class="portlet-body">
                                                        <div class="table-responsive">
                                                            <div class="total_record2">Total Count:
                                                                <div id="filter-count2"></div>
                                                            </div>
                                                            <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable" id="example1" class="display">
                                                                <thead>
                                                                    <tr>
                                                                        <th><input name="select_all" value="1" id="example-select-all" type="checkbox" />Select</th>
                                                                        <th>View</th>
                                                                        <th>Assign Expert</th>
                                                                        <th>Upload File</th>
                                                                        <th>Expert 1</th>
                                                                        <th>Expert 2</th>
                                                                        <th>Expert 3</th>
                                                                        <th>Avg. Score</th>
                                                                        <th>Registration Number</th>
                                                                        <th>Nominee Name</th>
                                                                        <th>Email Id</th>
                                                                        <th>Contact Number</th>
                                                                        <th>Award Category</th>
                                                                        <th>Promote/Rank</th>
                                                                        <th>Promote Status</th>
                                                                        <th>Rank</th>
                                                                        <th>Enterprise Category</th>
                                                                        <th>Award Sub Category</th>
                                                                        <th>State</th>

                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>

                                                                <tfoot>
                                                                    <tr id="filters2">
                                                                        <th></th>
                                                                        <th></th>
                                                                        <th></th>
                                                                        <th></th>
                                                                        <th>Expert 1</th>
                                                                        <th>Expert 2</th>
                                                                        <th>Expert 3</th>
                                                                        <th>Avg. Score</th>
                                                                        <th>Registration Number</th>
                                                                        <th>Nominee Name</th>
                                                                        <th>Email Id</th>
                                                                        <th>Contact Number</th>
                                                                        <th>Award Category</th>
                                                                        <th></th>
                                                                        <th>Promote Status</th>
                                                                        <th>Rank</th>
                                                                        <th>Enterprise Category</th>
                                                                        <th>Award Sub Category</th>
                                                                        <th>State</th>
                                                                    </tr>
                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <!--   END EXAMPLE TABLE PORTLET -->
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row datatable3">
                                            <div class="col-md-12">
                                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                                <div class="portlet light bordered">
                                                    <div class="portlet-title">
                                                        <div class="caption font-dark">
                                                            <i class="icon-settings font-dark"></i>
                                                            <span class="caption-subject bold uppercase"> Nominee Data Table</span>
                                                        </div>

                                                    </div>
                                                    <div class="portlet-body">
                                                        <div class="table-responsive">
                                                            <div class="total_record3">Total Count:
                                                                <div id="filter-count3"></div>
                                                            </div>
                                                            <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable" id="example3" class="display">
                                                                <thead>
                                                                    <tr>
                                                                        <th>ID</th>
                                                                        <th>View</th>
                                                                        <th>Registration Number</th>
                                                                        <th>Nominee Name</th>
                                                                        <th>Email Id</th>
                                                                        <th>Contact Number</th>
                                                                        <th>Award Category</th>
                                                                        <th>Enterprise Category</th>
                                                                        <th>Award Sub Category</th>
                                                                        <th>Gender</th>
                                                                        <th>Social Category</th>
                                                                        <th>DOB</th>
                                                                        <th>Special Category</th>
                                                                        <th>Enterprise Name</th>
                                                                        <th>Address1</th>
                                                                        <th>Address2</th>
                                                                        <th>City</th>
                                                                        <th>State</th>
                                                                    </tr>
                                                                </thead>

                                                                <tbody>

                                                                </tbody>

                                                                <tfoot>
                                                                    <tr id="filters3">
                                                                        <th></th>
                                                                        <th></th>
                                                                        <th>Registration Number</th>
                                                                        <th>Nominee Name</th>
                                                                        <th>Email Id</th>
                                                                        <th>Contact Number</th>
                                                                        <th>Award Category</th>
                                                                        <th>Enterprise Category</th>
                                                                        <th>Award Sub Category</th>
                                                                        <th>Gender</th>
                                                                        <th>Social Category</th>
                                                                        <th>DOB</th>
                                                                        <th>Special Category</th>
                                                                        <th>Enterprise Name</th>
                                                                        <th>Address1</th>
                                                                        <th>Address2</th>
                                                                        <th>City</th>
                                                                        <th>State</th>
                                                                    </tr>
                                                                </tfoot>

                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END EXAMPLE TABLE PORTLET-->
                                            </div>
                                        </div>

                                    </div>

                                    <!-- Modal -->
                                    <div id="myModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Mail</h4>
                                                </div>
                                                <div class="modal-body">

                                                    <form action="/neas/api/support/mail" method="post">
                                                        <div class="row">

                                                            <div class="col-sm-12">
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <label>To: </label>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="form-group">
                                                                            <input required type="text" class="form-control" id="mailTo" name="mailTo" placeholder="Mail To" value="" readonly>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-12">
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <label>Subject: </label>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="form-group">
                                                                            <input required type="text" id='subject' class="form-control" name="subject" placeholder="Subject">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-12">
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <label>Message: </label>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="form-group">
                                                                            <textarea class="form-control" name="msg" id="msg" required></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <button class="btn btn-primary btnsubmit-cnf" type="button">Send</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <button data-dismiss="modal" type="button" class="btn btn-default">Close</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>



                                    <!-- Modal assign expert to nominee -->
                                    <div class="modal fade" id="assign-expert" role="dialog">
                                        <div class="modal-dialog modal-lg">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <form action="/neas/api/assign/expert" method="post" novalidate="true">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">
          	<input type="text" id="nomemail" name="nomemail" readonly>
          </h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="input-wrapper">
                                                            <div class="col-md-4 select-opt-mult">
                                                                <select id="select1" name="expert1" class="form-control select2-placeholder-transition">
                                                                    <option value="" disabled="" selected="" style="display:none;">Select One...</option>

                                                                </select>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <select id="select2" name="expert2" class=" form-control select2-placeholder-transition2">
                                                                    <option value="" disabled="" selected="" style="display:none;">Select One...</option>

                                                                </select>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <select id="select3" name="expert3" class=" form-control select2-placeholder-transition3">
                                                                    <option value="" disabled="" selected="" style="display:none;">Select One...</option>

                                                                </select>
                                                            </div>

                                                        </div>
                                                        <br>
                                                        <br>
                                                        <br>


                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <button class="btn btn-primary btnassgsubmit" type="button" name="assign-expert">Assign</button>

                                                                <button type="button" class="btn btn-primary reset-form" name="assign-expert">Reset</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                        </div>
                                    </div>
                                    
                                    
                                    
                                    
                                    <!-- Modal assign expert to nominee ecosystem -->
                                    <div class="modal fade" id="assign-expert-ecosystem" role="dialog">
                                        <div class="modal-dialog modal-lg">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <form action="/neas/api/assign/expert" method="post" novalidate="true">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">
          	<input type="text" id="nomemail2" name="nomemail" readonly>
          </h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="input-wrapper">
                                                            <div class="col-md-4 single-select">
                                                                <select id="select1" name="expert1" class="form-control select2-placeholder-transition">
                                                                    <option value="" disabled="" selected="" style="display:none;">Select One...</option>

                                                                </select>
                                                            </div>
                                                            

                                                        </div>
                                                        <br>
                                                        <br>
                                                        <br>


                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <button class="btn btn-primary btnassgsubmit-single" type="button" name="assign-expert">Assign</button>

                                                                <button type="button" class="btn btn-primary reset-form" name="assign-expert">Reset</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                        </div>
                                    </div>


									<!-- Modal assign expert to nominee ecosystem -->
                                    <div class="modal fade" id="upload-file" role="dialog">
                                        <div class="modal-dialog modal-lg">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <form action="" method="post" novalidate="true" id="upload-file-form" enctype="multipart/form-data">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">
												          	<input type="text" id="uploademail" name="nomemail" readonly>
												          </h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="input-wrapper">
                                                            <div class="col-md-4 single-select">
																<input type="file" name="fieldvisitfile" id="upload-file-field" accept=".doc,.docx,.xls,.xlsx,.pdf">
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <br>
                                                        <br>


                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <button class="btn btn-primary" type="submit">Upload</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                        </div>
                                    </div>

									<!-- Modal assign expert to nominee ecosystem -->
                                    <div class="modal fade" id="msgbox" role="dialog">
                                        <div class="modal-dialog modal-lg">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                
                                            </div>

                                        </div>
                                    </div>

                                    <!-- END CONTENT BODY -->

                                    <!-- END CONTENT -->


                                    <!-- END CONTAINER -->


                                    <!-- BEGIN QUICK NAV -->

                                </div>


                                <!-- END QUICK NAV -->
                                <script src="/resources/dashboard/js/jquery.min.js" type="text/javascript"></script>

                                <script src="/resources/js/select2.min.js"></script>
                                <script src="/resources/js/select2-placeholder-transition.js"></script>

                                <style>
                                    .input-wrapper {
                                        padding-top: 30px;
                                    }
                                    
                                    .select2-placeholder-transition {
                                        width: 200px!important;
                                        max-width: 100%;
                                    }
                                    
                                    .select2-placeholder-transition2,
                                    .select2-placeholder-transition3 {
                                        width: 200px;
                                    }
                                    
                                    .select2-container+label {
                                        position: absolute;
                                        top: -28px;
                                        left: 0;
                                        padding: 5px;
                                        transition: transform .2s ease;
                                        color: #000;
                                        pointer-events: none;
                                    }
                                    
                                    tfoot #filters2 th:nth-child(1) select,
                                    tfoot #filters2 th:nth-child(2) select,
                                    tfoot #filters2 th:nth-child(3) select,
                                    tfoot #filters2 th:nth-child(4) select {
                                        display: none;
                                    }
                                    .hide2select .input-wrapper .col-md-4:nth-child(2),.hide2select .input-wrapper .col-md-4:nth-child(3)
                                    {
                                    display:none;}
                                    .portlet.light.bordered
                                    {    float: left;
    width: 100%;
                                    }
                                    .enterprisebtn,.ecosystembtn{margin-bottom:30px;}
                                </style>
                                
                                <%@include file="dashboard/footer.jsp" %>




                                    <script src="/resources/dashboard/js/workingjs/jquery.dataTables.min.new.js"></script>

                                    <link rel="stylesheet" type="text/css" href="/resources/dashboard/css/workingcss/jquery.dataTables.min.css">
                                    <link rel="stylesheet" type="text/css" href="/resources/dashboard/css/workingcss/buttons.dataTables.min.css">
                                    <script src="/resources/dashboard/js/workingjs/dataTables.buttons.min.js"></script>
                                    <script src="/resources/dashboard/js/workingjs/jszip.min.js"></script>
                                    <script src="/resources/dashboard/js/workingjs/pdfmake.min.js"></script>
                                    <script src="/resources/dashboard/js/workingjs/vfs_fonts.js"></script>
                                    <script src="/resources/dashboard/js/workingjs/buttons.html5.min.js"></script>




                                    <!-- this is ajax call to get value of nominator data from dashboard controller -->


                                    <script type="text/javascript">
                                    $(document).ready(function(){
                                            $('#example').DataTable({
                                                "ajax": {
                                                    "url": "/neas/api/regionaltabledata",
                                                    "async": false,
                                                    "type": 'POST',
                                                    "scrollX": true,
                                                    "dataSrc": ""
                                                },
                                                "columns": [{
                                                    "data": "id"
                                                }, {
                                                    "data": "view"
                                                }, {
                                                    "data": "mail"
                                                }, {
                                                    "data": "registrationnumber"
                                                }, {
                                                    "data": "fullname"
                                                }, {
                                                    "data": "role"
                                                }, {
                                                    "data": "institutename"
                                                }, {
                                                    "data": "emailid"
                                                }, {
                                                    "data": "contactnumber"
                                                }, {
                                                    "data": "occupation"
                                                }, {
                                                    "data": "addrline1"
                                                }, {
                                                    "data": "addrcity"
                                                }, {
                                                    "data": "addrstate"
                                                }, ],
                                                dom: 'Bfrtip',
                                                buttons: [
                                                    'copyHtml5',
                                                    'excelHtml5',
                                                    'csvHtml5',
                                                    'pdfHtml5'
                                                ],
                                                initComplete: function() {
                                                    this.api().columns().every(function() {
                                                        var column = this;


                                                        var select = $('<select><option value=""></option></select>')
                                                            .appendTo($("#filters").find("th").eq(column.index()))
                                                            .on('change', function() {
                                                                var val = $.fn.dataTable.util.escapeRegex(
                                                                    $(this).val());

                                                                column.search(val ? '^' + val + '$' : '', true, false)
                                                                    .draw();
                                                            });
                                                        //console.log(select);
                                                        column.data().unique().sort().each(function(d, j) {
                                                            select.append('<option value="' + d + '">' + d + '</option>')
                                                        });
                                                    });
                                                }
                                            });

                                    });
                                    </script>


                                    <!-- this is ajax call to get value of nominee data from dashboard controller -->

                                    <script type="text/javascript">
                                    $(document).ready(function(){
                                            var table = $('#example1').DataTable({
                                                "ajax": {
                                                    "url": "/neas/api/regionalnomineeshortlist",
                                                    "async": false,
                                                    "type": 'POST',
                                                    "scrollX": true,
                                                    "dataSrc": ""

                                                },
                                                "columns": [{
                                                    "data": "id"
                                                }, {
                                                    "data": "view"
                                                }, {
                                                    "data": "assign"
                                                }, {
                                                    "data": "uploadfile"
                                                }, {
                                                	"data": "expert1"
                                                }, {
                                                    "data": "expert2"
                                                }, {
                                                    "data": "expert3"
                                                }, {
                                                    "data": "avgscore"
                                                }, {
                                                    "data": "registrationnumber"
                                                }, {
                                                    "data": "nomineename"
                                                }, {
                                                    "data": "emailid"
                                                }, {
                                                    "data": "contactnumber"
                                                }, {
                                                    "data": "awardcategory"
                                                },{
                                                    "data": "promote"
                                                },{
                                                    "data": "promotestatus"
                                                },{
                                                    "data": "rank"
                                                },{
                                                    "data": "enterprisecategory"
                                                }, {
                                                    "data": "awardsub"
                                                }, {
                                                    "data": "state"
                                                }, ],
                                                dom: 'Bfrtip',

                                                buttons: [
                                                    'copyHtml5',
                                                    'excelHtml5',
                                                    'csvHtml5',
                                                    'pdfHtml5'
                                                ],
                                                'columnDefs': [{
                                                    'targets': 0,
                                                    'searchable': false,
                                                    'orderable': false,
                                                    'className': 'dt-body-center',
                                                    'render': function(data, type, full, meta) {
                                                        return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
                                                    }
                                                }],
                                                'order': [1, 'asc'],
                                                initComplete: function() {
                                                    this.api().columns().every(function() {
                                                        var column = this;


                                                        var select = $('<select><option value=""></option></select>')
                                                            .appendTo($("#filters2 ").find("th").eq(column.index()))
                                                            .on('change', function() {
                                                                var val = $.fn.dataTable.util.escapeRegex(
                                                                    $(this).val());

                                                                column.search(val ? '^' + val + '$' : '', true, false)
                                                                    .draw();
                                                            });
                                                        //console.log(select);
                                                        column.data().unique().sort().each(function(d, j) {
                                                            select.append('<option value="' + d + '">' + d + '</option>')
                                                        });
                                                    });
                                                }
                                            });
                                            $('#example-select-all').on('click', function() {
                                                // Check/uncheck all checkboxes in the table
                                                var rows = table.rows({
                                                    'search': 'applied'
                                                }).nodes();
                                                $('input[type="checkbox"]', rows).prop('checked', this.checked);


                                            });
                                            
                                            $('.ecosystembtn').prop("disabled", true);
                                        	$('.enterprisebtn').prop("disabled", true);
                                        	$('.enterprisebtn').attr('title', 'Select Award Category');
                                        	$('.ecosystembtn').attr('title', 'Select Award Category');

                                            $('#example1').on('change', function() {
                                            	  
                                            	
                                            
                                            	var target = $('#filters2 th:nth-child(13) option:selected').val();
                                            
                                            
                                            if (target == '') {
                                            	$('.ecosystembtn').prop("disabled", true);
                                            	$('.enterprisebtn').prop("disabled", true);
                                            }
                                            
                                            if (target == 'enterprise') {
                                            	$('.ecosystembtn').prop("disabled", false);
                                            	$('.enterprisebtn').prop("disabled", true);
                                            	$('.enterprisebtn').attr('title', 'Select Award Category');
                                            	$('.ecosystembtn').attr('title', '');
                                            	
                                            	
                                            	
                                            	$('.ecosystembtn').on('click', function() { 
                                            		var atLeastOneIsChecked = $('#example1 input[type="checkbox"]:checked').length > 0;
                                            		
                                            		if(atLeastOneIsChecked==false)
                                            			{
                                            			  alert("Please select Checkbox");
                                            			}
                                            		else
                                            			{
                                                     
                                                        var rows = table.rows({
                                                            'search': 'applied'
                                                        }).nodes();
                                                        var arrayOfValues = [];

                                                        $('input:checkbox:checked', rows).each(function() {
                                                            arrayOfValues.push($(this).closest('tr').find('td:nth-child(11)').text());
                                                        }).get();

                                                        document.getElementById("entemail").value  = arrayOfValues.toString();

                                                        $('#ecosystembtn-model').modal('show');
                                                        $(".modal select").val('').trigger('change');
                                            			}

                                                });                                            	
                                            }
                                            

                                            if (target == 'ecosystem') {
                                            	$('.enterprisebtn').prop("disabled", false);
                                            	$('.ecosystembtn').prop("disabled", true);
                                            	
                                            	$('.enterprisebtn').attr('title', '');
                                            	$('.ecosystembtn').attr('title', 'Select Award Category');
                                            	
                                            	$('.enterprisebtn').on('click', function() {  
                                            		var atLeastOneIsChecked = $('#example1 input[type="checkbox"]:checked').length > 0;
                                            		if(atLeastOneIsChecked==false)
                                        			{
                                        			  alert("Please select Checkbox");
                                        			}
                                            		else
                                            			{
                                            		 var rows = table.rows({
                                                         'search': 'applied'
                                                     }).nodes();
                                                     var arrayOfValues = [];

                                                     $('input:checkbox:checked', rows).each(function() {
                                                         arrayOfValues.push($(this).closest('tr').find('td:nth-child(11)').text());
                                                     }).get();

                                                     document.getElementById("ecoemail").value  = arrayOfValues.toString();

                                                     $('#enterprisebtn-model').modal('show');
                                                     $(".modal select").val('').trigger('change');
                                                   
                                            			}

                                                });                                            	
                                            }
                                            
                                            
                                            /*if (target == 'enterprise') {
                                            	$('.enterprisebtn').prop("disabled", false);
                                            }*/
                                            
                                           
                                            }); 

                                    });
                                        
                                    </script>
                                    
                          <script type="text/javascript">
                                    $(document).ready(function(){
                                            $('#example').DataTable({
                                                "ajax": {
                                                    "url": "/neas/api/regionaltabledata",
                                                    "async": false,
                                                    "type": 'POST',
                                                    "scrollX": true,
                                                    "dataSrc": ""
                                                },
                                                "columns": [{
                                                    "data": "id"
                                                }, {
                                                    "data": "view"
                                                }, {
                                                    "data": "mail"
                                                }, {
                                                    "data": "registrationnumber"
                                                }, {
                                                    "data": "fullname"
                                                }, {
                                                    "data": "role"
                                                }, {
                                                    "data": "institutename"
                                                }, {
                                                    "data": "emailid"
                                                }, {
                                                    "data": "contactnumber"
                                                }, {
                                                    "data": "occupation"
                                                }, {
                                                    "data": "addrline1"
                                                }, {
                                                    "data": "addrcity"
                                                }, {
                                                    "data": "addrstate"
                                                }, ],
                                                dom: 'Bfrtip',
                                                buttons: [
                                                    'copyHtml5',
                                                    'excelHtml5',
                                                    'csvHtml5',
                                                    'pdfHtml5'
                                                ],
                                                initComplete: function() {
                                                    this.api().columns().every(function() {
                                                        var column = this;


                                                        var select = $('<select><option value=""></option></select>')
                                                            .appendTo($("#filters").find("th").eq(column.index()))
                                                            .on('change', function() {
                                                                var val = $.fn.dataTable.util.escapeRegex(
                                                                    $(this).val());

                                                                column.search(val ? '^' + val + '$' : '', true, false)
                                                                    .draw();
                                                            });
                                                        //console.log(select);
                                                        column.data().unique().sort().each(function(d, j) {
                                                            select.append('<option value="' + d + '">' + d + '</option>')
                                                        });
                                                    });
                                                }
                                            });

                                    });
                                    </script>


                                    <!-- this is ajax call to get value of nominee data from dashboard controller -->

                                    <script type="text/javascript">
                                    $(document).ready(function(){
                                            var table = $('#example3').DataTable({
                                                "ajax": {
                                                    "url": "/neas/api/regionalnomineedata",
                                                    "async": false,
                                                    "type": 'POST',
                                                    "scrollX": true,
                                                    "dataSrc": ""

                                                },
                                                "columns": [{
                                                    "data": "id"
                                                }, {
                                                    "data": "view"
                                                }, {
                                                    "data": "registrationnumber"
                                                }, {
                                                    "data": "nomineename"
                                                }, {
                                                    "data": "emailid"
                                                }, {
                                                    "data": "contactnumber"
                                                }, {
                                                    "data": "awardcategory"
                                                }, {
                                                    "data": "enterprisecategory"
                                                }, {
                                                    "data": "awardsub"
                                                }, {
                                                    "data": "gender"
                                                }, {
                                                    "data": "socialcategory"
                                                }, {
                                                    "data": "dob"
                                                }, {
                                                    "data": "speccatval"
                                                }, {
                                                    "data": "enterprisename"
                                                }, {
                                                    "data": "address1"
                                                }, {
                                                    "data": "address2"
                                                }, {
                                                    "data": "city"
                                                }, {
                                                    "data": "state"
                                                }, ],
                                                dom: 'Bfrtip',

                                                buttons: [
                                                    'copyHtml5',
                                                    'excelHtml5',
                                                    'csvHtml5',
                                                    'pdfHtml5'
                                                ],                                                
                                                initComplete: function() {
                                                    this.api().columns().every(function() {
                                                        var column = this;


                                                        var select = $('<select><option value=""></option></select>')
                                                            .appendTo($("#filters3 ").find("th").eq(column.index()))
                                                            .on('change', function() {
                                                                var val = $.fn.dataTable.util.escapeRegex(
                                                                    $(this).val());

                                                                column.search(val ? '^' + val + '$' : '', true, false)
                                                                    .draw();
                                                            });
                                                        //console.log(select);
                                                        column.data().unique().sort().each(function(d, j) {
                                                            select.append('<option value="' + d + '">' + d + '</option>')
                                                        });
                                                    });
                                                }
                                            });

                                            
                                            
                                            /*if (target == 'enterprise') {
                                            	$('.enterprisebtn').prop("disabled", false);
                                            }*/
                                            
                                           
                                         

                                    });
                                        
                                    </script>
                                    
                          <!-- datable for regional shortlisted -->
                                    <!-- <script type="text/javascript">
                                    $(document).ready(function(){
                                            $('#example3').DataTable({
                                                "ajax": {
                                                    "url": "/neas/api/regionalshortlisteddata",
                                                    "async": false,
                                                    "type": 'POST',
                                                    "scrollX": true,
                                                    "dataSrc": ""
                                                },
                                                "columns": [{
                                                    "data": "id"
                                                }, {
                                                    "data": "nomineename"
                                                }, {
                                                    "data": "emailid"
                                                }, {
                                                    "data": "contact"
                                                }, {
                                                    "data": "state"
                                                }, {
                                                    "data": "expert1"
                                                }, {
                                                    "data": "expert2"
                                                }, {
                                                    "data": "expert3"
                                                }, {
                                                    "data": "avg"
                                                }, ],
                                                dom: 'Bfrtip',
                                                buttons: [
                                                    'copyHtml5',
                                                    'excelHtml5',
                                                    'csvHtml5',
                                                    'pdfHtml5'
                                                ],
                                                initComplete: function() {
                                                    this.api().columns().every(function() {
                                                        var column = this;


                                                        var select = $('<select><option value=""></option></select>')
                                                            .appendTo($("#filters3").find("th").eq(column.index()))
                                                            .on('change', function() {
                                                                var val = $.fn.dataTable.util.escapeRegex(
                                                                    $(this).val());

                                                                column.search(val ? '^' + val + '$' : '', true, false)
                                                                    .draw();
                                                            });
                                                        //console.log(select);
                                                        column.data().unique().sort().each(function(d, j) {
                                                            select.append('<option value="' + d + '">' + d + '</option>')
                                                        });
                                                    });
                                                }
                                            });

                                    });
                                    </script>
 -->
                                    <!-- this is to get regional data table from dashboard controller for both nominator and nominee-->

                                    <script>
                                        var app = angular.module('myApp', []);
                                        app.controller('jsonCtrl', function($scope, $http) {
                                            $http.post("/neas/api/regionaldata")
                                                .then(function(response) {
                                                    $scope.tableData = response.data;
                                                    document.getElementById('nominator-count').innerHTML = response.data.length;
                                                });

                                            $http.post("/neas/api/nomiregionaldata")
                                                .then(function(response) {
                                                    $scope.tableDataNew = response.data;
                                                    document.getElementById('nominee-count').innerHTML = response.data.length;
                                                });
                                        });
                                    </script>
                                    <script type="text/javascript">
							            $(document).ready(function() {
							            	
							            	 var url = "/neas/api/regional/shortlisted";
							            	 console.log(url);
							            	 $.ajax({
							            		 url:url,
							            		 type: 'POST',
							            		 
							            		success: function (result){
							            			var parsed = jQuery.parseJSON(result);
							            			console.log(parsed.count)
							            			$("#shortlisted-count").html();
							            	      	document.getElementById('shortlisted-count').innerHTML = parsed.count;	     
							            		} 
							            	 	 
							            	 });
							            });
							            
							         </script>


                                    <!--  js to show table on click  -->
                                    <script>
                                        $(document).ready(function() {
                                            $('.type-table1').click(function() {
                                                $('.datatable').fadeIn();
                                                $('.datatable2').fadeOut();
                                                $('.datatable3').fadeOut();
                                            });

                                            $('.type-table2').click(function() {
                                                $('.datatable2').fadeOut();
                                                $('.datatable').fadeOut();
                                                $('.datatable3').fadeIn();
                                            });
                                            $('.type-table3').click(function() {
                                                $('.datatable3').fadeOut();
                                                $('.datatable').fadeOut();
                                                $('.datatable2').fadeIn();
                                            });

                                            /* js for datatable count  */
                                            $('body').change(function() {
                                                $('.datatable #filter-count1 .dataTables_info').remove();
                                                $(".datatable .dataTables_info").clone().appendTo(".datatable #filter-count1");
                                                $('.total_record').show();

                                                $('.datatable2 #filter-count2 .dataTables_info').remove();
                                                $(".datatable2 .dataTables_info").clone().appendTo(".datatable2 #filter-count2");
                                                $('.total_record2').show();
                                            });



                                        });
                                    </script>


                                    <div id="myModal2" class="modal fade" role="dialog">
                                        <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Information</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Mail sent Successfully</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default allclose-assign" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>



                                    

                                    <script type="text/javascript">
                                        $(document).ready(function() {

                                            var url = "/neas/api/list/expert";
                                            var data = {
                                            		regionmail: $("#regionmail").val(),
                                                };
                                            $.ajax({
                                                url: url,
                                                type: 'POST',
                                                data: data,
                                                success: function(result) {
                                                    //$('#select1, #select4, #select7').append('<option>', '');
                                                    for (var key in result) {
                                                        firstProp = result[key];

                                                        $('#select1, #select4, #select7').append($('<option>', {
                                                            value: firstProp.emailId,
                                                            text: firstProp.expertName
                                                        }));
                                                    }
                                                }

                                            });
                                        });
                                    </script>
                                    <script type="text/javascript">
                                        $(document).ready(function() {

                                            var url = "/neas/api/list/expert";
                                            var data = {
                                            		regionmail: $("#regionmail").val(),
                                                };
                                            $.ajax({
                                                url: url,
                                                type: 'POST',
                                                data: data,
                                                success: function(result) {
                                                    
                                                    for (var key in result) {
                                                        firstProp = result[key];

                                                        $('#select2, #select5').append($('<option>', {
                                                            value: firstProp.emailId,
                                                            text: firstProp.expertName
                                                        }));
                                                    }
                                                }

                                            });
                                        });
                                    </script>
                                    <script type="text/javascript">
                                        $(document).ready(function() {

                                            var url = "/neas/api/list/expert";
                                            var data = {
                                            		regionmail: $("#regionmail").val(),
                                                };
                                            $.ajax({
                                                url: url,
                                                type: 'POST',
                                                data: data,
                                                success: function(result) {
                                                    
                                                    for (var key in result) {
                                                        firstProp = result[key];

                                                        $('#select3, #select6').append($('<option>', {
                                                            value: firstProp.emailId,
                                                            text: firstProp.expertName
                                                        }));
                                                    }
                                                }

                                            });
                                        });
                                    </script>

                                    <script>
                                        $(document).ready(function() {

                                            $('.btnassgsubmit').click(function() {
                                                //var inp = $("select").val();          

                                                //$('#myModal2a').modal('show');

                                                //var s1 = //$('.select-opt-mult select');
                                                var a = $( ".select-opt-mult option:selected" ).val(); //s1.options[s1.selectedIndex].value;
                                                var s2 = document.getElementById("select2");
                                                var b = s2.options[s2.selectedIndex].value;
                                                var s3 = document.getElementById("select3");
                                                var c = s3.options[s3.selectedIndex].value;

                                                /* var a = $("#select1 option:selected").val();
                                                    var b = $("#select2 option:selected").val();
                                                    var c = $("#select3 option:selected").val(); */
                                                    
                                                    if ((a == b) || (a == c) || (c == b)) {
                                                    	if ((a==="") || (b==="") || (c==="")) {
                                                        	alert('Please Select All Expert');
                                                        }
                                                    	else if((a!="" && b!="") || (a!="" && c!="") || (c!="" && b!="")){
                                                    		alert('Please Select Unique Expert');
                                                    	}
                                                    	else if((a==='') && (b==='') && (c==='') )
                                                    	{
                                                    		alert('Please Select Expert');
                                                    	}
                                                    	else {
                                                    		var data = {

                                                                    nomemail: $("#nomemail").val(),
                                                                    select1: a,
                                                                    select2: b,
                                                                    select3: c
                                                                };
                                                                $.ajax({
                                                                    type: "POST",
                                                                    url: "/neas/api/assign/expert",
                                                                    data: data,
                                                                    success: function() {

                                                                    }
                                                                });
                                                                $('#myModal2a').modal('show');
                                                        }
                                                    	
                                                    } else {
                                                    	if((a==='') && (b==='') && (c===''))
                                                    	{
                                                    		alert('Please Select Expert');
                                                    	}
                                                    	else if ((a==="") || (b==="") || (c==="")) {
                                                        	alert('Please Select All Expert');
                                                        }
                                                    	else{
                                                    		var data = {

                                                                    nomemail: $("#nomemail").val(),
                                                                    select1: a,
                                                                    select2: b,
                                                                    select3: c
                                                                };
                                                                $.ajax({
                                                                    type: "POST",
                                                                    url: "/neas/api/assign/expert",
                                                                    data: data,
                                                                    success: function() {

                                                                    }
                                                                });
                                                                $('#myModal2a').modal('show');
                                                    	}
                                                    }

                                            });

                                            
                                        
                                        


                                        $('.btnassgsubmit-single').click(function() {
                                            //var inp = $("select").val();          

                                            //$('#myModal2a').modal('show');

                                            //var s1 = $('.single-select select');
                                            var a = $( ".single-select option:selected" ).val();//s1.options[s1.selectedIndex].value;
                                            /* var s2 = document.getElementById("select2");
                                            var b = s2.options[s2.selectedIndex].value;
                                            var s3 = document.getElementById("select3");
                                            var c = s3.options[s3.selectedIndex].value; */
                                                
                                                if(a==='')
                                            	{
                                            	alert('Please Select Expert');
                                            	}
                                        	else{
                                        		var data = {

                                                        nomemail: $("#nomemail2").val(),
                                                        select1: a
                                                    };
                                                    $.ajax({
                                                        type: "POST",
                                                        url: "/neas/api/assign/expert",
                                                        data: data,
                                                        success: function() {

                                                        }
                                                    });
                                                    $('#myModal2a').modal('show');
                                        	}

                                        });

                                        $('.allclose').click(function() {
                                            $('#myModal2a').modal('hide');
                                            $('#assign-expert').modal('hide');

                                        });
                                        
                                        
                                       /* $('.btnupload').bind('click',function() {
                                        	$('#upload-file-form').attr('action','/neas/api/upload/fieldvisit/'+$(this).data('value'));
                                        	$('#uploademail').val($(this).data('email'));
                                        }); */
                                        $('#example1').on('click', '.btnupload', function(e){
                                        	$('#upload-file-form').attr('action','/neas/api/upload/fieldvisit/'+$(this).data('value'));
                                        	var upload_email="";
                                        	upload_email = $(this).attr('data-email');
                                            $('#uploademail').val(upload_email);                                            	
                                        	});
                                        
                                        

                                    });
                                    
                                    </script>


                                    <!-- Modal -->
                                    <div id="myModal2a" class="modal fade" role="dialog">
                                        <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Information</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Expert Added Successfully</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default allclose" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>






                                    <!-- Modal -->
                                    <div id="sentmailconfirm" class="modal fade" role="dialog">
                                        <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Information</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Message Successfully Sent</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default allclose" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>


                                    <script>
                                        $(document).ready(function() {

                                            $('.btnsubmit-cnf').click(function() {
                                                var inp = $("#subject").val();
                                                if (inp.length > 0) {
                                                    var data = {
                                                        subject: $("#subject").val(),
                                                        mailTo: $("#mailTo").val(),
                                                        msg: $("#msg").val()
                                                    };
                                                    $.ajax({
                                                        type: "POST",
                                                        url: "/neas/api/support/mail",
                                                        data: data,
                                                        success: function() {

                                                        }
                                                    });

                                                    $('#sentmailconfirm').modal('show');

                                                } else {
                                                    alert('please fill all detail');
                                                }

                                            });

                                            $('.allclose').click(function() {
                                                $('#myModal').modal('hide');
                                            });





                                        });
                                    </script>

                                    






                                    <!-- ecosystembtn multiple assign expert 3 popup-->
                                    <div class="modal fade" id="ecosystembtn-model" role="dialog">
                                        <div class="modal-dialog modal-lg">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <form action="/neas/api/assign/expert/ent" method="post" novalidate="true">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">
                                                        Assign Expert For Enterprise
												          	<input type="hidden" id="entemail" name="entemail" readonly>
												          </h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="input-wrapper">
                                                            <div class="col-md-4">
                                                                <select id="select4" name="expert1" class="form-control select2-placeholder-transition">
                                                                    <option value="" disabled="" selected="" style="display:none;">Select One...</option>

                                                                </select>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <select id="select5" name="expert2" class=" form-control select2-placeholder-transition2">
                                                                    <option value="" disabled="" selected="" style="display:none;">Select One...</option>

                                                                </select>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <select id="select6" name="expert3" class=" form-control select2-placeholder-transition3">
                                                                    <option value="" disabled="" selected="" style="display:none;">Select One...</option>

                                                                </select>
                                                            </div>

                                                        </div>
                                                        <br>
                                                        <br>
                                                        <br>


                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <button class="btn btn-primary ecosyste-assign" type="button" name="assign-expert">Assign</button>

                                                                <button type="button" class="btn btn-primary reset-form-ecosyste" name="assign-expert">Reset</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                        </div>
                                    </div>



                                    <!-- multiple assign expert Successfully Popup-->
                                    <div id="ecosystembtn-successful" class="modal fade" role="dialog">
                                        <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Information</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Expert Added Successfully</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default allclose-ecosystembtn-succ" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>


                                    <!-- multiple assign expert 3 script-->
                                    <script>
                                        $(document).ready(function() {
                                            $('.ecosyste-assign').click(function() {
                                                var s1 = document.getElementById("select4");
                                                var a = s1.options[s1.selectedIndex].value;
                                                var s2 = document.getElementById("select5");
                                                var b = s2.options[s2.selectedIndex].value;
                                                var s3 = document.getElementById("select6");
                                                var c = s3.options[s3.selectedIndex].value;
                                                
                                                
                                               

                                                if ((a == b) || (a == c) || (c == b)) {
                                                	if ((a==="") || (b==="") || (c==="")) {
                                                    	alert('Please Select All Expert');
                                                    }
                                                	else if((a!="" && b!="") || (a!="" && c!="") || (c!="" && b!="")){
                                                		alert('Please Select Unique Expert');
                                                	}
                                                	else if((a==='') && (b==='') && (c==='') )
                                                	{
                                                		alert('Please Select Expert');
                                                	}
                                                	else {
                                                		var data = {

                                                				entemail: $("#entemail").val(),
                                                                select4: a,
                                                                select5: b,
                                                                select6: c
                                                            };
                                                            $.ajax({
                                                                type: "POST",
                                                                url: "/neas/api/assign/expert/ent",
                                                                data: data,
                                                                success: function() {

                                                                }
                                                            });
                                                            $('#ecosystembtn-successful').modal('show');
                                                    }
                                                	
                                                } else {
                                                	if((a==='') && (b==='') && (c===''))
                                                	{
                                                		alert('Please Select Expert');
                                                	}
                                                	else if ((a==="") || (b==="") || (c==="")) {
                                                    	alert('Please Select All Expert');
                                                    }
                                                	else{
                                                		var data = {

                                                				entemail: $("#entemail").val(),
                                                                select4: a,
                                                                select5: b,
                                                                select6: c
                                                            };
                                                            $.ajax({
                                                                type: "POST",
                                                                url: "/neas/api/assign/expert/ent",
                                                                data: data,
                                                                success: function() {

                                                                }
                                                            });
                                                            $('#ecosystembtn-successful').modal('show');
                                                	}
                                                }
                                            });
                                            $('.allclose-ecosystembtn-succ').click(function() {
                                                $('#ecosystembtn-successful').modal('hide');
                                                $('#ecosystembtn-model').modal('hide');
                                            });
                                        });
                                    </script>





                                    <!-- enterprisebtn single assign expert popup-->
                                    <div class="modal fade" id="enterprisebtn-model" role="dialog">
                                        <div class="modal-dialog modal-lg">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <form action="/neas/api/assign/expert/eco" method="post" novalidate="true">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">
                                                        Assign Expert For Ecosystem
												          	<input type="hidden" id="ecoemail" name="ecoemail" readonly>
												          </h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="input-wrapper">
                                                            <div class="col-md-4">
                                                                <select id="select7" name="expert1" class="form-control select2-placeholder-transition">
                                                                    <option value="" disabled="" selected="" style="display:none;">Select One...</option>

                                                                </select>
                                                            </div>

                                                        </div>
                                                        <br>
                                                        <br>
                                                        <br>


                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <button class="btn btn-primary enterprise-assign" type="button" name="assign-expert">Assign</button>

                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                        </div>
                                    </div>



                                    <!-- enterprise assign expert Successfully Popup-->
                                    <div id="enterprise-successful" class="modal fade" role="dialog">
                                        <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Information</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Expert Added Successfully</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default allclose-enterprise-succ" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>


                                    <!-- enterprise assign expert 1 script-->
                                    <script>
                                        $(document).ready(function() {
                                            $('.enterprise-assign').click(function() {
                                                var s1 = document.getElementById("select7");
                                                var a = s1.options[s1.selectedIndex].value;
                                                var s2 = document.getElementById("select5");
                                                var b = s2.options[s2.selectedIndex].value;
                                                var s3 = document.getElementById("select6");
                                                var c = s3.options[s3.selectedIndex].value;

                                                if ((a == '')) {
                                                    alert('Please Select Expert');
                                                } else {
                                                    var data = {
                                                    		ecoemail: $("#ecoemail").val(),
                                                        select7: a
                                                    };
                                                    $.ajax({
                                                        type: "POST",
                                                        url: "/neas/api/assign/expert/eco",
                                                        data: data,
                                                        success: function() {}
                                                    });
                                                    $('#enterprise-successful').modal('show');
                                                }
                                            });
                                            $('.allclose-enterprise-succ').click(function() {
                                                $('#enterprisebtn-model').modal('hide');
                                                $('#enterprise-successful').modal('hide');
                                            });
                                        });
                                    </script>



<script>
                                    var $select = $(".input-wrapper select");

                                    $select.on("change", function() {
                                        var selected = [];
                                        $.each($select, function(index, select) {
                                            if (select.value !== "") {
                                                selected.push(select.value);

                                            } else {
                                                $(this).prop("disabled", false);
                                            }
                                            $("option").prop("disabled", false);
                                            for (var index in selected) {
                                                $('option[value="' + selected[index] + '"]').prop("disabled", true);
                                            }
                                        });
                                    });
                                    $('.reset-form').click(function() {
                                        $("#select1,#select2,#select3").prop("disabled", false);
                                        $(".modal select").val('').trigger('change')
                                    });

                                    $('.reset-form-ecosyste').click(function() {
                                        $("#select4,#select5,#select6").prop("disabled", false);
                                        $(".modal select").val('').trigger('change')
                                    });


                                    $('.allclose-assign,.allclose,.allclose-ecosystembtn-succ,.allclose-enterprise-succ').click(function() {
                                        $('#myModal2a').modal('hide');
                                        location.reload();
                                    });
                                </script>

<!--  script for disabled warning   -->
                           <script>
                                    $(document).ready(function(){
                                        $.fn.dataTable.ext.errMode = 'none';

                                        $('#example').on( 'error.dt', function ( e, settings, techNote, message ) {
                                        console.log( 'An error has been reported by DataTables: ', message );
                                        } ) .DataTable();
                                        $('#example').DataTable();
                                        
                                        $('#example1').on( 'error.dt', function ( e, settings, techNote, message ) {
                                            console.log( 'An error has been reported by DataTables: ', message );
                                            } ) .DataTable();
                                            $('#example1').DataTable();
                                            /*$('.fade').click(function(){
                                            	$('#nomemail2').val('');
                                            	$('#nomemail').val('');
                                            }); */
                                        });
                                    </script>   
                                    
                                    <script>
                                        $(function() {

                                            $('.btnmail').bind('click', function() {
                                                var email = $(this).attr('data-value');
                                                $('#mailTo').val(email);

                                            });


                                          /*  $('.btnassign2').bind('click', function() {
                                            	$('#nomemail2').val('');
                                                var mail = $(this).attr('data-value');
                                                $('#nomemail2').val(mail);
                                            }); */
                                            
                                            $('#example1').on('click', '.btnassign2', function(e){
                                            	$('#nomemail2').val('');
                                                var mail = $(this).attr('data-value');
                                                $('#nomemail2').val(mail);
                                            	
                                            });
                                            
                                           /* $('.btnassign').bind('click', function() {
                                            	$('#nomemail').val('');
                                                var mail = $(this).attr('data-value');
                                                $('#nomemail').val(mail);
                                            }); */
                                            
                                            $('#example1').on('click', '.btnassign', function(e){
                                            	$('#nomemail').val('');
                                                var mail = $(this).attr('data-value');
                                                $('#nomemail').val(mail);
                                            	
                                            });
                                           
                                            
                                        });

                                        function toggle(source) {
                                            var checkboxes = document.querySelectorAll('input[type="checkbox"]');
                                            for (var i = 0; i < checkboxes.length; i++) {
                                                if (checkboxes[i] != source)
                                                    checkboxes[i].checked = source.checked;
                                            }
                                        }
                                        
                                        $('#example1').on('change', function() {
                                        	
                                        	/*$('.btnassign2').bind('click', function() {
                                            	$('#nomemail2').val('');
                                                var mail = $(this).attr('data-value');
                                                $('#nomemail2').val(mail);
                                            });
                                        	
                                            $('.btnassign').bind('click', function() {
                                            	$('#nomemail').val('');
                                                var mail = $(this).attr('data-value');
                                                $('#nomemail').val(mail);
                                            });	*/
                                            
                                            $('#example1').on('click', '.btnassign', function(e){
                                            	$('#nomemail').val('');
                                                var mail = $(this).attr('data-value');
                                                $('#nomemail').val(mail);
                                            	
                                            });
                                            
                                            $('#example1').on('click', '.btnassign2', function(e){
                                            	$('#nomemail2').val('');
                                                var mail = $(this).attr('data-value');
                                                $('#nomemail2').val(mail);
                                            	
                                            });
                                            
                                            $('#example1').on('click', '.btnupload', function(e){
                                            	$('#upload-file-form').attr('action','/neas/api/upload/fieldvisit/'+$(this).data('value'));
                                            	var upload_email="";
                                            	upload_email = $(this).attr('data-email');
                                                $('#uploademail').val(upload_email);                                            	
                                            	});
                                        })
                                    </script>  
                                    
                                    <script>
                                    $(document).ready(function(){
                                    	$('.modal-backdrop').click(function() {
                                    		$(".modal select").val('').trigger('change');
                                    	});
                                    	
                                    	$('#filters2').on('change', function(){
                                    		$('#example1 input[type="checkbox"]').prop('checked', false); 
                                    		});
                                    });
                                    </script>   
                                    
                                    <script>
                                    $(document).ready(function(){
                                    	$('.modal-backdrop').click(function() {
                                    		$(".modal select").val('').trigger('change');
                                    	});                               
                                    });
                                    </script>    
                                    
                                    <script>
										function getUrlParameter(name) {
										    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
										    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
										    var results = regex.exec(location.search);
										    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
										};
										
										if(getUrlParameter("status")=='false'){
											
											$('#msgbox .modal-content').html("<div class='modal-header'><button type='button' class='close' data-dismiss='modal'>×</button></div><h3>Error</h3><p>"+getUrlParameter("reason")+"</p>");
											$('#msgbox').modal("show");
										}
										if(getUrlParameter("status")=='true'){
											$('#msgbox .modal-content').html("<div class='modal-header'><button type='button' class='close' data-dismiss='modal'>×</button></div><h3>Success</h3><p>"+getUrlParameter("reason")+"</p>");
											$('#msgbox').modal("show");
										}
									</script> 
									
									<!-- editscore model -->
<div id="editscore" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Score</h4>
      </div>
      <div class="modal-body">
        <form id="editscore-form" action="" method="post">
          <div class="col-md-6">
            <div class="form-group">
                <input type="hidden" name="hidemail" id="hidemail">
              <select id="editscoreselect" class="form-control">
                  <option value="">Want to Promote?</option>
                <option value="Yes">Yes</option>
                  <option value="No">No</option>
                </select>
              </div>
            </div>
          <div class="col-md-6">
            <div class="form-group">
            <label name="rankval">Rank</label>
                <input id="rankval" type="number" class="form-control" placeholder="Rank in Two Digit" >
              </div>
            </div>
            <div class="col-md-12">
              <button type="button" class="btn btn-primary" id="editscore-form-btn" name="submit">Update Score</button>
            </div>
            <div class="clearfix"></div>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div> 
        
    <script>
    $(document).ready(function () {
        
        	$('#editscore-form-btn').on('click', function(){
            var score = $('#editscoreselect option:selected').val();
            var rank = $('#rankval').val();
            var ranklen = rank.length;
            if(score=="")
             {
                 alert("Please Select Promote Option");
                 
             }
            else if(ranklen>2 || ranklen<2 || rank<0)
             {
                 alert("Invalid Rank, Provide rank like 01,02,03 .... upto 99");
                 
             }
            else
                {
            	 var s2 = document.getElementById("editscoreselect");
                 var b = s2.options[s2.selectedIndex].value;

                     		var data = {

                                     hideemail: $("#hidemail").val(),
                                     promote: b,
                                     rank: $("#rankval").val(),
                                 };
                                 $.ajax({
                                     type: "POST",
                                     url: "/neas/api/promote/rank",
                                     data: data,
                                     success: function() {
                                    	 
                                     }
                                 });
                                 location.reload();
                                 
                                 
                }
        });
        
        $('#example1').on('click', '.editscore', function(e){
        	$('#hidemail').val('');
            var hmail = $(this).attr('data-value');
            $('#hidemail').val(hmail);
        	
        });
    });
    
   
</script>

                    </body>

                </html>