package com.xlri.awards.dao.service.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.xlri.awards.dao.entity.Partners;
import com.xlri.awards.dao.service.api.PartnersService;

@Repository
@Transactional
public class PartnersServiceImpl implements PartnersService{
	

	private static final Logger LOGGER = LoggerFactory.getLogger(PartnersServiceImpl.class);
	
	@Autowired
	SessionFactory sessionFactory;

	@Override
	public boolean savePartners(Partners partners) {
		try {

			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			session.save(partners);
			session.getTransaction().commit();
			return true;
		}
	
		catch (Exception e) {
			LOGGER.error(e.getMessage());
			return false;

		}
	}

	@Override
	public boolean updatePartners(Partners partners) {
		try {

			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			session.update(partners);
			session.getTransaction().commit();
			return true;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return false;
		}
	}

	@Override
	public List<Partners> getAllPartners() {
		List<Partners> partnersList = null;
		try{
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			partnersList = session.createQuery("From Partners").getResultList();
			session.getTransaction().commit();
			return partnersList;
		}
		catch(Exception e){
			LOGGER.error(e.getMessage());
			return null;	
		}
	}
	
	@Override
	public Partners getPartnersById(String id) {
		
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<Partners> query =  session.createQuery("SELECT p FROM Partners p WHERE p.id='"+id+"'");
			session.getTransaction().commit();
			return query.getSingleResult();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

}
