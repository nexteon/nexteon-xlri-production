<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ page import="com.xlri.awards.common.StateUtil" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<%@include file="fragments/headerlibs.jsp" %>
<body class="innerpage">
<%@include file="fragments/header.jsp" %>
<!-- Body Starts Here -->
<div class="clearfix"></div>
<style>
 .innerpage .registration-close h3{font-size:38px;margin-top:50px;line-height:44px}
.innerpage .registration-close h4{font-size:30px;font-weight:600}
@media only screen and (max-width:767px) {
.innerpage .registration-close h3{font-size:28px;margin-top:20px;line-height:34px}
.innerpage .registration-close h4{font-size:20px;margin-bottom:20px}
}
</style>
<main role="main">
        <div class="page-title">
            <h2>Nominator Registration</h2>
        </div>
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="whitebg registration-close">
                            <h3 class="text-center">Registration is  now closed for this event.</h3>
                            <h4 class="text-center">Thanks for your support and co-operation.</h4>
                        </div>
                    </div>
                    <!--end of class whitebg-->
                </div>
            </div>
        </div>

</main>

<!-- Body Ends Here -->
<%@include file="fragments/footer.jsp" %>
</body>
</html>