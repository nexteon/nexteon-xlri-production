package com.xlri.awards.dao.service.api;

import java.util.List;

import com.xlri.awards.dao.entity.ThirdCatNominee;

public interface ThirdCatNomineeService {

	public boolean saveNominee(ThirdCatNominee nominee);
	
	public ThirdCatNominee getNomineeByContact(String contact);

	public List<ThirdCatNominee> getAllThirdCatNom();
}
