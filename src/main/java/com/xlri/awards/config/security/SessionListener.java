package com.xlri.awards.config.security;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
 
public class SessionListener implements HttpSessionListener {
 
    @Override
    public void sessionCreated(HttpSessionEvent event) {
        event.getSession().setMaxInactiveInterval(60*60*60);
        event.getSession().getServletContext().getSessionCookieConfig().setHttpOnly(true);
        event.getSession().getServletContext().getSessionCookieConfig().setSecure(true);
    }
 
    @Override
    public void sessionDestroyed(HttpSessionEvent event) {
    }
}