<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">
   <%@include file="dashboard/headerlibs.jsp" %>
   <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white" id="page-top" >
   			<div class="page-wrapper">
          <%@include file="dashboard/header.jsp" %>
           


                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="/userdata">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Dashboard</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> Expert</h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->

<!-- <form action="/neas/api/register/expert" method="post" data-toggle="validator" role="form"> -->
						<div class="row">
                        
                        <div class="col-md-12">
                        <div class="portlet box blue form-display1">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Add Expert</div>
                                        
                                    </div>
                                    <div class="portlet-body">
                                      <div class="col-xs-12 form-1-box">
                                          <form action="/neas/api/register/expert" data-toggle="validator" method="post" role="form">
											<div class="row">
												<div class="col-sm-6">
													<div class="row">
														<div class="col-lg-12">
															<label>Email Address</label>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-12">
                                                            <div class="form-group">
															<input required type="email" id="emailid" name="emailid" class="form-control" placeholder="Email Address" required data-required-error="Please fill in this field." data-remote="/neas/api/checkemail/expert" data-remote-error="Email is already registered." pattern="^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$" data-pattern-error="Please provide a valid email id."><div class="help-block with-errors"></div></div>
														</div>
													</div>											
												</div>                                                
                                                
                                                <div class="col-sm-6">
													<div class="row">
														<div class="col-lg-12">
															<label>Contact Number</label>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-12">
                                                            <div class="form-group">
															<input required placeholder="Contact Number" id="contact" name="contact"  type="tel" class="form-control" maxlength="10" pattern="[0-9]{10}" required data-required-error="Please fill in this field.">
															<div class="help-block with-errors"></div>
                                                            </div>
														</div>
													</div>
												</div>
											</div>
                                          
                                          <div class="row">
												<div class="col-sm-6">
													<div class="row">
														<div class="col-lg-12">
															<label>Expert Name</label>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-12">
                                                            <div class="form-group">
															<input required type="text" id="expertname" name="expertname"  class="form-control" placeholder="Expert Name">
															<div class="help-block with-errors"></div>
                                                            </div>
														</div>
													</div>											
												</div>                                                
                                                
                                                <div class="col-sm-6">
													<div class="row">
														<div class="col-lg-12">
															<label>Parter With</label>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-12">
                                                            <div class="form-group">
															<input required placeholder="Parter With" id="partnerwith" name="partnerwith"  type="text" value="${name}" readonly class="form-control" >
															<div class="help-block with-errors"></div>
                                                            </div>
														</div>
													</div>
												</div>
											</div>
                                          
                                          <div class="row">
												<div class="col-sm-6">
													<div class="row">
														<div class="col-lg-12">
															<label>Designation</label>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-12">
                                                            <div class="form-group">
															<input required type="text" id="designation" name="designation"  class="form-control" placeholder="Designation">
															<div class="help-block with-errors"></div>
                                                            </div>
														</div>
													</div>											
												</div>                                                
                                                
                                                <div class="col-sm-6">
													<div class="row">
														<div class="col-lg-12">
															<label>Specialization</label>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-6">
                                                            <div class="form-group">
															<select class="form-control" id="award-category-search" name="specialization" aria-required="true" required data-required-error="Please fill in this field.">
					                                              <option value="" selected>Select</option>
					                                              <option value="Textile, textile articles, leather and related goods">1. Textile, textile articles, leather and related goods</option>
                                                                    <option value="Agriculture, food processing and forestry – also includes vegetable products, vegetable fats, prepared foodstuffs and agri-tech">2. Agriculture, food processing and forestry – also includes vegetable products, vegetable fats, prepared foodstuffs and agri-tech</option>
                                                                    <option value="Chemicals, pharma, bio and others">3. Chemicals, pharma, bio and others</option>
                                                                    <option value="Retail Trade">4. Retail Trade</option>
                                                                    <option value="Renewable energy and waste management">5. Renewable energy and waste management</option>
                                                                    <option value="Handicrafts">6. Handicrafts</option>
                                                                    <option value="Healthcare">7. Healthcare</option>
                                                                    <option value="Hospitality, Tourism & Travel">8. Hospitality, Tourism & Travel</option>
                                                                    <option value="Logistics, transports, e-commerce & other services">9. Logistics, transports, e-commerce & other services</option>
					                                              <option value="other">Others</option> 
					                                          </select>
					                                          <div class="help-block with-errors"></div>
                                                            </div>
														</div>
														<div class="col-lg-6 other-spec">
                                                            <div class="form-group">
															<input required type="text" id="other-spec" name="other-specialization"  class="form-control" placeholder="Specialization">
                                                            </div>
														</div>
													</div>
												</div>
											</div>
                                          
                                          
                                          
											
											
											<div class="row">
												<div class="col-sm-12">

													<div class="row">
														<div class="col-lg-12">
															<label>Address</label>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-12">
                                                            <div class="form-group">
															<textarea required class="form-control" id="address" name="address"  placeholder="Address"></textarea>
															<div class="help-block with-errors"></div>
                                                            </div>
														</div>
													</div>
													
												</div>


												


											</div>
                                          
                                          <div class="row">
												<div class="col-sm-6">
													<div class="row">
														<div class="col-lg-12">
															<label>Password</label>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-12">
                                                            <div class="form-group">
															<input required type="password" class="form-control" id="password" name="password"  placeholder="Password" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&#1]{8,}$">
															<span style="font-size: 10px;">(An 8 character long password reqiured that must contain a small letter, a capital letter, a special character  ( @ $ ! % * ? &amp; ^ # ) and a digit.)</span>
															<div class="help-block with-errors"></div>
                                                            </div>
                                                            
														</div>
													</div>											
												</div>                                                
                                                
                                                <div class="col-sm-6">
													<div class="row">
														<div class="col-lg-12">
															<label>Confirm Password</label>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-12">
                                                            <div class="form-group">
															<input required placeholder="Confirm Password" id="confirm" data-match="#password" name="confirm"  type="password" class="form-control" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&#1]{8,}$">
															<div class="help-block with-errors"></div>
                                                            </div>
														</div>
													</div>
												</div>
											</div>
                                          
                                          <div class="row">
												<div class="col-sm-12">
                                                    <button class="btn btn-primary addExpert" type="submit" name="add-expert">Add Expert</button>
                                              </div>
                                              <span id="checkMsg"></span>
                                          </div>
                                          
                                          
											</form>
											
											</div>
											
											
											
											</div>
                                        
                                      	<c:if test="${not empty successMsg }">
										  	<div class="alert alert-success">
										  		Expert has been added successfully.
											</div>
						                </c:if>
						                <c:if test="${not empty errorMsg }">
										  	<div class="alert alert-danger">
										  		Some error occurred while adding expert.
											</div>
						                </c:if>
                                    </div>
                                </div>    
                            
                        </div>
                       <!--  </form> -->
                        
                        
                        

  </div>
</div>
                        
                        
                        
                       
                        
                   
                    <!-- END CONTENT BODY -->
                
                <!-- END CONTENT -->
               
         
            <!-- END CONTAINER -->
            
 
        <!-- BEGIN QUICK NAV -->
       
           </div>
           
         
        <!-- END QUICK NAV -->
        
        
     <%@include file="dashboard/footer.jsp"%>
     
      
      
      <script src="/resources/js/validator.min.js"></script>
     <script src="/resources/dashboard/js/workingjs/jquery.dataTables.min.new.js"></script>
    <link rel="stylesheet" type="text/css" href="/resources/dashboard/css/workingcss/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="/resources/dashboard/css/workingcss/buttons.dataTables.min.css">
    <script src="/resources/dashboard/js/workingjs/dataTables.buttons.min.js"></script>
    <script src="/resources/dashboard/js/workingjs/jszip.min.js"></script>
    <script src="/resources/dashboard/js/workingjs/pdfmake.min.js"></script>
    <script src="/resources/dashboard/js/workingjs/vfs_fonts.js"></script>
    <script src="/resources/dashboard/js/workingjs/buttons.html5.min.js"></script>
    
      
      <script>
      $(document).ready(function () {
      	$('.other-spec').hide();
      	$('#award-category-search').change(function(){
      		if($('#award-category-search').val()==='other'){
      			$('.other-spec').show();
  				$(".other-spec input").prop('required',true);
      		}else{
      			$('.other-spec').hide();
  				$(".other-spec input").prop('required',false);
      		}
      	});
      });
      
    
  </script>
  <style>
 .form-1-box .btn.disabled
  {cursor: pointer;
  opacity:1.0;
  }
  </style>    
     
                     
    </body>

</html>