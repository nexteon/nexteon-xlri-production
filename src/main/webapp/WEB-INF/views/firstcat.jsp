<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
   <%@include file="fragments/headerlibs.jsp" %>
   <body class="innerpage">
      <script>
      	
      </script>
      <%@include file="fragments/header.jsp" %>
      <!-- Body Starts Here -->
      <main role="main">
         <div class="page-title">
            <h2>NEAS 2018 Nomination Form</h2>
         </div>
         <div class="content">
            <div class="container">
               <div class="row">
                  <div class="col-md-12">
                     <div class="whitebg">
                        <div class="row corner-box">
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label for="registrationNumber">Registration Number:
                                 <span class="heading-span">${registrationNumber}</span></label>
                              </div>
                           </div>
                        </div>
                        <div class="row corner-box">
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label for="awardcat">Award category:
                                 <span class="heading-span">Initial Investment upto 1 Lakh</span></label>
                              </div>
                           </div>
                        </div>
                        <div class="circle-two">
                           <ul>
                              <li><a href="#"><span>Part A</span></a></li>
                              <li class="active-cir"><a href="#"><span>Part B</span></a></li>
                           </ul>
                        </div>
                        <div class="form-header">
                           <h4>PART B: Enterprise Details and Nominator's Assessment</h4>
                        </div>
                        <div style="border: 1px solid #ccc; width:100%;padding: 13px; margin-bottom: 30px;float: left;">
                           <p style="font-size: 16px">Of all the enterprises you have seen, rank the enterprise you are nominating on the following characteristics. In other words, imagine all similar organizations
                              and if you place the <i><u>highest ranked organization(10 points)</u></i> on the extreme right and <i><u>lowest ranked organization(0 points)</u></i> on the extreme left, then, where would you place the
                              enterprise you are nominating.
                           </p>
                        </div>
                        <div class="form-body">
                           <form id="rename-sheet" method="post" data-toggle="validator" role="form">
                              <input type="hidden" name="registrationNumber" value="${registrationNumber}"/>
                              <div class="rename-sheet">
                                 <div class="form-group col-md-12 ">
                                    <strong class="main-td col-md-4">1. Growth in customer base for last four years</strong>
                                    <div class="col-md-8 toggle">	
                                       <label class="radio-inline"><input type="radio" name="question1" value="0" required><span class="label-text">0</span></label>
                                       <label class="radio-inline"><input type="radio" name="question1" value="1"><span class="label-text">1</span></label>
                                       <label class="radio-inline"><input type="radio" name="question1" value="2"><span class="label-text">2</span></label>
                                       <label class="radio-inline"><input type="radio" name="question1" value="3"><span class="label-text">3</span></label>
                                       <label class="radio-inline"><input type="radio" name="question1" value="4"><span class="label-text">4</span></label>
                                       <label class="radio-inline"><input type="radio" name="question1" value="5"><span class="label-text">5</span></label>
                                       <label class="radio-inline"><input type="radio" name="question1" value="6"><span class="label-text">6</span></label>
                                       <label class="radio-inline"><input type="radio" name="question1" value="7"><span class="label-text">7</span></label>
                                       <label class="radio-inline"><input type="radio" name="question1" value="8"><span class="label-text">8</span></label>
                                       <label class="radio-inline"><input type="radio" name="question1" value="9"><span class="label-text">9</span></label>
                                       <label class="radio-inline"><input type="radio" name="question1" value="10"><span class="label-text">10</span></label>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                 </div>
                                 <div class="form-group col-md-12">
                                    <strong class="main-td col-md-4">2. Growth in new units/brances in the last four years</strong>
                                    <div class="col-md-8 toggle">	
                                       <label class="radio-inline"><input type="radio" name="question2" value="0" required><span class="label-text">0</span></label>
                                       <label class="radio-inline"><input type="radio" name="question2" value="1"><span class="label-text">1</span></label>
                                       <label class="radio-inline"><input type="radio" name="question2" value="2"><span class="label-text">2</span></label>
                                       <label class="radio-inline"><input type="radio" name="question2" value="3"><span class="label-text">3</span></label>
                                       <label class="radio-inline"><input type="radio" name="question2" value="4"><span class="label-text">4</span></label>
                                       <label class="radio-inline"><input type="radio" name="question2" value="5"><span class="label-text">5</span></label>
                                       <label class="radio-inline"><input type="radio" name="question2" value="6"><span class="label-text">6</span></label>
                                       <label class="radio-inline"><input type="radio" name="question2" value="7"><span class="label-text">7</span></label>
                                       <label class="radio-inline"><input type="radio" name="question2" value="8"><span class="label-text">8</span></label>
                                       <label class="radio-inline"><input type="radio" name="question2" value="9"><span class="label-text">9</span></label>
                                       <label class="radio-inline"><input type="radio" name="question2" value="10"><span class="label-text">10</span></label>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                 </div>
                                 <div class="form-group col-md-12">
                                    <strong class="main-td col-md-4">3. Local sourcing of material</strong>
                                    <div class="col-md-8 toggle">	
                                       <label class="radio-inline"><input type="radio" name="question3" value="0" required><span class="label-text">0</span></label>
                                       <label class="radio-inline"><input type="radio" name="question3" value="1"><span class="label-text">1</span></label>
                                       <label class="radio-inline"><input type="radio" name="question3" value="2"><span class="label-text">2</span></label>
                                       <label class="radio-inline"><input type="radio" name="question3" value="3"><span class="label-text">3</span></label>
                                       <label class="radio-inline"><input type="radio" name="question3" value="4"><span class="label-text">4</span></label>
                                       <label class="radio-inline"><input type="radio" name="question3" value="5"><span class="label-text">5</span></label>
                                       <label class="radio-inline"><input type="radio" name="question3" value="6"><span class="label-text">6</span></label>
                                       <label class="radio-inline"><input type="radio" name="question3" value="7"><span class="label-text">7</span></label>
                                       <label class="radio-inline"><input type="radio" name="question3" value="8"><span class="label-text">8</span></label>
                                       <label class="radio-inline"><input type="radio" name="question3" value="9"><span class="label-text">9</span></label>
                                       <label class="radio-inline"><input type="radio" name="question3" value="10"><span class="label-text">10</span></label>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                 </div>
                                 <div class="form-group col-md-12">
                                    <strong class="main-td col-md-4">4. Local sales as percentage of total sales</strong>
                                    <div class="col-md-8 toggle">	
                                       <label class="radio-inline"><input type="radio" name="question4" value="0" required><span class="label-text">0</span></label>
                                       <label class="radio-inline"><input type="radio" name="question4" value="1"><span class="label-text">1</span></label>
                                       <label class="radio-inline"><input type="radio" name="question4" value="2"><span class="label-text">2</span></label>
                                       <label class="radio-inline"><input type="radio" name="question4" value="3"><span class="label-text">3</span></label>
                                       <label class="radio-inline"><input type="radio" name="question4" value="4"><span class="label-text">4</span></label>
                                       <label class="radio-inline"><input type="radio" name="question4" value="5"><span class="label-text">5</span></label>
                                       <label class="radio-inline"><input type="radio" name="question4" value="6"><span class="label-text">6</span></label>
                                       <label class="radio-inline"><input type="radio" name="question4" value="7"><span class="label-text">7</span></label>
                                       <label class="radio-inline"><input type="radio" name="question4" value="8"><span class="label-text">8</span></label>
                                       <label class="radio-inline"><input type="radio" name="question4" value="9"><span class="label-text">9</span></label>
                                       <label class="radio-inline"><input type="radio" name="question4" value="10"><span class="label-text">10</span></label>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                 </div>
                                 <div class="form-group col-md-12">
                                    <strong class="main-td col-md-4">5. Good governance/management practices</strong>
                                    <div class="col-md-8 toggle">	
                                       <label class="radio-inline"><input type="radio" name="question5" value="0" required><span class="label-text">0</span></label>
                                       <label class="radio-inline"><input type="radio" name="question5" value="1"><span class="label-text">1</span></label>
                                       <label class="radio-inline"><input type="radio" name="question5" value="2"><span class="label-text">2</span></label>
                                       <label class="radio-inline"><input type="radio" name="question5" value="3"><span class="label-text">3</span></label>
                                       <label class="radio-inline"><input type="radio" name="question5" value="4"><span class="label-text">4</span></label>
                                       <label class="radio-inline"><input type="radio" name="question5" value="5"><span class="label-text">5</span></label>
                                       <label class="radio-inline"><input type="radio" name="question5" value="6"><span class="label-text">6</span></label>
                                       <label class="radio-inline"><input type="radio" name="question5" value="7"><span class="label-text">7</span></label>
                                       <label class="radio-inline"><input type="radio" name="question5" value="8"><span class="label-text">8</span></label>
                                       <label class="radio-inline"><input type="radio" name="question5" value="9"><span class="label-text">9</span></label>
                                       <label class="radio-inline"><input type="radio" name="question5" value="10"><span class="label-text">10</span></label>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                 </div>
                                 <div class="form-group col-md-12">
                                    <strong class="main-td col-md-4">6. Simplicity of Technology used</strong>
                                    <div class="col-md-8 toggle">	
                                       <label class="radio-inline"><input type="radio" name="question6" value="0" required><span class="label-text">0</span></label>
                                       <label class="radio-inline"><input type="radio" name="question6" value="1"><span class="label-text">1</span></label>
                                       <label class="radio-inline"><input type="radio" name="question6" value="2"><span class="label-text">2</span></label>
                                       <label class="radio-inline"><input type="radio" name="question6" value="3"><span class="label-text">3</span></label>
                                       <label class="radio-inline"><input type="radio" name="question6" value="4"><span class="label-text">4</span></label>
                                       <label class="radio-inline"><input type="radio" name="question6" value="5"><span class="label-text">5</span></label>
                                       <label class="radio-inline"><input type="radio" name="question6" value="6"><span class="label-text">6</span></label>
                                       <label class="radio-inline"><input type="radio" name="question6" value="7"><span class="label-text">7</span></label>
                                       <label class="radio-inline"><input type="radio" name="question6" value="8"><span class="label-text">8</span></label>
                                       <label class="radio-inline"><input type="radio" name="question6" value="9"><span class="label-text">9</span></label>
                                       <label class="radio-inline"><input type="radio" name="question6" value="10"><span class="label-text">10</span></label>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                 </div>
                                 <div class="form-group col-md-12">
                                    <strong class="main-td col-md-4">7. Innovative technology/processes</strong>
                                    <div class="col-md-8 toggle">		
                                       <label class="radio-inline"><input type="radio" name="question7" value="0" required><span class="label-text">0</span></label>
                                       <label class="radio-inline"><input type="radio" name="question7" value="1"><span class="label-text">1</span></label>
                                       <label class="radio-inline"><input type="radio" name="question7" value="2"><span class="label-text">2</span></label>
                                       <label class="radio-inline"><input type="radio" name="question7" value="3"><span class="label-text">3</span></label>
                                       <label class="radio-inline"><input type="radio" name="question7" value="4"><span class="label-text">4</span></label>
                                       <label class="radio-inline"><input type="radio" name="question7" value="5"><span class="label-text">5</span></label>
                                       <label class="radio-inline"><input type="radio" name="question7" value="6"><span class="label-text">6</span></label>
                                       <label class="radio-inline"><input type="radio" name="question7" value="7"><span class="label-text">7</span></label>
                                       <label class="radio-inline"><input type="radio" name="question7" value="8"><span class="label-text">8</span></label>
                                       <label class="radio-inline"><input type="radio" name="question7" value="9"><span class="label-text">9</span></label>
                                       <label class="radio-inline"><input type="radio" name="question7" value="10"><span class="label-text">10</span></label>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                 </div>
                                 <div class="form-group col-md-12">
                                    <strong class="main-td col-md-4">8. Fair business practices</strong>
                                    <div class="col-md-8 toggle">		
                                       <label class="radio-inline"><input type="radio" name="question8" value="0" required><span class="label-text">0</span></label>
                                       <label class="radio-inline"><input type="radio" name="question8" value="1"><span class="label-text">1</span></label>
                                       <label class="radio-inline"><input type="radio" name="question8" value="2"><span class="label-text">2</span></label>
                                       <label class="radio-inline"><input type="radio" name="question8" value="3"><span class="label-text">3</span></label>
                                       <label class="radio-inline"><input type="radio" name="question8" value="4"><span class="label-text">4</span></label>
                                       <label class="radio-inline"><input type="radio" name="question8" value="5"><span class="label-text">5</span></label>
                                       <label class="radio-inline"><input type="radio" name="question8" value="6"><span class="label-text">6</span></label>
                                       <label class="radio-inline"><input type="radio" name="question8" value="7"><span class="label-text">7</span></label>
                                       <label class="radio-inline"><input type="radio" name="question8" value="8"><span class="label-text">8</span></label>
                                       <label class="radio-inline"><input type="radio" name="question8" value="9"><span class="label-text">9</span></label>
                                       <label class="radio-inline"><input type="radio" name="question8" value="10"><span class="label-text">10</span></label>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                 </div>
                                 <div class="form-group col-md-12">
                                    <strong class="main-td col-md-4">9. Equal opportunity to all</strong>
                                    <div class="col-md-8 toggle">		
                                       <label class="radio-inline"><input type="radio" name="question9" value="0" required><span class="label-text">0</span></label>
                                       <label class="radio-inline"><input type="radio" name="question9" value="1"><span class="label-text">1</span></label>
                                       <label class="radio-inline"><input type="radio" name="question9" value="2"><span class="label-text">2</span></label>
                                       <label class="radio-inline"><input type="radio" name="question9" value="3"><span class="label-text">3</span></label>
                                       <label class="radio-inline"><input type="radio" name="question9" value="4"><span class="label-text">4</span></label>
                                       <label class="radio-inline"><input type="radio" name="question9" value="5"><span class="label-text">5</span></label>
                                       <label class="radio-inline"><input type="radio" name="question9" value="6"><span class="label-text">6</span></label>
                                       <label class="radio-inline"><input type="radio" name="question9" value="7"><span class="label-text">7</span></label>
                                       <label class="radio-inline"><input type="radio" name="question9" value="8"><span class="label-text">8</span></label>
                                       <label class="radio-inline"><input type="radio" name="question9" value="9"><span class="label-text">9</span></label>
                                       <label class="radio-inline"><input type="radio" name="question9" value="10"><span class="label-text">10</span></label>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                 </div>
                                 <div class="form-group col-md-12">
                                    <strong class="main-td col-md-4">10. Percentage of employees from weaker sections</strong>
                                    <div class="col-md-8 toggle">		
                                       <label class="radio-inline"><input type="radio" name="question10" value="0" required><span class="label-text">0</span></label>
                                       <label class="radio-inline"><input type="radio" name="question10" value="1"><span class="label-text">1</span></label>
                                       <label class="radio-inline"><input type="radio" name="question10" value="2"><span class="label-text">2</span></label>
                                       <label class="radio-inline"><input type="radio" name="question10" value="3"><span class="label-text">3</span></label>
                                       <label class="radio-inline"><input type="radio" name="question10" value="4"><span class="label-text">4</span></label>
                                       <label class="radio-inline"><input type="radio" name="question10" value="5"><span class="label-text">5</span></label>
                                       <label class="radio-inline"><input type="radio" name="question10" value="6"><span class="label-text">6</span></label>
                                       <label class="radio-inline"><input type="radio" name="question10" value="7"><span class="label-text">7</span></label>
                                       <label class="radio-inline"><input type="radio" name="question10" value="8"><span class="label-text">8</span></label>
                                       <label class="radio-inline"><input type="radio" name="question10" value="9"><span class="label-text">9</span></label>
                                       <label class="radio-inline"><input type="radio" name="question10" value="10"><span class="label-text">10</span></label>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                 </div>
                                 <div class="form-group col-md-12">
                                    <strong class="main-td col-md-4">11. Fair labor practices- minimum wages, security etc.</strong>
                                    <div class="col-md-8 toggle">		
                                       <label class="radio-inline"><input type="radio" name="question11" value="0" required><span class="label-text">0</span></label>
                                       <label class="radio-inline"><input type="radio" name="question11" value="1"><span class="label-text">1</span></label>
                                       <label class="radio-inline"><input type="radio" name="question11" value="2"><span class="label-text">2</span></label>
                                       <label class="radio-inline"><input type="radio" name="question11" value="3"><span class="label-text">3</span></label>
                                       <label class="radio-inline"><input type="radio" name="question11" value="4"><span class="label-text">4</span></label>
                                       <label class="radio-inline"><input type="radio" name="question11" value="5"><span class="label-text">5</span></label>
                                       <label class="radio-inline"><input type="radio" name="question11" value="6"><span class="label-text">6</span></label>
                                       <label class="radio-inline"><input type="radio" name="question11" value="7"><span class="label-text">7</span></label>
                                       <label class="radio-inline"><input type="radio" name="question11" value="8"><span class="label-text">8</span></label>
                                       <label class="radio-inline"><input type="radio" name="question11" value="9"><span class="label-text">9</span></label>
                                       <label class="radio-inline"><input type="radio" name="question11" value="10"><span class="label-text">10</span></label>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                 </div>
                                 <div class="form-group col-md-12">
                                    <strong class="main-td col-md-4">12. Overall, how will you rate this enterprise vis-a-vis the enterprises in the same category</strong>
                                    <div class="col-md-8 toggle">		
                                       <label class="radio-inline"><input type="radio" name="question12" value="0" required><span class="label-text">0</span></label>
                                       <label class="radio-inline"><input type="radio" name="question12" value="1"><span class="label-text">1</span></label>
                                       <label class="radio-inline"><input type="radio" name="question12" value="2"><span class="label-text">2</span></label>
                                       <label class="radio-inline"><input type="radio" name="question12" value="3"><span class="label-text">3</span></label>
                                       <label class="radio-inline"><input type="radio" name="question12" value="4"><span class="label-text">4</span></label>
                                       <label class="radio-inline"><input type="radio" name="question12" value="5"><span class="label-text">5</span></label>
                                       <label class="radio-inline"><input type="radio" name="question12" value="6"><span class="label-text">6</span></label>
                                       <label class="radio-inline"><input type="radio" name="question12" value="7"><span class="label-text">7</span></label>
                                       <label class="radio-inline"><input type="radio" name="question12" value="8"><span class="label-text">8</span></label>
                                       <label class="radio-inline"><input type="radio" name="question12" value="9"><span class="label-text">9</span></label>
                                       <label class="radio-inline"><input type="radio" name="question12" value="10"><span class="label-text">10</span></label>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                 </div>
                                 <div class="give-padding"></div>
                                 <div class="col-md-12">
                                    <p style="font-size: 17px!important; padding-top: 55px;">Please provide the answers to the following questions to the best of your knowledge for the current financial year.</p>
                                 </div>
                                 <div class="form-group col-md-12">
                                    <strong class="main-td col-md-4">13. Average number of customers served by the enterprise per day?</strong>
                                    <div class="col-md-8 toggle">		
                                       <label class="radio-inline"><input type="radio" name="question13" value="0-10" required><span class="label-text">0-10</span></label>
                                       <label class="radio-inline"><input type="radio" name="question13" value="10-20"><span class="label-text">10-20</span></label>
                                       <label class="radio-inline"><input type="radio" name="question13" value="20-30"><span class="label-text">20-30</span></label>
                                       <label class="radio-inline"><input type="radio" name="question13" value="30-40"><span class="label-text">30-40</span></label>
                                       <label class="radio-inline"><input type="radio" name="question13" value="40-50"><span class="label-text">40-50</span></label>
                                       <label class="radio-inline"><input type="radio" name="question13" value="above 50"><span class="label-text">above 50</span></label>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                 </div>
                                 <div class="form-group col-md-12">
                                    <strong class="main-td col-md-4">14. Number of people employed in the current financial year?</strong>
                                    <div class="col-md-8 toggle">		
                                       <label class="radio-inline"><input type="radio" name="question14" value="1" required><span class="label-text">1</span></label>
                                       <label class="radio-inline"><input type="radio" name="question14" value="2"><span class="label-text">2</span></label>
                                       <label class="radio-inline"><input type="radio" name="question14" value="3"><span class="label-text">3</span></label>
                                       <label class="radio-inline"><input type="radio" name="question14" value="4"><span class="label-text">4</span></label>
                                       <label class="radio-inline"><input type="radio" name="question14" value="5"><span class="label-text">5</span></label>
                                       <label class="radio-inline"><input type="radio" name="question14" value="above 5"><span class="label-text">above 5</span></label>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                 </div>
                                 <div class="form-group col-md-12">
                                    <strong class="main-td col-md-4">15. Where is the market of the product/service offered by the enterprise?</strong>
                                    <div class="col-md-8 toggle">		
                                       <label class="radio-inline"><input type="checkbox" name="question15" value="Only local market"><span class="label-text">Only local market</span></label>
                                       <label class="radio-inline"><input type="checkbox" name="question15" value="within the district"><span class="label-text">within the district</span></label>
                                       <label class="radio-inline"><input type="checkbox" name="question15" value="few districts within the State"><span class="label-text">few districts within the State</span></label>
                                       <label class="radio-inline"><input type="checkbox" name="question15" value="Outside the State"><span class="label-text">Outside the State</span></label>
                                       <label class="radio-inline"><input type="checkbox" name="question15" value="In the Export Market"><span class="label-text">In the Export Market</span></label>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                 </div>
                                 <div class="form-group col-md-12">
                                    <strong class="main-td col-md-4">16. What is the geographical area coverage (in square/km) by the enterprise’s product/service?</strong>
                                    <div class="col-md-8">		
                                       <textarea class="form-control" rows="4" name="question16"  onblur="check(this);"></textarea>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                 </div>
                                 <div class="form-group col-md-12">
                                    <strong class="main-td col-md-4">17. Average sale per customer?</strong>
                                    <div class="col-md-8">		
                                       <input type="text" class="form-control" name="question17"/>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                 </div>
                                 <div class="form-group col-md-12">
                                    <strong class="main-td col-md-4">18. Describe the story and the significance of the enterprise as a model for the youth.</strong>
                                    <div class="col-md-8">		
                                       <textarea class="form-control" rows="4" name="question18" placeholder="Hint: about the start of the business, product/service offered, USP, operations etc." onblur="check(this);"></textarea>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                 </div>
                                 <div class="form-group">
                                    <h4>Declaration</h4>
                                 </div>
                                 <div class="form-group">
                                    <label class="checkbox-inline"><input type="checkbox" required >Initial investment of the enterprise is upto 1 lakh.</label>
                                    <div class="help-block with-errors"></div>
                                 </div>
                                 <div class="form-group">
                                    <label class="checkbox-inline"><input type="checkbox" required >I hereby declare that the details furnished above are true and correct to the best of my knowledge and belief. In case any of the above information is found to be
                                    false or untrue or misleading or misrepresenting, I am aware that the candidature of the nominee for the National Awards will be withdrawn immediately and the nominator
                                    will not be allowed to file any other nominations in the future.</label>
                                    <div class="help-block with-errors"></div>
                                 </div>
                                 <div class="form-group">
                                    <label class="checkbox-inline"><input type="checkbox" required >By participating in the National Entrepreneurship Awards 2018, the nominee and the nominator agree to receive mobile SMS, phone calls and email communication.</label>
                                    <div class="help-block with-errors"></div>
                                 </div>
                                 <!-- ...-->  
                              </div>
                              <!--end of class balance sheet-->
                              <div class="clearfix"></div>
                              <input type="hidden" name="nomineeEmail" value="${nominee.emailId}"/>
                              <input type="hidden" name="category" value="first"/>
                              <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                              <input type="hidden" name="subval" id="subval" value="partBOnly">
                              <div class="submit-btn align-center">
                              	 <input type="hidden" name="currentpath" value=""/>
                                 <a class="btn btn-warning" href="/basicdetails?p1=true&p2=${encryptedEmail}&p3=awardbaseform">Back</a>
                                 <span class="btn btn-info review-form">Review Form</span>
                                 <input class="btn btn-warning" id="secsave" type="button" value="Save As Draft">
                                 <input class="btn btn-primary" id="submitNomination" type="button" name="Submit" value="Submit Nomination">
                              </div>
                              <div class="clearfix"></div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </main>
      <c:if test="${(not empty errorMsg) or (not empty successMsg)}">
         <div class="modal fade" id="errorModal" role="dialog">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                     <c:if test="${ not empty errorMsg }">
                        <h4 class="modal-title">Error</h4>
                     </c:if>
                     <c:if test="${ not empty successMsg }">
                        <h4 class="modal-title">Success</h4>
                     </c:if>
                  </div>
                  <div class="modal-body">
                     <p>
                        <c:if test="${ not empty errorMsg }">
                           ${ errorMsg }
                        </c:if>
                        <c:if test="${ not empty successMsg }">
                           ${ successMsg }
                        </c:if>
                     </p>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                  </div>
               </div>
            </div>
         </div>
      </c:if>
      <!--review popup -->
      <div id="myModal" class="modal fade" role="dialog">
         <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Review Form</h4>
               </div>
               <div class="modal-body">
                <table class="table table-bordered" id="review-table-partA"></table>
                  <table class="table table-bordered" id="review-table">
                       
                        <tr>
                           <th colspan="2">PART B: Enterprise Details and Nominator's Assessment</th>
                        </tr>
                     
                     <tr>
                        <td>1. Growth in customer base for last four years</td>
                        <td class="field10"></td>
                     </tr>
                     <tr>
                        <td>2. Growth in new units/brances in the last four years</td>
                        <td class="field11"></td>
                     </tr>
                     <tr>
                        <td>3. Local sourcing of material</td>
                        <td class="field12"></td>
                     </tr>
                     <tr>
                        <td>4. Local sales as percentage of total sales</td>
                        <td class="field13"></td>
                     </tr>
                     <tr>
                        <td>5. Good governance/management practices</td>
                        <td class="field14"></td>
                     </tr>
                     <tr>
                        <td>6. Simplicity of Technology used</td>
                        <td class="field15"></td>
                     </tr>
                     <tr>
                        <td>7. Innovative technology/processes</td>
                        <td class="field16"></td>
                     </tr>
                     <tr>
                        <td>8. Fair business practices</td>
                        <td class="field17"></td>
                     </tr>
                     <tr>
                        <td>9. Equal opportunity to all</td>
                        <td class="field18"></td>
                     </tr>
                     <tr>
                        <td>10. Percentage of employees from weaker sections</td>
                        <td class="field19"></td>
                     </tr>
                     <tr>
                        <td>11. Fair labor practices- minimum wages, security etc.</td>
                        <td class="field20"></td>
                     </tr>
                     <tr>
                        <td>12. Overall, how will you rate this enterprise vis-a-vis the enterprises in the same category</td>
                        <td class="field21"></td>
                     </tr>
                     <tr>
                        <td>13. Average number of customers served by the enterprise per day?</td>
                        <td class="field22"></td>
                     </tr>
                     <tr>
                        <td>14. Number of people employed in the current financial year?</td>
                        <td class="field23"></td>
                     </tr>
                     <tr>
                        <td>15. Where is the market of the product/service offered by the enterprise?</td>
                        <td class="field24"></td>
                     </tr>
                     <tr>
                        <td>16. What is the geographical area coverage (in square/km) by the enterprise’s product/service?</td>
                        <td class="field25"></td>
                     </tr>
                     <tr>
                        <td>17. Average sale per customer?</td>
                        <td class="field26"></td>
                     </tr>
                     <tr>
                        <td>18. Describe the story and the significance of the enterprise as a model for the youth.</td>
                        <td class="field27"></td>
                     </tr>
                  </table>
               </div>
               <div class="modal-footer align-center">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </div>
         </div>
      </div>
      <!--review popup -->
      <!-- Body Ends Here -->
      <%@include file="fragments/footer.jsp" %>
      <script>

      $('#secsave').on('click',function(){
   		//localStorage.setItem("firstcatform",$('#rename-sheet').serialize());
   		var userData = JSON.parse(localStorage.getItem("awardbaseformData"))
   		userData["partB"] = $('#rename-sheet').serialize()
   		localStorage.setItem("awardbaseformData",JSON.stringify(userData))
   		alert("Saved..");
   	});
               
         $('#submitNomination').on('click',function(e) {
        	 
        	 
        	 var validator = $("#rename-sheet").data("bs.validator");
             validator.validate();

             if (!validator.hasErrors()) {
            	 if (confirm('Have you reviewed your data and want to do final submission?')) {
            	 
            		//e.preventDefault();
              	console.log("form halt");
              	var userData = JSON.parse(localStorage.getItem("awardbaseformData"))
          		userData["status"] = "submit";
              	userData["partB"] = $('#rename-sheet').serialize()
              	//userData["partA"] = userData[""]
          		localStorage.setItem("awardbaseformData",JSON.stringify(userData));
              	
              	$.ajax({
              		url:'/anotherbasicdetail',
              		type:'POST',
              		data:userData["partA"]+'&'+userData["partB"]+'&status='+userData["status"],
              		success:function(data){
              			window.location.href="/nominatorlanding";
              			var myItem = localStorage.getItem('popup');
                		 localStorage.clear();
                		 localStorage.setItem('popup',myItem);
              		},
              		error:function(a,b,c){
              			alert("something went wrong. data not saved.")
              			console.log(a,b,c)
              		}
              	})
                	
             }
             }
			    //return true; 
         });
                
         $('.review-form').on('click',function()
    	          {
       	 console.log("review form")
       	 var pairs, i, keyValuePair, key, value, map = {};
            var query = JSON.parse(localStorage.getItem("awardbaseformData"))["partA"];
            pairs = query.split('&');
            var tabledata ="<tr><th colspan='2'>PART A: Nominee's Basic Details</th></tr>";
            for (i = 0; i < pairs.length; i += 1) {
          keyValuePair = pairs[i].split('=');
          key = decodeURIComponent(keyValuePair[0]);
          value = (keyValuePair.length > 1) ? decodeURIComponent(keyValuePair[1]) : '';
          value.replace('%20',' ');
         
           if(key === 'awardCategory' && value === 'enterprise'){
           tabledata += "<tr><td>Award Track</td><td>Enterprise Award Track</td></tr>";
           }
           if(key === 'awardCategory' && value === 'ecosystem'){
           tabledata += "<tr><td>Award Track</td><td>Ecosystem Builders Track</td></tr>";
           }
           if(key === 'enterpriseCategory' && value === 'onelac'){
        	   tabledata += '<tr><td>Award Category</td><td>Initial Investment upto 1 Lakh</td></tr>';
        	   if(key === 'awardsub1'){
             	  tabledata += '<tr><td>Award Sub Category</td><td>'+value+'</td></tr>';
               }
          }
          if(key === 'enterpriseCategory' && value === 'uptoten'){
        	  tabledata += '<tr><td>Award Category</td><td>Initial Investment between 1 Lakh to 10 Lakh</td></tr>';
        	  if(key === 'awardsub2'){
            	  tabledata += '<tr><td>Award Sub Category</td><td>'+value+'</td></tr>';
              }
          }
          if(key === 'enterpriseCategory' && value === 'uptocrore'){
        	  tabledata += '<tr><td>Award Category</td><td>Initial Investment between 10 Lakh to 1 Crore</td></tr>';
        	  if(key === 'awardsub3'){
            	  tabledata += '<tr><td>Award Sub Category</td><td>'+value+'</td></tr>';
              }
          }
          
          if(key === 'specifyproduct'){
        	  tabledata += '<tr><td>Please list about the product/service you offer</td><td>'+value+'</td></tr>';
          }
          if(key === 'nomineeName'){
        	  tabledata += '<tr><td>Nominee/Entrepreneur’s Name</td><td>'+value+'</td></tr>';
          }
     	 if(key === 'dob'){
     		tabledata += '<tr><td>Date of Birth of the Nominee/Entrepreneur</td><td>'+value+'</td></tr>';
          }
     	 }
          if(key === 'gender'){
        	  tabledata += '<tr><td>Gender</td><td>'+value+'</td></tr>';
          }
     	 if(key === 'socialCategory'){
     		tabledata += '<tr><td>Social Category</td><td>'+value+'</td></tr>';
          }
     	 if(key === 'pwd'){
     		tabledata += '<tr><td>Person with Disability (PwD)</td><td>'+value+'</td></tr>';
          }
     	 if(key === 'speccatopt'){
     		tabledata += '<tr><td>Do you wish to nominate the candidate in the Special Category awards?</td><td>'+value+'</td></tr>';
          }
     	 
     	 if(key === 'enterpriseName'){
     		tabledata += '<tr><td>Name of the Enterprise</td><td>'+value+'</td></tr>';
          }
     	 
     	 if(key === 'address1'){
     		tabledata += '<tr><td>House No.</td><td>'+value+'</td></tr>';
          }
     	 
     	 if(key === 'street'){
     		tabledata += '<tr><td>Mohalla/Village</td><td>'+value+'</td></tr>';
          }
     	 
     	 if(key === 'address2'){
     		tabledata += '<tr><td>Street Name</td><td>'+value+'</td></tr>';
          }
     	 
     	 if(key === 'city'){
     		tabledata += '<tr><td>City</td><td>'+value+'</td></tr>';
          }
     	 
     	 if(key === 'state'){
     		tabledata += '<tr><td>State/td><td>'+value+'</td></tr>';
          }
     	 
     	 
     	 if(key === 'landmark'){
     		tabledata += '<tr><td>Landmark</td><td>'+value+'</td></tr>';
          }
     	 
     	 if(key === 'pinCode'){
     		tabledata += '<tr><td>Pin Code</td><td>'+value+'</td></tr>';
          }
     	 
     	 if(key === 'website'){
     		tabledata += '<tr><td>Website(If available)</td><td>'+value+'</td></tr>';
          }
     	 
     	 
     	  if(key === 'emailId'){
     		 tabledata += '<tr><td>Email Id</td><td>'+value+'</td></tr>';
          }
     	 
     	 if(key === 'contactNumber'){
     		tabledata += '<tr><td>Contact Number</td><td>'+value+'</td></tr>';
          }
     	 
     	 if(key === 'knowNominee'){
     		tabledata += '<tr><td>How do you know your Nominee?</td><td>'+value+'</td></tr>';
          }
     	 
     	 
     	  if(key === 'otherNom'){
     		 tabledata += '<tr><td>Other nominee</td><td>'+value+'</td></tr>';
          }
     	 
     	  
     	 
          if(key === 'date'){
        	  tabledata += '<tr><td>Date of Birth of the Nominee/Entrepreneur</td><td>'+value+'</td></tr>';
          }
     	 
                   
                    $('#review-table-partA').html(tabledata);
                    
    	              $('.field10').html($('[name=question1]:checked').val());
    	              $('.field11').html($('[name=question2]:checked').val());
    	              $('.field12').html($('[name=question3]:checked').val());
    	              $('.field13').html($('[name=question4]:checked').val());
    	              $('.field14').html($('[name=question5]:checked').val());
    	              $('.field15').html($('[name=question6]:checked').val());
    	              $('.field16').html($('[name=question7]:checked').val());
    	              $('.field17').html($('[name=question8]:checked').val());
    	              $('.field18').html($('[name=question9]:checked').val());
    	              $('.field19').html($('[name=question10]:checked').val());
    	              $('.field20').html($('[name=question11]:checked').val());
    	              $('.field21').html($('[name=question12]:checked').val());
    	              $('.field22').html($('[name=question13]:checked').val());
    	              $('.field23').html($('[name=question14]:checked').val());
    	              var checkedArr =[];
    	              $('[name=question15]').each(function(){
    	            	  if($(this).prop('checked')){
    	            		  checkedArr.push($(this).val())
    	            	  }
    	            	  
    	              })
    	              $('.field24').html(checkedArr.join(','));
    	              $('.field25').html($('[name=question16]').val());
    	              $('.field26').html($('[name=question17]').val());
    	              $('.field27').html($('[name=question18]').val());
    	              
    	              
    	             $('#myModal').modal('show');
    	     });
         $(document).ready(function() {
        	// $('.toggle input').checkboxradio();
        //		$(".toggle").controlgroup();
        		
         	if($('#errorModal').length > 0){
         		$('#errorModal').modal('show');
         	}
         	
        /*  $("#rename-sheet").submit(function(){
        	 var myItem = localStorage.getItem('popup');
    		 localStorage.clear();
    		 localStorage.setItem('popup',myItem);
         	return;
         }); */
         
         if(localStorage.getItem("awardbaseformData")!=null){
            	var pairs, i, keyValuePair, key, value, map = {};
            	var data = JSON.parse(localStorage.getItem("awardbaseformData"))
            	if (typeof(data["partB"]) == 'undefined'){
            		return false;
            	}
            	var query = data["partB"];
            	pairs = query.split('&');
            	        for (i = 0; i < pairs.length; i += 1) {
            	            keyValuePair = pairs[i].split('=');
            	            key = decodeURIComponent(keyValuePair[0]);
            	            value = (keyValuePair.length > 1) ? decodeURIComponent(keyValuePair[1]) : '';
            	            if(key==='question16' || key==='question17' || key==='question18'){
            	            	$('#rename-sheet [name="'+key+'"]').val(value);
            	            }else{
            	            	$('#rename-sheet input[name="'+key+'"][value="'+value+'"]').prop("checked",true);
            	            }
            	        }
            	}
         
         
       
        });
         $(document).ready(function() {
  			$("#rename-sheet").on("submit", function(e) {
  				$(window).off("beforeunload");
  				return true;
  			}); 
  		});
          
          $(window).on("beforeunload", function() {
  			return "You didn't save your form. All the data will be lost if you leave this page. Click Save as draft to save your data.";
  		});
          
          $("#submitNomination").on("click", function(e) {
				$(window).off("beforeunload");
				return true;
			});
          
         
      </script>
   </body>
</html>