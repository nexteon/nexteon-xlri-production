<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<%@include file="fragments/headerlibs.jsp" %>
<body class="innerpage">

<%@include file="fragments/header.jsp" %>
<!-- Body Starts Here -->

<main role="main">
	<div class="page-title">
		<h2>Forgot Password</h2>
	</div>
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="whitebg">
						<div id="form1" class="cst-form">
                                <div class="form-header">
                                    <h4>Enter New Password</h4>
                                </div>
                                <div class="form-body">
                                    <form action="/changepassword/${registration}" method="post" data-toggle="validator" role="form">
                                        <div class="row">
	                                        <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="password1">Password<a href="#" data-toggle="tooltip" data-placement="right" title="An 8 character long password reqiured that must contain a small letter, a capital letter, a special character and a digit."><i class="fa fa-info-circle"></i></a></label>
                                                    <input type="password" name="password" id="password1" class="form-control" placeholder="Password" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&#]{8,}$" required data-required-error="Mandatory Field"  data-pattern-error="A minimum 8 character long password reqiured that must contain a small letter, a capital letter and a digit.">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="confpass1">Confirm Password</label>
                                                    <input type="password" name="confirmPass" id="confpass1" data-match="#password1" class="form-control" placeholder="Confirm Password" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&#]{8,}$" required data-required-error="Mandatory Field"  data-pattern-error="A minimum 8 character long password reqiured that must contain a small letter, a capital letter and a digit.">
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="form-group">
                                                	<input type="hidden" name="currentpath" value=""/>
                                                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	          										<input type="submit" class="btn btn-primary" value="Proceed"/>
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="form-group">
	          										<a class="btn btn-default" href="/login">Cancel</a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                         </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<c:if test="${not empty errorMsg}">
  <div class="modal fade" id="errorModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Error</h4>
        </div>
        <div class="modal-body">
          <p>${errorMsg}</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
</c:if>

<!-- Body Ends Here -->
<%@include file="fragments/footer.jsp" %>
	<script>
		$(document).ready(function() {
			$('[data-toggle="tooltip"]').tooltip();
			if($('#errorModal').length > 0){
				$('#errorModal').modal('show');
			}
		});
	</script>
</body>
</html>