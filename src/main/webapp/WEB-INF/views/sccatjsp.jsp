<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">
   <%@include file="dashboard/headerlibs.jsp" %>
   <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white" id="page-top" ng-app="myApp" ng-controller="jsonCtrl">
   			<div class="page-wrapper">
          <%@include file="dashboard/header.jsp" %>
           


                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="/userdata">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>ApplcantCategory</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> SC/ST Entrepreneur Category</h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <!-- BEGIN DASHBOARD STATS 1-->
                        
                        <input type="hidden" name="subCategory" value="SC/ST Category"/>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        
                        
                               <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="icon-settings font-dark"></i>
                                            <span class="caption-subject bold uppercase"> SC/ST Category Data</span>
                                        </div>
                                        
                                    </div>
                                    <div class="portlet-body">
                                        
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1_3">
                                            <thead>
                         <tr>
                       		  <th>ID</th>
                              <th>Application ID</th>
                              <th>Award Category</th>
                              <th>Nominee Name</th>
                              <th>Social Category</th>
                              <th>Email Id</th>
                              <th>Enterprise Name</th>
                       </tr>
                     </thead>
                                            
                     <tbody>
                     	 <tr ng-repeat="data in tableDataNew">
               				<td id="data.id">{{ data.id }}</td>
                              <td >{{ data.contact }}</td>
                              <td>{{ data.awardCategory }}</td>
                              <td>{{ data.nomineeName}}</td>
                              <td >{{ data.socialCategory}}</td>
                              <td>{{ data.emailId}}</td>
                              <td>{{ data.enterpriseName}}</td>
               </tr>
               
               </tbody>
             
                                             </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>
                        
                        
                        
                        </div>
                        </div>
                        </div>
                        
                        
                       <!-- END QUICK NAV -->
     <%@include file="dashboard/footer.jsp"%>
     
     
     
<script>
          var app = angular.module('myApp', []);
          app.controller('jsonCtrl', function($scope, $http) {
        	  $http({url: "/special/category", method: 'POST', data: {"subcat":"SC/ST Category"}})
              .then(function(response) {
              	$scope.tableDataNew = response.data;
              });
          });
</script>
     
     
     </body>
         </html>    