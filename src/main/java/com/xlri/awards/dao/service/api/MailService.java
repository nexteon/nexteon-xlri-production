package com.xlri.awards.dao.service.api;

import com.xlri.awards.dao.entity.Evaluation;
import com.xlri.awards.dao.entity.Expert;
import com.xlri.awards.dao.entity.Nominator;
import com.xlri.awards.dao.entity.NomineeDetails;
import com.xlri.awards.dao.entity.Partners;
import com.xlri.awards.dao.entity.User;

public interface MailService {

	public void sendOtp(Nominator user);
	public void sendNomineeMail(NomineeDetails nominee,String password);
	public boolean changePasswordMail(User user);
	public void sendNominatorMail(NomineeDetails nominatorUser);
	//boolean sendPasswordToExpertMail(Expert expert);
	//boolean sendPasswordToPartnersMail(Partners partners);
	boolean sendPasswordToPartnersMail(Partners partners, String password);
	boolean sendPasswordToExpertMail(Expert expert, String password);
	public void sendNomineeSupportMail(NomineeDetails nominee);
	public void sendNominatorSupportMail(Nominator nominator, NomineeDetails nominee);
	public void sendRemindSupportNominator(String mailTo, String subject, String msg, Nominator nominator);
	public void sendSingleAssignExpertMail(Evaluation eval);
}
