<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">
<%@include file="dashboard/headerlibs.jsp"%>
<body
	class="page-header-fixed page-sidebar-closed-hide-logo page-content-white"
	id="page-top">
	<div class="page-wrapper">
		<%@include file="dashboard/header.jsp"%>


                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="/userdata">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Partners</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <!-- BEGIN DASHBOARD STATS 1-->

                        <br>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="regionalUsers">Regional/Partners: <span class="mandate">*</span></label>
                                <select class="form-control" name="regionaluser" id="regionaluser" >
                                    <option value="" selected disabled>Select</option>
                                    <option value="AN,JH">XLRI, Jamshedpur</option>
                                    <option value="AD,TS">MANAGE, Hyderabad</option>
                                    <option value="AR,AS,MN,ML,MZ,NL,SK,TR">IITG, Guwahati</option>
                                    <option value="BR">NABARD, Mumbai</option>
                                    <option value="CH,DL,HR,JK,PB">IITD, New Delhi</option>
                                    <option value="CG,WB">TISS, Mumbai</option>
                                    <option value="DN,DD,GJ,RJ">IRMA, Anand</option>
                                    <option value="GA,MH">IITB, Mumbai</option>
                                    <option value="HP,OD,UK">NIF, Ahmedabad</option>
                                    <option value="KA,MP">RSETI, Bangalore</option>
                                    <option value="KL,LD,PY,TN">IITM, Chennai</option>
                                    <option value="UP">IITK, Kanpur</option>
                                </select>
                            </div>
                        </div>
                        
                        
                        <div class="portlet-body" style="background: #fff;"> 
                                         <div class="table-responsive">
                                        <div class="total_record22">Total Count:
                   						 <div id="filter-count22"></div>
               							 </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable" id="example11" class="display">
                                            <thead>
                           <tr>
                              <th>ID</th>
                              <th>View</th>
                              <th>Registration Number</th>
                              <th>Nominee Name</th>
                              <th>Email Id</th>
                              <th>Contact Number</th>
                              <th>Award Category</th>
                              <th>Enterprise Category</th>
                              <th>Award Sub Category</th>
                              <th>Gender</th>
                              <th>Social Category</th>
                              <th>DOB</th>
                              <th>Special Category</th>
                              <th>Enterprise Name</th>
                              <th>Address1</th>
                              <th>Address2</th>
                              <th>City</th>
                              <th>State</th>
                                                   
                           </tr>
                        </thead>
                      
                         
                         	 <tfoot>
                                <tr id="filters22">
                             <th>ID</th>
                             <th></th>
                              <th>Registration Number</th>
                              <th>Nominee Name</th>
                              <th>Email Id</th>
                              <th>Contact Number</th>
                              <th>Award Category</th>
                              <th>Enterprise Category</th>
                              <th>Award Sub Category</th>
                              <th>Gender</th>
                              <th>Social Category</th>
                              <th>DOB</th>
                              <th>Special Category</th>
                              <th>Enterprise Name</th>
                              <th>Address1</th>
                              <th>Address2</th>
                              <th>City</th>
                              <th>State</th>
                                 </tr>
                              </tfoot>
                                             </table>
                                    </div>
                                 </div>

         
         </div>
         </div>
         </div>
         
         
         
        <!-- END QUICK NAV -->
     <%@include file="dashboard/footer.jsp"%>
    <%@include file="dashboard/datatablescript.jsp" %>
     <script>
     $(document).ready(function() {
     $("#regionaluser").change(function(){
         var r = document.getElementById("regionaluser");
         var regn = r.options[r.selectedIndex].value;
         //$('#example11').clear();
         $('#example11').dataTable().fnDestroy();
         $('#example11 tfoot select').remove();

         $('#example11').DataTable( {
        	  "ajax": {
        	  "url": "/neas/api/regionalnominee?s="+regn,
        	  "type" : 'POST',
        	"scrollX": true,
        	  "dataSrc": ""
        	  },
        	  "columns": [
        	  { "data": "id" },
        	  { "data": "view" },
        	  { "data": "registrationnumber" },
        	  { "data": "nomineename" },
        	  { "data": "emailid" },
        	  { "data": "contactnumber" },
        	  { "data": "awardcategory" },
        	  { "data": "enterprisecategory" },
        	  { "data": "awardsub" },
        	  { "data": "gender" },
        	  { "data": "socialcategory" },
        	  { "data": "dob" },
        	  { "data": "speccatval" },
       	  { "data": "enterprisename" },
       	  { "data": "address1" },
       	  { "data": "address2" },
       	  { "data": "city" },
       	  { "data": "state" },
        	  ],dom: 'Bfrtip',
            buttons: [
                      'copyHtml5',
                      'excelHtml5',
                      'csvHtml5',
                      'pdfHtml5'
                  ], initComplete: function () {
        	  this.api().columns().every(function () {
        	  var column = this;

        	 
        	  var select = $('<select><option value=""></option></select>')
        	  .appendTo($("#filters22").find("th").eq(column.index()))
        	  .on('change', function () {
        	  var val = $.fn.dataTable.util.escapeRegex(
        	  $(this).val()); 

        	  column.search(val ? '^' + val + '$' : '', true, false)
        	  .draw();
        	  }); 
        	  //console.log(select);
        	  column.data().unique().sort().each(function (d, j) {
        	  select.append('<option value="' + d + '">' + d + '</option>')
        	  });
        	  });
        	  }
        	  } );
         });

     $.fn.dataTable.ext.errMode = 'none';

     $('#example11').on( 'error.dt', function ( e, settings, techNote, message ) {
     console.log( 'An error has been reported by DataTables: ', message );
     } ) .DataTable();
     $('#example11').DataTable();
     
     });
         


    </script>
     </body>
 </html>
                    
