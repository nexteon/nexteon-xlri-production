package com.xlri.awards.dao.service.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlri.awards.dao.entity.Expert;
import com.xlri.awards.dao.service.api.ExpertService;

@Repository
@Transactional
public class ExpertServiceImpl implements ExpertService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExpertServiceImpl.class);

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public boolean saveExpert(Expert expert) {
		try {

			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			session.save(expert);
			session.getTransaction().commit();
			return true;
		}

		catch (Exception e) {
			LOGGER.error(e.getMessage());
			return false;

		}

	}

	@Override
	public boolean updateExpert(Expert expert) {
		try {

			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			session.update(expert);
			session.getTransaction().commit();
			return true;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return false;
		}

	}

	@Override
	public List<Expert> getAllExpert() {
		List<Expert> expertList = null;
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			expertList = session.createQuery("From Expert").getResultList();
			session.getTransaction().commit();
			return expertList;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public Expert getExpertByEmail(String emailId) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<Expert> query = session.createQuery("SELECT e FROM Expert e WHERE e.emailId=:emailId");
			query.setParameter("emailId", emailId);
			session.getTransaction().commit();

			return query.getSingleResult();

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public Expert getExpertById(String id) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<Expert> query =  session.createQuery("SELECT e FROM Expert e WHERE e.id='"+id+"'");
			session.getTransaction().commit();
			return query.getSingleResult();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}
	
	@Override
	public List<Expert> getExpertCountCheck(String emailId) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<Expert> query =  session.createQuery("SELECT e FROM Expert e where e.partnerWith=:emailId");
			query.setParameter("emailId", emailId);
			session.getTransaction().commit();
			return query.getResultList();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

}
