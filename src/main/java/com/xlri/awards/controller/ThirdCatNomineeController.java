package com.xlri.awards.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.xlri.awards.common.EncryptionUtil;
import com.xlri.awards.common.OtpSalt;
import com.xlri.awards.dao.entity.NomineeDetails;
import com.xlri.awards.dao.entity.SecondCatNominee;
import com.xlri.awards.dao.entity.ThirdCatNominee;
import com.xlri.awards.dao.service.api.NominatorService;
import com.xlri.awards.dao.service.api.NomineeService;
import com.xlri.awards.dao.service.api.ThirdCatNomineeService;

@Controller
@PropertySource("classpath:application.properties")
public class ThirdCatNomineeController {

	@Autowired
	Environment env;
	
	@Autowired
	NomineeService nomineeService;

	@Autowired
	NominatorService nominatorService;

	@Autowired
	ThirdCatNomineeService thirdNomineeService;

	public static final Logger LOGGER = LoggerFactory.getLogger(ThirdCatNomineeController.class);


	@GetMapping(value = "/thirdcatreg/{encryptedID}")
	public ModelAndView getSecondCatNomineePage(@PathVariable String encryptedID) {
		String emailId = EncryptionUtil.decrypt(encryptedID, OtpSalt.SALT.toString());
		ModelAndView model = new ModelAndView();
		NomineeDetails nominee = nomineeService.getNomineeByEmailId(emailId);
		if (nominee != null && nominee.getEnterpriseCategory().equals("uptocrore")) {
			model.addObject("nominee", nominee);
			model.addObject("formNumber",getNewFormNumber(nominee));
			model.addObject("nominatorName",nominatorService.getNominatorByRegistrationNumber(nominee.getRegistrationNumber()));
			model.addObject("encryptedContact", EncryptionUtil.encrypt(nominee.getContact(), OtpSalt.SALT.toString()));
			model.setViewName("thirdcatnominee");
		} else {
			model.setViewName("index");
		}
		return model;
	}

	@RequestMapping(value = "/thirdcategory", method = RequestMethod.GET)
	public ModelAndView viewNominee(HttpServletRequest request) {
		ModelAndView model = new ModelAndView("thirdcatnominee");
		return model;
	}

	@RequestMapping(value = "/thirdnominee/{encryptedContact}", method = RequestMethod.POST)
	public ModelAndView registerThirdNominee(@PathVariable("encryptedContact") String encryptedContact,
			HttpServletRequest request, HttpServletResponse response,@RequestParam("file1") MultipartFile[] balanceSheet,
			@RequestParam("file2") MultipartFile[] profitLoss,@RequestParam("file3") MultipartFile[] cashFlow,
			@RequestParam("file4") MultipartFile[] moaFiles,@RequestParam("file5") MultipartFile[] aoaFiles,
			@RequestParam("file6") MultipartFile[] indCert,@RequestParam("file7") MultipartFile[] orgFiles)
			throws Exception {

		ModelAndView model = new ModelAndView("redirect:/nomineelanding");
		String contact = EncryptionUtil.decrypt(encryptedContact, OtpSalt.SALT.toString());
		NomineeDetails nominee = nomineeService.getNomineeByContact(contact);
		String UPLOAD_FILEPATH = env.getRequiredProperty("static.content.directory");
		
		if (nominee != null) {

			ThirdCatNominee nomineeObj = new ThirdCatNominee();
			try {
				nomineeObj.setContact(contact);
				nomineeObj.setEnterpriseDate(request.getParameter("enterpriseDate"));
				nomineeObj.setFormNumber(request.getParameter("formNumber"));
				/*String foundingMember;
				JSONObject jsonFounding = new JSONObject();
				jsonFounding.put("foundingName", request.getParameter("foundingName"));
				jsonFounding.put("foundingDesignation", request.getParameter("foundingDesignation"));
				foundingMember = jsonFounding.toString();*/
				nomineeObj.setFoundingMember(request.getParameter("foundingJson"));

				/*String otherPromoter;
				JSONObject jsonOtherProm = new JSONObject();
				jsonOtherProm.put("otherName", request.getParameter("otherName"));
				jsonOtherProm.put("otherDesignation", request.getParameter("otherDesignation"));
				otherPromoter = jsonOtherProm.toString();*/
				nomineeObj.setOtherPromoter(request.getParameter("promotorJson"));
				
				if (moaFiles[0].getSize() >0) {
					JSONObject jsonObject = new JSONObject();
					for (int i = 0; i < moaFiles.length; i++) {
						if(moaFiles[0].getSize() > 5242880){
							model.addObject("errorMsg", "File size exceeds 5 MB");
							return model;
						}
						StringBuilder filePath = new StringBuilder(
								UPLOAD_FILEPATH + File.separator + contact + File.separator + "MoA" + File.separator + i);
						File file = new File(filePath.toString());
						if (!file.exists()) {
							file.mkdirs();
						}
						String originalName = moaFiles[i].getOriginalFilename();
						File actFile = new File(filePath.append(File.separator + originalName).toString());
						Files.copy(moaFiles[i].getInputStream(), actFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
						jsonObject.put("path" + i, "/static/docs/"+contact+"/MoA/"+i+"/"+originalName);
					}
					nomineeObj.setMoaDoc(jsonObject.toString());
				}
				if (aoaFiles[0].getSize() >0) {
					JSONObject jsonObject = new JSONObject();
					for (int i = 0; i < aoaFiles.length; i++) {
						if(aoaFiles[0].getSize() > 5242880){
							model.addObject("errorMsg", "File size exceeds 5 MB");
							return model;
						}
						StringBuilder filePath = new StringBuilder(
								UPLOAD_FILEPATH + File.separator + contact + File.separator + "AoA" + File.separator + i);
						File file = new File(filePath.toString());
						if (!file.exists()) {
							file.mkdirs();
						}
						String originalName = aoaFiles[i].getOriginalFilename();
						File actFile = new File(filePath.append(File.separator + originalName).toString());
						Files.copy(aoaFiles[i].getInputStream(), actFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
						jsonObject.put("path" + i, "/static/docs/"+contact+"/AoA/"+i+"/"+originalName);
					}
					nomineeObj.setAoaDoc(jsonObject.toString());
				}
				if (balanceSheet[0].getSize() >0) {
					JSONObject jsonObject = new JSONObject();
					for (int i = 0; i < balanceSheet.length; i++) {
						if(balanceSheet[0].getSize() > 5242880){
							model.addObject("errorMsg", "File size exceeds 5 MB");
							return model;
						}
						StringBuilder filePath = new StringBuilder(
								UPLOAD_FILEPATH + File.separator + contact + File.separator + "BalanceSheet" + File.separator + i);
						File file = new File(filePath.toString());
						if (!file.exists()) {
							file.mkdirs();
						}
						String originalName = balanceSheet[i].getOriginalFilename();
						File actFile = new File(filePath.append(File.separator + originalName).toString());
						Files.copy(balanceSheet[i].getInputStream(), actFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
						jsonObject.put("path" + i, "/static/docs/"+contact+"/BalanceSheet/"+i+"/"+originalName);
					}
					nomineeObj.setBalanceSheet(jsonObject.toString());
				}
				if (profitLoss[0].getSize() >0) {
					JSONObject jsonObject = new JSONObject();
					for (int i = 0; i < profitLoss.length; i++) {
						if(profitLoss[0].getSize() > 5242880){
							model.addObject("errorMsg", "File size exceeds 5 MB");
							return model;
						}
						StringBuilder filePath = new StringBuilder(
								UPLOAD_FILEPATH + File.separator + contact + File.separator + "ProfitLoss" + File.separator + i);
						File file = new File(filePath.toString());
						if (!file.exists()) {
							file.mkdirs();
						}
						String originalName = profitLoss[i].getOriginalFilename();
						File actFile = new File(filePath.append(File.separator + originalName).toString());
						Files.copy(profitLoss[i].getInputStream(), actFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
						jsonObject.put("path" + i, "/static/docs/"+contact+"/ProfitLoss/"+i+"/"+originalName);
					}
					nomineeObj.setProfitLossStatement(jsonObject.toString());
				}
				if (cashFlow[0].getSize() >0) {
					JSONObject jsonObject = new JSONObject();
					for (int i = 0; i < cashFlow.length; i++) {
						if(cashFlow[0].getSize() > 5242880){
							model.addObject("errorMsg", "File size exceeds 5 MB");
							return model;
						}
						StringBuilder filePath = new StringBuilder(
								UPLOAD_FILEPATH + File.separator + contact + File.separator + "CashFlow" + File.separator + i);
						File file = new File(filePath.toString());
						if (!file.exists()) {
							file.mkdirs();
						}
						String originalName = cashFlow[i].getOriginalFilename();
						File actFile = new File(filePath.append(File.separator + originalName).toString());
						Files.copy(cashFlow[i].getInputStream(), actFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
						jsonObject.put("path" + i, "/static/docs/"+contact+"/CashFlow/"+i+"/"+originalName);
					}
					nomineeObj.setCashFlowStatement(jsonObject.toString());
				}
				if (indCert[0].getSize() >0) {
					JSONObject jsonObject = new JSONObject();
					for (int i = 0; i < indCert.length; i++) {
						if(indCert[0].getSize() > 5242880){
							model.addObject("errorMsg", "File size exceeds 5 MB");
							return model;
						}
						StringBuilder filePath = new StringBuilder(
								UPLOAD_FILEPATH + File.separator + contact + File.separator + "IndCert" + File.separator + i);
						File file = new File(filePath.toString());
						if (!file.exists()) {
							file.mkdirs();
						}
						String originalName = indCert[i].getOriginalFilename();
						File actFile = new File(filePath.append(File.separator + originalName).toString());
						Files.copy(indCert[i].getInputStream(), actFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
						jsonObject.put("path" + i, "/static/docs/"+contact+"/IndCert/"+i+"/"+originalName);
					}
					nomineeObj.setIndustryCert(jsonObject.toString());
				}
				if (orgFiles[0].getSize() >0) {
					JSONObject jsonObject = new JSONObject();
					for (int i = 0; i < orgFiles.length; i++) {
						if(orgFiles[0].getSize() > 5242880){
							model.addObject("errorMsg", "File size exceeds 5 MB");
							return model;
						}
						StringBuilder filePath = new StringBuilder(
								UPLOAD_FILEPATH + File.separator + contact + File.separator + "OrgFiles" + File.separator + i);
						File file = new File(filePath.toString());
						if (!file.exists()) {
							file.mkdirs();
						}
						String originalName = orgFiles[i].getOriginalFilename();
						File actFile = new File(filePath.append(File.separator + originalName).toString());
						Files.copy(orgFiles[i].getInputStream(), actFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
						jsonObject.put("path" + i, "/static/docs/"+contact+"/OrgFiles/"+i+"/"+originalName);
					}
					nomineeObj.setOrgCert(jsonObject.toString());
				}
				nomineeObj.setBalanceJson(request.getParameter("balanceJson")); 
				nomineeObj.setIncomeJson(request.getParameter("totalBasicFinance"));
				
				int countLeft = nominatorService.getEnterpriseCountLeft(request.getParameter("registrationNumber"));
					if (countLeft > 0) {
						nominatorService.decreaseEnterpriseCount(request.getParameter("registrationNumber"));
					}
					boolean status = thirdNomineeService.saveNominee(nomineeObj);
					
					if (status) {
						model.addObject("thirdnom", nomineeObj);
						model.addObject("nominee", nominee);
						model.addObject("successMsg", "Your data has been added successfully!");
						model.setViewName("nomineelanding");
						return model;
					} else {
						model.addObject("errorMsg", "An error occured while saving your data! Please try again.");
						model.setViewName("nomineelanding");
						return model;
					}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		model.addObject("errorMsg", "Something went wrong! Please try again.");
		return model;
	}

	@PostMapping("/neas/api/thirdupload/{filetype}/{encryptedPhone}")
	public ResponseEntity<Object> ajaxFileUpload(@PathVariable("encryptedPhone") String encryptedPhone, @PathVariable("filetype") String filetype,
			@RequestParam("file1") MultipartFile[] files) throws IOException, JSONException {
		try{
		String UPLOAD_FILEPATH = env.getRequiredProperty("static.content.directory");

		String contact = EncryptionUtil.decrypt(encryptedPhone, OtpSalt.SALT.toString());
		NomineeDetails nominee = nomineeService.getNomineeByContact(contact);
		if (nominee != null) {
			ThirdCatNominee thirdNominee = thirdNomineeService.getNomineeByContact(contact);
			if(thirdNominee == null){
				thirdNominee = new ThirdCatNominee();
			}
			thirdNominee.setContact(contact);
			if (files[0].getSize() >0) {
				JSONObject jsonObject = new JSONObject();
				for (int i = 0; i < files.length; i++) {
					StringBuilder filePath = new StringBuilder(
							UPLOAD_FILEPATH + File.separator + contact + File.separator + filetype + File.separator + i);
					File file = new File(filePath.toString());
					if (!file.exists()) {
						file.mkdirs();
					}
					String originalName = files[i].getOriginalFilename();
					File actFile = new File(filePath.append(File.separator + originalName).toString());
					Files.copy(files[i].getInputStream(), actFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
					jsonObject.put("path" + i, "/static/docs/"+contact+"/"+filetype+"/"+i+"/"+originalName);
				}
				switch(filetype){
					case "balance_sheet": thirdNominee.setBalanceSheet(jsonObject.toString()); break;
					case "profit_loss_statement":thirdNominee.setProfitLossStatement(jsonObject.toString()); break;
					case "cash_flow_statement": thirdNominee.setCashFlowStatement(jsonObject.toString());break;
					case "moa": thirdNominee.setMoaDoc(jsonObject.toString());break;
					case "aoa": thirdNominee.setAoaDoc(jsonObject.toString());break;
					case "mandatory_industry_certifications": thirdNominee.setIndustryCert(jsonObject.toString());break;
					case "organization_registration_certificate": thirdNominee.setOrgCert(jsonObject.toString());break;
		
				}
			}
			boolean status = thirdNomineeService.saveNominee(thirdNominee);
			if (status) {
				return new ResponseEntity<>("File Uploaded Successfully.",HttpStatus.OK);
			} else {
				return new ResponseEntity<>("Invalid file.",HttpStatus.BAD_REQUEST);
			}
		}
		return new ResponseEntity<>("Invalid file.",HttpStatus.BAD_REQUEST);
		}catch(Exception e){
			e.printStackTrace();
			return new ResponseEntity<>("Invalid file.",HttpStatus.BAD_REQUEST);
		}

	}
	
	private String getNewFormNumber(NomineeDetails nominee){
		try {
			String regNum = nominee.getRegistrationNumber();			
			StringBuilder sb = new StringBuilder(regNum+"-");
				int number = nomineeService.getAllNomineeByRegistration(regNum).size();
				sb.append(String.format("%04d", ++number));
				return sb.toString();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return "";
		}

	}
	
	@RequestMapping(value = "/createzip/thirdnominee/{encryptedPhone}", method = RequestMethod.GET)
	public void createNomineeExcel(@PathVariable("encryptedPhone") String encryptedPhone, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String contact = EncryptionUtil.decrypt(encryptedPhone, OtpSalt.SALT.toString());
		response.setContentType("APPLICATION/OCTET-STREAM");
		response.setHeader("Content-disposition", "attachment; filename="+contact+".zip");
		ThirdCatNominee secnom = thirdNomineeService.getNomineeByContact(contact);
		List <String> fileList = new ArrayList < String > ();
		String SOURCE_FOLDER = "";
		String OUTPUT_ZIP_FILE = "";
		if(secnom!=null) {
			SOURCE_FOLDER = env.getProperty("static.content.directory")+File.separator+contact;
			OUTPUT_ZIP_FILE = env.getProperty("static.content.directory")+File.separator+contact+".zip";
			File basePath = new File(SOURCE_FOLDER);
			generateFileList(SOURCE_FOLDER,basePath,fileList);
			zipIt(OUTPUT_ZIP_FILE,fileList,SOURCE_FOLDER);
		}
		
		OutputStream out = response.getOutputStream();
        FileInputStream in = new FileInputStream(OUTPUT_ZIP_FILE);
        byte[] buffer = new byte[4096];
        int length;
        while ((length = in.read(buffer)) > 0){
           out.write(buffer, 0, length);
        }
        in.close();
        out.flush();
		
	}
	public void generateFileList(String SOURCE_FOLDER,File node,List <String> fileList) {
        if (node.isFile()) {
            fileList.add(generateZipEntry(SOURCE_FOLDER,node.toString()));
        }

        if (node.isDirectory()) {
            String[] subNote = node.list();
            for (String filename: subNote) {
                generateFileList(SOURCE_FOLDER,new File(node, filename),fileList);
            }
        }
    }

    private String generateZipEntry(String SOURCE_FOLDER,String file) {
        return file.substring(SOURCE_FOLDER.length() + 1, file.length());
    }
	
    public void zipIt(String zipFile,List <String> fileList,String SOURCE_FOLDER) {
        byte[] buffer = new byte[1024];
        String source = new File(SOURCE_FOLDER).getName();
        FileOutputStream fos = null;
        ZipOutputStream zos = null;
        try {
            fos = new FileOutputStream(zipFile);
            zos = new ZipOutputStream(fos);

            System.out.println("Output to Zip : " + zipFile);
            FileInputStream in = null;

            for (String file: fileList) {
                System.out.println("File Added : " + file);
                ZipEntry ze = new ZipEntry(source + File.separator + file);
                zos.putNextEntry(ze);
                try {
                    in = new FileInputStream(SOURCE_FOLDER + File.separator + file);
                    int len;
                    while ((len = in .read(buffer)) > 0) {
                        zos.write(buffer, 0, len);
                    }
                } finally {
                    in.close();
                }
            }

            zos.closeEntry();
            System.out.println("Folder successfully compressed");

        } catch (IOException ex) {
        	System.out.println("Exception while zipping");
            ex.printStackTrace();
        } finally {
            try {
                zos.close();
            } catch (IOException e) {
            	System.out.println("Exception while zipping");
                e.printStackTrace();
            }
        }
    }
}
