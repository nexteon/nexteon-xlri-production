/*package com.xlri.awards.config;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebFilter(filterName = "myFilter", urlPatterns = "/*")
public class MyFilter implements Filter {

	private FilterConfig config;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.config = filterConfig;
		config.getServletContext().log("Filter  started");
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		boolean splchr_flag = false;
		Map inputFields = null;
		inputFields = request.getParameterMap();
		if (inputFields != null) {
			Iterator field_iter = inputFields.keySet().iterator();
			while (field_iter.hasNext())
			{
				String param_name = (String) field_iter.next();
				String[] param_value = (String[]) inputFields.get(param_name);
				for (int i = 0; i < param_value.length; i++) {
					if (checkSplChrs(param_value[i])) {
						splchr_flag = true;
						break;
					}
				}

				if (splchr_flag) {
					break;
				}
			}
		}

		if (splchr_flag) {
			try {
				HttpServletResponse httpResponse = (HttpServletResponse) response;
				httpResponse.sendRedirect(request.getParameter("currentpath")+"?invalidinput=true");
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} else
			chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
	}

	public static boolean checkSplChrs(String inputStr) {
		boolean splchr_flag = false;
		String[] splChrs = { "<", ">", "script", "alert", "truncate", "delete", "insert", "drop", "null", "xp_", "<>",
				"!", "`", "input" }; 
		for (int i = 0; i < splChrs.length; i++) {
			if (inputStr.indexOf(splChrs[i]) >= 0) {
				splchr_flag = true; 
				break;
			}
		}
		return true;
	}

}*/