package com.xlri.awards.config;

import java.util.EnumSet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.SessionTrackingMode;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import com.xlri.awards.config.security.SecurityConfig;
import com.xlri.awards.config.security.SessionListener;

public class WebInitializer extends AbstractAnnotationConfigDispatcherServletInitializer{

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return null;
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class<?>[]{WebConfig.class,MailConfig.class,HibernateConfig.class,SecurityConfig.class};
	}

	@Override
	protected String[] getServletMappings() {
		return new String[]{"/"};
	}

	@Override
    public void onStartup(ServletContext servletContext) throws ServletException {
		//servletContext.addListener(new SessionListener());
        //servletContext.setSessionTrackingModes(EnumSet.of(SessionTrackingMode.COOKIE));
        super.onStartup(servletContext);
    }
}
