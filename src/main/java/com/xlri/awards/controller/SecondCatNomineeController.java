package com.xlri.awards.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.xlri.awards.common.EncryptionUtil;
import com.xlri.awards.common.OtpSalt;
import com.xlri.awards.dao.entity.NomineeDetails;
import com.xlri.awards.dao.entity.SecondCatNominee;
import com.xlri.awards.dao.service.api.NominatorService;
import com.xlri.awards.dao.service.api.NomineeService;
import com.xlri.awards.dao.service.api.SecondCatNomineeService;

@Controller
@PropertySource("classpath:application.properties")
public class SecondCatNomineeController {

	@Autowired
	Environment env;

	@Autowired
	NomineeService nomineeService;

	@Autowired
	NominatorService nominatorService;
	
	@Autowired
	SecondCatNomineeService secNomineeService;

	public static final Logger LOGGER = LoggerFactory.getLogger(SecondCatNomineeController.class);

	@GetMapping(value = "/secondcatreg/{encryptedID}")
	public ModelAndView getSecondCatNomineePage(@PathVariable String encryptedID) {
		String emailId = EncryptionUtil.decrypt(encryptedID, OtpSalt.SALT.toString());
		ModelAndView model = new ModelAndView();
		NomineeDetails nominee = nomineeService.getNomineeByEmailId(emailId);
		if (nominee != null && nominee.getEnterpriseCategory().equals("uptoten")) {
			model.addObject("nominee", nominee);
			model.addObject("formNumber", getNewFormNumber(nominee));
			model.addObject("nominatorName",nominatorService.getNominatorByRegistrationNumber(nominee.getRegistrationNumber()));
			model.addObject("encryptedPhone", EncryptionUtil.encrypt(nominee.getContact(), OtpSalt.SALT.toString()));
			model.setViewName("secondcatnominee");
		} else {
			model.setViewName("index");
		}
		return model;
	}

	@PostMapping(value = "/saveseccat/{encryptedPhone}")
	public ModelAndView saveSecondNominee(@PathVariable("encryptedPhone") String encryptedPhone,
			@RequestParam("file1") MultipartFile[] moaFiles, @RequestParam("file3") MultipartFile[] certificateFiles,
			@RequestParam("file4") MultipartFile[] instituteFiles, @RequestParam("file2") MultipartFile[] aoaFiles, HttpServletRequest request) throws Exception {

		String UPLOAD_FILEPATH = env.getRequiredProperty("static.content.directory");

		ModelAndView model = new ModelAndView("nomineelanding");
		String contact = EncryptionUtil.decrypt(encryptedPhone, OtpSalt.SALT.toString());
		NomineeDetails nominee = nomineeService.getNomineeByContact(contact);
		if (nominee != null) {
			String emailId = nominee.getEmailId();
			SecondCatNominee secNominee = new SecondCatNominee();
			secNominee.setContact(contact);
			secNominee.setEmailId(emailId);
			secNominee.setFormNumber(request.getParameter("formNumber"));
			secNominee.setEnterpriseDate(request.getParameter("enterpriseDate"));
			secNominee.setBusinessFormation(request.getParameter("businessFormation"));
			secNominee.setFoundJson(request.getParameter("foundJson"));
			secNominee.setPromotorJson(request.getParameter("promotorJson"));
			secNominee.setCooperSgh(request.getParameter("cooperSgh"));
			secNominee.setCooperComittee(request.getParameter("cooperComittee"));
			secNominee.setBearerJson(request.getParameter("bearerJson"));
			secNominee.setRevenueJSON(request.getParameter("totalBasicFinance"));
			secNominee.setReleventCertification(request.getParameter("redioCert").equals("yes") ? true : false);
			if (moaFiles[0].getSize() >0) {
				
				JSONObject jsonObject = new JSONObject();
				for (int i = 0; i < moaFiles.length; i++) {
					if(moaFiles[0].getSize() > 5242880){
						model.addObject("errorMsg", "File size exceeds 5 MB");
						return model;
					}
					StringBuilder filePath = new StringBuilder(
							UPLOAD_FILEPATH + File.separator + contact + File.separator + "MoA" + File.separator + i);
					File file = new File(filePath.toString());
					if (!file.exists()) {
						file.mkdirs();
					}
					String originalName = moaFiles[i].getOriginalFilename();
					File actFile = new File(filePath.append(File.separator + originalName).toString());
					Files.copy(moaFiles[i].getInputStream(), actFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
					jsonObject.put("path" + i, "/static/docs/"+contact+"/MoA/"+i+"/"+originalName);
				}
				secNominee.setMoaFileUpload(jsonObject.toString());
			}
			if (aoaFiles[0].getSize() > 0) {
				JSONObject jsonObject = new JSONObject();
				for (int i = 0; i < aoaFiles.length; i++) {
					if(aoaFiles[0].getSize() > 5242880){
						model.addObject("errorMsg", "File size exceeds 5 MB");
						return model;
					}
					StringBuilder filePath = new StringBuilder(
							UPLOAD_FILEPATH + File.separator + contact + File.separator + "AoA" + File.separator + i);
					File file = new File(filePath.toString());
					if (!file.exists()) {
						file.mkdirs();
					}
					String originalName = aoaFiles[i].getOriginalFilename();
					File actFile = new File(filePath.append(File.separator + originalName).toString());
					Files.copy(aoaFiles[i].getInputStream(), actFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
					jsonObject.put("path" + i, "/static/docs/"+contact+"/AoA/"+i+"/"+originalName);
				}
				secNominee.setAoaFileUpload(jsonObject.toString());
			}
			if (certificateFiles[0].getSize() > 0) {
				JSONObject jsonObject = new JSONObject();
				for (int i = 0; i < certificateFiles.length; i++) {
					if(certificateFiles[0].getSize() > 5242880){
						model.addObject("errorMsg", "File size exceeds 5 MB");
						return model;
					}
					StringBuilder filePath = new StringBuilder(UPLOAD_FILEPATH + File.separator + contact
							+ File.separator + "Certificate" + File.separator + i);
					File file = new File(filePath.toString());
					if (!file.exists()) {
						file.mkdirs();
					}
					String originalName = certificateFiles[i].getOriginalFilename();
					File actFile = new File(filePath.append(File.separator + originalName).toString());
					Files.copy(certificateFiles[i].getInputStream(), actFile.toPath(),StandardCopyOption.REPLACE_EXISTING);
					jsonObject.put("path" + i, "/static/docs/"+contact+"/Certificate/"+i+"/"+originalName);
				}
				secNominee.setOrgFileUpload(jsonObject.toString());
			}
			if (request.getParameter("redioCert").equals("yes")) {
				if (instituteFiles[0].getSize() > 0) {
					JSONObject jsonObject = new JSONObject();
					for (int i = 0; i < instituteFiles.length; i++) {
						if(instituteFiles[0].getSize() > 5242880){
							model.addObject("errorMsg", "File size exceeds 5 MB");
							return model;
						}
						StringBuilder filePath = new StringBuilder(UPLOAD_FILEPATH + File.separator + contact
								+ File.separator + "Industry" + File.separator + i);
						File file = new File(filePath.toString());
						if (!file.exists()) {
							file.mkdirs();
						}
						String originalName = instituteFiles[i].getOriginalFilename();
						File actFile = new File(filePath.append(File.separator + originalName).toString());
						Files.copy(instituteFiles[i].getInputStream(), actFile.toPath(),StandardCopyOption.REPLACE_EXISTING);
						jsonObject.put("path" + i, "/static/docs/"+contact+"/Industry/"+i+"/"+originalName);
					}
					secNominee.setIndFileUpload(jsonObject.toString());
				}
			}
			model.addObject("nominee", nominee);
			model.addObject("encryptedID", EncryptionUtil.encrypt(emailId, OtpSalt.SALT.toString()));
			
			boolean status= false;
			if(secNomineeService.getNomineeByContact(contact)!=null){
				status = secNomineeService.updateNominee(secNominee);
			}else{
				status = secNomineeService.saveNominee(secNominee);
			}
			
			if (status) {
				model.addObject("secnom", secNominee);
				model.addObject("successMsg", "Your data has been added successfully!");
				return model;
			} else {
				model.addObject("errorMsg", "An error occured while saving your data! Please try again.");
				return model;
			}
		}
		model.addObject("errorMsg", "Something went wrong! Please try again.");
		return model;

	}
	
	@PostMapping("/neas/api/fileupload/{filetype}/{encryptedPhone}")
	public ResponseEntity<Object> ajaxFileUpload(@PathVariable("encryptedPhone") String encryptedPhone, @PathVariable("filetype") String filetype,
			@RequestParam("file1") MultipartFile[] files) throws IOException, JSONException {
		try{
		String UPLOAD_FILEPATH = env.getRequiredProperty("static.content.directory");

		String contact = EncryptionUtil.decrypt(encryptedPhone, OtpSalt.SALT.toString());
		NomineeDetails nominee = nomineeService.getNomineeByContact(contact);
		SecondCatNominee secNominee = null;
		if (nominee != null) {
			if(secNomineeService.getNomineeByContact(contact)!=null){
				secNominee = secNomineeService.getNomineeByContact(contact);
			}else{
				secNominee = new SecondCatNominee();	
			}
			
			secNominee.setContact(contact);
			secNominee.setEmailId(nominee.getEmailId());
			if (files != null) {
				JSONObject jsonObject = new JSONObject();
				for (int i = 0; i < files.length; i++) {
					StringBuilder filePath = new StringBuilder(
							UPLOAD_FILEPATH + File.separator + contact + File.separator + filetype + File.separator + i);
					File file = new File(filePath.toString());
					if (!file.exists()) {
						file.mkdirs();
					}
					String originalName = files[i].getOriginalFilename();
					File actFile = new File(filePath.append(File.separator + originalName).toString());
					Files.copy(files[i].getInputStream(), actFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
					jsonObject.put("path" + i, "/static/docs/"+contact+"/"+filetype+"/"+i+"/"+originalName);
				}
				if(filetype.equals("moa")){
					secNominee.setMoaFileUpload(jsonObject.toString());
				}else if(filetype.equals("aoa")){
					secNominee.setAoaFileUpload(jsonObject.toString());
				}else if(filetype.equals("orfile")){
					secNominee.setOrgFileUpload(jsonObject.toString());
				}else if(filetype.equals("infile")){
					secNominee.setIndFileUpload(jsonObject.toString());
				}
			}
			boolean status = false;
			if(secNomineeService.getNomineeByContact(contact)!=null){
				status = secNomineeService.updateNominee(secNominee);
			}else{
				status = secNomineeService.saveNominee(secNominee);
			}
			if (status) {
				return new ResponseEntity<>("File Uploaded Successfully.",HttpStatus.OK);
			} else {
				return new ResponseEntity<>("Invalid file.",HttpStatus.BAD_REQUEST);
			}
		}
		return new ResponseEntity<>("Invalid file.",HttpStatus.BAD_REQUEST);
		}catch(Exception e){
			e.printStackTrace();
			return new ResponseEntity<>("Invalid file.",HttpStatus.BAD_REQUEST);
		}

	}
	
/*	@RequestMapping(value = "/createzip/secnominee/{encryptedPhone}", method = RequestMethod.GET)
	public void createNomineeExcel(@PathVariable("encryptedPhone") String encryptedPhone, HttpServletRequest request, HttpServletResponse response) throws Exception {
		//String contact = EncryptionUtil.decrypt(encryptedPhone, OtpSalt.SALT.toString());
		response.setContentType("APPLICATION/OCTET-STREAM");
		response.setHeader("Content-disposition", "attachment; filename=NomineeFiles.zip");
		SecondCatNominee secnom = secNomineeService.getNomineeByContact(encryptedPhone);
		String filepathzipped = env.getProperty("static.content.directory")+File.separator+encryptedPhone+".zip";
		ZipOutputStream zipFile = new ZipOutputStream(new FileOutputStream(filepathzipped));
		JSONObject aoa=null,moa=null,org=null,ind=null;
		if(secnom.getAoaFileUpload()!=null) {
			aoa = new JSONObject(secnom.getAoaFileUpload());
			Iterator<String> keys = aoa.keys();
			while(keys.hasNext()) {
				String key = keys.next();
				zipFiles(aoa.getString(key),zipFile);
			}
		}
		if(secnom.getMoaFileUpload()!=null) {
			moa = new JSONObject(secnom.getMoaFileUpload());
			Iterator<String> keys = moa.keys();
			while(keys.hasNext()) {
				String key = keys.next();
				zipFiles(moa.getString(key),zipFile);
			}
		}
		if(secnom.getOrgFileUpload()!=null) {
			org = new JSONObject(secnom.getOrgFileUpload());
			Iterator<String> keys = org.keys();
			while(keys.hasNext()) {
				String key = keys.next();
				zipFiles(org.getString(key),zipFile);
			}
		}
		if(secnom.getIndFileUpload()!=null) {
			ind = new JSONObject(secnom.getIndFileUpload());
			Iterator<String> keys = ind.keys();
			while(keys.hasNext()) {
				String key = keys.next();
				zipFiles(ind.getString(key),zipFile);
			}
		}
		File file = new File(filepathzipped);
        if(!file.exists()){
            System.out.println("file not found");
        }
        response.setContentType("APPLICATION/OCTET-STREAM");

        OutputStream out = response.getOutputStream();
        FileInputStream in = new FileInputStream(file);
        byte[] buffer = new byte[4096];
        int length;
        while ((length = in.read(buffer)) > 0){
           out.write(buffer, 0, length);
        }
        in.close();
        out.flush();

	}
*/	
	
	private String getNewFormNumber(NomineeDetails nominee){
		try {
			String regNum = nominee.getRegistrationNumber();			
			StringBuilder sb = new StringBuilder(regNum+"-");
				int number = nomineeService.getAllNomineeByRegistration(regNum).size();
				sb.append(String.format("%04d", number));
				return sb.toString();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return "";
		}

	}
	
	@RequestMapping(value = "/createzip/secnominee/{encryptedPhone}", method = RequestMethod.GET)
	public void createNomineeExcel(@PathVariable("encryptedPhone") String encryptedPhone, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String contact = EncryptionUtil.decrypt(encryptedPhone, OtpSalt.SALT.toString());
		response.setContentType("APPLICATION/OCTET-STREAM");
		response.setHeader("Content-disposition", "attachment; filename="+contact+".zip");
		SecondCatNominee secnom = secNomineeService.getNomineeByContact(contact);
		List <String> fileList = new ArrayList < String > ();
		String SOURCE_FOLDER = "";
		String OUTPUT_ZIP_FILE = "";
		if(secnom!=null) {
			SOURCE_FOLDER = env.getProperty("static.content.directory")+File.separator+contact;
			OUTPUT_ZIP_FILE = env.getProperty("static.content.directory")+File.separator+contact+".zip";
			File basePath = new File(SOURCE_FOLDER);
			generateFileList(SOURCE_FOLDER,basePath,fileList);
			zipIt(OUTPUT_ZIP_FILE,fileList,SOURCE_FOLDER);
		}
		
		OutputStream out = response.getOutputStream();
        FileInputStream in = new FileInputStream(OUTPUT_ZIP_FILE);
        byte[] buffer = new byte[4096];
        int length;
        while ((length = in.read(buffer)) > 0){
           out.write(buffer, 0, length);
        }
        in.close();
        out.flush();
		
	}


	public void generateFileList(String SOURCE_FOLDER,File node,List <String> fileList) {
        if (node.isFile()) {
            fileList.add(generateZipEntry(SOURCE_FOLDER,node.toString()));
        }

        if (node.isDirectory()) {
            String[] subNote = node.list();
            for (String filename: subNote) {
                generateFileList(SOURCE_FOLDER,new File(node, filename),fileList);
            }
        }
    }

    private String generateZipEntry(String SOURCE_FOLDER,String file) {
        return file.substring(SOURCE_FOLDER.length() + 1, file.length());
    }
	
    public void zipIt(String zipFile,List <String> fileList,String SOURCE_FOLDER) {
        byte[] buffer = new byte[1024];
        String source = new File(SOURCE_FOLDER).getName();
        FileOutputStream fos = null;
        ZipOutputStream zos = null;
        try {
            fos = new FileOutputStream(zipFile);
            zos = new ZipOutputStream(fos);

            System.out.println("Output to Zip : " + zipFile);
            FileInputStream in = null;

            for (String file: fileList) {
                System.out.println("File Added : " + file);
                ZipEntry ze = new ZipEntry(source + File.separator + file);
                zos.putNextEntry(ze);
                try {
                    in = new FileInputStream(SOURCE_FOLDER + File.separator + file);
                    int len;
                    while ((len = in .read(buffer)) > 0) {
                        zos.write(buffer, 0, len);
                    }
                } finally {
                    in.close();
                }
            }

            zos.closeEntry();
            System.out.println("Folder successfully compressed");

        } catch (IOException ex) {
        	System.out.println("Exception while zipping");
            ex.printStackTrace();
        } finally {
            try {
                zos.close();
            } catch (IOException e) {
            	System.out.println("Exception while zipping");
                e.printStackTrace();
            }
        }
    }

	

}