package com.xlri.awards.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.xlri.awards.common.EncryptionUtil;

@Entity
@Table(name="Expert")
public class Expert {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	
	private Long id;
	
	@Column(nullable=false)
	private String expertName;
	
	@Column(nullable=false,unique=true)
	private String emailId;
	
	@Column(nullable=false,unique=true)
	private String contactNumber;
	
	@Column(nullable=false)
	private String designation;
	
	@Column(nullable=false)
	private String specialization;
	
	@Column(nullable=false)
	private String address;
	
	private String partnerWith;
	
	private String password;
	
	private String salt;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getExpertName() {
		return expertName;
	}

	public void setExpertName(String expertName) {
		this.expertName = expertName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getSpecialization() {
		return specialization;
	}

	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPartnerWith() {
		return partnerWith;
	}

	public void setPartnerWith(String partnerWith) {
		this.partnerWith = partnerWith;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.getNewSalt();
		this.password = EncryptionUtil.encryptPass(password + this.getSalt());
	}

	public String getSalt() {
		return salt;
	}

	/*public void setSalt(String salt) {
		this.salt = salt;
	}*/
	
	private void getNewSalt() {
		this.salt = EncryptionUtil.generateRandomString();
	}
	
}
