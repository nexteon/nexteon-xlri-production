package com.xlri.awards.dao.service.api;

import java.util.List;

import com.xlri.awards.dao.entity.Partners;

public interface PartnersService {

	public boolean savePartners(Partners partners);

	public boolean updatePartners(Partners partners);

	public List<Partners> getAllPartners();

	Partners getPartnersById(String id);
}
