package com.xlri.awards.dao.service.api;

import java.util.List;

import com.xlri.awards.dao.entity.Evaluation;
import com.xlri.awards.dao.entity.NomineeDetails;

public interface EvaluationService {

	public boolean saveEvaluationDetails(Evaluation evaluation);
	
	public boolean updateEvaluationDeatails(Evaluation evaluation);
	
	public List<Evaluation> getAllEvaluationValues();

	public List<NomineeDetails> getNomineeByExpertAssigned(String emailId);
	
	public Evaluation getNomineeByEmailId(String emailId);
	
	public List<Evaluation> getAllListByNominee(String... state);

	public Evaluation getEvaluationByNominee(String emailId);
	
}
