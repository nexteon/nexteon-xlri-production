<div class="rename-sheet">
	<div class="give-padding"></div>
    <!-- <div class="form-group col-xs-12">
        <label class="" for="balance_sheet">1. Date of Enterprise's Incorporation</label>
        <input type="date" name="enterpriseDate" id="enterprise-date" class="form-control" placeholder="Date of Enterprise's Incorporation"  max="2014-03-31" min="2011-04-01" required>
    </div> -->
    
    
    							<div class="col-md-12">
                                               <div class="form-group">
                                                   <label for="enterprise-date">Date of Enterprise's Incorporation (The day when nominee's company/organization was registered.)<span class="mandate">*</span></label>
                                                 <div class="row">  <div class="col-md-3">
                                                    <select name="date" class="form-control" id="yearidd" required>
                                                     <option value="" selected disabled>YYYY</option>
                                                     <c:forEach begin="1978" end="2018" step="1" var="d">
                                                      <option value="${d}">${d}</option>
                                                     </c:forEach>
                                                    </select>
                                                   </div>
                                                   
                                                   <div class="col-md-3">
                                                    <select name="month" class="form-control" id="monthidd" required>
                                                     <option value="" selected disabled>MM</option>
                                                     <option id="jan1" value="01">Jan</option>
                                                     <option id="feb1" value="02">Feb</option>
                                                     <option id="mar1" value="03">Mar</option>
                                                     <option id="apr1" value="04">Apr</option>
                                                     <option id="may1" value="05">May</option>
                                                     <option id="jun1" value="06">Jun</option>
                                                     <option id="jul1" value="07">Jul</option>
                                                     <option id="aug1" value="08">Aug</option>
                                                     <option id="sep1" value="09">Sep</option>
                                                     <option id="oct1" value="10">Oct</option>
                                                     <option id="nov1" value="11">Nov</option>
                                                     <option id="dec1" value="12">Dec</option>
                                                    </select>
                                                   </div>
                                                   
                                                   <div class="col-md-2">
                                                    <select name="day" class="form-control" id="dayidd" required>
                                                     <option value="" selected disabled>DD</option>
                                                     <option value="01">01</option>
                                                     <option value="02">02</option>
                                                     <option value="03">03</option>
                                                     <option value="04">04</option>
                                                     <option value="05">05</option>
                                                     <option value="06">06</option>
                                                     <option value="07">07</option>
                                                     <option value="08">08</option>
                                                     <option value="09">09</option>
                                                     <c:forEach begin="10" end="28" step="1" var="d">
                                                      <option value="${d}">${d}</option>
                                                     </c:forEach>
                                                     <option id="d29" style="display: none;" value="29">29</option>
									                 <option id="d30" style="display: none;" value="30">30</option>
									                 <option id="d31" style="display: none;" value="31">31</option>
                                                    </select>
                                                   </div>
                                                   <div class="help-block with-errors"></div>
                                                   </div>
                                               </div>
                                           </div>
                                   
                                   <input type="hidden" class="form-control" name="enterpriseDate" id="dateeidd">
    
    
    <div class="give-padding"></div>
    <div class="col-md-12">
        <strong class="main-td">2. Founding Team Members (Mention their name and credentials)</strong>
        <input type="button"  value="+" onClick="aadTextBoxFT()" >
        <input type="button" value="-"  onClick="removeTextBoxFT()">
    </div>
    <div class="give-padding"></div>
    <div id="ft">
        <div id="ft_child">
            <div class="col-xs-6">
                <label class="" for="ft_name">Name/s </label>
                <input id="ft_name" type="text" name="foundingName" class="ft_crt_assts_17  form-control" >
            </div>
            <div class="col-xs-6">
                <label class="" for="ft_designation">Designation</label>
                <input id="ft_designation" type="text" name="foundingDesignation" class="ft_crt_assts_17  form-control" >
            </div>
        </div>
    </div>
    <div class="give-padding"></div>
    <div class="col-md-12">
        <strong class="main-td">3. Other Promoters, if any (Mention their name and credentials) </strong>
        <input type="button" value="+" onClick="aadTextBoxOP()" > <input type="button" value="-"  onClick="removeTextBoxOP()">
    </div>
    <div class="give-padding"></div>
    <div id="op">
        <div id="op_child">
            <div class="col-xs-6">
                <label class="" for="ot_name">Name/s </label>
                <input id="ot_name" type="text" name="otherName" class="ft_crt_assts_17  form-control" >
            </div>
            <div class="col-xs-6">
                <label class="" for="ot_designation">Designation</label>
                <input id="ot_designation" type="text" name="otherDesignation" class="ft_crt_assts_17  form-control" >
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="foundingJson" value="" >
<input type="hidden" name="promotorJson" value="" >
<div class="clearfix"></div>
<div class="give-padding"></div>
<h4>4. Balance Sheet Details</h4>
<div class="bal-sheet">
    <table class="table table-bordered" id="balance-details">
        <tr>
            <th>Financial Year (ending March)</th>
            <th>2017-18</th>
            <th>2016-17</th>
            <th>2015-16</th>
            <th>2014-15</th>
        </tr>
        <tr>
            <td><strong class="main-td2">Current assets</strong></td>
            <td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="cash_2017-18" data-name="tot_crt_assts_17" class="tot_crt_assts_17 form-control onlyDigit" onkeyup="tot_crt_assts_17_word.innerHTML=convertNumberToWords(this.value)" required><div id="tot_crt_assts_17_word"></div></div> </td>
            <td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="cash_2016-17" data-name="tot_crt_assts_16" class="tot_crt_assts_16 form-control onlyDigit" onkeyup="tot_crt_assts_16_word.innerHTML=convertNumberToWords(this.value)" required><div id="tot_crt_assts_16_word"></div> </div></td>
            <td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="cash_2015-16" data-name="tot_crt_assts_15" class="tot_crt_assts_15 form-control onlyDigit" onkeyup="tot_crt_assts_15_word.innerHTML=convertNumberToWords(this.value)" required><div id="tot_crt_assts_15_word"></div> </div></td>
            <td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="cash_2014-15" data-name="tot_crt_assts_14" class="tot_crt_assts_14 form-control onlyDigit" onkeyup="tot_crt_assts_14_word.innerHTML=convertNumberToWords(this.value)" required><div id="tot_crt_assts_14_word"></div> </div></td>
        </tr>
        
        
        
        <tr>
            <td><strong class="main-td2">Fixed assets</strong></td>
            <td><div class="form-group">
                <i class="fa fa-inr" aria-hidden="true"></i>
                <input id="fixed_assets_17_18" type="text" data-name="tot_fix_assts_17" class="tot_fix_assts_17 form-control onlyDigit" onkeyup="tot_fix_assts_17_word.innerHTML=convertNumberToWords(this.value)" required><div id="tot_fix_assts_17_word"></div> 
           </div> </td>
            
            <td><div class="form-group">
                <i class="fa fa-inr" aria-hidden="true"></i> 
                <input id="fixed_assets_16_17" type="text" data-name="tot_fix_assts_16" class="tot_fix_assts_16 form-control onlyDigit" onkeyup="tot_fix_assts_16_word.innerHTML=convertNumberToWords(this.value)" required><div id="tot_fix_assts_16_word"></div>
           </div> </td>
            <td><div class="form-group">
                <i class="fa fa-inr" aria-hidden="true"></i> 
                <input id="fixed_assets_15_16" type="text" data-name="tot_fix_assts_15" class="tot_fix_assts_15 form-control onlyDigit" onkeyup="tot_fix_assts_15_word.innerHTML=convertNumberToWords(this.value)" required><div id="tot_fix_assts_15_word"></div>
            </div></td>
            <td><div class="form-group">
                <i class="fa fa-inr" aria-hidden="true"></i> 
                <input id="fixed_assets_14_15" type="text" data-name="tot_fix_assts_14" class="tot_fix_assts_14 form-control onlyDigit" onkeyup="tot_fix_assts_14_word.innerHTML=convertNumberToWords(this.value)" required><div id="tot_fix_assts_14_word"></div>
           </div> </td>
        </tr>
        
        
        
        
        <tr>
            <td><strong class="main-td">Total assets</strong></td>
            <td><div class="form-group">
                <i class="fa fa-inr" aria-hidden="true"></i>
                <input id="total_assts_17_18"type="text" data-name="total_assts_17" class="total_assts_17  form-control onlyDigit" onkeyup="total_assts_17_word.innerHTML=convertNumberToWords(this.value)" readonly><div id="total_assts_17_word"></div>  
            </div> </td>
            
            <td><div class="form-group">
                <i class="fa fa-inr" aria-hidden="true"></i> 
                <input id="total_assts_16_17" type="text" data-name="total_assts_16" class="total_assts_16 form-control onlyDigit" onkeyup="total_assts_16_word.innerHTML=convertNumberToWords(this.value)" readonly><div id="total_assts_16_word"></div>
           </div>  </td>
            <td><div class="form-group">
                <i class="fa fa-inr" aria-hidden="true"></i> 
                <input id="total_assts_15_16"type="text" data-name="total_assts_15" class="total_assts_15 form-control onlyDigit" onkeyup="total_assts_15_word.innerHTML=convertNumberToWords(this.value)" readonly><div id="total_assts_15_word"></div>
            </div> </td>
            <td><div class="form-group">
                <i class="fa fa-inr" aria-hidden="true"></i> 
                <input id="total_assts_14_15"type="text" data-name="total_assts_14" class="total_assts_14 form-control onlyDigit" onkeyup="total_assts_14_word.innerHTML=convertNumberToWords(this.value)" readonly><div id="total_assts_14_word"></div>
            </div> </td>
        </tr>
        <!--by Manish-->  
        <tr>
            <td><strong class="main-td2">Current/short-term liabilities</strong></td>
            <td><div class="form-group">
                <label class="hidden" for="current_short_term_liabilities_17_18">>Current/short-term liabilities 2017-2018</label>
                <i class="fa fa-inr" aria-hidden="true"></i>
                <input id="current_short_term_liabilities_17_18" type="text" data-name="current_short_term_liabilities_17" class="current_short_term_liabilities_17  form-control onlyDigit" onkeyup="current_short_term_liabilities_17_word.innerHTML=convertNumberToWords(this.value)" required><div id="current_short_term_liabilities_17_word"></div>
                </div>
            </td>
            
            <td>
                <div class="form-group">
                    <label class="hidden" for="current_short_term_liabilities_16_17">>Current/short-term liabilities 2016-2017</label>
                    <i class="fa fa-inr" aria-hidden="true"></i>
                    <input id="current_short_term_liabilities_16_17" type="text" data-name="current_short_term_liabilities_16" class="current_short_term_liabilities_16 form-control onlyDigit" onkeyup="current_short_term_liabilities_16_word.innerHTML=convertNumberToWords(this.value)" required><div id="current_short_term_liabilities_16_word"></div>
                </div>
            </td>
            
            <td>
                <div class="form-group">
                    <label class="hidden" for="current_short_term_liabilities_15_16">>Current/short-term liabilities 2015-2016</label>
                    <i class="fa fa-inr" aria-hidden="true"></i> 
                    <input id="current_short_term_liabilities_15_16" type="text" data-name="current_short_term_liabilities_15" class="current_short_term_liabilities_15 form-control onlyDigit" onkeyup="current_short_term_liabilities_15_word.innerHTML=convertNumberToWords(this.value)" required><div id="current_short_term_liabilities_15_word"></div>
                </div>
            </td>
            
            <td>
                <div class="form-group">
                    <label class="hidden" for="current_short_term_liabilities_14_15">>Current/short-term liabilities 2014-2015</label>
                    <i class="fa fa-inr" aria-hidden="true"></i> 
                    <input id="current_short_term_liabilities_14_15" type="text" data-name="current_short_term_liabilities_14" class="current_short_term_liabilities_14 form-control onlyDigit" onkeyup="current_short_term_liabilities_14_word.innerHTML=convertNumberToWords(this.value)" required><div id="current_short_term_liabilities_14_word"></div>
                </div>
            </td>
        </tr>
        
        
        
        <tr>
            <td><strong class="main-td2">Long-term liabilities</strong></td>
            <td><div class="form-group">
                <label class="hidden" for="long_term_liabilities_17_18">Long-term liabilities 2017-2018</label>
                <i class="fa fa-inr" aria-hidden="true"></i>
                <input id="long_term_liabilities_17_18" type="text" data-name="long_term_liabilities_17" class="long_term_liabilities_17  form-control onlyDigit" onkeyup="long_term_liabilities_17_word.innerHTML=convertNumberToWords(this.value)" required><div id="long_term_liabilities_17_word"></div>
                </div>
            </td>
            
            <td>
                <div class="form-group">
                    <label class="hidden" for="long_term_liabilities_16_17">Long-term liabilities 2016-2017</label>
                    <i class="fa fa-inr" aria-hidden="true"></i>
                    <input id="long_term_liabilities_16_17" type="text" data-name="long_term_liabilities_16" class="long_term_liabilities_16 form-control onlyDigit" onkeyup="long_term_liabilities_16_word.innerHTML=convertNumberToWords(this.value)" required><div id="long_term_liabilities_16_word"></div>
                </div>
            </td>
            
            <td>
                <div class="form-group">
                    <label class="hidden" for="long_term_liabilities_15_16">Long-term liabilities 2015-2016</label>
                    <i class="fa fa-inr" aria-hidden="true"></i> 
                    <input id="long_term_liabilities_15_16" type="text" data-name="long_term_liabilities_15" class="long_term_liabilities_15 form-control onlyDigit" onkeyup="long_term_liabilities_15_word.innerHTML=convertNumberToWords(this.value)" required><div id="long_term_liabilities_15_word"></div>
                </div>
            </td>
            
            <td>
                <div class="form-group">
                    <label class="hidden" for="long_term_liabilities_14_15">Long-term liabilities 2014-2015</label>
                    <i class="fa fa-inr" aria-hidden="true"></i> 
                    <input id="long_term_liabilities_14_15" type="text" data-name="long_term_liabilities_14" class="long_term_liabilities_14 form-control onlyDigit" onkeyup="long_term_liabilities_14_word.innerHTML=convertNumberToWords(this.value)" required><div id="long_term_liabilities_14_word"></div>
                </div>
            </td>
        </tr>
        
        <tr>
            <td><strong class="main-td">(B)Total liabilities </strong></td>
            <td><div class="form-group">
                <label class="hidden" for="total_liabilities_17_18">Total liabilities  2017-2018</label>
                <i class="fa fa-inr" aria-hidden="true"></i>
                <input id="total_liabilities_17_18" type="text" data-name="total_liabilities_17" class="total_liabilities_17  form-control onlyDigit" onkeyup="total_liabilities_17_word.innerHTML=convertNumberToWords(this.value)" readonly><div id="total_liabilities_17_word"></div>
                </div>
            </td>
            
            <td>
                <div class="form-group">
                    <label class="hidden" for="total_liabilities_16_17">Total liabilities  2016-2017</label>
                    <i class="fa fa-inr" aria-hidden="true"></i>
                    <input id="total_liabilities_16_17" type="text" data-name="total_liabilities_16" class="total_liabilities_16 form-control onlyDigit" onkeyup="total_liabilities_16_word.innerHTML=convertNumberToWords(this.value)" readonly><div id="total_liabilities_16_word"></div>
                </div>
            </td>
            
            <td>
                <div class="form-group">
                    <label class="hidden" for="total_liabilities_15_16">Total liabilities  2015-2016</label>
                    <i class="fa fa-inr" aria-hidden="true"></i> 
                    <input id="total_liabilities_15_16" type="text" data-name="total_liabilities_15" class="total_liabilities_15 form-control onlyDigit" onkeyup="total_liabilities_15_word.innerHTML=convertNumberToWords(this.value)" readonly><div id="total_liabilities_15_word"></div>
                </div>
            </td>
            
            <td>
                <div class="form-group">
                    <label class="hidden" for="total_liabilities_14_15">Total liabilities  2014-2015</label>
                    <i class="fa fa-inr" aria-hidden="true"></i> 
                    <input id="total_liabilities_14_15" type="text" data-name="total_liabilities_14" class="total_liabilities_14 form-control onlyDigit" onkeyup="total_liabilities_14_word.innerHTML=convertNumberToWords(this.value)" readonly><div id="total_liabilities_14_word"></div>
                </div>
            </td>
        </tr>
        
        
        
        <tr>
            <td><strong class="main-td">(C)NET WORTH</strong></td>
            <td><div class="form-group">
                <label class="hidden" for="net_assets_net_worth_17_18"> NET ASSETS (NET WORTH) 2017-2018</label>
                <i class="fa fa-inr" aria-hidden="true"></i>
                <input id="net_assets_net_worth_17_18" type="text" data-name="net_worth_17" class="net_worth_17  form-control onlyDigit" onkeyup="net_worth_17_word.innerHTML=convertNumberToWords(this.value)" readonly><div id="net_worth_17_word"></div>
                </div>
            </td>
            
            <td>
                <div class="form-group">
                    <label class="hidden" for="net_assets_net_worth_16_17"> NET ASSETS (NET WORTH) 2016-2017</label>
                    <i class="fa fa-inr" aria-hidden="true"></i>
                    <input id="net_assets_net_worth_16_17" type="text" data-name="net_worth_16" class="net_worth_16 form-control onlyDigit" onkeyup="net_worth_16_word.innerHTML=convertNumberToWords(this.value)" readonly><div id="net_worth_16_word"></div>
                </div>
            </td>
            
            <td>
                <div class="form-group">
                    <label class="hidden" for="net_assets_net_worth_15_16"> NET ASSETS (NET WORTH) 2015-2016</label>
                    <i class="fa fa-inr" aria-hidden="true"></i> 
                    <input id="net_assets_net_worth_15_16" type="text" data-name="net_worth_15" class="net_worth_15 form-control onlyDigit" onkeyup="net_worth_15_word.innerHTML=convertNumberToWords(this.value)" readonly><div id="net_worth_15_word"></div>
                </div>
            </td>
            
            <td>
                <div class="form-group">
                    <label class="hidden" for="net_assets_net_worth_14_15"> NET ASSETS (NET WORTH) 2014-2015</label>
                    <i class="fa fa-inr" aria-hidden="true"></i> 
                    <input id="net_assets_net_worth_14_15" type="text" data-name="net_worth_14" class="net_worth_14 form-control onlyDigit" onkeyup="net_worth_14_word.innerHTML=convertNumberToWords(this.value)" readonly><div id="net_worth_14_word"></div>
                </div>
            </td>
        </tr>
        
        
        <tr>
            <td><strong class="main-td">Shareholders Equity </strong></td>
            <td><div class="form-group">
                <label class="hidden" for="shareholders_equity_17_18">Shareholders Equity  2017-2018</label>
                <i class="fa fa-inr" aria-hidden="true"></i>
                <input id="shareholders_equity_17_18" type="text" data-name="shareholders_equity_17" class="shareholders_equity_17  form-control onlyDigit" onkeyup="shareholders_equity_17_word.innerHTML=convertNumberToWords(this.value)" required><div id="shareholders_equity_17_word"></div>
                </div>
            </td>
            
            <td>
                <div class="form-group">
                    <label class="hidden" for="shareholders_equity_16_17">Shareholders Equity  2016-2017</label>
                    <i class="fa fa-inr" aria-hidden="true"></i>
                    <input id="shareholders_equity_16_17" type="text" data-name="shareholders_equity_16" class="shareholders_equity_16 form-control onlyDigit" onkeyup="shareholders_equity_16_word.innerHTML=convertNumberToWords(this.value)" required><div id="shareholders_equity_16_word"></div>
                </div>
            </td>
            
            <td>
                <div class="form-group">
                    <label class="hidden" for="shareholders_equity_15_16">Shareholders Equity  2015-2016</label>
                    <i class="fa fa-inr" aria-hidden="true"></i> 
                    <input id="shareholders_equity_15_16" type="text" data-name="shareholders_equity_15" class="shareholders_equity_15 form-control  onlyDigit" onkeyup="shareholders_equity_15_word.innerHTML=convertNumberToWords(this.value)" required><div id="shareholders_equity_15_word"></div>
                </div>
            </td>
            
            <td>
                <div class="form-group">
                    <label class="hidden" for="shareholders_equity_14_15">Shareholders Equity  2014-2015</label>
                    <i class="fa fa-inr" aria-hidden="true"></i> 
                    <input id="shareholders_equity_14_15" type="text" data-name="shareholders_equity_14" class="shareholders_equity_14 form-control onlyDigit" onkeyup="shareholders_equity_14_word.innerHTML=convertNumberToWords(this.value)" required><div id="shareholders_equity_14_word"></div>
                </div>
            </td>
        </tr>
        
        <tr>
            <td><strong class="main-td2">Capital</strong></td>
            <td><div class="form-group">
                <label class="hidden" for="capital_17_18">Capital 2017-2018</label>
                <i class="fa fa-inr" aria-hidden="true"></i>
                <input id="capital_17_18" type="text" data-name="capital_17" class="capital_17  form-control onlyDigit" onkeyup="capital_17_word.innerHTML=convertNumberToWords(this.value)" required><div id="capital_17_word"></div>
                </div>
            </td>
            
            <td>
                <div class="form-group">
                    <label class="hidden" for="capital_16_17">Capital 2016-2017</label>
                    <i class="fa fa-inr" aria-hidden="true"></i>
                    <input id="capital_16_17" type="text" data-name="capital_16" class="capital_16 form-control onlyDigit" onkeyup="capital_16_word.innerHTML=convertNumberToWords(this.value)" required><div id="capital_16_word"></div>
                </div>
            </td>
            
            <td>
                <div class="form-group">
                    <label class="hidden" for="capital_15_16">Capital 2015-2016</label>
                    <i class="fa fa-inr" aria-hidden="true"></i> 
                    <input id="capital_15_16" type="text" data-name="capital_15" class="capital_15 form-control onlyDigit" onkeyup="capital_15_word.innerHTML=convertNumberToWords(this.value)" required><div id="capital_15_word"></div>
                </div>
            </td>
            
            <td>
                <div class="form-group">
                    <label class="hidden" for="capital_14_15">Capital 2014-2015</label>
                    <i class="fa fa-inr" aria-hidden="true"></i> 
                    <input id="capital_14_15" type="text" data-name="capital_14" class="capital_14 form-control onlyDigit" onkeyup="capital_14_word.innerHTML=convertNumberToWords(this.value)" required><div id="capital_14_word"></div>
                </div>
            </td> 
        </tr>
        
        <tr>
            <td><strong class="main-td2">Additional paid-in capital</strong></td>
            <td><div class="form-group">
                <label class="hidden" for="additional_paid_in_capital_17_18">Additional paid-in capital 2017-2018</label>
                <i class="fa fa-inr" aria-hidden="true"></i>
                <input id="additional_paid_in_capital_17_18" type="text" data-name="additional_paid_in_capital_17" class="additional_paid_in_capital_17  form-control onlyDigit" onkeyup="additional_paid_in_capital_17_word.innerHTML=convertNumberToWords(this.value)" required><div id="additional_paid_in_capital_17_word"></div>
                </div>
            </td>
            
            <td>
                <div class="form-group">
                    <label class="hidden" for="additional_paid_in_capital_16_17">Additional paid-in capital 2016-2017</label>
                    <i class="fa fa-inr" aria-hidden="true"></i>
                    <input id="additional_paid_in_capital_16_17" type="text" data-name="additional_paid_in_capital_16" class="additional_paid_in_capital_16 form-control onlyDigit" onkeyup="additional_paid_in_capital_16_word.innerHTML=convertNumberToWords(this.value)" required><div id="additional_paid_in_capital_16_word"></div>
                </div>
            </td>
            
            <td>
                <div class="form-group">
                    <label class="hidden" for="additional_paid_in_capital_15_16">Additional paid-in capital 2015-2016</label>
                    <i class="fa fa-inr" aria-hidden="true"></i> 
                    <input id="additional_paid_in_capital_15_16" type="text" data-name="additional_paid_in_capital_15" class="additional_paid_in_capital_15 form-control onlyDigit" onkeyup="additional_paid_in_capital_15_word.innerHTML=convertNumberToWords(this.value)" required><div id="additional_paid_in_capital_15_word"></div>
                </div>
            </td>
            
            <td>
                <div class="form-group">
                    <label class="hidden" for="additional_paid_in_capital_14_15">Additional paid-in capital 2014-2015</label>
                    <i class="fa fa-inr" aria-hidden="true"></i> 
                    <input id="additional_paid_in_capital_14_15" type="text" data-name="additional_paid_in_capital_14" class="additional_paid_in_capital_14 form-control onlyDigit" onkeyup="additional_paid_in_capital_14_word.innerHTML=convertNumberToWords(this.value)" required><div id="additional_paid_in_capital_14_word"></div>
                </div>
            </td>
        </tr>
        
        <tr>
            <td><strong class="main-td2">Retained earnings </strong></td>
            <td><div class="form-group">
                <label class="hidden" for="retained_earnings_17_18">Retained earnings 2017-2018</label>
                <i class="fa fa-inr" aria-hidden="true"></i>
                <input id="retained_earnings_17_18" type="text" data-name="retained_earnings_17" class="retained_earnings_17  form-control onlyDigit" onkeyup="retained_earnings_17_word.innerHTML=convertNumberToWords(this.value)" required><div id="retained_earnings_17_word"></div>
                </div>
            </td>
            
            <td>
                <div class="form-group">
                    <label class="hidden" for="retained_earnings_16_17">Retained earnings 2016-2017</label>
                    <i class="fa fa-inr" aria-hidden="true"></i>
                    <input id="retained_earnings_16_17" type="text" data-name="retained_earnings_16" class="retained_earnings_16 form-control onlyDigit" onkeyup="retained_earnings_16_word.innerHTML=convertNumberToWords(this.value)" required><div id="retained_earnings_16_word"></div>
                </div>
            </td>
            
            <td>
                <div class="form-group">
                    <label class="hidden" for="retained_earnings_15_16">Retained earnings 2015-2016</label>
                    <i class="fa fa-inr" aria-hidden="true"></i> 
                    <input id="retained_earnings_15_16" type="text" data-name="retained_earnings_15" class="retained_earnings_15 form-control onlyDigit" onkeyup="retained_earnings_15_word.innerHTML=convertNumberToWords(this.value)" required><div id="retained_earnings_15_word"></div>
                </div>
            </td>
            
            <td>
                <div class="form-group">
                    <label class="hidden" for="retained_earnings_14_15">Retained earnings 2014-2015</label>
                    <i class="fa fa-inr" aria-hidden="true"></i> 
                    <input id="retained_earnings_14_15" type="text" data-name="retained_earnings_14" class="retained_earnings_14 form-control onlyDigit" onkeyup="retained_earnings_14_word.innerHTML=convertNumberToWords(this.value)" required><div id="retained_earnings_14_word"></div>
                </div>
            </td>
        </tr>
        
        <tr>
            <td><strong class="main-td">Total equity</strong></td>
            <td><div class="form-group">
                <label class="hidden" for="total_equity_17_18">Total equity 2017-2018</label>
                <i class="fa fa-inr" aria-hidden="true"></i>
                <input id="total_equity_17_18" type="text" data-name="total_equity_17" class="total_equity_17  form-control onlyDigit" onkeyup="total_equity_17_word.innerHTML=convertNumberToWords(this.value)" required><div id="total_equity_17_word"></div>
                </div>
            </td>
            
            <td>
                <div class="form-group">
                    <label class="hidden" for="total_equity_16_17">Total equity 2016-2017</label>
                    <i class="fa fa-inr" aria-hidden="true"></i>
                    <input id="total_equity_16_17" type="text" data-name="total_equity_16" class="total_equity_16 form-control onlyDigit" onkeyup="total_equity_16_word.innerHTML=convertNumberToWords(this.value)" required><div id="total_equity_16_word"></div>
                </div>
            </td>
            
            <td>
                <div class="form-group">
                    <label class="hidden" for="total_equity_15_16">Total equity 2015-2016</label>
                    <i class="fa fa-inr" aria-hidden="true"></i> 
                    <input id="total_equity_15_16" type="text" data-name="total_equity_15" class="total_equity_15 form-control onlyDigit" onkeyup="total_equity_15_word.innerHTML=convertNumberToWords(this.value)" required><div id="total_equity_15_word"></div>
                </div>
            </td>
            
            <td>
                <div class="form-group">
                    <label class="hidden" for="total_equity_14_15">Total equity 2014-2015</label>
                    <i class="fa fa-inr" aria-hidden="true"></i> 
                    <input id="total_equity_14_15" type="text" data-name="total_equity_14" class="total_equity_14 form-control onlyDigit" onkeyup="total_equity_14_word.innerHTML=convertNumberToWords(this.value)" required><div id="total_equity_14_word"></div>
                </div>
            </td>
        </tr>
        
        <tr>
            <td><strong class="main-td2"></strong></td>
            
        </tr>
        
        <tr>
            <td><strong class="main-td">Total liabilities and equity</strong></td>
            <td><div class="form-group">
                <label class="hidden" for="total_liabilities_and_quity_17_18">Total liabilities and equity 2017-2018</label>
                <i class="fa fa-inr" aria-hidden="true"></i>
                <input id="total_liabilities_and_quity_17_18" type="text" data-name="total_liabilities_and_quity_17" class="total_liabilities_and_quity_17  form-control onlyDigit" onkeyup="total_liabilities_and_quity_17_word.innerHTML=convertNumberToWords(this.value)" required><div id="total_liabilities_and_quity_17_word"></div>
                </div>
            </td>
            
            <td>
                <div class="form-group">
                    <label class="hidden" for="total_liabilities_and_quity_16_17">Total liabilities and equity 2016-2017</label>
                    <i class="fa fa-inr" aria-hidden="true"></i>
                    <input id="total_liabilities_and_quity_16_17" type="text" data-name="total_liabilities_and_quity_16" class="total_liabilities_and_quity_16 form-control onlyDigit" onkeyup="total_liabilities_and_quity_16_word.innerHTML=convertNumberToWords(this.value)" required><div id="total_liabilities_and_quity_16_word"></div>
                </div>
            </td>
            
            <td>
                <div class="form-group">
                    <label class="hidden" for="total_liabilities_and_quity_15_16">Total liabilities and equity 2015-2016</label>
                    <i class="fa fa-inr" aria-hidden="true"></i> 
                    <input id="total_liabilities_and_quity_15_16" type="text" data-name="total_liabilities_and_quity_15" class="total_liabilities_and_quity_15 form-control onlyDigit" onkeyup="total_liabilities_and_quity_15_word.innerHTML=convertNumberToWords(this.value)" required><div id="total_liabilities_and_quity_15_word"></div>
                </div>
            </td>
            
            <td>
                <div class="form-group">
                    <label class="hidden" for="total_liabilities_and_quity_14_15">Total liabilities and equity 2014-2015</label>
                    <i class="fa fa-inr" aria-hidden="true"></i> 
                    <input id="total_liabilities_and_quity_14_15" type="text" data-name="total_liabilities_and_quity_14" class="total_liabilities_and_quity_14 form-control onlyDigit" onkeyup="total_liabilities_and_quity_14_word.innerHTML=convertNumberToWords(this.value)" required><div id="total_liabilities_and_quity_14_word"></div>
                </div>
            </td>
        </tr>
        <!-- ...-->  
        
    </table> 
    <input type="hidden" name="balanceJson" value="" >
</div>