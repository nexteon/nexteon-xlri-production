package com.xlri.awards.config.security;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import com.xlri.awards.dao.entity.LogTable;
import com.xlri.awards.dao.service.api.LogTableService;
import com.xlri.awards.dao.service.impl.LoginAttemptService;


@Component
public class CustomAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

	@Autowired
	private LoginAttemptService loginAttemptService;
	
	@Autowired
	private LogTableService logTableService;
	
	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		
		setDefaultFailureUrl("/login?error=true");
		
		super.onAuthenticationFailure(request, response, exception);
		
		loginAttemptService.loginFailed(request.getRemoteAddr());
		LogTable logTable = new LogTable();
		logTable.setIpAddress(request.getRemoteAddr());
		logTable.setLoginType("Login Failed");
		logTable.setTimestamp(new Date());
		logTable.setUsername(request.getParameter("emailId"));
		String errorMessage = "Invalid Username or Password";
		if (exception.getMessage().contains("blocked")) {
			errorMessage = "You have reached maximum number of login Attempt. Please try after 10 minutes.";
		}
		logTableService.saveLog(logTable);
		request.getSession().setAttribute(WebAttributes.AUTHENTICATION_EXCEPTION, errorMessage);
	}
}