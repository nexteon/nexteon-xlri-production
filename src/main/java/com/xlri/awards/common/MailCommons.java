package com.xlri.awards.common;

import java.util.Properties;

import javax.mail.Session;
import javax.mail.internet.MimeMessage;

public class MailCommons {

	public MimeMessage mimeMessage(){
		
        Properties javaMailProperties = new Properties();
        javaMailProperties.put("mail.smtp.host", "relay.nic.in");
        javaMailProperties.put("mail.smtp.socketFactory.port", 25);
        javaMailProperties.put("mail.smtp.port", 25);       
        
        Session session = Session.getInstance(javaMailProperties);
        MimeMessage message = new MimeMessage(session); 
        return message;
	}
	
	
}
