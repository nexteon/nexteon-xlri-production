<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script src="/resources/dashboard/js/workingjs/jquery.dataTables.min.new.js"></script>
    
<link rel="stylesheet" type="text/css" href="/resources/dashboard/css/workingcss/jquery.dataTables.min.css">
    
    <link rel="stylesheet" type="text/css" href="/resources/dashboard/css/workingcss/buttons.dataTables.min.css">
    
    <script src="/resources/dashboard/js/workingjs/dataTables.buttons.min.js"></script>
    <script src="/resources/dashboard/js/workingjs/jszip.min.js"></script>
    <script src="/resources/dashboard/js/workingjs/pdfmake.min.js"></script>
    <script src="/resources/dashboard/js/workingjs/vfs_fonts.js"></script>
    <script src="/resources/dashboard/js/workingjs/buttons.html5.min.js"></script>