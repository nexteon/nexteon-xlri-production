<div class="give-padding"></div>
<div class="file-sheet">
    <div class="form-group">
        <label class="" for="balance_sheet">5. Balance Sheet (Last 4 years audited balance sheet)<span class="mandate">*</span></label>
        <div class="input-file1">
            <input id="balance_sheet" type="file" name="file1" class="form-control"  onchange="ValidateSize(this)" accept="application/pdf,image/jpeg" multiple required>
            <div id="myProgress1">
                <div id="myBar1">0%</div>
            </div>
            <button class="upload-btn1 btn btn-primary" onclick="move()">Upload</button>
            <a href="javascript:void(0)" class="close-file1 btn btn-primary">X</a>
        </div>
        <div class="help-block with-errors"></div>
    </div>

    <div class="form-group">
        <label class="" for="profit_loss_statement">6. Profit & Loss Statement (Last 4 years)<span class="mandate">*</span></label>
        <div class="input-file2">
            <input id="profit_loss_statement" type="file" name="file2" class="form-control" onchange="ValidateSize(this)" accept="application/pdf,image/jpeg" multiple>
            <div id="myProgress2">
                <div id="myBar2">0%</div>
            </div>
            <button class="upload-btn2 btn btn-primary" onclick="move()">Upload</button>
            <a href="javascript:void(0)" class="close-file2 btn btn-primary">X</a>
        </div>
        <div class="help-block with-errors"></div>
    </div>
    <div class="form-group">
        <label class="" for="cash_flow_statement">7. Cash Flow Statement (Last 4 years)<span class="mandate">*</span></label>
        <div class="input-file3">
            <input id="cash_flow_statement" type="file" name="file3" class="form-control" onchange="ValidateSize(this)" accept="application/pdf,image/jpeg" multiple>
            <div id="myProgress3">
                <div id="myBar3">0%</div>
            </div>
            <button class="upload-btn3 btn btn-primary" onclick="move()">Upload</button>
            <a href="javascript:void(0)" class="close-file3 btn btn-primary">X</a>
        </div>
        <div class="help-block with-errors"></div>
    </div>
    <div class="form-group">
        <label class="" for=" moa">8. Memorandum of Association (MoA) (of the enterprise and/or of the co-producers)<span class="mandate">*</span></label>
        <div class="input-file4">
            <input id="moa" type="file" name="file4" class="form-control" onchange="ValidateSize(this)" accept="application/pdf,image/jpeg" multiple>
            <div id="myProgress4">
                <div id="myBar4">0%</div>
            </div>
            <button class="upload-btn4 btn btn-primary" onclick="move()">Upload</button>
            <a href="javascript:void(0)" class="close-file4 btn btn-primary">X</a>
        </div>
        <div class="help-block with-errors"></div>
    </div>
    <div class="form-group">
        <label class="" for=" aoa">9. Articles of Association (AoA) (of the enterprise and/or of the co-producers)<span class="mandate">*</span></label>
        <div class="input-file5">
            <input id="aoa" type="file" name="file5" class="form-control" onchange="ValidateSize(this)" accept="application/pdf,image/jpeg" multiple>
            <div id="myProgress5">
                <div id="myBar5">0%</div>
            </div>
            <button class="upload-btn5 btn btn-primary" onclick="move()">Upload</button>
            <a href="javascript:void(0)" class="close-file5 btn btn-primary">X</a>
        </div>
        <div class="help-block with-errors"></div>
    </div>
    <div class="form-group">
        <label class="" for="mandatory_industry_certifications">10. Mandatory Industry Certifications<span class="mandate">*</span></label>
        <div class="input-file6">
            <input id="mandatory_industry_certifications" type="file" name="file6" class="form-control" onchange="ValidateSize(this)" accept="application/pdf,image/jpeg" multiple>
            <div id="myProgress6">
                <div id="myBar6">0%</div>
            </div>
            <button class="upload-btn6 btn btn-primary" onclick="move()">Upload</button>
            <a href="javascript:void(0)" class="close-file6 btn btn-primary">X</a>
        </div>
        <div class="help-block with-errors"></div>
    </div>
    <div class="form-group">
        <label class="" for="organization_registration_certificate">11. Organization Registration Certificate<span class="mandate">*</span></label>
        <div class="input-file7">
            <input id="organization_registration_certificate" type="file" name="file7" class="form-control" onchange="ValidateSize(this)" accept="application/pdf,image/jpeg" multiple>
            <div id="myProgress7">
                <div id="myBar7">0%</div>
            </div>
            <button class="upload-btn7 btn btn-primary" onclick="move()">Upload</button>
            <a href="javascript:void(0)" class="close-file7 btn btn-primary">X</a>
        </div>
        <div class="help-block with-errors"></div>
    </div>
    <hr>
    <div class="form-group">
        <h4>Declaration</h4>
    </div>
    <div class="form-group">
        <label class="checkbox-inline"><input type="checkbox" required >Nominee/Entrepreneur declares that I have not received the NEA Awards in any of the previous editions.</label>
        <div class="help-block with-errors"></div>
    </div>
    <div class="form-group">
        <label class="checkbox-inline"><input type="checkbox" required >Nominee/Entrepreneur is a First Generation Entrepreneur. The definition of a first generation entrepreneur for the scheme is "An entrepreneur who is not in the same line of business as his/her parent. They should have started the enterprise on their own initiative, idea or innovation".</label>
        <div class="help-block with-errors"></div>
    </div>
    <div class="form-group">
        <label class="checkbox-inline"><input type="checkbox" required >The enterprise is duly registered and has at least last four financial years of audited reports from 2014-15, 2015-16, 2016-17 and 2017-18.</label>
        <div class="help-block with-errors"></div>
    </div>
    <div class="form-group">
        <label class="checkbox-inline"><input type="checkbox" required >Initial investment of the enterprise is between INR 10lakh up to INR 1 crore.</label>
        <div class="help-block with-errors"></div>
    </div>
    <div class="form-group">
        <label class="checkbox-inline"><input type="checkbox" required >The enterprise has mandatory industry certifications (example: FDA, CE, FSSAI, MSME etc.)</label>
        <div class="help-block with-errors"></div>
    </div>
    <div class="form-group">
        <label class="checkbox-inline"><input type="checkbox" required >By participating in the National Entrepreneurship Awards 2018, the nominee agrees to receive mobile SMS, phone calls and email communication.</label>
        <div class="help-block with-errors"></div>
    </div>
    <div class="form-group">
        <label class="checkbox-inline"><input type="checkbox" required >I / We hereby declare that all the information provided by me / us is true and correct to the best of my/our knowledge. Any false information provided in the context of National Entrepreneurship Awards Scheme by the nominee may result in the immediate elimination from the Award process.</label>
        <div class="help-block with-errors"></div>
    </div>
</div>
<!--end of class balance sheet-->
<div class="clearfix"></div>