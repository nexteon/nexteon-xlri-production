package com.xlri.awards.config;

import java.util.Properties;

import javax.mail.Session;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:application.properties")
@ComponentScan(basePackages="com.xlri.awards")
public class MailConfig {

	@Autowired
	Environment env;
	
	@Bean
	public MimeMessage mimeMessage(){
		
        Properties javaMailProperties = new Properties();
        javaMailProperties.put("mail.smtp.host", env.getProperty("mail.smtp.host"));
        javaMailProperties.put("mail.smtp.socketFactory.port", env.getProperty("mail.smtp.socketFactory.port"));
        javaMailProperties.put("mail.smtp.port", env.getProperty("mail.smtp.port"));       
        
        Session session = Session.getInstance(javaMailProperties);
        MimeMessage message = new MimeMessage(session); 
        return message;
	}
	
}
