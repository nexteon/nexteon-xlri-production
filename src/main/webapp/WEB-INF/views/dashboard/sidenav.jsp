<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<div class="page-sidebar-wrapper">
                    <!-- BEGIN SIDEBAR -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <div class="page-sidebar navbar-collapse collapse">
                        
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                            
                            <li class="nav-item">
                                <a href="/userdata" class="nav-link">
                                    <i class="icon-home"></i>
                                    <span class="title">Dashboard</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>
                                
                            </li>
                            
                           
                            <c:if test ="${isAdmin || isRegional}">
                                                       
                            <li class="nav-item ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-briefcase"></i>
                                    <span class="title">Experts</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                <c:if test ="${isRegional}">
                                    <li class="nav-item  ">
                                        <a href="/neas/api/addexpert" class="nav-link">
                                            <span class="title">Add Experts</span>
                                        </a>
                                    </li>
                                    </c:if>
                                    <li class="nav-item  ">
                                        <a href="/allexperts" class="nav-link ">
                                            <span class="/allexperts">All Experts</span>
                                        </a>
                                    </li>
                                  
                                        </ul>
                                        
                                        
                                    </li> 
                                    <c:if test ="${isAdmin}">                          
                             <li class="nav-item ">
                                <a href="/newssectionget" class="nav-link">
                                    <i class="icon-briefcase"></i>
                                    <span class="title">News Section</span>
                                 </a>                                   
                             </li>
                           </c:if>
                           </c:if>
                            <c:if test ="${isAdmin}">
                            <li class="nav-item ">
                                <a href="/category/misreport" class="nav-link ">
                                    <i class="icon-briefcase"></i>
                                    <span class="title">MIS Report</span>
                                    <span class="arrow"></span>
                                </a>
                            </li>
                            <li class="nav-item ">
                                <a href="/upload/shortlisted" class="nav-link ">
                                    <i class="icon-briefcase"></i>
                                    <span class="title">Upload Shortlisted Data</span>
                                    <span class="arrow"></span>
                                </a>
                            </li>
                            </c:if>
                            <c:if test ="${isAdmin}">
	                            <li class="nav-item ">
	                                <a href="/upload/show/shortlisted" class="nav-link">
	                                    <i class="icon-briefcase"></i>
	                                    <span class="title">View Shortlisted Data</span>
	                                    <span class="arrow"></span>
	                                </a>
	                            </li>    
                            </c:if>
                            <c:if test ="${isAdmin}">
	                            <li class="nav-item ">
	                                <a href="/partnerslanding" class="nav-link">
	                                    <i class="icon-briefcase"></i>
	                                    <span class="title">Partners</span>
	                                    <span class="arrow"></span>
	                                </a>
	                            </li>    
                           </c:if>
                           <c:if test ="${isAdmin}">
	                            <li class="nav-item ">
	                                <a href="/promotednominee" class="nav-link">
	                                    <i class="icon-briefcase"></i>
	                                    <span class="title">Promoted Nominee(Regional)</span>
	                                    <span class="arrow"></span>
	                                </a>
	                            </li>    
                           </c:if>
                           
                        </ul>
                        <!-- END SIDEBAR MENU -->
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
                </div>