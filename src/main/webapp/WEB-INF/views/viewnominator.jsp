<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">
   <%@include file="dashboard/headerlibs.jsp" %>
   <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white" id="page-top" >
   			<div class="page-wrapper">
          <%@include file="dashboard/header.jsp" %>
           

				
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="/userdata">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Nominator View</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> Nominator View</h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <!-- BEGIN DASHBOARD STATS 1-->

        
		<!-- Form Container -->
		
		 <div class="row">
                        
                        <div class="col-md-12">
                            
                        <div class="portlet box blue form-display1">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            Registration Number:  ${nominatorr.registrationNumber}</div>
                                        
                                    </div>
                                    <div class="portlet-body">
                                      <div class="col-xs-12 form-1-box">
											<div class="row">
												<div class="col-sm-6">

													<div class="row">
														<div class="col-lg-12">
															<label>Nominator's Name</label>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-12">
															<input type="text" class="form-control" value="${nominatorr.fullName}" readonly="">
														</div>
													</div>
													
												</div>
												
												<c:if test="${nominatorr.nominationType}">
												<div class="col-sm-6">
													<div class="row">
														<div class="col-lg-12">
															<label>Institution/Organization Name</label>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-12">
															<input type="text" class="form-control" value="${nominatorr.instituteName}" readonly="">
														</div>
													</div>
												</div>
												</c:if>


											</div>
											<div class="row">
											<c:if test="${nominatorr.nominationType == 'organisation'}">
												<div class="col-sm-6">

													<div class="row">
														<div class="col-lg-12">
															<label>Designation </label>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-12">
															<input type="text" class="form-control" value="${nominatorr.designation}" readonly="">
														</div>
													</div>
													
												</div>
												</c:if>


												<div class="col-sm-6">
													<div class="row">
														<div class="col-lg-12">
															<label>Phone No</label>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-12">
															<input type="text" class="form-control" value="${nominatorr.contactNumber}" readonly="">
														</div>
													</div>
												</div>


											</div>
											<div class="row">
											<c:if test="${nominatorr.nominationType == 'individual'}">
												<div class="col-sm-6">

													<div class="row">
														<div class="col-lg-12">
															<label>Occupation </label>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-12">
															<input type="text" class="form-control" value="${nominatorr.occupation}" readonly="">
														</div>
													</div>
													
												</div>
												</c:if>


												<div class="col-sm-6">
													<div class="row">
														<div class="col-lg-12">
															<label>Pincode</label>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-12">
															<input type="text" class="form-control" value="${nominatorr.addrPinCode}" readonly="">
														</div>
													</div>
												</div>


											</div>
											<div class="row">
												<div class="col-sm-6">

													<div class="row">
														<div class="col-lg-12">
															<label>Website(If Available)</label>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-12">
															<input type="text" class="form-control" value="${nominatorr.website}" readonly="">
														</div>
													</div>
													
												</div>


												<div class="col-sm-6">
													<div class="row">
														<div class="col-lg-12">
															<label>House No., Mohalla, Village</label>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-12">
															<input type="text" class="form-control" value="${nominatorr.addrLine1}" readonly="">
														</div>
													</div>
												</div>


											</div>
											<div class="row">
												<div class="col-sm-6">

													<div class="row">
														<div class="col-lg-12">
															<label>District</label>
														</div>
													</div>
													
													<div class="row">
														<div class="col-lg-12">
															<input type="text" class="form-control" value="${nominatorr.addrCity}" readonly="">
														</div>
													</div>
													
													
													
												</div>


												<div class="col-sm-3">
													<div class="row">
														<div class="col-lg-12">
															<label>State</label>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-12">
															<input type="text" class="form-control" value="${nominatorr.addrState}" readonly="">
														</div>
													</div>
												</div>
												
												<div class="col-sm-3">
													<div class="row">
														<div class="col-lg-12">
															<label>Pin Code</label>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-12">
															<input type="text" class="form-control" value="${nominatorr.addrPinCode}" readonly="">
														</div>
													</div>
												</div>


											</div>
											<div class="row">
												<div class="col-sm-6">

													<div class="row">
														<div class="col-lg-12">
															<label>Landmark </label>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-12">
															<input type="text" class="form-control" value="${nominatorr.addrLandmark}" readonly="">
														</div>
													</div>
													
												</div>


												<div class="col-sm-6">
													<div class="row">
														<div class="col-lg-12">
															<label>Email</label>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-12">
															<input type="text" class="form-control" value="${nominatorr.emailId}" readonly="">
														</div>
													</div>
												</div>


											</div>
											<button type="button" class="btn btn-warning" id="back-btn">BACK</button>
											</div>
											
											
											
											</div>
                                        
                                      <%--   <div class="col-xs-12 form-1-box">
									<div class="panel panel-info">
										<div class="panel-heading">
											<center><h3 class="panel-title">Accept/Reject Application for Evalutation</h3></center>
										</div>
										<div class="panel-body">
										
											<form action="#" name="firststep" id="partnerEvalutionForm" method="post">	
											<input type="text" name="app_id" class="form-control" value="109" style="display:none">
											<select name="p_evalute_control" class="form-control" style="width: 40%; float: left;" id="p_evalute_control_id">
											<option value="">-- SELECT --</option>
											<option value="1">Accepted</option>
											<option value="2" selected="">Rejected</option>
											</select>
											<input type="submit" value="SUBMIT" style="float: left; margin-left: 10px;" class="btn btn-primary" name="save">
											</form>
									
								
										</div>
									</div>
								</div> --%>
                                    </div>
                                </div>    
                            
                        </div>
                        
		      </div>
         
        <!-- END QUICK NAV -->
     <%@include file="dashboard/footer.jsp"%>
   <style>
        .form-display1 .portlet-body{float:left;width:100%}
.portlet.box.blue{float:left;width:100%}
.col-xs-12.form-1-box{padding:15px;border:solid 1px #ccc;margin-bottom:30px}
.form-display1 .form-control{margin-bottom:15px}
.form-display1 label{font-weight:600;font-size:14px;padding-bottom:4px}
.formsecondpart{padding-top:15px!important}
        
        </style>
 
 
 <script>
	$(document).ready(function(){
		$('#back-btn').click(function(){
   			window.location.href="/userdata?nominatorback";
   		});
	});
</script>
     
     </body>
         </html>
                    
