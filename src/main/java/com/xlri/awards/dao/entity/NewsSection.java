package com.xlri.awards.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="NewsSection")
public class NewsSection {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	
	private Long id;
	
	private String title;
	
	private String subCategory;
	
	@Column(length=100000)
	private String description;
	
	@Temporal(TemporalType.DATE)
    private Date createdOn;
	
	private String activatedStatus;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn() {
		this.createdOn = new Date();
	}

	public String getActivatedStatus() {
		return activatedStatus;
	}

	public void setActivatedStatus(String activatedStatus) {
		this.activatedStatus = activatedStatus;
	}
	
	
	
	
	
	

}
