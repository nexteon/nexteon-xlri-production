package com.xlri.awards.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import com.xlri.awards.common.EncryptionUtil;

@Entity
@Table(name = "NomineeDetails",uniqueConstraints=@UniqueConstraint(columnNames={"emailId","contact"}))
public class NomineeDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable=false)
	private String registrationNumber;
	
	@Column(nullable=false)
	private String awardCategory;
	
	private String enterpriseCategory;
	
	private String awardSub;
	
	private String specifyproduct;
	
	@Column(nullable=false)
	private String nomineeName;
	
	@Column(nullable=false)
	private String gender;
	
	private String socialCategory;
	
	private String dob;
	
	private boolean pwd;
	
	private boolean speccatopt;
	
	private String speccatval;
	
	@Column(nullable=false)
	private String enterpriseName;
	
	private String address1;
	
	private String address2;
	
	private String city;
	
	private String state;
	
	private String landmark;
	
	private String street;
	
	private String pinCode;
	
	private String website;
	
	@Column(nullable=false)
	private String contact;
	
	@Column(nullable=false)
	private String emailId;
	
    private String role;
    
	@Column(nullable=false)
	private String password;
	
	private String salt;
	
	@Column(length=100000)
	private String ratings;
	
	@Column(length=1050)
	private String knowNominee;
	
	@Column(length=1050)
	private String roleNominee;
    
	@Column(length=1050)
	private String impactNominee;
    
	@Column(length=1050)
	private String recognitionNominee;
	
	@Column(length=1050)
	private String collaborationNominee;

	@Column(length=1050)
	private String specMentionNominee;
	
	private String filledStatus;
	
	private String formula;
	
	@Temporal(TemporalType.DATE)
    private Date createdOn;
	
	private String remark;

	private String fileName;
	
	private String shortlisted;
	
	@Column(length=10)
	private String ranking;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date scoreUpdate;
	
	private String promoteStatus;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public String getAwardCategory() {
		return awardCategory;
	}

	public void setAwardCategory(String awardCategory) {
		this.awardCategory = awardCategory;
	}

	public String getEnterpriseCategory() {
		return enterpriseCategory;
	}

	public void setEnterpriseCategory(String enterpriseCategory) {
		this.enterpriseCategory = enterpriseCategory;
	}

	public String getAwardSub() {
		return awardSub;
	}

	public void setAwardSub(String awardSub) {
		this.awardSub = awardSub;
	}

	public String getSpecifyproduct() {
		return specifyproduct;
	}

	public void setSpecifyproduct(String specifyproduct) {
		this.specifyproduct = specifyproduct;
	}

	public String getNomineeName() {
		return nomineeName;
	}

	public void setNomineeName(String nomineeName) {
		this.nomineeName = nomineeName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getSocialCategory() {
		return socialCategory;
	}

	public void setSocialCategory(String socialCategory) {
		this.socialCategory = socialCategory;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public boolean isPwd() {
		return pwd;
	}

	public void setPwd(boolean pwd) {
		this.pwd = pwd;
	}

	public boolean getSpeccatopt() {
		return speccatopt;
	}

	public void setSpeccatopt(boolean speccatopt) {
		this.speccatopt = speccatopt;
	}

	public String getSpeccatval() {
		return speccatval;
	}

	public void setSpeccatval(String speccatval) {
		this.speccatval = speccatval;
	}

	public String getEnterpriseName() {
		return enterpriseName;
	}

	public void setEnterpriseName(String enterpriseName) {
		this.enterpriseName = enterpriseName;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getLandmark() {
		return landmark;
	}

	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.getNewSalt();
		this.password = EncryptionUtil.encryptPass(password + this.getSalt());
	}

	public String getSalt() {
		return salt;
	}
	
	private void getNewSalt() {
		this.salt = EncryptionUtil.generateRandomString();
	}

	public String getRatings() {
		return ratings;
	}

	public void setRatings(String ratings) {
		this.ratings = ratings;
	}

	public String getKnowNominee() {
		return knowNominee;
	}

	public void setKnowNominee(String knowNominee) {
		this.knowNominee = knowNominee;
	}

	public String getRoleNominee() {
		return roleNominee;
	}

	public void setRoleNominee(String roleNominee) {
		this.roleNominee = roleNominee;
	}

	public String getImpactNominee() {
		return impactNominee;
	}

	public void setImpactNominee(String impactNominee) {
		this.impactNominee = impactNominee;
	}

	public String getRecognitionNominee() {
		return recognitionNominee;
	}

	public void setRecognitionNominee(String recognitionNominee) {
		this.recognitionNominee = recognitionNominee;
	}

	public String getCollaborationNominee() {
		return collaborationNominee;
	}

	public void setCollaborationNominee(String collaborationNominee) {
		this.collaborationNominee = collaborationNominee;
	}

	public String getSpecMentionNominee() {
		return specMentionNominee;
	}

	public void setSpecMentionNominee(String specMentionNominee) {
		this.specMentionNominee = specMentionNominee;
	}

	public String getFilledStatus() {
		return filledStatus;
	}

	public void setFilledStatus(String filledStatus) {
		this.filledStatus = filledStatus;
	}

	public String getFormula() {
		return formula;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn() {
		this.createdOn = new Date();
	}

	public String getShortlisted() {
		return shortlisted;
	}

	public void setShortlisted(String shortlisted) {
		this.shortlisted = shortlisted;
	}

	public Date getScoreUpdate() {
		return scoreUpdate;
	}

	public void setScoreUpdate(Date scoreUpdate) {
		this.scoreUpdate = scoreUpdate;
	}

	public String getPromoteStatus() {
		return promoteStatus;
	}

	public void setPromoteStatus(String promoteStatus) {
		this.promoteStatus = promoteStatus;
	}

	public String getRanking() {
		return ranking;
	}

	public void setRanking(String ranking) {
		this.ranking = ranking;
	}
	
}

