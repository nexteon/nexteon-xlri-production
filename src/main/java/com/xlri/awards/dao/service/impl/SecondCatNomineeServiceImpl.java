package com.xlri.awards.dao.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xlri.awards.dao.entity.SecondCatNominee;
import com.xlri.awards.dao.service.api.SecondCatNomineeService;

@Repository
@Transactional
public class SecondCatNomineeServiceImpl implements SecondCatNomineeService{

	
	private static final Logger LOGGER = LoggerFactory.getLogger(NomineeServiceImpl.class);

	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public boolean saveNominee(SecondCatNominee nominee) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			session.save(nominee);
			session.getTransaction().commit();
 			System.out.println();
			return true;
 			
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return false;
		}
	}

	@Override
	public SecondCatNominee getNomineeByEmailId(String emailId) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<SecondCatNominee> query =  session.createQuery("SELECT n FROM SecondCatNominee n WHERE n.emailId="+emailId);
			session.getTransaction().commit();
			return query.getSingleResult();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}
	
	@Override
	public SecondCatNominee getNomineeByContact(String contact) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<SecondCatNominee> query =  session.createQuery("SELECT n FROM SecondCatNominee n WHERE n.contact=:contact");
			query.setParameter("contact", contact);
			session.getTransaction().commit();
			return query.getResultList().get(0);
		} catch (Exception e) {
			//LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public boolean updateNominee(SecondCatNominee secNominee) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			session.update(secNominee);
			session.getTransaction().commit();
 			return true;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return false;
		}
	}

	@Override
	public List<SecondCatNominee> getAllSecCatNom() {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			List<SecondCatNominee> query =  session.createQuery("From SecondCatNominee").getResultList();
			session.getTransaction().commit();
			return query;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

}
