package com.xlri.awards.dao.service.api;

import java.util.List;

import com.xlri.awards.dao.entity.SupportAgent;



public interface SupportService {

	public boolean saveSupport(SupportAgent supportAgent);

	public boolean updateSupport(SupportAgent supportAgent);

	public List<SupportAgent> getAllSupportData();

	public List<SupportAgent> getPending();
	
	public List<SupportAgent> getAccept();
	
	public List<SupportAgent> getDeclined();

}
