<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">
   <%@include file="dashboard/headerlibs.jsp" %>
   <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white" id="page-top" >
   			<div class="page-wrapper">
          <%@include file="dashboard/header.jsp" %>
           


                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="/userdata">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Agriculture</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title">Agriculture and Food</h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <!-- BEGIN DASHBOARD STATS 1-->
                        
                        <input type="hidden" name="subCategory" value="Agriculture, food processing and forestry – also includes vegetable products, vegetable fats, prepared foodstuffs and agri-tech"/>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        
                        
                               <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="icon-settings font-dark"></i>
                                            <span class="caption-subject bold uppercase">  Data Table</span>
                                        </div>
                                        
                                    </div>
                                    <div class="portlet-body">
                                        
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable" id="example" class="display">
                                            <thead>
                         <tr>
                       		  <th>ID</th>
                              <th>Application ID</th>
                              <th>Award Category</th>
                              <th>Nominee Name</th>
                              <th>Social Category</th>
                              <th>Email Id</th>
                              <th>Enterprise Name</th>
                       </tr>
                     </thead>
                                            
                     <tbody>
                     	
               
               </tbody>
             
                                             </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>
                        
                        
                        
                        </div>
                        </div>
                        </div>
                        
                        
                       <!-- END QUICK NAV -->
     <%@include file="dashboard/footer.jsp"%>
      <%@include file="dashboard/datatablescript.jsp" %>
     
     
      
<script>
$('#example').DataTable( {
	  "ajax": {
	  "url": "/goods/category?param=agriculture",
	  "type" : 'POST',
	"scrollX": true,
	  "dataSrc": ""
	  },
	  "columns": [
	  { "data": "id" },
	  { "data": "contact" },
	  { "data": "awardCategory" },
	  { "data": "nomineeName" },
	  { "data": "socialCategory" },
	  { "data": "emailId" },
	  { "data": "enterpriseName" },
	  ],dom: 'Bfrtip',
    buttons: [
              'copyHtml5',
              'excelHtml5',
              'csvHtml5',
              'pdfHtml5'
          ], initComplete: function () {
	  this.api().columns().every(function () {
	  var column = this;

	 
	  var select = $('<select><option value=""></option></select>')
	  .appendTo($("#filters").find("th").eq(column.index()))
	  .on('change', function () {
	  var val = $.fn.dataTable.util.escapeRegex(
	  $(this).val()); 

	  column.search(val ? '^' + val + '$' : '', true, false)
	  .draw();
	  }); 
	  console.log(select);
	  column.data().unique().sort().each(function (d, j) {
	  select.append('<option value="' + d + '">' + d + '</option>')
	  });
	  });
	  }
	  } );
</script>
     
      <%@include file="dashboard/disablewarning.jsp" %>
     </body>
         </html>    