<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<%@include file="fragments/headerlibs.jsp" %>
<body class="innerpage">

<%@include file="fragments/header.jsp" %>
<!-- Body Starts Here -->


<main role="main">
   <div class="page-title">
      <h2>OTP Verification</h2>
   </div>
   <div class="content">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <div class="whitebg">
                  <div class="form-body">
                     <form action="/otp/verify/${registration}" method="POST" data-toggle="validator" role="form">
                        <c:if test="${not empty error}">
                           <div class="form-header">
                              <h4>
                                 <c:out value="${error}"/>
                              </h4>
                           </div>
                        </c:if>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="otp">OTP</label>
                              <input type="text" name="otpnumber" id="otp" class="form-control" placeholder="Enter OTP" required data-minlength="6" pattern="[0-9]{6}" data-required-error="Please provide the OTP" data-pattren-error="Please provide a valid 6 digit valid OTP">
                           </div>
                        </div>
                        <input type="hidden" name="currentpath" value=""/>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        <div class="col-md-1">
                           <div class="submit-btn form-group">
                              <label for="otp" class="vis-hidden">Submit</label>
                              <input class="btn btn-primary" type="button" name="Submit" value="Submit">
                           </div>
                        </div>
                       <%-- <div class="col-md-2">
                          <div class="submit-btn form-group">
                          	 <label for="otp" class="vis-hidden">Resend OTP</label>
                             <button class="btn btn-warning resend-otp" href="/otp/resend/${registration}">Resend OTP</button>
                          </div>
                       </div> --%>
                     </form>
                     <div class="col-md-2">
                          <div class="submit-btn form-group">
                          	 <label for="otp" class="vis-hidden">Resend OTP</label>
                             <button class="btn btn-warning resend-otp" href="/otp/resend/${registration}">Resend OTP</button>
                          </div>
                       </div>
                     <div class="col-md-12">
                          <div class="submit-btn">
                          	 <h4 style="color:red">An OTP (One Time Password) has been sent to the registered mobile number and E-Mail.</h4>
                          </div>
                       </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</main>

<c:if test="${not empty successMsg}">
  <div class="modal fade" id="successModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Success</h4>
        </div>
        <div class="modal-body">
          <p>${successMsg}</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Proceed</button>
        </div>
      </div>
      
    </div>
  </div>
</c:if>

<div class="modal fade" id="otpModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer">
        </div>
      </div>
      
    </div>
  </div>

<!-- Body Ends Here -->
<%@include file="fragments/footer.jsp" %>
<script>
		$(document).ready(function() {
			if($('#successModal').length > 0){
				$('#successModal').modal('show');
			}
			$('input[name=Submit]').click(function(){
				$.ajax({
					url: '/otp/verify/${registration}',
					type: 'POST',
					data: 'otpnumber='+$('#otp').val()+"&${_csrf.parameterName}=${_csrf.token}",
					success: function(data){
						console.log(data['status']);
						if(data['status'] === true){
							$('#otpModal .modal-title').html("Success");
							$('#otpModal .modal-body').html("OTP Verified Successfully! Proceed to Login page.");
							$('#otpModal .modal-footer').html('<a href="/login" class="btn btn-primary">Proceed</a>');
							$('#otpModal').modal('show');
						}
						if(data['status'] === false){
							$('#otpModal .modal-title').html("Error");
							$('#otpModal .modal-body').html("OTP Verification failed! Please enter a valid OTP or press Resend OTP button to get a new OTP.");
							$('#otpModal .modal-footer').html('<button class="btn btn-danger" data-dismiss="modal">Cancel</button>');
							$('#otpModal').modal('show');
						}
					}
				})
			});
			
			$('.resend-otp').click(function(){
				$.ajax({
					url: '/otp/resend/${registration}',
					type: 'POST',
					success: function(data){
						console.log(data['status']);
						if(data['status'] === true){
							$('#otpModal .modal-title').html("Success");
							$('#otpModal .modal-body').html("OTP has been resent to your registered Mobile Number and E-mail Id!");
							$('#otpModal .modal-footer').html('<button class="btn btn-primary" data-dismiss="modal">Proceed</button>');
							$('#otpModal').modal('show');
						}
						if(data['status'] === false){
							$('#otpModal .modal-title').html("Error");
							$('#otpModal .modal-body').html("OTP has been sent. Kindly wait for a while");
							$('#otpModal .modal-footer').html('<button class="btn btn-warning" data-dismiss="modal">Ok</button>');
							$('#otpModal').modal('show');
						}
					}
				})
			});	
		})
	</script>
</body>
</html>