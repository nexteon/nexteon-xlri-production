<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
   <%@include file="fragments/headerlibs.jsp" %>
   <body class="innerpage">
      <%@include file="fragments/header.jsp" %>
      <!-- Body Starts Here -->
      <main role="main">
         <div class="page-title">
            <h2>FAQs NEA 2018</h2>
         </div>
         <div class="content">
            <div class="container">
               <div class="row">
                  <div class="col-md-12">
                     <div class="whitebg faq">
                        <div class="col-xs-12">
                           <h4 class="blue-hdg">What is the ‘National Entrepreneurship Awards’ are for? </h4>
                           <p>The awards are to recognise the efforts of outstanding young entrepreneurs and honour outstanding young first generation entrepreneurs and their Ecosystem Builders for their outstanding contribution in entrepreneurship development.<br>
                               A total of 43 Awards under NEA2018 have been carefully considered to enable participation of young achievers below the age of 40 years and their ecosystem builders across sectors, geographies and socio-economic background through the process of nomination. The purpose is to highlight models of excellence for others to emulate and improve upon.

                           </p>
                        </div>
                        <div class="col-xs-12">
                           <h4 class="blue-hdg">Who all can apply for the awards?</h4>
                           <p>Any Indian national working in the Indian Territory and who fits to
                              the eligibility criteria, can apply for the awards. Please refer
                              to eligibility criteria for details in the Guidelines
                              (hyperlinked) section.
                           </p>
                        </div>
                        <div class="col-xs-12">
                           <h4 class="blue-hdg">Who can nominate for the awards?</h4>
                           <p>Organisations and individuals can both nominate deserving candidates for the Award. These may include: </p>
                            <ul>
                            <li>Government: Central, State Governments, Entrepreneurship and Skill Mission, Panchayati Raj Institutions (PRI) and Urban Local Bodies (ULB), etc.</li>
                                <li>Government Institutions: such as NIESBUD, IIE, NIMSME, etc.</li>
                                <li>Non-Governmental Organisations (Entrepreneurship Development)</li>
                                <li>Incubators & Mentor Organisations</li>
                                <li>Industry Associations</li>
                                <li>Banking and Non-Banking Financial Institutions </li>
                                <li>Educational Institutions</li>
                                <li>General Public </li>
                            </ul>
                        </div>
                        <div class="col-xs-12">
                           <h4 class="blue-hdg">Is there any limit to the number of nominations one can file for the awards? </h4>
                           <p>Yes. In the Enterprise Award Track, an individual can maximum up to five enterprises whereas an institution can nominate a up to ten enterprises. </p><p>In the ‘Ecosystem Builder Track’, an individual can nominate in maximum two categories, while an institution can nominate in maximum four award categories. </p>
                        </div>
                        <div class="col-xs-12">
                           <h4 class="blue-hdg">What is the age restriction for the awards? </h4>
                            <p>The age criterion is common for all the potential aspirants in the ‘Enterprise Award Track’. Entrepreneurs who are being nominated should be below the age of 40 years as on April 1st2018. No separate age relaxation will be given to candidates from SC, ST and Persons with Disability (PwD) category. The enterprise must have been registered at least 4 years before the date of first advertisement of the current call.</p><p>There is no age bar for the ‘Ecosystem Builders Track’. </p>
                        </div>
                        <div class="col-xs-12">
                           <h4 class="blue-hdg">Who all can be nominated for the awards? 
</h4>
                           <p>Any Indian national working in the Indian Territory and who fits to the eligibility criteria can be nominated for the awards. Please refer to eligibility criteria for details in the Guidelines (hyperlinked) section.
</p>
                        </div>
                        <div class="col-xs-12">
                           <h4 class="blue-hdg">What is ‘Special Category’ Awards? In how many ‘Special Category’ Awards can one apply?</h4>
                           <p>Special Category Awards are to recognise the efforts of entrepreneurs in four categories – (a) Women Entrepreneurs; (b) Entrepreneur from SC/ST category; (c) Entrepreneur from PwD category; (d) Entrepreneur from Difficult Areas. Any deserving candidate can be nominated under maximum two ‘Special Category’ awards; however, the award will be given in only one special category in which the candidate has scored the highest.</p>
                        </div>
                        <div class="col-xs-12">
                           <h4 class="blue-hdg">How many Award Category one can be nominated in?</h4>
                           <p>
                              A single Nominee may not be nominated for more than two Industry sectors in the ‘Enterprise Awards Track’. However, the final evaluation and selection will be done only in one category in which the Nominee’s candidature is found most suitable.
                           </p>
                        </div>
                        <div class="col-xs-12">
                           <h4 class="blue-hdg">What is the total number of awards to be given this year?</h4>
                           <p>Total 43 awards will be given in the two Award Categories. 
                           </p>
                        </div>
                        <div class="col-xs-12">
                           <h4 class="blue-hdg">What is the mode of nominations? </h4>
                           <p>The nomination for all the categories will only be accepted online.
                           </p>
                        </div>
                        <div class="col-xs-12">
                           <h4 class="blue-hdg">What is the duration for filing nominations?
 </h4>
                           <p>Nominations for the year 2018 will be accepted from October 17, 2018 to November16, 2018.

                           </p>
                        </div>
                        <div class="col-xs-12">
                           <h4 class="blue-hdg">Will there be any physical examination and verification of selected enterprises? </h4>
                           <p>Yes. Regional teams will conduct field visits to verify the information provided by the nominator/ nominee before qualifying them for the final round of evaluation.</p>
                        </div>
                        <div class="col-xs-12">
                           <h4 class="blue-hdg">When will the winners be announced?</h4>
                           <p>The winners will be announced around second week of December 2018.</p>
                        </div>
                        <div class="col-xs-12">
                           <h4 class="blue-hdg">Who all can give recommendations for the mentor in the Ecosystem Builder Award Category? </h4>
                           <p>The mentor must get three to five recommendations from a mentee, teacher, senior colleagues etc. 
</p>
                           
                        </div>
                         <div class="col-xs-12">
                           <h4 class="blue-hdg">What will happen if the nominator/ nominee submit incorrect information/ documents? 
 </h4>
                           <p>In case of incorrect information/ documents, the nomination will be immediately rejected. If the nominator repeatedly submits incorrect information, the nominator, nominee or both will be blacklisted from participating in NEA 2018.

</p>
                           
                        </div>
                         <div class="col-xs-12">
                           <h4 class="blue-hdg">Can an individual be nominated for ‘Enterprise Award Track’ and ‘Eco-system Award Track’?
 </h4>
                           <p>Yes. If the candidate meets the eligibility criteria, s/he may be nominated in both the tracks.</p>
                           
                        </div>
                         
                         <div class="col-xs-12">
                           <h4 class="blue-hdg">What are the steps involved in the entire nomination and application process in NEA 2018.</h4>
                          <div class="table-responsive">
                             <table class="table table-bordered">
<tbody>
<tr>
<td  width="93">
<p ><strong><span >Award Track</span></strong></p>
</td>
<td  width="202">
<p ><strong><span >Award Category</span></strong></p>
</td>
<td  width="106">
<p ><strong><span >Step 1</span></strong></p>
</td>
<td  width="118">
<p ><strong><span >Step 2</span></strong></p>
</td>
<td  width="142">
<p ><strong><span >Step 3</span></strong></p>
</td>
<td  width="130">
<p ><strong><span >Step 4</span></strong></p>
</td>
<td  width="118">
<p ><strong><span >Step 5</span></strong></p>
</td>
<td  width="142">
<p ><strong><span >Step 6</span></strong></p>
</td>
<td  width="142">
<p ><strong><span >Step 7</span></strong></p>
</td>
<td  width="165">
<p ><strong><span >Step 8</span></strong></p>
</td>
</tr>
<tr>
<td  width="93">
<p ><span >Enterprise Award Track</span></p>
</td>
<td  width="202">
<p><span >A1 (Initial investment up to </span><span >₹</span><span >1 L)</span></p>
</td>
<td  width="106">
<p ><span >Nomination</span></p>
</td>
<td  width="118">
<p ><span >Screening of Nomination</span></p>
</td>
<td  width="142">
<p ><span >Evaluation of Nomination</span></p>
</td>
<td  width="130">
<p ><span >Field Verification</span></p>
</td>
<td  width="118">
<p ><span >Regional Level Evaluation</span></p>
</td>
<td  width="142">
<p ><span >National Level Expert Evaluation</span></p>
</td>
<td  width="142">
<p ><span >National Jury Winner Selection</span></p>
</td>
<td  width="165">
<p ><span >--</span></p>
</td>
</tr>
<tr>
<td  width="93">
<p ><span >Enterprise Award Track</span></p>
</td>
<td  width="202">
<p><span >A2 (Initial investment <strong><span >above </span></strong></span><strong><span >₹</span></strong><strong><span >1 L to </span></strong><strong><span >₹</span></strong><strong><span >&nbsp;</span></strong><strong><span > 10 L</span></strong><strong><span >)</span></strong></p>
</td>
<td  width="106">
<p ><span >Nomination</span></p>
</td>
<td  width="118">
<p ><span >Screening of Nomination</span></p>
</td>
<td  width="142">
<p ><span >Evaluation of Nomination</span></p>
</td>
<td  width="130">
<p ><span >Basic Data Submission</span></p>
</td>
<td  width="118">
<p ><span >Field Verification</span></p>
</td>
<td  width="142">
<p ><span >Regional Level Evaluation</span></p>
</td>
<td  width="142">
<p ><span >National Level Expert Evaluation</span></p>
</td>
<td  width="165">
<p ><span >National Jury Winner Selection</span></p>
</td>
</tr>
<tr>
<td  width="93">
<p ><span >Enterprise Award Track</span></p>
</td>
<td  width="202">
<p><span >A3 (Initial investment <strong><span >above </span></strong></span><strong><span >₹</span></strong><strong><span >10 L to </span></strong><strong><span >₹</span></strong><strong><span >&nbsp;</span></strong><strong><span > 1Cr</span></strong><strong><span >)</span></strong></p>
</td>
<td  width="106">
<p ><span >Nomination</span></p>
</td>
<td  width="118">
<p ><span >Screening of Nomination</span></p>
</td>
<td  width="142">
<p ><span >Evaluation of Nomination</span></p>
</td>
<td  width="130">
<p ><span >Detail&nbsp; Data Submission</span></p>
</td>
<td  width="118">
<p ><span >Field Verification</span></p>
</td>
<td  width="142">
<p ><span >Regional Level Evaluation</span></p>
</td>
<td  width="142">
<p ><span >National Level Expert Evaluation</span></p>
</td>
<td  width="165">
<p ><span >National Jury Winner Selection</span></p>
</td>
</tr>
<tr>
<td  width="93">
<p ><span >Ecosystem Builders Track</span></p>
</td>
<td  width="202">
<p ><span >a)<span >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span >Mentor&nbsp; </span></p>
<p ><span >b)<span >&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span >Incubation Centre&nbsp; </span></p>
<p ><span >c)<span >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span >Entrepreneurship Development Institutes or Organisations</span></p>
<p ><span >d)<span >&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span >Promoters Rural Producer Group Enterprise</span></p>
</td>
<td  width="106">
<p ><span >Nomination</span></p>
</td>
<td  width="118">
<p ><span >Screening of Nomination</span></p>
</td>
<td  width="142">
<p ><span >Evaluation of Nomination</span></p>
</td>
<td  width="130">
<p ><span >Field Verification</span></p>
</td>
<td  width="118">
<p ><span >National Level Expert Evaluation</span></p>
</td>
<td  colspan="3" width="449">
<p ><span >National Jury Winner Selection</span></p>
</td>
</tr>
</tbody>
</table>
                            </div>
                           
                        </div>
                         
                         <div class="col-xs-12">
                           <h4 class="blue-hdg">What are the key documents required for the application?

 </h4>
                           <p>The application requires following key documents from the applicant-
</p>
                             <ol class="faq-ol-text">
                                 <li>Audited Bank Statements or Self-attested scanned copy of Bank Passbook (first & last Page)
</li>
                                 <li>Cash Flow Statement</li>
                                 <li>MoA and AoA (of the enterprise and/or of the co-producers)
</li>
                                 <li>Balance Sheet and P&L (last four years)
</li>
                                 <li>Details of patents/ trademarks/ copyrights etc., if any</li>
                                 <li>Mandatory Industry Certifications
</li>
                                 <li>Credentials of the founding team members
</li>
                                 <li>Business flow diagram, if any
</li>
                                 <li>Letter of recognition/certificate of award etc. won in the last 4 years</li>
                                 <li>Any other supporting document to substantiate the data</li>
                             </ol>
                           
                        </div>
                         <div class="col-xs-12">
                           <h4 class="blue-hdg">Are all the documents mentioned above mandatory?

 </h4>
                           <p>No. Not all the documents mentioned above are mandatory to apply for the awards. The category A1 entries need not submit any documents at the Nomination stage. However, the category A2 and A3 entries require certain mandatory and optional documents. The mandatory documents are marked with (*). 
</p>
                           
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </main>
      <!-- Body Ends Here -->
      <%@include file="fragments/footer.jsp" %>
   </body>
</html>