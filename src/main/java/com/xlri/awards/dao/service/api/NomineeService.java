package com.xlri.awards.dao.service.api;

import java.util.List;
import java.util.Map;

import com.xlri.awards.dao.entity.NomineeDetails;

public interface NomineeService {

	public boolean saveNominee(NomineeDetails nominee);
	public boolean updateNominee(NomineeDetails nominator); 
	public List<NomineeDetails> getAllNominee();
	//public NomineeDetails getNomineeById(int id);
	public NomineeDetails getNomineeByEmailId(String emailId);
	public NomineeDetails getNomineeByRegistrationNumber(String registrationNumber);
	public boolean deleteNomineeByRegistrationNumber(String registrationNumber);
	public Long getNomineeBySocialCategory(String socialCategory);
	public int getNomineeBySocialCat(String socialCategory);
	public NomineeDetails getNomineeByContact(String contact);
	public List<NomineeDetails> getAllNomineeByRegistration(String registration);
	public List<NomineeDetails> getNomineeByState(String...state);
	public List<NomineeDetails> getNomineeByPWD();
	public List<NomineeDetails> getNomineeByAwardSubCategory(String awardSub);
	public NomineeDetails getNomineeById(Long id);
	public List<NomineeDetails> getNomineeBySpecialCategory(String speccatval);
	public boolean changeNomineeStatusRemark(String emailId, String remark);
	public boolean changeNomineeStatus(String emailId, String remark);
	public boolean changeNomineeStatusPending(String emailId, String remark);
	public List<NomineeDetails> getNomineeAcceptedCount();
	public List<NomineeDetails> getNomineePendingCount();
	public List<NomineeDetails> getNomineeDeclinedCount();
	public List<NomineeDetails> getAllSpecialCat();
	public List<NomineeDetails> getMISCount(String states, String category);
	public List<String> getDistinctAwardSubCategory();
	public List<NomineeDetails> getMISReport(String state, String awardcat, String awardsub);
	public List<NomineeDetails> getAllShortlistedNominee();
	public List<NomineeDetails> getResultByQuery(String query, Map<String, Object> map);
	public List<NomineeDetails> getNomineeRegionalShortlist(String... state);
	public List<NomineeDetails> getAllPromotedNomineeFiles();
	public List<NomineeDetails> getAllPromotedNominee();

}
