<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<%@include file="fragments/headerlibs.jsp" %>
<body class="innerpage">

<%@include file="fragments/header.jsp" %>
<!-- Body Starts Here -->

<main role="main">
	<div class="page-title">
		<h2>Rules for Participating in NEA 2018</h2>
	</div>
	<div class="content guidelines-page">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="whitebg">
						<div class="col-xs-12">
							<p>Following rules will be strictly followed:</p>
							<ol>
                            <li>All Support Agency(s), Lead Partner, Regional Partners, will sign (physically or digitally), a non-disclosure agreement with the MSDE.
</li>
                                <li>Participation in the National Entrepreneurship Awards 2018 is voluntary.</li>
                                <li>Applications for the NEA 2018 awards will be invited only by nominations.</li>
                                <li>Limits for Individuals:
                                  <ol class="lower-alpha">
                                    <li>a maximum of 5 nominations for the Enterprise Award</li>
                                      <li>a maximum of 2 nominations for the Ecosystem Builders Award
</li>
                                    
                                    </ol>
                                </li>
                                <li>Limits for Institutions/Organizations:
                                <ol class="lower-alpha">
                                <li>a maximum of 10 nominations for the Enterprise Award</li>    <li>a maximum of 4 nominations for the Ecosystem Builders Award</li>
                                </ol>
                                </li>
                                <li>A single Nominee may not be Nominated for more than two Industry sectors in the Enterprise Awards category. However, the final evaluation and selection will be done only in one category in which the Nominee’s candidature is be found most suitable.</li>
                                <li>The Core Committee Institutions are not eligible to nominate candidates for the awards.</li>
                                <li>Only nominations that are submitted within the cut-off time for nomination as per the NEA announcement and meet all eligibility requirements will be considered for National Entrepreneurship Awards. Any late nominations will not be considered.</li>
                                <li>The decisions of the Jury and the implementation committee will be final and binding.</li>
                                <li>Finalists maybe subject to a legal due diligence review by our Implementing Partners. If the organization refuses to such a request, MSDE holds the right to select the Jury’s next highest score outcome to receive the benefits and/or Award.
</li><li>By participating in the National Entrepreneurship Awards, the enterprise, Nominator, Nominee agrees to Government of India’s and its partners’ use of its name, URL, photos and videos for promotional purposes on its website and in other promotional material.
</li><li>Any false information provided within the context of National Entrepreneurship Awards by any enterprise concerning identity, mailing address, telephone number, email address, ownership of right, or non-compliance with these rules or any terms and conditions or the like may result in the immediate elimination of the candidate from the Awards process.
</li><li>MSDE and GoI reserve the right at its sole discretion to cancel, terminate, modify, or suspend the National Entrepreneurship Awards. MSDE and GoI further reserves the right to disqualify any candidate that tampers with the submission process, commits fraud or is in violation of criminal and/or civil laws.
</li><li>Nominations are to be filled in English only.
</li><li>Nominations for Women Entrepreneurs, SC/ST, Persons with Disability (PwD) and Difficult Areas (North east/ J&K/ islands/ backward area as notified by GoI/ rural enterprises) will be considered under both the Sector Awards as well as Special Awards category.
</li>
                                <li>A single nominee may be nominated for Special Category Awards along with the Sector Category Award as a general candidate; however, the final selection for award will be done only in one category either in special category or in general category where the nominee’s candidature is found most deserving.
</li><li>A single nominee may be nominated in maximum two Special Category Awards at a time; however, the selection for award will be done only in one Special where the nominee’s candidature is found most deserving.
</li><li>The Partner Institutes will be eligible to apply for the award except the Lead Partner. However, mentors associated with all the partner institutions can apply.
</li><li>By applying to National Entrepreneurship Awards, the Nominator, Nominee, Enterprise provides its full consent and acceptance of the above terms and conditions.
</li><li>The Award winner will be allowed the following facilities in order to attend the Award Ceremony.<ol class="lower-alpha">
                                <li><strong>Travel Expense</strong> – Travel expense usually includes roundtrip plane (economy), train or bus fare [as determined by Government of India] for the representatives to travel from the major airport/train/bus station closest to the individual’s residence to the event venue.
</li>
                                <li><strong>Accommodation</strong> - Double occupancy lodging and food facilities for two nights.</li>
                                </ol>
</li>
                            </ol>
                            <p>Kindly note that (1) in case of winning enterprises, all the co-founders (maximum up to three) along with one companion each will be paid for travel and boarding expenses. No personal and incidental expenses will be borne by the Ministry.
</p><p>(2) In case of winning Individuals/ organisations, one representative from the organisation along with one companion will be paid for travel and boarding expenses. No personal and incidental expenses will be borne by the Ministry.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>



<!-- Body Ends Here -->
<%@include file="fragments/footer.jsp" %>
</body>
</html>