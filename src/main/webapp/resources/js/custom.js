$(document).ready(function() {
    $(".owl-one").owlCarousel({
        items: 1,
        loop: true,
        mouseDrag: false,
        animateOut: 'fadeOut',
        margin: 10,
        autoplay: true,
        autoplayTimeout: 4000,
        autoplayHoverPause: false
    });
    var windowheight = jQuery(window).height();
    $('.banner .owl-one.owl-carousel .owl-item, .banner .owl-one.owl-carousel .item').css({
        "height": windowheight - 128
    })

$(".owl-two").owlCarousel({
        items: 8,
        loop: true,                
        margin: 10,
        autoplay: true,
        autoplayTimeout: 4000,
        autoplaySpeed: 500,
        autoplayHoverPause: false,
        responsiveClass:true,
        responsive:{
            0:{
                items:2,
            },
            600:{
                items:3,
            },
            1000:{
                items:8,
                loop:true
            }
        }
    });

$(".owl-three").owlCarousel({
        items: 1,
        loop: true,                
        margin: 10,
        autoplay: true,
        autoplayTimeout: 4000,
        autoplaySpeed: 500,
        autoplayHoverPause: false
    });

$(".owl-four").owlCarousel({
        items: 1,
        loop: true,                
        margin: 10,
        autoplay: true,
        autoplayTimeout: 4000,
        autoplaySpeed: 500,
        autoplayHoverPause: true
    });

});




function scroll_to_class(element_class, removed_height) {
	var scroll_to = $(element_class).offset().top - removed_height;
	if($(window).scrollTop() != scroll_to) {
		$('html, body').stop().animate({scrollTop: scroll_to}, 0);
	}
}

function bar_progress(progress_line_object, direction) {
	var number_of_steps = progress_line_object.data('number-of-steps');
	var now_value = progress_line_object.data('now-value');
	var new_value = 0;
	if(direction == 'right') {
		new_value = now_value + ( 100 / number_of_steps );
	}
	else if(direction == 'left') {
		new_value = now_value - ( 100 / number_of_steps );
	}
	progress_line_object.attr('style', 'width: ' + new_value + '%;').data('now-value', new_value);
}

jQuery(document).ready(function() {
	
    /*
        Fullscreen background
    */
    
    /*
        Form
    */
    $('.f1 fieldset:first').fadeIn('slow');
    
    $('.f1 input[type="text"], .f1 input[type="password"], .f1 textarea').on('focus', function() {
    	$(this).removeClass('input-error');
    });
    
    // next step
    $('.f1 .btn-next').on('click', function() {
    	var parent_fieldset = $(this).parents('fieldset');
    	var next_step = true;
    	// navigation steps / progress steps
    	var current_active_step = $(this).parents('.f1').find('.f1-step.active');
    	var progress_line = $(this).parents('.f1').find('.f1-progress-line');
    	
    	// fields validation
    	parent_fieldset.find('input[type="text"], input[type="password"], textarea').each(function() {
    		if( $(this).val() == "" ) {
    			$(this).addClass('input-error');
    			next_step = false;
    		}
    		else {
    			$(this).removeClass('input-error');
    		}
    	});
    	// fields validation
    	
    	if( next_step ) {
    		parent_fieldset.fadeOut(400, function() {
    			// change icons
    			current_active_step.removeClass('active').addClass('activated').next().addClass('active');
    			// progress bar
    			bar_progress(progress_line, 'right');
    			// show next step
	    		$(this).next().fadeIn();
	    		// scroll window to beginning of the form
    			scroll_to_class( $('.f1'), 20 );
	    	});
    	}
    	
    });
    
    // previous step
    $('.f1 .btn-previous').on('click', function() {
    	// navigation steps / progress steps
    	var current_active_step = $(this).parents('.f1').find('.f1-step.active');
    	var progress_line = $(this).parents('.f1').find('.f1-progress-line');
    	
    	$(this).parents('fieldset').fadeOut(400, function() {
    		// change icons
    		current_active_step.removeClass('active').prev().removeClass('activated').addClass('active');
    		// progress bar
    		bar_progress(progress_line, 'left');
    		// show previous step
    		$(this).prev().fadeIn();
    		// scroll window to beginning of the form
			scroll_to_class( $('.f1'), 20 );
    	});
    });
    
    // submit
    $('.f1').on('submit', function(e) {
    	
    	// fields validation
    	$(this).find('input[type="text"], input[type="password"], textarea').each(function() {
    		if( $(this).val() == "" ) {
    			e.preventDefault();
    			$(this).addClass('input-error');
    		}
    		else {
    			$(this).removeClass('input-error');
    		}
    	});
    	// fields validation
    	
    });
    
    
});
$('input[type="range"]').on('input', function() {

	  var control = $(this),
	    controlMin = control.attr('min'),
	    controlMax = control.attr('max'),
	    controlVal = control.val(),
	    controlThumbWidth = control.data('thumbwidth');

	  var range = controlMax - controlMin;
	  
	  var position = ((controlVal - controlMin) / range) * 100;
	  var positionOffset = Math.round(controlThumbWidth * position / 100) - (controlThumbWidth / 2);
	  var output = control.next('output');
	  
	  output
	    .css('left', 'calc(' + position + '% - ' + positionOffset + 'px)')
	    .text(controlVal);

	});  
	   /* negative number script   */ 
	    $("#balance-sheet").keypress(function(event) {
	  if ( event.which == 45 || event.which == 189 ) {
	      event.preventDefault();
	   }
	});
	    
//other option hide of Know Nominee
	    
$(document).ready(function() {
    $("#knowNominee").change(function() {
        if ($(this).val() == 'other') { 
            $('.other-fieldhide').show();
        	$('#other-field').show();
            $('.other-field').addClass('oth-active');
        }
        if ($(this).val() !== 'other') { 
            $('.other-fieldhide').hide();
            $('#other-field').hide();
            $('.other-field').removeClass('oth-active');
        }
    });
});


/*Skip main content js   */
$(document).ready(function() {
$(".aembootstrap-gv-rmds-skp a").click(function(){
$('html,body').animate({
scrollTop: $("main").offset().top-100},'slow');     
});
});
/*banner js   */
$(document).ready(function() {
            var owl = $('.owl-carousel');
            owl.owlCarousel({
                items: 1,
                loop: true,
                mouseDrag: false,
                animateOut: 'fadeOut',
                margin: 10,
                autoplay: true,
                autoplayTimeout: 4000,
                autoplayHoverPause: false
            });
            var windowheight = jQuery(window).height();
            $('.banner .owl-carousel .owl-item, .banner .owl-carousel .item').css({
                "height": windowheight - 128
            })
        });
$(document).ready(function() {
	$('[data-toggle="tooltip"]').tooltip();
	if($('#errorModal').length > 0){
		$('#errorModal').modal('show');
	}
});
//JS for Gallery
$(document).ready(function(){
	$(function(){
		$('ul.demo li').picEyes();
	});
	});


//balance sheet script
/* Current assets 17-18   */
$(document).on("change", ".crt_assts_17", function() {
var current_assets_sum_17 = 0;
$(".crt_assts_17").each(function(){
    current_assets_sum_17 += +$(this).val();
});
$(".tot_crt_assts_17").val(current_assets_sum_17);
});
    
/* Current assets 16-17   */
$(document).on("change", ".crt_assts_16", function() {
var current_assets_sum_16 = 0;
$(".crt_assts_16").each(function(){
    current_assets_sum_16 += +$(this).val();
});
$(".tot_crt_assts_16").val(current_assets_sum_16);
});
    
/* Current assets 15-16   */
$(document).on("change", ".crt_assts_15", function() {
var current_assets_sum_15 = 0;
$(".crt_assts_15").each(function(){
    current_assets_sum_15 += +$(this).val();
});
$(".tot_crt_assts_15").val(current_assets_sum_15);
});
    
    
    
/* Fixed assets 17-18   */
$(document).on("change", ".fix_assts_17", function() {
var fix_assets_sum_17 = 0;
$(".fix_assts_17").each(function(){
    fix_assets_sum_17 += +$(this).val();
});
$(".tot_fix_assts_17").val(fix_assets_sum_17);
});
    
 /* Fixed assets 16-17   */   
$(document).on("change", ".fix_assts_16", function() {
var fix_assets_sum_16 = 0;
$(".fix_assts_16").each(function(){
    fix_assets_sum_16 += +$(this).val();
});
$(".tot_fix_assts_16").val(fix_assets_sum_16);
});
    
/* Current assets 15-16   */
$(document).on("change", ".fix_assts_15", function() {
var fix_assets_sum_15 = 0;
$(".fix_assts_15").each(function(){
    fix_assets_sum_15 += +$(this).val();
});
$(".tot_fix_assts_15").val(fix_assets_sum_15);
});
    
/* total for Fixed and current assets   */  
$(document).on('change', 'input', function() {        
var primaryincome17 = $(".tot_crt_assts_17").val();
var otherincome17 = $(".tot_fix_assts_17").val();
var totalincome17 = parseInt(primaryincome17) + parseInt(otherincome17);    
$(".total_assts_17").val(totalincome17); 
    
var primaryincome16 = $(".tot_crt_assts_16").val();
var otherincome16 = $(".tot_fix_assts_16").val();
var totalincome16 = parseInt(primaryincome16) + parseInt(otherincome16);    
$(".total_assts_16").val(totalincome16);
    
var primaryincome15 = $(".tot_crt_assts_15").val();
var otherincome15 = $(".tot_fix_assts_15").val();
var totalincome15 = parseInt(primaryincome15) + parseInt(otherincome15);    
$(".total_assts_15").val(totalincome15);
    
});


/* Current/short-term liabilities 17-18   */
$(document).on("change", ".curr_short_lib_17", function() {
var curr_short_lib_17_sum = 0;
$(".curr_short_lib_17").each(function(){
    curr_short_lib_17_sum += +$(this).val();
});
$(".total_curr_short_lib_17").val(curr_short_lib_17_sum);
});

/* Current/short-term liabilities 16-17   */
$(document).on("change", ".curr_short_lib_16", function() {
var curr_short_lib_16_sum = 0;
$(".curr_short_lib_16").each(function(){
    curr_short_lib_16_sum += +$(this).val();
});
$(".total_curr_short_lib_16").val(curr_short_lib_16_sum);
});

/* Current/short-term liabilities 15-16  */
$(document).on("change", ".curr_short_lib_15", function() {
var curr_short_lib_15_sum = 0;
$(".curr_short_lib_15").each(function(){
    curr_short_lib_15_sum += +$(this).val();
});
$(".total_curr_short_lib_15").val(curr_short_lib_15_sum);
});



/*Long-term liabilities 17-18*/
$(document).on("change", ".long_term_liabilities_17", function() {
var long_term_liabilities_17_sum = 0;
$(".long_term_liabilities_17").each(function(){
    long_term_liabilities_17_sum += +$(this).val();
});
$(".total_long_term_liabilities_17").val(long_term_liabilities_17_sum);
});

/*Long-term liabilities 16-17*/
$(document).on("change", ".long_term_liabilities_16", function() {
var long_term_liabilities_16_sum = 0;
$(".long_term_liabilities_16").each(function(){
    long_term_liabilities_16_sum += +$(this).val();
});
$(".total_long_term_liabilities_16").val(long_term_liabilities_16_sum);
});

/*Long-term liabilities 15-16*/
$(document).on("change", ".long_term_liabilities_15", function() {
var long_term_liabilities_15_sum = 0;
$(".long_term_liabilities_15").each(function(){
    long_term_liabilities_15_sum += +$(this).val();
});
$(".total_long_term_liabilities_15").val(long_term_liabilities_15_sum);
});


 /* total for Total liabilities   */  
$(document).on('change', 'input', function() {        
var var_lib_17 = $(".total_curr_short_lib_17").val();
var var_lib2_17 = $(".total_long_term_liabilities_17").val();
var total_long_short_17 = parseInt(var_lib_17) + parseInt(var_lib2_17);    
$(".total_short_long_liabilities_17").val(total_long_short_17); 
    
var var_lib_16 = $(".total_curr_short_lib_16").val();
var var_lib2_16 = $(".total_long_term_liabilities_16").val();
var total_long_short_16 = parseInt(var_lib_16) + parseInt(var_lib2_16);    
$(".total_short_long_liabilities_16").val(total_long_short_16); 
    
var var_lib_15 = $(".total_curr_short_lib_15").val();
var var_lib2_15 = $(".total_long_term_liabilities_15").val();
var total_long_short_15 = parseInt(var_lib_15) + parseInt(var_lib2_15);    
$(".total_short_long_liabilities_15").val(total_long_short_15); 
    
});
    
    
/*Total equity 17-18*/
$(document).on("change", ".equity_17", function() {
var equity_17_sum = 0;
$(".equity_17").each(function(){
    equity_17_sum += +$(this).val();
});
$(".total_equity_17").val(equity_17_sum);
});
    
    /*Total equity 16-17*/
$(document).on("change", ".equity_16", function() {
var equity_16_sum = 0;
$(".equity_16").each(function(){
    equity_16_sum += +$(this).val();
});
$(".total_equity_16").val(equity_16_sum);
});
    
       /*Total equity 15-16*/
$(document).on("change", ".equity_15", function() {
var equity_15_sum = 0;
$(".equity_15").each(function(){
    equity_15_sum += +$(this).val();
});
$(".total_equity_15").val(equity_15_sum);
});
    
    
    
    /* total for liabilities and equity   */  
$(document).on('change', 'input', function() {        
var var_eqt_lib_17 = $(".total_equity_17").val();
var var_eqt2_lib_17 = $(".total_short_long_liabilities_17").val();
var total_eqt_lib_17 = parseInt(var_eqt_lib_17) + parseInt(var_eqt2_lib_17);    
$(".total_liabilities_and_equity_17").val(total_eqt_lib_17); 
    
var var_eqt_lib_16 = $(".total_equity_16").val();
var var_eqt2_lib_16 = $(".total_short_long_liabilities_16").val();
var total_eqt_lib_16 = parseInt(var_eqt_lib_16) + parseInt(var_eqt2_lib_16);    
$(".total_liabilities_and_equity_16").val(total_eqt_lib_16); 
    
var var_eqt_lib_15 = $(".total_equity_15").val();
var var_eqt2_lib_15 = $(".total_short_long_liabilities_15").val();
var total_eqt_lib_15 = parseInt(var_eqt_lib_15) + parseInt(var_eqt2_lib_15);    
$(".total_liabilities_and_equity_15").val(total_eqt_lib_15); 
    
});

$(document).ready(function() {
    if ("popup" in localStorage) {
    } else {
        localStorage.setItem("popup", "shown");
        $('#onload').modal('show');
    }
});


$(document).ready(function() {
    var text_max = parseInt($('#textarea').data("length"));
    $('#textarea_feedback').html(text_max + ' characters remaining');

    $('#textarea').keyup(function() {
        var text_length = $('#textarea').val().length;
        var text_remaining = text_max - text_length;

        $('#textarea_feedback').html(text_remaining + ' characters remaining');
    });
});

/* date dropdowns checker START */

$("#monthidd, #yearidd, #dayidd").change(function(){
	$("#jan1").show(); $("#feb1").show(); $("#mar1").show();
    $("#apr1").show(); $("#may1").show(); $("#jun1").show(); $("#jul1").show(); $("#aug1").show(); $("#sep1").show();
	$("#oct1").show(); $("#nov1").show(); $("#dec1").show();
  $("#d29").hide(); $("#d30").hide(); $("#d31").hide();
  var m = document.getElementById("monthidd");
  var y = document.getElementById("yearidd");
  var d = document.getElementById("dayidd");
  var iMonth = m.options[m.selectedIndex].value;
  var iYear = y.options[y.selectedIndex].value;
  var iday = d.options[d.selectedIndex].value;
  var dayVal = daysInMonth(iMonth -1, iYear);

  if(dayVal == 29){$("#d29").show();}
  if(dayVal == 30){$("#d29").show(); $("#d30").show();}
  if(dayVal == 31){$("#d29").show(); $("#d30").show(); $("#d31").show();}
  if(iYear == 2011){$("#jan1").hide(); $("#feb1").hide(); $("#mar1").hide();}
  if(iYear == 2014){$("#apr1").hide(); $("#may1").hide(); $("#jun1").hide(); $("#jul1").hide(); $("#aug1").hide(); $("#sep1").hide();
  		$("#oct1").hide(); $("#nov1").hide(); $("#dec1").hide();}

  if(iMonth != "MM" && iYear != "yyyy" && iday != "dd"){
   document.getElementById('dateeidd').value = iday+"-"+iMonth+"-"+iYear;
  }
  function daysInMonth(iMonth, iYear)
    {
     return 32 - new Date(iYear, iMonth, 32).getDate();
    }
});

/* date dropdowns checker END */


/* date dropdowns checker START */

$("#monthidd, #yearidd, #dayidd").change(function(){
 $("#jan1").show(); $("#feb1").show(); $("#mar1").show();
    $("#apr1").show(); $("#may1").show(); $("#jun1").show(); $("#jul1").show(); $("#aug1").show(); $("#sep1").show();
 $("#oct1").show(); $("#nov1").show(); $("#dec1").show();
  $("#d29").hide(); $("#d30").hide(); $("#d31").hide();
  var m = document.getElementById("monthidd");
  var y = document.getElementById("yearidd");
  var d = document.getElementById("dayidd");
  var iMonth = m.options[m.selectedIndex].value;
  var iYear = y.options[y.selectedIndex].value;
  var iday = d.options[d.selectedIndex].value;
  var dayVal = daysInMonth(iMonth -1, iYear);

  if(dayVal == 29){$("#d29").show();}
  if(dayVal == 30){$("#d29").show(); $("#d30").show();}
  if(dayVal == 31){$("#d29").show(); $("#d30").show(); $("#d31").show();}
  if(iYear == 2011){$("#jan1").hide(); $("#feb1").hide(); $("#mar1").hide();}
  if(iYear == 2014){$("#apr1").hide(); $("#may1").hide(); $("#jun1").hide(); $("#jul1").hide(); $("#aug1").hide(); $("#sep1").hide();
    $("#oct1").hide(); $("#nov1").hide(); $("#dec1").hide();}

  if(iMonth != "MM" && iYear != "yyyy" && iday != "dd"){
   document.getElementById('dateeidd').value = iday+"-"+iMonth+"-"+iYear;
  }
  function daysInMonth(iMonth, iYear)
    {
     return 32 - new Date(iYear, iMonth, 32).getDate();
    }
});

$("#yearidd").change(function(){
  var y = document.getElementById("yearidd");
  var iYear = y.options[y.selectedIndex].value;
  if(iYear==2011){
   $("#monthidd").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
  }
});


$('.partner-inner a').on('click', function() {
    if ( this.host !== window.location.host ) {
        if ( window.confirm('You are going to other website') ) {
        }
    }
});

/*$(document).ready(function(){
	$('[name="currentpath"]').val($(location).attr('pathname'));
	$('textarea,input[type=text]').bind('change', function () {
		if (this.value.match(/[^a-zA-Z0-9 .@_,\/-]/g)) {
		alert("You have entered an invalid data. Special Characters are not allowed except . @ _ and ,");	
		this.value = this.value.replace(/[^a-zA-Z0-9 .@_,]/g, '');
		}
		});
});*/
