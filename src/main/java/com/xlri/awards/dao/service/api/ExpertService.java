package com.xlri.awards.dao.service.api;

import java.util.List;

import com.xlri.awards.dao.entity.Expert;

public interface ExpertService {

	public boolean saveExpert(Expert expert);

	public boolean updateExpert(Expert expert);

	public List<Expert> getAllExpert();

	public Expert getExpertByEmail(String emailId);
	
	public Expert getExpertById(String id);
	
	public List<Expert> getExpertCountCheck(String emailId);
}
