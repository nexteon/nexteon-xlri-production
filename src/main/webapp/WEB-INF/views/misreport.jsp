<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="com.xlri.awards.common.StateUtil" %>
<!DOCTYPE html>
<html lang="en">
   <%@include file="dashboard/headerlibs.jsp" %>
   <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white" id="page-top">
   			<div class="page-wrapper">
          <%@include file="dashboard/header.jsp" %>
           


                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="/userdata">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>MIS Report</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title">MIS Report</h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <!-- BEGIN DASHBOARD STATS 1-->
                     
                     <div class="row">
                     	<div class="col-lg-12">
                     		<div class="table-responsive">
                     		<input type="button" id="btnExport" value="Download to Excel" class="btn btn-warning" style="float: right" />
                        <table class="table table-bordered mistable" id="mistable">
                            <tr class="datarow1">
                                <th>Regional Partner</th>
                                <th>A1-Bel-1L</th>
                                <th>A2-1L-10L</th>
                                <th>A3-10L-1C</th>
                                <th>B-DvInst</th>
                                <th>B-Incube</th>
                                <th>B-Mentor</th>
                                <th>B-RurPrd</th>
                                <th>Grand total</th>
                            </tr>
                            <tr class="iitb">
                                <td>IITB, Mumbai</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                            </tr>

                            <tr class="iitd">
                                <td>IITD, New Delhi</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                            </tr>

                            <tr class="iitg">
                                <td>IITG, Guwahati</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                            </tr>

                            <tr class="iitk">
                                <td>IITK, kanpur</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                            </tr>
                            <tr class="iitm">
                                <td>IITM, Chennai</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                            </tr>
                            <tr class="irma">
                                <td>IRMA, Anand</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                            </tr>

                            <tr class="manage">
                                <td>MANAGE, Hyderabad</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                            </tr>

                            <tr class="nabard">
                                <td>NABARD, Mumbai</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                            </tr>

                            <tr class="nif">
                                <td>NIF, Ahmedabad</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                            </tr>

                            <tr class="rseti">
                                <td>RSETI, Bangalore</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                            </tr>
                            <tr class="tiss">
                                <td>TISS, Mumbai</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                            </tr>
                            <tr class="xlri">
                                <td>XLRI, Jamshedpur</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                            </tr>
                            <tr class="datarow12">
                                <td class="totalCol">Grand Total</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                            </tr>
                        </table>
                    </div>
                     	</div>
                     </div>
                     <h1 class="page-title">Report</h1>
                     <div class="row">
                     	<div class="col-lg-12">
                     		
                     		<div class="row">
                     			<div class="col-lg-3">
                     				<div class="form-group">
                                          <label for="state">State</label>
                                          <select class="form-control" id="state-search" aria-required="true" required data-required-error="Please fill in this field.">
                                              <option value="" selected>Select</option>
                                              <c:set var="stateValues" value="<%=StateUtil.values()%>"/>
							              		<c:forEach items="${stateValues}" var="state">
							              			<option value="${state}">${state.name}</option>
							              		</c:forEach>
                                          </select>
                                      </div>
                     			</div>
                     			<div class="col-lg-3">
                     				<div class="form-group">
                                          <label for="state">Award Category</label>
                                          <select class="form-control" id="award-category-search" aria-required="true" required data-required-error="Please fill in this field.">
                                              <option value="" selected>Select</option>
                                              <option value="onelac">Initial Investment below 1 lakh</option>
                                              <option value="uptoten">Initial Investment between 1 lakh and 10 lakh</option>
                                              <option value="uptocrore">Initial Investment between 10 lakh and 1 Crore</option>
                                              <option value="Entrepreneurship Development Institutes/Organisations">Entrepreneurship Development Institutes/Organisations</option>
                                              <option value="Mentor">Mentor</option>
                                              <option value="Incubator">Incubator</option>
                                              <option value="Promoters of Rural Producer Enterprises">Promoters of Rural Producer Enterprises</option> 
                                          </select>
                                      </div>
                     			</div>
                     			<div class="col-lg-3">
                     				  <div class="form-group">
                                          <label for="state">Award Sub Category</label>
                                          <select class="form-control" id="award-sub-category-search" aria-required="true" required data-required-error="Please fill in this field.">
                                              <option value="" selected>Select</option>
                                              <c:set var="awardSub" value="${distinctAwardSubCat}"/>
							              		<c:forEach items="${awardSub}" var="sub">
							              			<c:if test="${not empty sub}">
							              				<c:set var="enco" value="${fn:replace(sub, '&', '%26')}" />
							              				<option value="${enco}">${sub}</option>
							              			</c:if>
							              		</c:forEach>
                                          </select>
                                      </div>
                     			</div>
                     			<div class="col-lg-3">
                     				<button class="btn btn-info" id="button-search">Search</button>
                     			</div>
                     		</div>
                     		<div class="row">
                     			<div class="col-lg-12">
                     				<h4>Total Count: <span class="total-count"></span></h4>
                     				<div class="response-data row">
                     					<div class="award-cat-res col-lg-6">
                     					</div>
                     					<div class="award-sub-res col-lg-6">
                     					</div>
                     				</div>
                     			</div>
                     		</div>
                     	</div>
                     </div>
                     
                     	</div>
                        </div>
                        </div>
                       <!-- END QUICK NAV -->
     <%@include file="dashboard/footer.jsp"%>
 
<script>
	$(document).ready(function(){
		var data = JSON.parse('${data}');
		for (var key in data) {
			var actKey = key.split("-");
			if(actKey[1] == 'onelac'){
				$('.'+actKey[0].split("@")[0]).find('td:nth-child(2)').text(data[key]);
			}
			if(actKey[1] == 'uptoten'){
				$('.'+actKey[0].split("@")[0]).find('td:nth-child(3)').text(data[key]);
			}   
			if(actKey[1] == 'uptocrore'){
				$('.'+actKey[0].split("@")[0]).find('td:nth-child(4)').text(data[key]);
			}
			if(actKey[1] == 'Entrepreneurship Development Institutes/Organisations'){
				$('.'+actKey[0].split("@")[0]).find('td:nth-child(5)').text(data[key]);
			}
			if(actKey[1] == 'Mentor'){
				$('.'+actKey[0].split("@")[0]).find('td:nth-child(6)').text(data[key]);
			}   
			if(actKey[1] == 'Incubator'){
				$('.'+actKey[0].split("@")[0]).find('td:nth-child(7)').text(data[key]);
			}  
			if(actKey[1] == 'Promoters of Rural Producer Enterprises'){
				$('.'+actKey[0].split("@")[0]).find('td:nth-child(8)').text(data[key]);
			}  
		}
		var totals=[0,0,0,0,0,0,0,0];
		var $dataRows=$(".mistable tr:not('.datarow1, .datarow12')");
		$dataRows.each(function() {
            $(this).find('td').each(function(i){        
                totals[i]+=parseInt( $(this).html());
            });
        });
		$(".mistable .datarow12 td").each(function(i){
			if(i!=0){
            	$(this).html(totals[i]);
			}
        });
		
		var $dataCols=$(".mistable tr:not(tr:first-child)")
		$dataCols.each(function(i) {
			var sum = 0;
            $(this).find('td:not(td:first-child,td:last-child)').each(function(i){  
                sum = sum + parseInt($(this).text());
            });
            $(this).find('td:last-child').text(sum); 
        });
		
		
		
		$("#btnExport").click(function(e) {
	        e.preventDefault();
	        
	        var data_type = 'data:application/vnd.ms-excel';
	        var table_div = document.getElementById('mistable');

	        var table_html = table_div.outerHTML.replace(/ /g, '%20');
	        
	        var a = document.createElement('a');
	        document.body.appendChild(a);
	        a.href = data_type + ', ' + table_html;
	        a.download = 'MIS Table.xls';
	        a.click();
	    });
		
	});
	
	
	$(document).ready(function(){
		$('#button-search').click(function(){
			var state = $('#state-search option:selected').val();
			var awardcat = $('#award-category-search option:selected').val();
			var awardsub = $('#award-sub-category-search option:selected').val();
			if(state === '' && awardcat === '' && awardsub === ''){
				alert("Please select value in any one of the three drop-downs.");
			}else{
				$.ajax({
					url: '/neas/api/get/searchdata?state='+state+'&awardcat='+awardcat+'&awardsub='+awardsub,
					type: 'GET',
					success: function(result){
						$('.total-count').html(result['Count']);
						var cat = "<table class='table table-bordered table-reponsive'>";
						var sub = "<table class='table table-bordered table-reponsive'>"; var x = 1;
						for(var k in result) {
							if(k!=='Count'){
							if(x<8){
								cat += "<tr><td>"+k+"</td><td>"+result[k]+"</td></tr>";
								x++;
							}else{
								sub += "<tr><td>"+k+"</td><td>"+result[k]+"</td></tr>";
								x++;
							}
							}
						}
						$('.award-cat-res').html(cat+"</table>");
						$('.award-sub-res').html(sub+"</table>");
					}
				});
			}
			
		});
	});
	
	
</script>
     
     </body>
         </html>    