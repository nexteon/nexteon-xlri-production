package com.xlri.awards.dao.entity;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import com.xlri.awards.common.EncryptionUtil;

@Entity
@Table(name = "Nominator",uniqueConstraints=@UniqueConstraint
		(columnNames={"registrationNumber","emailId","contactNumber"}))
public class Nominator {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(nullable=false,unique=true)
	private String registrationNumber;
	@Column(nullable=false)
	private String nominationType;
	@Column(nullable=false)
	private String fullName;
	
	private String instituteName;
	
	private String designation;

	private String addrLine1;
	private String addrLine2;
	@Column(nullable=false)
	private String addrCity;
	@Column(nullable=false)
	private String addrState;

	private String addrLandmark;
	@Column(nullable=false)
	private String addrPinCode;
	@Column(nullable=false,unique=true)
	private String emailId;
	@Column(nullable=false)
	private String password;
	private String salt;
	@Column(nullable=false,unique=true)
	private String contactNumber;
	private String occupation;
	private String website;
	private boolean isActivated;
	private String otp;
	@Temporal(TemporalType.TIMESTAMP)
	private Date otpExpiry;
    private String role;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;
    
    @Column(nullable=false)
    private int enterpriseCount;
    @Column(nullable=false)
    private int ecosystemCount;
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		//this.registrationNumber = CommonUtil.generateRegistrationNumber(state);
		this.registrationNumber = registrationNumber;
	}

	public String getNominationType() {
		return nominationType;
	}

	public void setNominationType(String nominationType) {
		this.nominationType = nominationType;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getAddrLine1() {
		return addrLine1;
	}

	public void setAddrLine1(String addrLine1) {
		this.addrLine1 = addrLine1;
	}

	public String getAddrLine2() {
		return addrLine2;
	}

	public void setAddrLine2(String addrLine2) {
		this.addrLine2 = addrLine2;
	}

	public String getAddrCity() {
		return addrCity;
	}

	public void setAddrCity(String addrCity) {
		this.addrCity = addrCity;
	}

	public String getAddrState() {
		return addrState;
	}

	public void setAddrState(String addrState) {
		this.addrState = addrState;
	}

	public String getAddrLandmark() {
		return addrLandmark;
	}

	public void setAddrLandmark(String addrLandmark) {
		this.addrLandmark = addrLandmark;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.getNewSalt();
		this.password = EncryptionUtil.encryptPass(password + this.getSalt());
		//this.password = password;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public boolean getIsActivated() {
		return isActivated;
	}

	public void setIsActivated(boolean isActivated) {
		this.isActivated = isActivated;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp() {
		try {
			this.generatePin();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getRole() {
		return role;
	}

	public String getInstituteName() {
		return instituteName;
	}

	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getAddrPinCode() {
		return addrPinCode;
	}

	public void setAddrPinCode(String addrPinCode) {
		this.addrPinCode = addrPinCode;
	}

	public Date getOtpExpiry() {
		return otpExpiry;
	}

	public void setOtpExpiry() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.MINUTE, 15);
		this.otpExpiry = cal.getTime();
	}
		
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn() {
		this.createdOn = new Date();
	}

	public int getEnterpriseCount() {
		return enterpriseCount;
	}

	public void setEnterpriseCount(int enterpriseCount) {
		this.enterpriseCount = enterpriseCount;
	}

	public int getEcosystemCount() {
		return ecosystemCount;
	}

	public void setEcosystemCount(int ecosystemCount) {
		this.ecosystemCount = ecosystemCount;
	}

	public void setActivated(boolean isActivated) {
		this.isActivated = isActivated;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public void setOtpExpiry(Date otpExpiry) {
		this.otpExpiry = otpExpiry;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	private void getNewSalt() {
		this.salt = EncryptionUtil.generateRandomString();
	}

	public void generatePin() throws Exception {
		Random generator = new Random();
		generator.setSeed(System.currentTimeMillis());

		int num = generator.nextInt(99999) + 99999;
		if (num < 100000 || num > 999999) {
			num = generator.nextInt(99999) + 99999;
			if (num < 100000 || num > 999999) {
				throw new Exception("Unable to generate PIN at this time..");
			}
		}
		this.otp = String.valueOf(num);
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}



}
