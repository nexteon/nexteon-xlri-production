<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script src="/resources/js/jquery.min.js"></script>
<script src="/resources/js/bootstrap.min.js"></script>
<script src="/resources/js/validator.min.js"></script>
<script src="/resources/js/owl.carousel.min.js"></script>
<script src="/resources/js/fontselector.js"></script>
<script src="/resources/js/colorChanger.js"></script>
<script src="/resources/js/color-selection.js"></script>
<script src="/resources/js/bootstrap-dropdownhover.js"></script>
<script src="/resources/js/jquery.picEyes.js"></script>
<script src="/resources/js/jquery-ui.js"></script>
<script src="/resources/js/custom.js"></script>

