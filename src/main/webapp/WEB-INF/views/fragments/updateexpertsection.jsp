<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import = "java.io.*,java.util.*" %>
 <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>NEAS - Dashboard</title>

    <!-- Bootstrap core CSS-->
   
   
    <link href="/resources/dashboard/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/dashboard/css/font-awesome.css" rel="stylesheet" type="text/css">   
    <link href="/resources/dashboard/css/dataTables.css" rel="stylesheet">
    <link href="/resources/dashboard/css/custom.css" rel="stylesheet">
    <link href="/resources/dashboard/css/richtext.min.css" rel="stylesheet" >

  </head>

  <body id="page-top" >

    <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

      <a class="navbar-brand mr-1" href="/userdata">NEAS Dashboard</a>

      <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
      </button>

      <!-- Navbar -->
      <ul class="navbar-nav ml-auto ml-md-0">

		 <sec:authorize access="hasRole('ADMIN')">
				<sec:authentication var="name" property="principal" />
				<c:set var="username" value="${fn:split(name,'@')}" />
				<li><a href="/userdata">Welcome, <c:out value="${username[0]}" /></a></li>
		 </sec:authorize>
										
			<li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user-circle fa-fw"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
            
            <a class="dropdown-item" href="/userdata"><c:out value="${username[0]}" /></a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="/logout" data-toggle="modal" data-target="#logoutModal">Logout</a>
           
          </div>
        </li>
        
      </ul>

    </nav>

    <div id="wrapper">
<!-- Sidebar -->
      <ul class="sidebar navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="/userdata">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/newssections">
            <i class="fa fa-newspaper" aria-hidden="true"></i>
            <span>News-Section</span></a>
        </li>
        <li class="nav-item dropdown show">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <i class="fas fa-fw fa-folder"></i>
            <span>Expert</span>
          </a>
          <div class="dropdown-menu show" aria-labelledby="pagesDropdown" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(5px, 56px, 0px);">
            <h6 class="dropdown-header">Expert Details:</h6>
            <a class="dropdown-item" href="/expertsection">Add Expert</a>
            <a class="dropdown-item" href="#">All Expert Table</a>
            <a class="dropdown-item" href="/updateexpertsection">Update Experts</a>
            <div class="dropdown-divider"></div>
            
          </div>
        </li>
      </ul>


      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Overview</li>
          </ol>
          
          
       <!-- Form Container -->
   <div class="form-outer">
           <h2>Modify Expert Detalis</h2>
                <div class="form">
                    <form form onsubmit="alert('success');" action="/updateexpertsection" method="post">
                        <div class="row">
                        <div class="col-md-6">
                            <div class="form-group has-error has-danger">
                                <label for="expertName">Expert Name</label>
                                <input type="text" name="expertName" id="expertName" class="form-control" placeholder="Expert Name" required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="emailId">Email</label>
                                <input type="email" name="emailId" id="emailId" class="form-control" placeholder="Email" required="">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="contactNumber">Contact No</label>
                                <input type="tel" name="contactNumber" id="contactNumber" class="form-control" placeholder="Contact No" maxlength="10" pattern="[0-9]{10}" required="">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="designation">Designation</label>
                                <input type="text" name="designation" id="designation" class="form-control" placeholder="Designation" required="">
                                
                                </select>
                            </div>
                           
                        </div>
						<div class="clearfix"></div>
                            <div class="col-md-6">
                             <div class="form-group">

                                <div class="other-fieldhide">
                                    <label class="vis-hidden" for="specialization">Specialization</label>
                                  <select name="specialization" id="specialization" class="form-control">
                                                     
														<option value="" style="font-weight:bold;">Expert Specialization *</option>


                                                     
														<option value="Textiles, leather and related goods (Goods)">Textiles, leather and related goods (Goods)</option><option value="Agri, food and forestry products (Goods)">Agri, food and forestry products (Goods)</option><option value="Chemicals, pharma, bio and other processed materials (Goods)">Chemicals, pharma, bio and other processed materials (Goods)</option><option value="Engineering systems (Goods)">Engineering systems (Goods)</option><option value="Renewable energy and waste  (Goods)">Renewable energy and waste  (Goods)</option><option value="Handicrafts, traditional manufacturing &amp; other goods (Goods)">Handicrafts, traditional manufacturing &amp; other goods (Goods)</option><option value="IT &amp;ITES, Financial (Services)">IT &amp;ITES, Financial (Services)</option><option value="Education (Services)">Education (Services)</option><option value="Healthcare (Services)">Healthcare (Services)</option><option value="Hospitality (Services)">Hospitality (Services)</option><option value="E-Commerce, logistics, transport &amp; other services (Services)">E-Commerce, logistics, transport &amp; other services (Services)</option><option value="Social Enterprise">Social Enterprise</option><option value="Barefoot">Barefoot</option>   
														</select>
                                </div>


                            </div>
                            </div>
                       
                            <div class="col-md-6">
                             <div class="form-group">

                                <div class="other-fieldhide">
                                    <label class="vis-hidden" for="address">Expert Address</label>
                                    <input type="text" name="address" id="address" class="form-control" placeholder="Expert Address">
                                </div>
                                </div>
                                </div>

								 <div class="col-md-6">
                             <div class="form-group">

                                <div class="other-fieldhide">
                                    <label class="vis-hidden" for="expert-address"></label>
                                    <select name="partnerWith" id="partnerWith" class="form-control">
                                                     
														<option value="" style="font-weight:bold;">Select Patrners</option>


                                                     
														<option value="Indian Institute of Technology Delhi">Indian Institute of Technology Delhi</option><option value="Indian Institute of Technology Kanpur">Indian Institute of Technology Kanpur</option><option value="Indian Institute of Technology Bombay">Indian Institute of Technology Bombay</option><option value="">Indian Institute of Technology Madras</option><option value="Indian Institute of Management Ahmedabad">Indian Institute of Management Ahmedabad</option><option value="XLRI Jamshedpur">XLRI Jamshedpur</option><option value="Tata Institute of Social Sciences (TISS), Mumbai">Tata Institute of Social Sciences (TISS), Mumbai</option>   
														</select>
                                </div>

                            </div>
                            </div>
                            
                            <div class="col-md-6">
                             <div class="form-group">

                                <div class="other-fieldhide">
                                    <label class="vis-hidden" for="password">Password</label>
                                    <input type="text" name="password" id="password" class="form-control" placeholder="Password">
                                </div>
                                </div>
                                </div>
                                
                        <div class="col-md-12">
                            <div class="submit-btn">
                                <div class="btn-group">
                                      <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                    <input class="btn btn-primary" type="submit" name="Submit" value="Submit">
                                </div>
                            </div>


                        </div>
                        </div>
                    </form>

                </div>
</div>
 



        <!-- Sticky Footer -->
        <footer class="sticky-footer">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span>Copyright © XLRI 2018</span>
            </div>
          </div>
        </footer>

      </div>
      </div>
      </div>
      <!-- /.content-wrapper -->

   
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="/logout">Logout</a>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="/resources/dashboard/js/jquery.min.js"></script>
    <script src="/resources/dashboard/js/angular.min.js"></script>
    <script src="/resources/dashboard/js/bootstrap.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="/resources/dashboard/js/jquery.easing.min.js"></script>

    <script src="/resources/dashboard/js/jquery.dataTables.js"></script>
    <script src="/resources/dashboard/js/dataTables.bootstrap4.js"></script>
<script src="/resources/dashboard/js/datatables-demo.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="/resources/dashboard/js/custom.js"></script>
    
    <script src="/resources/dashboard/js/jquery.richtext.js"></script>
    
   
    
  </body>

</html>
