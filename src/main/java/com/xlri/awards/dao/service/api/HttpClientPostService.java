package com.xlri.awards.dao.service.api;

import org.apache.sling.commons.json.JSONObject;

public interface HttpClientPostService {

	public JSONObject postToURL(String postUrl, JSONObject json);
	
}