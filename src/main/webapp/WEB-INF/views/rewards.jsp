<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
   <%@include file="fragments/headerlibs.jsp" %>
   <body class="innerpage">
      <%@include file="fragments/header.jsp" %>
      <!-- Body Starts Here -->
      <main role="main">
         <div class="page-title">
            <h2>REWARD</h2>
         </div>
         <div class="content">
            <div class="container">
               <div class="row">
                  <div class="col-md-12">
                     <div class="whitebg awards-page">
                            <div class="awards-inner">
                            <div class="text-centerfull">
                           <div class="award-hdg blue-hdg">Winners will be felicitated in a high profile Award Ceremony and will be presented</div>
                           <ul>
                              <li>Trophy</li>
                              <li>Certificate</li>
                              <li>Cash Prize worth 2.3 Cr.</li>
                              <li>
                              <ul>
                               <li>Enterprise/ Individuals - 5 lakh</li>
                              <li>Organisations/ Institutions - 10 lakh</li>
                              </ul>
                              </li>
                           </ul>
                          </div>
                        </div>
                        </div>
                  </div>
               </div>
            </div>
         </div>
      </main>
      <!-- Body Ends Here -->
      <%@include file="fragments/footer.jsp" %>
   </body>
</html>