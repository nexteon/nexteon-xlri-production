package com.xlri.awards.common;

public enum StateUtil {
	AN("ANDAMAN AND NICOBAR ISLANDS"),
	AD("ANDHRA PRADESH"),
	AR("ARUNACHAL PRADESH"),
	AS("ASSAM"),
	BR("BIHAR"),
	CH("CHANDIGARH"),
	CG("CHATTISGARH"),
	DN("DADRA AND NAGAR HAVELI"),
	DD("DAMAN AND DIU"),
	DL("DELHI"),
	GA("GOA"),
	GJ("GUJARAT"),
	HR("HARYANA"),
	HP("HIMACHAL PRADESH"),
	JK("JAMMU AND KASHMIR"),
	JH("JHARKHAND"),
	KA("KARNATAKA"),
	KL("KERALA"),
	LD("LAKSHADWEEP ISLANDS"),
	MP("MADHYA PRADESH"),
	MH("MAHARASHTRA"),
	MN("MANIPUR"),
	ML("MEGHALAYA"),
	MZ("MIZORAM"),
	NL("NAGALAND"),
	OD("ODISHA"),
	PY("PONDICHERRY"),
	PB("PUNJAB"),
	RJ("RAJASTHAN"),
	SK("SIKKIM"),
	TN("TAMIL NADU"),
	TS("TELANGANA"),
	TR("TRIPURA"),
	UP("UTTAR PRADESH"),
	UK("UTTARAKHAND"),
	WB("WEST BENGAL");

 
	private String name;
	private StateUtil(String name) {
	        this.name = name;
	}
	public String getName() {
	        return name;
	}
 
	@Override
	public String toString() {
	        return name;
	}
}