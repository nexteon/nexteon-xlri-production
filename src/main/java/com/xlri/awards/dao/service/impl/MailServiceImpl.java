package com.xlri.awards.dao.service.impl;

import java.io.UnsupportedEncodingException;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xlri.awards.common.EncryptionUtil;
import com.xlri.awards.common.MailCommons;
import com.xlri.awards.common.OtpSalt;
import com.xlri.awards.dao.entity.Evaluation;
import com.xlri.awards.dao.entity.Expert;
import com.xlri.awards.dao.entity.Nominator;
import com.xlri.awards.dao.entity.NomineeDetails;
import com.xlri.awards.dao.entity.Partners;
import com.xlri.awards.dao.entity.User;
import com.xlri.awards.dao.service.api.ExpertService;
import com.xlri.awards.dao.service.api.MailService;
import com.xlri.awards.dao.service.api.NominatorService;
import com.xlri.awards.dao.service.api.UserService;

@Repository
public class MailServiceImpl implements MailService{

	
	
	@Autowired
	UserService userService;
	
	@Autowired
	NominatorService nominatorService;
	
	@Autowired
	ExpertService expertService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MailServiceImpl.class);
	
	@Override
	public void sendOtp(Nominator user) {
		MimeMessage mimeMessage = new MailCommons().mimeMessage();
		try {
			mimeMessage.setFrom("contact-neas@nic.in");
			mimeMessage.setRecipient(Message.RecipientType.TO,new InternetAddress(user.getEmailId(),"Team NEA 2018"));
			mimeMessage.setSubject("Team NEA 2018 :: Account Verification");
			StringBuilder sb=new StringBuilder();
			sb.append("Dear "+user.getFullName()+",");
			sb.append("<br/><br/>Thank you for registering as 'Nominator' for the National Enterpreneurship Awards 2018 and shouldering the responsibility to find the first generation young entrepreneurs. Kindly use the below OTP to activate your Nominator Account. ");
			sb.append("Also, find below the 'Nominator Registration Number' for the future reference. This Registration Number will be same for all the nominations filed by you. ");
			sb.append("<br/><br/><b>Registration Number: </b>"+user.getRegistrationNumber());
			sb.append("<br/> This OTP will expire in <b>10 minutes </b>.");
			sb.append("<br/><b>OTP: </b>"+user.getOtp());
			sb.append("<br/><br/>Use the following link to activate your account.");
			String encrypt = "";
			try {
				encrypt = EncryptionUtil.encrypt(user.getRegistrationNumber(), OtpSalt.SALT.getSalt());
			} catch (Exception e) {
				e.printStackTrace();
			}
			sb.append("<a href='http://neas.gov.in/otp/verify/"+encrypt+"'>Click Here</a>");
			sb.append("<br/><br/>Thank You,<br/><br/>Team NEA 2018");
			mimeMessage.setContent(sb.toString(), "text/html; charset=utf-8");
			Transport.send(mimeMessage); 
		} catch (MessagingException e) {
			LOGGER.error(e.getMessage());
		} catch (UnsupportedEncodingException e1) {
			LOGGER.error(e1.getMessage());
			e1.printStackTrace();
		}
	}
	
	@Override
	public void sendNomineeMail(NomineeDetails nominee,String password) {
		MimeMessage mimeMessage = new MailCommons().mimeMessage();
		try {
			mimeMessage.setFrom("contact-neas@nic.in");
			mimeMessage.setRecipient(Message.RecipientType.TO,new InternetAddress(nominee.getEmailId(),"Team NEA 2018"));
			mimeMessage.setSubject("Team NEA 2018 :: Successful Nomination");
			StringBuilder sb=new StringBuilder();
			sb.append("Dear "+nominee.getNomineeName()+",");
			sb.append("<br/><br/>Greetings from the team of National Enterpreneurship Awards(NEA) 2018. ");
			String cat = "";
			if(nominee.getEnterpriseCategory().equals("onelac")){
				cat = "A1 (Initial Investment upto 1 Lakh)";
			}else if(nominee.getEnterpriseCategory().equals("uptoten")){
				cat = "A2 (Initial Investment between 1 Lakh to 10 Lakh)";
			}else if(nominee.getEnterpriseCategory().equals("uptocrore")){
				cat = "A3 (Initial Investment between 10 Lakh to 1 Crore)";
			}else{
				cat = nominee.getEnterpriseCategory();
			}
			sb.append("<br/><br/>We are pleased to inform you that you have been successfully nominated for NEA 2018 in '"+(nominee.getAwardCategory().equals("enterprise")?"Enterprise Award Track":"Ecosystem Builders Track")+"'' under category '"+cat+"'.");
			if(nominee.getEnterpriseCategory().equals("onelac")){
				sb.append("Your nomination will undergo an evaluation in the next stage and based on the score secured in the evaluation, your nomination will be considered for the next round of assessment,i.e., Field verification.");
				sb.append("<br/><br/>You will be updated soon about the status of your nomination via Email and SMS.");
			}else if(nominee.getAwardCategory().equals("ecosystem")){
				sb.append("<br/><br/>Your Nomination will undergo an evaluation and based on the score secured in the evaluation your nomination may be considered for the next stage of evaluation; Field Verification. We may call or write to you for more details on your nomination.");
				sb.append("<br/><br/>You will be updated soon about the status of your nomination via registered Email and SMS. ");
			}else{
				sb.append("<br/><br/>Your Nomination will undergo an evaluation and based on the score secured in the evaluation your nomination may be considered for the next stage of data submission.");
				sb.append("<br/><br/>You will be updated soon about the status of your nomination via Email and SMS.");
			}
			sb.append("To check the status of your nomination by yourself, activate account by clicking on the link - ");
			String encrypt = "";
			try {
				encrypt = EncryptionUtil.encrypt(nominee.getEmailId(), OtpSalt.SALT.getSalt());
			} catch (Exception e) {
				e.printStackTrace();
			}
			sb.append("<a href='http://neas.gov.in/verify/nominee/"+encrypt+"'>Click Here</a>");
			
			sb.append("<br/><br/>Use the following credentials once your account is activated.<br/><br/><b>Username: </b>"+nominee.getEmailId());
			sb.append("<br/><b>password: </b>"+password);
			
			sb.append("<br/><br/>Thank You,<br/>Team NEA 2018");
			mimeMessage.setContent(sb.toString(), "text/html; charset=utf-8");
			Transport.send(mimeMessage);
		} catch (MessagingException e) {
			LOGGER.error(e.getMessage());
		} catch (UnsupportedEncodingException e1) {
			LOGGER.error(e1.getMessage());
		}
	}
	
	@Override
	public boolean changePasswordMail(User user){
		MimeMessage mimeMessage = new MailCommons().mimeMessage();
		try {
			mimeMessage.setFrom("contact-neas@nic.in");
				mimeMessage.setRecipient(Message.RecipientType.TO,new InternetAddress(user.getUsername(),"Team NEA 2018"));
				mimeMessage.setSubject("Team NEA 2018 :: Change Password");
				StringBuilder sb=new StringBuilder();
				sb.append("Dear "+user.getFullName()+",");
				sb.append("<br/><br/>We have recieved a change password request for your account.");
				sb.append("<br/><br/>Use the following link to change your account password.");
				sb.append("<br/><a href='http://neas.gov.in/forgetpassword/"+EncryptionUtil.encrypt(user.getUsername(), OtpSalt.SALT.toString())+"'>Click Here</a>");
				sb.append("<br/><br/>Thank You,<br/>Team NEA 2018");
				mimeMessage.setContent(sb.toString(), "text/html; charset=utf-8");
				Transport.send(mimeMessage);
			return true;
		} catch (AddressException e) {
			e.printStackTrace();
			return false;
		} catch (MessagingException e) {
			e.printStackTrace();
			return false;
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
			return false;
		}
	}
	
	@Override
	public void sendNominatorMail(NomineeDetails nominatorUser) {
		MimeMessage mimeMessage = new MailCommons().mimeMessage();
		try {
			mimeMessage.setFrom("contact-neas@nic.in");
			String regNo = nominatorUser.getRegistrationNumber();
			Nominator nominator = nominatorService.getNominatorByRegistrationNumber(regNo);
			mimeMessage.setRecipient(Message.RecipientType.TO,new InternetAddress(nominator.getEmailId(),"Team NEA 2018"));
			mimeMessage.setSubject("Team NEA 2018 :: Successful Nomination");
			StringBuilder sb=new StringBuilder();
			sb.append("Dear "+nominator.getFullName()+",");
			String category = "";
			if(nominatorUser.getEnterpriseCategory().equals("onelac")){
				category = "Initial investment upto 1 lakh";
			}else if(nominatorUser.getEnterpriseCategory().equals("uptoten")){
				category = "Initial investment between 1 lakh to 10 lakh";
			}else if(nominatorUser.getEnterpriseCategory().equals("uptocrore")){
				category = "Initial investment between 10 lakh to 1 Crore";
			}else{
				category = nominatorUser.getEnterpriseCategory();
			}
			sb.append("<br/><br/>Thank you for successfully nominating ("+nominatorUser.getEnterpriseName()+") for the National Enterpreneurship Award 2018 in the '"+(nominatorUser.getAwardCategory().equals("enterprise")?"Enterprise Award Track":"Ecosystem Builders Track")+"' under the category '"+category+"'");
			if(nominatorUser.getEnterpriseCategory().equals("onelac")){
				sb.append("Your nomination will undergo an evaluation in the next stage and based on the score secured in the evaluation, the nomination may be considered for the next round of assessment,i.e., Field verification.");
				sb.append("<br/>You will be updated soon about the status of your nomination via Email and SMS.");
			}else if(nominatorUser.getAwardCategory().equals("ecosystem")){
				sb.append("<br/><br/>Your nomination will undergo any evaluation process and based on the score secured in the evaluation the nomination may be considered for the next stage of evaluation; Field Verification.");
				sb.append("<br/><br/>You will be updated soon about the status of your nomination via registered Email and SMS.");
			}else{
				sb.append("<br/><br/>Your Nomination will undergo an evaluation and based on the score secured in the evaluation, the nomination may be considered for the next stage of data submission.");
				sb.append("<br/>You will be updated soon about the status of your nomination via Email and SMS.");
			}
			sb.append("<br/><br/>Thank You,<br/>Team NEA 2018");
			mimeMessage.setContent(sb.toString(), "text/html; charset=utf-8");
			Transport.send(mimeMessage);
		} catch (MessagingException e) {
			LOGGER.error(e.getMessage());
		} catch (UnsupportedEncodingException e) {
			LOGGER.error(e.getMessage());
		}
	}
	
	@Override
	public boolean sendPasswordToExpertMail(Expert expert,String password){
		MimeMessage mimeMessage = new MailCommons().mimeMessage();
		try {
			mimeMessage.setFrom("contact-neas@nic.in");
				mimeMessage.setRecipient(Message.RecipientType.TO,new InternetAddress(expert.getEmailId()));
				mimeMessage.setSubject("Congratulations! You have successfully registered - NEA 2018");
				StringBuilder sb=new StringBuilder();
				sb.append("Dear "+expert.getExpertName()+",");
				sb.append("<br/><br/>Use the following password to your account.");
				sb.append("Your Password is "+password);
				sb.append("<br/><br/>Thank You,<br/>Team NEA 2018");
				mimeMessage.setContent(sb.toString(), "text/html; charset=utf-8");
				Transport.send(mimeMessage);
			return true;
		} catch (AddressException e) {
			e.printStackTrace();
			return false;
		} catch (MessagingException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	@Override
	public boolean sendPasswordToPartnersMail(Partners partners,String password){
		MimeMessage mimeMessage = new MailCommons().mimeMessage();
		try {
			mimeMessage.setFrom("contact-neas@nic.in");
				mimeMessage.setRecipient(Message.RecipientType.TO,new InternetAddress(partners.getEmailId()));
				mimeMessage.setSubject("Congratulations! You have successfully registered - NEA 2018");
				StringBuilder sb=new StringBuilder();
				sb.append("Dear "+partners.getPartnerName()+",");
				sb.append("<br/><br/>Use the following password to your account.");
				sb.append("Your Password is "+ password);
				sb.append("<br/><br/>Thank You,<br/>Team NEA 2018");
				mimeMessage.setContent(sb.toString(), "text/html; charset=utf-8");
				Transport.send(mimeMessage);
			return true;
		} catch (AddressException e) {
			e.printStackTrace();
			return false;
		} catch (MessagingException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public void sendNomineeSupportMail(NomineeDetails nominee) {
		MimeMessage mimeMessage = new MailCommons().mimeMessage();
		try {
			String active = null;
			String decline = null;
			if(nominee.getFilledStatus().equals("ACCEPTED")){
				active = "Your submition is Accepted by our team";
			}
			if(nominee.getFilledStatus().equals("DECLINED")){
				decline = "Your submition is Declined by our team";
			}
			mimeMessage.setFrom("contact-neas@nic.in");
			mimeMessage.setRecipient(Message.RecipientType.TO,new InternetAddress(nominee.getEmailId(),"Team NEA 2018"));
			mimeMessage.setSubject("Team NEA 2018 :: "+ active );
			StringBuilder sb=new StringBuilder();
			sb.append("Dear "+nominee.getNomineeName()+",");
			sb.append("<br/><br/>Greetings from the team of National Enterpreneurship Awards(NEA) 2018. ");
			if(nominee.getFilledStatus().equals("ACCEPTED")){
				sb.append("<br/> Your submission with registration number: "+nominee.getRegistrationNumber()+", has been accepted by our NEA support team");
			}
			if(nominee.getFilledStatus().equals("DECLINED")){
				sb.append("<br/> Your submission with registration number: "+nominee.getRegistrationNumber()+", has been declined by our NEA support team");
			}
			sb.append("<br/><br/>Thank You,<br/>Team NEA 2018");
			mimeMessage.setContent(sb.toString(), "text/html; charset=utf-8");
			Transport.send(mimeMessage);
		} catch (MessagingException e) {
			LOGGER.error(e.getMessage());
		} catch (UnsupportedEncodingException e1) {
			LOGGER.error(e1.getMessage());
		}
		
	}

	@Override
	public void sendNominatorSupportMail(Nominator nominator, NomineeDetails nominee) {
		MimeMessage mimeMessage = new MailCommons().mimeMessage();
		try {
			String active = null;
			String decline = null;
			if(nominee.getFilledStatus().equals("ACCEPTED")){
				active = "Your submition of "+nominee.getNomineeName()+" (Nominee) is Accepted by our team";
			}
			if(nominee.getFilledStatus().equals("DECLINED")){
				decline = "Your submition of "+nominee.getNomineeName()+" (Nominee) is Declined by our team";
			}
			mimeMessage.setFrom("contact-neas@nic.in");
			mimeMessage.setRecipient(Message.RecipientType.TO,new InternetAddress(nominator.getEmailId(),"Team NEA 2018"));
			if(nominee.getFilledStatus().equals("ACCEPTED")){
				mimeMessage.setSubject("Team NEA 2018 :: "+ active );
			}
			if(nominee.getFilledStatus().equals("DECLINED")){
				mimeMessage.setSubject("Team NEA 2018 :: "+ decline );
			}
			StringBuilder sb=new StringBuilder();
			sb.append("Dear "+nominator.getFullName()+",");
			sb.append("<br/><br/>Greetings from the team of National Enterpreneurship Awards(NEA) 2018. ");
			if(nominee.getFilledStatus().equals("ACCEPTED")){
				sb.append("<br/> Your submission with registration number: "+nominee.getRegistrationNumber()+" of "+nominee.getNomineeName()+" (nominee), has been accepted by our NEA support team");
			}
			if(nominee.getFilledStatus().equals("DECLINED")){
				sb.append("<br/> Your submission with registration number: "+nominee.getRegistrationNumber()+" of "+nominee.getNomineeName()+" (nominee), has been declined by our NEA support team");
			}
			sb.append("<br/><br/>Thank You,<br/>Team NEA 2018");
			mimeMessage.setContent(sb.toString(), "text/html; charset=utf-8");
			Transport.send(mimeMessage);
		} catch (MessagingException e) {
			LOGGER.error(e.getMessage());
		} catch (UnsupportedEncodingException e1) {
			LOGGER.error(e1.getMessage());
		}
		
	}
	
	@Override
	public void sendRemindSupportNominator(String mailTo, String subject, String msg, Nominator nominator) {
		MimeMessage mimeMessage = new MailCommons().mimeMessage();
		try {
			mimeMessage.setFrom("contact-neas@nic.in");
			mimeMessage.setRecipient(Message.RecipientType.TO,new InternetAddress(mailTo,"Team NEA 2018"));
			mimeMessage.setSubject("Support Team NEA 2018 :: "+ subject );
			StringBuilder sb=new StringBuilder();
			sb.append("Dear "+nominator.getFullName()+",");
			sb.append("<br/><br/> "+ msg +" ");
			sb.append("<br/><br/>Greetings from the team of National Enterpreneurship Awards(NEA) 2018. ");
			sb.append("<br/><br/>Thank You,<br/>Team NEA 2018");
			mimeMessage.setContent(sb.toString(), "text/html; charset=utf-8");
			Transport.send(mimeMessage);
		} catch (MessagingException e) {
			LOGGER.error(e.getMessage());
		} catch (UnsupportedEncodingException e1) {
			LOGGER.error(e1.getMessage());
		}
		
	}
	
	@Override
	public void sendSingleAssignExpertMail(Evaluation eval) {
		try {
			if(eval.getExpert1() != null) {
				MimeMessage mimeMessage = new MailCommons().mimeMessage();
				mimeMessage.setFrom("contact-neas@nic.in");
				mimeMessage.setRecipient(Message.RecipientType.TO,new InternetAddress(eval.getExpert1(),"Team NEA 2018"));
				mimeMessage.setSubject("Team NEA 2018 :: "+ "New Nominee Assigned");
				StringBuilder sb=new StringBuilder();
				Expert expert = new Expert();
				expert = expertService.getExpertByEmail(eval.getExpert1());
				sb.append("Dear "+expert.getExpertName()+",");
				sb.append("<br/><br/> "+" A nominee is assigned to you, Kindly have a look to the details of nominee("+eval.getNomineeName()+") and evaluate.");
				sb.append("<br/><br/>Greetings from the team of National Enterpreneurship Awards(NEA) 2018. ");
				sb.append("<br/><br/>Thank You,<br/>Team NEA 2018");
				mimeMessage.setContent(sb.toString(), "text/html; charset=utf-8");
				Transport.send(mimeMessage);
			}
			if(eval.getExpert2() != null) {
				MimeMessage mimeMessage = new MailCommons().mimeMessage();
				mimeMessage.setFrom("contact-neas@nic.in");
				mimeMessage.setRecipient(Message.RecipientType.TO,new InternetAddress(eval.getExpert2(),"Team NEA 2018"));
				mimeMessage.setSubject("Team NEA 2018 :: "+ "New Nominee Assigned");
				StringBuilder sb=new StringBuilder();
				Expert expert = new Expert();
				expert = expertService.getExpertByEmail(eval.getExpert2());
				sb.append("Dear "+expert.getExpertName()+",");
				sb.append("<br/><br/> "+" A nominee is assigned to you, Kindly have a look to the details of nominee("+eval.getNomineeName()+") and evaluate.");
				sb.append("<br/><br/>Greetings from the team of National Enterpreneurship Awards(NEA) 2018. ");
				sb.append("<br/><br/>Thank You,<br/>Team NEA 2018");
				mimeMessage.setContent(sb.toString(), "text/html; charset=utf-8");
				Transport.send(mimeMessage);
			}
			if(eval.getExpert3() != null) {
				MimeMessage mimeMessage = new MailCommons().mimeMessage();
				mimeMessage.setFrom("contact-neas@nic.in");
				mimeMessage.setRecipient(Message.RecipientType.TO,new InternetAddress(eval.getExpert3(),"Team NEA 2018"));
				mimeMessage.setSubject("Team NEA 2018 :: "+ "New Nominee Assigned");
				StringBuilder sb=new StringBuilder();
				Expert expert = new Expert();
				expert = expertService.getExpertByEmail(eval.getExpert3());
				sb.append("Dear "+expert.getExpertName()+",");
				sb.append("<br/><br/> "+" A nominee is assigned to you, Kindly have a look to the details of nominee("+eval.getNomineeName()+") and evaluate.");
				sb.append("<br/><br/>Greetings from the team of National Enterpreneurship Awards(NEA) 2018. ");
				sb.append("<br/><br/>Thank You,<br/>Team NEA 2018");
				mimeMessage.setContent(sb.toString(), "text/html; charset=utf-8");
				Transport.send(mimeMessage);
			}
			
		} catch (MessagingException e) {
			LOGGER.error(e.getMessage());
		} catch (UnsupportedEncodingException e1) {
			LOGGER.error(e1.getMessage());
		}
	}

}
