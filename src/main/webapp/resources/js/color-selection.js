$(function() {
    $("#aembootstrap-gv-rmds-col-o").on("click", function() {
        $("body").colorChanger(customlist, $(this).attr("data-bg"), $(this).attr("data-fg"), false, "#" + $(this).attr("id"));
    });
    $("#aembootstrap-gv-rmds-col-t").on("click", function() {
        $("body").colorChanger(customlist, $(this).attr("data-bg"), $(this).attr("data-fg"), false, "#" + $(this).attr("id"));
    });
    $("#aembootstrap-gv-rmds-col-th").on("click", function() {
        $("body").colorChanger(customlist, $(this).attr("data-bg"), $(this).attr("data-fg"), true, "#" + $(this).attr("id"));
    });
    if (window.localStorage !== null) {
        var themeclass = window.localStorage.getItem("rajColorTheme");
        $("body").colorChanger(customlist, $(themeclass).attr("data-bg"), $(themeclass).attr("data-fg"));
    } else {
        console.log("Localstorage not available");
    }
    $('body').setFont(fontSelectors);
});
var customlist = {
    "main-column": {
        "fg": [".navbar-default", ".guideline", "#footer", "#header-wrap", ".logo a small span", ".logo a small", ".skiptocontent a", "#fontChoose .btn", "#sectoin1", ".heading-block h3", ".search-btn", ".footer-widgets-wrap a"],
        "bg": [".header-top", "footer", "header"]
    }
}
var fontSelectors = {
	    "minFontSize": 14,
	    "maxFontSize": 42,
	    "p": 16,
	    ".copyright": 16,
	    ".banner .bnr-hdg": 36,
	    ".innerpage .whitebg ol li":16,
	    ".innerpage .whitebg ul li":16,
	    ".footer-col2 li a":16,
	    ".link-bottom li a":16,
	    ".partner-inner h3":29,
	    ".winner-section h2":29,
	    ".winner-section h2":22
	};