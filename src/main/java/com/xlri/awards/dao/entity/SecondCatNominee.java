package com.xlri.awards.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="SecondCatNominee")
public class SecondCatNominee {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	
	private Long id;
	private String emailId;
	private String contact;
	private String enterpriseDate;
	private String businessFormation;
	@Column(length=1000)
	private String foundJson;
	@Column(length=1000)
	private String promotorJson;
	private String cooperSgh;
    private String cooperComittee;
    @Column(length=1000)
    private String bearerJson;
    private String moaFileUpload;
    private String aoaFileUpload;
    private String orgFileUpload;
    private boolean releventCertification;
    private String indFileUpload;
    @Column(length=1000)
    private String revenueJSON;
    private String formNumber;
    
    
    
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getEnterpriseDate() {
		return enterpriseDate;
	}
	public void setEnterpriseDate(String enterpriseDate) {
		this.enterpriseDate = enterpriseDate;
	}
	public String getBusinessFormation() {
		return businessFormation;
	}
	public void setBusinessFormation(String businessFormation) {
		this.businessFormation = businessFormation;
	}
	public String getFoundJson() {
		return foundJson;
	}
	public void setFoundJson(String foundJson) {
		this.foundJson = foundJson;
	}
	public String getPromotorJson() {
		return promotorJson;
	}
	public void setPromotorJson(String promotorJson) {
		this.promotorJson = promotorJson;
	}
	public String getCooperSgh() {
		return cooperSgh;
	}
	public void setCooperSgh(String cooperSgh) {
		this.cooperSgh = cooperSgh;
	}
	public String getCooperComittee() {
		return cooperComittee;
	}
	public void setCooperComittee(String cooperComittee) {
		this.cooperComittee = cooperComittee;
	}
	public String getBearerJson() {
		return bearerJson;
	}
	public void setBearerJson(String bearerJson) {
		this.bearerJson = bearerJson;
	}
	public String getOrgFileUpload() {
		return orgFileUpload;
	}
	public void setOrgFileUpload(String orgFileUpload) {
		this.orgFileUpload = orgFileUpload;
	}
	public boolean isReleventCertification() {
		return releventCertification;
	}
	public void setReleventCertification(boolean releventCertification) {
		this.releventCertification = releventCertification;
	}
	public String getIndFileUpload() {
		return indFileUpload;
	}
	public void setIndFileUpload(String indFileUpload) {
		this.indFileUpload = indFileUpload;
	}
	public String getRevenueJSON() {
		return revenueJSON;
	}
	public void setRevenueJSON(String revenueJSON) {
		this.revenueJSON = revenueJSON;
	}
	public String getFormNumber() {
		return formNumber;
	}
	public void setFormNumber(String formNumber) {
		this.formNumber = formNumber;
	}
	public String getMoaFileUpload() {
		return moaFileUpload;
	}
	public void setMoaFileUpload(String moaFileUpload) {
		this.moaFileUpload = moaFileUpload;
	}
	public String getAoaFileUpload() {
		return aoaFileUpload;
	}
	public void setAoaFileUpload(String aoaFileUpload) {
		this.aoaFileUpload = aoaFileUpload;
	}

}
