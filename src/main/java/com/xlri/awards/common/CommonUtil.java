package com.xlri.awards.common;

public class CommonUtil {

	private static int regiNum = 0;

	public static String generateRegistrationNumber(String state) {
		StringBuilder sb = new StringBuilder("NEAS"+state);
		sb.append(String.format("%04d", ++regiNum));
		return sb.toString();
	}
	
	public static String generateNomineePassword() {
		String generatedPass = EncryptionUtil.generateRandomString().substring(0, 7);
		return generatedPass;
	}

}
