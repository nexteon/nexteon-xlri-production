<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
   <%@include file="fragments/headerlibs.jsp" %>
   <body class="innerpage">
      <%@include file="fragments/header.jsp" %>
      <!-- Body Starts Here -->
      <main role="main">
         <div class="page-title">
            <h2>Contact Us</h2>
         </div>
         <div class="content">
            <div class="container">
               <div class="row">
                  <div class="col-md-12">
                     <div class="whitebg">
                        <div class="col-md-6 line">
                        	<h3>Get in touch with us</h3>
                
                
                
                <div class="contactForm">
                    <form action="/neas/api/mail/sendcontact" method="post" data-toggle="validator" role="form" id="contact-form">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="hidden" for="your_name">Your Name<span class="mandate">*</span></label>
                                    <input type="text" name="fullName" id="your_name" class="form-control" placeholder="Your Name" required autocomplete="off">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="hidden" for="your_email">Your Email<span class="mandate">*</span></label>
                                    <input type="email" name="emailId" id="your_email" class="form-control" placeholder="Your Email" required autocomplete="off"  pattern="^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$" data-pattern-error="Please provide a valid email id.">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="hidden" for="subject">Subject<span class="mandate">*</span></label>
                                    <input type="text" name="subject" id="subject" class="form-control" placeholder="Subject" required autocomplete="off">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="hidden" for="message">Message</label>
                                    <textarea id="message" name="message" class="form-control" placeholder="Message" onblur="check(this);"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
	                            <div class="form-group">
									<div id="captchaBlock">
									        <div id="resource1" class="captchaWrap">
									            <div class="c-img">
									                <div class="captchaContainer"></div>
									            </div>
									            <div class="captcha-input">
									                <input type="hidden" name="enc">
									                <input type="hidden" name="salt">
									                <input type="text" name="code" class="form-control" placeholder="Enter Captcha">
									                <span id="captchaMessage"></span>
									                <input type="hidden" name="redirect" value="/signup">
									            </div>
									            <div class="refresh-wrap">
									                <span id="searchButton"><i class="fa fa-refresh" aria-hidden="true"></i></span>
									            </div>
									        </div>
									   </div>
								</div>
							</div>
							<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
							<input type="hidden" name="currentpath" value="/neas/contact"/>
                            <div class="col-md-12">
                                <div class="submit-btn">
                                    <input class="btn btn-primary" type="submit" name="Submit" value="Submit">
                                </div>
                            </div>
                        </div> 
                    </form>
                </div>
                <c:if test="${not empty successMsg }">
				  <div class="alert alert-success">
				  ${successMsg }
				</div>
                </c:if>
                <c:if test="${not empty errorMsg }">
				  <div class="alert alert-danger">
				  ${errorMsg }
				</div>
                </c:if>
            </div>
            <div class="col-md-offset-1 col-md-5">
                <div class="clearfix"></div>
                <div class="contact-info">
                    
                    <h3>Contact information</h3>
                    <ul>
                        <li>
                            <label>Address :</label>
                            <strong>Xavier School of Management</strong><br>
                            Rivers Meet Road, Circuit House Area<br>
                            East, Jamshedpur<br>
                            Jharkhand - 831001<br>
                        </li>                       
                    </ul>
                    <h3>Helpline Number</h3>
                    <ul>
                        <li>
                            <label>Name: </label>
                            Ms. Richa
                        </li>
                        <li>
                            <label>Number: </label>
                            +91-9717211060
                        </li>
                        <li>
                            <label>Email Address: </label>
                            help.nea@ettech.com
                        </li>
                    </ul>
                </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </main>
      <!-- Body Ends Here -->
      <%@include file="fragments/footer.jsp" %>
      <script>
    $( document ).ready(function() {
        getCaptcha();
         validateCaptcha();
        $("#searchButton").click(function(){
            console.log('inside call');
            getCaptcha();
        });


    });

    function getCaptcha(){
        var html = "<h2>Loading Captcha</h2>"

        console.log("/bin/captcha");
        var url = "/bin/captcha"; 

        console.log(url);

        $.ajax({
            url: url, 
            type: 'get',
            success: function (result) {
                html = "";
                var captchaImg = "", captcha="",salt="";
                if(result !== undefined || result !== null){
                    captchaImg  = result.image;
                    captcha = result.captcha;
                    salt = result.salt;
                     $('#resource1').find('.captchaContainer').html("<img id='captchaImage' src='data:image/png;base64,"+ captchaImg+"'></img>");

                    $('#resource1').find('[name="enc"]').val(captcha);
                    $('#resource1').find('[name="salt"]').val(salt);
                }


            },
            error: function() {
                console.log("Error while creating captcha");
            }
        });


    }

   function validateCaptcha() {
        var isValid = false;
        $('#contact-form').find('[type="submit"]').on('click', function(event) {
            if(event.hasOwnProperty('originalEvent')){
            $('#captchaMessage').html("");
            event.preventDefault();
                if($("[name='code']").val() != ""){
                var salt = $('[name="salt"]').val();
                var code = $('[name="code"]').val();
                var enc = $('[name="enc"]').val();

                $.ajax({
                    url: '/bin/validateCaptcha',
                    type: 'post',
                    data: $('#contact-form').find('[name="salt"], [name="code"], [name="enc"], [name="${_csrf.parameterName}"]').serialize(),    
                    success: function(data){
                        console.log("Inside success");
                        console.log(data.valid);
                        if(!data.valid){
                            $("#captchaMessage").html("You entered wrong captcha code, please enter again");
                            return false;

                        }
                        else{
                            console.log("now submit");
                            isValid = true;
                            //$('#' + "${formId}").submit();
                              $('#contact-form').find('[type="submit"]').trigger('click');
                        }
                        return false;
                    },
                    error: function() {
                        console.log("An error occured while validating captcha");
                    },
                    complete: function(){
                        console.log("ajax completed ");
                    }
                });

            }

            else{

                $("#captchaMessage").html("Please enter captcha code");

                return false;
            }
        }
        });

       return false;
    }
</script>
          
          <script>
          $("#message,#subject").keyup(function(){
 check_val();
});
           
function check_val()
{
 var bad_words=new Array("word","anal","anus","arse","ass","ass fuck","ass hole","assfucker","asshole","assshole","bastard","bitch","black cock","bloody hell","boong","cock","cockfucker","cocksuck","cocksucker","coon","coonnass","crap","cunt","cyberfuck","damn","darn","dick","dirty","douche","dummy","erect","erection","erotic","escort","fag","faggot","fuck","Fuck off","fuck you","fuckass","fuckhole","god damn","gook","hard core","hardcore","homoerotic","hore","lesbian","lesbians","mother fucker","motherfuck","motherfucker","negro","nigger","orgasim","orgasm","penis","penisfucker","piss","piss off","porn","porno","pornography","pussy","retard","sadist","sex","sexy","shit","slut","son of a bitch","suck","tits","viagra","whore","xxx","viagina","penetration");
 var check_text=document.getElementById("message").value;
    var check_text2=document.getElementById("subject").value;
 var error=0;
 for(var i=0;i<bad_words.length;i++)
 {
  var val=bad_words[i];
  if((check_text.toLowerCase()).indexOf(val.toString())>-1 || (check_text2.toLowerCase()).indexOf(val.toString())>-1)
  {
   error=error+1;
  }
 }
	
 if(error>0)
 {
     $('#message').val('');
     $('#subject').val(''); 
 } 
}
          </script>
      
   </body>
</html>