<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">
   <%@include file="dashboard/headerlibs.jsp" %>
   <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white" id="page-top" ng-app="myApp" ng-controller="jsonCtrl">
   			<div class="page-wrapper">
          <%@include file="dashboard/header.jsp" %>
           


                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="/userdata">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>NewsSection</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> News Section</h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <!-- BEGIN DASHBOARD STATS 1-->

        
		<!-- Form Container -->
		 <div class="container">
		      <div class="card mx-auto mt-12">
		        <div class="card-header">News Details</div>
		        <div class="card-body">
		          <form action="/newssectionpost" method="post" autocomplete="off" >
		            <div class="form-group">
		              <div class="form-label-group">
		                <input type="text" id="title" class="form-control" placeholder="Title" required="required" autofocus="autofocus" name="title">
		                <label for="title">Title</label>
		              </div>
		            </div>
		            <div class="form-group">
		              <div class="form-label-group">
		                <input type="text" id="subcategory" class="form-control" placeholder="Sub Category" required="required" name="subcategory">
		                <label for="subcategory">Sub Category</label>
		              </div>
		            </div>
		            
		              
		              <div class="form-group">
		                  <label for="description">Description</label>
		               <textarea class="form-control" id="description" name="description"></textarea>
		               <input type="hidden" name="currentpath" value=""/>           
		               <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		              </div>		              
		           <input type="submit" class="btn btn-primary"  value="Submit">
		          </form>
		          
		        </div>
		      </div>
		    </div>
		    </div>
    
         </div>
         </div>
         
     <c:if test="${not empty param.invalidinput}">
     	<div class="modal fade" id="errorModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>

                                <h4 class="modal-title">Error</h4>

                        </div>
                        <div class="modal-body">
                            <p>
								Some values Entered were incorrect. Please check that values do not contain &lt; and &gt; or any other invalid content.
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    
                </div>
            </div>
     </c:if>
         
        <!-- END QUICK NAV -->
     <%@include file="dashboard/footer.jsp"%>
   
     <c:if test="${not empty param.invalidinput}">
     	<script>
     	 $(document).ready(function() {
             if($('#errorModal').length > 0){
                 $('#errorModal').modal('show');
             }
         });
     	</script>
     </c:if>
     </body>
         </html>
                    
