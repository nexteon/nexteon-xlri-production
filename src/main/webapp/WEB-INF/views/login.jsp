<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <%@include file="fragments/headerlibs.jsp" %>
    <body class="innerpage">
        
        <%@include file="fragments/header.jsp" %>
        <!-- Body Starts Here -->
        
        <main role="main">
            <div class="page-title">
                <h2>Please Login Here</h2>
            </div>
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="whitebg">
                                <div class="form-body">
                                    <form action="/login" method="POST" data-toggle="validator" role="form" id="login-form">
                                        <div class="clearfix"></div>
                                        <c:if test="${not empty param.error}">
                                            <div class="col-md-12">
                                                <div class="alert alert-danger">
                                                    Invalid Username or Password!
                                                    NOTE: Nomination has been closed for this event
                                                </div>
                                            </div>
                                        </c:if>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="email">Email Id</label>
                                                <input type="email" name="emailId" id="email" class="form-control" placeholder="Email ID" required autocomplete="off"  pattern="^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$" data-pattern-error="Please provide a valid email id.">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="password">Password</label>
                                                <input type="password" name="password" id="password" class="form-control" placeholder="Password" required autocomplete="off">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <c:if test="${not empty param.error}">
                                         	<div class="col-md-12">
					                            <div class="form-group">
													<div id="captchaBlock">
													        <div id="resource1" class="captchaWrap">
													            <div class="c-img">
													                <div class="captchaContainer"></div>
													            </div>
													            <div class="captcha-input">
													                <input type="hidden" name="enc">
													                <input type="hidden" name="salt">
													                <input type="text" name="code" class="form-control" placeholder="Enter Captcha">
													                <span id="captchaMessage"></span>
													                <input type="hidden" name="redirect" value="/login">
													            </div>
													            <div class="refresh-wrap">
													                <span id="searchButton"><i class="fa fa-refresh" aria-hidden="true"></i></span>
													            </div>
													        </div>
													   </div>
												</div>
											</div>
                                        </c:if>
                                        <input type="hidden" name="currentpath" value=""/>
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                        <div class="clearfix"></div>
                                        <div class="col-md-12">
                                            <div class="btn-group">
                                               
                                                    <input class="btn btn-primary" type="submit" name="Submit" value="Submit">
                                                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#openForget">Forgot Password</button>
                                                </div>
                                         </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        
        
        <div class="modal fade" id="openForget" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="/forgetpassword" method="POST" name="forgot-form">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Forgot Password</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="email1">Email Id</label>
                                <input type="email" name="emailId" id="email1" class="form-control" placeholder="Email ID" required  pattern="^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$" data-pattern-error="Please provide a valid email id.">
                            </div>
                        </div>
                        <div class="modal-footer">
                        <input type="hidden" name="currentpath" value=""/>
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                            <input type="submit" class="btn btn-primary" value="Proceed"/>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
                
            </div>
        </div>
        
        <c:if test="${(not empty errorMsg) or (not empty successMsg)}">
            <div class="modal fade" id="errorModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <c:if test="${ not empty errorMsg }">
                                <h4 class="modal-title">Error</h4>
                            </c:if>
                            <c:if test="${ not empty successMsg }">
                                <h4 class="modal-title">Success</h4>
                            </c:if>
                        </div>
                        <div class="modal-body">
                            <p>
                                <c:if test="${ not empty errorMsg }">
                                    ${ errorMsg }
                                </c:if>
                                <c:if test="${ not empty successMsg }">
                                    ${ successMsg }
                                </c:if>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    
                </div>
            </div>
        </c:if>
        <!-- Body Ends Here -->
        <%@include file="fragments/footer.jsp" %>
        
        <script>
            $(document).ready(function() {
                if($('#errorModal').length > 0){
                    $('#errorModal').modal('show');
                }
            });
        <c:if test="${not empty param.error}">
    $( document ).ready(function() {
        getCaptcha();
         validateCaptcha();
        $("#searchButton").click(function(){
            console.log('inside call');
            getCaptcha();
        });


    });

    function getCaptcha(){
        var html = "<h2>Loading Captcha</h2>"

        console.log("/bin/captcha");
        var url = "/bin/captcha"; 

        console.log(url);

        $.ajax({
            url: url, 
            type: 'get',
            success: function (result) {
                html = "";
                var captchaImg = "", captcha="",salt="";
                if(result !== undefined || result !== null){
                    captchaImg  = result.image;
                    captcha = result.captcha;
                    salt = result.salt;
                     $('#resource1').find('.captchaContainer').html("<img id='captchaImage' src='data:image/png;base64,"+ captchaImg+"'></img>");

                    $('#resource1').find('[name="enc"]').val(captcha);
                    $('#resource1').find('[name="salt"]').val(salt);
                }


            },
            error: function() {
                console.log("Error while creating captcha");
            }
        });


    }

   function validateCaptcha() {
        var isValid = false;
        $('#login-form').find('[type="submit"]').on('click', function(event) {
            if(event.hasOwnProperty('originalEvent')){
            $('#captchaMessage').html("");
            event.preventDefault();
                if($("[name='code']").val() != ""){
                var salt = $('[name="salt"]').val();
                var code = $('[name="code"]').val();
                var enc = $('[name="enc"]').val();

                $.ajax({
                    url: '/bin/validateCaptcha',
                    type: 'post',
                    data: $('#login-form').find('[name="salt"], [name="code"], [name="enc"], [name="${_csrf.parameterName}"]').serialize(),    
                    success: function(data){
                        console.log("Inside success");
                        console.log(data.valid);
                        if(!data.valid){
                            $("#captchaMessage").html("You entered wrong captcha code, please enter again");
                            return false;

                        }
                        else{
                            console.log("now submit");
                            isValid = true;
                            //$('#' + "${formId}").submit();
                              $('#login-form').find('[type="submit"]').trigger('click');
                        }
                        return false;
                    },
                    error: function() {
                        console.log("An error occured while validating captcha");
                    },
                    complete: function(){
                        console.log("ajax completed ");
                    }
                });

            }

            else{

                $("#captchaMessage").html("Please enter captcha code");

                return false;
            }
        }
        });

       return false;
    }
   </c:if>
</script>
        
    </body>
</html>