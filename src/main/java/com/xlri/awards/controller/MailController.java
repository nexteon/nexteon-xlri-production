package com.xlri.awards.controller;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.xlri.awards.dao.entity.User;
import com.xlri.awards.dao.service.api.MailService;
import com.xlri.awards.dao.service.api.NewsService;
import com.xlri.awards.dao.service.api.NominatorService;
import com.xlri.awards.dao.service.api.NomineeService;
import com.xlri.awards.dao.service.api.UserService;

/**
 * @author AsharamVerma Nexteon
 * Send mails to roles specific users
 */

@Controller
public class MailController {
	
	@Autowired
	NominatorService nominatorService;

	@Autowired
	UserService userService;

	@Autowired
	MailService mailService;

	@Autowired
	NewsService newsService;

	@Autowired
	NomineeService nomineeService;
	
	
/** added by asharamverma for mail section  */
	
	@RequestMapping(value = "/mailsection", method = RequestMethod.GET)
	public ModelAndView getMailSection(HttpServletRequest request) {
		ModelAndView mView = new ModelAndView("mailsection");
		
		
		List<User> list = userService.getAllUsers();
		Set set = new HashSet();
		Iterator<User> itr = list.iterator();
		while(itr.hasNext()){
			set.add(itr.next().getRole());
		}
		mView.addObject("roles", set);
		return mView;
	}
	
	
	@RequestMapping(value = "/neas/api/email/{role}", method = RequestMethod.GET)
	public void getEmail(HttpServletRequest request, HttpServletResponse response,@PathVariable("role") String role) throws JSONException, IOException {
		//ModelAndView mView = new ModelAndView("mailsection");
		
		if(role != null && role.equals("all")){
			List<User> list = userService.getAllUsers();
			Iterator<User> itr = list.iterator();
			JSONObject jsonObj = new JSONObject();
			while(itr.hasNext()){
				jsonObj.put(itr.next().getUsername(), itr.next().getFullName());
			}
			
			System.out.println("Result : "+jsonObj.toString());
			response.getWriter().println(jsonObj.toString());
		}else{
			
			List<User> list = userService.getUserByRole(role);
			Iterator<User> itr = list.iterator();
			JSONObject jsonObj = new JSONObject();
			while(itr.hasNext()){
				jsonObj.put(itr.next().getUsername(), itr.next().getFullName());
			}
			
			System.out.println("Result : "+jsonObj.toString());
			response.getWriter().println(jsonObj.toString());
		}
		
	}
	
	@RequestMapping(value = "/mailsection", method = RequestMethod.POST)
	public void sendEmail(HttpServletRequest request, HttpServletResponse response) throws JSONException, IOException {
		//ModelAndView mView = new ModelAndView("mailsection");
		
		//String email
		/*List<User> list = userService.getAllUsers();
		Iterator<User> itr = list.iterator();
		JSONObject jsonObj = new JSONObject();
		while(itr.hasNext()){
			jsonObj.put(itr.next().getUsername(), itr.next().getFullName());
		}
		
		System.out.println("Result : "+jsonObj.toString());
		response.getWriter().println(jsonObj.toString());*/
		
	}
}
