<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">
   <%@include file="dashboard/headerlibs.jsp" %>
   <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white" id="page-top" >
   			<div class="page-wrapper">
          <%@include file="dashboard/header.jsp" %>
           


                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="/userdata">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Dashboard</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title">Dashboard</h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <!-- BEGIN DASHBOARD STATS 1-->
                        <div class="row" >
                        			<!--Start,  Following div is only used for Regional Dashbaord  -->
                        	<c:if test ="${isRegional}">
							<div class = "row" ng-app="myApp" ng-controller="jsonCtrl">
                        	 <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 blue type-table1"  href="javascript:;">
                                    <div class="visual">
                                        <i class="fa fa-comments"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" data-value="1349" id="nominator-count"></span>
                                        </div>
                                        <div class="desc">Nominator Registration</div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 red type-table2"  href="javascript:;">
                                    <div class="visual">
                                        <i class="fa fa-bar-chart-o"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" data-value="12,5" id="nominee-count"></span></div>
                                        <div class="desc">Nominations Received</div>
                                    </div>
                                </a>
                            </div>
							</div>
                            </c:if>
                            <!--END,  Above div is only used for Regional Dashbaord  -->
                            
                        <c:if test ="${isAdmin}">
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 blue type-table1"  href="javascript:;">
                                    <div class="visual">
                                        <i class="fa fa-comments"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" data-value="1349" id="nominator-count"></span>
                                        </div>
                                        <div class="desc">Nominator Registration</div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 red type-table2"  href="javascript:;">
                                    <div class="visual">
                                        <i class="fa fa-bar-chart-o"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" data-value="12,5" id="nominee-count"></span></div>
                                        <div class="desc">Nominations Received</div>
                                    </div>
                                </a>
                            </div>
                            
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 green" href="#">
                                    <div class="visual">
                                        <i class="fa fa-shopping-cart"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" data-value="549"></span>
                                        </div>
                                          <div class="mr-5">
                              <div class="desc">
                                 <p>GEN:</p>
                                 <span id="category-count"></span>
                              </div>
                              <div class="desc">
                                 <p>SC:</p>
                                 <span id="category-sccount"></span>
                              </div>
                              <div class="desc">
                                 <p>ST:</p>
                                 <span id="category-stcount"></span>
                              </div>
                              <div class="desc">
                                 <p>PWD:</p>
                                 <span id="pwd-count"></span>
                              </div>
                           </div>
                                        <div class="desc"> Category Wise count </div>
                                        
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 green" href="#">
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" data-value="549"></span>
                                        </div>
                                          <div class="mr-5">
                              <div class="desc">
                                 <p>Women:</p>
                                 <span id="spec-women"></span>
                              </div>
                              <div class="desc">
                                 <p>SC/ST:</p>
                                 <span id="spec-scst"></span>
                              </div>
                              <div class="desc">
                                 <p>Difficult:</p>
                                 <span id="spec-diff"></span>
                              </div>
                              <div class="desc">
                                 <p>PWD:</p>
                                 <span id="spec-pwd"></span>
                              </div>
                           </div>
                                        <div class="desc"> Special Category count </div>
                                        
                                    </div>
                                </a>
                            </div>
                            </c:if>
                            
                            <c:if test ="${isSupport}">
                            
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 red type-table2"  href="javascript:;">
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" data-value="12,5" id="nominee-count"></span>
                                        </div>
                                        <div class="desc">Nominations Received</div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 blue type-table1"  href="javascript:void(0);">
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" data-value="1349" id="nominator-count"></span>
                                        </div>
                                        <div class="desc">Nominator Registration</div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12" id="accepted-count">
                                <a class="dashboard-stat dashboard-stat-v2 red"  href="#">
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" data-value="1349" id="accept-nominee"></span>
                                        </div>
                                        <div class="desc"> Nominees Accepted by Support Agency</div>
                                    </div>
                                </a>
                            </div>
                            
                            
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 green" id="pending-count" href="javascript:;">
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" id="pending-nominee" data-value="549"></span>
                                        </div>
                                        <div class="desc"> Nominees Pending By Support Agency </div>

                                    </div>
                                </a>
                            </div>
                            
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 purple" id="decline-count" href="javascript:;">
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" id="decline-nominee" data-value="89"></span></div>
                                        <div class="desc">Nominees Declined By Support Agency</div>
                                    </div>
                                </a>
                            </div>
                            </c:if>
                           
                        </div>
                        <div class="clearfix"></div>
                        <!-- END DASHBOARD STATS 1-->
                        
                        <input type="hidden" name="subCategory" value=""/>
                        <c:if test ="${isAdmin || isSupport ||isRegional}">
                          <div class="row datatable">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="icon-settings font-dark"></i>
                                            <span class="caption-subject bold uppercase"> Nominator Details</span>
                                        </div>
                                        <a href="#" id="refresh-page" class="btn btn-info">Reset Filters</a>
                                        <c:if test ="${isAdmin || isSupport}">
                                           <a href="/neas/api/createexcel/nominator" class="btn btn-primary downloadbtn pull-right">Download Nominator Excel</a>
                                        </c:if>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-responsive">
                                        <div class="total_record">Total Count:
                    <div id="filter-count1"></div>
                </div>
                                         <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable nominator-reg-table" id="example" class="display">
                                            <thead>
                         <tr>
                         <th>ID</th>
                         <th>Registration Number</th>
                         <th>View</th>
                          <th>Name</th>
                          <!-- <th>Role</th> -->
                          <th>Email Id</th>
                          <th>Contact Number</th>
                          
                   
                     
                            <th>City</th>
                            <th>State</th>
                          
                       </tr>
                     </thead>
                                            
                                            <tbody>
                     	<!-- <tr ng-repeat="data in tableData">
               <td>{{ data.id }}</td>
               <td>{{ data.registrationNumber }}</td>
               <td>{{ data.fullName}}</td>
               <td>{{ data.instituteName}}</td>
               <td>{{ data.emailId }}</td>
               <td>{{ data.contactNumber}}</td>
               <td>{{ data.occupation}}</td>
               
               <td>{{ data.addrLine1}} {{ data.addrLine2}}</td>
               <td>{{ data.addrCity}}</td>
               <td>{{ data.addrState}}</td>
               </tr> -->
               
               </tbody>
               
               <tfoot>
               		<tr id="filters">
                         <th>ID</th>
                         <th>Registration Number</th>
                         <th></th>
                          <th>Name</th>
                          <!-- <th>Role</th> -->
                          <th>Email Id</th>
                          <th>Contact Number</th>
                          
                            <th>City</th>
                            <th>State</th>
                          
                       </tr>
                     
                       
               </tfoot>
             
                                             </table></div>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>
                        </c:if>
                        
                        
                         <!-- this is our second table -->
                     <!--   <input type="hidden" name="subCategory" value=""/> -->
                         <c:if test ="${isAdmin || isSupport  ||isRegional}">
                                   <div class="row datatable2">
                            <div class="col-md-12">
                             <!--     BEGIN EXAMPLE TABLE PORTLET -->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="icon-settings font-dark"></i>
                                            <span class="caption-subject bold uppercase"> Nominee Details</span>
                                        </div>
                                        <a href="#" id="refresh-page1" class="btn btn-info">Reset Filters</a>
                                        <c:if test ="${isAdmin || isSupport}">
                                           <a href="/neas/api/createexcel/nominee" class="btn btn-primary downloadbtn pull-right">Download Nominee Excel</a>
                                        </c:if>
                                    </div>
                                    <div class="portlet-body">
                                         <div class="table-responsive">
                                        <div class="total_record2">Total Count:
                    <div id="filter-count2"></div>
                </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable nominations-rec-table" id="example1" class="display">
                                            <thead>
                           <tr>
                              <th>ID</th>
                              <th>Registration Number</th>
                              <th>View</th>
                              <th>Nominee Name</th>
                              <th>Email Id</th>
                              <th>Contact Number</th>
                              <th>Award Track</th>
                              <th>Award Category</th>   
                              <th>State</th>
                              <th>Social Category</th>                              
                              <th>Status</th>
                              <th>File Uploaded</th>                     
                           </tr>
                        </thead>
                        <tfoot>
                                <tr id="filters2">
		                              <th>ID</th>
		                              <th>Registration Number</th>
		                              <th></th>
		                              <th>Nominee Name</th>
		                              <th>Email Id</th>
		                              <th>Contact Number</th>
		                              <th>Award Track</th>
		                              <th>Award Category</th>                             
		                              <th>State</th>
									  <th>Social Category</th>
									  <th>Status</th>
									  <th>File Uploaded</th>
                                 </tr>
                              </tfoot>
                        
                       <tbody>
                       </tbody>
                         
                         	                                              </table>
                                    </div>
                                 </div> 
                               <!--  END EXAMPLE TABLE PORTLET -->
                            </div>
                        </div> 
                        </div> 
                        </c:if>
                        </div>
                        
                        
                        
                       
                        
                   
                    <!-- END CONTENT BODY -->
                
                <!-- END CONTENT -->
               
         
            <!-- END CONTAINER -->
            
 
        <!-- BEGIN QUICK NAV -->
       
           </div>
           
         
        <!-- END QUICK NAV -->
     <%@include file="dashboard/footer.jsp"%>
     <%@include file="dashboard/datatablescript.jsp" %>
      
      
      <!-- this is ajax call to get value of nominator data from dashboard controller -->
      
      <c:if test ="${isAdmin || isSupport}">
      <script type="text/javascript">
		        
	        $('#example').DataTable( {
	      	  "ajax": {
	      	  "url": "/neas/api/nominatordata",
	      	  "type" : 'POST',
	      	"scrollX": true,
	      	  "dataSrc": ""
	      	  },
	      	"stateSave": true,
	      	  "columns": [
	      	  { "data": "id" },
	      	  { "data": "registrationnumber" },
	      	{ "data": "view" },
	      	  { "data": "fullname" },
	      	  /* { "data": "role" }, */
	      	  { "data": "emailid" },
	      	  { "data": "contactnumber" },
	      	  /*{ "data": "occupation" },
	      	  { "data": "institutename" },
	      	  { "data": "addrline1" },*/
	      	  { "data": "addrcity" }, 
	      	  { "data": "addrstate" },
	      	  ],dom: 'Bfrtip',
	          buttons: [
	                    'copyHtml5',
	                    'excelHtml5',
	                    'csvHtml5',
	                    'pdfHtml5'
	                ], initComplete: function () {
	      	  this.api().columns().every(function () {
	      	  var column = this;

	      	 
	      	  var select = $('<select><option value=""></option></select>')
	      	  .appendTo($("#filters").find("th").eq(column.index()))
	      	  .on('change', function () {
	      	  var val = $.fn.dataTable.util.escapeRegex(
	      	  $(this).val()); 

	      	  column.search(val ? '^' + val + '$' : '', true, false)
	      	  .draw();
	      	  }); 
	      	  //console.log(select);
	      	  column.data().unique().sort().each(function (d, j) {
	      	  select.append('<option value="' + d + '">' + d + '</option>')
	      	  });
	      	  });
	      	  }
	      	  } );
	 </script>
	 </c:if>

<!-- this is ajax call to get value of nominee data from dashboard controller -->
	 <c:if test ="${isAdmin || isSupport}">
	  <script type="text/javascript" >
	 
	var datatable= $('#example1').DataTable( {
     	  "ajax": {
     	  "url": "/neas/api/nomineedata",
     	  "type" : 'POST',
     	"scrollX": true,
     	  "dataSrc": ""
     	  },
     	 "stateSave": true,
     	  "columns": [
     	  { "data": "id" },
     	  { "data": "registrationnumber" },
     	  { "data": "view" },
     	  { "data": "nomineename" },
     	  { "data": "emailid" },
     	  { "data": "contactnumber" },
     	  { "data": "awardcategory" },
     	  { "data": "enterprisecategory" },
     	 /* { "data": "awardsub" },
     	  { "data": "gender" },
     	  { "data": "socialcategory" },
     	  { "data": "dob" },
     	  { "data": "speccatval" },
    	  { "data": "enterprisename" },
    	  { "data": "address1" },
    	  { "data": "address2" },
    	  { "data": "city" },*/
    	  { "data": "state" },
    	  { "data": "socialcategory" },
    	  /*{ "data": "createdon"}, */
    	  { "data": "status" },
    	  { "data": "fileuploaded" },
     	  ],dom: 'Bfrtip',
         buttons: [
                   'copyHtml5',
                   'excelHtml5',
                   'csvHtml5',
                   'pdfHtml5'
               ], initComplete: function () {
     	  this.api().columns().every(function () {
     	  var column = this;

     	 
     	  var select = $('<select><option value=""></option></select>')
     	  .appendTo($("#filters2").find("th").eq(column.index()))
     	  .on('change', function () {
     	  var val = $.fn.dataTable.util.escapeRegex(
     	  $(this).val()); 

     	  column.search(val ? '^' + val + '$' : '', true, false)
     	  .draw();
     	  }); 
     	  //console.log(select);
     	  column.data().unique().sort().each(function (d, j) {
     	  select.append('<option value="' + d + '">' + d + '</option>')
     	  });
     	  });
     	  }
     	  } );
	 
	
	 </script>
	 </c:if>
     
    
    <script>
            /* var app = angular.module('myApp', []);
            app.controller('jsonCtrl', function($scope, $http) {
                $http({url: "/neas/api/nominatordata", method: 'POST',async: false })
                 .then(function(response) {
                	$scope.tableData = response.data;
                	 $('#sample_1_2').DataTable( {
                        initComplete: function () {
                            this.api().columns().every( function () {
                                var column = this;
                                var select = $('<select><option value=""></option></select>')
                                    .appendTo( $(column.footer()).empty() )
                                    .on( 'change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                        );
                 
                                        column
                                            .search( val ? '^'+val+'$' : '', true, false )
                                            .draw();
                                    } );
                 
                                column.data().unique().sort().each( function ( d, j ) {
                                    select.append( '<option value="'+d+'">'+d+'</option>' )
                                } );
                            } );
                        }
                    } );
                }); */
                
// angular datatable
//==================================== 
/* var app = angular.module('myApp', []);
app.controller('jsonCtrl', function($scope, $http) {

  angular.element(document).ready(function() {
    // Setup - add a text input to each footer cell

    
    
    $scope.data = $http({url: "/neas/api/nominatordata", method: 'POST'})
    .then(function(response) {
   	$scope.tableData = response.data;
   });


$scope.dataTableOpt = {
//custom datatable options 
// or load data through ajax call also
// this is not real binding, the real binding is ui-jq="dataTable" ui-options="dataTableOpt", fill $scope.data
"retrieve": true, // angularjs at begining initialize datatable, but don't get a handle to the table, later you want to add search column, you need to get the table handle.
"aLengthMenu": [
[10, 50, 100, -1],
[10, 50, 100, 'All']
],
};

$('#sample_1_2 tfoot th').each(function() {
    var title = $(this).text();
    $(this).html('<input type="text" placeholder="Search ' + title + '" />');
  });

  console.log('  document ready function, add search by column feature ');

    var table = $('#sample_1_2').DataTable();
     console.log("TBLE--- "+ table);
     console.log("TBLE colmn--- "+ table.columns().data());
     console.log("TBLE row--- "+ table.rows().data());
     
    // Apply the search
    table.columns().every(function() {
      var that = this;

      $('input', this.footer()).on('keyup change', function() {
        if (that.search() !== this.value) {
          that
            .search(this.value)
            .draw();
        }
      });
    });

  }); // document ready				  



                
                $http.post("/neas/api/nomineedata")
                .then(function(response) {
                	$scope.tableDataNew = response.data;
                }); 
                            
            });
 */         </script>
 
 
 
 <c:if test ="${isRegional}">
	  <script type="text/javascript" >
	  
	 $('#example').DataTable( {
     	  "ajax": {
     	  "url": "/neas/api/regionaltabledata",
     	  "type" : 'POST',
     	"scrollX": true,
     	  "dataSrc": ""
     	  },
     	  "columns": [
     	  { "data": "id" },
     	  { "data": "registrationnumber" },
     	  { "data": "nomineename" },
     	  { "data": "emailid" },
     	  { "data": "contactnumber" },
     	  { "data": "awardcategory" },
     	  { "data": "enterprisecategory" },
     	 /* { "data": "awardsub" },
     	  { "data": "gender" },
     	  { "data": "socialcategory" },
     	  { "data": "dob" },
     	  { "data": "speccatval" },
    	  { "data": "enterprisename" },
    	  { "data": "address1" },
    	  { "data": "address2" },
    	  { "data": "city" },*/
    	  { "data": "state" },<i class="fas fa-th-list"></i>
     	  ],dom: 'Bfrtip',
         buttons: [
                   'copyHtml5',
                   'excelHtml5',
                   'csvHtml5',
                   'pdfHtml5'
               ], initComplete: function () {
     	  this.api().columns().every(function () {
     	  var column = this;

     	 
     	  var select = $('<select><option value=""></option></select>')
     	  .appendTo($("#filters").find("th").eq(column.index()))
     	  .on('change', function () {
     	  var val = $.fn.dataTable.util.escapeRegex(
     	  $(this).val()); 

     	  column.search(val ? '^' + val + '$' : '', true, false)
     	  .draw();
     	  }); 
     	  console.log(select);
     	  column.data().unique().sort().each(function (d, j) {
     	  select.append('<option value="' + d + '">' + d + '</option>')
     	  });
     	  });
     	  }
     	  
	  });
	 
	 </script>
	 </c:if>
 
         
         <!-- this is ajax call to get total number of nominators data from dashboard controller -->
      <c:if test ="${isAdmin || isSupport}">
         <script type="text/javascript">
            $(document).ready(function() {
            	
            	 var url = "/neas/api/nominator";
            	 console.log(url);
            	 $.ajax({
            		 url:url,
            		 type: 'POST',
            		 
            		success: function (nomresult){
            						
            			var parsed = jQuery.parseJSON(nomresult);
            			console.log(parsed.count)
            			$("#nominator-count").html();
            	      	document.getElementById('nominator-count').innerHTML = parsed.count;
            			/*console.log("stringfy"+ JSON.stringify(nomresult))
            			$("#nominator-count").html();
            			    ('#nominator-count').html(nomresult['count']);
            			     console.log("nomnommm--- "+ nomresult['count']);
            			      document.getElementById('nominator-count').innerHTML = nomresult;
            			     console.log("nomnom--- "+ nomresult); 
            			 */      
            			     
            		} 
            	 	 
            	 });
            
            	 
            });
            
            
         </script>
      </c:if>
      
       <!-- this is ajax call to get total number of nominees data from dashboard controller -->
      
      <c:if test ="${isAdmin || isSupport}">
         <script type="text/javascript">
            $(document).ready(function() {
            	
            	 var url = "/neas/api/nominee";
            	 console.log(url);
            	 $.ajax({
            		 url:url,
            		 type: 'POST',
            		 
            		success: function (nomineeresult){
            						
            			var parsed = jQuery.parseJSON(nomineeresult);
            			console.log(parsed.count)
            			$("#nominee-count").html();
            	      	document.getElementById('nominee-count').innerHTML = parsed.count;
            	      	
            				     
            		} 
            	 	 
            	 });
            });
            
         </script>
      </c:if>
      <c:if test ="${isSupport}">
      <script type="text/javascript">
            $(document).ready(function() {
            	
            	 var url = "/neas/api/support/acceptcount";
            	 $.ajax({
            		 url:url,
            		 type: 'POST',
            		 
            		success: function (result){
            						
            			var parsed = jQuery.parseJSON(result);
            			console.log(parsed.count)
            			$("#accept-nominee").html();
            	      	document.getElementById('accept-nominee').innerHTML = parsed.count;
            	      	
            				     
            		} 
            	 	 
            	 });
            	 
            });
            
         </script>
         <script type="text/javascript">
            $(document).ready(function() {
            	
            	var url = "/neas/api/support/pendingcount";
           	 $.ajax({
           		 url:url,
           		 type: 'POST',
           		 
           		success: function (result){
           						
           			var parsed = jQuery.parseJSON(result);
           			console.log(parsed.count)
           			$("#pending-nominee").html();
           	      	document.getElementById('pending-nominee').innerHTML = parsed.count;
           	      	
           				     
           		} 
           	 	 
           	 });
            });
            </script>
            
            <script type="text/javascript">
            $(document).ready(function() {
            	
            	var url = "/neas/api/support/declinecount";
           	 $.ajax({
           		 url:url,
           		 type: 'POST',
           		 
           		success: function (result){
           						
           			var parsed = jQuery.parseJSON(result);
           			console.log(parsed.count)
           			$("#decline-nominee").html();
           	      	document.getElementById('decline-nominee').innerHTML = parsed.count;
           	      	
           				     
           		} 
           	 	 
           	 });
            });
            </script>
         </c:if>
      
      <!-- this is ajax call to get category wise data from dashboard controller -->
      <c:if test ="${isAdmin}">
         <script type="text/javascript">
            $(document).ready(function() {
            	 var url = "/neas/api/categorywisecount";
            	 console.log(url);
            	 $.ajax({
            		 url:url,
            		 type: 'GET',
            		 
            		 success: function (nomineeresult){
            						
            			var parsed = jQuery.parseJSON(nomineeresult);
            			console.log(parsed.count)
            			console.log(parsed.sccount)
            			console.log(parsed.stcount)
            			console.log(parsed.pwdcount)
            			$("#category-count").html();
            	      	document.getElementById('category-count').innerHTML = parsed.count;
            	      	$("#category-count1").html();
            	      	document.getElementById('category-sccount').innerHTML = parsed.sccount;
            	      	$("#category-count2").html();
            	      	document.getElementById('category-stcount').innerHTML = parsed.stcount;
            	      	/* $("#category-count3").html();
            	      	document.getElementById('category-pwdcount').innerHTML = parsed.pwdcount; */
            	      	
            		} 
            	 });
            });
            
            
            $(document).ready(function() {
           	 var url = "/neas/api/specialcatcount";
           	 console.log(url);
           	 $.ajax({
           		 url:url,
           		 type: 'POST',
           		 
           		 success: function (nomineeresult){
           						
           			var parsed = JSON.parse(nomineeresult);
           			console.log(parsed.women)
           			console.log(parsed.scst)
           			console.log(parsed.diff)
           			console.log(parsed.pwd)
           			$("#spec-women").html();
           	      	document.getElementById('spec-women').innerHTML = parsed.women;
           	      	$("#spec-scst").html();
           	      	document.getElementById('spec-scst').innerHTML = parsed.scst;
           	      	$("#spec-diff").html();
           	      	document.getElementById('spec-diff').innerHTML = parsed.diff;
           	      	 $("#spec-pwd").html();
           	      	document.getElementById('spec-pwd').innerHTML = parsed.pwd; 
           	      	
           		} 
           	 });
           });
            
         </script>
      </c:if>
      
      
      
      <!-- this is to get regional data table from dashboard controller for both nominator and nominee-->
      <c:if test ="${isRegional}">
         <script>
            var app = angular.module('myApp', []);
            app.controller('jsonCtrl', function($scope, $http) {
                $http.post("/neas/api/regionaldata")
                .then(function(response) {
                	$scope.tableData = response.data;
                	document.getElementById('nominator-count').innerHTML = response.data.length;
                });
                
                $http.post("/neas/api/nomiregionaldata")
                .then(function(response) {
                	$scope.tableDataNew = response.data;
                	document.getElementById('nominee-count').innerHTML = response.data.length;
                });
            });
         </script>
         </c:if>
     
      <!-- this is to get pwd count from nominee table-->
      <c:if test ="${isAdmin}">
      <script type="text/javascript">
         $(document).ready(function() {
         	
         	 var url = "/neas/api/pwdcount";
         	 console.log(url);
         	 $.ajax({
         		 url:url,
         		 type: 'POST',
         		 
         		success: function (pwdcountresult){
         						
         			var parsed = jQuery.parseJSON(pwdcountresult);
         			console.log(parsed.pwdcount)
         			$("#pwd-count").html();
         	      	document.getElementById('pwd-count').innerHTML = parsed.pwdcount;
         	      	
         				     
         		} 
         	 	 
         	 });
         });
         
      </script>
      </c:if>
      
            
      
      
      
     <!--  <script type="text/javascript">
      $(document).ready(function () {
  	    $.ajax({
  	        url: "/neas/api/nominatordata",
  	        type : 'GET',

  	        success: function (result) {
  	        	for(var key in result) {
  	        		var json = result[key];
  	        		console.log("value ajax "+ json['fullName']);
  	        	}
  	        } 
  	           
  	        }
  	    });
  	});
     
      </script> -->
       <!-- <script type="text/javascript">
      $(document).ready(function() {
    	$('#dataTable2').DataTable( {
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    } );
} );
      </script> -->
                     
                     <script>
         $(document).ready(function() {
             $('.type-table1').click(function(){
                 $('.datatable').fadeIn();
                 $('.datatable2').fadeOut();
                 $("#example").DataTable().columns(1).search("", true, false, true).draw();
             });
              
              $('.type-table2').click(function(){
                 $('.datatable2').fadeIn();
                 $('.datatable').fadeOut();
                 $("#example1").DataTable().columns(10).search("", true, false, true).draw();
             });
              /* js for datatable count  */
              $('body').change(function(){
                  $('.datatable #filter-count1 .dataTables_info').remove();
                  $(".datatable .dataTables_info").clone().appendTo(".datatable #filter-count1");
                       $('.total_record').show();
                       
                       $('.datatable2 #filter-count2 .dataTables_info').remove();
                       $(".datatable2 .dataTables_info").clone().appendTo(".datatable2 #filter-count2");
                            $('.total_record2').show();
                  });
              
              
              var path = $(location).attr('href');
              if(path.includes('nomineeback')){
            	  $('.datatable').fadeOut();
                  $('.datatable2').fadeIn();
              }
              if(path.includes('nominatorback')){
            	  $('.datatable').fadeIn();
                  $('.datatable2').fadeOut();
              }
              
              $("#refresh-page").click( function(e){
            	    e.preventDefault();
            	    $('#example').DataTable().search('').draw();
            	    localStorage.removeItem("DataTables_example_/userdata");
            	    window.location.href="/userdata?nominatorback";
            	});
              $("#refresh-page1").click( function(e){
          	    e.preventDefault();
          	    $('#example1').DataTable().search('').draw();
          	    localStorage.removeItem("DataTables_example1_/userdata");
          	  	window.location.href="/userdata?nomineeback";
          	});
              
          });
         $(document).ready(function(){
 	 		$("#accepted-count").click(function(){
                $('.datatable2').fadeIn();
                $('.datatable').fadeOut();
 	 			$("#example1").DataTable().columns(10).search("ACCEPTED", true, false, true).draw();
 	 		});
 	 		$("#decline-count").click(function(){
                $('.datatable2').fadeIn();
                $('.datatable').fadeOut();
 	 			$("#example1").DataTable().columns(10).search("DECLINED", true, false, true).draw();
 	 		});
 	 		$("#pending-count").click(function(){
                $('.datatable2').fadeIn();
                $('.datatable').fadeOut();
 	 			$("#example1").DataTable().columns(10).search("PENDING", true, false, true).draw();
 	 		});
 	 });
         
      </script>
      
      <!-- <script type="text/javascript">
      $(document).ready(function() {
    	$('#dataTable2').DataTable( {
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    } );
} );
      </script> -->
      <!-- qwertyuiop 
         <script src="/resources/dashboard/js/pagination.js"></script>
         <script src="/resources/dashboard/js/pagination-code.js"></script>
         <script src="/resources/dashboard/js/data-table.yadcf.js"></script>
         <script src="/resources/dashboard/js/data-table.js"></script>  -->
     
     <%--  <!-- this is to get regional data table from dashboard controller for both nominator -->
      <c:if test ="${isRegional}">
         <script>
            var app = angular.module('myApp', []);
            app.controller('jsonCtrl', function($scope, $http) {
                $http.post("/neas/api/regionaldata")
                .then(function(response) {
                	$scope.tableData = response.data;
                	document.getElementById('nominator-count').innerHTML = response.data.length;
                });
                
                $http.post("/neas/api/nomiregionaldata")
                .then(function(response) {
                	$scope.tableDataNew = response.data;
                	document.getElementById('nominee-count').innerHTML = response.data.length;
                });
            });
         </script>
      </c:if> --%>
      
     
     <%@include file="dashboard/disablewarning.jsp" %>
     
     
     
     <style>
     .dashboard-stat.blue {
    background-color: #3598dc;
    margin-left: 15px;
}
        .datatable .table-responsive,.datatable .portlet-title,.new{width:100%!important;float:left}
        .datatable2 .table-responsive,.datatable2 .portlet-title,.new{width:100%!important;float:left}
.datatable #example_filter,.datatable2 #example_filter{float:right}
.new.affix{position:fixed;background:#fff;z-index:9999;width:1200px!important;top:51px}
.new2.affix{position:fixed;background:#fff;z-index:9999;width:1200px!important;top:51px}
.portlet.box .portlet-title .caption{padding:11px 0 9px}
.portlet.box>.portlet-body{background-color:#fff;padding:15px}
.portlet.light{padding:12px 20px 15px;background-color:#fff}
.portlet.light.bordered{border:1px solid #e7ecf1!important}
.portlet.light.bordered .portlet-title{border-bottom:1px solid #eef1f5;padding:0;min-height:48px;border-bottom:1px solid #eee;padding:0;margin-bottom:10px;min-height:41px;-webkit-border-radius:4px 4px 0 0;-moz-border-radius:4px 4px 0 0;-ms-border-radius:4px 4px 0 0;-o-border-radius:4px 4px 0 0;border-radius:4px 4px 0 0}
.portlet.light.bg-inverse{background:#f1f4f7}
.portlet.light> portlet-title{padding:0;min-height:48px}
.portlet.light> portlet-title>.caption{color:#666;padding:10px 0}
.portlet.light> portlet-title>.caption>.caption-subject{font-size:16px}
.dataTables_filter{float:right}
.portlet.light.bordered{border:1px solid #e7ecf1!important;float:left;width:100%}
.datatable2, .datatable
{
position:absolute;
left:0;
width:100%;
}
.page-content-wrapper .page-content
{
position:relative;padding-left: 0;}
.page-content-white .page-bar
{margin-left: 0;
}.page-content-white .page-title
{padding-left: 17px;
}
.page-footer{
display:none;}
        </style>
       
<script>
jQuery(document).ready(function(){
	$( ".datatable .portlet-title,.datatable .dt-buttons,.datatable .dataTables_filter,.datatable .total_record" ).wrapAll( "<div class='new' />") ;
	$( ".datatable2 .portlet-title,.datatable2 .dt-buttons,.datatable2 .dataTables_filter,.datatable2 .total_record2" ).wrapAll( "<div class='new2' />") ;
	
	$('.type-table1').on('click',function(){
	
var a=jQuery('.datatable .new').offset().top-70;

jQuery(document).scroll(function(){
		if(jQuery(document).scrollTop()>a)
		{	
jQuery(".datatable .new").addClass('affix');
			jQuery(".datatable .portlet-body").css({"padding-top":80});
            
		}

		if(jQuery(document).scrollTop()<a)
		{
jQuery(".datatable .new").removeClass('affix');
			jQuery(".datatable .portlet-body").css({"padding-top":0});
				
		}
		});
		
		});
	
	$('.type-table2').on('click',function(){
		
		var b=jQuery('.datatable2 .new2').offset().top-70;
		
		jQuery(document).scroll(function(){
				if(jQuery(document).scrollTop()>b)
				{	
		jQuery(".datatable2 .new2").addClass('affix');
					jQuery(".datatable2 .portlet-body").css({"padding-top":80});
		            
				}

				if(jQuery(document).scrollTop()<b)
				{
		jQuery(".datatable2 .new2").removeClass('affix');
					jQuery(".datatable2 .portlet-body").css({"padding-top":0});
						
				}
				});
				
				});
	
	});
$(document).ready(function(){
    $.fn.dataTable.ext.errMode = 'none';

    $('#example').on( 'error.dt', function ( e, settings, techNote, message ) {
    console.log( 'An error has been reported by DataTables: ', message );
    } ) .DataTable();
    $('#example').DataTable();
    $('#example1').on( 'error.dt', function ( e, settings, techNote, message ) {
        console.log( 'An error has been reported by DataTables: ', message );
        } ) .DataTable();
        $('#example1').DataTable();
    
    });
</script>
     
                     
    </body>
	
</html>