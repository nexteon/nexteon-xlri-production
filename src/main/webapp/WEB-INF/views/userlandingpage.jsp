<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>


<sec:authorize access="isAnonymous()">
	<c:redirect url="/login"/>
</sec:authorize>

<sec:authorize access="hasRole('ADMIN') or hasRole('REGIONAL') or hasRole('SUPPORT') or hasRole('EXPERT')">
	<c:redirect url="/userdata"/>
</sec:authorize>

<sec:authorize access="hasRole('NOMINATOR')">
	<c:redirect url="/nominatorlanding"/>
</sec:authorize>

<sec:authorize access="hasRole('NOMINEE')">
	<c:redirect url="/nomineelanding"/>
</sec:authorize>
