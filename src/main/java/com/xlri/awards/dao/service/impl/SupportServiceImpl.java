package com.xlri.awards.dao.service.impl;



import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlri.awards.dao.entity.NewsSection;
import com.xlri.awards.dao.entity.SupportAgent;
import com.xlri.awards.dao.service.api.NewsService;
import com.xlri.awards.dao.service.api.SupportService;

@Repository
@Transactional
public class SupportServiceImpl implements SupportService{

	private static final Logger LOGGER = LoggerFactory.getLogger(NominatorServiceImpl.class);
	
	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public boolean saveSupport(SupportAgent supportAgent){
		try{
			
		Session session = sessionFactory.openSession();
		session.getTransaction().begin();
		session.save(supportAgent);
		session.getTransaction().commit();
		return true;
		}
		
		catch(Exception e){
			LOGGER.error(e.getMessage());
			return false;
			
		}
		
	}
	@Override
	public boolean updateSupport(SupportAgent supportAgent){
		try{
			
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			session.update(supportAgent);
			session.getTransaction().commit();
			return true;
		}
		catch(Exception e){
			LOGGER.error(e.getMessage());
			return false;
		}
	}
	
	
	@Override
	public List<SupportAgent> getAllSupportData() {
		List<SupportAgent> list = null;
		try{
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			list = session.createQuery("From NewsSection").getResultList();
			session.getTransaction().commit();
			return list;
		}
		catch(Exception e){
			LOGGER.error(e.getMessage());
			return null;	
		}
	}
	@Override
	public List<SupportAgent> getPending() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<SupportAgent> getAccept() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<SupportAgent> getDeclined() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
}
