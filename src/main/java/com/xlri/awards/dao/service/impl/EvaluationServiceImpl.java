package com.xlri.awards.dao.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlri.awards.dao.entity.Evaluation;
import com.xlri.awards.dao.entity.Expert;
import com.xlri.awards.dao.entity.NomineeDetails;
import com.xlri.awards.dao.service.api.EvaluationService;

@Repository
@Transactional
public class EvaluationServiceImpl implements EvaluationService{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PartnersServiceImpl.class);
	
	@Autowired
	SessionFactory sessionFactory;

	@Override
	public boolean saveEvaluationDetails(Evaluation evaluation) {
		try {

			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			session.save(evaluation);
			session.getTransaction().commit();
			return true;
		}
	
		catch (Exception e) {
			LOGGER.error(e.getMessage());
			return false;

		}

	}

	@Override
	public boolean updateEvaluationDeatails(Evaluation evaluation) {
		try {

			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			session.update(evaluation);
			session.getTransaction().commit();
			return true;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return false;
		}
	}

	@Override
	public List<Evaluation> getAllEvaluationValues() {
		List<Evaluation> evaluationList = null;
		try{
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			evaluationList = session.createQuery("From Evaluation").getResultList();
			session.getTransaction().commit();
			return evaluationList;
		}
		catch(Exception e){
			LOGGER.error(e.getMessage());
			return null;	
		}
	}

	@Override
	public List<NomineeDetails> getNomineeByExpertAssigned(String emailId) {
		try{
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<NomineeDetails> query = session.createQuery("SELECT n from NomineeDetails n where n.emailId in(SELECT e.emailId FROM Evaluation e" + 
					" where" + 
					" e.expert1 = :emailId or "+ 
					" e.expert2 = :emailId or" + 
					" e.expert3 = :emailId)");
			query.setParameter("emailId", emailId);
			session.getTransaction().commit();
			return query.getResultList();
		}
		catch(Exception e){
			LOGGER.error(e.getMessage());
			return null;	
		}
	}
	
	@Override
	public Evaluation getNomineeByEmailId(String emailId) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<Evaluation> query =  session.createQuery("SELECT n FROM Evaluation n WHERE n.emailId='"+emailId+"'");
			session.getTransaction().commit();
			return query.getSingleResult();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}
	
	@Override
	public List<Evaluation> getAllListByNominee(String... state) {
		List<Evaluation> stateList = new ArrayList<>();
		for(String x : state){
				
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<Evaluation> query =  session.createQuery("SELECT e FROM Evaluation e WHERE e.state=:state");
			query.setParameter("state", x);
			stateList.addAll(query.getResultList());
			session.getTransaction().commit();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
		}
		return stateList;
	}

	@Override
	public Evaluation getEvaluationByNominee(String emailId) {
		try{
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<Evaluation> query = session.createQuery("SELECT e FROM Evaluation e where e.emailId = :emailId");
			query.setParameter("emailId", emailId);
			session.getTransaction().commit();
			return query.getResultList().get(0);
		}
		catch(Exception e){
			LOGGER.error(e.getMessage());
			return null;	
		}
	}
	
}
