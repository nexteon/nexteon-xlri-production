<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="com.xlri.awards.common.StateUtil" %>
<!DOCTYPE html>
<html lang="en">
	<%@include file="dashboard/headerlibs.jsp" %>
	<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white" id="page-top">
		<div class="page-wrapper">
			<%@include file="dashboard/header.jsp" %>
			<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<!-- BEGIN CONTENT BODY -->
				<div class="page-content">
					<!-- BEGIN PAGE HEADER-->
					<!-- BEGIN PAGE BAR -->
					<div class="page-bar">
						<ul class="page-breadcrumb">
							<li>
								<a href="/userdata">Home</a>
								<i class="fa fa-circle"></i>
							</li>
							<li>
								<span>Upload Shortlisted Candidate Data</span>
							</li>
						</ul>
					</div>
					<h1 class="page-title">Upload Shortlisted Candidate Data</h1>
					<div class="row">
						<div class="col-xs-12">
							<form action="/neas/api/upload/shortlisted" enctype="multipart/form-data" method="post">
								<input type="file" name="uploadfile"/>
								<input type="submit" class="btn btn-danger"/>
							</form>
						</div>
						<div class="col-xs-12">
							<h4 style="color:green">${successMsg}</h4>
							<h4 style="color:red">${errorMsg}</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END QUICK NAV -->
		<%@include file="dashboard/footer.jsp"%>
	</body>
</html>