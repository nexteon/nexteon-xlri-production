package com.xlri.awards.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Evaluation")
public class Evaluation {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	
	private Long id;
	
	@Column(nullable=false)
	private String nomineeName;
	
	@Column(nullable=false)
	private String emailId; 
	
	@Column(nullable=false)
	private String contact;
	
	private String state;
	
	private String expert1;
	
	private int marks1;
	
	private int marks2;
	
	private int marks3;
	
	private boolean activeStatus;
	
	private String comments;
	
	private String expert2;
	
	private String expert3;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNomineeName() {
		return nomineeName;
	}

	public void setNomineeName(String nomineeName) {
		this.nomineeName = nomineeName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getMarks1() {
		return marks1;
	}

	public void setMarks1(int marks1) {
		this.marks1 = marks1;
	}

	public int getMarks2() {
		return marks2;
	}

	public void setMarks2(int marks2) {
		this.marks2 = marks2;
	}

	public boolean isActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(boolean activeStatus) {
		this.activeStatus = activeStatus;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getExpert1() {
		return expert1;
	}

	public void setExpert1(String expert1) {
		this.expert1 = expert1;
	}

	public String getExpert2() {
		return expert2;
	}

	public void setExpert2(String expert2) {
		this.expert2 = expert2;
	}

	public String getExpert3() {
		return expert3;
	}

	public void setExpert3(String expert3) {
		this.expert3 = expert3;
	}

	public int getMarks3() {
		return marks3;
	}

	public void setMarks3(int marks3) {
		this.marks3 = marks3;
	}
	
	
	
	
	
	
	
	

}
