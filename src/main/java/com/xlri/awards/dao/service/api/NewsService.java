package com.xlri.awards.dao.service.api;

import java.util.List;

import com.xlri.awards.dao.entity.NewsSection;



public interface NewsService {

	public boolean saveNews(NewsSection newsSection);

	public boolean updateNews(NewsSection newsSection);

	public List<NewsSection> getAllNews();

	public List<NewsSection> getNewsDescFour();

}
