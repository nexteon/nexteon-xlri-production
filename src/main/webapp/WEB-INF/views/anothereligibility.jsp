<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
   <%@include file="fragments/headerlibs.jsp" %>
   <body class="innerpage">
      <%@include file="fragments/header.jsp" %>
      <!-- Body Starts Here -->
      <main role="main">
         <div class="page-title">
            <h2>ELIGIBILITY</h2>
         </div>
         <div class="content">
            <div class="container">
               <div class="row">
                  <div class="col-md-12">
                     <div class="whitebg eligibilitypg">
                        <div class="clearfix"></div>
                        <div class="col-md-12 pull-left">
                           <h2 class="inner-heading blue-hdg">Summary Table of Key Eligibility Indicators</h2>
                           <div class="table-responsive">
                           <table class="table table-bordered">
								<tbody>
									<tr>
									<td>
									<p><strong>Category</strong></p>
									</td>
									<td>
									<p><strong>Type of Entrepreneur / Enterprise</strong></p>
									</td>
									<td>
									<p><strong>Age</strong></p>
									<p><strong>(as on 01 Apr 2018)*</strong></p>
									</td>
									<td>
									<p><strong>Generation*</strong></p>
									</td>
									<td>
									<p><strong>Ownership*</strong></p>
									</td>
									</tr>
									<tr>
									<td rowspan="4">
									<p><strong>Special</strong></p>
									</td>
									<td>
									<p><strong>Woman/Women</strong></p>
									</td>
									<td>
									<p>Majority owners must be woman/women aged 40 years or less</p>
									</td>
									<td>
									<p>Majority owners must be First-Generation Entrepreneurs</p>
									</td>
									<td>
									<p>Woman/women must individually/collectively own <strong><u>75% or more</u></strong> of the enterprise</p>
									</td>
									</tr>
									<tr>
									<td>
									<p><strong>Scheduled Caste/ Schedule Tribe</strong></p>
									</td>
									<td>
									<p>Majority owners must belong to either SC or ST Category and must be aged 40 years or less</p>
									</td>
									<td>
									<p>Majority owners must be First-Generation Entrepreneurs</p>
									</td>
									<td>
									<p>People belonging to the SC/ST category must individually/collectively own 75<strong><u>% or more</u></strong> of the enterprise</p>
									</td>
									</tr>
									<tr>
									<td>
									<p><strong>Person(s) with Disabilities</strong></p>
									</td>
									<td>
									<p>Majority owners must be person(s) with disabilities and must be aged 40 years or less</p>
									</td>
									<td>
									<p>Majority owners must be First-Generation Entrepreneurs</p>
									</td>
									<td>
									<p>People with Disability/disabilities must individually/collectively own <strong><u>51%</u></strong> or more of the enterprise</p>
									</td>
									</tr>
									<tr>
									<td>
									<p><strong>Difficult Areas</strong></p>
									</td>
									<td>
									<p>Majority owners must be residents of Difficult Areas and must be aged 40 years or less</p>
									</td>
									<td>
									<p>Majority owners must be First-Generation Entrepreneurs</p>
									</td>
									<td>
									<p>People belonging to the Difficult Areas must individually/collectively own <strong><u>75% or more</u></strong> of the enterprise</p>
									</td>
									</tr>
									<tr>
									<td rowspan="4">
									<p><strong>General</strong></p>
									</td>
									<td>
									<p><strong>Collective Enterprises such as Producers Companies / Self-Help Groups / Cooperatives</strong></p>
									</td>
									<td>
									<p>Majority members must be aged less than 40 years</p>
									</td>
									<td>
									<p>Majority members must be First-Generation Entrepreneurs</p>
									</td>
									<td>&nbsp;</td>
									</tr>
									<tr>
									<td>
									<p><strong>Partnership Firm</strong></p>
									</td>
									<td>
									<p>Majority owners&rsquo; stake must belong to person/persons who are aged less than 40 years</p>
									</td>
									<td>
									<p>Majority owners must be First-Generation Entrepreneurs</p>
									</td>
									<td>
									<p>Majority owners must hold more than <strong><u>51%</u></strong> or more ownership, must be with First Generation Entrepreneurs of age less than 40 years</p>
									</td>
									</tr>
									<tr>
									<td>
									<p><strong>Company</strong></p>
									</td>
									<td>
									<p>Majority of shareholders must be aged less than 40 years</p>
									</td>
									<td>
									<p>Majority shareholders must be First-Generation Entrepreneurs</p>
									</td>
									<td>
									<p>Majority shareholders must hold <strong><u>51%</u></strong> or more equity and be of age less than 40 years</p>
									</td>
									</tr>
									<tr>
									<td>
									<p><strong>Sole-Proprietorship</strong></p>
									</td>
									<td>
									<p>Owner must be aged less than 40 years</p>
									</td>
									<td>
									<p>Owner must be a First-Generation Entrepreneur</p>
									</td>
									<td>&nbsp;</td>
									</tr>
								</tbody>
                           </table>
                           </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 pull-left">
                           <p>All the above aspects will be verified during the field visits, for shortlisted nominations; any additional information required will need to be promptly provided by the Nominee.</p>
                           <!-- <p><b>NOTE:</b> In all the above, the enterprise must be a young enterprise started within last 6 years, i.e., on or after April 1st 2012</p> -->
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </main>
      <!-- Body Ends Here -->
      <%@include file="fragments/footer.jsp" %>
   </body>
</html>