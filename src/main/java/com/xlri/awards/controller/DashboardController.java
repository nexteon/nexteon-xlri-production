package com.xlri.awards.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.xlri.awards.common.CommonUtil;
import com.xlri.awards.common.EncryptionUtil;
import com.xlri.awards.common.OtpSalt;
import com.xlri.awards.common.SMSUtil;
import com.xlri.awards.dao.entity.Evaluation;
import com.xlri.awards.dao.entity.Expert;
import com.xlri.awards.dao.entity.NewsSection;
import com.xlri.awards.dao.entity.Nominator;
import com.xlri.awards.dao.entity.NomineeDetails;
import com.xlri.awards.dao.entity.Partners;
import com.xlri.awards.dao.entity.SecondCatNominee;
import com.xlri.awards.dao.entity.ThirdCatNominee;
import com.xlri.awards.dao.entity.User;
import com.xlri.awards.dao.service.api.EvaluationService;
import com.xlri.awards.dao.service.api.ExpertService;
import com.xlri.awards.dao.service.api.MailService;
import com.xlri.awards.dao.service.api.NewsService;
import com.xlri.awards.dao.service.api.NominatorService;
import com.xlri.awards.dao.service.api.NomineeService;
import com.xlri.awards.dao.service.api.PartnersService;
import com.xlri.awards.dao.service.api.SecondCatNomineeService;
import com.xlri.awards.dao.service.api.ThirdCatNomineeService;
import com.xlri.awards.dao.service.api.UserService;

@Controller
@PropertySource("classpath:application.properties")
public class DashboardController {
	
	@Autowired
	Environment env;

	@Autowired
	UserService userservice;

	@Autowired
	NominatorService nominatorservice;

	@Autowired
	NomineeService nomineeservice;
	
	@Autowired
	NewsService newsService;
	
	@Autowired
	ExpertService expertService;
	
	@Autowired
	PartnersService partnersService;
	
	@Autowired
	SecondCatNomineeService secCatService;
	
	@Autowired
	ThirdCatNomineeService thirdCatService;
	
	@Autowired
	MailService mailService;
	
	@Autowired
	EvaluationService evaluationService;
	
	@Autowired
	UserService userService;

	@RequestMapping(value = "/userdata", method = RequestMethod.GET)
	public String userData() {
		Collection<? extends GrantedAuthority> auth = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		for(GrantedAuthority role : auth){
			if(role.getAuthority().equals("ROLE_ADMIN")){
				return "dashboard";
			}
			if(role.getAuthority().equals("ROLE_SUPPORT")){
				return "dashboard";
			}
			if(role.getAuthority().equals("ROLE_REGIONAL")){
				return "regionaldashboard";
			}
			if(role.getAuthority().equals("ROLE_EXPERT")){
				return "expertdashboard";
			}
		}
		return "index";
	}

	/* To get Nominator Table Data in JSON Format */
	
	@RequestMapping(value = "/neas/api/nominatordata", method = RequestMethod.POST)
	public void handleJSON(HttpServletRequest request,HttpServletResponse respnose) throws IOException{
		List<Nominator> u1 = nominatorservice.getAllNominators();
		  JSONArray array = new JSONArray();
		  for(int i=0;i<u1.size();i++)
		        try{
		        	//Nominator nominator = nominatorservice.getNominatorByEmailId(u1.get(i).getEmailId());
		        	JSONObject result = new JSONObject();
		        	result.put("id", u1.get(i).getId());
		        	result.put("registrationnumber",u1.get(i).getRegistrationNumber());
		        	result.put("view", "<a class='btn btn-primary' href='/view/nominator?p="+u1.get(i).getRegistrationNumber()+"'> View </a>");
		        	result.put("fullname", u1.get(i).getFullName());
		            result.put("role",u1.get(i).getRole());
		            result.put("emailid", u1.get(i).getEmailId());
		            result.put("contactnumber",u1.get(i).getContactNumber());
		            result.put("occupation",u1.get(i).getOccupation());
		            result.put("designation",u1.get(i).getDesignation());
		            result.put("institutename", u1.get(i).getInstituteName());
		            result.put("nominationtype",u1.get(i).getNominationType());        
		            result.put("addrline1", u1.get(i).getAddrLine1());
		            result.put("addrline2",u1.get(i).getAddrLine2());
		            result.put("addrcity",u1.get(i).getAddrCity());
		            result.put("addrstate", u1.get(i).getAddrState());
		            result.put("addrlandmark",u1.get(i).getAddrLandmark());
		            result.put("addrpincode",u1.get(i).getAddrPinCode());
		            array.put(i, result);
		        }
		        catch(Exception ex){
		         ex.printStackTrace();
		        }
		  
		  respnose.setContentType("application/json");
		  respnose.getWriter().write(array.toString());
		}
	
	
	
/* To get Nominee Table Data in JSON Format */
	
	@RequestMapping(value = "/neas/api/nomineedata", method = RequestMethod.POST)
	 public void nomineeJson(HttpServletRequest request ,HttpServletResponse response) throws IOException,ServletException{
		/* List<NomineeDetails> u2 = nomineeservice.getAllNominee();
		Gson gson = new Gson();
		String result = gson.toJson(u2);
		response.getWriter().println(result);*/
		
		List<NomineeDetails> u1 = nomineeservice.getAllNominee();
		List<SecondCatNominee> secNom = secCatService.getAllSecCatNom();
		List<ThirdCatNominee> thirdNom = thirdCatService.getAllThirdCatNom();
		JSONArray array = new JSONArray();
		  for(int i=0;i<u1.size();i++)
	          try{
	           JSONObject result = new JSONObject();
	           result.put("id", u1.get(i).getId());
	           result.put("registrationnumber",u1.get(i).getRegistrationNumber());
	           result.put("view", "<a class='btn btn-primary' href='/view/nominee?p="+u1.get(i).getContact()+"'> View </a>");
	           result.put("nomineename", u1.get(i).getNomineeName());
	              //result.put("role",u1.get(i).getRole());
	              result.put("emailid", u1.get(i).getEmailId());
	              result.put("contactnumber",u1.get(i).getContact());
	              result.put("awardcategory",u1.get(i).getAwardCategory().equals("enterprise")?"Enterprise Award Track":"Ecosystem Builder's Track");
	              switch(u1.get(i).getEnterpriseCategory()) {
	              case "onelac":	result.put("enterprisecategory","Initial Investment upto 1 Lakh");break;
	              case "uptoten":	result.put("enterprisecategory","Initial Investment between 1 Lakh to 10 Lakh");break;
	              case "uptocrore":	result.put("enterprisecategory","Initial Investment between 10 Lakh to 1 Crore");break;
	              default: result.put("enterprisecategory",u1.get(i).getEnterpriseCategory());break;
	              }
	              result.put("awardsub",u1.get(i).getAwardSub());
	              switch(u1.get(i).getGender()) {
	              case "m":	result.put("gender","Male");break;
	              case "f":	result.put("gender","Female");break;
	              case "t":	result.put("gender","Third Gender");break;
	              default: result.put("gender",u1.get(i).getGender());break;
	              }
	              if(u1.get(i).getSocialCategory()!=null) {
	              switch(u1.get(i).getSocialCategory()) {
	              case "gen":	result.put("socialcategory","General");break;
	              case "sc":	result.put("socialcategory","SC");break;
	              case "st":	result.put("socialcategory","ST");break;
	              default: result.put("socialcategory",u1.get(i).getSocialCategory());break;
	              }        
	              }else {
	            	  result.put("socialcategory",u1.get(i).getSocialCategory());
	              }
	              result.put("dob", u1.get(i).getDob());
	              result.put("speccatval",u1.get(i).getSpeccatval());
	              result.put("enterprisename",u1.get(i).getEnterpriseName());
	              result.put("address1", u1.get(i).getAddress1());
	              result.put("address2",u1.get(i).getAddress2());
	              result.put("city",u1.get(i).getCity());          
	              result.put("state",u1.get(i).getState()); 
	              result.put("createdon", u1.get(i).getCreatedOn());
	              result.put("status", u1.get(i).getFilledStatus());
	              boolean s = false;
	              for(SecondCatNominee scn:secNom){
	            	  if(scn.getContact().equals(u1.get(i).getContact())){
	            		  result.put("fileuploaded", "Yes");
	            		  s = true;
	            		  break;
	            	  }
	              }
	              for(ThirdCatNominee scn:thirdNom){
	            	  if(scn.getContact().equals(u1.get(i).getContact())){
	            		  result.put("fileuploaded", "Yes");
	            		  s = true;
	            		  break;
	            	  }
	              }
	              if(!s){result.put("fileuploaded", "No");}
	              //result.put("landmark", u1.get(i).getLandmark());
	              //result.put("street",u1.get(i).getStreet());
	              //result.put("pincode",u1.get(i).getPinCode());
	              //result.put("website", u1.get(i).getWebsite());
	              //result.put("knownominee",u1.get(i).getKnowNominee());
	              
	              
	              //result.put("rolenominee",u1.get(i).getRoleNominee());        
	              //result.put("impactnominee", u1.get(i).getImpactNominee());
	              //result.put("recognitionnominee",u1.get(i).getRecognitionNominee());
	              //result.put("collaborationnominee",u1.get(i).getCollaborationNominee());
	              //result.put("specmentionnominee", u1.get(i).getSpecMentionNominee());
	              array.put(i, result);
	          }
	          catch(Exception ex){
	           ex.printStackTrace();
	          }
		  response.setContentType("application/json");
		  response.getWriter().write(array.toString());
	    }
	
	

	/*
	 * @RequestMapping(value = "/nomdata", method = RequestMethod.GET) public
	 * ModelAndView getTotalNomJSONData(HttpServletRequest request){
	 * ModelAndView model = new ModelAndView("dashboard");
	 * 
	 * List<Nominator> u1 = nominatorservice.getAllNominators();
	 * System.out.println(u1+""+ "DashboardData");
	 * 
	 * JSONObject result = new JSONObject(); try{
	 * 
	 * result.put("username", u1.get(0).getFullName());
	 * result.put("role",u1.get(0).getRole()); System.out.println(result); }
	 * 
	 * catch(Exception ex){ ex.printStackTrace();
	 * 
	 */

	@RequestMapping(value = "/userrole", method = RequestMethod.GET)
	public void getUserRole() {
		/*
		 * User user = new User(); role=user.getRole(); if(role != "NOMINATOR"){
		 * System.out.println("you are an individiual"); }
		 * 
		 * else{ System.out.println("you are a NOMINATOR"); } return user
		 */;

		userservice.getAllRole();
		System.out.println(userservice.getAllRole().size() + " " + " DashBoard");

	}

	
	/* To get total number of nominators */
	
	@RequestMapping(value = "/neas/api/nominator", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void getTotalNominator(HttpServletResponse response)throws IOException,ServletException{
		Long nomi = (long) nominatorservice.getAllNominators().size();
		Map<String,Long> map = new HashMap<>();
		map.put("count",nomi);
		Gson gson = new Gson();
		String nomresult = gson.toJson(map);
		response.getWriter().println(nomresult);
/*		nominator=nominatorservice.getAllNominators().size() + "nominator all";
		map.addAttribute("total", nominatorservice.getAllNominators());
*/	    	
       }

	/* To get total number of nominees */
	
	@RequestMapping(value = "/neas/api/nominee", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void getTotalNominee(HttpServletResponse response)throws IOException,ServletException{
		Long nominee = (long) nomineeservice.getAllNominee().size();
		Map<String,Long> map = new HashMap<>();
		map.put("count",nominee);
		Gson gson = new Gson();
		String nomineeresult = gson.toJson(map);
		response.getWriter().println(nomineeresult);
	

	}
	
	/* To get category wise nominees count from nomineedetails entity */
	
	@RequestMapping(value = "/neas/api/categorywisecount", method = RequestMethod.GET)
	public void getsocialCategory(HttpServletResponse response)throws IOException,ServletException{
		
		int genNominee = nomineeservice.getNomineeBySocialCat("gen");
		int SCNominee = nomineeservice.getNomineeBySocialCat("sc");
		int STNominee = nomineeservice.getNomineeBySocialCat("st");
		int PWDNominee = nomineeservice.getNomineeBySocialCat("pwd");
		
		Map<String,Long> map = new HashMap<>();
		map.put("count",(long) genNominee);
		map.put("sccount", (long) SCNominee);
		map.put("stcount", (long) STNominee);
		map.put("pwdcount", (long) PWDNominee);
				
		Gson gson = new Gson();
		String nomineeresult = gson.toJson(map);
		response.getWriter().println(nomineeresult);
		
	}
	
	/*
	@RequestMapping(value = "/neas/api/categorywisecount", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void getCategoryCount(HttpServletResponse response) throws IOException,ServletException{
		Long category = (long) nomineeservice.getAllCategory();
		Map<String,Long> map = new HashMap<>();
		map.put("count", category);
		Gson gson = new Gson();
		String nomineeresult = gson.toJson(map);
		response.getWriter().println(nomineeresult);
				
	}*/
	
	/* To show news section in admin dashboard */
	
	@GetMapping(value = "/newssectionget")
	public ModelAndView registerNewsPage(HttpServletRequest request) {
		
		return new ModelAndView("newssection");
	}
	
	/* To add news in admin dashboard */
	@PostMapping(value = "/newssectionpost")
	public ModelAndView registerNews(HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		
		NewsSection news = new NewsSection();
		try{
			news.setActivatedStatus("yes");
			news.setTitle(request.getParameter("title"));
			news.setSubCategory(request.getParameter("subcategory"));
			news.setDescription(request.getParameter("description"));
			news.setCreatedOn();
			newsService.saveNews(news);
			model.setViewName("redirect:/newssectionget");
			model.addObject("successMsg", "News has been added.");
			return model;
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		model.setViewName("redirect:/newssection");
		model.addObject("errorMsg","Something went wrong! Please try again.");		
		return model;
		
		
	}
	
	/* To get all the regional data of nominator state wise */
	@RequestMapping(value="/neas/api/regionaldata", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void regionalListJson(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
		String emailId = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
		User user= userservice.getUserByUsername(emailId);
		String[] states= user.getState().split(",");
		List<Nominator> nominators = nominatorservice.getNominatorByState(states);
		response.getWriter().write(new Gson().toJson(nominators));
		
 	}
	
	/* To get all the regional data of nominee state wise */
	
	@RequestMapping(value="/neas/api/nomiregionaldata", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void nomiRegionalListJson(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
		String emailId = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
		User user= userservice.getUserByUsername(emailId);
		String[] states= user.getState().split(",");
		List<NomineeDetails> nominee = nomineeservice.getNomineeByState(states);
		response.getWriter().write(new Gson().toJson(nominee));
		
		
	}
	
	@RequestMapping(value="/neas/api/expertnomidata", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void nomiExpertListJson(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
		String emailId = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
		List<NomineeDetails> nominee = evaluationService.getNomineeByExpertAssigned(emailId);
		response.getWriter().write(new Gson().toJson(nominee));
		
		
	}
	
	/* To get the pwd count */
	@RequestMapping(value = "/neas/api/pwdcount", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void getTotalPWD(HttpServletResponse response)throws IOException,ServletException{
		Long pwdcount = (long) nomineeservice.getNomineeByPWD().size();
		Map<String,Long> map = new HashMap<>();
		map.put("pwdcount",pwdcount);
		Gson gson = new Gson();
		String pwdcountresult = gson.toJson(map);
		response.getWriter().println(pwdcountresult);
	

	}
	
	/* To show Expert section in dashboard */
	@RequestMapping(value = "/expertsection", method = RequestMethod.GET)
	public ModelAndView registerNewExpert(HttpServletRequest request) {
		
		return new ModelAndView("/expertsection");
	}
	
	
	/*To add Expert in Admin Dashboard */
	/*To add Expert in Admin Dashboard */
	
	@RequestMapping(value = "/expertsection", method = RequestMethod.POST)
	public ModelAndView registerExpert(HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		
		Expert expert = new Expert();
		User user = new User();
		try{
			expert.setExpertName(request.getParameter("expertName"));
			expert.setEmailId(request.getParameter("emailId"));
			expert.setContactNumber(request.getParameter("contactNumber"));
			expert.setDesignation(request.getParameter("designation"));
			expert.setSpecialization(request.getParameter("specialization"));
			expert.setAddress(request.getParameter("address"));
			expert.setPartnerWith(request.getParameter("partnerWith"));
			
			String password = request.getParameter("password");
			expert.setPassword(password);
			
			boolean f = false;
			 f = expertService.saveExpert(expert);
			
			if(f){
			
			user.setUsername(expert.getEmailId());
			user.setPassword(expert.getPassword());
			user.setSalt(expert.getSalt());
			user.setRole("EXPERT");
			user.setFullName(expert.getExpertName());
			userservice.saveUser(user);
			new Thread(new Runnable() {
				@Override
				public void run() {
					try{
						SMSUtil sMSUtil=new SMSUtil();
						sMSUtil.sendSMS("This is your Password:- " + password , expert.getContactNumber());
					}catch(Exception e){
						System.out.println(e);
					}
				}
			}).start();
			Thread t1 = new Thread(new Runnable() {
				@Override
				public void run() {
					mailService.sendPasswordToExpertMail(expert,password);
				}
			});
			t1.start();
			
			model.setViewName("redirect:/expertsection");
			model.addObject("successMsg", "Expert has been added.");
			return model;
			
			}
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		model.setViewName("redirect:/expertsection");
		model.addObject("errorMsg","Something went wrong! Please try again.");		
		return model;
		
		
	}
	
	/*@RequestMapping(value = "/expertsection", method = RequestMethod.POST)
	public ModelAndView registerExpert(HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		
		Expert expert = new Expert();
		try{
			expert.setExpertName(request.getParameter("expertName"));
			expert.setEmailId(request.getParameter("emailId"));
			expert.setContactNumber(request.getParameter("contactNumber"));
			expert.setDesignation(request.getParameter("designation"));
			expert.setSpecialization(request.getParameter("specialization"));
			expert.setAddress(request.getParameter("address"));
			expert.setPartnerWith(request.getParameter("partnerWith"));
			expert.setPassword(request.getParameter("password"));
			expertService.saveExpert(expert);
			model.setViewName("redirect:/expertsection");
			model.addObject("successMsg", "Expert has been added.");
			return model;
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		model.setViewName("redirect:/expertsection");
		model.addObject("errorMsg","Something went wrong! Please try again.");		
		return model;
		
		
	}*/
	
	/* To show Expert section in dashboard */
	@RequestMapping(value = "/allexperts", method = RequestMethod.GET)
	public ModelAndView getAllExpert(HttpServletRequest request) {
		
		return new ModelAndView("/allexperts");
	}
	
	/* To show Promoted Nominee by region in dashboard */
	@RequestMapping(value = "/promotednominee", method = RequestMethod.GET)
	public ModelAndView getPromotedNominee(HttpServletRequest request) {
		
		return new ModelAndView("/promotednominee");
	}
	
	/* To send all the expert details in json format*/
	@RequestMapping(value = "/allexperts", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void expertJson(HttpServletResponse response) throws IOException,ServletException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String emailId = auth.getPrincipal().toString();
		Collection<? extends GrantedAuthority> roles = auth.getAuthorities();
		for(GrantedAuthority role:roles){
			if(role.getAuthority().equals("ROLE_REGIONAL")){
				List<Expert> obj = expertService.getExpertCountCheck(emailId);
				Gson gson = new Gson();
				String result = gson.toJson(obj);
				response.getWriter().println(result);
			}
			if(role.getAuthority().equals("ROLE_ADMIN")){
				List<Expert> obj = expertService.getAllExpert();
				Gson gson = new Gson();
				String result = gson.toJson(obj);
				response.getWriter().println(result);
			}
		}
	}
	
	
/*To Update Expert in Admin Dashboard */
	
	
/*	@RequestMapping(value = "/updateexpertsection", method = RequestMethod.POST)
	public ModelAndView updateExpert(HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		
		Expert expert = new Expert();
		

		try{
			
			
			expert.setExpertName(request.getParameter("expertName"));
			expert.setEmailId(request.getParameter("emailId"));
			expert.setContactNumber(request.getParameter("contactNumber"));
			expert.setDesignation(request.getParameter("designation"));
			expert.setSpecialization(request.getParameter("specialization"));
			expert.setAddress(request.getParameter("address"));
			expert.setPartnerWith(request.getParameter("partnerWith"));
			expert.setPassword(request.getParameter("password"));
			expertService.updateExpert(expert);
			model.setViewName("redirect:/updateexpertsection");
			model.addObject("successMsg", "Expert has been updated.");
			return model;
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		model.setViewName("redirect:/updateexpertsection");
		model.addObject("errorMsg","Something went wrong! Please try again.");		
		return model;
		
		
	}*/
	
	/* To show Partners section in dashboard */
	@RequestMapping(value = "/allpartners", method = RequestMethod.GET)
	public ModelAndView getAllPartners(HttpServletRequest request) {
		
		return new ModelAndView("partners");
	}
	
	
	/* To show Partner section in dashboard */
	@RequestMapping(value = "/partnersection", method = RequestMethod.GET)
	public ModelAndView registerNewPartner(HttpServletRequest request) {
		
		return new ModelAndView("/partnersection");
	}
	
	/* To add Partners in admin dashboard */
	
	
	
	
	
	
	/*@RequestMapping(value = "/partnersection", method = RequestMethod.POST)
	public ModelAndView registerPartners(HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		
		Partners partners = new Partners();
		try{
			partners.setPartnerName(request.getParameter("partnerName"));
			partners.setEmailId(request.getParameter("emailId"));
			partners.setContactNumber(request.getParameter("contactNumber"));
			partners.setAddress(request.getParameter("address"));
			partners.setState(request.getParameter("state"));
			boolean f = partnersService.savePartners(partners);
			if(f){
				model.setViewName("redirect:/partnersection");
				model.addObject("successMsg", "Partner has been added.");
			}else{
				model.setViewName("redirect:/partnersection");
				model.addObject("errorMsg", "Error! Please Try Again with diffrent entry");
			}
			return model;
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		model.setViewName("redirect:/partnersection");
		model.addObject("errorMsg","Something went wrong! Please try again.");		
		return model;
		
		
	}*/
	
	/* To update Partners in admin dashboard */
	/*@RequestMapping(value = "/updatepartnersection", method = RequestMethod.POST)
	public ModelAndView updatePartners(HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		
		Partners partners = new Partners();
		try{
			partners.setPartnerName(request.getParameter("partnerName"));
			partners.setEmailId(request.getParameter("emailId"));
			partners.setContactNumber(request.getParameter("contactNumber"));
			partners.setAddress(request.getParameter("address"));
			partners.setState(request.getParameter("state"));
			partnersService.updatePartners(partners);
			model.setViewName("redirect:/updatepartnersection");
			model.addObject("successMsg", "Partner has been added.");
			return model;
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		model.setViewName("redirect:/updatepartnersection");
		model.addObject("errorMsg","Something went wrong! Please try again.");		
		return model;
		
		
	}*/
	 /*To send all the patners details in json format */
	@RequestMapping(value = "/allpartners", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void partnerJson(HttpServletResponse response) throws IOException,ServletException {
		
		List<Partners> obj = partnersService.getAllPartners();
		Gson gson = new Gson();
		String result = gson.toJson(obj);
		response.getWriter().println(result);
		}
	
	@RequestMapping(value = "/category/textile", method = RequestMethod.GET)
	public ModelAndView goTextilePage(HttpServletRequest request) {
		
		return new ModelAndView("/textile");
	}
	
	@RequestMapping(value = "/category/agriculture", method = RequestMethod.GET)
	public ModelAndView goAgriculturePage(HttpServletRequest request) {
		
		return new ModelAndView("/agriculture");
	}
	
	@RequestMapping(value = "/category/chemicals", method = RequestMethod.GET)
	public ModelAndView goChemicalsPage(HttpServletRequest request) {
		
		return new ModelAndView("/chemicals");
	}
	
	@RequestMapping(value = "/category/retailtrade", method = RequestMethod.GET)
	public ModelAndView goRetailtradePage(HttpServletRequest request) {
		
		return new ModelAndView("/retailtrade");
	}
	
	@RequestMapping(value = "/category/renewableenergy", method = RequestMethod.GET)
	public ModelAndView goRenewableEnergyPage(HttpServletRequest request) {
		
		return new ModelAndView("/renewableenergy");
	}
	
	@RequestMapping(value = "/category/handicraft", method = RequestMethod.GET)
	public ModelAndView goHandicraftPage(HttpServletRequest request) {
		
		return new ModelAndView("/handicraft");
	}
	
	@RequestMapping(value = "/category/healthcare", method = RequestMethod.GET)
	public ModelAndView goHealthcarePage(HttpServletRequest request) {
		
		return new ModelAndView("/healthcare");
	}
	
	@RequestMapping(value = "/category/hospitality", method = RequestMethod.GET)
	public ModelAndView goHospitalityPage(HttpServletRequest request) {
		
		return new ModelAndView("/hospitality");
	}
	
	@RequestMapping(value = "/category/logistics", method = RequestMethod.GET)
	public ModelAndView goLogisticsPage(HttpServletRequest request) {
		
		return new ModelAndView("/logistics");
	}
	
	@RequestMapping(value = "/goods/category", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void getTextileCatData(HttpServletRequest request, HttpServletResponse response)throws IOException,ServletException, JSONException{
		StringBuilder sb = new StringBuilder();
		JSONArray array = new JSONArray(); 
		List<NomineeDetails> u1 = null;
		//param=textile
		if(request.getParameter("param") != null) {
			String param = request.getParameter("param");
			if(param.equals("textile")) {
				String text = "Textile, textile articles, leather and related goods";
				u1 = nomineeservice.getNomineeByAwardSubCategory(text);
				  
			}
			else if(param.equals("agriculture")) {
				String text = "Agriculture, food processing and forestry � also includes vegetable products, vegetable fats, prepared foodstuffs and agri-tech";
				u1 = nomineeservice.getNomineeByAwardSubCategory(text);
				  
			}
			else if(param.equals("chemicals")) {
				String text = "Chemicals, pharma, bio and others";
				u1 = nomineeservice.getNomineeByAwardSubCategory(text);
				  
			}
			else if(param.equals("retailtrade")) {
				String text = "Retail Trade";
				u1 = nomineeservice.getNomineeByAwardSubCategory(text);
				  
			}
			else if(param.equals("renewableenergy")) {
				String text = "Renewable energy and waste management";
				u1 = nomineeservice.getNomineeByAwardSubCategory(text);
				  
			}
			else if(param.equals("handicraft")) {
				String text = "Handicraft";
				u1 = nomineeservice.getNomineeByAwardSubCategory(text);
				  
			}
			else if(param.equals("healthcare")) {
				String text = "Healthcare";
				u1 = nomineeservice.getNomineeByAwardSubCategory(text);
				  
			}
			else if(param.equals("hospitality")) {
				String text = "Hospitality, Tourism & Travel";
				u1 = nomineeservice.getNomineeByAwardSubCategory(text);
				  
			}
			else if(param.equals("logistics")) {
				String text = "Logistics, transports, e-commerce & other services";
				u1 = nomineeservice.getNomineeByAwardSubCategory(text);
				  
			}
			else {
				array = null;
			}
			for(int i=0;i<u1.size();i++)
		        try{
		        	JSONObject result = new JSONObject();
		        	result.put("id", u1.get(i).getId());
		        	result.put("contact",u1.get(i).getContact());
		        	result.put("awardCategory", u1.get(i).getAwardCategory());
		            result.put("nomineeName",u1.get(i).getNomineeName());
		            result.put("socialCategory",u1.get(i).getSocialCategory());
		            result.put("emailid", u1.get(i).getEmailId());
		            result.put("enterpriseName",u1.get(i).getEnterpriseName());
		            array.put(i, result);

		        }
		        catch(Exception ex){
		         ex.printStackTrace();
		        }
			 response.setContentType("application/json");
			 response.getWriter().write(array.toString());
			
		}
		/*BufferedReader br = request.getReader();
        String str = null;
        while ((str = br.readLine()) != null) {
            sb.append(str);
        }
        JSONObject jObj = new JSONObject(sb.toString());
        String name = jObj.getString("subcat");
		List<NomineeDetails> obj = nomineeservice.getNomineeByAwardSubCategory(name);
		Gson gson = new Gson();
		String result = gson.toJson(obj);
		response.getWriter().println(result);*/
		
		}
	
	/* All the new changes starts  here */ 
	
	/* To add Partners in admin dashboard */
	@RequestMapping(value = "/partnersection", method = RequestMethod.POST)
	public ModelAndView registerPartners(HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		
		Partners partners = new Partners();
		String state = "";
		
		User user = new User();  /* changes made by ashish*/
		
		try{
			partners.setPartnerName(request.getParameter("partnerName"));
			partners.setEmailId(request.getParameter("emailId"));
			partners.setContactNumber(request.getParameter("contactNumber"));
			partners.setAddress(request.getParameter("address"));
			//partners.setState(request.getParameter("addrState"));
			String password=CommonUtil.generateNomineePassword();
			partners.setPassword(password);
			
			
			String[] states = request.getParameterValues("addrState");
			for(String str : states){
				state += str+",";
			}
			state = state.substring(0, (state.length()-1));
			partners.setState(state);
			
			boolean f = partnersService.savePartners(partners);
			if(f){
				model.setViewName("redirect:/partnersection");
				model.addObject("successMsg", "Partner has been added.");
				
				user.setUsername(partners.getEmailId());
				user.setPassword(partners.getPassword());
				user.setSalt(partners.getSalt());
				user.setRole("REGIONAL");
				user.setFullName(partners.getPartnerName());
				user.setState(state);
				userservice.saveUser(user);
				new Thread(new Runnable() {
					@Override
					public void run() {
						try{
				SMSUtil sMSUtil=new SMSUtil();
				sMSUtil.sendSMS("This is your Password:- " + password , partners.getContactNumber());
			}catch(Exception e){
				System.out.println(e);
			}
		}
	}).start();
				Thread t1 = new Thread(new Runnable() {
					@Override
					public void run() {
						mailService.sendPasswordToPartnersMail(partners,password);
					}
				});
				t1.start();

			}else{
				model.setViewName("redirect:/partnersection");
				model.addObject("errorMsg", "Error! Please Try Again with diffrent entry");
			}
			return model;
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		model.setViewName("redirect:/partnersection");
		model.addObject("errorMsg","Something went wrong! Please try again.");		
		return model;
		
		
	}
	
	/* To update Partners in admin dashboard */
	@RequestMapping(path = "/editpartner/{id}", method = RequestMethod.GET)
	public ModelAndView getEditPartners(HttpServletRequest request, @PathVariable("id") String id) {
		ModelAndView model = new ModelAndView("editPartner");
		//String emailId = EncryptionUtil.decrypt(email, OtpSalt.SALT.toString());
		
		
		
		Partners partner = null;
		
		partner = partnersService.getPartnersById(id);
		model.addObject("partner", partner);
		return model;
		
	}
	
	/* To update Partners in admin dashboard */
	@RequestMapping(path = "/editpartner/{id}", method = RequestMethod.POST)
	public ModelAndView postEditPartners(@PathVariable("id") String id, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView model = new ModelAndView();
		Partners partner = new Partners();
		
		Partners upadatePartner = null;
		upadatePartner = partnersService.getPartnersById(id);
		
		String[] states = {};
		try {
			String partnerName = request.getParameter("partnerName");
			String contactNumber = request.getParameter("contactNumber");
			String address = request.getParameter("address");
			states = request.getParameterValues("addrState");
			String emailId = request.getParameter("emailId");
			String state = "";
			if(states != null) {
				for(String str : states){
					state += str+",";
				}
				state = state.substring(0, (state.length()-1));
				upadatePartner.setState(state);	
			}
				
			upadatePartner.setPartnerName(partnerName);
			upadatePartner.setContactNumber(contactNumber);
			upadatePartner.setAddress(address);
			boolean status = false;
			status = partnersService.updatePartners(upadatePartner);
			
			if(status){
				model.setViewName("redirect:/allpartners");
				model.addObject("successMsg", "Partner has been updated successfully.");
				
				User user = null;
				user = userservice.getUserByUsername(emailId);
				
				if(user !=null)
				{
					user.setUsername(emailId);
					//user.setPassword(partners.getPassword());
					//user.setSalt(partners.getSalt());
					user.setRole("REGIONAL");
					user.setFullName(partnerName);
					user.setState(upadatePartner.getState());
					userservice.updateUser(user);
				}

				
				
				
				
				
			}else{
				model.setViewName("redirect:/editpartner/"+id);
				model.addObject("errorMsg", "Error! Please Try Again with diffrent entry");
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}	
		return model;
		
	}

	@RequestMapping(path = "/editexpert", method = RequestMethod.POST)
	public ModelAndView postEditExpert(@PathVariable("id") String id, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView model = new ModelAndView();
		String val = request.getParameter("id");
		Expert expert = null;
		User user = null;
		try{
			
			expert = expertService.getExpertByEmail(request.getParameter("emailId"));
			user = userservice.getUserByUsername(request.getParameter("emailId"));
			
			if(expert != null && user != null) {
				
				expert.setExpertName(request.getParameter("expertName"));
				//expert.setEmailId(request.getParameter("emailId"));
				expert.setContactNumber(request.getParameter("contactNumber"));
				expert.setDesignation(request.getParameter("designation"));
				String specialization = request.getParameter("specialization");
				if(specialization != null && !specialization.equals("")){
					expert.setSpecialization(specialization);
				}
				
				String partnerWith = request.getParameter("partnerWith");
				if(partnerWith != null && !partnerWith.equals("")){
					expert.setPartnerWith(partnerWith);
				}
				expert.setAddress(request.getParameter("address"));
				//expert.setPassword(request.getParameter("password"));
				boolean status = false;
				status = expertService.updateExpert(expert);
				if(status){
					user.setUsername(expert.getEmailId());
					user.setPassword(expert.getPassword());
					user.setSalt(expert.getSalt());
					user.setRole("EXPERT");
					user.setFullName(expert.getExpertName());
					userservice.updateUser(user);
					
					model.setViewName("redirect:/allexperts");
					model.addObject("successMsg", "Expert has been updated.");
					return model;
				}
				else{
					model.setViewName("redirect:/editexpert?param="+val);
					model.addObject("errorMsg","Something went wrong! Please try again.");		
					return model;
				}
				
			
			}else {
				model.setViewName("redirect:/editexpert?param="+val);
				model.addObject("errorMsg","Something went wrong! Please try again.");		
				return model;
			}
				
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		/*model.setViewName("redirect:/updateexpertsection");
		model.addObject("errorMsg","Something went wrong! Please try again.");	*/	
		return model;
		
		
	}
	
	/* To update Expert in admin dashboard */
	@RequestMapping(path = "/editexpert", method = RequestMethod.GET)
	public ModelAndView getEditExpert(HttpServletRequest request) {
		ModelAndView model = new ModelAndView("editExpert");
		//String emailId = EncryptionUtil.decrypt(email, OtpSalt.SALT.toString());
		String val = request.getParameter("param");
		
		Expert expert = null;
		expert = expertService.getExpertById(val);
		model.addObject("expert", expert);
		return model;
		
	}
	
	
	@RequestMapping(value = "/category/misreport", method = RequestMethod.GET)
	public ModelAndView goWomenCatPage(HttpServletRequest request) throws JSONException {
		ModelAndView model = new ModelAndView("misreport");
		List<User> users = userservice.getUserByRole("REGIONAL");
		if(users!=null) {
			JSONObject jsonObj = new JSONObject();
			for(User user : users) {
				jsonObj.put(user.getUsername()+"-onelac", nomineeservice.getMISCount(user.getState(), "onelac").size());
				jsonObj.put(user.getUsername()+"-uptoten", nomineeservice.getMISCount(user.getState(), "uptoten").size());
				jsonObj.put(user.getUsername()+"-uptocrore", nomineeservice.getMISCount(user.getState(), "uptocrore").size());
				jsonObj.put(user.getUsername()+"-Entrepreneurship Development Institutes/Organisations", nomineeservice.getMISCount(user.getState(), "Entrepreneurship Development Institutes/Organisations").size());
				jsonObj.put(user.getUsername()+"-Mentor", nomineeservice.getMISCount(user.getState(), "Mentor").size());
				jsonObj.put(user.getUsername()+"-Incubator", nomineeservice.getMISCount(user.getState(), "Incubator").size());
				jsonObj.put(user.getUsername()+"-Promoters of Rural Producer Enterprises", nomineeservice.getMISCount(user.getState(), "Promoters of Rural Producer Enterprises").size());
			}
			model.addObject("data",jsonObj.toString());
		}
		List<String> distinctAwardSubCat = nomineeservice.getDistinctAwardSubCategory();
		model.addObject("distinctAwardSubCat",distinctAwardSubCat);
		return model;
	}
	
	
	@RequestMapping(value = "/category/sccat", method = RequestMethod.GET)
	public ModelAndView goSCCatPage(HttpServletRequest request) {
		
		return new ModelAndView("/sccat");
	}
	
	@RequestMapping(value = "/category/diffcat", method = RequestMethod.GET)
	public ModelAndView goDiffcultCatPage(HttpServletRequest request) {
		
		return new ModelAndView("/diffcat");
	}
	
	@RequestMapping(value = "/category/pwdcat", method = RequestMethod.GET)
	public ModelAndView goPWDCatPage(HttpServletRequest request) {
		
		return new ModelAndView("/pwdcat");
	}
	
	
	@RequestMapping(value = "/special/category", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void getWomenCatData(HttpServletRequest request, HttpServletResponse response)throws IOException,ServletException, JSONException{
		StringBuilder sb = new StringBuilder();
		BufferedReader br = request.getReader();
        String str = null;
        while ((str = br.readLine()) != null) {
            sb.append(str);
        }
        JSONObject jObj = new JSONObject(sb.toString());
        String name = jObj.getString("subcat");
		
		List<NomineeDetails> obj = nomineeservice.getNomineeBySpecialCategory(name);
		Gson gson = new Gson();
		String result = gson.toJson(obj);
		response.getWriter().println(result);
		}
	
	/* The new changes after taking pull */
	
	/* To get all the Regional data State Wise */
	
		@RequestMapping(value = "/neas/api/regionaltabledata", method = RequestMethod.POST)
			public void getRegionalJSON(HttpServletRequest request, HttpServletResponse respnose) throws IOException {
				
				String emailId = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
				User user = userservice.getUserByUsername(emailId);
				String[] states = user.getState().split(",");
				List<Nominator> u1 = nominatorservice.getNominatorByState(states);
				JSONArray array = new JSONArray();
				for (int i = 0; i < u1.size(); i++)
					try {
					JSONObject result = new JSONObject();
						result.put("id", u1.get(i).getId());
						result.put("view", "<a class='btn btn-primary' href='/view/nominator?p="+u1.get(i).getRegistrationNumber()+"'> View </a>");
						result.put("mail", "<a class='btn btn-info btnmail' data-toggle='modal' data-target='#myModal' data-value='"+u1.get(i).getEmailId()+"'> Mail </a>");
						result.put("registrationnumber", u1.get(i).getRegistrationNumber());
						result.put("fullname", u1.get(i).getFullName());
						result.put("role", u1.get(i).getRole());
						result.put("emailid", u1.get(i).getEmailId());
						result.put("contactnumber", u1.get(i).getContactNumber());
						result.put("occupation", u1.get(i).getOccupation());
						result.put("designation", u1.get(i).getDesignation());
						result.put("institutename", u1.get(i).getInstituteName());
						result.put("nominationtype", u1.get(i).getNominationType());
						result.put("addrline1", u1.get(i).getAddrLine1());
						result.put("addrline2", u1.get(i).getAddrLine2());
						result.put("addrcity", u1.get(i).getAddrCity());
						result.put("addrstate", u1.get(i).getAddrState());
						result.put("addrlandmark", u1.get(i).getAddrLandmark());
						result.put("addrpincode", u1.get(i).getAddrPinCode());
						array.put(i, result);
						//System.out.println(result);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
		
				respnose.setContentType("application/json");
				respnose.getWriter().write(array.toString());
				
		
			}
			
			/*  To get all the Regional data State Wise */
		
			@RequestMapping(value = "/neas/api/regionalnomineedata", method = RequestMethod.POST)
			public void nomineeRegionalJson(HttpServletRequest request, HttpServletResponse response)
					throws IOException, ServletException {
				String emailId = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
				User user = userservice.getUserByUsername(emailId);
				String[] state = user.getState().split(",");
				List<NomineeDetails> u1 = nomineeservice.getNomineeByState(state);
				JSONArray array = new JSONArray();
				for (int i = 0; i < u1.size(); i++)
					try {
						JSONObject result = new JSONObject();
						result.put("id", u1.get(i).getId());
						result.put("view", "<a class='btn btn-primary' href='/view/nominee?p="+u1.get(i).getContact()+"'> View </a>");
						if(u1.get(i).getAwardCategory().equals("ecosystem")) {
							result.put("assign", "<a class='btn btn-info btnassign2' data-toggle='modal' data-target='#assign-expert-ecosystem' data-value='"+u1.get(i).getEmailId()+"'> Assign </a>");
						}
						if(u1.get(i).getAwardCategory().equals("enterprise")) {
							result.put("assign", "<a class='btn btn-info btnassign' data-toggle='modal' data-target='#assign-expert' data-value='"+u1.get(i).getEmailId()+"'> Assign </a>");
						}
						result.put("registrationnumber", u1.get(i).getRegistrationNumber());
						result.put("nomineename", u1.get(i).getNomineeName());
						// result.put("role",u1.get(i).getRole());
						result.put("emailid", u1.get(i).getEmailId());
						result.put("contactnumber", u1.get(i).getContact());
						result.put("awardcategory", u1.get(i).getAwardCategory());
						result.put("enterprisecategory", u1.get(i).getEnterpriseCategory());
						result.put("awardsub", u1.get(i).getAwardSub());
						result.put("gender", u1.get(i).getGender());
						result.put("socialcategory", u1.get(i).getSocialCategory());
						result.put("dob", u1.get(i).getDob());
						result.put("speccatval", u1.get(i).getSpeccatval());
						result.put("enterprisename", u1.get(i).getEnterpriseName());
						result.put("address1", u1.get(i).getAddress1());
						result.put("address2", u1.get(i).getAddress2());
						result.put("city", u1.get(i).getCity());
						result.put("state", u1.get(i).getState());
						array.put(i, result);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				response.setContentType("application/json");
				response.getWriter().write(array.toString());
			}
			
			@RequestMapping(value = "/view/nominator", method = RequestMethod.GET)
			public ModelAndView viewNominatorWindow(HttpServletRequest request) {
				ModelAndView model = new ModelAndView();
				String reg = request.getParameter("p");
				Nominator nm = nominatorservice.getNominatorByRegistrationNumber(reg);
				model.addObject("nominatorr", nm);
				model.setViewName("viewnominator");
				return model;
			}
			
			@RequestMapping(value = "/view/nominee", method = RequestMethod.GET)
			public ModelAndView viewNomineeWindow(HttpServletRequest request) {
				ModelAndView model = new ModelAndView();
				String reg = request.getParameter("p");
				NomineeDetails nm = nomineeservice.getNomineeByContact(reg);
				model.addObject("nominee", nm);
				model.setViewName("viewnominee");
				SecondCatNominee secnom = secCatService.getNomineeByContact(nm.getContact());
				ThirdCatNominee thirdnom = thirdCatService.getNomineeByContact(nm.getContact());
				model.addObject("secnomcontact", EncryptionUtil.encrypt(nm.getContact(), OtpSalt.SALT.toString()));
				model.addObject("thirdnomcontact", EncryptionUtil.encrypt(nm.getContact(), OtpSalt.SALT.toString()));
				model.addObject("secnom", secnom);
				model.addObject("thirdnom", thirdnom);
				return model;
			}
			
			@RequestMapping(value = "/neas/api/support/aaction", method = RequestMethod.POST)
			public ModelAndView changeCheckAccept(HttpServletRequest request){
				ModelAndView model = new ModelAndView();
				nomineeservice.changeNomineeStatus(request.getParameter("emailId"), request.getParameter("remark"));
				NomineeDetails nominee =  nomineeservice.getNomineeByEmailId(request.getParameter("emailId"));
				String registrationNumber = nominee.getRegistrationNumber();
				Nominator nominator = nominatorservice.getNominatorByRegistrationNumber(registrationNumber);
				Thread t1 = new Thread(new Runnable() {
					@Override
					public void run() {
						mailService.sendNomineeSupportMail(nominee);
					}
				});
				t1.start();
				Thread t2 = new Thread(new Runnable() {
					@Override
					public void run() {
						mailService.sendNominatorSupportMail(nominator, nominee);
					}
				});
				t2.start();
				
				model.addObject("success", "Nominee has been Accepted");
				model.setViewName("");
				return model;
			}
			
			@RequestMapping(value = "/neas/api/support/daction", method = RequestMethod.POST)
			public ModelAndView changeCheckDecline(HttpServletRequest request){
				ModelAndView model = new ModelAndView();
				nomineeservice.changeNomineeStatusRemark(request.getParameter("emailId"), request.getParameter("remark"));
				NomineeDetails nominee =  nomineeservice.getNomineeByEmailId(request.getParameter("emailId"));
				String registrationNumber = nominee.getRegistrationNumber();
				Nominator nominator = nominatorservice.getNominatorByRegistrationNumber(registrationNumber);
				Thread t1 = new Thread(new Runnable() {
					@Override
					public void run() {
						mailService.sendNomineeSupportMail(nominee);
					}
				});
				t1.start();
				Thread t2 = new Thread(new Runnable() {
					@Override
					public void run() {
						mailService.sendNominatorSupportMail(nominator, nominee);
					}
				});
				t2.start();
				model.addObject("success", "Nominee has been Declined");
				model.setViewName("");
				return model;
			}
			
			@RequestMapping(value = "/neas/api/support/paction", method = RequestMethod.POST)
			public ModelAndView changeCheckPending(HttpServletRequest request){
				ModelAndView model = new ModelAndView();
				nomineeservice.changeNomineeStatusPending(request.getParameter("emailId"), request.getParameter("remark"));
				model.addObject("success", "Nominee has been Declined");
				model.setViewName("");
				return model;
			}
			
			@RequestMapping(value = "/neas/api/support/acceptcount", method = RequestMethod.POST)
			public void supportAcceptCount(HttpServletResponse response)throws IOException,ServletException{
				Long nominee = (long) nomineeservice.getNomineeAcceptedCount().size();
				Map<String,String> map = new HashMap<>();
				map.put("count",String.valueOf(nominee));
				Gson gson = new Gson();
				String result = gson.toJson(map);
				response.getWriter().println(result);
			}
			
			@RequestMapping(value = "/neas/api/support/pendingcount", method = RequestMethod.POST)
			public void supportPendingCount(HttpServletResponse response)throws IOException,ServletException{
				Long nominee = (long) nomineeservice.getNomineePendingCount().size();
				Map<String,String> map = new HashMap<>();
				map.put("count",String.valueOf(nominee));
				Gson gson = new Gson();
				String result = gson.toJson(map);
				response.getWriter().println(result);
				
				/*ModelAndView model = new ModelAndView();
				model.addObject("pendingcount", nomineeservice.getNomineePendingCount());
				return model;*/
			}
			
			@RequestMapping(value = "/neas/api/support/declinecount", method = RequestMethod.POST)
			public void supportDeclineCount(HttpServletResponse response)throws IOException,ServletException{
				Long nominee = (long) nomineeservice.getNomineeDeclinedCount().size();
				Map<String,String> map = new HashMap<>();
				map.put("count",String.valueOf(nominee));
				Gson gson = new Gson();
				String result = gson.toJson(map);
				response.getWriter().println(result);
			}
				
			
			@RequestMapping(value = "/neas/api/specialcatcount", method = RequestMethod.POST)
			public void specialCategoryCount(HttpServletResponse response)throws IOException,ServletException{
				List<NomineeDetails> nomineeList = nomineeservice.getAllSpecialCat();
				JSONObject obj = new JSONObject();
				int wc=0,sc=0,dc=0,pc=0;
				for(NomineeDetails n : nomineeList) {
					if(n.getSpeccatval().contains("Difficult")) {
						++dc;
					}
					if(n.getSpeccatval().contains("Women")) {
						++wc;
					}
					if(n.getSpeccatval().contains("PwD")) {
						++pc;
					}
					if(n.getSpeccatval().contains("SC/ST")) {
						++sc;
					}
				}
				try {
				obj.put("women", wc);
				obj.put("scst", sc);
				obj.put("pwd", pc);
				obj.put("diff", dc);
				}catch(Exception e) {
					System.out.println(e);
				}
				response.getWriter().println(obj.toString());

			}
			
			@RequestMapping(value = "/neas/api/regionalnominee", method = RequestMethod.POST)
			public void nomineeRegionalDataAdmin(HttpServletRequest request, HttpServletResponse response)
					throws IOException, ServletException {
				/*String emailId = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
				User user = userservice.getUserByUsername(emailId);*/
				String getstate = request.getParameter("s");
				String[] state = getstate.split(",");
				List<NomineeDetails> u1 = nomineeservice.getNomineeByState(state);
				JSONArray array = new JSONArray();
				for (int i = 0; i < u1.size(); i++)
					try {
						JSONObject result = new JSONObject();
						result.put("id", u1.get(i).getId());
						result.put("view", "<a class='btn btn-primary' href='/view/nominee?p="+u1.get(i).getContact()+"'> view </a>");
						result.put("registrationnumber", u1.get(i).getRegistrationNumber());
						result.put("nomineename", u1.get(i).getNomineeName());
						// result.put("role",u1.get(i).getRole());
						result.put("emailid", u1.get(i).getEmailId());
						result.put("contactnumber", u1.get(i).getContact());
						result.put("awardcategory", u1.get(i).getAwardCategory());
						result.put("enterprisecategory", u1.get(i).getEnterpriseCategory());
						result.put("awardsub", u1.get(i).getAwardSub());
						result.put("gender", u1.get(i).getGender());
						result.put("socialcategory", u1.get(i).getSocialCategory());
						result.put("dob", u1.get(i).getDob());
						result.put("speccatval", u1.get(i).getSpeccatval());
						result.put("enterprisename", u1.get(i).getEnterpriseName());
						result.put("address1", u1.get(i).getAddress1());
						result.put("address2", u1.get(i).getAddress2());
						result.put("city", u1.get(i).getCity());
						result.put("state", u1.get(i).getState());
						array.put(i, result);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				response.setContentType("application/json");
				response.getWriter().write(array.toString());
			}
			
			@RequestMapping(value = "/neas/api/support/mail", method = RequestMethod.POST)
			public ModelAndView supportMailNominee(HttpServletRequest request) {
				ModelAndView model = new ModelAndView();
				String mailto = request.getParameter("mailTo");
				String subject = request.getParameter("subject");
				String msg = request.getParameter("msg");
				Nominator nm = nominatorservice.getNominatorByEmailId(mailto);
				Thread t = new Thread(new Runnable() {
					@Override
					public void run() {
						mailService.sendRemindSupportNominator(mailto, subject, msg, nm);
					}
				});
				t.start();
				return model;
			}
			
			@RequestMapping(value = "/neas/api/get/searchdata", method = RequestMethod.GET)
			public void getSearchData(HttpServletRequest request, HttpServletResponse response) throws IOException, JSONException {
				String state = request.getParameter("state");
				String awardcat = request.getParameter("awardcat");
				String awardsub = request.getParameter("awardsub");
				if(state.equals("") && awardcat.equals("") && awardsub.equals("")) {
					response.setContentType("application/json");
					response.getWriter().write(new JSONObject().put("status", "false").toString());
				}else {
					JSONObject obj = new JSONObject();
					int ol=0,tl=0,oc=0,en=0,me=0,in=0,ru=0;
					int textile=0,agri=0,checmical=0,retail=0,renewable=0,handicraft=0,health=0,hospital=0,logistic=0,engg=0;
					List<NomineeDetails> nomineeList = nomineeservice.getMISReport(state,awardcat,awardsub);
					for(NomineeDetails n : nomineeList) {
						if(n.getEnterpriseCategory().contains("onelac")) {
							++ol;
						}
						if(n.getEnterpriseCategory().contains("uptoten")) {
							++tl;
						}
						if(n.getEnterpriseCategory().contains("uptocrore")) {
							++oc;
						}
						if(n.getEnterpriseCategory().contains("Mentor")) {
							++me;
						}
						if(n.getEnterpriseCategory().contains("Promoters of Rural Producer Enterprises")) {
							++ru;
						}
						if(n.getEnterpriseCategory().contains("Entrepreneurship Development Institutes/Organisations")) {
							++en;
						}
						if(n.getEnterpriseCategory().contains("Incubator")) {
							++in;
						}
						if(n.getAwardSub()!=null) {
						switch(n.getAwardSub()) {
							case "Textile, textile articles, leather and related goods": ++textile;break;
							case "Agriculture, food processing and forestry � also includes vegetable products, vegetable fats, prepared foodstuffs and agri-tech": ++agri;break;
							case "Chemicals, pharma, bio and others": ++checmical;break;
							case "Retail Trade": ++retail;break;
							case "Renewable energy and waste management": ++renewable;break;
							case "Handicrafts": ++handicraft;break;
							case "Healthcare": ++health;break;
							case "Hospitality, Tourism & Travel": ++hospital;break;
							case "Logistics, transports, e-commerce & other services": ++logistic;break;
							case "Engineering systems, base metals & articles, machinery & mechanical appliances and medical devices": ++engg;break;
						}
						}
					}
					try {
						obj.put("Count", nomineeList.size());
						obj.put("Initial Investment upto 1 lakh", ol);
						obj.put("Initial Investment between 1 lakh and 10 lakh", tl);
						obj.put("Initial Investment between 10 lakh and 1 Crore", oc);
						obj.put("Entrepreneurship Development Institutes/Organisations", en);
						obj.put("Mentor", me);
						obj.put("Incubator", in);
						obj.put("Promoters of Rural Producer Enterprises", ru);
						obj.put("Textile, textile articles, leather and related goods", textile);
						obj.put("Agriculture, food processing and forestry � also includes vegetable products, vegetable fats, prepared foodstuffs and agri-tech", agri);
						obj.put("Chemicals, pharma, bio and others", checmical);
						obj.put("Retail Trade", retail);
						obj.put("Renewable energy and waste management", renewable);
						obj.put("Handicrafts", handicraft);
						obj.put("Healthcare", health);
						obj.put("Hospitality, Tourism & Travel", hospital);
						obj.put("Logistics, transports, e-commerce & other services", logistic);
						obj.put("Engineering systems, base metals & articles, machinery & mechanical appliances and medical devices", engg);
						}catch(Exception e) {
							System.out.println(e);
						}
						response.setContentType("application/json");
						response.getWriter().println(obj.toString());
				}
				
			}
			
			@RequestMapping(value = "/neas/api/expertnomineedata", method = RequestMethod.POST)
			public void nomineeExpertDataAdmin(HttpServletRequest request, HttpServletResponse response)
					throws IOException, ServletException {
				String emailId = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
				List<NomineeDetails> u1 = evaluationService.getNomineeByExpertAssigned(emailId);
				JSONArray array = new JSONArray();
				for (int i = 0; i < u1.size(); i++)
					try {
						Evaluation eval = evaluationService.getEvaluationByNominee(u1.get(i).getEmailId());
						JSONObject result = new JSONObject();
						result.put("id", u1.get(i).getId());
						result.put("view", "<a class='btn btn-primary' href='/view/nominee?p="+u1.get(i).getContact()+"'> View </a>");
						result.put("registrationnumber", u1.get(i).getRegistrationNumber());
						result.put("nomineename", u1.get(i).getNomineeName());
						result.put("download","<a class='btn btn-success' href='/createzip/fieldvisit/"+
								EncryptionUtil.encrypt(u1.get(i).getContact(), OtpSalt.SALT.toString()) +"'> Download </a>");
						int marks = 0;
						if(eval.getExpert1().equals(emailId)){
							marks = eval.getMarks1();
						}else if(eval.getExpert2().equals(emailId) && u1.get(i).getAwardCategory().equals("enterprise")){
							marks = eval.getMarks2();
						}else if(eval.getExpert3().equals(emailId) && u1.get(i).getAwardCategory().equals("enterprise")){
							marks = eval.getMarks3();
						}
						
						if(marks != 0){
							result.put("score",marks);
						}else{
							result.put("score","-");
						}
						result.put("scoreupdate","<a class='btn btn-info btnscore' data-toggle='modal' data-target='#edit-score' data-value='"+u1.get(i).getEmailId()+"'> Edit Score </a>");
						result.put("emailid", u1.get(i).getEmailId());
						result.put("contactnumber", u1.get(i).getContact());
						result.put("awardcategory", u1.get(i).getAwardCategory());
						result.put("enterprisecategory", u1.get(i).getEnterpriseCategory());
						result.put("awardsub", u1.get(i).getAwardSub());
						result.put("gender", u1.get(i).getGender());
						result.put("socialcategory", u1.get(i).getSocialCategory());
						result.put("dob", u1.get(i).getDob());
						result.put("speccatval", u1.get(i).getSpeccatval());
						result.put("enterprisename", u1.get(i).getEnterpriseName());
						result.put("address1", u1.get(i).getAddress1());
						result.put("address2", u1.get(i).getAddress2());
						result.put("city", u1.get(i).getCity());
						result.put("state", u1.get(i).getState());
						array.put(i, result);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				response.setContentType("application/json");
				response.getWriter().write(array.toString());
			}
			
	@RequestMapping(value = "/upload/shortlisted", method = RequestMethod.GET)
	public ModelAndView getShortlistUpload(HttpServletRequest request) throws JSONException {
		ModelAndView model = new ModelAndView("uploadshortlisted");
		return model;
	}
	
	@RequestMapping(value = "/upload/show/shortlisted", method = RequestMethod.GET)
	public ModelAndView viewShortlistUpload(HttpServletRequest request,HttpServletResponse response) throws JSONException {
		ModelAndView model = new ModelAndView("viewshortlisted");		
		return model;
	}
	
	@RequestMapping(value = "/neas/api/upload/show/shortlisted", method = RequestMethod.POST)
	public void shortlistUploadJson(HttpServletRequest request,HttpServletResponse response) throws JSONException, IOException {
		List<NomineeDetails> u1 = nomineeservice.getAllShortlistedNominee();
		List<SecondCatNominee> secNom = secCatService.getAllSecCatNom();
		List<ThirdCatNominee> thirdNom = thirdCatService.getAllThirdCatNom();
		JSONArray array = new JSONArray();
		for (int i = 0; i < u1.size(); i++)
			try {
				JSONObject result = new JSONObject();
				result.put("id", u1.get(i).getId());
				result.put("view", "<a class='btn btn-primary' href='/view/nominee?p="+u1.get(i).getContact()+"'> View </a>");
				if(u1.get(i).getShortlisted()!=null){
					result.put("shortliststatus", u1.get(i).getShortlisted());
				}else{
					result.put("shortliststatus", "");
				}
				if(u1.get(i).getFormula()!=null){
					result.put("score", u1.get(i).getFormula());
				}else{
					result.put("score", "");
				}
				if(u1.get(i).getScoreUpdate()!=null){
					result.put("scoreupdated", u1.get(i).getScoreUpdate());
				}else{
					result.put("scoreupdated", "");
				}
				result.put("registrationnumber", u1.get(i).getRegistrationNumber());
				result.put("nomineename", u1.get(i).getNomineeName());
				result.put("emailid", u1.get(i).getEmailId());
				result.put("contactnumber", u1.get(i).getContact());
				result.put("awardcategory", u1.get(i).getAwardCategory());
				result.put("enterprisecategory", u1.get(i).getEnterpriseCategory());
				result.put("state", u1.get(i).getState());
				result.put("socialcategory", u1.get(i).getSocialCategory());
				boolean s = false;
	              for(SecondCatNominee scn:secNom){
	            	  if(scn.getContact().equals(u1.get(i).getContact())){
	            		  result.put("fileuploaded", "Yes");
	            		  s = true;
	            		  break;
	            	  }
	              }
	              for(ThirdCatNominee scn:thirdNom){
	            	  if(scn.getContact().equals(u1.get(i).getContact())){
	            		  result.put("fileuploaded", "Yes");
	            		  s = true;
	            		  break;
	            	  }
	              }
	              if(!s){result.put("fileuploaded", "No");}
				array.put(i, result);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		response.setContentType("application/json");
		response.getWriter().write(array.toString());
	}
		
	@RequestMapping(value = "/neas/api/upload/shortlisted", method = RequestMethod.POST)
	public ModelAndView getShortlistUploadPost(HttpServletRequest request, HttpServletResponse response,@RequestParam("uploadfile") MultipartFile shortlisted) throws IOException {
		ModelAndView model=new ModelAndView("uploadshortlisted");
		try(Workbook workbook = new XSSFWorkbook(shortlisted.getInputStream())){
		if (shortlisted.getSize() > 0) {
				Sheet sheet = workbook.getSheetAt(0);
				int emailColNum = 0,contactColNum = 1,scoreColNum = 2,shortColNum = 3;
				Row row = sheet.getRow(0);
				row.createCell(row.getPhysicalNumberOfCells()).setCellValue("Status");
				row.createCell(row.getPhysicalNumberOfCells()+1).setCellValue("Reason for Not Update");
				for(int i=1;i<=sheet.getPhysicalNumberOfRows()-2;i++){
					Row dataRow = sheet.getRow(i);
					String query="SELECT n FROM NomineeDetails n WHERE n.contact=:contact and n.emailId=:emailId";
					Map<String, Object> map = new HashMap<>();
					if(dataRow.getCell(emailColNum).getStringCellValue().equals("")){
						dataRow.createCell(dataRow.getPhysicalNumberOfCells()).setCellValue("Not Updated");
						dataRow.createCell(dataRow.getPhysicalNumberOfCells()+1).setCellValue("Email Id Not Given");
						continue;
					}
					if(dataRow.getCell(contactColNum).getStringCellValue().equals("")){
						dataRow.createCell(dataRow.getPhysicalNumberOfCells()).setCellValue("Not Updated");
						dataRow.createCell(dataRow.getPhysicalNumberOfCells()+1).setCellValue("Contact Not Given");
						continue;
					}
					map.put("emailId", dataRow.getCell(emailColNum).getStringCellValue());
					map.put("contact", dataRow.getCell(contactColNum).getStringCellValue());
					NomineeDetails nominee = null;
					try{
						nominee = nomineeservice.getResultByQuery(query,map).get(0);
					}catch(Exception e){
						dataRow.createCell(dataRow.getPhysicalNumberOfCells()).setCellValue("Not Updated");
						dataRow.createCell(dataRow.getPhysicalNumberOfCells()+1).setCellValue(e.getMessage());
						continue;
					}
					if(nominee!=null){
						if(dataRow.getCell(scoreColNum).getNumericCellValue()>=0.0 && dataRow.getCell(scoreColNum).getNumericCellValue()<=100.0)
						{
							nominee.setFormula(String.valueOf(dataRow.getCell(scoreColNum).getNumericCellValue()));
						}else{
							dataRow.createCell(dataRow.getPhysicalNumberOfCells()).setCellValue("Not Updated");
							dataRow.createCell(dataRow.getPhysicalNumberOfCells()+1).setCellValue("Score is not between 0 to 100");
							continue;
						}
						switch((int)dataRow.getCell(shortColNum).getNumericCellValue()){
							case 1: nominee.setShortlisted("Shortlisted");break;
							case 9: nominee.setShortlisted("Not Shortlisted");break;
							case 0: nominee.setShortlisted("Pending");break;
						}
						nominee.setScoreUpdate(new Date());
						if(nomineeservice.updateNominee(nominee)){
							dataRow.createCell(dataRow.getPhysicalNumberOfCells()).setCellValue("Updated");
						}else{
							dataRow.createCell(dataRow.getPhysicalNumberOfCells()).setCellValue("Not Updated");
							dataRow.createCell(dataRow.getPhysicalNumberOfCells()+1).setCellValue("Data was unable to update in DB");
							System.out.println("Shortlisted Email ID not updated in the DB: "+sheet.getRow(i).getCell(emailColNum).getStringCellValue());
							model.addObject("errorMsg","File has been uploaded successfully!");
						}
					}else{
						dataRow.createCell(dataRow.getLastCellNum()+1).setCellValue("Not Updated");
						dataRow.createCell(dataRow.getPhysicalNumberOfCells()+1).setCellValue("Email ID not found in DB/Data was unable to update in DB");
						System.out.println("Shortlisted Email ID not found in the DB: "+sheet.getRow(i).getCell(emailColNum).getStringCellValue());
					}
				}	
			model.addObject("successMsg","File has been uploaded successfully!");
			response.setContentType("text/plain");
			response.setHeader("Content-disposition", "attachment; filename=NomineeDetails.xlsx");
			workbook.write(response.getOutputStream());
		}
		}catch(Exception e){
			model.addObject("errorMsg","File Uploaded failed due to following reason: "+e.getMessage());
		}
		return model;
	}
	
	@PostMapping(value = "/neas/api/upload/fieldvisit/{encryptedPhone}")
	public ModelAndView saveSecondNominee(@PathVariable("encryptedPhone") String encryptedPhone,@RequestParam("fieldvisitfile") MultipartFile[] visitFiles,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView model = new ModelAndView();
		model.setViewName("redirect:/userdata");
		String UPLOAD_FILEPATH = env.getRequiredProperty("static.fieldvisit.directory");
		String contactNumber = EncryptionUtil.decrypt(encryptedPhone, OtpSalt.SALT.toString());
		NomineeDetails nominee = nomineeservice.getNomineeByContact(contactNumber);
		JSONObject jsonRes = new JSONObject();
		
		if(nominee!=null){
			if (visitFiles[0].getSize() > 0) {
				for (int i = 0; i < visitFiles.length; i++) {
					StringBuilder filePath = new StringBuilder(UPLOAD_FILEPATH + File.separator + contactNumber);
					File file = new File(filePath.toString());
					if (!file.exists()) {
						file.mkdirs();
					}
					String originalName = visitFiles[i].getOriginalFilename();
					if(!originalName.endsWith(".doc") && !originalName.endsWith(".docx") && !originalName.endsWith(".xlsx") && !originalName.endsWith(".xls") && !originalName.endsWith(".pdf")){
						model.addObject("status", "false");
						model.addObject("reason", "Invalid File! Only PDF, MS-Word or MS-Excel file is allowed");
						return model;
					}
					File actFile = new File(filePath.append(File.separator + originalName).toString());
					Files.copy(visitFiles[i].getInputStream(), actFile.toPath(),StandardCopyOption.REPLACE_EXISTING);
					nominee.setFileName("/static/fieldvisit/"+contactNumber+"/"+originalName);
				}
				if(nomineeservice.updateNominee(nominee)){
					model.addObject("status", "true");
					model.addObject("reason", "File Uploaded Successfully!");
				}
			}else{
				model.addObject("status", "false");
				model.addObject("reason", "Invalid File or File not uploaded");
			}
		}else{
			model.addObject("status", "false");
			model.addObject("reason", "Nominee Details not matching with the DB");
		}
		
		return model;
		
	}
	
	/*  To get all the Regional data State Wise */
	
	@RequestMapping(value = "/neas/api/regionalnomineeshortlist", method = RequestMethod.POST)
	public void nomineeRegionalShortlistJson(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		String emailId = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
		User user = userservice.getUserByUsername(emailId);
		String[] state = user.getState().split(",");
		List<NomineeDetails> u1 = nomineeservice.getNomineeRegionalShortlist(state);
		JSONArray array = new JSONArray();
		DecimalFormat df = new DecimalFormat("#.00"); 
		for (int i = 0; i < u1.size(); i++)
			try {
				JSONObject result = new JSONObject();
				result.put("id", u1.get(i).getId());
				result.put("view", "<a class='btn btn-primary' href='/view/nominee?p="+u1.get(i).getContact()+"'> View </a>");
				if(u1.get(i).getAwardCategory().equals("ecosystem")) {
					result.put("assign", "<a class='btn btn-info btnassign2' data-toggle='modal' data-target='#assign-expert-ecosystem' data-value='"+u1.get(i).getEmailId()+"'> Assign </a>");
				}
				if(u1.get(i).getAwardCategory().equals("enterprise")) {
					result.put("assign", "<a class='btn btn-info btnassign' data-toggle='modal' data-target='#assign-expert' data-value='"+u1.get(i).getEmailId()+"'> Assign </a>");
				}
				String filename = "";
				if(u1.get(i).getFileName()!=null && !u1.get(i).getFileName().equals("")){
					filename = "<a class='btn btn-link' href='/createzip/fieldvisit/"+EncryptionUtil.encrypt(u1.get(i).getContact(), OtpSalt.SALT.toString())
					+"'>"+u1.get(i).getFileName().substring(u1.get(i).getFileName().lastIndexOf("/")+1)+"</a>";
				}
				
				result.put("uploadfile", filename+"<a class='btn btn-info btnupload' data-toggle='modal' data-target='#upload-file' data-value='"
							+EncryptionUtil.encrypt(u1.get(i).getContact(), OtpSalt.SALT.toString())+"' data-email='"+u1.get(i).getEmailId()+"'> Upload </a>");
				
				Evaluation eval = evaluationService.getNomineeByEmailId(u1.get(i).getEmailId());
				
				if(eval!=null){
					if(eval.getExpert1()!=null && !eval.getExpert1().equals("")){
						String color="",status="";
						if(eval.getMarks1()==0){
							color = "red";
							status = "Pending";
						}else{
							color = "green";
							status = "Completed";
						}
						result.put("expert1",expertService.getExpertByEmail(eval.getExpert1()).getExpertName()+"  -  "+status);
					}else{
						result.put("expert1", "-");
					}
					if(u1.get(i).getAwardCategory().equals("enterprise")) {
					if(eval.getExpert2()!=null && !eval.getExpert2().equals("")){
						String color="",status="";
						if(eval.getMarks2()==0){
							color = "red";
							status = "Pending";
						}else{
							color = "green";
							status = "Completed";
						}
						result.put("expert2", expertService.getExpertByEmail(eval.getExpert2()).getExpertName()+"  -  "+status);
					}else{
						result.put("expert2", "-");
					}
					if(eval.getExpert3()!=null && !eval.getExpert3().equals("")){
						String color="",status="";
						if(eval.getMarks3()==0){
							color = "red";
							status = "Pending";
						}else{
							color = "green";
							status = "Completed";
						}
						result.put("expert3", expertService.getExpertByEmail(eval.getExpert3()).getExpertName()+"  -  "+status);
					}else{
						result.put("expert3", "-");
					}
					}
					
					if(u1.get(i).getAwardCategory().equals("enterprise")){
						if(eval.getMarks1()==0 || eval.getMarks2()==0 || eval.getMarks3()==0){
							result.put("avgscore", "-");
						}else{
							result.put("avgscore", df.format((eval.getMarks1()+eval.getMarks2()+eval.getMarks3())/3.0));
						}
					}else{
						if(eval.getMarks1()==0){
							result.put("avgscore", "-");
						}else{
							result.put("avgscore", eval.getMarks1());
						}
					}
					
					
				}else{
					result.put("expert1", "-");
					result.put("expert2", "-");
					result.put("expert3", "-");
					result.put("avgscore", "-");
				}
				result.put("promote", "<a class='btn btn-info editscore' data-toggle='modal' data-target='#editscore' data-value='"+u1.get(i).getEmailId()+"'> Promote/Rank </a>");
				if(u1.get(i).getPromoteStatus() != null && !u1.get(i).getPromoteStatus().equals("")) {
					result.put("promotestatus", u1.get(i).getPromoteStatus());
				}
				else {
					result.put("promotestatus", "-");
				}
				if(u1.get(i).getRanking() != null && !u1.get(i).getRanking().equals("")) {
					result.put("rank", u1.get(i).getRanking());
				}
				else {
					result.put("rank", "-");
				}
				result.put("registrationnumber", u1.get(i).getRegistrationNumber());
				result.put("nomineename", u1.get(i).getNomineeName());
				result.put("emailid", u1.get(i).getEmailId());
				result.put("contactnumber", u1.get(i).getContact());
				result.put("awardcategory", u1.get(i).getAwardCategory());
				result.put("enterprisecategory", u1.get(i).getEnterpriseCategory());
				result.put("awardsub", u1.get(i).getAwardSub());
				result.put("gender", u1.get(i).getGender());
				result.put("socialcategory", u1.get(i).getSocialCategory());
				result.put("dob", u1.get(i).getDob());
				result.put("speccatval", u1.get(i).getSpeccatval());
				result.put("enterprisename", u1.get(i).getEnterpriseName());
				result.put("address1", u1.get(i).getAddress1());
				result.put("address2", u1.get(i).getAddress2());
				result.put("city", u1.get(i).getCity());
				result.put("state", u1.get(i).getState());
				array.put(i, result);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		response.setContentType("application/json");
		response.getWriter().write(array.toString());
	}
	
	
	@RequestMapping(value = "/createzip/fieldvisit/{encryptedPhone}", method = RequestMethod.GET)
	public void createNomineeExcel(@PathVariable("encryptedPhone") String encryptedPhone, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String contact = EncryptionUtil.decrypt(encryptedPhone, OtpSalt.SALT.toString());
		response.setContentType("APPLICATION/OCTET-STREAM");
		response.setHeader("Content-disposition", "attachment; filename="+contact+".zip");
		NomineeDetails secnom = nomineeservice.getNomineeByContact(contact);
		List <String> fileList = new ArrayList < String > ();
		String SOURCE_FOLDER = "";
		String OUTPUT_ZIP_FILE = "";
		if(secnom!=null) {
			SOURCE_FOLDER = env.getProperty("static.fieldvisit.directory")+File.separator+contact;
			OUTPUT_ZIP_FILE = env.getProperty("static.fieldvisit.directory")+File.separator+contact+".zip";
			File basePath = new File(SOURCE_FOLDER);
			generateFileList(SOURCE_FOLDER,basePath,fileList);
			zipIt(OUTPUT_ZIP_FILE,fileList,SOURCE_FOLDER);
		}
		
		OutputStream out = response.getOutputStream();
        FileInputStream in = new FileInputStream(OUTPUT_ZIP_FILE);
        byte[] buffer = new byte[4096];
        int length;
        while ((length = in.read(buffer)) > 0){
           out.write(buffer, 0, length);
        }
        in.close();
        out.flush();
		
	}


	public void generateFileList(String SOURCE_FOLDER,File node,List <String> fileList) {
        if (node.isFile()) {
            fileList.add(generateZipEntry(SOURCE_FOLDER,node.toString()));
        }

        if (node.isDirectory()) {
            String[] subNote = node.list();
            for (String filename: subNote) {
                generateFileList(SOURCE_FOLDER,new File(node, filename),fileList);
            }
        }
    }

    private String generateZipEntry(String SOURCE_FOLDER,String file) {
        return file.substring(SOURCE_FOLDER.length() + 1, file.length());
    }
	
    public void zipIt(String zipFile,List <String> fileList,String SOURCE_FOLDER) {
        byte[] buffer = new byte[1024];
        String source = new File(SOURCE_FOLDER).getName();
        FileOutputStream fos = null;
        ZipOutputStream zos = null;
        try {
            fos = new FileOutputStream(zipFile);
            zos = new ZipOutputStream(fos);

            System.out.println("Output to Zip : " + zipFile);
            FileInputStream in = null;

            for (String file: fileList) {
                System.out.println("File Added : " + file);
                ZipEntry ze = new ZipEntry(source + File.separator + file);
                zos.putNextEntry(ze);
                try {
                    in = new FileInputStream(SOURCE_FOLDER + File.separator + file);
                    int len;
                    while ((len = in .read(buffer)) > 0) {
                        zos.write(buffer, 0, len);
                    }
                } finally {
                    in.close();
                }
            }

            zos.closeEntry();
            System.out.println("Folder successfully compressed");

        } catch (IOException ex) {
        	System.out.println("Exception while zipping");
            ex.printStackTrace();
        } finally {
            try {
                zos.close();
            } catch (IOException e) {
            	System.out.println("Exception while zipping");
                e.printStackTrace();
            }
        }
    }
    
    
    @RequestMapping(value = "/neas/api/expert/editscore", method = RequestMethod.POST)
	public ModelAndView editScore(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
    	ModelAndView model = new ModelAndView("redirect:/userdata");
    	String emailId = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
    	try{
    	String email = request.getParameter("nomemail"); 
    	String score = request.getParameter("nomscore"); 
    	
    	Evaluation eval = evaluationService.getEvaluationByNominee(email);
    	if(eval.getExpert1().equals(emailId)){
    		eval.setMarks1(Integer.parseInt(score));
    	}else if(eval.getExpert2().equals(emailId)){
    		eval.setMarks2(Integer.parseInt(score));
    	}else if(eval.getExpert3().equals(emailId)){
    		eval.setMarks3(Integer.parseInt(score));
    	}else{
    		throw new RuntimeException("Not able to set marks for emailId: "+email+" by evaluator: "+emailId);
    	}
    	
    	
    	if(evaluationService.updateEvaluationDeatails(eval)){
    		model.addObject("status", "true");
    		model.addObject("reason", "Score Updated Successfully");
    	}
    	
    	}catch(Exception e){
    		model.addObject("status", "false");
    		model.addObject("reason", e.getMessage());
    	}
    	return model;
    }

    @RequestMapping(value = "/neas/api/promoted/data", method = RequestMethod.POST)
	public void promotedNomineeJson(HttpServletRequest request,HttpServletResponse response) throws JSONException, IOException {
		List<NomineeDetails> u1 = nomineeservice.getAllPromotedNominee();
		JSONArray array = new JSONArray();
		for (int i = 0; i < u1.size(); i++)
			try {
				int j = i+1;
				JSONObject result = new JSONObject();
				result.put("id", j);
				result.put("nomineename", u1.get(i).getNomineeName());
				result.put("emailid", u1.get(i).getEmailId());
				result.put("rank", u1.get(i).getRanking());
				result.put("contactnumber", u1.get(i).getContact());
				result.put("awardcategory", u1.get(i).getAwardCategory());
				result.put("enterprisecategory", u1.get(i).getEnterpriseCategory());
				result.put("state", u1.get(i).getState());
				String filename = "";
				if(u1.get(i).getFileName()!=null && !u1.get(i).getFileName().equals("")){
					filename = "<a class='btn btn-link' href='/createzip/fieldvisit/"+EncryptionUtil.encrypt(u1.get(i).getContact(), OtpSalt.SALT.toString())
					+"'>"+u1.get(i).getFileName().substring(u1.get(i).getFileName().lastIndexOf("/")+1)+"</a>";
				}
				result.put("file", filename);
				array.put(i, result);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		response.setContentType("application/json");
		response.getWriter().write(array.toString());
	}
    
    @RequestMapping(value = "/createzip/fieldvisit/allpromoted", method = RequestMethod.GET)
	public void promotedNomineeAllZip( HttpServletRequest request, HttpServletResponse response) throws Exception {
		List<NomineeDetails> nomList = nomineeservice.getAllPromotedNomineeFiles();
		response.setContentType("APPLICATION/OCTET-STREAM");
		response.setHeader("Content-disposition", "attachment; filename=fieldvisit.zip");
		String OUTPUT_ZIP_FILE = "";
		String SOURCE_FOLDER = env.getProperty("static.fieldvisit.directory");
		List <String> fileList = new ArrayList < String > ();
		OUTPUT_ZIP_FILE = env.getProperty("static.fieldvisit.directory")+File.separator+"fieldvisit.zip";
		for (int i = 0; i < nomList.size(); i++) {
			String phone = nomList.get(i).getContact();
		
		if(nomList!=null) {
			String SOURCE_FOLD = env.getProperty("static.fieldvisit.directory")+File.separator+phone;
			File basePath = new File(SOURCE_FOLD);
			generatePromotedFileList(SOURCE_FOLD,basePath,fileList);
		}
		}
		zipItPromote(OUTPUT_ZIP_FILE,fileList,SOURCE_FOLDER);
		OutputStream out = response.getOutputStream();
        FileInputStream in = new FileInputStream(OUTPUT_ZIP_FILE);
        byte[] buffer = new byte[4096];
        int length;
        while ((length = in.read(buffer)) > 0){
           out.write(buffer, 0, length);
        }
        in.close();
        out.flush();
        
		
	}
    public void generatePromotedFileList(String SOURCE_FOLDER,File node,List <String> fileList) {
        if (node.isFile()) {
            fileList.add(generateZipEntryPromote(SOURCE_FOLDER,node.toString()));
        }

        if (node.isDirectory()) {
            String[] subNote = node.list();
            for (String filename: subNote) {
            	generatePromotedFileList(SOURCE_FOLDER,new File(node, filename),fileList);
            }
        }
    }
    
    private String generateZipEntryPromote(String SOURCE_FOLDER,String file) {
        return file.substring(SOURCE_FOLDER.lastIndexOf("\\") + 1, file.length());
    }
    
    public void zipItPromote(String zipFile,List <String> fileList,String SOURCE_FOLDER) {
        byte[] buffer = new byte[1024];
        String source = new File(SOURCE_FOLDER).getName();
        FileOutputStream fos = null;
        ZipOutputStream zos = null;
        int i = 0;
        try {
            fos = new FileOutputStream(zipFile);
            zos = new ZipOutputStream(fos);

            System.out.println("Output to Zip : " + zipFile);
            FileInputStream in = null;

            for (String file: fileList) {
                System.out.println("File Added : " + file);
                ZipEntry ze = new ZipEntry(source + File.separator + file);
                zos.putNextEntry(ze);
                try {
                    in = new FileInputStream(SOURCE_FOLDER + File.separator + file);
                    int len;
                    while ((len = in .read(buffer)) > 0) {
                        zos.write(buffer, 0, len);
                    }
                } finally {
                    in.close();
                }
            }

            	zos.closeEntry();
            
            System.out.println("Folder successfully compressed");

        } catch (IOException ex) {
        	System.out.println("Exception while zipping");
            ex.printStackTrace();
        } finally {
            try {
                zos.close();
            } catch (IOException e) {
            	System.out.println("Exception while zipping");
                e.printStackTrace();
            }
        }
    }
}