package com.xlri.awards.config.security;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.xlri.awards.common.EncryptionUtil;
import com.xlri.awards.dao.entity.User;
import com.xlri.awards.dao.service.api.UserService;
import com.xlri.awards.dao.service.impl.UserDetailService;

@Component
public class ApplicationAuthenticationProvider implements AuthenticationProvider {

	private static final Logger LOG = LoggerFactory.getLogger(ApplicationAuthenticationProvider.class);
	
	@Autowired
    UserDetailService userDetailService;
	
	@Autowired
	UserService userServce;
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		UsernamePasswordAuthenticationToken auth = (UsernamePasswordAuthenticationToken) authentication;
		String username = String.valueOf(auth.getPrincipal());
        String password = String.valueOf(auth.getCredentials());
        User user = this.userServce.getUserByUsername(username);
        if(user.getRole().equals("NOMINEE") || user.getRole().equals("NOMINATOR")){
        	return null;
        }
        
        String actPass = null;
        if(user!=null){
        	actPass = EncryptionUtil.encryptPass(password + user.getSalt());
        }
        UserDetails userDetails = null;
        try {
            userDetails = this.userDetailService.loadUserByUsername(username);
        }
        catch(UsernameNotFoundException ex){
        	LOG.error("Cannot find user with this username: "+ username);
        }
        if(userDetails != null) {
        	
            if (userDetails.getUsername().equals(username) && userDetails.getPassword().equals(actPass)) {
                Collection<? extends GrantedAuthority> authorities = userDetails.getAuthorities();
                Authentication authenticated = new UsernamePasswordAuthenticationToken(username, user.getFullName(), authorities);
                return authenticated;
            }
        }
        return null;
	}

	@Override
	public boolean supports(Class<?> aClass) {
		return true;
	}

}
