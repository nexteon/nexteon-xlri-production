<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<%@include file="fragments/headerlibs.jsp" %>

<body class="innerpage">

<%@include file="fragments/header.jsp" %>
<!-- Body Starts Here -->



<main role="main">
	<div class="page-title">
		<h2>Welcome, ${fullName}</h2>
	</div>
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="whitebg">
						<div class="col-md-12">
							<h4>Dear ${fullName},</h4> 
							<c:if test="${nominator.nominationType == 'individual'}">
							<p>We thank you and appreciate your efforts to be a nominator for NEA 2018. As an individual nominator, you are entitled to file a maximum of 5 nominations in the 'Enterprise Award Track' and a maximum of 2 nominations in the 'Ecosystem Builders Track'. Please note that once a nomination is submitted, you cannot make any changes in your nomination. The summary of the total nominations filed by you under both the tracks can be seen below, whereas, the registration number mentioned in the middle is the unique number assigned to you which will also be prefixed to each of the nominations filed by you.</p>
							</c:if>
							<c:if test="${nominator.nominationType == 'organisation'}">
							<p>We thank you and appreciate your efforts to be a nominator for NEA 2018. As an Intitution/Organisation nominator, you are entitled to file a maximum of 10 nominations in the 'Enterprise Award Track' and a maximum of 4 nominations in the 'Ecosystem Builders Track'. Please note that once a nomination is submitted, you cannot make any changes in your nomination. The summary of the total nominations filed by you under both the tracks can be seen below, whereas, the registration number mentioned in the middle is the unique number assigned to you which will also be prefixed to each of the nominations filed by you.</p>
							</c:if>
						</div>
						<div class="card reg col-md-4">
							<div>
								<h3>${entCount}</h3>
								<p class="title">Enterprise Award Track Nomination</p>
							</div>
						</div>
						<div class="card reg col-md-4">
							<div>
								<h3>${registrationNumber}</h3>
								<p class="title">Nominator Registration Number</p>
							</div>
						</div>
						<div class="card reg col-md-4">
							<div>
								<h3>${ecoCount}</h3>
								<p class="title">Ecosystem Builders Track Nomination</p>
							</div>
						</div>
						<input type="hidden" value="${registrationNumber}" name="regCheck" id="regCheck"/>
						<div class="row">
						<input type="hidden" value="${entCountLeft}" name="entCountLeft" id="entCountLeft"/>
						<input type="hidden" value="${ecoCountLeft}" name="ecoCountLeft" id="ecoCountLeft"/>
						<div class="clearfix"></div>
                            <br>
						<div class="col-md-12" style="text-align:center">
						<c:choose>
						<c:when test="${pendingStatus == true}">
							<c:set value="disabled" var="disabled"/>
							<c:set value="javascript:void(0);" var="link"/>
						</c:when>
						<c:otherwise>
							<c:set value="" var="disabled"/>
							<c:set value="/basicdetails" var="link"/>
						</c:otherwise>
						</c:choose>
						<a class="btn btn-primary btncls proceed-nomination" href="${link}" ${disabled}>Proceed for the Nomination</a>
						</div>
						<c:if test="${countLeft <= 0}">
						<div class="clearfix"></div>
                            <br> <br>
							<div class="col-md-12">
								<div class="alertmsg alert alert-warning">You've reached the limit of adding Nominee with this user</div>
							</div>
						</c:if>
						<div class="give-padding"></div>
						<%-- <c:if test="${not empty nomineeData}"> --%>
						<div class="col-md-12">
							<div class="table-responsive">
							<table class="table table-bordered" id="nomination-table">
								<tr>
									<th colspan="8">Summary of all the Nomination</th>
								</tr>
								<tr>
									<th>Award Track</th>
									<th>Award Category</th>
									<th>Award Sub Category</th>
									<th>Enterprise Name</th>
									<th>Nominee Name</th>
									<th>Email Id</th>
									<th>Contact Number</th>
									<th>Status</th>
								</tr>
								
								<c:forEach items="${nomineeData}" var="nominee" varStatus="theCount">
									<tr>
										<td>
											<c:if test="${nominee.awardCategory == 'enterprise'}">
												<c:out value="Enterprise Award Track"/>
											</c:if>
											<c:if test="${nominee.awardCategory == 'ecosystem'}">
												<c:out value="Ecosystem Builders Track"/>
											</c:if>
										</td>
										<td>
											<c:if test="${nominee.enterpriseCategory == 'onelac'}">
												<c:out value="Initial Investment upto 1 Lakh"/>
											</c:if>
											<c:if test="${nominee.enterpriseCategory == 'uptoten'}">
												<c:out value="Initial Investment between 1 Lakh to 10 Lakh"/>
											</c:if>
											<c:if test="${nominee.enterpriseCategory == 'uptocrore'}">
												<c:out value="Initial Investment between 10 Lakh to 1 Crore"/>
											</c:if>
											<c:if test="${nominee.awardCategory == 'ecosystem'}">
												${nominee.enterpriseCategory}
											</c:if>
										</td>
										<td>${nominee.awardSub}</td>
										<td>${nominee.enterpriseName}</td>
										<td>${nominee.nomineeName}</td>
										<td><c:out value="${fn:replace(nominee.emailId, '(Pending)','')}"/></td>
										<td><c:out value="${fn:replace(nominee.contact, '(Pending)','')}"/></td>
										<td>Completed</td>
									</tr>
								</c:forEach>
								
							</table>
						</div>
						</div>
						<%-- </c:if> --%>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<c:if test="${(not empty errorMsg) or (not empty successMsg)}">
  <div class="modal fade" id="errorModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <c:if test="${ not empty errorMsg }">
          	<h4 class="modal-title">Error</h4>
          </c:if>
          <c:if test="${ not empty successMsg }">
          	<h4 class="modal-title">Success</h4>
          </c:if>
        </div>
        <div class="modal-body">
          <p>
          	<c:if test="${ not empty errorMsg }">
          		${ errorMsg }
          </c:if>
          <c:if test="${ not empty successMsg }">
          		${ successMsg }
          </c:if>
          </p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
</c:if>
<!-- Body Ends Here -->
<%@include file="fragments/footer.jsp" %>
<script>
$(document).ready(function(){
$('.alertmsg').hide();
var value = $('#entCountLeft').val();
var value1 = $('#ecoCountLeft').val();
var regGlobal;
if(value<=0 && value1<=0)
    {
    	$('.alertmsg').show();
        $('.btncls').addClass('disabled');
    }
else
    {
		$('.alertmsg').hide();
    } 


if(localStorage.getItem("awardbaseformData")!=null && localStorage.getItem("awardbaseformData")!='undefined'){
	
	var partA = JSON.parse(localStorage.getItem("awardbaseformData"))["partA"];
	var partB = JSON.parse(localStorage.getItem("awardbaseformData"))["partB"];
	var status = JSON.parse(localStorage.getItem("awardbaseformData"))["status"];
	var tableData = "<tr>";
	if(status == 'Pending'){
		$('.proceed-nomination').attr("disabled","disabled");
		
		if(partA != 'undefined'){
			var pairs, i, keyValuePair, key, value, map = {};
            var query = JSON.parse(localStorage.getItem("awardbaseformData"))["partA"];
            pairs = query.split('&');
            var pairsplt= pairs[0].split('=');
            var neareg = pairsplt[1];
            var regChck = document.getElementById("regCheck");
            var regChk = regChck.value;
            regGlobal = neareg;
            if(regChk == neareg){
            for (i = 0; i < pairs.length; i += 1) {
                keyValuePair = pairs[i].split('=');
                key = decodeURIComponent(keyValuePair[0]);
                value = (keyValuePair.length > 1) ? decodeURIComponent(keyValuePair[1]) : '';
                value.replace('%20',' ');
                if(key === 'awardCategory' && value === 'enterprise'){
                	tableData += '<td>Enterprise Award Track</td>';
                }
                if(key === 'awardCategory' && value === 'ecosystem'){
                	tableData += '<td>Ecosystem Builders Track</td>';
                }
                if(key === 'enterpriseCategory' && value === 'onelac'){
                	tableData += '<td>Initial Investment upto 1 Lakh</td>';
                }
                if(key === 'enterpriseCategory' && value === 'uptoten'){
                	tableData += '<td>Initial Investment between 1 Lakh to 10 Lakh</td>';
                }
                if(key === 'enterpriseCategory' && value === 'uptocrore'){
                	tableData += '<td>Initial Investment between 10 Lakh to 1 Crore</td>';
                }
                if(key === 'awardsub1'){
                	tableData += '<td>'+value+'</td>';
                }
                if(key === 'enterpriseName'){
                	tableData += '<td>'+value+'</td>';
                }
                if(key === 'nomineeName'){
                	tableData += '<td>'+value+'</td>';
                }
                if(key === 'emailId'){
                	tableData += '<td>'+value+'</td>';
                }
                if(key === 'contactNumber'){
                	tableData += '<td>'+value+'</td>';
                }
                	
            }
            tableData += '<td><a href="/basicdetails">'+status+'</a></td>';
            tableData += "</tr>";
            $('#nomination-table').append(tableData);
            }
		}
		
		else if(partB != 'undefined'){
			var formtype = JSON.parse(localStorage.getItem("awardbaseformData"))["formtype"];
			var pairs, i, keyValuePair, key, value, map = {};
            var query = JSON.parse(localStorage.getItem("awardbaseformData"))["partA"];
            pairs = query.split('&');
            for (i = 0; i < pairs.length; i += 1) {
                keyValuePair = pairs[i].split('=');
                key = decodeURIComponent(keyValuePair[0]);
                value = (keyValuePair.length > 1) ? decodeURIComponent(keyValuePair[1]) : '';
                value.replace('%20',' ');
                if(key === 'awardCategory' && value === 'enterprise'){
                	tableData += '<td>Enterprise Award Track</td>';
                }
                if(key === 'awardCategory' && value === 'ecosystem'){
                	tableData += '<td>Ecosystem Builders Track</td>';
                }
                if(key === 'enterpriseCategory' && value === 'onelac'){
                	tableData += '<td>Initial Investment upto 1 Lakh</td>';
                }
                if(key === 'enterpriseCategory' && value === 'uptoten'){
                	tableData += '<td>Initial Investment between 1 Lakh to 10 Lakh</td>';
                }
                if(key === 'enterpriseCategory' && value === 'uptocrore'){
                	tableData += '<td>Initial Investment between 10 Lakh to 1 Crore</td>';
                }
                if(key === 'awardsub1'||key === 'awardsub2'||key === 'awardsub3'){
                	tableData += '<td>'+value+'</td>';
                }
                if(key === 'enterpriseName'){
                	tableData += '<td>'+value+'</td>';
                }
                if(key === 'nomineeName'){
                	tableData += '<td>'+value+'</td>';
                }
                if(key === 'emailId'){
                	tableData += '<td>'+value+'</td>';
                }
                if(key === 'contactNumber'){
                	tableData += '<td>'+value+'</td>';
                }
            }
			if(formtype == 'first'){
				tableData += '<td><a href="/openfirstcat">'+status+'</a></td>';
			}else if(formtype == 'second'){
				tableData += '<td><a href="/openseccat">'+status+'</a></td>';
			}else if(formtype == 'third'){
				tableData += '<td><a href="/openthirdcat">'+status+'</a></td>';
			}
			$('#nomination-table').append(tableData);
		
		}
	}else{
		$('.proceed-nomination').attr("disabled","");
		$('.proceed-nomination').attr("href","/basicdetails");
	}
	}
	
	
if(localStorage.getItem("ecosystembaseformdata")!=null && localStorage.getItem("ecosystembaseformdata")!='undefined'){
	var partA = JSON.parse(localStorage.getItem("ecosystembaseformdata"))["partA"];
	var partB = JSON.parse(localStorage.getItem("ecosystembaseformdata"))["partB"];
	var status = JSON.parse(localStorage.getItem("ecosystembaseformdata"))["status"];
	var tableData = "<tr>";
	if(status == 'Pending'){
		$('.proceed-nomination').attr("disabled","disabled");
		$('.proceed-nomination').attr("href","");
		if(partA != 'undefined'){
			var pairs, i, keyValuePair, key, value, map = {};
            var query = JSON.parse(localStorage.getItem("ecosystembaseformdata"))["partA"];
            pairs = query.split('&');
            for (i = 0; i < pairs.length; i += 1) {
                keyValuePair = pairs[i].split('=');
                key = decodeURIComponent(keyValuePair[0]);
                value = (keyValuePair.length > 1) ? decodeURIComponent(keyValuePair[1]) : '';
                value.replace('%20',' ');
                if(key === 'awardCategory' && value === 'enterprise'){
                	tableData += '<td>Enterprise Award Track</td>';
                }
                if(key === 'awardCategory' && value === 'ecosystem'){
                	tableData += '<td>Ecosystem Builders Track</td><td></td><td></td>';
                }
                if(key === 'enterpriseCategory' && value === 'onelac'){
                	tableData += '<td>Initial Investment upto 1 Lakh</td>';
                }
                if(key === 'enterpriseCategory' && value === 'uptoten'){
                	tableData += '<td>Initial Investment between 1 Lakh to 10 Lakh</td>';
                }
                if(key === 'enterpriseCategory' && value === 'uptocrore'){
                	tableData += '<td>Initial Investment between 10 Lakh to 1 Crore</td>';
                }
                if(key === 'awardsub1'){
                	tableData += '<td></td>';
                }
                if(key === 'enterpriseName'){
                	tableData += '<td>'+value+'</td>';
                }
                if(key === 'nomineeName'){
                	tableData += '<td>'+value+'</td>';
                }
                if(key === 'emailId'){
                	tableData += '<td>'+value+'</td>';
                }
                if(key === 'contactNumber'){
                	tableData += '<td>'+value+'</td>';
                }
                	
            }
            tableData += '<td><a href="/basicdetails">'+status+'</a></td>';
            tableData += "</tr>";
            $('#nomination-table').append(tableData);
		}else if(partB != 'undefined'){
			var formtype = JSON.parse(localStorage.getItem("ecosystembaseformdata"))["formtype"];
			var pairs, i, keyValuePair, key, value, map = {};
            var query = JSON.parse(localStorage.getItem("ecosystembaseformdata"))["partA"];
            pairs = query.split('&');
            for (i = 0; i < pairs.length; i += 1) {
                keyValuePair = pairs[i].split('=');
                key = decodeURIComponent(keyValuePair[0]);
                value = (keyValuePair.length > 1) ? decodeURIComponent(keyValuePair[1]) : '';
                value.replace('%20',' ');
                if(key === 'awardCategory' && value === 'enterprise'){
                	tableData += '<td>Enterprise Award Track</td>';
                }
                if(key === 'awardCategory' && value === 'ecosystem'){
                	tableData += '<td>Ecosystem Builders Track</td>';
                }
                if(key === 'enterpriseCategory' && value === 'onelac'){
                	tableData += '<td>Initial Investment upto 1 Lakh</td>';
                }
                if(key === 'enterpriseCategory' && value === 'uptoten'){
                	tableData += '<td>Initial Investment between 1 Lakh to 10 Lakh</td>';
                }
                if(key === 'enterpriseCategory' && value === 'uptocrore'){
                	tableData += '<td>Initial Investment between 10 Lakh to 1 Crore</td>';
                }
                if(key === 'awardsub1'||key === 'awardsub2'||key === 'awardsub3'){
                	tableData += '<td>'+value+'</td>';
                }
                if(key === 'enterpriseName'){
                	tableData += '<td>'+value+'</td>';
                }
                if(key === 'nomineeName'){
                	tableData += '<td>'+value+'</td>';
                }
                if(key === 'emailId'){
                	tableData += '<td>'+value+'</td>';
                }
                if(key === 'contactNumber'){
                	tableData += '<td>'+value+'</td>';
                }
            }
			if(formtype == 'first'){
				tableData += '<td><a href="/openfirstcat">'+status+'</a></td>';
			}else if(formtype == 'second'){
				tableData += '<td><a href="/openseccat">'+status+'</a></td>';
			}else if(formtype == 'third'){
				tableData += '<td><a href="/openthirdcat">'+status+'</a></td>';
			}
			$('#nomination-table').append(tableData);
		
		}
	}else{
		var regChck = document.getElementById("regCheck");
        var regChk = regChck.value;
        if(regChk == regGlobal){
			$('.proceed-nomination').attr("disabled","");
			$('.proceed-nomination').attr("href","/basicdetails");
        }
	}
	}
var regChck = document.getElementById("regCheck");
var regChk = regChck.value;
if(regChk == regGlobal){
	$('.proceed-nomination').attr("href","");
	$('.proceed-nomination').attr("disabled","disabled");
}
else{
	$('.proceed-nomination').removeAttr("disabled")
	//$('.proceed-nomination').prop('disabled', false);
}
});
</script>
</body>
</html>