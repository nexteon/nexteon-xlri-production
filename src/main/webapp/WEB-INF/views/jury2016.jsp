
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
   <%@include file="fragments/headerlibs.jsp" %>
   <body class="innerpage">
      <%@include file="fragments/header.jsp" %>
      <!-- Body Starts Here -->
      <main role="main">
         <div class="page-title">
            <h2>NEA JURY 2016</h2>
         </div>
         <div class="content">
            <div class="container">
               <div class="row">
                  <div class="col-md-12">
                     <div class="whitebg">
                        <div class="row">
                           <div class="column">
                              <div class="card">
                                 <img src="/static/images/jury/2016/jury1.jpg" alt="Jury 1">
                                 <div>
                                    <h2>Ms.Naina Lal Kidwai</h2>
                                    <p class="title">Retired Chairman of HSBC Bank</p>
                                 </div>
                              </div>
                           </div>
                           <div class="column">
                              <div class="card">
                                 <img src="/static/images/jury/2016/jury2.jpg" alt="Jury 2">
                                 <div>
                                    <h2>Mr.T.V.Mohandas Pai</h2>
                                    <p class="title">Chairman of Manipal Global Education Services</p>
                                 </div>
                              </div>
                           </div>
                           <div class="column">
                              <div class="card">
                                 <img src="/static/images/jury/2016/jury3.jpg" alt="Jury 3">
                                 <div>
                                    <h2>Dr. Raghunath Anant Mashelkar</h2>
                                    <p class="title">Former Director General,CSIR</p>
                                 </div>
                              </div>
                           </div>
                           <div class="column">
                              <div class="card">
                                 <img src="/static/images/jury/2016/jury4.jpg" alt="Jury 4">
                                 <div>
                                    <h2>Maharaj Kishan Bhan</h2>
                                    <p class="title">Former Secretary, Dept. of Biotechnology, Ministry of Science & Technology,Govt. of India</p>
                                 </div>
                              </div>
                           </div>
                           <div class="column">
                              <div class="card">
                                 <img src="/static/images/jury/2016/jury5.jpg" alt="Jury 5">
                                 <div>
                                    <h2>Dr.Bindeshwar Pathak</h2>
                                    <p class="title">Founder of Sulabh International</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         </div>
      </main>
      <!-- Body Ends Here -->
      <%@include file="fragments/footer.jsp" %>
   </body>
</html>