
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <%@include file="fragments/headerlibs.jsp" %>
    <body class="innerpage">
        
        <%@include file="fragments/header.jsp" %>
        <!-- Body Starts Here -->
        
        <main role="main">
            <div class="page-title">
                <h2>Welcome, ${nominee.nomineeName }</h2>
            </div>
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="whitebg">
                                <div class="row">
                                <c:if test="${empty secnom && empty thirdnom}">
                                    <div class="col-md-12">
                                        <h4>Dear ${nominee.nomineeName},</h4><br/>
                                        <p>Thank you for participating in NEA 2018 and congratulations on reaching to the data submission stage. You have been nominated for NEA 2018 by ${nominatorName.fullName} (${nominee.registrationNumber}) under following details:</p>
                                    </div>
                                 </c:if>
                                 <c:if test="${not empty secnom || not empty thirdnom}">
                                    <div class="col-md-12">
                                        <h4>Congratulations ${nominee.nomineeName}!</h4><br/>
                                        <p>You have successfully completed the data submission round and your application now will go through the next stage that is; screening and multi-stage evaluation process. Following is the summary table of your application -</p>
                                    </div>
                           	      </c:if>
                                </div>
                                <div class="row">
                                    <table class="table table-responsive">
                                        <tbody>
                                            <tr>
                                                <th>Award Track</th>
                                                <td>
                                                    <c:if test="${nominee.awardCategory =='enterprise'}">
                                                        Enterprise Award Category
                                                    </c:if>
                                                    <c:if test="${nominee.awardCategory =='ecosystem'}">
                                                        Ecosystem Builder's Category
                                                    </c:if>
                                                </td>
                                            </tr>
                                            <c:if test="${nominee.awardCategory =='enterprise'}">
                                            <tr>
                                                <th>Award Category</th>
                                                <td>
                                                    <c:if test="${nominee.enterpriseCategory =='onelac'}">
                                                        Initial Investment upto 1 Lakh
                                                    </c:if>
                                                    <c:if test="${nominee.enterpriseCategory =='uptoten'}">
                                                        Initial Investment between 1 Lakh  to 10 Lakh
                                                    </c:if>
                                                    <c:if test="${nominee.enterpriseCategory =='uptocrore'}">
                                                        Initial Investment between 10 Lakh  to 1 Crore
                                                    </c:if>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <th>Award Sub Category</th>
                                                <td>${nominee.awardSub}</td>
                                            </tr>
                                            <tr>
                                                <th>Enterprise Name</th>
                                                <td>${nominee.enterpriseName}</td>
                                            </tr>
                                            <c:if test="${not empty secnom}">
	                                            <tr>
	                                                <th>Form number</th>
	                                                <td>${secnom.formNumber}</td>
	                                            </tr>
                                                <tr>
                                                    <th>Bussiness Formation</th>
                                                    <td>${secnom.businessFormation}</td>
                                                </tr>
                                                <tr>
                                                    <th>MoA Files Uploaded</th>
                                                    <td id="moapath"></td>
                                                </tr>
                                                <tr>
                                                    <th>AoA Files Uploaded</th>
                                                    <td id="aoapath"></td>
                                                </tr>
                                                <tr>
                                                    <th>Organization Registration Certificate</th>
                                                    <td id="orgpath"></td>
                                                </tr>
                                                <tr>
                                                    <th>Industry certificate</th>
                                                    <td id="indpath"></td>
                                                </tr>
                                            </c:if>
                                            <c:if test="${not empty thirdnom}">
	                                            <tr>
	                                                <th>Form number</th>
	                                                <td>${thirdnom.formNumber}</td>
	                                            </tr>
                                                <tr>
                                                    <th>Date of Enterprise's Incorporation</th>
                                                    <td>${thirdnom.enterpriseDate}</td>
                                                </tr>
                                                <tr>
                                                    <th>Balance Sheet</th>
                                                    <td id="balancepath"></td>
                                                </tr>
                                                <tr>
                                                    <th>MoA Files Uploaded</th>
                                                    <td id="moapath"></td>
                                                </tr>
                                                <tr>
                                                    <th>AoA Files Uploaded</th>
                                                    <td id="aoapath"></td>
                                                </tr>
                                                <tr>
                                                    <th>Organization Registration Certificate</th>
                                                    <td id="orgpath"></td>
                                                </tr>
                                                <tr>
                                                    <th>Industry certificate</th>
                                                    <td id="indpath"></td>
                                                </tr>
                                                <tr>
                                                    <th>Profit & Loss Statement</th>
                                                    <td id="profitpath"></td>
                                                </tr>
                                                <tr>
                                                    <th>Cash Flow Statement</th>
                                                    <td id="cashpath"></td>
                                                </tr>
                                            </c:if>
                                            </c:if>
                                            <c:if test="${nominee.awardCategory =='ecosystem'}">
                                            	<tr>
                                                    <th>Nominee's Institution's Name</th>
                                                    <td>${nominee.enterpriseName}</td>
                                                </tr>
                                                <tr>
                                                    <th>Nominee's Name</th>
                                                    <td>${nominee.nomineeName}</td>
                                                </tr>
                                                <tr>
                                                    <th>Email ID</th>
                                                    <td>${nominee.emailId}</td>
                                                </tr>
                                                <tr>
                                                    <th>Contact Number</th>
                                                    <td>${nominee.contact}</td>
                                                </tr>
                                                <tr>
                                                    <th>Please describe the role the Nominee is playing as an Eco-System Contributor.</th>
                                                    <td>${nominee.roleNominee}</td>
                                                </tr>
                                                <tr>
                                                    <th>Please describe the impact the Nominee has created by playing their role as Mentor/Incubation Center/Entrepreneurship Development Institutes.</th>
                                                    <td>${nominee.impactNominee}</td>
                                                </tr>
                                                <tr>
                                                    <th>Please mention about the recognitions/awards the Nominee has received in the area of Entrepreneurship development.</th>
                                                    <td>${nominee.recognitionNominee}</td>
                                                </tr>
                                                <tr>
                                                    <th>Please mention about industry collaborations, Road shows/Bussiness Plan Competitions/Funding Events organised.</th>
                                                    <td>${nominee.collaborationNominee}</td>
                                                </tr>
                                                <tr>
                                                    <th>Any other special mentions?</th>
                                                    <td>${nominee.specMentionNominee}</td>
                                                </tr>
                                                <tr>
                                                    <th>Ecosystem Builder's Track</th>
                                                    <td>${nominee.enterpriseCategory}</td>
                                                </tr>
                                            </c:if>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <c:if test="${nominee.enterpriseCategory == 'uptoten' && empty secnom}">
                                            <a class="btn btn-primary" href="/secondcatreg/${ encryptedID }">Submit Basic Financial & Operational Information</a>
                                        </c:if>
                                        <c:if test="${nominee.enterpriseCategory == 'uptocrore' && empty thirdnom}">
                                            <a class="btn btn-primary" href="/thirdcatreg/${ encryptedID }">Submit Basic Financial & Operational Information</a>
                                        </c:if>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        
        
        
        <c:if test="${(not empty errorMsg)}">
            <div class="modal fade" id="errorModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Error</h4>
                        </div>
                        <div class="modal-body">
                            <p>
                                ${ errorMsg }
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    
                </div>
            </div>
            
        </c:if>
        
        <c:if test="${(not empty successMsg)}">
            <div class="modal fade" id="successModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Success</h4>
                        </div>
                        <div class="modal-body">
                            <p>
                                ${ successMsg }
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    
                </div>
            </div>
        </c:if>
        
        
        <!-- Body Ends Here -->
        <%@include file="fragments/footer.jsp" %>
        <script>
            $(document).ready(function(){
                if($('#successModal').length > 0){
                    $('#successModal').modal('show');
                }
            });
            $(document).ready(function(){
                if($('#errorModal').length > 0){
                    $('#errorModal').modal('show');
                }
            });
            <c:if test="${not empty secnom}">
            $(document).ready(function(){
                <c:if test="${not empty secnom.moaFileUpload}">
                    var moaJson = ${secnom.moaFileUpload};
                var moaHtml="<ul>"
                for (var key in moaJson) {
                	var x = 0;
                    if (moaJson.hasOwnProperty(key)) {
                    	var uripath= moaJson[key];
                    	var filename = uripath.substring(uripath.lastIndexOf('/')+1);
                        moaHtml+="<li><a href='"+moaJson[key]+"' target='_blank' rel='noopener noreferrer'>"+filename+"</a></li>";    
                    }
                }
                moaHtml+="</ul>";
                $("#moapath").html(moaHtml);
    			</c:if>
            <c:if test="${not empty secnom.aoaFileUpload}">
                var aoaJson = ${secnom.aoaFileUpload};
            var aoaHtml="<ul>"
            for (var key in aoaJson) {
            	var x = 0;
                if (aoaJson.hasOwnProperty(key)) {
                	var uripath= aoaJson[key];
                	var filename = uripath.substring(uripath.lastIndexOf('/')+1);
                    aoaHtml+="<li><a href='"+aoaJson[key]+"' target='_blank' rel='noopener noreferrer'>"+filename+"</a></li>";    
                }
            }
            aoaHtml+="</ul>";
            $("#aoapath").html(aoaHtml);
    </c:if>
    <c:if test="${not empty secnom.orgFileUpload}">
        var orgJson = ${secnom.orgFileUpload};
            var orgHtml="<ul>"
            for (var key in orgJson) {
            	var x = 0;
                if (orgJson.hasOwnProperty(key)) {
                	var uripath= orgJson[key];
                	var filename = uripath.substring(uripath.lastIndexOf('/')+1);
                    orgHtml+="<li><a href='"+orgJson[key]+"' target='_blank' rel='noopener noreferrer'>"+filename+"</a></li>";   
                }
            }
            orgHtml+="</ul>";
            $("#orgpath").html(orgHtml);
    </c:if>
    <c:if test="${not empty secnom.indFileUpload}">
        var indJson = ${secnom.indFileUpload};
            var indHtml="<ul>"
            for (var key in indJson) {
            	var x = 0;
                if (indJson.hasOwnProperty(key)) {
                	var uripath= indJson[key];
                	var filename = uripath.substring(uripath.lastIndexOf('/')+1);
                    indHtml+="<li><a href='"+indJson[key]+"' target='_blank' rel='noopener noreferrer'>"+filename+"</a></li>";   
                }
            }
            indHtml+="</ul>";
            $("#indpath").html(indHtml);
    </c:if>	
        });
    </c:if>
    
    
    <c:if test="${not empty thirdnom}">
    $(document).ready(function(){
        <c:if test="${not empty thirdnom.moaDoc}">
            var moaJson = ${thirdnom.moaDoc};
        var moaHtml="<ul>"
        for (var key in moaJson) {
        	var x = 0;
            if (moaJson.hasOwnProperty(key)) {
            	var uripath= moaJson[key];
            	var filename = uripath.substring(uripath.lastIndexOf('/')+1);
                moaHtml+="<li><a href='"+moaJson[key]+"' target='_blank' rel='noopener noreferrer'>"+filename+"</a></li>";    
            }
        }
        moaHtml+="</ul>";
        $("#moapath").html(moaHtml);
		</c:if>
    <c:if test="${not empty thirdnom.aoaDoc}">
        var aoaJson = ${thirdnom.aoaDoc};
    var aoaHtml="<ul>"
    for (var key in aoaJson) {
    	var x = 0;
        if (aoaJson.hasOwnProperty(key)) {
        	var uripath= aoaJson[key];
        	var filename = uripath.substring(uripath.lastIndexOf('/')+1);
            aoaHtml+="<li><a href='"+aoaJson[key]+"' target='_blank' rel='noopener noreferrer'>"+filename+"</a></li>";    
        }
    }
    aoaHtml+="</ul>";
    $("#aoapath").html(aoaHtml);
</c:if>
<c:if test="${not empty thirdnom.orgCert}">
var orgJson = ${thirdnom.orgCert};
    var orgHtml="<ul>"
    for (var key in orgJson) {
    	var x = 0;
        if (orgJson.hasOwnProperty(key)) {
        	var uripath= orgJson[key];
        	var filename = uripath.substring(uripath.lastIndexOf('/')+1);
            orgHtml+="<li><a href='"+orgJson[key]+"' target='_blank' rel='noopener noreferrer'>"+filename+"</a></li>";   
        }
    }
    orgHtml+="</ul>";
    $("#orgpath").html(orgHtml);
</c:if>
<c:if test="${not empty thirdnom.industryCert}">
var indJson = ${thirdnom.industryCert};
    var indHtml="<ul>"
    for (var key in indJson) {
    	var x = 0;
        if (indJson.hasOwnProperty(key)) {
        	var uripath= indJson[key];
        	var filename = uripath.substring(uripath.lastIndexOf('/')+1);
            indHtml+="<li><a href='"+indJson[key]+"' target='_blank' rel='noopener noreferrer'>"+filename+"</a></li>";   
        }
    }
    indHtml+="</ul>";
    $("#indpath").html(indHtml);
</c:if>
<c:if test="${not empty thirdnom.balanceSheet}">
var balJson = ${thirdnom.balanceSheet};
var balHtml="<ul>"
for (var key in balJson) {
	var x = 0;
    if (balJson.hasOwnProperty(key)) {
    	var uripath= balJson[key];
    	var filename = uripath.substring(uripath.lastIndexOf('/')+1);
        balHtml+="<li><a href='"+balJson[key]+"' target='_blank' rel='noopener noreferrer'>"+filename+"</a></li>";   
    }
}
balHtml+="</ul>";
$("#balancepath").html(balHtml);
</c:if>
<c:if test="${not empty thirdnom.profitLossStatement}">
var profitJson = ${thirdnom.profitLossStatement};
var profitHtml="<ul>"
for (var key in profitJson) {
	var x = 0;
    if (profitJson.hasOwnProperty(key)) {
    	var uripath= profitJson[key];
    	var filename = uripath.substring(uripath.lastIndexOf('/')+1)
    	profitHtml+="<li><a href='"+profitJson[key]+"' target='_blank' rel='noopener noreferrer'>"+filename+"</a></li>";   
    }
} 
profitHtml+="</ul>";
$("#profitpath").html(profitHtml);
</c:if>
<c:if test="${not empty thirdnom.cashFlowStatement}">
var cashJson = ${thirdnom.cashFlowStatement};
var cashHtml="<ul>"
for (var key in cashJson) {
	var x = 0;
    if (cashJson.hasOwnProperty(key)) {
    	var uripath= cashJson[key];
    	var filename = uripath.substring(uripath.lastIndexOf('/')+1)
    	cashHtml+="<li><a href='"+cashJson[key]+"' target='_blank' rel='noopener noreferrer'>"+filename+"</a></li>";   
    }
}
cashHtml+="</ul>";
$("#cashpath").html(cashHtml);
</c:if>
});
</c:if>
        </script>
    </body>
</html>