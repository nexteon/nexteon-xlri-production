<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ page import="com.xlri.awards.common.StateUtil" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<%@include file="fragments/headerlibs.jsp" %>
  <body class="innerpage">

<%@include file="fragments/header.jsp" %>
<!-- Body Starts Here -->
<div class="clearfix"></div>

<main role="main">
        <div class="page-title">
            <h2>Nominator Registration</h2>
        </div>
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="whitebg">
                            <h3>Are you nominating in the capacity of an-</h3>
                            <select id="first-step-registarion" class="form-control" name="nominatorType">
                            	<option value="" selected disabled>Select</option>
                                <option value="form1">Individual</option>
                                <option value="form2">Institution/Organization</option>
                            </select>
                            <!-- Individual Form Start -->
                            <div id="form1" class="cst-form form-hide">
                                <div class="form-header">
                                    <h4>Registration Form For Individual</h4>
                                </div>

                                <div class="form-body">
                                    <form action="/registration" method="post" data-toggle="validator" role="form">
                                        <div class="row">
											<input type="hidden" value="individual" name="nominatorType"/>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="nominators-name">Nominator's Name<span class="mandate">*</span></label>
                                                    <input type="text" name="nominatorName" id="nominators-name" class="form-control" placeholder="Nominator's Name" required data-required-error="Please fill in this field.">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="nominators-email">Email<span class="mandate">*</span></label>
                                                    <input type="email" name="emailId" id="nominators-email" class="form-control" placeholder="Email" required data-required-error="Please fill in this field." data-remote="/neas/api/checkemail/nominator" data-remote-error="Email is already registered." pattern="^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$" data-pattern-error="Please provide a valid email id.">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone">Phone No<span class="mandate">*</span></label>
                                                    <div class="input-group">
                                                    	<span class="input-group-addon">+91</span>
                                                    	<input type="text" name="contactNumber" id="phone1" class="form-control" placeholder="10 Digit Contact Number" maxlength="10" pattern="[0-9]{10,10}" required data-required-error="Please fill in this field." data-remote="/neas/api/checkcontact/nominator" data-remote-error="Contact Number is already registered.">
                                                    </div>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>

                                            <div class="col-md-6 other-field">
                                                <div class="form-group">
                                                    <label for="occupation">Occupation<span class="mandate">*</span></label>
                                                    <select class="form-control" name="occupation" id="occupation" aria-required="true" required data-required-error="Please fill in this field.">
                                                    	<option value="" selected disabled>Select</option>
                                                        <option value="Salaried Employee">Salaried Employee</option>
                                                        <option value="Entrepreneur">Entrepreneur</option>
                                                        <option value="Mentor">Mentor</option>
                                                        <option value="other">Other</option>
                                                    </select>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="other-fieldhide">
                                                        <label class="vis-hidden" for="other-field">Other (Please Specify)</label>
                                                        <input type="text" name="otherOccupation" id="other-field" class="form-control" placeholder="Please Specify">
                                                    </div>
                                                </div>
                                            </div>
											<div class="clearfix"></div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="web1">Website(If Available)</label>
                                                   	<input type="text" name="website" id="web1" class="form-control" placeholder="Website"/>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12">
                                                <h4>Address</h4>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="address1">House No., Mohalla, Village</label>
                                                    <textarea name="addrLine1" class="form-control" id="address1" onblur="check(this);"></textarea>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="city">District</label>
                                                    <input type="text" name="addrCity" id="city" class="form-control" placeholder="District/City">
                                                </div>
                                                <div class="form-group">
                                                    <label for="state">State<span class="mandate">*</span></label>
                                                    <select class="form-control" name="addrState" id="state" aria-required="true" required data-required-error="Please fill in this field.">
                                                        <option value="" selected disabled>Select</option>
                                                        <c:set var="stateValues" value="<%=StateUtil.values()%>"/>
									               		<c:forEach items="${stateValues}" var="state">
									               			<option value="${state}">${state.name}</option>
									               		</c:forEach>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="pin">Pin Code<span class="mandate">*</span></label>
                                                    <input type="text" class="form-control onlyDigit" name="addrPinCode"  id="pinCode1" class="form-control" placeholder="Pin No" pattern="[0-9]{6}" required data-required-error="Please fill in this field." data-pattern-error="Only six numeric digit allowed.">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="landmark">Landmark</label>
                                                    <input type="text" name="addrLandmark" id="landmark" class="form-control" placeholder="Landmark">
                                                </div>
                                            </div>
                                            
                                            <div class="clearfix"></div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="password">Password<span class="mandate">*</span> </label>
                                                    <input type="password" name="password" id="password" class="form-control" placeholder="Password" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&^#])[A-Za-z\d@$!%*?&^#]{8,}$" required data-required-error="Please fill in this field." data-pattern-error="An 8 character long password reqiured that must contain a small letter, a capital letter, a special character  ( @ $ ! % * ? & ^ # ) and a digit.">
                                                	<span>An 8 character long password reqiured that must contain a small letter, a capital letter, a special character  ( @ $ ! % * ? & ^ # ) and a digit.</span>
                                                	<div class="help-block with-errors"></div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="confpass">Confirm Password<span class="mandate">*</span></label>
                                                    <input type="password" name="confirmPass" id="confpass" data-match="#password" class="form-control" placeholder="Confirm Password" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&^#])[A-Za-z\d@$!%*?&^#]{8,}$" required data-required-error="Please fill in this field." data-pattern-error="An 8 character long password reqiured that must contain a small letter, a capital letter, a special character  ( @ $ ! % * ? & ^ # ) and a digit.">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                                                                        
                                            <div class="clearfix"></div>
											<div class="col-md-12">
                                                <div class="form-group">
													<div id="captchaBlock">
													        <div id="resource1" class="captchaWrap">
													            <div class="c-img">
													                <div class="captchaContainer"></div>
													            </div>
													            <div class="captcha-input">
													                <input type="hidden" name="enc">
													                <input type="hidden" name="salt">
													                <input type="text" name="code" class="form-control" placeholder="Enter Captcha">
													                <span id="captchaMessage"></span>
													                <input type="hidden" name="redirect" value="/signup">
													            </div>
													            <div class="refresh-wrap">
													                <span id="searchButton"><i class="fa fa-refresh" aria-hidden="true"></i></span>
													            </div>
													        </div>
													   </div>
													   <input type="hidden" name="currentpath" value=""/>
													<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
												</div>
											</div>
											
											<div class="clearfix"></div>
                                            <div class="col-md-12">
                                                <div class="submit-btn">
                                                    <input class="btn btn-primary" type="submit" name="Submit" value="Submit">
                                                </div>
                                            </div>

                                        </div>

                                    </form>
                                </div>
                            </div>
                            <!-- Individual Form End -->


                            <!-- Institution/Organization -->
                            <div id="form2" class="cst-form form-hide">
                                <div class="form-header">
                                    <h4>Registration Form For Institution/Organization</h4>

                                </div>

                                <div class="form-body">
                                    <form action="/registration" method="post" data-toggle="validator" role="form">
                                        <div class="row">
											<input type="hidden" value="organisation" name="nominatorType"/>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="institution-organization-name">Institution/Organization Name<span class="mandate">*</span></label>
                                                    <input type="text" name="instituteName" id="institution-organization-name" class="form-control" placeholder="Institution/Organization Name" required data-required-error="Please fill in this field.">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="nominators-name2">Institution/Organization's Representative's Name<span class="mandate">*</span></label>
                                                    <input type="text" name="nominatorName" id="nominators-name2" class="form-control" placeholder="Nominator's Name" required data-required-error="Please fill in this field.">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="nominators-email1">Email<span class="mandate">*</span></label>
                                                    <input type="email" name="emailId" id="nominators-email1" class="form-control" placeholder="Email" required data-required-error="Please fill in this field."  data-remote="/neas/api/checkemail/nominator" data-remote-error="Email Id is already registered" pattern="^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$" data-pattern-error="Please provide a valid email id.">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone2">Phone No<span class="mandate">*</span></label>
                                                    <div class="input-group">
                                                    	<span class="input-group-addon">+91</span>
                                                    	<input type="tel" name="contactNumber" id="phone2" class="form-control" placeholder="10 Digit Contact Number" maxlength="10" pattern="[0-9]{10}" required data-required-error="Please fill in this field."  data-remote="/neas/api/checkcontact/nominator" data-remote-error="Contact Number is already registered">
                                                    	
                                                    </div>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="designation">Designation<span class="mandate">*</span></label>
                                                    <input type="text" name="designation" id="designation" class="form-control" placeholder="Designation" required data-required-error="Please fill in this field.">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="web2">Website(If Available)</label>
                                                   	<input type="text" name="website" id="web2" class="form-control" placeholder="Website"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <h4>Address</h4>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="nominator-address1">House No., Mohalla, Village</label>
                                                    <textarea class="form-control" name="addrLine1" id="nominator-address1" onblur="check(this);"></textarea>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="nominator-city">District</label>
                                                    <input type="text" name="addrCity" id="nominator-city" class="form-control" placeholder="District/City">
                                                </div>
                                                <div class="form-group">
                                                    <label for="state1">State<span class="mandate">*</span></label>
                                                    <select class="form-control" name="addrState" id="state1" aria-required="true" required data-required-error="Please fill in this field.">
                                                        <option value="" selected disabled>Select</option>
                                                        <c:set var="stateValues" value="<%=StateUtil.values()%>"/>
									               		<c:forEach items="${stateValues}" var="state">
									               			<option value="${state}">${state.name}</option>
									               		</c:forEach>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="nominator-pin">Pin Code<span class="mandate">*</span></label>
                                                    <input type="text" name="addrPinCode"  id="pinCode2" class="form-control onlyDigit" placeholder="Pin No" pattern="[0-9]{6}" required data-required-error="Please fill in this field." data-pattern-error="Only six numeric digit allowed.">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="nominator-landmark">Landmark</label>
                                                    <input type="text" name="addrLandmark" id="nominator-landmark" class="form-control" placeholder="Landmark">
                                                </div>
                                            </div>
                                            
                                            <div class="clearfix"></div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="password1">Password<span class="mandate">*</span></label>
                                                    <input type="password" name="password" id="password1" class="form-control" placeholder="Password" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&^#])[A-Za-z\d@$!%*?&^#]{8,}$" required data-required-error="Please fill in this field."  data-pattern-error="A minimum 8 character long password reqiured that must contain a small letter, a capital letter, a special character  ( @ $ ! % * ? & ^ # ) and a digit.">
                                                    <span>An 8 character long password reqiured that must contain a small letter, a capital letter, a special character ( @ $ ! % * ? & ^ # ) and a digit.</span>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="confpass1">Confirm Password<span class="mandate">*</span></label>
                                                    <input type="password" name="confirmPass" id="confpass1" data-match="#password1" class="form-control" placeholder="Confirm Password" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&^#])[A-Za-z\d@$!%*?&^#]{8,}$" required data-required-error="Please fill in this field."  data-pattern-error="A minimum 8 character long password reqiured that must contain a small letter, a capital letter, a special character  ( @ $ ! % * ? & ^ # ) and a digit.">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            
                                         <!--    <div class="clearfix"></div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="nominee-enterprise2">How do you know nominee's enterprise?</label>
                                                    <textarea name="knowNominee" class="form-control" id="nominee-enterprise2" required></textarea>
                                                </div>
                                            </div>  -->

											<div class="clearfix"></div>
											<div class="col-md-12">
                                                <div class="form-group">
													  <div id="captchaBlock1">
													        <div id="resource2" class="captchaWrap1">
													            <div class="c-img1">
													                <div class="captchaContainer1"></div>
													            </div>
													            <div class="captcha-input1">
													                <input type="hidden" name="enc">
													                <input type="hidden" name="salt">
													                <input type="text" name="code" placeholder="Enter Captcha">
													                <span id="captchaMessage1"></span>
													                <input type="hidden" name="redirect" value="/signup">
													            </div>
													            <div class="refresh-wrap">
													                <span id="searchButton1"><i class="fa fa-refresh" aria-hidden="true"></i></span>
													            </div>
													        </div>
													   </div>
													   <input type="hidden" name="currentpath" value=""/>
													<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
												</div>
											</div>
											
											<div class="clearfix"></div>
                                            <div class="col-md-12 align-center">
                                                <div class="submit-btn">
                                                    <input class="btn btn-primary" type="submit" name="Submit" value="Submit">
                                                </div>
                                            </div>

                                        </div>

                                    </form>
                                </div>
                            </div>
                            <!-- Institution/Organization -->
                        </div>

                    </div>
                    <!--end of class whitebg-->
                </div>
            </div>
        </div>

    </main>

<c:if test="${not empty openDialog}">
  <div class="modal fade" id="infoModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Nominator Registration NEAS 2018-19</h4>
        </div>
        <div class="modal-body">
          <p>Thank you for taking an interest in becoming a Nominator for the National Entrepreneurship Awards (NEAS) 2018-19. The Nominator will take care of following things–</p>
			<ol>
				<li>Check the eligibility criteria of all the three categories before nominating the entrepreneurs for the National Awards.</li>
				<li>Provides an assessment of an enterprise during the nomination process in order to support the evaluation team and the National Jury in selecting the winner.</li>
				<li>There are two parts of the nomination process-
				<ol> 
					<li>Part A - Basic details of the nominee/enterprise</li>
					<li>Part B - Enterprise details along with the nominator’s assessment</li>
				</ol>
				</li>
				<li>The Nomination Process for the 'Ecosystem Builders Track' follows the similar stages-
				<ol> 
					<li>Part A - Basic details of the nominee/institution</li>
					<li>Part B - Nominee/Institution's details along with recommendations</li>
				</ol>
				</li>
				
			</ol>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Proceed</button>
        </div>
      </div>
      
    </div>
  </div>
</c:if>


<c:if test="${not empty errorMsg}">
  <div class="modal fade" id="errorModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Error</h4>
        </div>
        <div class="modal-body">
          <p>${errorMsg}</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
</c:if>

<!-- Body Ends Here -->
<%@include file="fragments/footer.jsp" %>
<script>
        $(document).ready(function() {
            $('#form1').hide();
            $('#form2').hide();
            $(function() {
                $('#first-step-registarion').change(function() {
                    $('.form-hide').hide();
                    $('#' + $(this).val()).show();
                });
            });
        });
    </script>

    <script>
        $(document).ready(function() {
            $("#occupation").change(function() {
                if ($(this).val() == 'other') { // EDITED THIS LINE
                    $('.other-fieldhide').show();
                	$('#other-field').show();
                    $('.other-field').addClass('oth-active');
                }
                if ($(this).val() !== 'other') { // EDITED THIS LINE
                    $('.other-fieldhide').hide();
                    $('#other-field').hide();
                    $('.other-field').removeClass('oth-active');
                }
            });
        });
    </script>
<script>
		$(document).ready(function() {
			<c:if test="${not empty openDialog}">
				$('#infoModal').modal('show');
			</c:if>
			$('[data-toggle="tooltip"]').tooltip();
			if($('#errorModal').length > 0){
				$('#errorModal').modal('show');
			}
		});
	    $( document ).ready(function() {
	        getCaptcha();
	        validateCaptcha();
	        getCaptcha1();
	        validateCaptcha1();
	        $("#searchButton").click(function(){
	            getCaptcha();
	        });
	        $("#searchButton1").click(function(){
	            getCaptcha1();
	        });
	    });
	    $( '#first-step-registarion' ).change(function() {
	        getCaptcha();
	        validateCaptcha();
	        getCaptcha1();
	        validateCaptcha1();
	        $("#searchButton").click(function(){
	            getCaptcha();
	            getCaptcha1();
	        });
	        $("#searchButton1").click(function(){
	            getCaptcha();
	            getCaptcha1();
	        });
	    });

	    function getCaptcha(){
	        var html = "<h2>Loading Captcha</h2>"
	        console.log("/bin/captcha");
	        var url = "/bin/captcha"; 

	        $.ajax({
	            url: url, 
	            type: 'get',
	            success: function (result) {
	                html = "";
	                var captchaImg = "", captcha="",salt="";
	                if(result !== undefined || result !== null){
	                    captchaImg  = result.image;
	                    captcha = result.captcha;
	                    salt = result.salt;
	                    $('#resource1').find('.captchaContainer').html("<img id='captchaImage' src='data:image/png;base64,"+ captchaImg+"'></img>");
	                    $('#resource1').find('[name="enc"]').val(captcha);
	                    $('#resource1').find('[name="salt"]').val(salt);
	                }
	            },
	            error: function() {
	                console.log("Error while creating captcha");
	            }
	        });
	    }
	   function validateCaptcha() {
	        var isValid = false;
	        $("#"+$('#first-step-registarion').val()).find('[type="submit"]').on('click', function(event) {
	            if(event.hasOwnProperty('originalEvent')){
	            $('#captchaMessage').html("");
	            event.preventDefault();
	                if($("#"+$('#first-step-registarion').val()+" [name='code']").val() != ""){
	                	var salt,code,enc,datas;
	                	if($("#resource1 [name='code']").val()!=""){
	                		salt = $('#resource1 [name="salt"]').val();
	    	                code = $('#resource1 [name="code"]').val();
	    	                enc = $('#resource1 [name="enc"]').val();
	    	                datas = $('#form1').find('[name="salt"], [name="code"], [name="enc"], [name="${_csrf.parameterName}"]').serialize();
	                	}
	                
	                $.ajax({
	                    url: '/bin/validateCaptcha',
	                    type: 'post',
	                    data: datas,    
	                    success: function(data){
	                        console.log("Inside success");
	                        console.log(data.valid);
	                        if(!data.valid){
	                            $("#captchaMessage").html("You entered wrong captcha code, please enter again");
	                            return false;
	                        }
	                        else{
	                            console.log("now submit");
	                            isValid = true;
	                            //$('#' + "${formId}").submit();
	                              $("#"+$('#first-step-registarion').val()).find('[type="submit"]').trigger('click');
	                        }
	                        return false;
	                    },
	                    error: function() {
	                        console.log("An error occured while validating captcha");
	                    },
	                    complete: function(){
	                        console.log("ajax completed");
	                    }
	                });
	            }
	            else{
	            	$("#captchaMessage").html("Please enter captcha code");
	                return false;
	            }
	        }
	        });
	       return false;
	    }
	   
	   
	   function getCaptcha1(){
	        var html = "<h2>Loading Captcha</h2>"
	        console.log("/bin/captcha");
	        var url = "/bin/captcha"; 

	        $.ajax({
	            url: url, 
	            type: 'get',
	            success: function (result) {
	                html = "";
	                var captchaImg = "", captcha="",salt="";
	                if(result !== undefined || result !== null){
	                    captchaImg  = result.image;
	                    captcha = result.captcha;
	                    salt = result.salt;
	                    $('#resource2').find('.captchaContainer1').html("<img id='captchaImage' src='data:image/png;base64,"+ captchaImg+"'></img>");
	                    $('#resource2').find('[name="enc"]').val(captcha);
	                    $('#resource2').find('[name="salt"]').val(salt);
	                }
	            },
	            error: function() {
	                console.log("Error while creating captcha");
	            }
	        });
	    }
	   function validateCaptcha1() {
	        var isValid = false;
	        $("#"+$('#first-step-registarion').val()).find('[type="submit"]').on('click', function(event) {
	            if(event.hasOwnProperty('originalEvent')){
	            $('#captchaMessage1').html("");
	            event.preventDefault();
	                if($("#"+$('#first-step-registarion').val()+" [name='code']").val() != ""){
	                	var salt,code,enc,datas;
	                	
	                	if($("#resource2 [name='code']").val()!=""){
	                		salt = $('#resource2 [name="salt"]').val();
	    	                code = $('#resource2 [name="code"]').val();
	    	                enc = $('#resource2 [name="enc"]').val();
	    	                datas = $('#form2').find('[name="salt"], [name="code"], [name="enc"], [name="${_csrf.parameterName}"]').serialize();
	                	}
	                
	                $.ajax({
	                    url: '/bin/validateCaptcha',
	                    type: 'post',
	                    data: datas,    
	                    success: function(data){
	                        console.log("Inside success");
	                        console.log(data.valid);
	                        if(!data.valid){
	                            $("#captchaMessage1").html("You entered wrong captcha code, please enter again");
	                            return false;
	                        }
	                        else{
	                            console.log("now submit");
	                            isValid = true;
	                            //$('#' + "${formId}").submit();
	                              $("#"+$('#first-step-registarion').val()).find('[type="submit"]').trigger('click');
	                        }
	                        return false;
	                    },
	                    error: function() {
	                        console.log("An error occured while validating captcha");
	                    },
	                    complete: function(){
	                        console.log("ajax completed");
	                    }
	                });
	            }
	            else{
	            	$("#captchaMessage1").html("Please enter captcha code");
	                return false;
	            }
	        }
	        });
	       return false;
	    }
   $(document).ready(function () {
		  	$(".onlyDigit").keypress(function (e) {
			 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
					   return false;
			}
		   });
		});
	</script>
</body>
</html>