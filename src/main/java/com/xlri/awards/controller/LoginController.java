package com.xlri.awards.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.xlri.awards.common.EncryptionUtil;
import com.xlri.awards.common.OtpSalt;
import com.xlri.awards.common.SMSUtil;
import com.xlri.awards.dao.entity.Nominator;
import com.xlri.awards.dao.entity.NomineeDetails;
import com.xlri.awards.dao.entity.User;
import com.xlri.awards.dao.service.api.HttpClientPostService;
import com.xlri.awards.dao.service.api.MailService;
import com.xlri.awards.dao.service.api.NominatorService;
import com.xlri.awards.dao.service.api.NomineeService;
import com.xlri.awards.dao.service.api.UserService;


@Controller
public class LoginController {

	@Autowired
	MailService mailService;

	@Autowired
	NominatorService nominatorService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	NomineeService nomineeService;

	@Autowired
	HttpClientPostService postService;
	
	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public ModelAndView getSignupPage() {
		ModelAndView model = new ModelAndView("registrationclose");
		//model.addObject("openDialog", true);
		return  model;
	}

	
	 @RequestMapping(value = "/login", method = RequestMethod.GET)
	 public ModelAndView login() {
	  Authentication auth = SecurityContextHolder.getContext().getAuthentication();

	  if (!(auth instanceof AnonymousAuthenticationToken)) {
	   //return "redirect:/userpage";
	   return new ModelAndView("userpage");
	  }
	  else{
	   ModelAndView model = new ModelAndView();
	   model.setViewName("login");
	   return model;
	  }
	 }

	@RequestMapping(value = "/registration", method = RequestMethod.POST)
	public ModelAndView registerUser(HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		try {
				String enteredEmail = request.getParameter("emailId");
				String enteredContact = request.getParameter("contactNumber");
				Nominator existedUser = nominatorService.getNominatorByEmailId(enteredEmail);
				if(existedUser!=null){
					model.setViewName("registration");
					model.addObject("errorMsg", "The email address you have entered is already registered. "
							+ "If you have forgotten your password, click on below link. "
							+ "<div class='clearfix'><a class='btn btn-primary' href='/forgetpassword/"
							+EncryptionUtil.encrypt(existedUser.getRegistrationNumber(), OtpSalt.SALT.getSalt())
							+"'>Forgot Password</a></div>");
					return model;
				}
				existedUser = nominatorService.getNominatorByContactNumber(enteredContact);
				if(existedUser!=null){
					model.setViewName("registration");
					model.addObject("errorMsg", "The contact number you entered is already registered."
							+ " Kindly provide any other contact number");
					return model;
				}
				
				
				Nominator user = new Nominator();
				user.setNominationType(request.getParameter("nominatorType"));
				if(user.getNominationType().equals("organisation")){
					user.setInstituteName(request.getParameter("instituteName"));
					user.setDesignation(request.getParameter("designation"));
					user.setEnterpriseCount(10);
					user.setEcosystemCount(4);
				}else{
				
				}
				user.setFullName(request.getParameter("nominatorName"));
				user.setAddrLine1(request.getParameter("addrLine1"));
				user.setAddrLine2(request.getParameter("addrLine2"));
				user.setAddrCity(request.getParameter("addrCity"));
				user.setAddrState(request.getParameter("addrState"));
				user.setAddrLandmark(request.getParameter("addrLandmark"));
				user.setAddrPinCode(request.getParameter("addrPinCode"));
				user.setEmailId(request.getParameter("emailId"));
				user.setPassword(request.getParameter("password"));
				user.setContactNumber(request.getParameter("contactNumber"));
				if(user.getNominationType().equals("individual")){
					if(request.getParameter("occupation").equals("other")){
						user.setOccupation(request.getParameter("otherOccupation"));
					}else{
						user.setOccupation(request.getParameter("occupation"));
					}
					
					user.setEnterpriseCount(5);
					user.setEcosystemCount(2);
				}
				user.setWebsite(request.getParameter("website"));
				user.setIsActivated(false);
				user.setOtp();
				user.setOtpExpiry();
				user.setCreatedOn();
				String regNumber = nominatorService.getNewRegistrationNumber(request.getParameter("addrState"));
				if(regNumber.equals("")){
					model.setViewName("registration");
					model.addObject("errorMsg", "Something went wrong! Please contact helpdesk.");
					return model;
				}
				user.setRegistrationNumber(regNumber);
				user.setRole("NOMINATOR");
				
				boolean status = nominatorService.saveNominator(user);
				if(status){
					new Thread(new Runnable() {
						@Override
						public void run() {
							mailService.sendOtp(user);
						}
					}).start();
					
						new Thread(new Runnable() {
							@Override
							public void run() {
								try{
									new SMSUtil().sendOTP(user.getOtp()+" is your One Time Password to register yourself as Nominator with NEA 2018.", user.getContactNumber());
								}catch(Exception e){
									System.out.println(e);
								}
							}
						}).start();
					
					ModelAndView otpModel = new ModelAndView("otpverification");
					otpModel.addObject("registration",EncryptionUtil.encrypt(user.getRegistrationNumber(),
							OtpSalt.SALT.getSalt()));
					otpModel.addObject("successMsg","An OTP (One Time Password) has been sent to the registered mobile number and E-Mail.");
					return otpModel;
				}else{
					model.setViewName("registration");
					model.addObject("errorMsg", "Please fill all the necessary information!");
					return model;
				}
			//}
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.setViewName("registration");
		//model.addObject("errorMsg", "Captcha Validation Failed! Please try again.");
		return model;
	}
	
	
	@PostMapping(value = "/forgetpassword")
	public ModelAndView forgetPassword(@RequestParam("emailId") String emailId) {
		User user = userService.getUserByUsername(emailId);
		ModelAndView model = new ModelAndView();
		if(user!=null){
		new Thread(new Runnable() {
			@Override
			public void run() {
				mailService.changePasswordMail(user);
			}
		}).start();
		model.setViewName("index");
		model.addObject("successMsg", "An email has been sent to the entered email id. Kindly click the link provided in the mail to reset your password.");
		}else{
			model.setViewName("login");
			model.addObject("errorMsg", "Email Id entered is not registered. Please sign up first.");		
		}
		return model;
	}
	
	
	@GetMapping(value = "/forgetpassword/{encyptedRegistration}")
	public ModelAndView resetPassword(@PathVariable String encyptedRegistration) {
		ModelAndView model = new ModelAndView("forgot");
		if(!encyptedRegistration.equals("")){
			model.addObject("registration",encyptedRegistration);
		}	
		return model;
	}
	
	@PostMapping(value = "/changepassword/{encyptedRegistration}")
	public ModelAndView resetPasswordForm(@PathVariable String encyptedRegistration, HttpServletRequest request, RedirectAttributes redir) {
		ModelAndView model = new ModelAndView();
		String email = "";
		if(encyptedRegistration.equals("")){
			email = request.getParameter("emailId");
		}else{
			email = EncryptionUtil.decrypt(encyptedRegistration, OtpSalt.SALT.toString());
		}
		User user = userService.getUserByUsername(email);
		if(user == null){
			model.setViewName("forgot");
			model.addObject("errorMsg", "Email ID you entered is not registered.");
			return model;
		}else{
			switch(user.getRole()){
				case "NOMINATOR": 
					Nominator nominator = nominatorService.getNominatorByEmailId(email);
					nominator.setPassword(request.getParameter("password"));
					nominatorService.updateNominator(nominator);
					user.setPassword(nominator.getPassword());
					user.setSalt(nominator.getSalt());
				break;	
				case "NOMINEE":
					NomineeDetails nominee = nomineeService.getNomineeByEmailId(email);
					nominee.setPassword(request.getParameter("password"));
					nomineeService.updateNominee(nominee);
					user.setPassword(nominee.getPassword());
					user.setSalt(nominee.getSalt());
				break;
			}
			
			boolean status = userService.updateUser(user);
			if(status){
				model.setViewName("login");
				model.addObject("successMsg","Your password has been successfully changed! You may login now.");
				return model;
			}
		}
		model.setViewName("forgot");
		model.addObject("errorMsg", "Something went wrong! Please try again.");
		return model;
	}

		
}
