package com.xlri.awards.dao.service.api;

import java.util.List;

import com.xlri.awards.dao.entity.Nominator;

public interface NominatorService {

	public boolean saveNominator(Nominator nominator);
	public boolean updateNominator(Nominator nominator); 
	public List<Nominator> getAllNominators();
	public Nominator getNominatorById(int id);
	public Nominator getNominatorByEmailId(String emailId);
	public Nominator getNominatorByRegistrationNumber(String registrationNumber);
	public boolean checkCredentials(String password, Nominator nominatorList);
	public boolean deleteNominatorByRegistrationNumber(String registrationNumber);
	public int getEnterpriseCountLeft(String registrationNumber);
	public int decreaseEnterpriseCount(String registrationNumber);
	public int getEcosystemCountLeft(String registrationNumber);
	public int decreaseEcosystemCount(String registrationNumber);
	public Long getNominatorMaxId();
	public Nominator getNominatorByContactNumber(String contactNumber);
	public String getNewRegistrationNumber(String state);
	public List<Nominator> getNominatorByState(String... addrState);
	
}
