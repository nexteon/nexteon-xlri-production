<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">
	<link href="/resources/css/select2.min.css" rel="stylesheet" />
	<link href="/resources/css/select2-placeholder-transition.css" rel="stylesheet" />
	<%@include file="dashboard/headerlibs.jsp" %>
	<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white" id="page-top" >
		<div class="page-wrapper">
		<%@include file="dashboard/header.jsp" %>
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<!-- BEGIN CONTENT BODY -->
			<div class="page-content">
				<!-- BEGIN PAGE HEADER-->
				<!-- BEGIN PAGE BAR -->
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<a href="/userdata">Home</a>
							<i class="fa fa-circle"></i>
						</li>
						<li>
							<span>Dashboard</span>
						</li>
					</ul>
					<div class="page-toolbar">
						<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
							<i class="icon-calendar"></i>&nbsp;
							<span class="thin uppercase hidden-xs"></span>&nbsp;
							<i class="fa fa-angle-down"></i>
						</div>
					</div>
				</div>
				<!-- END PAGE BAR -->
				<!-- BEGIN PAGE TITLE-->
				<h1 class="page-title"> Expert Dashboard</h1>
				<!-- END PAGE TITLE-->
				<!-- END PAGE HEADER-->
				<!-- BEGIN DASHBOARD STATS 1-->
				<div class="row" >
					<!--Start,  Following div is only used for Regional Dashbaord  -->
					<div class = "row" ng-app="myApp" ng-controller="jsonCtrl">
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<div class="dashboard-stat dashboard-stat-v2 red type-table2">
								<div class="visual">
									<i class="fa fa-bar-chart-o"></i>
								</div>
								<div class="details">
									<div class="number">
										<span data-counter="counterup" data-value="12,5" id="nominee-count"></span>
									</div>
									<div class="desc"> Nominee Count</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<!-- END DASHBOARD STATS 1-->
				<input type="hidden" name="subCategory" value=""/>
				<!--   <input type="hidden" name="subCategory" value=""/> -->
				<div class="row datatable2">
					<div class="col-md-12">
						<!--  BEGIN EXAMPLE TABLE PORTLET -->
						<div class="portlet light bordered">
							<div class="portlet-title">
								<div class="caption font-dark">
									<i class="icon-settings font-dark"></i>
									<span class="caption-subject bold uppercase"> Nominee Data Table</span>
								</div>
							</div>
							<div class="portlet-body">
								<div class="table-responsive">
									<div class="total_record2">
										Total Count:
										<div id="filter-count2"></div>
									</div>
									<table class="table table-striped table-bordered table-hover table-checkable order-column dataTable" id="example1" class="display">
										<thead>
											<tr>
												<th>ID</th>
												<th>View</th>
												<th>Download Field Report</th>
												<th>Score</th>
												<th>Score Update</th>
												<th>Registration Number</th>
												<th>Nominee Name</th>
												<th>Email Id</th>
												<th>Contact Number</th>
												<th>Award Category</th>
												<th>Enterprise Category</th>
												<th>Award Sub Category</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
										<tfoot>
											<tr id="filters2">
												<th>ID</th>
												<th></th>
												<th>Download Field Report</th>
												<th>Score</th>
												<th>Score Update</th>
												<th>Registration Number</th>
												<th>Nominee Name</th>
												<th>Email Id</th>
												<th>Contact Number</th>
												<th>Award Category</th>
												<th>Enterprise Category</th>
												<th>Award Sub Category</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
							<!--   END EXAMPLE TABLE PORTLET -->
						</div>
					</div>
				</div>
				
				
				<!-- Modal assign expert to nominee ecosystem -->
                <div class="modal fade" id="edit-score" role="dialog">
                    <div class="modal-dialog modal-lg">

                        <!-- Modal content-->
                            <div class="modal-content">
                                <form action="" method="post" novalidate="true" id="edit-score-form">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="input-wrapper">
                                            <div class="col-md-4 single-select">
												<input type="email" readonly id="nom-email" name="nomemail" readonly/><br/>
                                            </div>
                                        </div>
                                        <div class="input-wrapper">
                                            <div class="col-md-4 single-select">
												<input type="number" min="0" max="100" id="nom-score" name="nomscore"/><br/>
                                            </div>
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <button class="btn btn-primary" type="submit">Edit Score</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
						
				
			</div>
			<!-- END CONTENT BODY -->
			<!-- END CONTENT -->
			<!-- END CONTAINER -->
			<!-- BEGIN QUICK NAV -->
		</div>
		<!-- END QUICK NAV -->
		<script src="/resources/dashboard/js/jquery.min.js" type="text/javascript"></script>
		<%@include file="dashboard/footer.jsp"%>
		<script src="/resources/dashboard/js/workingjs/jquery.dataTables.min.new.js"></script>
		<link rel="stylesheet" type="text/css" href="/resources/dashboard/css/workingcss/jquery.dataTables.min.css">
		<link rel="stylesheet" type="text/css" href="/resources/dashboard/css/workingcss/buttons.dataTables.min.css">
		<script src="/resources/dashboard/js/workingjs/dataTables.buttons.min.js"></script>
		<script src="/resources/dashboard/js/workingjs/jszip.min.js"></script>
		<script src="/resources/dashboard/js/workingjs/pdfmake.min.js"></script>
		<script src="/resources/dashboard/js/workingjs/vfs_fonts.js"></script>
		<script src="/resources/dashboard/js/workingjs/buttons.html5.min.js"></script>
		<!-- this is ajax call to get value of nominee data from dashboard controller -->
		<script>
            var app = angular.module('myApp', []);
            app.controller('jsonCtrl', function($scope, $http) {
                $http.post("/neas/api/expertnomineedata")
                    .then(function(response) {
                        $scope.tableData = response.data;
                        document.getElementById('nominee-count').innerHTML = response.data.length;
                    });
            });
        </script>
		<script type="text/javascript" >
			$('#example1').DataTable( {
			   	  "ajax": {
			   	  "url": "/neas/api/expertnomineedata",
			   	  "type" : 'POST',
			   	"scrollX": true,
			   	  "dataSrc": ""
			   	  },
			   	  "columns": [
			   	  { "data": "id" },
			   	  { "data": "view" },
			   	  { "data": "download" },
			   	  { "data": "score" },
			   	  { "data": "scoreupdate" },
			   	  { "data": "registrationnumber" },
			   	  { "data": "nomineename" },
			   	  { "data": "emailid" },
			   	  { "data": "contactnumber" },
			   	  { "data": "awardcategory" },
			   	  { "data": "enterprisecategory" },
			   	  { "data": "awardsub" },
			   	  ],dom: 'Bfrtip',
			       buttons: [
			                 'copyHtml5',
			                 'excelHtml5',
			                 'csvHtml5',
			                 'pdfHtml5'
			             ], initComplete: function () {
			   	  this.api().columns().every(function () {
			   	  var column = this;
			
			   	 
			   	  var select = $('<select><option value=""></option></select>')
			   	  .appendTo($("#filters2").find("th").eq(column.index()))
			   	  .on('change', function () {
			   	  var val = $.fn.dataTable.util.escapeRegex(
			   	  $(this).val()); 
			
			   	  column.search(val ? '^' + val + '$' : '', true, false)
			   	  .draw();
			   	  }); 
			   	  column.data().unique().sort().each(function (d, j) {
			   	  select.append('<option value="' + d + '">' + d + '</option>')
			   	  });
			   	  });
			   	  }
			   	  } );
			
			      $(document).ready(function() {
			    	  
					    
					    
					    
					    
					    $('#example1').on('click', '.btnscore', function(){
					    	$('#edit-score-form').attr('action','/neas/api/expert/editscore');
		                	$('#nom-email').val($(this).data('value'));
					    	});
					    
			          $('.type-table1').click(function(){
			              $('.datatable').fadeIn();
			              $('.datatable2').fadeOut();
			          });
			           
			           $('.type-table2').click(function(){
			              $('.datatable2').fadeIn();
			              $('.datatable').fadeOut();
			          });
			          $('body').change(function(){			                    
		                    $('.datatable2 #filter-count2 .dataTables_info').remove();
		                    $(".datatable2 .dataTables_info").clone().appendTo(".datatable2 #filter-count2");
		                    $('.total_record2').show();
			          });
			       
			     
				         
			      });
			      
			   $(document).ready(function(){
			    $.fn.dataTable.ext.errMode = 'none';
			    $('#example1').on( 'error.dt', function ( e, settings, techNote, message ) {
			    	console.log( 'An error has been reported by DataTables: ', message );
			    }).DataTable();
			    $('#example1').DataTable();
			    

			   });
			   
		</script>
	</body>
</html>