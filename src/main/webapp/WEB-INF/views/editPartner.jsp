<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="com.xlri.awards.common.StateUtil" %>
<!DOCTYPE html>
<html lang="en">
   <%@include file="dashboard/headerlibs.jsp" %>
   <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
   			<div class="page-wrapper">
          <%@include file="dashboard/header.jsp" %>
           


                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="/userdata">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>EditPartner</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> Edit Partner</h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <!-- BEGIN DASHBOARD STATS 1-->



<!-- Form Container -->
   <div class="form-outer">
                <div class="form">
                    <form action="/editpartner/${partner.id}" method="post">
                        <div class="row">
                        <div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-12">Partner Name
									<span class="required"> * </span>
								</label>
								<div class="col-md-12">
									<input name="partnerName" id="partnerName" type="text" class="form-control" placeholder="Partner Name" required="" value="${partner.partnerName }"/> 
									</div>
							</div>
						</div>
                      <!--   <div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-12">Email
									<span class="required"> * </span>
								</label>
								<div class="col-md-12">
									<input name="emailId" id="emailId" type="email" class="form-control" placeholder="Email Id"  required=""/>
							</div>
						</div>
						</div> -->
						
						<input name="emailId" id="emailId" type="hidden" class="form-control" required="" value="${partner.emailId }"/>
						
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-12">Contact No
									<span class="required"> * </span>
								</label>
								<div class="col-md-12">
									<input name="contactNumber" id="contactNumber" type="tel" class="form-control" placeholder="Contact No" maxlength="10" pattern="[0-9]{10}" required="" value="${partner.contactNumber}"/> 
									</div>
							</div>
						</div>
						
							<div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-12">Partner Address
									<span class="required"> * </span>
								</label>
								<div class="col-md-12">
									<input name="address" id="address" type="text" class="form-control" placeholder="Partner Address" value="${partner.address}"/> 
									</div>
							</div>
						</div>
						
						
						
						<div class="col-md-6">
							<!-- <div class="form-group">
								<label class="control-label col-md-12">Partner State
									<span class="required"> * </span>
								</label>
								<div class="col-md-12">
									<input name="state" id="state" type="text" class="form-control" placeholder="Partner States" /> 
									</div>
							</div> -->
							
							<div class="form-group">
                               <label for="state">State<span class="mandate">*</span></label>
                               <div class="col-md-12">
	                               <select multiple="multiple" class="form-control" name="addrState" id="state" aria-required="true" required data-required-error="Please fill in this field.">
	                               <option value="">Select</option>
	                               <c:set var="stateValues" value="<%=StateUtil.values()%>"/>
	                          		<c:forEach items="${stateValues}" var="state">
	                           			<option value="${state}">${state.name}</option>
	                          		</c:forEach>
	                               </select>
	                               <p class="info">*You can select multiple values</p>
	                           </div>
                            </div>
						</div>
						
                        
                        
                        <div class="col-md-12">
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<%-- <button type="submit" name="${_csrf.parameterName}" class="btn green" value="${_csrf.token}">Submit</button>
									 --%>
									 <input type="hidden" name="currentpath" value=""/>
									 <input type="hidden" name="${_csrf.parameterName}" class="btn green" value="${_csrf.token}"/>
									<input class="btn green" type="submit" name="Submit" value="Submit">
								</div>
							</div>
						</div>
						</div>
						
                        </div>
                    </form>
                   </div>
           </div>
         
         </div>
         </div>
         </div>
         
        <!-- END QUICK NAV -->
     <%@include file="dashboard/footer.jsp"%>
     </body>
         </html>
                    
