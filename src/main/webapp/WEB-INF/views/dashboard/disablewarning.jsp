<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    
<!--  script for disabled warning   -->
     <script> 
     $(document).ready(function(){
     $.fn.dataTable.ext.errMode = 'none';

     $('#example').on( 'error.dt', function ( e, settings, techNote, message ) {
     console.log( 'An error has been reported by DataTables: ', message );
     } ) .DataTable();
     $('#example').DataTable();
     
     });
     </script>