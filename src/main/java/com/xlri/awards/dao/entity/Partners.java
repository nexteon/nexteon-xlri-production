package com.xlri.awards.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.xlri.awards.common.EncryptionUtil;

@Entity
@Table(name="Partners")
public class Partners {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	
	private Long id;
	
	@Column(nullable=false)
	private String partnerName;
	
	@Column(nullable=false,unique=true)
	private String emailId;
	
	@Column(nullable=false,unique=true)
	private String contactNumber;
	
	private String address;
	
	private String state;
	
	private String password;
	
	private String salt;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.getNewSalt();
		this.password = EncryptionUtil.encryptPass(password + this.getSalt());
	}

	public String getSalt() {
		return salt;
	}

	/*public void setSalt(String salt) {
		this.salt = salt;
	}*/
	
	private void getNewSalt() {
		this.salt = EncryptionUtil.generateRandomString();
	}

}
