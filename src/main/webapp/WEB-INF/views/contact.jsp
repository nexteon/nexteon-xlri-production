<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
   <%@include file="fragments/headerlibs.jsp" %>
   <body class="innerpage">
      <%@include file="fragments/header.jsp" %>
      <!-- Body Starts Here -->
      <main role="main">
         <div class="page-title">
            <h2>Contact Us</h2>
         </div>
         <div class="content">
            <div class="container">
               <div class="row">
                  <div class="col-md-12">
                     <div class="whitebg">
                        <div class="col-xs-12">
                           <p class="contactmsg">Following are the contact points of all
                              the implementing partners for National Entrepreneurship Awards
                              2017. Kindly connect with us in case you have any query related
                              to the program / application. Please contact only the relevant
                              person as per your geographical area -
                           </p>
                        </div>
                        <div class="col-xs-12">
                           <h4 style="color:red">Timing: 9:30 AM to 6:30 PM (Monday to
                              Saturday)
                           </h4>
                        </div>
                        <div class="col-xs-12">
                           <table class="table table-bordered">
                              <thead>
                                 <tr class="text-center">
                                    <th>Name</th>
                                    <th>State</th>
                                    <th>Contact Person</th>
                                    <th>Contact</th>
                                    <th>Email</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td>Indian Institute of Technology Delhi</td>
                                    <td>Jammu & Kashmir, Uttarakhand, Delhi, Himachal Pradesh,
                                       Punjab, Haryana
                                    </td>
                                    <td>Prof P Somrajan</td>
                                    <td>9899156633</td>
                                    <td>contact-neas@nic.in</td>
                                 </tr>
                                 <tr>
                                    <td>Indian Institute of Technology Kanpur</td>
                                    <td>Uttar Pradesh, Bihar, West Bengal</td>
                                    <td>Dr. Mohan Krishna Mishra</td>
                                    <td>9839084301</td>
                                    <td>contact-neas@nic.in</td>
                                 </tr>
                                 <tr>
                                    <td>Indian Institute of Technology Bombay</td>
                                    <td>Madhya Pradesh, Chhattisgarh, Maharashtra, Karnataka</td>
                                    <td>Ms Vandana</td>
                                    <td>9819225007</td>
                                    <td>contact-neas@nic.in</td>
                                 </tr>
                                 <tr>
                                    <td>Indian Institute of Technology Madras</td>
                                    <td>Tamil Nadu, Andhra Pradesh, Kerala, Puducherry, A&N
                                       Islands
                                    </td>
                                    <td>Ms. Jayalakshmi Umadikar</td>
                                    <td>9176217205</td>
                                    <td>contact-neas@nic.in</td>
                                 </tr>
                                 <tr>
                                    <td>Indian Institute of Management Ahmedabad</td>
                                    <td>Rajasthan, Gujarat, Goa, Daman & Diu</td>
                                    <td>Ms. Susmita Ghosh</td>
                                    <td>8141212679</td>
                                    <td>contact-neas@nic.in</td>
                                 </tr>
                                 <tr>
                                    <td>XLRI Jamshedpur</td>
                                    <td>Jharkhand, Odisha, Sikkim, North East</td>
                                    <td>Prof. Madhukar Shukla</td>
                                    <td>9431113764</td>
                                    <td>contact-neas@nic.in</td>
                                 </tr>
                                 <tr>
                                    <td>Tata Institute of Social Sciences (TISS), Mumbai</td>
                                    <td>Telengana</td>
                                    <td>Mr Kunwar Rajeev Singh</td>
                                    <td>9868726645</td>
                                    <td>contact-neas@nic.in</td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                        <div class="col-xs-12">
                           <p class="contactmsg">For any technical issues related to
                              website while submitting the application kindly contact Mr. Anil
                              Kumar on +91-9650100239
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </main>
      <!-- Body Ends Here -->
      <%@include file="fragments/footer.jsp" %>
   </body>
</html>