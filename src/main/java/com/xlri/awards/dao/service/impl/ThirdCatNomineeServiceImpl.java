package com.xlri.awards.dao.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xlri.awards.dao.entity.ThirdCatNominee;
import com.xlri.awards.dao.service.api.ThirdCatNomineeService;

@Repository
@Transactional
public class ThirdCatNomineeServiceImpl implements ThirdCatNomineeService {

	private static final Logger LOGGER = LoggerFactory.getLogger(NomineeServiceImpl.class);

	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public boolean saveNominee(ThirdCatNominee nominee) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			session.save(nominee);
			session.getTransaction().commit();
 			System.out.println();
			return true;
 			
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return false;
		}
	}

	@Override
	public ThirdCatNominee getNomineeByContact(String contact) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<ThirdCatNominee> query =  session.createQuery("SELECT n FROM ThirdCatNominee n WHERE n.contact=:contact");
			query.setParameter("contact", contact);
			session.getTransaction().commit();
			return query.getSingleResult();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public List<ThirdCatNominee> getAllThirdCatNom() {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			List<ThirdCatNominee> query =  session.createQuery("From ThirdCatNominee").getResultList();
			session.getTransaction().commit();
			return query;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

}
