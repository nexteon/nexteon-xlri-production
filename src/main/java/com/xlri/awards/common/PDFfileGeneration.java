package com.xlri.awards.common;

import java.io.FileOutputStream;
import java.io.IOException;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.pdf.PdfWriter;

public class PDFfileGeneration {

	private static String FILE = "/home/mdbm/Desktop/PDF/FirstPdf.pdf";
	
	
	public static void pdfFile(String text) throws IOException, DocumentException{
		
		Document document = new Document();
		PdfWriter.getInstance(document, new FileOutputStream(FILE));
		 
		document.open();
		Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
		Chunk chunk = new Chunk(text, font);
		 
		document.add(chunk);
		document.close();

		
	}
	
	/*public static void main (String args[]) throws IOException, DocumentException {
	       
		PDFfileGeneration pDFfileGeneration=new PDFfileGeneration();  
		pDFfileGeneration.pdfFile("This is the sample document and we are adding content to it."); 
	      

	   } */ 
}
