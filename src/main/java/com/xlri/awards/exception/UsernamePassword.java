package com.xlri.awards.exception;

import com.xlri.awards.common.EncryptionUtil;

public class UsernamePassword {

	public static void main(String args[]) {
		String salt = EncryptionUtil.generateRandomString();
		String pass = EncryptionUtil.encryptPass("iitg@eastneas2018"+salt);
		System.out.println(salt);
		System.out.println(pass);
	}
	
}
