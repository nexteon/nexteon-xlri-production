package com.xlri.awards.dao.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlri.awards.common.EncryptionUtil;
import com.xlri.awards.dao.entity.Nominator;
import com.xlri.awards.dao.service.api.NominatorService;

@Repository
@Transactional
public class NominatorServiceImpl implements NominatorService {

	private static final Logger LOGGER = LoggerFactory.getLogger(NominatorServiceImpl.class);

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public boolean saveNominator(Nominator nominator) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			session.save(nominator);
			session.getTransaction().commit();
 			return true;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return false;
		}
	}

	@Override
	public boolean updateNominator(Nominator nominator) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			session.update(nominator);
			session.getTransaction().commit();
 			return true;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return false;
		}
	}

	@Override
	public List<Nominator> getAllNominators() {
		List<Nominator> nominatorList = null;
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			nominatorList = session.createQuery("From Nominator").getResultList();
			session.getTransaction().commit();
			return nominatorList;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public Nominator getNominatorById(int id) {
		Nominator nominator = null;
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			nominator = session.find(Nominator.class, id);
			session.getTransaction().commit();
			return nominator;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public Nominator getNominatorByRegistrationNumber(String registrationNumber) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<Nominator> query =  session.createQuery("SELECT e FROM Nominator e WHERE e.registrationNumber=:registrationNumber");
		    query.setParameter("registrationNumber", registrationNumber);
			session.getTransaction().commit();
			return query.getSingleResult();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public boolean checkCredentials(String password, Nominator nominator) {
		String salt = nominator.getSalt();
		String encytPass = EncryptionUtil.encryptWithSHA(password+salt);
		if(encytPass.equals(nominator.getPassword())){
            return true;
        }else {
            return false;
        }
	}

	@Override
	public boolean deleteNominatorByRegistrationNumber(String registrationNumber) {

		return false;
	}

	@Override
	public Nominator getNominatorByEmailId(String emailId) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<Nominator> query =  session.createQuery("SELECT e FROM Nominator e WHERE e.emailId=:emailId");
		    query.setParameter("emailId", emailId);
			session.getTransaction().commit();
			return query.getSingleResult();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}
	
	@Override
	public Nominator getNominatorByContactNumber(String contactNumber) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<Nominator> query =  session.createQuery("SELECT e FROM Nominator e WHERE e.contactNumber=:contactNumber");
		    query.setParameter("contactNumber", contactNumber);
			session.getTransaction().commit();
			return query.getSingleResult();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public int getEnterpriseCountLeft(String registrationNumber) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<Nominator> query =  session.createQuery("SELECT e FROM Nominator e WHERE e.registrationNumber=:registrationNumber");
		    query.setParameter("registrationNumber", registrationNumber);
			session.getTransaction().commit();
			return query.getSingleResult().getEnterpriseCount();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return -1;
		}
	}

	@Override
	public int decreaseEnterpriseCount(String registrationNumber) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<Nominator> query =  session.createQuery("SELECT e FROM Nominator e WHERE e.registrationNumber=:registrationNumber");
		    query.setParameter("registrationNumber", registrationNumber);
			Nominator nominator = query.getSingleResult();
			nominator.setEnterpriseCount(nominator.getEnterpriseCount()-1);
			session.update(nominator);
			session.getTransaction().commit();
			return nominator.getEnterpriseCount();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return -1;
		}
	}
	
	@Override
	public Long getNominatorMaxId() {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<Nominator> query =  session.createQuery("SELECT MAX(n.id) as id FROM Nominator n");
			session.getTransaction().commit();
			Nominator nominator = query.getSingleResult();
			Long id = nominator.getId();
			return id;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}
	
	@Override
	public String getNewRegistrationNumber(String state){
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<Nominator> query =  session.createQuery("from Nominator order by id DESC");
			query.setMaxResults(1);
			session.getTransaction().commit();
			Nominator nominator = query.uniqueResult();
			StringBuilder sb = new StringBuilder("NEAS"+state);
			if(nominator!=null){
				int number = Integer.parseInt(nominator.getRegistrationNumber().substring(6));
				sb.append(String.format("%04d", ++number));
				return sb.toString();
			}else{
				sb.append(String.format("%04d", 1));
				return sb.toString();
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return "";
		}

	}

	@Override
	public List<Nominator> getNominatorByState(String... addrState) {
		
		List<Nominator> stateList = new ArrayList<>();
		for(String x : addrState){
				
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<Nominator> query =  session.createQuery("SELECT e FROM Nominator e WHERE e.addrState=:addrState");
			query.setParameter("addrState", x);
			stateList.addAll(query.getResultList());
			session.getTransaction().commit();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
		}
		return stateList;
	}
	
	@Override
	public int getEcosystemCountLeft(String registrationNumber) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<Nominator> query =  session.createQuery("SELECT e FROM Nominator e WHERE e.registrationNumber=:registrationNumber");
		    query.setParameter("registrationNumber", registrationNumber);
			session.getTransaction().commit();
			return query.getSingleResult().getEcosystemCount();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return -1;
		}
	}

	@Override
	public int decreaseEcosystemCount(String registrationNumber) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<Nominator> query =  session.createQuery("SELECT e FROM Nominator e WHERE e.registrationNumber=:registrationNumber");
		    query.setParameter("registrationNumber", registrationNumber);
			Nominator nominator = query.getSingleResult();
			nominator.setEcosystemCount(nominator.getEcosystemCount()-1);
			session.update(nominator);
			session.getTransaction().commit();
			return nominator.getEcosystemCount();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return -1;
		}
	}

}
