<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
   <%@include file="fragments/headerlibs.jsp" %>
   <body class="innerpage">
      <%@include file="fragments/header.jsp" %>
      <!-- Body Starts Here -->
      <main role="main">
         <div class="page-title">
            <h2>Awards</h2>
         </div>
         <div class="content">
            <div class="container">
               <div class="row">
                  <div class="col-md-12">
                     <div class="whitebg">
                        <div class="clearfix"></div>
                        <div class="col-md-12 pull-left tx1-hdg">
                           <h4 class="blue-hdg">Awards: Awards will be presented under the following Award Categories:</h4>
                              <h5>Award Category A: Enterprise Awards - 39 Awards</h5>
                              <h4 class="blue-hdg">Award Types under Category A1 i.e. initial investment up to ₹ 1 L:</h4>
                           <div class="panel panel-default">
                              <div class="panel-body">
                                 <p><strong>Sector Awards with investment up to </strong><strong>₹</strong><strong> 1L (9 Nos.)</strong></p>
                                
                                 <ol>
                                    <li>Textile, textile articles, leather and related goods</li>
                                    <li>Agriculture, food processing and forestry &ndash; also includes vegetable products, vegetable fats, prepared foodstuffs and agri-tech</li>
                                    <li>Chemicals, pharma, bio and others</li>
                                    <li>Retail Trade</li>
                                    <li>Renewable energy and waste management</li>
                                    <li>Handicrafts</li>
                                    <li>Healthcare</li>
                                    <li>Hospitality, Tourism &amp; Travel</li>
                                    <li>Logistics, transports, e-commerce &amp; other services</li>
                                 </ol>
                                 
                              </div>
                           </div>
                           <div class="panel panel-default">
                              <div class="panel-body">
                                 <p><strong>Special Category Award with investment up to </strong><strong>₹</strong><strong> 1L (4 Nos.)</strong></p>
                                 <ol>
                                    <li>Women Entrepreneurs</li>
                                    <li>Entrepreneurs from SC/ST</li>
                                    <li>Entrepreneur from PwDs Category</li>
                                    <li>Entrepreneurs from Difficult Areas</li>
                                 </ol>
                              </div>
                           </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 pull-left">
                           <h4 class="blue-hdg">Award Types under Categories A2 and A3 i.e. initial investment above ₹1 L to ₹ 10 L and Initial investment above ₹10 L to ₹1 Cr:
                           </h4>
                           <div class="panel panel-default">
                              <div class="panel-body">
                                 <p><strong>Sector Awards with investment above </strong><strong>₹</strong> <strong>1L to </strong><strong>₹</strong> <strong>10L (9 Nos.) and above </strong><strong>₹</strong> <strong>10L to </strong><strong>₹</strong> <strong>1Cr (9 Nos.)</strong></p>
                                 <ol>
                                    <li>Textile, textile articles, leather and related goods</li>
                                    <li>Agriculture, food processing and forestry &ndash; also includes vegetable products, vegetable fats, prepared foodstuffs and agri-tech</li>
                                    <li>Chemicals, pharma, bio and others</li>
                                    <li>Engineering systems, base metals &amp; articles, machinery &amp; mechanical appliances and medical devices</li>
                                    <li>Renewable energy and waste management</li>
                                    <li>Handicrafts</li>
                                    <li>Healthcare</li>
                                    <li>Hospitality, Tourism &amp; Travel</li>
                                    <li>Logistics, transports, e-commerce &amp; other services</li>
                                 </ol>
                               
                              </div>
                           </div>
                           <div class="panel panel-default">
                              <div class="panel-body">
                                 <p><strong>Special Category Awards with investment above </strong><strong>₹</strong> <strong>1L to </strong><strong>₹</strong> <strong>10L (4 Nos.) and above </strong><strong>₹</strong> <strong>10L to </strong><strong>₹</strong> <strong>1Cr (4 Nos.)</strong></p>
                                 
                                 <ol>
                                    <li>Women Entrepreneurs</li>
                                    <li>Entrepreneurs from SC/ST Category</li>
                                    <li>Entrepreneur from PwDs Category</li>
                                    <li>Entrepreneurs from Difficult Areas</li>
                                 </ol>
                              </div>
                           </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 pull-left">
                           <h4 class="blue-hdg">Award Category B: Ecosystem Builders Award- 4 Awards <br/><br/>
                              Award types under Ecosystem Builders Category:
                           </h4>
                           <div class="panel panel-default">
                              <div class="panel-body">
                                 <ol>
                                    <li>Entrepreneurship Development Institutes/ Organisations</li>
                                    <li>Incubation Centres</li>
                                    <li>Mentors</li>
                                    <li>Promoters Rural Producer Group Enterprise</li>
                                 </ol>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </main>
      <!-- Body Ends Here -->
      <%@include file="fragments/footer.jsp" %>
   </body>
</html>