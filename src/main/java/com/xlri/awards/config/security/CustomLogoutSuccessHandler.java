package com.xlri.awards.config.security;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;

import com.xlri.awards.dao.entity.LogTable;
import com.xlri.awards.dao.service.api.LogTableService;

public class CustomLogoutSuccessHandler implements LogoutHandler {

	@Autowired
	private LogTableService logTableService;

	@Override
	 public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication){
		
		LogTable logTable = new LogTable();
		logTable.setIpAddress(request.getRemoteAddr());
		logTable.setLoginType("Logout Success");
		logTable.setTimestamp(new Date());
		logTable.setUsername(authentication.getPrincipal().toString());
		logTableService.saveLog(logTable);
		
		response.setStatus(HttpServletResponse.SC_OK);
		try {
			response.sendRedirect("/login");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}