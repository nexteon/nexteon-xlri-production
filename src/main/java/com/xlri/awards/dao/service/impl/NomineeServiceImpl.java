package com.xlri.awards.dao.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlri.awards.common.EncryptionUtil;

import com.xlri.awards.dao.entity.NomineeDetails;

import com.xlri.awards.dao.service.api.NomineeService;

@Repository
@Transactional
public class NomineeServiceImpl implements NomineeService {

	private static final Logger LOGGER = LoggerFactory.getLogger(NomineeServiceImpl.class);

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public boolean saveNominee(NomineeDetails nominee) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			session.save(nominee);
			session.getTransaction().commit();
 			return true;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return false;
		}
	}

	@Override
	public boolean updateNominee(NomineeDetails nominee) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			session.update(nominee);
			session.getTransaction().commit();
 			return true;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return false;
		}
	}

	@Override
	public List<NomineeDetails> getAllNominee() {
		List<NomineeDetails> nomineeList = null;
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			nomineeList = session.createQuery("From NomineeDetails").getResultList();
			session.getTransaction().commit();
			return nomineeList;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public NomineeDetails getNomineeById(Long id) {
		NomineeDetails nominee = null;
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			nominee = session.find(NomineeDetails.class, id);
			session.getTransaction().commit();
			return nominee;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}
	/*@Override
	public NomineeDetails getNomineeById(int id) {
		NomineeDetails nominee = null;
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			nominee = session.find(NomineeDetails.class, id);
			session.getTransaction().commit();
			return nominee;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}*/

	@Override
	public NomineeDetails getNomineeByEmailId(String emailId) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<NomineeDetails> query =  session.createQuery("SELECT n FROM NomineeDetails n WHERE n.emailId='"+emailId+"'");
			session.getTransaction().commit();
			return query.getSingleResult();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public NomineeDetails getNomineeByRegistrationNumber(String registrationNumber) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<NomineeDetails> query =  session.createQuery("SELECT n FROM NomineeDetails n WHERE n.registrationNumber=:registrationNumber");
			query.setParameter("registrationNumber", registrationNumber);
			session.getTransaction().commit();
			return query.getSingleResult();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public boolean deleteNomineeByRegistrationNumber(String registrationNumber) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Long getNomineeBySocialCategory(String socialCategory) {
		try {
			
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<NomineeDetails> query = session.createQuery("SELECT COUNT(c) as id FROM NomineeDetails c WHERE c.socialCategory=:socialCategory");
			query.setParameter("socialCategory", socialCategory);
			session.getTransaction().commit();
			return query.getSingleResult().getId();
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
		return null;
	}
	
	@Override
	public int getNomineeBySocialCat(String socialCategory) {
		try {
			
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<NomineeDetails> query = session.createQuery("SELECT c as id FROM NomineeDetails c WHERE c.socialCategory=:socialCategory");
			query.setParameter("socialCategory", socialCategory);
			session.getTransaction().commit();
			List<NomineeDetails> list = query.getResultList();
			int listCount = list.size();
			return listCount;
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
		return 0;
	}
	
	@Override
	public NomineeDetails getNomineeByContact(String contact) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<NomineeDetails> query =  session.createQuery("SELECT n FROM NomineeDetails n WHERE n.contact=:contact");
			query.setParameter("contact", contact);
			session.getTransaction().commit();
			return query.getSingleResult();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}
	
	@Override
	public List<NomineeDetails> getAllNomineeByRegistration(String registration) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<NomineeDetails> query =  session.createQuery("SELECT n FROM NomineeDetails n WHERE n.registrationNumber=:registration");
			query.setParameter("registration", registration);
			session.getTransaction().commit();
			return query.getResultList();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public List<NomineeDetails> getNomineeByState(String... state) {
		List<NomineeDetails> stateList = new ArrayList<>();
		for(String x : state){
				
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<NomineeDetails> query =  session.createQuery("SELECT e FROM NomineeDetails e WHERE e.state=:state");
			query.setParameter("state", x);
			stateList.addAll(query.getResultList());
			session.getTransaction().commit();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
		}
		return stateList;
	}

	@Override
	public List<NomineeDetails> getNomineeByPWD() {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<NomineeDetails> query =  session.createQuery("SELECT v FROM NomineeDetails v WHERE v.pwd=:pwd");
			query.setParameter("pwd", true);
			session.getTransaction().commit();
			return query.getResultList();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public List<NomineeDetails> getNomineeByAwardSubCategory(String awardSub) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<NomineeDetails> query =  session.createQuery("SELECT s FROM NomineeDetails s WHERE s.awardSub=:awardsub");
			query.setParameter("awardsub", awardSub);
			session.getTransaction().commit();
			return query.getResultList();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}
	
	@Override
	public List<NomineeDetails> getNomineeBySpecialCategory(String speccatval) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<NomineeDetails> query =  session.createQuery("SELECT t FROM NomineeDetails t WHERE t.speccatval=:speccatval");
			query.setParameter("speccatval", speccatval);
			session.getTransaction().commit();
			return query.getResultList();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public boolean changeNomineeStatusRemark(String emailId, String remark){
		try{
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<NomineeDetails> query=  session.createQuery("UPDATE NomineeDetails t SET t.filledStatus= 'DECLINED' , t.remark=:remark where t.emailId=:emailId ");
			query.setParameter("emailId", emailId);
			query.setParameter("remark", remark);
			query.executeUpdate();
			session.getTransaction().commit();
			return true;
		}
		catch(Exception ex){
			LOGGER.error(ex.getMessage());
			return false;
		}
	}

	public boolean changeNomineeStatus(String emailId, String remark) {
		try{
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<NomineeDetails> query =  session.createQuery("UPDATE NomineeDetails t SET t.filledStatus= 'ACCEPTED' , t.remark=:remark where t.emailId=:emailId");
			query.setParameter("emailId", emailId);
			query.setParameter("remark", remark);
			query.executeUpdate();
			session.getTransaction().commit();
			return true;
		}
		catch(Exception ex){
			LOGGER.error(ex.getMessage());
			return false;
		}
	}
	
	@Override
	public boolean changeNomineeStatusPending(String emailId, String remark){
		try{
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<NomineeDetails> query=  session.createQuery("UPDATE NomineeDetails t SET t.filledStatus= 'PENDING' , t.remark=:remark where t.emailId=:emailId ");
			query.setParameter("emailId", emailId);
			query.setParameter("remark", remark);
			query.executeUpdate();
			session.getTransaction().commit();
			return true;
			}
		catch(Exception ex){
			LOGGER.error(ex.getMessage());
			return false;
		}
	}
	
	@Override
	public List<NomineeDetails> getNomineeAcceptedCount() {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<NomineeDetails> query =  session.createQuery("SELECT n FROM NomineeDetails n where n.filledStatus= 'ACCEPTED'");
			session.getTransaction().commit();
			return query.getResultList();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}
	
	@Override
	public List<NomineeDetails> getNomineePendingCount() {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<NomineeDetails> query =  session.createQuery("SELECT n FROM NomineeDetails n where n.filledStatus= 'PENDING'");
			session.getTransaction().commit();
			return query.getResultList();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}
	
	@Override
	public List<NomineeDetails> getNomineeDeclinedCount() {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<NomineeDetails> query =  session.createQuery("SELECT n FROM NomineeDetails n where n.filledStatus= 'DECLINED'");
			session.getTransaction().commit();
			return query.getResultList();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}
	
	@Override
	public List<NomineeDetails> getAllSpecialCat() {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<NomineeDetails> query =  session.createQuery("SELECT n FROM NomineeDetails n where n.speccatopt=:speccatopt");
			query.setParameter("speccatopt", true);
			session.getTransaction().commit();
			return query.getResultList();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}
	
	@Override
	public List<NomineeDetails> getMISCount(String states, String category){
		int count = 1;
		String sqlQuery = "SELECT n from NomineeDetails n";
		if( category!=null && !category.equals("")) {
			sqlQuery += " where n.enterpriseCategory = '" + category +"'";
		}
		if( states!=null && !states.equals("")) {
			if(count == 1) {
				sqlQuery += " and  n.state IN (";
				count++;
			}
			for(String state : states.split(",")) {
				sqlQuery += "'" + state +"',";
			}
			sqlQuery = sqlQuery.substring(0, sqlQuery.length()-1);
			sqlQuery += ") GROUP BY n.id";
		}
		
		
		
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<NomineeDetails> query =  session.createQuery(sqlQuery);
			session.getTransaction().commit();
			return query.getResultList();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public List<String> getDistinctAwardSubCategory() {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<String> query =  session.createQuery("SELECT distinct(n.awardSub) from NomineeDetails n");
			session.getTransaction().commit();
			return query.getResultList();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public List<NomineeDetails> getMISReport(String state, String awardcat, String awardsub) {
		int count = 1;
		String sqlQuery = "SELECT n from NomineeDetails n where";
		if( state!=null && !state.equals("")) {
			if(count == 1) {
				sqlQuery += " n.state = '"+state+"'";
				count++;
			}
		}
		
		if(awardcat!=null && !awardcat.equals("")) {
			if(count > 1) {
				sqlQuery += " and";
			}
			sqlQuery += " n.enterpriseCategory = '"+awardcat+"'";
			count++;
		}
		
		if(awardsub!=null && !awardsub.equals("")) {
			if(count > 1) {
				sqlQuery += " and";
			}
			sqlQuery += " n.awardSub = '"+awardsub+"'";
			count++;
		}
		
		
		
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<NomineeDetails> query =  session.createQuery(sqlQuery);
			session.getTransaction().commit();
			return query.getResultList();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
		
	}

	@Override
	public List<NomineeDetails> getAllShortlistedNominee() {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<NomineeDetails> query =  session.createQuery("SELECT n FROM NomineeDetails n WHERE n.shortlisted!=''");
			session.getTransaction().commit();
			return query.getResultList();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public List<NomineeDetails> getResultByQuery(String query, Map<String, Object> map) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<NomineeDetails> q = session.createQuery(query);
			q.setProperties(map);
			session.getTransaction().commit();
			return q.getResultList();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}
	
	@Override
	public List<NomineeDetails> getNomineeRegionalShortlist(String... state) {
		List<NomineeDetails> stateList = new ArrayList<>();
		for(String x : state){
				
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<NomineeDetails> query =  session.createQuery("SELECT e FROM NomineeDetails e WHERE e.shortlisted='Shortlisted' and e.state=:state");
			query.setParameter("state", x);
			stateList.addAll(query.getResultList());
			session.getTransaction().commit();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
		}
		return stateList;
	}
	
	@Override
	public List<NomineeDetails> getAllPromotedNomineeFiles() {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<NomineeDetails> query =  session.createQuery("SELECT n FROM NomineeDetails n WHERE n.promoteStatus='Yes' and n.fileName!=''");
			session.getTransaction().commit();
			return query.getResultList();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public List<NomineeDetails> getAllPromotedNominee() {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<NomineeDetails> query =  session.createQuery("SELECT n FROM NomineeDetails n WHERE n.promoteStatus='Yes'");
			session.getTransaction().commit();
			return query.getResultList();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}
}
