package com.xlri.awards.controller;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.xlri.awards.common.CommonUtil;
import com.xlri.awards.common.EncryptionUtil;
import com.xlri.awards.common.OtpSalt;
import com.xlri.awards.common.PDFfileGeneration;
import com.xlri.awards.common.SMSUtil;
import com.xlri.awards.dao.entity.Nominator;
import com.xlri.awards.dao.entity.NomineeDetails;
import com.xlri.awards.dao.service.api.HttpClientPostService;
import com.xlri.awards.dao.service.api.MailService;
import com.xlri.awards.dao.service.api.NominatorService;
import com.xlri.awards.dao.service.api.NomineeService;
import com.xlri.awards.dao.service.api.SecondCatNomineeService;
import com.xlri.awards.dao.service.api.ThirdCatNomineeService;


@Controller
public class NomineeController {

	@Autowired
	MailService mailService;

	@Autowired
	NominatorService nominatorService;
	
	@Autowired
	NomineeService nomineeService;

	@Autowired
	SecondCatNomineeService secNomineeService;
	
	@Autowired
	ThirdCatNomineeService thirdNomineeService;
	
	@Autowired
	HttpClientPostService postService;
	
	@RequestMapping(value = "/nomineelanding", method = RequestMethod.GET)
	public ModelAndView viewNomineeLanding(HttpServletRequest request) {
		ModelAndView model=new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String emailId = auth.getPrincipal().toString();
		Collection<? extends GrantedAuthority> roles = auth.getAuthorities();
		for(GrantedAuthority role:roles){
			if(role.getAuthority().equals("ROLE_NOMINEE")){
				NomineeDetails nominee = nomineeService.getNomineeByEmailId(emailId);
				model.addObject("nominee", nominee);
				model.addObject("secnom", secNomineeService.getNomineeByContact(nominee.getContact()));
				model.addObject("thirdnom", thirdNomineeService.getNomineeByContact(nominee.getContact()));
				model.addObject("nominatorName",nominatorService.getNominatorByRegistrationNumber(nominee.getRegistrationNumber()));
				model.addObject("encryptedID", EncryptionUtil.encrypt(emailId, OtpSalt.SALT.toString()));
				model.setViewName("nomineelanding");
			}
		}
		
		
		return model;
	}
	
	
	@RequestMapping(value = "/basicdetails", method = RequestMethod.GET)
	public ModelAndView viewNominee(HttpServletRequest request) {
		ModelAndView model = new ModelAndView("nomineedetail");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String emailId = auth.getPrincipal().toString();
		Nominator nominator = nominatorService.getNominatorByEmailId(emailId);
		
		if(request.getParameter("p1")!=null && request.getParameter("p2")!=null && request.getParameter("p3")!=null){
			String nomineeEmail = EncryptionUtil.decrypt(request.getParameter("p2"), OtpSalt.SALT.toString());
			NomineeDetails nominee = nomineeService.getNomineeByEmailId(nomineeEmail);
			if(nominee!=null){
				if(!nominee.getEmailId().contains(nominator.getRegistrationNumber())){	
					nominee.setEmailId(nominator.getRegistrationNumber()+nominee.getEmailId());
					nominee.setContact(nominator.getRegistrationNumber()+nominee.getContact());
				}
			nomineeService.updateNominee(nominee);
			}
			model.addObject("nomineeBack",nominee);
			model.addObject("formtype",request.getParameter("p3"));
		}
		
		
		model.addObject("count", nominator.getEnterpriseCount());
		model.addObject("registrationNumber", nominator.getRegistrationNumber());
		model.addObject("fullName", nominator.getFullName());
		return model;
	}

	@RequestMapping(value = "/basicdetail", method = RequestMethod.POST)
	public ModelAndView registerNominee(HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String emailId = auth.getPrincipal().toString();
		Nominator nominator = nominatorService.getNominatorByEmailId(emailId);
		
		NomineeDetails nominee = new NomineeDetails();
		try{
			nominee.setRegistrationNumber(request.getParameter("registrationNumber"));
			nominee.setAwardCategory(request.getParameter("awardCategory"));
			nominee.setNomineeName(request.getParameter("nomineeName"));			
			nominee.setGender(request.getParameter("gender"));
			nominee.setEnterpriseName(request.getParameter("enterpriseName"));
			nominee.setAddress1(request.getParameter("address1"));
			nominee.setAddress2(request.getParameter("address2"));
			nominee.setStreet(request.getParameter("street"));
			nominee.setCity(request.getParameter("city"));
			nominee.setLandmark(request.getParameter("landmark"));
			nominee.setState(request.getParameter("state"));
			nominee.setPinCode(request.getParameter("pinCode"));
			nominee.setWebsite(request.getParameter("website"));
			nominee.setContact(request.getParameter("contactNumber"));
			nominee.setEmailId(request.getParameter("emailId"));
			
			if(nominee.getAwardCategory().equals("enterprise")){
				nominee.setEnterpriseCategory(request.getParameter("enterpriseCategory"));
				if(request.getParameter("enterpriseCategory").equals("onelac")){
					nominee.setAwardSub(request.getParameter("awardsub1"));
				}else if(request.getParameter("enterpriseCategory").equals("uptoten")){
					nominee.setAwardSub(request.getParameter("awardsub2"));
				}else if(request.getParameter("enterpriseCategory").equals("uptocrore")){
					nominee.setAwardSub(request.getParameter("awardsub3"));
				}else{
					nominee.setAwardSub(request.getParameter(""));
				}
				nominee.setSpecifyproduct(request.getParameter("specifyproduct"));
				nominee.setDob(request.getParameter("dob"));
				nominee.setSocialCategory(request.getParameter("socialCategory"));
				nominee.setPwd(request.getParameter("pwd").equals("yes")?true:false);
				nominee.setSpeccatopt(request.getParameter("speccatopt").equals("yes")?true:false);
				String vals = "";
				if(request.getParameter("speccatval1")!=null){
					vals += request.getParameter("speccatval1")+",";
				}
				if(request.getParameter("speccatval2")!=null){
					vals += request.getParameter("speccatval2")+",";
				}
				if(request.getParameterValues("speccatval3")!=null){
					vals += request.getParameter("speccatval3")+",";
				}
				if(request.getParameterValues("speccatval4")!=null){
					vals += request.getParameter("speccatval4")+",";
				}
				if(!vals.equals("")){
					vals = vals.substring(0, vals.length()-1);
					nominee.setSpeccatval(vals);
				}
				if(request.getParameter("knowNominee").equals("other")){
					nominee.setKnowNominee(request.getParameter("otherNom"));
				}else{
					nominee.setKnowNominee(request.getParameter("knowNominee"));
				}
			}
			if(nominee.getAwardCategory().equals("ecosystem")){
				nominee.setRoleNominee(request.getParameter("roleNominee"));
				nominee.setImpactNominee(request.getParameter("impactNominee"));
				nominee.setRecognitionNominee(request.getParameter("recognitionNominee"));
				nominee.setCollaborationNominee(request.getParameter("collaborationNominee"));
				nominee.setSpecMentionNominee(request.getParameter("specMentionNominee"));
			}
			nominee.setRole("NOMINEE");
			String password = "Dummy";
			nominee.setPassword(password);
			nominee.setFilledStatus("PENDING");
			boolean status = false;

			if(nomineeService.getNomineeByEmailId(nominee.getRegistrationNumber()+request.getParameter("emailId"))!=null){
				NomineeDetails oldNominee = nomineeService.getNomineeByEmailId(nominee.getRegistrationNumber()+request.getParameter("emailId"));
				nominee.setId(oldNominee.getId());
				status = nomineeService.updateNominee(nominee);
			}else{
				status = nomineeService.saveNominee(nominee);
			}
			if (status) {

				
				if(nominee.getAwardCategory().equals("enterprise") && nominee.getEnterpriseCategory().equals("onelac")){
					model.addObject("fullName", nominator.getFullName());
					model.addObject("nominee", nominee);
					model.addObject("registrationNumber", nominator.getRegistrationNumber());
					model.addObject("encryptedEmail", EncryptionUtil.encrypt(nominee.getEmailId(), OtpSalt.SALT.toString()));
					model.setViewName("firstcat");
				}
				if(nominee.getAwardCategory().equals("enterprise") && nominee.getEnterpriseCategory().equals("uptoten")){
					model.addObject("fullName", nominator.getFullName());
					model.addObject("nominee", nominee);
					model.addObject("registrationNumber", nominator.getRegistrationNumber());
					model.addObject("encryptedEmail", EncryptionUtil.encrypt(nominee.getEmailId(), OtpSalt.SALT.toString()));
					model.setViewName("secondcat");
				}
				if(nominee.getAwardCategory().equals("enterprise") && nominee.getEnterpriseCategory().equals("uptocrore")){
					model.addObject("fullName", nominator.getFullName());
					model.addObject("nominee", nominee);
					model.addObject("registrationNumber", nominator.getRegistrationNumber());
					model.addObject("encryptedEmail", EncryptionUtil.encrypt(nominee.getEmailId(), OtpSalt.SALT.toString()));
					model.setViewName("thirdcat");
				}
				if(nominee.getAwardCategory().equals("ecosystem")){
					model.addObject("fullName", nominator.getFullName());
					model.addObject("nominee", nominee);
					model.addObject("registrationNumber", nominator.getRegistrationNumber());
					model.addObject("encryptedEmail", EncryptionUtil.encrypt(nominee.getEmailId(), OtpSalt.SALT.toString()));
					model.setViewName("secondecoform");
				}
				//model.addObject("successMsg","Nomination has been successfully done! An email and SMS has also been sent to the Nominee.");		
				return model;
			}
		}
		catch(Exception ex){
			ex.printStackTrace();
		} 
		model.setViewName("redirect:/nominatorlanding");
		model.addObject("errorMsg","Something went wrong! Please try again.");		
		return model;
	}

	@RequestMapping(value = "/neas/api/checkecosystemcount", method = RequestMethod.POST)
	public void checkEcosystemCount(HttpServletRequest request,HttpServletResponse response) throws IOException {
		response.setContentType("application/json");
		
		String emailId = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
		Nominator nominator = nominatorService.getNominatorByEmailId(emailId);
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("eco", nominator.getEcosystemCount());
			jsonObject.put("ent", nominator.getEnterpriseCount());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		response.getWriter().write(jsonObject.toString());
	}
	
	
	
	@RequestMapping(value = "/basicdetail/pending/partb/{encryptedEmail}", method = RequestMethod.POST)
	public ModelAndView getPartBNominee(@PathVariable("encryptedEmail") String encryptedEmail,HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		
		NomineeDetails nominee = nomineeService.getNomineeByEmailId(request.getParameter("emailId"));
		String emailId = EncryptionUtil.decrypt(encryptedEmail, OtpSalt.SALT.toString());
		Nominator nominator = nominatorService.getNominatorByEmailId(emailId);
		
		if(nominee.getAwardCategory().equals("enterprise") && nominee.getEnterpriseCategory().equals("onelac")){
			model.addObject("fullName", nominator.getFullName());
			model.addObject("nominee", nominee);
			model.addObject("registrationNumber", nominator.getRegistrationNumber());
			model.addObject("encryptedEmail", EncryptionUtil.encrypt(nominee.getEmailId(), OtpSalt.SALT.toString()));
			model.setViewName("firstcat");
		}
		if(nominee.getAwardCategory().equals("enterprise") && nominee.getEnterpriseCategory().equals("uptoten")){
			model.addObject("fullName", nominator.getFullName());
			model.addObject("nominee", nominee);
			model.addObject("registrationNumber", nominator.getRegistrationNumber());
			model.addObject("encryptedEmail", EncryptionUtil.encrypt(nominee.getEmailId(), OtpSalt.SALT.toString()));
			model.setViewName("secondcat");
		}
		if(nominee.getAwardCategory().equals("enterprise") && nominee.getEnterpriseCategory().equals("uptocrore")){
			model.addObject("fullName", nominator.getFullName());
			model.addObject("nominee", nominee);
			model.addObject("registrationNumber", nominator.getRegistrationNumber());
			model.addObject("encryptedEmail", EncryptionUtil.encrypt(nominee.getEmailId(), OtpSalt.SALT.toString()));
			model.setViewName("thirdcat");
		}
		if(nominee.getAwardCategory().equals("ecosystem")){
			model.addObject("fullName", nominator.getFullName());
			model.addObject("nominee", nominee);
			model.addObject("registrationNumber", nominator.getRegistrationNumber());
			model.addObject("encryptedEmail", EncryptionUtil.encrypt(nominee.getEmailId(), OtpSalt.SALT.toString()));
			model.setViewName("secondecoform");
		}
		return model;
	}
	
	@RequestMapping(value = "/basicdetail/partb/{encryptedEmail}", method = RequestMethod.POST)
	public ModelAndView registerPartBNominee(@PathVariable String encryptedEmail,HttpServletRequest request) {
		ModelAndView model=new ModelAndView();
		JSONObject json =new JSONObject();
		NomineeDetails nominee = nomineeService.getNomineeByEmailId(request.getParameter("nomineeEmail"));
		try{
			int q1 = 0,q2 = 0,q3 = 0,q4 = 0,q5 = 0,q6 = 0,q7 = 0,q8 = 0,q9 = 0,q10 = 0,q11 = 0,q12=0,q13=0,q14=0,q15=0,q16=0,q17=0,q18 =0;
			if(request.getParameter("question1")!=null) {
				q1 = Integer.parseInt(request.getParameter("question1"));
			}
			if(request.getParameter("question2")!=null) {
				q2 = Integer.parseInt(request.getParameter("question2"));
			}
			if(request.getParameter("question3")!=null) {
				q3 = Integer.parseInt(request.getParameter("question3"));
			}
			if(request.getParameter("question4")!=null) {
				q4 = Integer.parseInt(request.getParameter("question4"));
			}
			if(request.getParameter("question5")!=null) {
				q5 = Integer.parseInt(request.getParameter("question5"));
			}
			if(request.getParameter("question6")!=null) {
				q6 = Integer.parseInt(request.getParameter("question6"));
			}
			if(request.getParameter("question7")!=null) {
				q7 = Integer.parseInt(request.getParameter("question7"));
			}
			if(request.getParameter("question8")!=null) {
				q8 = Integer.parseInt(request.getParameter("question8"));
			}
			if(request.getParameter("question9")!=null) {
				q9 = Integer.parseInt(request.getParameter("question9"));
			}
			if(request.getParameter("question10")!=null) {
				q10 = Integer.parseInt(request.getParameter("question10"));
			}
			if(request.getParameter("question11")!=null) {
				q11 = Integer.parseInt(request.getParameter("question11"));
			}
			if(request.getParameter("question12")!=null) {
				q12 = Integer.parseInt(request.getParameter("question12"));
			}
			
		if(request.getParameter("category").equals("first")){
			json.put("Growth in customer base for last four years", request.getParameter("question1"));
			json.put("Growth in new units/brances in the last four years", request.getParameter("question2"));
			json.put("Local sourcing of material", request.getParameter("question3"));
			json.put("Local sales as percentage of total sales", request.getParameter("question4"));
			json.put("Good governance/management practices", request.getParameter("question5"));
			json.put("Simplicity of Technology used", request.getParameter("question6"));
			json.put("Innovative technology/processes", request.getParameter("question7"));
			json.put("Fair business practices", request.getParameter("question8"));
			json.put("Equal opportunity to all", request.getParameter("question9"));
			json.put("Percentage of employees from weaker sections", request.getParameter("question10"));
			json.put("Fair labor practices- minimum wages, security etc.", request.getParameter("question11"));
			json.put("Overall, how will you rate this enterprise vis-a-vis the enterprises in the same category", request.getParameter("question12"));
			json.put("Average number of customers served by the enterprise per day?", request.getParameter("question13"));
			json.put("Number of people employed in the current financial year?", request.getParameter("question14"));
			
			String ques15[] = request.getParameterValues("question15");
			String temp =null;
		    StringBuffer temp15 = new StringBuffer();
		    for(String str : ques15)
		    {
		      temp15.append(str + ", ");
		   
		    } 
		    if(!temp15.equals("")){
		    	temp = temp15.substring(0, temp15.length()-1);
	      }
			
			json.put("Where is the market of the product/service offered by the enterprise?", temp);
			json.put("What is the geographical area coverage (in square/km) by the enterprise’s product/service?", request.getParameter("question16"));
			json.put("Average sale per customer?", request.getParameter("question17"));
			json.put("Describe the story and the significance of the enterprise as a model for the youth.", request.getParameter("question18"));
			double calc = (.4*((q1+q2+q3+q4+q5)/5)) + (.3*((q6+q7)/2)) + (.2*((q8+q9+q10+q11)/4)) + (.1*((q12)));
			nominee.setFormula(String.valueOf(calc));
		}
		if(request.getParameter("category").equals("second")){
			if(request.getParameter("question13")!=null) {
				q13 = Integer.parseInt(request.getParameter("question13"));
			}
			if(request.getParameter("question14")!=null) {
				q14 = Integer.parseInt(request.getParameter("question14"));
			}
			
			json.put("Growth in customer base for last four years", request.getParameter("question1"));
			json.put("Growth in new units/brances in the last four years", request.getParameter("question2"));
			json.put("Local sourcing of material", request.getParameter("question3"));
			json.put("Local sales as percentage of total sales", request.getParameter("question4"));
			json.put("Good governance/management practices", request.getParameter("question5"));
			json.put("Simplicity of Technology used", request.getParameter("question6"));
			json.put("Innovative technology/processes", request.getParameter("question7"));
			json.put("Fair business practices", request.getParameter("question8"));
			json.put("Equal opportunity to all", request.getParameter("question9"));
			json.put("Percentage of employees from weaker sections", request.getParameter("question10"));
			json.put("Fair labor practices- minimum wages, security etc.", request.getParameter("question11"));
			json.put("Environment friendly operation/production", request.getParameter("question12"));
			json.put("Waste disposal practices", request.getParameter("question13"));
			json.put("Overall, how will you rate this enterprise vis-a-vis the enterprises in the same category", request.getParameter("question14"));
			double calc = (.3*((q1+q2+q3+q4+q5)/5)) + (.3*((q6+q7)/2)) + (.2*((q8+q9+q10+q11)/4)) + (.1*((q12+q13)/2)) + (.1*((q14)));
			nominee.setFormula(String.valueOf(calc));
		}
		if(request.getParameter("category").equals("third")){
			if(request.getParameter("question13")!=null) {
				q13 = Integer.parseInt(request.getParameter("question13"));
			}
			if(request.getParameter("question14")!=null) {
				q14 = Integer.parseInt(request.getParameter("question14"));
			}
			if(request.getParameter("question15")!=null) {
				q15 = Integer.parseInt(request.getParameter("question15"));
			}
			if(request.getParameter("question16")!=null) {
				q16 = Integer.parseInt(request.getParameter("question16"));
			}
			json.put("Growth in customer base for last four years", request.getParameter("question1"));
			json.put("Growth in new units/brances in the last four years", request.getParameter("question2"));
			json.put("Local sourcing of material", request.getParameter("question3"));
			json.put("Local sales as percentage of total sales", request.getParameter("question4"));
			json.put("Good governance/management practices", request.getParameter("question5"));
			json.put("Simplicity of Technology used", request.getParameter("question6"));
			json.put("Innovative technology/processes", request.getParameter("question7"));
			json.put("Fair business practices", request.getParameter("question8"));
			json.put("Equal opportunity to all", request.getParameter("question9"));
			json.put("Percentage of employees from weaker sections", request.getParameter("question10"));
			json.put("Fair labor practices- minimum wages, security etc.", request.getParameter("question11"));
			json.put("Environment friendly operation/production", request.getParameter("question12"));
			json.put("Waste disposal practices", request.getParameter("question13"));
			json.put("Benefits customers have derived", request.getParameter("question14"));
			json.put("Poor customers served", request.getParameter("question15"));
			json.put("Overall, how will you rate this enterprise vis-a-vis the enterprises in the same category", request.getParameter("question16"));
			double calc = (.2*((q1+q2+q3+q4+q5)/5)) + (.2*((q6+q7)/2)) + (.2*((q8+q9+q10+q11)/4)) + (.2*((q12+q13)/2)) + (.1*((q14+q15)/2)) + (.1*((q16)));
			nominee.setFormula(String.valueOf(calc));
		}
		if(request.getParameter("category").equals("ecosystem")){
			nominee.setEnterpriseCategory(request.getParameter("builder-category"));
			if(request.getParameter("builder-category").equals("Entrepreneurship Development Institutes/Organisations")){
				json.put("Nature of Programs Offered", request.getParameter("Programs-Offered"));
				json.put("Level of Programs Offered", request.getParameter("Level-Offered"));
				json.put("Programs offered since?", request.getParameter("Programs-Offered"));
				json.put("Number of Professional Resource Persons (Faculty, Guest Faculty, etc.)", request.getParameter("Pro-Resource"));
				json.put("Number of Students Graduated (in past 3 years)", request.getParameter("Student-Graduated"));
				json.put("Number of Students turned Entrepreneurs (in past 3 years)", request.getParameter("Student-Entrepreneurs"));
				json.put("Number of IPRs/Trademarks filed by Students (in past 3 years)", request.getParameter("Student-IPR"));
				json.put("Number of Enterpreneur References being provided", request.getParameter("Enterpreneur-References"));
			}
			if(request.getParameter("builder-category").equals("Mentor")){
				json.put("Number of Mentees Mentored (in past 3 years)", request.getParameter("Mentees-Mentored"));
				json.put("How many Mentees turned Entrepreneurs (in past 3 years)", request.getParameter("Mentees-Entrepreneurs"));
				json.put("Number of IPRs/Trademarks filed by Mentees (in past 3 years)", request.getParameter("Mentees-IPR"));
				json.put("Number of Enterpreneur References being provided", request.getParameter("Enterpreneur-References"));
			}
			if(request.getParameter("builder-category").equals("Incubator")){
				json.put("Number of Incubatees (in past 3 years)", request.getParameter("Incubatees"));
				json.put("Number of Professional Resource Persons (Faculty, Guest Faculty, etc.)", request.getParameter("Professional-Resource"));
				json.put("Quantum of Funds Invested/Granted to Incubatee Enterprises", request.getParameter("Quantum-Funds"));
				json.put("Services Provided", request.getParameter("Services-Provided"));
				json.put("If Capital provided, Total Monetized Equity Value held by Incubator (as on 31 Mar 2018)", request.getParameter("Monetized-Equity"));
				json.put("Number of IPRs/Trademarks filed by Incubatees (in past 3 years)", request.getParameter("Trademarks-IPR"));
				json.put("How many Incubatees turned Entrepreneurs (in past 3 years)", request.getParameter("Incubatees-Entrepreneurs"));
				json.put("Number of Enterpreneur References being provided", request.getParameter("Enterpreneur-References"));
			}
			if(request.getParameter("builder-category").equals("Promoters of Rural Producer Enterprises")){
				json.put("Number of States, Districts, Villages covered?", request.getParameter("Area-Covered"));
				json.put("Total Population covered?", request.getParameter("Total-Population"));
				json.put("Number of Cooperative Societies/Self-Help Groups?", request.getParameter("Cooperative-Societies"));
				json.put("Number of Rural Producers Associated (including through Societies & SHGs)", request.getParameter("Rural-Producers"));
				json.put("What all Industry/Industries are you working with? (eg: Dairy, Handicrafts, etc.)", request.getParameter("Working-Industries"));
			}
	
		}
			if(request.getParameter("question1") == null) {
				nominee.setFormula(null);
			}
			nominee.setRatings(json.toString());
			nominee.setFilledStatus("PENDING");
			String password = CommonUtil.generateNomineePassword();
			nominee.setPassword(password);
			
			//nominee.setEmailId(request.getParameter("emailId"));
			boolean status = nomineeService.updateNominee(nominee);
			if(status){
				int ecoCountLeft = nominatorService.getEcosystemCountLeft(request.getParameter("registrationNumber"));
				int entCountLeft = nominatorService.getEnterpriseCountLeft(request.getParameter("registrationNumber"));
				if(ecoCountLeft > 0 && nominee.getAwardCategory().equals("ecosystem")){
					nominatorService.decreaseEcosystemCount(request.getParameter("registrationNumber"));
				}
				if(entCountLeft > 0 && nominee.getAwardCategory().equals("enterprise")){
					nominatorService.decreaseEnterpriseCount(request.getParameter("registrationNumber"));
				}
				Thread t1 = new Thread(new Runnable() {
					@Override
					public void run() {
						mailService.sendNomineeMail(nominee,password);
					}
				});
				t1.start();
				
				Thread t2 = new Thread(new Runnable() {
					@Override
					public void run() {
						mailService.sendNominatorMail(nominee);
					}
				});
				t2.start();
				new Thread(new Runnable() {
					@Override
					public void run() {
						try{
				new SMSUtil().sendSMS("Congratulation, You have been nominated for National entrepreneurship Awards 2018. Please check your mail for details.", nominee.getContact());
						}catch(Exception e){
							System.out.println(e);
						}
					}
				}).start();
				model.setViewName("redirect:/nominatorlanding");
				model.addObject("successMsg", "You have successfully nominated "+nominee.getEnterpriseName()+"!");
			}
			else{
				model.setViewName("redirect:/nominatorlanding");
				model.addObject("errorMsg", "Something went wrong! Please try again.");
			}
		}catch(Exception e){
			model.setViewName("redirect:/nominatorlanding");
			model.addObject("errorMsg", e.getMessage());
			e.printStackTrace();
		}
		return model;
	}
	
	@RequestMapping(value = "/neas/api/nominee/details/pdf", method = RequestMethod.POST)
	public void/*ModelAndView*/ nomineeDetailsPDF(@RequestParam(required = true) Long id, HttpServletRequest request) {
		
		ModelAndView model=new ModelAndView();
		//JSONObject json =new JSONObject();
		NomineeDetails nominee = nomineeService.getNomineeById(id);
		
		try{
			if(nominee != null) {
				String text=nominee.toString();
				PDFfileGeneration.pdfFile(text);
				
				/*nominee.getRegistrationNumber();
				nominee.getAwardCategory();
				nominee.getEnterpriseCategory();
				nominee.getAwardSub();
				nominee.getSpecifyproduct();
				nominee.getNomineeName();
				nominee.getGender();
				nominee.getSocialCategory();
				nominee.getDob();
				nominee.getSpeccatopt();
				nominee.getSpeccatval();
				nominee.getEnterpriseName();
				nominee.getAddress1();
				nominee.getAddress2();
				nominee.getCity();
				nominee.getState();
				nominee.getLandmark();
				nominee.getStreet();
				nominee.getPinCode();
				nominee.getWebsite();
				nominee.getContact();
				nominee.getEmailId();
				nominee.getRole();
				nominee.getSalt();
				nominee.getRatings();
				nominee.getKnowNominee();
				nominee.getRoleNominee();
				nominee.getImpactNominee();
				nominee.getRecognitionNominee();
				nominee.getCollaborationNominee();
				nominee.getSpecMentionNominee();
				//nominee.getFilledStatus();*/
				
				model.setViewName("redirect:/nominatorlanding");
				model.addObject("success");
				
			}else {
				model.setViewName("redirect:/nominatorlanding");
				model.addObject("errorMsg", "Something went wrong! Please try again.");
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		//return model;
	}
	
	@RequestMapping(value="/openfirstcat", method = RequestMethod.GET)
	public ModelAndView openFirst(){
		ModelAndView model = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String emailId = auth.getPrincipal().toString();
		Nominator nominator = nominatorService.getNominatorByEmailId(emailId);
		model.addObject("fullName", nominator.getFullName());
		model.addObject("registrationNumber", nominator.getRegistrationNumber());
		model.setViewName("firstcat");
		return model;
	}
	@RequestMapping(value="/openseccat", method = RequestMethod.GET)
	public ModelAndView openSecond(){
		ModelAndView model = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String emailId = auth.getPrincipal().toString();
		Nominator nominator = nominatorService.getNominatorByEmailId(emailId);
		model.addObject("fullName", nominator.getFullName());
		model.addObject("registrationNumber", nominator.getRegistrationNumber());
		model.setViewName("secondcat");
		return model;
	}
	@RequestMapping(value="/openthirdcat", method = RequestMethod.GET)
	public ModelAndView openThird(){
		ModelAndView model = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String emailId = auth.getPrincipal().toString();
		Nominator nominator = nominatorService.getNominatorByEmailId(emailId);
		model.addObject("fullName", nominator.getFullName());
		model.addObject("registrationNumber", nominator.getRegistrationNumber());
		model.setViewName("thirdcat");
		return model;
	}
	
	@RequestMapping(value="/openecosystemcat", method = RequestMethod.GET)
	public ModelAndView openEcosystem(){
		ModelAndView model = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String emailId = auth.getPrincipal().toString();
		Nominator nominator = nominatorService.getNominatorByEmailId(emailId);
		model.addObject("fullName", nominator.getFullName());
		model.addObject("registrationNumber", nominator.getRegistrationNumber());
		model.setViewName("secondecoform");
		return model;
	}
	
	@RequestMapping(value = "/anotherbasicdetail", method = RequestMethod.POST)
	public void anotherRegisterNominee(HttpServletRequest request, HttpServletResponse response) {
		
		if(request.getParameter("status").equals("submit")){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String emailId = auth.getPrincipal().toString();
		Nominator nominator = nominatorService.getNominatorByEmailId(emailId);
		
		NomineeDetails nominee = new NomineeDetails();
		try{
			nominee.setRegistrationNumber(nominator.getRegistrationNumber());
			nominee.setAwardCategory(request.getParameter("awardCategory"));
			nominee.setNomineeName(request.getParameter("nomineeName"));			
			nominee.setGender(request.getParameter("gender"));
			nominee.setEnterpriseName(request.getParameter("enterpriseName"));
			nominee.setAddress1(request.getParameter("address1"));
			nominee.setAddress2(request.getParameter("address2"));
			nominee.setStreet(request.getParameter("street"));
			nominee.setCity(request.getParameter("city"));
			nominee.setLandmark(request.getParameter("landmark"));
			nominee.setState(request.getParameter("state"));
			nominee.setPinCode(request.getParameter("pinCode"));
			nominee.setWebsite(request.getParameter("website"));
			nominee.setContact(request.getParameter("contactNumber"));
			nominee.setEmailId(request.getParameter("emailId"));
			nominee.setCreatedOn();
			if(nominee.getAwardCategory().equals("enterprise")){
				nominee.setEnterpriseCategory(request.getParameter("enterpriseCategory"));
				if(request.getParameter("enterpriseCategory").equals("onelac")){
					nominee.setAwardSub(request.getParameter("awardsub1"));
				}else if(request.getParameter("enterpriseCategory").equals("uptoten")){
					nominee.setAwardSub(request.getParameter("awardsub2"));
				}else if(request.getParameter("enterpriseCategory").equals("uptocrore")){
					nominee.setAwardSub(request.getParameter("awardsub3"));
				}else{
					nominee.setAwardSub(request.getParameter(""));
				}
				nominee.setSpecifyproduct(request.getParameter("specifyproduct"));
				nominee.setDob(request.getParameter("dob"));
				nominee.setSocialCategory(request.getParameter("socialCategory"));
				nominee.setPwd(request.getParameter("pwd").equals("yes")?true:false);
				nominee.setSpeccatopt(request.getParameter("speccatopt").equals("yes")?true:false);
				String vals = "";
				if(request.getParameter("speccatval1")!=null){
					vals += request.getParameter("speccatval1")+",";
				}
				if(request.getParameter("speccatval2")!=null){
					vals += request.getParameter("speccatval2")+",";
				}
				if(request.getParameterValues("speccatval3")!=null){
					vals += request.getParameter("speccatval3")+",";
				}
				if(request.getParameterValues("speccatval4")!=null){
					vals += request.getParameter("speccatval4")+",";
				}
				if(!vals.equals("")){
					vals = vals.substring(0, vals.length()-1);
					nominee.setSpeccatval(vals);
				}
				if(request.getParameter("knowNominee").equals("other")){
					nominee.setKnowNominee(request.getParameter("otherNom"));
				}else{
					nominee.setKnowNominee(request.getParameter("knowNominee"));
				}
			}
			if(nominee.getAwardCategory().equals("ecosystem")){
				nominee.setRoleNominee(request.getParameter("roleNominee"));
				nominee.setImpactNominee(request.getParameter("impactNominee"));
				nominee.setRecognitionNominee(request.getParameter("recognitionNominee"));
				nominee.setCollaborationNominee(request.getParameter("collaborationNominee"));
				nominee.setSpecMentionNominee(request.getParameter("specMentionNominee"));
			}
			nominee.setRole("NOMINEE");

				String password = "Dummy";
				nominee.setPassword(password);
				nominee.setFilledStatus("PENDING");

			boolean status = false;
			JSONObject json =new JSONObject();
			try{
			if(request.getParameter("category").equals("first")){
				json.put("Growth in customer base for last four years", request.getParameter("question1"));
				json.put("Growth in new units/brances in the last four years", request.getParameter("question2"));
				json.put("Local sourcing of material", request.getParameter("question3"));
				json.put("Local sales as percentage of total sales", request.getParameter("question4"));
				json.put("Good governance/management practices", request.getParameter("question5"));
				json.put("Simplicity of Technology used", request.getParameter("question6"));
				json.put("Innovative technology/processes", request.getParameter("question7"));
				json.put("Fair business practices", request.getParameter("question8"));
				json.put("Equal opportunity to all", request.getParameter("question9"));
				json.put("Percentage of employees from weaker sections", request.getParameter("question10"));
				json.put("Fair labor practices- minimum wages, security etc.", request.getParameter("question11"));
				json.put("Overall, how will you rate this enterprise vis-a-vis the enterprises in the same category", request.getParameter("question12"));
				json.put("Average number of customers served by the enterprise per day?", request.getParameter("question13"));
				json.put("Number of people employed in the current financial year?", request.getParameter("question14"));
				String ques15[] = request.getParameterValues("question15");
				String temp =null;
			    StringBuffer temp15 = new StringBuffer();
			    for(String str : ques15)
			    {
			      temp15.append(str + ", ");
			   
			    } 
			    if(!temp15.equals("")){
			    	temp = temp15.substring(0, temp15.length()-1);
			    }
				json.put("Where is the market of the product/service offered by the enterprise?", temp);
				json.put("What is the geographical area coverage (in square/km) by the enterprise’s product/service?", request.getParameter("question16"));
				json.put("Average sale per customer?", request.getParameter("question17"));
				json.put("Describe the story and the significance of the enterprise as a model for the youth.", request.getParameter("question18"));
			}
			if(request.getParameter("category").equals("second")){
				json.put("Growth in customer base for last four years", request.getParameter("question1"));
				json.put("Growth in new units/brances in the last four years", request.getParameter("question2"));
				json.put("Local sourcing of material", request.getParameter("question3"));
				json.put("Local sales as percentage of total sales", request.getParameter("question4"));
				json.put("Good governance/management practices", request.getParameter("question5"));
				json.put("Simplicity of Technology used", request.getParameter("question6"));
				json.put("Innovative technology/processes", request.getParameter("question7"));
				json.put("Fair business practices", request.getParameter("question8"));
				json.put("Equal opportunity to all", request.getParameter("question9"));
				json.put("Percentage of employees from weaker sections", request.getParameter("question10"));
				json.put("Fair labor practices- minimum wages, security etc.", request.getParameter("question11"));
				json.put("Environment friendly operation/production", request.getParameter("question12"));
				json.put("Waste disposal practices", request.getParameter("question13"));
				json.put("Overall, how will you rate this enterprise vis-a-vis the enterprises in the same category", request.getParameter("question14"));
			}
			if(request.getParameter("category").equals("third")){
				json.put("Growth in customer base for last four years", request.getParameter("question1"));
				json.put("Growth in new units/brances in the last four years", request.getParameter("question2"));
				json.put("Local sourcing of material", request.getParameter("question3"));
				json.put("Local sales as percentage of total sales", request.getParameter("question4"));
				json.put("Good governance/management practices", request.getParameter("question5"));
				json.put("Simplicity of Technology used", request.getParameter("question6"));
				json.put("Innovative technology/processes", request.getParameter("question7"));
				json.put("Fair business practices", request.getParameter("question8"));
				json.put("Equal opportunity to all", request.getParameter("question9"));
				json.put("Percentage of employees from weaker sections", request.getParameter("question10"));
				json.put("Fair labor practices- minimum wages, security etc.", request.getParameter("question11"));
				json.put("Environment friendly operation/production", request.getParameter("question12"));
				json.put("Waste disposal practices", request.getParameter("question13"));
				json.put("Benefits customers have derived", request.getParameter("question14"));
				json.put("Poor customers served", request.getParameter("question15"));
				json.put("Overall, how will you rate this enterprise vis-a-vis the enterprises in the same category", request.getParameter("question16"));
			}
			if(request.getParameter("category").equals("ecosystem")){
				nominee.setEnterpriseCategory(request.getParameter("builder-category"));
				if(request.getParameter("builder-category").equals("Entrepreneurship Development Institutes/Organisations")){
					String prgoffered[] = {request.getParameter("question1"),request.getParameter("question2"),request.getParameter("question3")};
					String temp =null;
				    StringBuffer prg = new StringBuffer();
				    for(String str : prgoffered)
				    {
				      prg.append(str + ", ");
				   
				    } 
				    if(!prg.equals("")){
				    	temp = prg.substring(0, prg.length()-1);
			      }
					json.put("Nature of Programs Offered", temp);
					
					String lvloffered[] = {request.getParameter("question4"),request.getParameter("question5"),request.getParameter("question6"),request.getParameter("question7")};
					String lvltemp =null;
				    StringBuffer lvl = new StringBuffer();
				    for(String str1 : lvloffered)
				    {
				    	lvl.append(str1 + ", ");
				   
				    } 
				    if(!lvl.equals("")){
				    	lvltemp = lvl.substring(0, lvl.length()-1);
			      }
					json.put("Level of Programs Offered", lvltemp);
					
					String dtoffered[] = {request.getParameter("Date-Offered1"),request.getParameter("Date-Offered2"),request.getParameter("Date-Offered3"),request.getParameter("Date-Offered4")};
					String dttemp =null;
				    StringBuffer dt = new StringBuffer();
				    for(String str2 : dtoffered)
				    {
				    	dt.append(str2 + ", ");
				   
				    } 
				    if(!dt.equals("")){
				    	dttemp = dt.substring(0, dt.length()-1);
				    }
					json.put("Programs offered since?", dttemp);
					json.put("Number of Professional Resource Persons (Faculty, Guest Faculty, etc.)", request.getParameter("Pro-Resource"));
					json.put("Number of Students Graduated (in past 3 years)", request.getParameter("Student-Graduated"));
					json.put("Number of Students turned Entrepreneurs (in past 3 years)", request.getParameter("Student-Entrepreneurs"));
					json.put("Number of IPRs/Trademarks filed by Students (in past 3 years)", request.getParameter("Student-IPR"));
					json.put("Number of Enterpreneur References being provided", request.getParameter("Enterpreneur-References"));
				}
				if(request.getParameter("builder-category").equals("Mentor")){
					json.put("Number of Mentees Mentored (in past 3 years)", request.getParameter("Mentees-Mentored"));
					json.put("How many Mentees turned Entrepreneurs (in past 3 years)", request.getParameter("Mentees-Entrepreneurs"));
					json.put("Number of IPRs/Trademarks filed by Mentees (in past 3 years)", request.getParameter("Mentees-IPR"));
					json.put("Number of Enterpreneur References being provided", request.getParameter("Enterpreneur-References"));
				}
				if(request.getParameter("builder-category").equals("Incubator")){
					json.put("Number of Incubatees (in past 3 years)", request.getParameter("Incubatees"));
					json.put("Number of Professional Resource Persons (Faculty, Guest Faculty, etc.)", request.getParameter("Professional-Resource"));
					json.put("Quantum of Funds Invested/Granted to Incubatee Enterprises", request.getParameter("Quantum-Funds"));
					String services[] = request.getParameterValues("Services-Provided");
					String serv =null;
				    StringBuffer tempServ = new StringBuffer();
				    for(String str : services)
				    {
				    	tempServ.append(str + ", ");
				   
				    } 
				    if(!tempServ.equals("")){
				    	serv = tempServ.substring(0, tempServ.length()-1);
				    }
					json.put("Services Provided", serv);
					json.put("If Capital provided, Total Monetized Equity Value held by Incubator (as on 31 Mar 2018)", request.getParameter("Monetized-Equity"));
					json.put("Number of IPRs/Trademarks filed by Incubatees (in past 3 years)", request.getParameter("Trademarks-IPR"));
					json.put("How many Incubatees turned Entrepreneurs (in past 3 years)", request.getParameter("Incubatees-Entrepreneurs"));
					json.put("Number of Enterpreneur References being provided", request.getParameter("Enterpreneur-References"));
				}
				if(request.getParameter("builder-category").equals("Promoters of Rural Producer Enterprises")){
					json.put("Number of States, Districts, Villages covered?", request.getParameter("Area-Covered"));
					json.put("Total Population covered?", request.getParameter("Total-Population"));
					json.put("Number of Cooperative Societies/Self-Help Groups?", request.getParameter("Cooperative-Societies"));
					json.put("Number of Rural Producers Associated (including through Societies & SHGs)", request.getParameter("Rural-Producers"));
					json.put("What all Industry/Industries are you working with? (eg: Dairy, Handicrafts, etc.)", request.getParameter("Working-Industries"));
				}
		
			}
				nominee.setRatings(json.toString());
					nominee.setFilledStatus("PENDING");
					String password1 = CommonUtil.generateNomineePassword();
					nominee.setPassword(password1);
				//nominee.setEmailId(request.getParameter("emailId"));
				boolean status1 = nomineeService.saveNominee(nominee);
				if(status1){
					int ecoCountLeft = nominatorService.getEcosystemCountLeft(request.getParameter("registrationNumber"));
					int entCountLeft = nominatorService.getEnterpriseCountLeft(request.getParameter("registrationNumber"));
					if(ecoCountLeft > 0 && nominee.getAwardCategory().equals("ecosystem")){
						nominatorService.decreaseEcosystemCount(request.getParameter("registrationNumber"));
					}
					if(entCountLeft > 0 && nominee.getAwardCategory().equals("enterprise")){
						nominatorService.decreaseEnterpriseCount(request.getParameter("registrationNumber"));
					}
					Thread t1 = new Thread(new Runnable() {
						@Override
						public void run() {
							mailService.sendNomineeMail(nominee,password1);
						}
					});
					t1.start();
					
					Thread t2 = new Thread(new Runnable() {
						@Override
						public void run() {
							mailService.sendNominatorMail(nominee);
						}
					});
					t2.start();
					new Thread(new Runnable() {
						@Override
						public void run() {
							try{
					new SMSUtil().sendSMS("Congratulation, You have been nominated for National entrepreneurship Awards 2018. Please check your mail for details.", nominee.getContact());
				}catch(Exception e){
					System.out.println(e);
				}
			}
		}).start();
					JSONObject obj = new JSONObject();
					obj.put("success", true);
					response.getWriter().println(obj.toString());
				}
				else{
					JSONObject obj = new JSONObject();
					try {
						obj.put("error", true);
					response.getWriter().println(obj.toString());
					} catch (JSONException | IOException e) {
						e.printStackTrace();
					}
				}
			}catch(Exception e){
				e.printStackTrace();
				JSONObject obj = new JSONObject();
				try {
					obj.put("error", true);
				response.getWriter().println(obj.toString());
				} catch (JSONException | IOException e1) {
					e1.printStackTrace();
				}
				
			}

		}
		catch(Exception ex){
			ex.printStackTrace();
			JSONObject obj = new JSONObject();
			try {
				obj.put("error", true);
			response.getWriter().println(obj.toString());
			} catch (JSONException | IOException e) {
				e.printStackTrace();
			}
		}
		}
	}
	
	
	@RequestMapping(value = "/neas/api/createexcel/nominee", method = RequestMethod.GET)
	public void createNomineeExcel(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String[] columns = {"S. No.","Created On","Nominator's Registration Number","Award Track","Award Category","Award Subcategory",
				"Enterprise Name","Nominee's Name","Email Id","Contact","State","Special categiry Opted","Special Categiry Value",
				"Social Category","Gender","Approved Status","PWD","How do you know the Nominee?","Please List/Specify your product",
				"Please describe the role the Nominee is playing as an Eco-System Contributor.","Please describe the impact the Nominee has created by playing their role as Mentor/Incubation Center/Entrepreneurship Development Institutes.",
				"Please mention about the recognitions/awards the Nominee has received in the area of Entrepreneurship development.","Please mention about industry collaborations, Road shows/Bussiness Plan Competitions/Funding Events organised.",
				"Any other special mentions?",
				"Growth in customer base for last four years",
				"Growth in new units/brances in the last four years",
				"Local sourcing of material",
				"Local sales as percentage of total sales",
				"Good governance/management practices",
				"Simplicity of Technology used",
				"Innovative technology/processes",
				"Fair business practices",
				"Equal opportunity to all",
				"Percentage of employees from weaker sections",
				"Fair labor practices- minimum wages, security etc.",
				"Environment friendly operation/production",
				"Waste disposal practices",
				"Benefits customers have derived",
				"Poor customers served",
				"Overall, how will you rate this enterprise vis-a-vis the enterprises in the same category",
				"Average number of customers served by the enterprise per day?",
				"Number of people employed in the current financial year?",
				"Where is the market of the product/service offered by the enterprise?",
				"What is the geographical area coverage (in square/km) by the enterprise's product/service?",
				"Average sale per customer?",
				"Describe the story and the significance of the enterprise as a model for the youth.",
				"Nature of Programs Offered",
           	    "Level of Programs Offered",
           	    "Program Offered Since?",
           	    "Number of Professional Resource Persons (Faculty, Guest Faculty, etc.)",
           	    "Number of Students Graduated (in past 3 years)",
           	    "Number of Students turned Entrepreneurs (in past 3 years)",
           	    "Number of IPRs/Trademarks filed by Students (in past 3 years)",
           	    "Number of Mentees Mentored (in past 3 years)",
				"How many Mentees turned Entrepreneurs (in past 3 years)",
				"Number of IPRs/Trademarks filed by Mentees (in past 3 years)",
				"Number of Incubatees (in past 3 years)",
				"Quantum of Funds Invested/Granted to Incubatee Enterprises",
				"Services Provided?",
				"If Capital provided, Total Monetized Equity Value held by Incubator (as on 31 Mar 2018)",
				"Number of IPRs/Trademarks filed by Incubatees (in past 3 years)",
				"How many Incubatees turned Entrepreneurs (in past 3 years)",
				"Number of Entrepreneur References being provided",
				"Number of States, Districts, Villages covered?",
				"Total Population covered?",
				"Number of Cooperative Societies/Self-Help Groups?",
				"Number of Rural Producers Associated (including through Societies & SHGs)",
				"What all Industry/Industries are you working with? (eg: Dairy, Handicrafts, etc.)",
				"DOB","Address 1","Address 2","Street","Landmark","City","Pin Code","Website"};
		

		List<NomineeDetails> nomineeList = nomineeService.getAllNominee();
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("Nominee Details");
		
		Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.RED.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);

        for(int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }
        
        CreationHelper createHelper = workbook.getCreationHelper();
        CellStyle dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));
        
        int rowNum = 1;
        for(NomineeDetails nominee: nomineeList) {
            Row row = sheet.createRow(rowNum);
            row.createCell(0).setCellValue(rowNum);
            Cell dateOfBirthCell = row.createCell(1);
            dateOfBirthCell.setCellValue(nominee.getCreatedOn());
            dateOfBirthCell.setCellStyle(dateCellStyle);
            row.createCell(2).setCellValue(nominee.getRegistrationNumber());
            row.createCell(3).setCellValue(nominee.getAwardCategory().equals("enterprise")?"Enterprise Award Track":"Ecosystem Builder's Track");
            switch(nominee.getEnterpriseCategory()) {
            case "onelac":	row.createCell(4).setCellValue("Initial Investment upto 1 Lakh");break;
            case "uptoten":	row.createCell(4).setCellValue("Initial Investment between 1 Lakh to 10 Lakh");break;
            case "uptocrore":	row.createCell(4).setCellValue("Initial Investment between 10 Lakh to 1 Crore");break;
            default: row.createCell(4).setCellValue(nominee.getEnterpriseCategory());break;
            }
            row.createCell(5).setCellValue(nominee.getAwardSub());
            row.createCell(6).setCellValue(nominee.getEnterpriseName());
            row.createCell(7).setCellValue(nominee.getNomineeName());
            row.createCell(8).setCellValue(nominee.getEmailId());
            row.createCell(9).setCellValue(nominee.getContact());
            row.createCell(10).setCellValue(nominee.getState());
            row.createCell(11).setCellValue(nominee.getSpeccatopt()?"Yes":"No");
            row.createCell(12).setCellValue(nominee.getSpeccatval());
            row.createCell(13).setCellValue(nominee.getSocialCategory());
            row.createCell(14).setCellValue(nominee.getGender());
            row.createCell(15).setCellValue(nominee.getFilledStatus());
            row.createCell(16).setCellValue(nominee.isPwd()?"Yes":"No");
            row.createCell(17).setCellValue(nominee.getKnowNominee());
            row.createCell(18).setCellValue(nominee.getSpecifyproduct());
            row.createCell(19).setCellValue(nominee.getRoleNominee());
            row.createCell(20).setCellValue(nominee.getImpactNominee());
            row.createCell(21).setCellValue(nominee.getRecognitionNominee());
            row.createCell(22).setCellValue(nominee.getCollaborationNominee());
            row.createCell(23).setCellValue(nominee.getSpecMentionNominee());
            
            JSONObject jsonObj = new JSONObject(nominee.getRatings());
            Iterator<String> keys = jsonObj.keys();
            while(keys.hasNext()) {
            	String key = keys.next();
            	switch(key) {
            	case "Growth in customer base for last four years":	row.createCell(24).setCellValue(jsonObj.get(key).toString());break;
            	case "Growth in new units/brances in the last four years":	row.createCell(25).setCellValue(jsonObj.get(key).toString());break;
            	case "Local sourcing of material":	row.createCell(26).setCellValue(jsonObj.get(key).toString());break;
            	case "Local sales as percentage of total sales":	row.createCell(27).setCellValue(jsonObj.get(key).toString());break;
            	case "Good governance/management practices":	row.createCell(28).setCellValue(jsonObj.get(key).toString());break;
            	case "Simplicity of Technology used":	row.createCell(29).setCellValue(jsonObj.get(key).toString());break;
            	case "Innovative technology/processes":	row.createCell(30).setCellValue(jsonObj.get(key).toString());break;
            	case "Fair business practices":	row.createCell(31).setCellValue(jsonObj.get(key).toString());break;
            	case "Equal opportunity to all":	row.createCell(32).setCellValue(jsonObj.get(key).toString());break;
            	case "Percentage of employees from weaker sections":	row.createCell(33).setCellValue(jsonObj.get(key).toString());break;
            	case "Fair labor practices- minimum wages, security etc.":	row.createCell(34).setCellValue(jsonObj.get(key).toString());break;
				case "Environment friendly operation/production":	row.createCell(35).setCellValue(jsonObj.get(key).toString());break;
				case "Waste disposal practices":	row.createCell(36).setCellValue(jsonObj.get(key).toString());break;
				case "Benefits customers have derived":	row.createCell(37).setCellValue(jsonObj.get(key).toString());break;
				case "Poor customers served":	row.createCell(38).setCellValue(jsonObj.get(key).toString());break;	
            	case "Overall, how will you rate this enterprise vis-a-vis the enterprises in the same category":	row.createCell(39).setCellValue(jsonObj.get(key).toString());break;
            	case "Average number of customers served by the enterprise per day?":	row.createCell(40).setCellValue(jsonObj.get(key).toString());break;
            	case "Number of people employed in the current financial year?":	row.createCell(41).setCellValue(jsonObj.get(key).toString());break;
            	case "Where is the market of the product/service offered by the enterprise?":	row.createCell(42).setCellValue(jsonObj.get(key).toString());break;
            	case "What is the geographical area coverage (in square/km) by the enterprise�s product/service?":	row.createCell(43).setCellValue(jsonObj.get(key).toString());break;
            	case "Average sale per customer?":	row.createCell(44).setCellValue(jsonObj.get(key).toString());break;
            	case "Describe the story and the significance of the enterprise as a model for the youth.":	row.createCell(45).setCellValue(jsonObj.get(key).toString());break;
            	case "Nature of Programs Offered":	row.createCell(46).setCellValue(jsonObj.get(key).toString());break;
            	case "Level of Programs Offered":	row.createCell(47).setCellValue(jsonObj.get(key).toString());break;
            	case "Program Offered Since?":	row.createCell(48).setCellValue(jsonObj.get(key).toString());break;
            	case "Number of Professional Resource Persons (Faculty, Guest Faculty, etc.)":	row.createCell(49).setCellValue(jsonObj.get(key).toString());break;
            	case "Number of Students Graduated (in past 3 years)":	row.createCell(50).setCellValue(jsonObj.get(key).toString());break;
            	case "Number of Students turned Entrepreneurs (in past 3 years)":	row.createCell(51).setCellValue(jsonObj.get(key).toString());break;
            	case "Number of IPRs/Trademarks filed by Students (in past 3 years)":	row.createCell(52).setCellValue(jsonObj.get(key).toString());break;
				case "Number of Mentees Mentored (in past 3 years)":	row.createCell(53).setCellValue(jsonObj.get(key).toString());break;
            	case "How many Mentees turned Entrepreneurs (in past 3 years)":	row.createCell(54).setCellValue(jsonObj.get(key).toString());break;
            	case "Number of IPRs/Trademarks filed by Mentees (in past 3 years)":	row.createCell(55).setCellValue(jsonObj.get(key).toString());break;
            	case "Number of Incubatees (in past 3 years)":	row.createCell(56).setCellValue(jsonObj.get(key).toString());break;
            	case "Quantum of Funds Invested/Granted to Incubatee Enterprises":	row.createCell(57).setCellValue(jsonObj.get(key).toString());break;
            	case "Services Provided?":	row.createCell(58).setCellValue(jsonObj.get(key).toString());break;
            	case "If Capital provided, Total Monetized Equity Value held by Incubator (as on 31 Mar 2018)":	row.createCell(59).setCellValue(jsonObj.get(key).toString());break;
				case "Number of IPRs/Trademarks filed by Incubatees (in past 3 years)":	row.createCell(60).setCellValue(jsonObj.get(key).toString());break;
				case "How many Incubatees turned Entrepreneurs (in past 3 years)":	row.createCell(61).setCellValue(jsonObj.get(key).toString());break;
				case "Number of Entrepreneur References being provided":	row.createCell(62).setCellValue(jsonObj.get(key).toString());break;
				case "Number of States, Districts, Villages covered?":	row.createCell(63).setCellValue(jsonObj.get(key).toString());break;
				case "Total Population covered?":	row.createCell(64).setCellValue(jsonObj.get(key).toString());break;
				case "Number of Cooperative Societies/Self-Help Groups?":	row.createCell(65).setCellValue(jsonObj.get(key).toString());break;
				case "Number of Rural Producers Associated (including through Societies & SHGs)":	row.createCell(66).setCellValue(jsonObj.get(key).toString());break;
				case "What all Industry/Industries are you working with? (eg: Dairy, Handicrafts, etc.)":	row.createCell(67).setCellValue(jsonObj.get(key).toString());break;
            	
            	}
            }
            
            
            
            row.createCell(68).setCellValue(nominee.getDob());
            row.createCell(69).setCellValue(nominee.getAddress1());
            row.createCell(70).setCellValue(nominee.getAddress2());
            row.createCell(71).setCellValue(nominee.getStreet());
            row.createCell(72).setCellValue(nominee.getLandmark());
            row.createCell(73).setCellValue(nominee.getCity());
            row.createCell(74).setCellValue(nominee.getPinCode());
            row.createCell(75).setCellValue(nominee.getWebsite());
            rowNum++;
        }
		
		response.setContentType("text/plain");
		response.setHeader("Content-disposition", "attachment; filename=NomineeDetails.xlsx");
		workbook.write(response.getOutputStream());
		
	}
	
}
