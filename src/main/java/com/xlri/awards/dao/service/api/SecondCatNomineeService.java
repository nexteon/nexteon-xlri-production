package com.xlri.awards.dao.service.api;

import java.util.List;

import com.xlri.awards.dao.entity.SecondCatNominee;

public interface SecondCatNomineeService {

	public boolean saveNominee(SecondCatNominee nominee);
	
	public List<SecondCatNominee> getAllSecCatNom();

	public SecondCatNominee getNomineeByEmailId(String emailId);

	public SecondCatNominee getNomineeByContact(String contact);

	public boolean updateNominee(SecondCatNominee secNominee);

}
