<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <%@include file="fragments/headerlibs.jsp" %>
    <body class="home">
        <%@include file="fragments/header.jsp" %>
        <!-- Body Starts Here -->
        <div class="banner-outer">
            <div class="banner" role="banner">
                <div class="owl-one owl-carousel owl-theme">
                    <div class="item">
                        <img src="/static/images/Rubber Anglong, Assam.jpg" alt="Banner 1"/>
                        <div class="banner-txt">
                            <div class="bnr-hdg">NATIONAL ENTREPRENEURSHIP Awards 2018</div>
                            <p>Honouring Young Entrepreneurs and Eco-system Builders for their outstanding efforts in Entrepreneurship Development</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="/static/images/website.jpg" alt="Banner 2"/>
                        <div class="banner-txt">
                            <div class="bnr-hdg">NATIONAL ENTREPRENEURSHIP Awards 2018</div>
                            <p>Honouring Young Entrepreneurs and Eco-system Builders for their outstanding efforts in Entrepreneurship Development</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="/static/images/Women.jpg" alt="Banner 3"/>
                        <div class="banner-txt">
                            <div class="bnr-hdg">NATIONAL ENTREPRENEURSHIP Awards 2018</div>
                            <p>Honouring Young Entrepreneurs and Eco-system Builders for their outstanding efforts in Entrepreneurship Development</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="/static/images/Terracota.jpg" alt="Banner 4"/>
                        <div class="banner-txt">
                            <div class="bnr-hdg">NATIONAL ENTREPRENEURSHIP Awards 2018</div>
                            <p>Honouring Young Entrepreneurs and Eco-system Builders for their outstanding efforts in Entrepreneurship Development</p>
                        </div>
                    </div>
                </div>
                <div class="pm-image">
                    <img src="/static/images/prime-minister.png" alt="prime minister image">
                </div>
                <div class="banner-txt2">
                    <img src="/static/images/pm-yuva-yojana.png" alt="">
                </div>
            </div>
            <div class="banner-gal">
                <div class="banner-gal-hdg">
                    Nomination Deadline Extended. Nominations are now accepted till Nov 22, 2018 by 23:59:59
                </div>
                <div class="banner-gal-sub">
                    Past Awardees and Rural Entrepreneurs
                </div>
                <div class="banner-gal-inn">
                    <div class="owl-two owl-carousel owl-theme">
                        <div class="item">
                            <div class="item-box">
                                <img src="/static/images/winners/winner1.jpg" alt="" class="winners-img">
                            </div>
                        </div>
                        <div class="item">
                            <div class="item-box">
                                <img src="/static/images/winners/winner2.jpg" alt="" class="winners-img">
                            </div>
                        </div>
                        <div class="item">
                            <div class="item-box">
                                <img src="/static/images/winners/winner3.jpg" alt="" class="winners-img">
                            </div>
                        </div>
                        <div class="item">
                            <div class="item-box">
                                <img src="/static/images/winners/winner4.jpg" alt="" class="winners-img">
                            </div>
                        </div>
                        <div class="item">
                            <div class="item-box">
                                <img src="/static/images/winners/winner5.jpg" alt="" class="winners-img">
                            </div>
                        </div>
                        <div class="item">
                            <div class="item-box">
                                <img src="/static/images/winners/winner6.jpg" alt="" class="winners-img">
                            </div>
                        </div>
                        <div class="item">
                            <div class="item-box">
                                <img src="/static/images/winners/winner7.jpg" alt="" class="winners-img">
                            </div>
                        </div>
                        <div class="item">
                            <div class="item-box">
                                <img src="/static/images/winners/winner8.jpg" alt="" class="winners-img">
                            </div>
                        </div>
                        <div class="item">
                            <div class="item-box">
                                <img src="/static/images/winners/winner9.jpg" alt="" class="winners-img">
                            </div>
                        </div>
                        <div class="item">
                            <div class="item-box">
                                <img src="/static/images/winners/winner10.jpg" alt="" class="winners-img">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <main role="main">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="main-content">
                            
                            <div class="col-md-7">
                                <div class="about-des">
                                    <ul>
                                        <li>
                                            <i class="fa fa-address-card"></i>
                                            <a href="/neas/about/overview">
                                                
                                                <div class="circle-box">OVERVIEW</div>
                                            </a>
                                        </li>
                                        <li>
                                            <i class="fa fa-trophy"></i>
                                            <a href="/neas/about/award">
                                                
                                                <div class="circle-box">AWARD CATEGORIES</div>
                                            </a>
                                        </li>
                                        <li>
                                            <i class="fa fa-check-square-o"></i>
                                            <a href="/neas/about/eligibility">
                                                
                                                <div class="circle-box">eligibility</div>
                                            </a>
                                        </li>
                                        <li>
                                            <i class="fa fa-gift"></i>
                                            <a href="/neas/about/reward">
                                                
                                                <div class="circle-box">REWARDS</div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            
                  <sec:authorize access="isAnonymous()">
                            <div class="col-md-5">
                                <div class="login-gorm">
                                    <h2>Login</h2>
                                    <form action="/login" method="POST" data-toggle="validator"
                                    role="form">
                                        <div class="form-group">
                                            <label class="hidden" for="usernsme">usernsme</label> <i
                                            class="fa fa-user" aria-hidden="true"></i> <input
                                            placeholder="Email Id" id="usernsme" type="text"
                                            name="emailId" class="form-control" required>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="hidden" for="password">password</label> <i
                                            class="fa fa-lock" aria-hidden="true"></i> <input
                                            placeholder="Password" id="password" type="password"
                                            name="password" class="form-control" required>
                                        </div>
                                        <div class="forgot-pass">
                                            <a href="#" data-toggle="modal" data-target="#openForget">Forgot
                                                Password</a>
                                        </div>
                                        <div class="login-btn text-center">
                                        <input type="hidden" name="currentpath" value=""/>
                                            <input type="hidden" name="${_csrf.parameterName}"
                                            value="${_csrf.token}" /> <input type="submit" name="submit"
                                            class="btn btn-primary btn-block" value="Login">
                                        </div>
                                    </form>
                                    <div class="frm-btn">
                                        <p>
                                            Don't have an Account? <a class="btn btn-primary"
                                            href="/signup">Register Now!</a>
                                        </p>
                                    </div>
                                    
                                </div>
                            </div>
                            </sec:authorize>
                          
                            <div class="clearfix"></div>
                            
                            
                        </div>
                        
                        <div class="winner-section">
                            <h2>AWARD CEREMONY NEA 2016 & 2017</h2>
                            <div class="col-md-3 gov-quot">
                                <div class="owl-three owl-carousel owl-theme">
                                    
                                    <div class="item">
                                   <div class="scroll-txt">
                                   <img src="/static/images/mohandas_pai1.png" alt="">
                                       <div class="scroll-btm">
                                       "Entrepreneurs create jobs, they are risk takers, they are the heroes of our society, because they make thing happens, and as a society, as a government, as a people, we need to recognize them."
                                       </div>
                                       <p><strong>Mr. Mohandas Pai</strong></p>
                                       <p>Chairman of Manipal Global Education (Manipal University)</p>
                                   </div>
                               </div>
                               
                               <div class="item">
                                   <div class="scroll-txt">
                                   <img src="/static/images/Naina-Lal-Kidwai.jpg" alt="">
                                       <div class="scroll-btm">
                                       "These awards are such a wonderful ideas coming from the ministry, the purpose is really to encourage entrepreneurship in the country."
                                       </div>
                                       <p><strong>Ms. Naina Lal Kidwai</strong></p>
                                       <p>Retired Chairman of HSBC Bank</p>
                                   </div>
                               </div>
                                    
                                    
                                    
                                    
                                    
                                </div>
                            </div>
                            <div class="col-md-6 cnt-gallery">
                                <div class="gal-box">
                                    <img class="img-fade1" src="/static/images/gallery/gal1.JPG" alt="">
                                    <img class="img-fade2" src="/static/images/gallery/gal10.JPG" alt="">   
                                </div>  
                                <div class="gal-box">
                                    <img class="img-fade1" src="/static/images/gallery/gal2.JPG" alt="">
                                    <img class="img-fade2" src="/static/images/gallery/gal11.JPG" alt="">
                                    
                                </div> 
                                <div class="gal-box">
                                    <img class="img-fade1" src="/static/images/gallery/gal3.JPG" alt="">
                                    <img class="img-fade2" src="/static/images/gallery/gal12.JPG" alt="">
                                    
                                </div> 
                                <div class="gal-box">
                                    <img class="img-fade1" src="/static/images/gallery/gal4.JPG" alt="">
                                    <img class="img-fade2" src="/static/images/gallery/gal13.JPG" alt="">
                                    
                                </div>
                                <div class="gal-box">
                                    <img class="img-fade1" src="/static/images/gallery/gal5.JPG" alt="">
                                    <img class="img-fade2" src="/static/images/gallery/gal14.JPG" alt="">   
                                </div>  
                                
                                <div class="gal-box">
                                    <img class="img-fade1" src="/static/images/gallery/gal6.JPG" alt="">
                                    <img class="img-fade2" src="/static/images/gallery/gal15.JPG" alt="">
                                    
                                </div> 
                                
                                <div class="gal-box">
                                    <img class="img-fade1" src="/static/images/gallery/gal7.JPG" alt="">
                                    <img class="img-fade2" src="/static/images/gallery/gal16.JPG" alt="">
                                    
                                </div> 
                                <div class="gal-box">
                                    <img class="img-fade1" src="/static/images/gallery/gal8.JPG" alt="">
                                    <img class="img-fade2" src="/static/images/gallery/gal17.JPG" alt="">
                                    
                                </div> 
                                <div class="gal-box">
                                    <img class="img-fade1" src="/static/images/gallery/gal9.JPG" alt="">
                                    <img class="img-fade2" src="/static/images/gallery/gal18.JPG" alt="">
                                    
                                </div> 
                            </div>
                            <div class="col-md-3 Judge-quot">
                                <div class="owl-four owl-carousel owl-theme">
                             <div class="item">
                                   <div class="scroll-txt">
                                   <img src="/static/images/Narendra-Modi.jpg" alt="">
                                       <div class="scroll-btm">
                                       "The power to think differently and ahead of the times for the betterment of mankind is what sets entrepreneurs apart."
                                       </div>
                                       <p><strong> Shri. Narendra Modi</strong></p>
                                       <p>Hon’ble Prime Minister of India</p>
                                   </div>
                               </div>
                               <div class="item">
                                   <div class="scroll-txt">
                                   <img src="/static/images/Arun-Jaitley.jpg" alt="">
                                       <div class="scroll-btm">
                                       "The strength of the Indian economy is going to be entrepreneurship. There is relevance of both skilling and entrepreneurship in India. In the government, both at Centre and the states, as also public sector enterprises, there is limited capacity of job creation."
                                       </div>
                                       <p><strong>Shri. Arun Jaitley</strong></p>
                                       <p>Hon’ble Minister of Finance and Corporate Affairs </p>
                                   </div>
                               </div>
                            
                            <div class="item">
                                   <div class="scroll-txt">
                                   <img src="/static/images/dharmendra-pradhan.jpg" alt="">
                                       <div class="scroll-btm">
                                       "Skill development and entrepreneurship form the basis for the growth of any country."
                                       </div>
                                       <p><strong>Shri. Dharmendra Pradhan</strong></p>
                                       <p>Hon’ble Minister of Petroleum & Natural Gas and Skill Development & Entrepreneurship </p>
                                   </div>
                               </div>
                            
                           
                               
                           </div>
                                
                                
                            </div>
                        </div>
                        <!--end of class winner-section-->
                        
                        <div class="tweet-section">
                            
                            <div class="col-md-6 twt-lft">
                                <div class="twt-heading">
                                    <span>Our<br> News
                                    </span>
                                </div>
                                
                                <div class="twt-block">
                                    <ul>
                                        <c:if test="${not empty news}">
                                            <c:forEach items="${news}" var="newsSec">
                                                <li>
                                                    <div class="twt-text">
                                                        <p>${newsSec.title}</p>
                                                        <a href="javascript:void(0);">${newsSec.subCategory}</a>
                                                        <div class="tw-date">${newsSec.createdOn}</div>
                                                    </div>
                                                </li>
                                            </c:forEach>
                                        </c:if>
                                    </ul>
                                    
                                </div>
                            </div>
                            
                            <div class="col-md-6 twt-rft">
                                <div class="twt-heading">
                                    <span>Our<br> Twitter
                                    </span>
                                </div>
                                
                                <div class="twt-block">
                                    <a class="twitter-timeline"
                                    href="https://twitter.com/Youth4Entrprise?ref_src=twsrc%5Etfw"
                                    data-chrome="noscrollbar" data-tweet-limit="4">Tweets by
                                        NEAS</a>
                                    <script async src="https://platform.twitter.com/widgets.js"
                                    charset="utf-8"></script>
                                </div>
                            </div>
                        </div>
                        <!--end of class tweet-section-->
                        
                        <div class="partner-section">
                            <div class="partner-inner">
                                <h3>OUR IMPLEMENTING PARTNERS</h3>
                                
 				<ul class="main-partnerlogo">
                                <li>
                                    <a rel="noopener noreferrer" target="_blank" href="https://www.xlri.ac.in/"><img src="/static/images/partner/xlri.png" alt="xlri"></a>
				    <p>(Lead Partner)</p>
                                </li>
				</ul>
                                 <ul>
                                <li>
                                    <a rel="noopener noreferrer" target="_blank" href="http://www.iitb.ac.in/"><img src="/static/images/partner/iitb.png" alt="iitb"></a>
                                </li>
                                <li>
                                    <a rel="noopener noreferrer" target="_blank" href="http://www.iitd.ac.in/"><img src="/static/images/partner/iitd.png" alt="iitd"></a>
                                </li>
                                <li>
                                    <a rel="noopener noreferrer" target="_blank" href="http://www.iitg.ac.in/"><img src="/static/images/partner/iitg.png" alt="xlri"></a>
                                </li>
                                <li>
                                    <a rel="noopener noreferrer" target="_blank" href="https://www.iitk.ac.in/"><img src="/static/images/partner/iitk.png" alt="iitk"></a>
                                </li>
                                <li>
                                    <a rel="noopener noreferrer" target="_blank" href="https://www.iitm.ac.in/"><img src="/static/images/partner/iitm.png" alt="iitm"></a>
                                </li>
                            </ul>
                            <ul>
                                <li>
                                    <a rel="noopener noreferrer" target="_blank" href="http://www.tiss.edu/"><img src="/static/images/partner/tiss.png" alt="tiss"></a>
                                </li>
                                <li>
                                    <a rel="noopener noreferrer" target="_blank" href="https://www.irma.ac.in/"><img src="/static/images/partner/irma.png" alt="irma"></a>
                                </li>
                                <li>
                                    <a rel="noopener noreferrer" target="_blank" href="http://www.manage.gov.in/"><img src="/static/images/partner/manage.png" alt="manage"></a>
                                </li>
                                <li>
                                    <a rel="noopener noreferrer" target="_blank" href="http://www.rsetimis.org/"><img src="/static/images/partner/rseti.png" alt="rseti"></a>
                                </li>
                                <li>
                                    <a rel="noopener noreferrer" target="_blank" href="https://www.nabard.org/"><img src="/static/images/partner/naward.png" alt="naward"></a>
                                </li>
                                <li>
                                    <a rel="noopener noreferrer" target="_blank" href="http://nif.org.in/"><img src="/static/images/partner/logo-6.png" alt="logo"></a>
                                </li>
                                
                            </ul>
                                
                            </div>
                        </div>
                        <!--end of class partner-section-->
                    </div>
                </div>
            </div>
        </main>
        
        
        <div class="modal fade" id="openForget" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="/forgetpassword" method="POST">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Forget Password</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="email1">Email Id</label> <input type="email"
                                name="emailId" id="email1" class="form-control"
                                placeholder="Email ID" required  pattern="^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$" data-pattern-error="Please provide a valid email id.">
                            </div>
                        </div>
                        <div class="modal-footer">
                        <input type="hidden" name="currentpath" value=""/>
                            <input type="hidden" name="${_csrf.parameterName}"
                            value="${_csrf.token}" /> <input type="submit"
                            class="btn btn-primary" value="Proceed" />
                            <button type="button" class="btn btn-default"
                            data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
                
            </div>
        </div>
        
        <c:if test="${(not empty errorMsg) or (not empty successMsg)}">
            <div class="modal fade" id="errorModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <c:if test="${ not empty errorMsg }">
                                <h4 class="modal-title">Error</h4>
                            </c:if>
                            <c:if test="${ not empty successMsg }">
                                <h4 class="modal-title">Success</h4>
                            </c:if>
                        </div>
                        <div class="modal-body">
                            <p>
                                <c:if test="${ not empty errorMsg }">
                                    ${ errorMsg }
                                </c:if>
                                <c:if test="${ not empty successMsg }">
                                    ${ successMsg }
                                </c:if>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary"
                            data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    
                </div>
            </div>
        </c:if>
        <!-- Body Ends Here -->
        <%@include file="fragments/footer.jsp"%>
        
        <!-- Modal -->
<div id="awardpopup" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body">
       <img src="/static/images/awwards.jpg" alt="">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
    
   <style>
    #awardpopup .modal-body
       {
           padding: 0;
       }
        #awardpopup img
       {
           width: 100%;
       }
    </style>
    <script>
   $(document).ready(function(){
        $("#awardpopup").modal('show');
    });
    
    </script>
    </body>
</html>
