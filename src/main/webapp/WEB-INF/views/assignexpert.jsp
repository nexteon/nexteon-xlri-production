<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">
<%@include file="dashboard/headerlibs.jsp"%>

<link href="/resources/css/select2.min.css" rel="stylesheet" />
<link href="/resources/css/select2-placeholder-transition.css" rel="stylesheet" />
<body
	class="page-header-fixed page-sidebar-closed-hide-logo page-content-white"
	id="page-top">
	<div class="page-wrapper">
		<%@include file="dashboard/header.jsp"%>



		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<!-- BEGIN CONTENT BODY -->
			<div class="page-content">
				<!-- BEGIN PAGE HEADER-->

				<!-- BEGIN PAGE BAR -->
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li><a href="/userdata">Home</a> <i class="fa fa-circle"></i>
						</li>
						<li><span>AssignExperts</span></li>
					</ul>
					<div class="page-toolbar">
						<div id="dashboard-report-range"
							class="pull-right tooltips btn btn-sm" data-container="body"
							data-placement="bottom"
							data-original-title="Change dashboard date range">
							<i class="icon-calendar"></i>&nbsp; <span
								class="thin uppercase hidden-xs"></span>&nbsp; <i
								class="fa fa-angle-down"></i>
								
								
								
								
						</div>
					</div>
				</div>
				<!-- END PAGE BAR -->
				<!-- BEGIN PAGE TITLE-->
				<h1 class="page-title">Assign Experts</h1>
				<!-- END PAGE TITLE-->
				<!-- END PAGE HEADER-->
				<!-- BEGIN DASHBOARD STATS 1-->


<form action="/neas/api/assign/expert" method="post" data-toggle="validator" role="form">
				<div class="row">
                        
                        <div class="col-md-12">
                            
                        <div class="portlet box blue form-display1">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Assign Expert</div>
                                        
                                    </div>
                                    <div class="portlet-body">
                                      <div class="col-xs-12 form-1-box">
                                         <form>
											
                                              <div class="input-wrapper">
     <div class="col-md-4">
    <select id="select1" name="destinations" class="form-control select2-placeholder-transition">
        <option value="" disabled="" selected="" style="display:none;">Select One...</option>
       <option value="1">Sample 1</option>
      <option value="2">Sample 2</option>
      <option value="3">Sample 3</option>
      <option value="4">Sample 4</option>
    </select>
     </div> 
        <div class="col-md-4">
      <select id="select2" name="destinations2" class=" form-control select2-placeholder-transition2">
          <option value="" disabled="" selected="" style="display:none;">Select One...</option>
      <option value="1">Sample 1</option>
      <option value="2">Sample 2</option>
      <option value="3">Sample 3</option>
      <option value="4">Sample 4</option>
    </select>
                                                  </div>
                                                   <div class="col-md-4">
      <select id="select3" name="destinations3" class=" form-control select2-placeholder-transition3">
          <option value="" disabled="" selected="" style="display:none;">Select One...</option>
      <option value="1">Sample 1</option>
      <option value="2">Sample 2</option>
      <option value="3">Sample 3</option>
      <option value="4">Sample 4</option>
    </select>
                                                  </div>
      
  </div>
                                          
                                          <br><br><br>
                                          
                                          <div class="row">
												<div class="col-sm-12">
                                                    <button class="btn btn-primary" type="submit" name="add-expert">Add Expert</button>
                                              </div>
                                          </div>
                                          </form>
                                          
										
											
											</div>
											
											
											
											</div>
                                        
                                      
                                    </div>
                                </div>    
                            
                        </div>
                        </form>
				



			</div>
		</div>
	</div>

	<!-- END QUICK NAV -->
	<%@include file="dashboard/footer.jsp"%>
	
<script src="/resources/js/select2.min.js"></script>
<script src="/resources/js/select2-placeholder-transition.js"></script>	
	
	<script>
    $(document).ready(function () {
      $(".select2-placeholder-transition").select2PlaceholderTransition({
        placeholder: 'Select Expert 1',
        allowClear: true
      });
        $(".select2-placeholder-transition2").select2PlaceholderTransition({
        placeholder: 'Select Expert 2',
        allowClear: true
      });
         $(".select2-placeholder-transition3").select2PlaceholderTransition({
        placeholder: 'Select Expert 3',
        allowClear: true
      });
       
    
            
    });
    
  </script>
    <style>.input-wrapper {
      padding-top: 30px;
    }

    .select2-placeholder-transition {
      width: 300px;
      max-width: 100%;
    }       
    .select2-placeholder-transition2,.select2-placeholder-transition3
        {
            width: 300px;
        }
        .select2-container+label {
    position: absolute;
    top: -28px;
    left: 0;
    padding: 5px;
    transition: transform .2s ease;
    color: #000;
    pointer-events: none;
}
    </style>   
       <script>
      
    var $select = $(".input-wrapper select");
$select.on("change", function() {
    var selected = [];  
    $.each($select, function(index, select) {           
        if (select.value !== "") { selected.push(select.value); }
          
   $("option").prop("disabled", false);         
   for (var index in selected) { $('option[value="'+selected[index]+'"]').prop("disabled", true); }
});

});   
    </script>
    
    <style>
        .form-display1 .portlet-body{float:left;width:100%}
.portlet.box.blue{float:left;width:100%}
.col-xs-12.form-1-box{padding:15px;border:solid 1px #ccc;margin-bottom:30px}
.form-display1 .form-control{margin-bottom:15px}
.form-display1 label{font-weight:600;font-size:14px;padding-bottom:4px}
.formsecondpart{padding-top:15px!important}
        
        </style>
	
	
</body>
</html>

