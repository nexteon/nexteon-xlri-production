<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<head>
    <title>NEAS</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href='/resources/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
    <link href='/resources/css/bootstrap-dropdownhover.css' rel='stylesheet' type='text/css'>
    <link href='/resources/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link href='/resources/css/owl.carousel.min.css' rel='stylesheet' type='text/css'>
    <link href='/resources/css/jquery-ui.css' rel='stylesheet' type='text/css'>
    <link href='/resources/css/custom.css' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">    
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127402164-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());
	
	  gtag('config', 'UA-127402164-1');
	</script>
    
</head>
