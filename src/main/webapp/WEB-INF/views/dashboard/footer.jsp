<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    
<!-- BEGIN FOOTER -->
            <div class="page-footer">
                <div class="page-footer-inner"> 2018 &copy; &nbsp;
                    <a href="/userdata" title="NEAS" target="_blank">Copyright © XLRI 2018</a>
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            </div>
            
            <%@include file="footerlibs.jsp" %>
            <!-- END FOOTER -->