package com.xlri.awards.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.xlri.awards.common.EncryptionUtil;
import com.xlri.awards.common.MailCommons;
import com.xlri.awards.common.OtpSalt;
import com.xlri.awards.common.SMSUtil;
import com.xlri.awards.dao.entity.Nominator;
import com.xlri.awards.dao.entity.NomineeDetails;
import com.xlri.awards.dao.entity.User;
import com.xlri.awards.dao.service.api.ExpertService;
import com.xlri.awards.dao.service.api.MailService;
import com.xlri.awards.dao.service.api.NewsService;
import com.xlri.awards.dao.service.api.NominatorService;
import com.xlri.awards.dao.service.api.NomineeService;
import com.xlri.awards.dao.service.api.UserService;

@Controller
public class BaseController {

	@Autowired
	NominatorService nominatorService;

	@Autowired
	UserService userService;

	@Autowired
	MailService mailService;

	@Autowired
	NewsService newsService;

	@Autowired
	NomineeService nomineeService;
	
	@Autowired
	ExpertService expertService;

	@RequestMapping(value = { "/", "/neas" }, method = RequestMethod.GET)
	public ModelAndView getIndexPage() {
		ModelAndView model = new ModelAndView("index");
		model.addObject("news", newsService.getNewsDescFour());
		return model;
	}

	@RequestMapping(value = { "/userpage" }, method = RequestMethod.GET)
	public ModelAndView getUserlandingpage() {
		return new ModelAndView("userlandingpage");
	}

	@RequestMapping(value = { "/nominatorlanding" }, method = RequestMethod.GET)
	public ModelAndView getNominatorlandingpage() {
		ModelAndView model = new ModelAndView("nominatorlanding");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Collection<? extends GrantedAuthority> roles = auth.getAuthorities();
		for (GrantedAuthority role : roles) {
			if (role.getAuthority().equals("ROLE_NOMINATOR")) {
				String emailId = auth.getPrincipal().toString();
				Nominator nominator = nominatorService.getNominatorByEmailId(emailId);
				model.addObject("entCount", nominator.getEnterpriseCount());
				model.addObject("nominator", nominator);
				model.addObject("ecoCount", nominator.getEcosystemCount());
				model.addObject("registrationNumber", nominator.getRegistrationNumber());
				model.addObject("fullName", nominator.getFullName());
				model.addObject("encryptedEmail",EncryptionUtil.encrypt(nominator.getEmailId(), OtpSalt.SALT.toString()));
				List<NomineeDetails> list = nomineeService.getAllNomineeByRegistration(nominator.getRegistrationNumber());
				model.addObject("nomineeData",list);
				/*boolean pendingStatus = false;
				for(NomineeDetails nominee:list){
					if(!nominee.getFilledStatus()){
						pendingStatus = true;
						break;
					}
				}
				if(pendingStatus){
					model.addObject("pendingStatus",pendingStatus);
				}*/
				model.addObject("entCountLeft",nominatorService.getEnterpriseCountLeft(nominator.getRegistrationNumber()));
				model.addObject("ecoCountLeft",nominatorService.getEcosystemCountLeft(nominator.getRegistrationNumber()));
				return model;
			}
		}
		return new ModelAndView("index");
	}

	@GetMapping(value = "/otp/verify/{encyptedRegistration}")
	public ModelAndView verifyOTPPage(@PathVariable String encyptedRegistration) {
		ModelAndView model = new ModelAndView();
		String registrationNumber = EncryptionUtil.decrypt(encyptedRegistration, OtpSalt.SALT.toString());
		Nominator nominator = nominatorService.getNominatorByRegistrationNumber(registrationNumber);
		boolean status=userService.checkNomineeUser(nominator.getEmailId());
		if(status){
			model.addObject("errorMsg", "This user is already registered. Please login or use forgot password option to reset the password");
			model.setViewName("login");
			return model;
			
		}else{
			model = new ModelAndView("otpverification");
			model.addObject("registration", encyptedRegistration);
			return model;
		}
	}

	@PostMapping(value = "/otp/verify/{encyptedRegistration}")
	public void verifyOTP(@PathVariable String encyptedRegistration, HttpServletRequest request,
			HttpServletResponse response) throws JSONException, IOException {
		String otp = request.getParameter("otpnumber");
		String regiNum = EncryptionUtil.decrypt(encyptedRegistration, OtpSalt.SALT.getSalt());
		Nominator nominator = nominatorService.getNominatorByRegistrationNumber(regiNum);
		JSONObject resJSON = new JSONObject();
		if (otp.equals(nominator.getOtp()) && (new Date().compareTo(nominator.getOtpExpiry()) < 0)) {
			User user = new User();
			user.setUsername(nominator.getEmailId());
			user.setPassword(nominator.getPassword());
			user.setSalt(nominator.getSalt());
			user.setRole(nominator.getRole());
			user.setFullName(nominator.getFullName());
			userService.saveUser(user);
			response.setStatus(200);
			resJSON.put("status", true);
			response.setContentType("application/json");
			response.getWriter().println(resJSON.toString());
		} else {
			resJSON.put("status", false);
			response.setContentType("application/json");
			response.getWriter().println(resJSON.toString());
		}
	}

	@PostMapping(value = "/otp/resend/{encyptedRegistration}")
	public void resendOTP(@PathVariable String encyptedRegistration,HttpServletRequest request, HttpServletResponse response) throws Exception {
		String regiNum = EncryptionUtil.decrypt(encyptedRegistration, OtpSalt.SALT.getSalt());
		Nominator nominator = nominatorService.getNominatorByRegistrationNumber(regiNum);
		if(nominator.getOtpExpiry().after(new Date())){
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("status", false);
			response.setContentType("application/json");
			response.getWriter().write(jsonObj.toString());
		}else{
		nominator.setOtp();
		nominator.setOtpExpiry();
		nominatorService.updateNominator(nominator);
		new Thread(new Runnable() {
			@Override
			public void run() {
				mailService.sendOtp(nominator);
			}
		}).start();
		new Thread(new Runnable() {
			@Override
			public void run() {
				try{
				new SMSUtil().sendOTP(nominator.getOtp()+" is your One Time Password to register yourself as Nominator with NEA 2018.", nominator.getContactNumber());
				}catch(Exception e){
					System.out.println(e);
				}
			}
		}).start();
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("status", true);
		response.setContentType("application/json");
		response.getWriter().write(jsonObj.toString());
		}
	}

	@GetMapping(value = "/verify/nominee/{encyptedEmailId}")
	public ModelAndView addNomineeToUser(@PathVariable String encyptedEmailId) {
		ModelAndView model = new ModelAndView();

		String emailId = EncryptionUtil.decrypt(encyptedEmailId, OtpSalt.SALT.getSalt());
		NomineeDetails nominee = nomineeService.getNomineeByEmailId(emailId);

		User user = new User();

		if (userService.checkNomineeUser(emailId)) {
			model.setViewName("login");
		} else {
			user.setUsername(nominee.getEmailId());
			user.setPassword(nominee.getPassword());
			user.setSalt(nominee.getSalt());
			user.setRole(nominee.getRole());
			user.setFullName(nominee.getNomineeName());
			userService.saveUser(user);
			model.setViewName("login");
		}
		return model;
	}

	@RequestMapping(value = "/neas/about/overview", method = RequestMethod.GET)
	public ModelAndView getAboutUs() {
		return new ModelAndView("aboutus");
	}

	@RequestMapping(value = "/neas/about/award", method = RequestMethod.GET)
	public ModelAndView getAboutAwards() {
		return new ModelAndView("awards");
	}

	@RequestMapping(value = "/neas/about/eligibility", method = RequestMethod.GET)
	public ModelAndView getAboutEligibility() {
		return new ModelAndView("eligibility");
	}
	
	@RequestMapping(value = "/neas/about/eligibility/second", method = RequestMethod.GET)
	public ModelAndView getAboutSecondEligibility() {
		return new ModelAndView("anothereligibility");
	}

	@RequestMapping(value = "/neas/about/reward", method = RequestMethod.GET)
	public ModelAndView getAboutRewards() {
		return new ModelAndView("rewards");
	}

	@RequestMapping(value = "/neas/programme", method = RequestMethod.GET)
	public ModelAndView getProgrammeDetail() {
		return new ModelAndView("programme");
	}

	@RequestMapping(value = "/neas/past/2016/jury", method = RequestMethod.GET)
	public ModelAndView getAward2016() {
		return new ModelAndView("jury2016");
	}

	@RequestMapping(value = "/neas/past/2017/jury", method = RequestMethod.GET)
	public ModelAndView getAwardJury2017() {
		return new ModelAndView("jury2017");
	}

	@RequestMapping(value = "/neas/past/2016/winner/award", method = RequestMethod.GET)
	public ModelAndView getAwardWinner2016() {
		return new ModelAndView("award2016");
	}

	@RequestMapping(value = "/neas/past/2017/winner/award", method = RequestMethod.GET)
	public ModelAndView getAwardWinner2017() {
		return new ModelAndView("award2017");
	}

	@RequestMapping(value = "/neas/past/2016/winner/recognition", method = RequestMethod.GET)
	public ModelAndView getRecogWinner2016() {
		return new ModelAndView("recog2016");
	}

	@RequestMapping(value = "/neas/past/2017/winner/recognition", method = RequestMethod.GET)
	public ModelAndView getRecogWinner2017() {
		return new ModelAndView("recog2017");
	}

	@RequestMapping(value = "/neas/guidelines", method = RequestMethod.GET)
	public ModelAndView getGuideline() {
		return new ModelAndView("guidelines");
	}

	@RequestMapping(value = "/neas/scheme", method = RequestMethod.GET)
	public ModelAndView getScheme() {
		return new ModelAndView("scheme");
	}

	@RequestMapping(value = "/neas/contact", method = RequestMethod.GET)
	public ModelAndView getContact() {
		return new ModelAndView("contactus");
	}

	@RequestMapping(value = "/neas/awards/2017", method = RequestMethod.GET)
	public ModelAndView getAward2017() {
		return new ModelAndView("award2017");
	}

	@RequestMapping(value = "/neas/api/checkemail/{type}", method = RequestMethod.GET)
	public void checkEmail(@PathVariable String type, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String emailId = request.getParameter("emailId");
		boolean status1 = false, status2 = false;
			status1 = nomineeService.getNomineeByEmailId(emailId) != null;
			status2 = nominatorService.getNominatorByEmailId(emailId) != null;
		
		if (status1 || status2) {
			response.setStatus(403);
		} else {
			JSONObject json = new JSONObject();
			try {
				json.put("status", status1||status2);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			response.setStatus(200);
			response.setContentType("application/json");
			response.getWriter().write(json.toString());
			
		}
	}
	
	@RequestMapping(value = "/neas/api/checkemail/expert", method = RequestMethod.GET)
	public void checkExpertEmail(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String emailId = request.getParameter("emailid");
		boolean status1 = false;
			status1 = userService.getUserByUsername(emailId) != null;
		
		if (status1) {
			response.setStatus(403);
		} else {
			JSONObject json = new JSONObject();
			try {
				json.put("status", status1);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			response.setStatus(200);
			response.setContentType("application/json");
			response.getWriter().write(json.toString());
			
		}
	}

	@RequestMapping(value = "/neas/api/mail/sendcontact", method = RequestMethod.POST)
	public ModelAndView sendContactDetails(HttpServletRequest request, HttpServletResponse response) throws IOException {

		ModelAndView model = new ModelAndView("contactus");
		
		String fullName = request.getParameter("fullName");
		String emailId = request.getParameter("emailId");
		String subject = request.getParameter("subject");
		String message = request.getParameter("message");
		try{
			loadConfigs();
			if(filterText(subject)) {
				new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							MimeMessage mimeMessage = new MailCommons().mimeMessage();
							mimeMessage.setFrom("contact-neas@nic.in");
							mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(emailId));
							mimeMessage.setSubject("Thank you for contacting NEAS");
							StringBuilder sb = new StringBuilder();
							sb.append("Dear " + fullName + ",");
							sb.append(
									"<br/><br/>Thank you for get in touch with us. Our experts will get back to you shortly.");
							sb.append("<br/><br/>Thank You<br/>Team NEAS");
							mimeMessage.setContent(sb.toString(), "text/html; charset=utf-8");

							Transport.send(mimeMessage);
						} catch (MessagingException e) {
							e.printStackTrace();
						}
					}
				}).start();

				new Thread(new Runnable() {

					@Override
					public void run() {
						try {
							MimeMessage mimeMessage = new MailCommons().mimeMessage();
							mimeMessage.setFrom("contact-neas@nic.in");
							mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress("kunwar@xlri.ac.in"));
							mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress("chauhan.vikas990@gmail.com"));
							mimeMessage.setSubject("User Contact :: " + subject);
							StringBuilder sb = new StringBuilder();
							sb.append("Dear Team,");
							sb.append("<br/><br/>You have got new query by: " + fullName);
							sb.append("<br/>Email Id: " + emailId);
							sb.append("<br/>Message: " + message);
							sb.append("<br/><br/>Kindly reply ASAP.");
							sb.append("<br/><br/>Thank You<br/>Team NEAS");
							mimeMessage.setContent(sb.toString(), "text/html; charset=utf-8");
							Transport.send(mimeMessage);
						} catch (MessagingException e) {
							e.printStackTrace();
						}
					}
				}).start();
				
				model.addObject("successMsg","Thank you for reaching us! We will get back to you shortly.");
			}
			else {
				model.addObject("errorMsg","Something went wrong. Please try again");
			}
		
		}catch(Exception e){
			model.addObject("errorMsg","Something went wrong. Please try again");
		}
		return model;
		
	}
	
	@RequestMapping(value = "/neas/api/username", method = RequestMethod.POST)
	public void getUserName(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType("text/plain");
		String email = request.getParameter("email");
		User user = userService.getUserByUsername(email);
		response.getWriter().write(user.getFullName());
	}
	
	@RequestMapping(value = "/neas/api/checkcontact/{type}", method = RequestMethod.GET)
	public void checkContact(@PathVariable String type, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String contact = request.getParameter("contactNumber");
		boolean status1 = false, status2 = false;
			status1 = nomineeService.getNomineeByContact(contact) != null;
			status2 = nominatorService.getNominatorByContactNumber(contact) != null;
		
		if (status1 || status2) {
			response.setStatus(403);
		} else {
			JSONObject json = new JSONObject();
			try {
				json.put("status", status1||status2);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			response.setStatus(200);
			response.setContentType("application/json");
			response.getWriter().write(json.toString());
			
		}
	}
	
	@RequestMapping(value = "/neas/api/createexcel/nominator", method = RequestMethod.GET)
	public void createNominatorExcel(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String[] columns = { "S. No.","Nominator's Registration Number",  "Nominator's Name","Email Id", "Contact","Nominator's Type",
				"Institute's Name", "Occupation","Designation",	"Enterprise Count","EcoSystem Count","Address Line1","Address Line2",
				 "Address Landmark","AddressCity", "State","Address PinCode", "Created On",
				 "Nominator's Role", "Website" };

		List<Nominator> nomineeList = nominatorService.getAllNominators();
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("Nominator's Details");

		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 12);
		headerFont.setColor(IndexedColors.RED.getIndex());

		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);

		Row headerRow = sheet.createRow(0);

		for (int i = 0; i < columns.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(columns[i]);
			cell.setCellStyle(headerCellStyle);
		}

		CreationHelper createHelper = workbook.getCreationHelper();
		CellStyle dateCellStyle = workbook.createCellStyle();
		dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));

		int rowNum = 1;
		for (Nominator nominee : nomineeList) {
			Row row = sheet.createRow(rowNum);
			row.createCell(0).setCellValue(rowNum);
			
						
			row.createCell(1).setCellValue(nominee.getRegistrationNumber());
			row.createCell(2).setCellValue(nominee.getFullName());
			row.createCell(3).setCellValue(nominee.getEmailId());
			
			row.createCell(4).setCellValue(nominee.getContactNumber());
			row.createCell(5).setCellValue(nominee.getNominationType());
			row.createCell(6).setCellValue(nominee.getInstituteName());
			row.createCell(7).setCellValue(nominee.getOccupation());
			row.createCell(8).setCellValue(nominee.getDesignation());	
			row.createCell(9).setCellValue(nominee.getEnterpriseCount());
			row.createCell(10).setCellValue(nominee.getEcosystemCount());
			row.createCell(11).setCellValue(nominee.getAddrLine1());
			row.createCell(12).setCellValue(nominee.getAddrLine2());
			row.createCell(13).setCellValue(nominee.getAddrLandmark());
	    	row.createCell(14).setCellValue(nominee.getAddrCity());
	    	row.createCell(15).setCellValue(nominee.getAddrState());
		    row.createCell(16).setCellValue(nominee.getAddrPinCode());
			
			Cell dateOfBirthCell = row.createCell(17);
			dateOfBirthCell.setCellValue(nominee.getCreatedOn());
			dateOfBirthCell.setCellStyle(dateCellStyle);

			
			row.createCell(18).setCellValue(nominee.getRole());
			row.createCell(19).setCellValue(nominee.getWebsite());
			rowNum++;
		}
		response.setContentType("text/plain");
		response.setHeader("Content-disposition", "attachment; filename=NominatorDetails.xlsx");
		workbook.write(response.getOutputStream());
	}
	
	// ---------------------
	
	static Map<String, String[]> words = new HashMap<>();
    static int largestWordLength = 0;
    public static void loadConfigs() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new URL("https://docs.google.com/spreadsheets/d/1hIEi2YG3ydav1E06Bzf2mQbGZ12kh2fe4ISgLg_UBuM/export?format=csv").openConnection().getInputStream()));
            String line = "";
            int counter = 0;
            while((line = reader.readLine()) != null) {
                counter++;
                String[] content = null;
                try {
                    content = line.split(",");
                    if(content.length == 0) {
                        continue;
                    }
                    String word = content[0];
                    String[] ignore_in_combination_with_words = new String[]{};
                    if(content.length > 1) {
                        ignore_in_combination_with_words = content[1].split("_");
                    }

                    if(word.length() > largestWordLength) {
                        largestWordLength = word.length();
                    }
                    words.put(word.replaceAll(" ", ""), ignore_in_combination_with_words);

                } catch(Exception e) {
                    e.printStackTrace();
                }

            }
            System.out.println("Loaded " + counter + " words to filter out");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    /**
     * Iterates over a String input and checks whether a cuss word was found in a list, then checks if the word should be ignored (e.g. bass contains the word *ss).
     * @param input
     * @return
     */
    public static ArrayList<String> badWordsFound(String input) {
        if(input == null) {
            return new ArrayList<>();
        }

        // remove leetspeak
        input = input.replaceAll("1","i");
        input = input.replaceAll("!","i");
        input = input.replaceAll("3","e");
        input = input.replaceAll("4","a");
        input = input.replaceAll("@","a");
        input = input.replaceAll("5","s");
        input = input.replaceAll("7","t");
        input = input.replaceAll("0","o");
        input = input.replaceAll("9","g");

        ArrayList<String> badWords = new ArrayList<>();
        input = input.toLowerCase().replaceAll("[^a-zA-Z]", "");

        // iterate over each letter in the word
        for(int start = 0; start < input.length(); start++) {
            // from each letter, keep going to find bad words until either the end of the sentence is reached, or the max word length is reached. 
            for(int offset = 1; offset < (input.length()+1 - start) && offset < largestWordLength; offset++)  {
                String wordToCheck = input.substring(start, start + offset);
                if(words.containsKey(wordToCheck)) {
                    // for example, if you want to say the word bass, that should be possible.
                    String[] ignoreCheck = words.get(wordToCheck);
                    boolean ignore = false;
                    for(int s = 0; s < ignoreCheck.length; s++ ) {
                        if(input.contains(ignoreCheck[s])) {
                            ignore = true;
                            break;
                        }
                    }
                    if(!ignore) {
                        badWords.add(wordToCheck);
                    }
                }
            }
        }

        return badWords;

    }

    public static boolean filterText(String input) {
        ArrayList<String> badWords = badWordsFound(input);
        if(badWords.size() > 0) {
            return false;
        }
        return true;
    }

}
