<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">
<%@include file="dashboard/headerlibs.jsp"%>
<body
	class="page-header-fixed page-sidebar-closed-hide-logo page-content-white"
	id="page-top" ng-app="myApp" ng-controller="jsonCtrl">
	<div class="page-wrapper">
		<%@include file="dashboard/header.jsp"%>


                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="/userdata">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>AllPartners</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> All Partners</h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <!-- BEGIN DASHBOARD STATS 1-->

    <div class="row" >
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light portlet-fit bordered">
                                    <!-- <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-settings font-red"></i>
                                            <span class="caption-subject font-red sbold uppercase">Editable Table</span>
                                        </div>
                                        <div class="actions">
                                            <div class="btn-group btn-group-devided" data-toggle="buttons">
                                                <label class="btn btn-transparent red btn-outline btn-circle btn-sm active">
                                                    <input type="radio" name="options" class="toggle" id="option1">Actions</label>
                                                <label class="btn btn-transparent red btn-outline btn-circle btn-sm">
                                                    <input type="radio" name="options" class="toggle" id="option2">Settings</label>
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="portlet-body">
                                        <!-- <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="btn-group">
                                                        <button id="sample_editable_1_new" class="btn green"> Add New
                                                            <i class="fa fa-plus"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                        <button class="btn green btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a href="javascript:;"> Print </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;"> Save as PDF </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;"> Export to Excel </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                        <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                                            <thead>
                                                <tr>
														<th>Partner Name</th>
														<th>Email ID</th>
														<th>Contact Number</th>
														<th>Address</th>
														<th>States</th>
														<th>Edit</th>
														<!-- <th>Delete</th> -->
												</tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="data in tableData">
											<td>{{ data.partnerName }}</td>
											<td>{{ data.emailId }}</td>
											<td>{{ data.contactNumber}}</td>
											<td>{{ data.address}}</td>
											<td>{{ data.state}}</td>

										<!-- 	<td><a class="edit" href="javascript:;"> Edit </a></td>
											<td><a class="delete" href="javascript:;"> Delete </a></td> -->
											
											<td><a href="/editpartner/{{ data.id}}"> Edit </a></td>
											<!-- <td><a href="/deletepartner/{{ data.id}}"> Delete </a></td> -->
										</tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>
                        


         
         </div>
         </div>
         </div>
         
         
         
        <!-- END QUICK NAV -->
     <%@include file="dashboard/footer.jsp"%>
     <script>
            var app = angular.module('myApp', []);
            app.controller('jsonCtrl', function($scope, $http) {
                $http.post("/allpartners")
                
                .then(function(response) {
                 $scope.tableData = response.data; 
                 
                });
            });
            
            
            $(document).ready(function(){
            	$('.editBtn').click(function(){
            		$('.hidden-email').val($(this).data('email'));
            		$("#editModal").modal('show');

            	});
            });
   
    </script>
     </body>
         </html>
                    
