package com.xlri.awards.dao.service.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlri.awards.dao.entity.LogTable;
import com.xlri.awards.dao.service.api.LogTableService;


@Service
public class LogTableServiceImpl implements LogTableService{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(NominatorServiceImpl.class);

	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public boolean saveLog(LogTable logTable) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			session.save(logTable);
			session.getTransaction().commit();
 			return true;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return false;
		}
	}

	@Override
	public List<LogTable> getAllLogs() {
		List<LogTable> logList = null;
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			logList = session.createQuery("From LogTable").getResultList();
			session.getTransaction().commit();
			return logList;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public List<LogTable> getLogsByUsername(String username) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<LogTable> query =  session.createQuery("SELECT e FROM LogTable e WHERE e.username=:username");
		    query.setParameter("username", username);
			session.getTransaction().commit();
			return query.getResultList();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public List<LogTable> getLogsByIPAddress(String ipAddress) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<LogTable> query =  session.createQuery("SELECT e FROM LogTable e WHERE e.ipAddress=:ipAddress");
		    query.setParameter("ipAddress", ipAddress);
			session.getTransaction().commit();
			return query.getResultList();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	
	
}
