<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
   <%@include file="fragments/headerlibs.jsp" %>
   <body class="innerpage">
      <%@include file="fragments/header.jsp" %>
      <!-- Body Starts Here -->
      <main role="main">
         <div class="page-title">
            <h2>NEA AWARDS 2017</h2>
         </div>
         <div class="content awr-2016">
            <div class="container">
               <div class="row">
                  <div class="col-md-12">
                     <div class="whitebg">
                        <div class="winner-section">
                           <div class="award-hdg text-center">
                              <h3 class="blue-hdg">Agri, Food, & Forestry Products</h3>
                              <a href="https://www.sattviko.com/"><img src="/static/images/neaswinner/2017/logo/sattviko.jpg" alt="Rays Culinary Delights Private Limited"/></a>
                              <h4>Rays Culinary Delights Private Limited</h4>
                           </div>
                           <div class="award-images text-center">
                              <ul>
                                 <li>
                                    <img src="/static/images/neaswinner/2017/sattviko1.jpg" alt="Mr.Prasoon Gupta"/>
                                    <p>Mr. Prasoon Gupta</p>
                                 </li>
                                 <li>
                                    <img src="/static/images/neaswinner/2017/sattviko2.jpg" alt="Mr. Ankush Sharma"/>
                                    <p>Mr. Ankush Sharma</p>
                                 </li>
                              </ul>
                           </div>
                        </div>
                        <div class="winner-section">
                           <div class="award-hdg text-center">
                              <h3 class="blue-hdg">Engineering Systems</h3>
                              <a href="https://www.sensegiz.com/"><img src="/static/images/neaswinner/2017/logo/sensegiz.jpg" alt="SenseGiz Technologies Pvt.Ltd."/></a>
                              <h4>SenseGiz Technologies Pvt.Ltd.</h4>
                           </div>
                           <div class="award-images text-center">
                              <ul>
                                 <li>
                                    <img src="/static/images/neaswinner/2017/sensegiz1.jpg" alt="Mr. Abhishek Latthe"/>
                                    <p>Mr. Abhishek Latthe</p>
                                 </li>
                              </ul>
                           </div>
                        </div>
                        <div class="winner-section">
                           <div class="award-hdg text-center">
                              <h3 class="blue-hdg">Renewable Energy and Waste Management</h3>
                              <a href="https://atomberg.com/"><img src="/static/images/neaswinner/2017/logo/atomberg.jpg" alt="Atomberg Technologies Pvt. Ltd"/></a>
                              <h4>Atomberg Technologies Pvt. Ltd</h4>
                           </div>
                           <div class="award-images text-center">
                              <ul>
                                 <li>
                                    <img src="/static/images/neaswinner/2017/atomberg1.jpg" alt="Mr. Manoj Meena"/>
                                    <p>Mr. Manoj Meena</p>
                                 </li>
                                 <li>
                                    <img src="/static/images/neaswinner/2017/atomberg2.jpg" alt="Mr. Sibabrata Das"/>
                                    <p>Mr. Sibabrata Das</p>
                                 </li>
                              </ul>
                           </div>
                        </div>
                        <div class="winner-section">
                           <div class="award-hdg text-center">
                              <h3 class="blue-hdg">IT&ITES, Financial</h3>
                              <a href=""><img src="/static/images/neaswinner/2017/logo/naffa.jpg" alt="Naffa Innovations Pvt.Ltd."/></a>
                              <h4>Naffa Innovations Pvt.Ltd.</h4>
                           </div>
                           <div class="award-images text-center">
                              <ul>
                                 <li>
                                    <img src="/static/images/neaswinner/2017/naffa1.jpg" alt="Mr.Abhishek Kumar"/>
                                    <p>Mr.Abhishek Kumar</p>
                                 </li>
                                 <li>
                                    <img src="/static/images/neaswinner/2017/naffa2.jpg" alt="Mr. Vivek  Kumar Singh"/>
                                    <p>Mr. Vivek  Kumar Singh</p>
                                 </li>
                              </ul>
                           </div>
                        </div>
                        <div class="winner-section">
                           <div class="award-hdg text-center">
                              <h3 class="blue-hdg">Education</h3>
                              <a href="http://www.simulanis.com/"><img src="/static/images/neaswinner/2017/logo/simulanis.jpg" alt="Simulanis Solutions Pvt Ltd"/></a>
                              <h4>Simulanis Solutions Pvt Ltd</h4>
                           </div>
                           <div class="award-images text-center">
                              <ul>
                                 <li>
                                    <img src="/static/images/neaswinner/2017/simulanis1.jpg" alt="Mr. Raman Talwar"/>
                                    <p>Mr. Raman Talwar</p>
                                 </li>
                              </ul>
                           </div>
                        </div>
                        <div class="winner-section">
                           <div class="award-hdg text-center">
                              <h3 class="blue-hdg">Hospitality</h3>
                              <a href="https://www.oyorooms.com/"><img src="/static/images/neaswinner/2017/logo/oyo.jpg" alt="Oravel Stays Pvt. Ltd.(Brand Name: OYO)"/></a>
                              <h4>Oravel Stays Pvt. Ltd.(Brand Name: OYO)</h4>
                           </div>
                           <div class="award-images text-center">
                              <ul>
                                 <li>
                                    <img src="/static/images/neaswinner/2017/oyo1.jpg" alt="Mr. Ritesh Agarwal"/>
                                    <p>Mr. Ritesh Agarwal</p>
                                 </li>
                              </ul>
                           </div>
                        </div>
                        <div class="winner-section">
                           <div class="award-hdg text-center">
                              <h3 class="blue-hdg">E-Commerce, Logistics, Transport & Other Services</h3>
                              <a href="https://www.loginextsolutions.com/"><img src="/static/images/neaswinner/2017/logo/loginext.jpg" alt="LogiNext Solutions Private Limited"/></a>
                              <h4>LogiNext Solutions Private Limited</h4>
                           </div>
                           <div class="award-images text-center">
                              <ul>
                                 <li>
                                    <img src="/static/images/neaswinner/2017/loginext1.jpg" alt="Mr. Dhruvil Sanghvi"/>
                                    <p>Mr. Dhruvil Sanghvi</p>
                                 </li>
                                 <li>
                                    <img src="/static/images/neaswinner/2017/loginext2.jpg" alt="Ms. Manisha Raisinghani"/>
                                    <p>Ms. Manisha Raisinghani</p>
                                 </li>
                              </ul>
                           </div>
                        </div>
                        <div class="winner-section">
                           <div class="award-hdg text-center">
                              <h3 class="blue-hdg">Social Enterprises</h3>
                              <a href="http://www.sampurnearth.com/"><img src="/static/images/neaswinner/2017/logo/ses.jpg" alt="Sampurn(e)arth Environment Solutions Pvt Ltd"/></a>
                              <h4>Sampurn(e)arth Environment Solutions Pvt Ltd</h4>
                           </div>
                           <div class="award-images text-center">
                              <ul>
                                 <li>
                                    <img src="/static/images/neaswinner/2017/ses1.jpg" alt="Mr. Jayanth N"/>
                                    <p>Mr. Jayanth N</p>
                                 </li>
                                 <li>
                                    <img src="/static/images/neaswinner/2017/ses2.jpg" alt="Mr. Ritvik Rao"/>
                                    <p>Mr. Ritvik Rao</p>
                                 </li>
                              </ul>
                           </div>
                        </div>
                        <div class="winner-section">
                           <div class="award-hdg text-center">
                              <h3 class="blue-hdg">Barefoot Entreprises</h3>
                              <a href="#"><img src="/static/images/neaswinner/2017/logo/ses.jpg" alt="Thejaswini Coconut Farmers Producer Company Ltd."/></a>
                              <h4>Thejaswini Coconut Farmers Producer Company Ltd.</h4>
                           </div>
                           <div class="award-images text-center">
                              <ul>
                                 <li>
                                    <img src="/static/images/neaswinner/2017/thejawani1.jpg" alt="Mr. Sunny George"/>
                                    <p>Mr. Sunny George</p>
                                 </li>
                              </ul>
                           </div>
                        </div>
                        <div class="winner-section">
                           <div class="award-hdg text-center">
                              <h3 class="blue-hdg">Woman</h3>
                              <a href="https://sproboticworks.com/"><img src="/static/images/neaswinner/2017/logo/sprw.jpg" alt="SP Robotic Works Pvt. Ltd."/></a>
                              <h4>SP Robotic Works Pvt. Ltd.</h4>
                           </div>
                           <div class="award-images text-center">
                              <ul>
                                 <li>
                                    <img src="/static/images/neaswinner/2017/sprw1.jpg" alt="Ms. Sneha Priya "/>
                                    <p>Ms. Sneha Priya </p>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </main>
      <!-- Body Ends Here -->
      <%@include file="fragments/footer.jsp" %>
   </body>
</html>