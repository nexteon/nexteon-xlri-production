package com.xlri.awards.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class SMSUtil {

	
	private static final String PIN = "oec0cofn";
	
	public boolean sendSMS(String message, String mnumber) throws Exception{
		String USERNAME = "neas.sms";
		message = message.replaceAll(" ", "%20");
		String response = readResponse(USERNAME, message, mnumber);
		if(response.contains("Accepted") && response!=null){
			return true;
		}else{
			return false;
		}
	}
	
	public boolean sendOTP(String message, String mnumber) throws Exception{
		String USERNAME = "neas.otp";
		message = message.replaceAll(" ", "%20");
		String response = readResponse(USERNAME, message, mnumber);
		if(response.contains("Accepted") && response!=null){
			return true;
		}else{
			return false;
		}
	}

	public  String readResponse(String username, String message, String mnumber) throws Exception {
		// Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
        };
 
        // Install the all-trusting trust manager
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
 
        // Create all-trusting host name verifier
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };
 
        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		String actURL = "https://smsgw.sms.gov.in/failsafe/HttpLink?username="+username+"&pin="+PIN+"&message="+message+"&mnumber=91"+mnumber+"&signature=NEASMS";
		String response = null;
		String output = null;
		  try {
			 
			URL url = new URL(actURL);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			
			conn.setRequestMethod("GET");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));
			while ((output = br.readLine()) != null) {
				response = output;
			}
			conn.disconnect();
		  } catch (MalformedURLException e) {
			e.printStackTrace();
		  } catch (IOException e) {
			e.printStackTrace();
		  }
		  return response;
		}

	
}
