package com.xlri.awards.controller;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.webflow.action.EvaluateAction;

import com.google.gson.Gson;
import com.xlri.awards.common.EncryptionUtil;
import com.xlri.awards.common.OtpSalt;
import com.xlri.awards.common.StorageService;
import com.xlri.awards.dao.entity.Evaluation;
import com.xlri.awards.dao.entity.Expert;
import com.xlri.awards.dao.entity.Nominator;
import com.xlri.awards.dao.entity.NomineeDetails;
import com.xlri.awards.dao.entity.Partners;
import com.xlri.awards.dao.entity.User;
import com.xlri.awards.dao.service.api.EvaluationService;
import com.xlri.awards.dao.service.api.ExpertService;
import com.xlri.awards.dao.service.api.MailService;
import com.xlri.awards.dao.service.api.NomineeService;
import com.xlri.awards.dao.service.api.PartnersService;
import com.xlri.awards.dao.service.api.UserService;


@Controller
public class PartnersController {

    private static final String VIEW_NAME = "partners";

    private final NomineeService nomineeService;
    private final PartnersService partnersService;
    private final StorageService storageService;
    private final UserService userService;
    
    @Autowired
    ExpertService expertService;
    
    @Autowired
    EvaluationService evaluationService;    
    
    @Autowired
    MailService mailService;

    @Autowired
    public PartnersController(NomineeService nomineeService, PartnersService partnersService, StorageService storageService, UserService userService) {
        this.nomineeService = nomineeService;
        this.partnersService = partnersService;
        this.storageService = storageService;
        this.userService = userService;
    }

    @RequestMapping(value = "/partnerslanding", method = RequestMethod.GET)
    public ModelAndView viewPartnersLanding(HttpServletRequest request) {
        ModelAndView model = new ModelAndView(VIEW_NAME);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String emailId = auth.getPrincipal().toString();
        Collection<? extends GrantedAuthority> roles = auth.getAuthorities();
        for (GrantedAuthority role : roles) {
            if (role.getAuthority().equals("ROLE_ADMIN")) {
                model.addObject("partners", userService.getUserByRole("REGIONAL"));
                model.addObject("encryptedID", EncryptionUtil.encrypt(emailId, OtpSalt.SALT.toString()));
            }
        }

        return model;
    }


    @RequestMapping(value = "/searchNominee/{partnerId}", method = RequestMethod.GET)
    public ModelAndView viewSearchNominee(final HttpServletRequest request, @PathVariable String partnerId) {
        ModelAndView mav = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String emailId = auth.getPrincipal().toString();
        Collection<? extends GrantedAuthority> roles = auth.getAuthorities();
        for (GrantedAuthority role : roles) {
            if (role.getAuthority().equals("ROLE_NOMINEE")) {
                // find partner
                Partners partnersInfo = partnersService.getPartnersById(partnerId);
                // find nominees
                mav.addObject("nomineeDetailsList", nomineeService.getNomineeByState(partnersInfo.getState()));
                mav.addObject("encryptedID", EncryptionUtil.encrypt(emailId, OtpSalt.SALT.toString()));
                mav.setViewName(VIEW_NAME);
            }

        }
        return mav;
    }

    /* To send all the expert details in json format*/
    @RequestMapping(value = "/nominees/{partnerId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public void expertJson(final HttpServletResponse response, @PathVariable String partnerId) throws IOException {

        Partners partnersInfo = partnersService.getPartnersById(partnerId);
        List<NomineeDetails> list = nomineeService.getNomineeByState(partnersInfo.getState().split(","));
        Gson gson = new Gson();
        String result = gson.toJson(list);
        response.getWriter().println(result);
    }

    @PostMapping("/neas/api/nominees/file")
    public ResponseEntity nomineeFileUpload(@RequestParam("nomineeFile") MultipartFile uploadfile, @RequestParam("nomineeId") final Long nomineeId){
        if (uploadfile.isEmpty()) {
            return new ResponseEntity("please select a file!", HttpStatus.OK);
        }

        String filename = storageService.store(uploadfile);
        // update for file name
        NomineeDetails nomineeDetails = nomineeService.getNomineeById(nomineeId);
        nomineeDetails.setFileName(filename);
        nomineeService.saveNominee(nomineeDetails);

        return new ResponseEntity("Successfully uploaded - " +filename, new HttpHeaders(), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/neas/api/addexpert", method = RequestMethod.GET)
	public ModelAndView goSCCatPage(HttpServletRequest request) {
		
		return new ModelAndView("/addexpert");
	}
    
    @RequestMapping(value = "/neas/api/register/expert", method = RequestMethod.POST)
	public ModelAndView registerExpert(HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		
		Expert expert = new Expert();
		try{
			boolean status = false;
			boolean statusU = false;
			//Long count = (long) expertService.getExpertCountCheck(request.getParameter("partnerwith")).size();
			// if (count < 3){
			/*Evaluation eval = evaluationService.get*/
			expert.setAddress(request.getParameter("address"));
			expert.setContactNumber(request.getParameter("contact"));
			expert.setDesignation(request.getParameter("designation"));			
			expert.setEmailId(request.getParameter("emailid"));
			expert.setExpertName(request.getParameter("expertname"));
			expert.setPartnerWith(request.getParameter("partnerwith"));
			if(request.getParameter("specialization").equals("other")){
				expert.setSpecialization(request.getParameter("other-specialization"));
			}else{
				expert.setSpecialization(request.getParameter("specialization"));
			}
			if(request.getParameter("password").equals(request.getParameter("confirm"))){
				expert.setPassword(request.getParameter("password"));
			}
			else{
				model.setViewName("addexpert");
				model.addObject("errorMsg","false");
				return model;
			}

			status = expertService.saveExpert(expert);

			 
				 Expert expObj = expertService.getExpertByEmail(request.getParameter("emailid"));
				 User user = new User();
				 user.setFullName(request.getParameter("expertname"));
				 if(request.getParameter("password").equals(request.getParameter("confirm"))){
					 user.setPassword(expObj.getPassword());
					 user.setSalt(expObj.getSalt());
					}
				 else{
					 	model.setViewName("addexpert");
						model.addObject("errorMsg","false");
						return model;
					}
				 user.setRole("EXPERT");
				 user.setUsername(request.getParameter("emailid"));
				 statusU = userService.saveUser(user);
 			// }
			 if (status && statusU){
				 model.setViewName("addexpert");
				 model.addObject("successMsg","true");
				 return model;
			 }
		}
		catch(Exception ex){
			ex.printStackTrace();
		} 
		model.setViewName("addexpert");
		model.addObject("errorMsg","false");
		return model;
	}
    
    @RequestMapping(value = "/neas/api/assign/expert", method = RequestMethod.POST)
	public ModelAndView assignexpert(HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		
		Evaluation evaluation = new Evaluation();
		boolean status = false;
		try{
			NomineeDetails nominee = nomineeService.getNomineeByEmailId(request.getParameter("nomemail"));
			evaluation.setActiveStatus(true);
			evaluation.setContact(nominee.getContact());
			if(request.getParameter("select1") != null && request.getParameter("select1") != ""){
				evaluation.setExpert1(request.getParameter("select1"));
			}
			if(request.getParameter("select2") != null && request.getParameter("select2") != ""){
				evaluation.setExpert2(request.getParameter("select2"));
			}
			if(request.getParameter("select3") != null && request.getParameter("select3") != ""){
				evaluation.setExpert3(request.getParameter("select3"));
			}
			evaluation.setEmailId(request.getParameter("nomemail"));
			evaluation.setNomineeName(nominee.getNomineeName());
			evaluation.setState(nominee.getState());

			if(evaluationService.getNomineeByEmailId(request.getParameter("nomemail")) != null) {
				Evaluation evl = evaluationService.getNomineeByEmailId(request.getParameter("nomemail"));
				evaluation.setId(evl.getId());
				status = evaluationService.updateEvaluationDeatails(evaluation);
			}
			else {
				status = evaluationService.saveEvaluationDetails(evaluation);
			}
			if (status){
				Thread t1 = new Thread(new Runnable() {
					@Override
					public void run() {
						mailService.sendSingleAssignExpertMail(evaluation);
					}
				});
				t1.start();
				 model.addObject("successMsg","Data has been added Successfully.");
				 return model;
			 }
		}
		catch(Exception ex){
			ex.printStackTrace();
		} 
		model.addObject("errorMsg","Something went wrong! Please try again.");		
		return model;
	}
    
    @RequestMapping(value = "/neas/api/list/expert", method = RequestMethod.POST)
	public void expertListing(HttpServletResponse response, HttpServletRequest request)throws IOException,ServletException{
		List<Expert> exp = expertService.getExpertCountCheck(request.getParameter("regionmail")); 
		/*Map<String,List<Expert>> map = new HashMap<>();
		map.put("count",exp);
		Gson gson = new Gson();
		String result = gson.toJson(map);
		response.getWriter().println(result);*/
		JSONArray array = new JSONArray();
		for (int i = 0; i < exp.size(); i++)
			try {
				JSONObject result = new JSONObject();
				result.put("emailId", exp.get(i).getEmailId());
				result.put("expertName", exp.get(i).getExpertName()+", "+exp.get(i).getSpecialization());
				array.put(i, result);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		response.setContentType("application/json");
		response.getWriter().write(array.toString());
	}
    
    @RequestMapping(value = "/neas/api/check/expert", method = RequestMethod.POST)
	public void pertnerCountChecker(HttpServletResponse response, HttpServletRequest request)throws IOException,ServletException{
		Long count = (long) expertService.getExpertCountCheck(request.getParameter("partner")).size();
		Map<String,String> map = new HashMap<>();
		map.put("count",String.valueOf(count));
		Gson gson = new Gson();
		String result = gson.toJson(map);
		response.getWriter().println(result);
	}
    
    @RequestMapping(value = "/neas/api/assign/expert/ent", method = RequestMethod.POST)
	public ModelAndView assignexpertEnt(HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		
		Evaluation evaluation = new Evaluation();
		boolean status = false;
		try{
			String emails = request.getParameter("entemail");
			String[] emailAr = emails.split(",");
			for (int i = 0; i < emailAr.length; i++) {
			NomineeDetails nominee = nomineeService.getNomineeByEmailId(emailAr[i]);
			evaluation.setActiveStatus(true);
			evaluation.setContact(nominee.getContact());
			if(request.getParameter("select4") != null && request.getParameter("select4") != ""){
				evaluation.setExpert1(request.getParameter("select4"));
			}
			if(request.getParameter("select5") != null && request.getParameter("select5") != ""){
				evaluation.setExpert2(request.getParameter("select5"));
			}
			if(request.getParameter("select6") != null && request.getParameter("select6") != ""){
				evaluation.setExpert3(request.getParameter("select6"));
			}
			evaluation.setEmailId(emailAr[i]);
			evaluation.setNomineeName(nominee.getNomineeName());
			evaluation.setState(nominee.getState());

			if(evaluationService.getNomineeByEmailId(emailAr[i]) != null) {
				Evaluation evl = evaluationService.getNomineeByEmailId(emailAr[i]);
				evaluation.setId(evl.getId());
				status = evaluationService.updateEvaluationDeatails(evaluation);
			}
			else {
				status = evaluationService.saveEvaluationDetails(evaluation);
			}
			}
			if (status){
				Thread t1 = new Thread(new Runnable() {
					@Override
					public void run() {
						mailService.sendSingleAssignExpertMail(evaluation);
					}
				});
				t1.start();
				 model.addObject("successMsg","Data has been added Successfully.");
				 return model;
			 }
		}
		catch(Exception ex){
			ex.printStackTrace();
		} 
		model.addObject("errorMsg","Something went wrong! Please try again.");		
		return model;
	}
    
    @RequestMapping(value = "/neas/api/assign/expert/eco", method = RequestMethod.POST)
	public ModelAndView assignexpertEco(HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		
		Evaluation evaluation = new Evaluation();
		boolean status =false;
		try{
			String emails = request.getParameter("ecoemail");
			String[] emailAr = emails.split(",");
			for (int i = 0; i < emailAr.length; i++) {
			NomineeDetails nominee = nomineeService.getNomineeByEmailId(emailAr[i]);
			evaluation.setActiveStatus(true);
			evaluation.setContact(nominee.getContact());
			if(request.getParameter("select7") != null && request.getParameter("select7") != ""){
				evaluation.setExpert1(request.getParameter("select7"));
			}
			evaluation.setEmailId(emailAr[i]);
			evaluation.setNomineeName(nominee.getNomineeName());
			evaluation.setState(nominee.getState());

			
			if(evaluationService.getNomineeByEmailId(emailAr[i]) != null) {
				Evaluation evl = evaluationService.getNomineeByEmailId(emailAr[i]);
				evaluation.setId(evl.getId());
				status = evaluationService.updateEvaluationDeatails(evaluation);
			}
			else {
				status = evaluationService.saveEvaluationDetails(evaluation);
			}
			
			 
			}
			if (status){
				Thread t1 = new Thread(new Runnable() {
					@Override
					public void run() {
						mailService.sendSingleAssignExpertMail(evaluation);
					}
				});
				t1.start();
				 model.addObject("successMsg","Data has been added Successfully.");
				 return model;
			 }
		}
		catch(Exception ex){
			ex.printStackTrace();
		} 
		model.addObject("errorMsg","Something went wrong! Please try again.");		
		return model;
	}
    
    @RequestMapping(value = "/neas/api/regional/shortlisted", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void getTotalNominator(HttpServletResponse response)throws IOException,ServletException{
    	String emailId = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
		User user = userService.getUserByUsername(emailId);
		String[] state = user.getState().split(",");
		Long nomi = (long)nomineeService.getNomineeRegionalShortlist(state).size();
		//Long nomi = (long) evaluationService.getAllEvaluationValues().size();
		Map<String,Long> map = new HashMap<>();
		map.put("count",nomi);
		Gson gson = new Gson();
		String result = gson.toJson(map);
		response.getWriter().println(result);
       }
    
    /*  To get all the Regional Shortlisted data */
	
	@RequestMapping(value = "/neas/api/regionalshortlisteddata", method = RequestMethod.POST)
	public void nomineeRegionalShortlisted(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		String emailId = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
		User user = userService.getUserByUsername(emailId);
		String[] state = user.getState().split(",");
		//List<NomineeDetails> u1 = nomineeService.getNomineeByState(state);
		List<Evaluation> u2 = evaluationService.getAllListByNominee(state);
		JSONArray array = new JSONArray();
		//for (int i = 0; i < u1.size(); i++)
			try {
				//String nomineeMail = u1.get(i).getEmailId();
				//List<Evaluation> u2 = evaluationService.getAllListByNominee(nomineeMail);
				for(int j=0; j < u2.size(); j++) {
					/*<th>ID</th>
	                <th>Nominee Name</th>
	                <th>Email-Id</th>
	                <th>Contact</th>
	                <th>State</th>
	                <th>Expert 1</th>
	                <th>Expert 2</th>
	                <th>Expert 3</th>
	                <th>Average Marks</th>*/
					JSONObject result = new JSONObject();
					result.put("id", u2.get(j).getId());
					result.put("nomineename", u2.get(j).getNomineeName());
					result.put("emailid", u2.get(j).getEmailId());
					result.put("contact", u2.get(j).getContact());
					result.put("state", u2.get(j).getState());
					result.put("expert1", u2.get(j).getExpert1());
					result.put("expert2", u2.get(j).getExpert2());
					result.put("expert3", u2.get(j).getExpert3());
					result.put("avg", "-");
					array.put(j, result);
				}

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		response.setContentType("application/json");
		response.getWriter().write(array.toString());
	}
	
	@RequestMapping(value = "/neas/api/promote/rank", method = RequestMethod.POST)
	public ModelAndView promoteRankNominee(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
    	ModelAndView model = new ModelAndView();
    	//String emailId = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
    	try{
    	NomineeDetails nominee = nomineeService.getNomineeByEmailId(request.getParameter("hideemail"));
    	nominee.setPromoteStatus(request.getParameter("promote"));
    	nominee.setRanking(request.getParameter("rank"));
    	
    	if(nomineeService.updateNominee(nominee)){
    		model.addObject("status", "true");
    	}
    	
    	}catch(Exception e){
    		model.addObject("status", "false");
    	}
    	return model;
    }
    
}
