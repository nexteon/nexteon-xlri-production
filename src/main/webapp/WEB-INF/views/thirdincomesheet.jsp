<div class="give-padding"></div>
<div class="left-form">
   <label for="email">
   Key Financial Details of the Enterprise
   </label>
   <table class="table table-bordered" id="basicFinancial">
      <tr>
         <th><b>Key Financial Data/Financial Year</b></th>
         <th>2014-15</th>
         <th>2015-16</th>
         <th>2016-17</th>
         <th>2017-18</th>
      </tr>
      <tr>
         <td>Total Sales or Revenue  of the Enterprise</td>
         <td ><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="Revenue14" class="form-control onlyDigit" data-name="reve2014" maxlength="9" onkeyup="reve2014_word.innerHTML=convertNumberToWords(this.value)"><div id="reve2014_word"></div></div> </td>
         <td ><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="Revenue15" class="form-control onlyDigit" data-name="reve2015" maxlength="9" onkeyup="reve2015_word.innerHTML=convertNumberToWords(this.value)"><div id="reve2015_word"></div></div> </td>
         <td ><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="Revenue16" class="form-control onlyDigit" data-name="reve2016" maxlength="9" onkeyup="reve2016_word.innerHTML=convertNumberToWords(this.value)"><div id="reve2016_word"></div></div> </td>
         <td ><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="Revenue17" class="form-control onlyDigit" data-name="reve2017" maxlength="9" onkeyup="reve2017_word.innerHTML=convertNumberToWords(this.value)"><div id="reve2017_word"></div></div> </td>
      </tr>
      <tr>
         <td>Total Cost of Goods/Services Sold</td>
         <td ><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="NetAssert14" class="form-control onlyDigit" data-name="netAssert2014" maxlength="9" onkeyup="netAssert2014_word.innerHTML=convertNumberToWords(this.value)"><div id="netAssert2014_word"></div></div> </td>
         <td ><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="NetAssert15" class="form-control onlyDigit" data-name="netAssert2015" maxlength="9" onkeyup="netAssert2015_word.innerHTML=convertNumberToWords(this.value)"><div id="netAssert2015_word"></div></div> </td>
         <td ><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="NetAssert16" class="form-control onlyDigit" data-name="netAssert2016" maxlength="9" onkeyup="netAssert2016_word.innerHTML=convertNumberToWords(this.value)"><div id="netAssert2016_word"></div></div> </td>
         <td ><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="NetAssert17" class="form-control onlyDigit" data-name="netAssert2017" maxlength="9" onkeyup="netAssert2017_word.innerHTML=convertNumberToWords(this.value)"><div id="netAssert2017_word"></div></div> </td>
      </tr>
      <tr>
         <td>Net Profit</td>
         <td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="NetProfit14" class="form-control onlyDigit" data-name="netProfit2014" maxlength="9" onkeyup="netProfit2014_word.innerHTML=convertNumberToWords(this.value)"><div id="netProfit2014_word"></div></div> </td>
         <td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="NetProfit15" class="form-control onlyDigit" data-name="netProfit2015" maxlength="9" onkeyup="netProfit2015_word.innerHTML=convertNumberToWords(this.value)"><div id="netProfit2015_word"></div></div> </td>
         <td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="NetProfit16" class="form-control onlyDigit" data-name="netProfit2016" maxlength="9" onkeyup="netProfit2016_word.innerHTML=convertNumberToWords(this.value)"><div id="netProfit2016_word"></div></div> </td>
         <td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="NetProfit17" class="form-control onlyDigit" data-name="netProfit2017" maxlength="9" onkeyup="netProfit2017_word.innerHTML=convertNumberToWords(this.value)"><div id="netProfit2017_word"></div></div> </td>
      </tr>
      <tr>
         <td>General Administrative Expenses</td>
         <td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="admin14" class="form-control onlyDigit" data-name="admin2014" maxlength="9" onkeyup="admin2014_word.innerHTML=convertNumberToWords(this.value)"><div id="admin2014_word"></div></div> </td>
         <td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="admin15" class="form-control onlyDigit" data-name="admin2015" maxlength="9" onkeyup="admin2015_word.innerHTML=convertNumberToWords(this.value)"><div id="admin2015_word"></div></div> </td>
         <td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="admin16" class="form-control onlyDigit" data-name="admin2016" maxlength="9" onkeyup="admin2016_word.innerHTML=convertNumberToWords(this.value)"><div id="admin2016_word"></div></div> </td>
         <td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="admin17" class="form-control onlyDigit" data-name="admin2017" maxlength="9" onkeyup="admin2017_word.innerHTML=convertNumberToWords(this.value)"><div id="admin2017_word"></div></div> </td>
      </tr>
      <tr>
         <td>Number of Employees</td>
         <td><div class="form-group"><i class="" aria-hidden="true"></i> <input type="text" id="employee14" class="form-control onlyDigit" data-name="employee2014" maxlength="9" onkeyup="employee2014_word.innerHTML=convertNumberToWords(this.value)"><div id="employee2014_word"></div></div> </td>
         <td><div class="form-group"><i class="" aria-hidden="true"></i> <input type="text" id="employee15" class="form-control onlyDigit" data-name="employee2015" maxlength="9" onkeyup="employee2015_word.innerHTML=convertNumberToWords(this.value)"><div id="employee2015_word"></div></div> </td>
         <td><div class="form-group"><i class="" aria-hidden="true"></i> <input type="text" id="employee16" class="form-control onlyDigit" data-name="employee2016" maxlength="9" onkeyup="employee2016_word.innerHTML=convertNumberToWords(this.value)"><div id="employee2016_word"></div></div> </td>
         <td><div class="form-group"><i class="" aria-hidden="true"></i> <input type="text" id="employee17" class="form-control onlyDigit" data-name="employee2017" maxlength="9" onkeyup="employee2017_word.innerHTML=convertNumberToWords(this.value)"><div id="employee2017_word"></div></div> </td>
      </tr>
      <tr>
         <td>Total wages or salary paid</td>
         <td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="wages14" class="form-control onlyDigit" data-name="wages2014" maxlength="9" onkeyup="wages2014_word.innerHTML=convertNumberToWords(this.value)"><div id="wages2014_word"></div></div> </td>
         <td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="wages15" class="form-control onlyDigit" data-name="wages2015" maxlength="9" onkeyup="wages2015_word.innerHTML=convertNumberToWords(this.value)"><div id="wages2015_word"></div></div> </td>
         <td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="wages16" class="form-control onlyDigit" data-name="wages2016" maxlength="9" onkeyup="wages2016_word.innerHTML=convertNumberToWords(this.value)"><div id="wages2016_word"></div></div> </td>
         <td><div class="form-group"><i class="fa fa-inr" aria-hidden="true"></i> <input type="text" id="wages17" class="form-control onlyDigit" data-name="wages2017" maxlength="9" onkeyup="wages2017_word.innerHTML=convertNumberToWords(this.value)"><div id="wages2017_word"></div></div> </td>
      </tr>
      <tr>
         <td>Number of Customers</td>
         <td><div class="form-group"><i class="" aria-hidden="true"></i> <input type="text" id="customers14" class="form-control onlyDigit" data-name="customers2014" maxlength="9" onkeyup="customers2014_word.innerHTML=convertNumberToWords(this.value)"><div id="customers2014_word"></div></div> </td>
         <td><div class="form-group"><i class="" aria-hidden="true"></i> <input type="text" id="customers15" class="form-control onlyDigit" data-name="customers2015" maxlength="9" onkeyup="customers2015_word.innerHTML=convertNumberToWords(this.value)"><div id="customers2015_word"></div></div> </td>
         <td><div class="form-group"><i class="" aria-hidden="true"></i> <input type="text" id="customers16" class="form-control onlyDigit" data-name="customers2016" maxlength="9" onkeyup="customers2016_word.innerHTML=convertNumberToWords(this.value)"><div id="customers2016_word"></div></div> </td>
         <td><div class="form-group"><i class="" aria-hidden="true"></i> <input type="text" id="customers17" class="form-control onlyDigit" data-name="customers2017" maxlength="9" onkeyup="customers2017_word.innerHTML=convertNumberToWords(this.value)"><div id="customers2017_word"></div></div> </td>
      </tr>
      <tr>
         <td>Number of branches/centres/units of the enterprise</td>
         <td><div class="form-group"><i class="" aria-hidden="true"></i> <input type="text" id="branches14" class="form-control onlyDigit" data-name="branches2014" maxlength="9" onkeyup="branches2014_word.innerHTML=convertNumberToWords(this.value)"><div id="branches2014_word"></div></div> </td>
         <td><div class="form-group"><i class="" aria-hidden="true"></i> <input type="text" id="branches15" class="form-control onlyDigit" data-name="branches2015" maxlength="9" onkeyup="branches2015_word.innerHTML=convertNumberToWords(this.value)"><div id="branches2015_word"></div></div> </td>
         <td><div class="form-group"><i class="" aria-hidden="true"></i> <input type="text" id="branches16" class="form-control onlyDigit" data-name="branches2016" maxlength="9" onkeyup="branches2016_word.innerHTML=convertNumberToWords(this.value)"><div id="branches2016_word"></div></div> </td>
         <td><div class="form-group"><i class="" aria-hidden="true"></i> <input type="text" id="branches17" class="form-control onlyDigit" data-name="branches2017" maxlength="9" onkeyup="branches2017_word.innerHTML=convertNumberToWords(this.value)"><div id="branches2017_word"></div></div> </td>
      </tr>
   </table>
   <input type="hidden" id="totalBasicFinance" name="totalBasicFinance">
</div>