package com.xlri.awards.config;

import com.xlri.awards.dao.entity.NomineeDetails;
import com.xlri.awards.dao.entity.Partners;
import com.xlri.awards.dao.entity.User;
import com.xlri.awards.dao.service.api.NomineeService;
import com.xlri.awards.dao.service.api.PartnersService;
import com.xlri.awards.dao.service.api.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

@Component
public class DataSetupLoader implements ApplicationListener<ContextRefreshedEvent> {
    boolean isSetup = true;

    @Autowired private UserService userService;
    @Autowired private PartnersService partnersService;
    @Autowired private NomineeService nomineeService;

    @Override public void onApplicationEvent(ContextRefreshedEvent event) {
        if(!isSetup) {
            System.out.println("--------------------- Setting up Data -------------------------");
            setUser();
            setPartners();
            //addNominees();
        }
    }

     void setUser() {
        User userInfo = new User();
        userInfo.setFullName("Test");
        userInfo.setPassword("Test"); // need salt
        userInfo.setUsername("test");
        userInfo.setRole("ADMIN");
        userInfo.setState("DL");

       // userService.saveUser(userInfo);
    }

    void setPartners() {
        // setup 10 partners
        for(int count = 1; count<11; count++) {
            Partners partnersInfo = new Partners();
            partnersInfo.setContactNumber("98765432" + count);
            partnersInfo.setAddress("Address");
            partnersInfo.setEmailId("Partner " +count+"@gmail.com");
            partnersInfo.setState("DL");
            partnersInfo.setPassword("Test");
            partnersInfo.setPartnerName("Partner " +count);

            partnersService.savePartners(partnersInfo);
        }
    }

    void addNominees() {
        for(int count = 1; count<11; count++) {
            NomineeDetails nomineeDetails = new NomineeDetails();
            nomineeDetails.setState("DL");
            nomineeDetails.setCity("New Delhi");
            nomineeDetails.setContact("+919876548" + count);
            nomineeDetails.setEmailId("nominee"+count+"@gmail.com");

            //nomineeService.saveNominee(nomineeDetails);
        }
    }

}
