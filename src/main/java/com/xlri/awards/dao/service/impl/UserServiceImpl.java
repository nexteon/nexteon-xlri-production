package com.xlri.awards.dao.service.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlri.awards.common.EncryptionUtil;
import com.xlri.awards.dao.entity.Nominator;
import com.xlri.awards.dao.entity.User;
import com.xlri.awards.dao.service.api.UserService;

@Repository
@Transactional
public class UserServiceImpl implements UserService{

	private static final Logger LOGGER = LoggerFactory.getLogger(NominatorServiceImpl.class);

	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public User getUserByUsername(String username) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<User> query =  session.createQuery("SELECT e FROM User e WHERE e.username=:username");
		    query.setParameter("username", username);
			session.getTransaction().commit();
			
			return query.getSingleResult();			
			
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public List<User> getAllUsers() {
		List<User> userList = null;
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			userList = session.createQuery("From User").getResultList();
			session.getTransaction().commit();
			
			return userList;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public boolean saveUser(User user) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			session.save(user);
			session.getTransaction().commit();
			session.close();
 			return true;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return false;
		}
	}

	@Override
	public boolean deleteUserByUsername(String username) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean checkCredentials(String password, User user) {
		String salt = user.getSalt();
		String encytPass = EncryptionUtil.encryptWithSHA(password+salt);
		if(encytPass.equals(user.getPassword())){
            return true;
        }else {
            return false;
        }
	}

	
	@Override
	public List<User> getUserByRole(String role) {
	
		
		try {
		Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<User> query =  session.createQuery("SELECT u FROM User u where u.role=:role");
			query.setParameter("role", role);
			session.getTransaction().commit();
			
			return query.getResultList();

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	
	
	@Override
	public List<User> getAllRole() {
	
		
		try {
		Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<User> query =  session.createQuery("SELECT u.role FROM User u");
			session.getTransaction().commit();
			
			return query.getResultList();

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public boolean updateUser(User user) {
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			session.update(user);
			session.getTransaction().commit();
			
 			return true;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return false;
		}
	}
	
	@Override
	public boolean checkNomineeUser(String email){
		try{
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<User> query =  session.createQuery("SELECT u FROM User u WHERE u.username=:username");
		    query.setParameter("username", email);
			session.getTransaction().commit();
			User user = query.getResultList().get(0);
			if(user!=null){
				return true;
			}else{
				return false;
			}
		}
		catch(Exception ex){
			LOGGER.error(ex.getMessage());
			return false;
		}
		
	}

	@Override
	public User getUserByState(String state) {
		
		try {
			Session session = sessionFactory.openSession();
			session.getTransaction().begin();
			Query<User> query = session.createQuery("SELECT u FROM User u WHERE u.state like :state");
			query.setParameter("state", "%" + state + "%");
			session.getTransaction().commit();
			return query.getSingleResult();
			

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	
}

