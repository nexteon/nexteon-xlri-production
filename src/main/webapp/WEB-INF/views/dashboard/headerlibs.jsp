 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<head>
        <meta charset="utf-8" />
        <title>NEAS DASHBOARD</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #1 for statistics, charts, recent events and reports" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="/resources/dashboard/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="/resources/dashboard/css/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="/resources/dashboard/css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        
        
        <link href="/resources/dashboard/css/morris/morris.css" rel="stylesheet" type="text/css" />
        
        
        <link href="/resources/dashboard/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="/resources/dashboard/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="/resources/dashboard/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="/resources/dashboard/css/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        
        <link href="/resources/dashboard/css/richtext.min.css" rel="stylesheet" >
        <link href="/resources/dashboard/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" />
        
        </head>
    <!-- END HEAD -->