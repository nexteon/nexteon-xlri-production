package com.xlri.awards.dao.service.api;

import java.util.List;

import com.xlri.awards.dao.entity.Nominator;
import com.xlri.awards.dao.entity.User;

public interface UserService {

	public User getUserByUsername(String username);

	public List<User> getAllUsers();

	public boolean saveUser(User user);

	public boolean updateUser(User user);

	public boolean deleteUserByUsername(String username);

	boolean checkCredentials(String password, User user);

	public List<User> getAllRole();

	public boolean checkNomineeUser(String email);

	public User getUserByState(String state);

	public List<User> getUserByRole(String role);


}
