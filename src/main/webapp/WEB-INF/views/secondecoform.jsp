<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
   <%@include file="fragments/headerlibs.jsp"%>
   <body class="innerpage">
      <%@include file="fragments/header.jsp"%>
      <!-- Body Starts Here -->
      <main role="main">
         <div class="page-title">
            <h2>Welcome, ${fullName}</h2>
         </div>
         <div class="content">
            <div class="container">
               <div class="row">
                  <div class="col-md-12">
                     <div class="whitebg">
                        <div class="row corner-box">
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label for="registrationNumber">Registration Number:
                                 <span class="heading-span">${registrationNumber}</span></label>
                              </div>
                           </div>
                        </div>
                        <div class="row corner-box">
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label for="awardcat">Award category:
                                 <span class="heading-span">Ecosystem Builder's Track</span></label>
                              </div>
                           </div>
                        </div>
                        <div class="circle-two">
                           <ul>
                              <li><a href="#"><span>Part A</span></a>
                              </li>
                              <li class="active-cir"><a href="#"><span>Part B</span></a></li>
                           </ul>
                        </div>
                        <div class="form-header">
                           <h4>PART B: Nominee's Basic Details</h4>
                        </div>
                        <div class="form-body">
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <label for="builder-category">Ecosystem Builder's Track</label>
                                    <select class="form-control" name="builder-category" id="builder-category" required>
                                       <option value="" selected disabled>Select</option>
                                       <option value="Entrepreneurship Development Institutes/Organisations">Entrepreneurship Development Institutes/Organisations</option>
                                       <option value="Mentor">Mentor</option>
                                       <option value="Incubator">Incubator</option>
                                       <option value="Promoters of Rural Producer Enterprises">Promoters of Rural Producer Enterprises</option>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                 </div>
                              </div>
                           </div>
                           <div class="row edioform">
                              <form id="ecosystembaseform1" method="POST" role="form" data-toggle="validator">
                                 <input type="hidden" name="registrationNumber" value="${registrationNumber}" />
                                 <input type="hidden" name="builder-category" value="Entrepreneurship Development Institutes/Organisations" />
                                 <div class="col-md-12">
                                    <div class="form-group">
                                       <label for="Programs-Offered">1. Nature of Programs Offered</label>
                                       <label class="radio-inline">
                                       <input type="checkbox" id="check1" name="question1" value="Full Time or Regular Course"><span class="label-text">Full Time or Regular Course</span>
                                       </label>
                                       <label class="radio-inline">
                                       <input type="checkbox" id="check2" name="question2" value="Part Time or Correspondence Course"><span class="label-text">Part Time or Correspondence Course</span>
                                       </label>
                                       <label class="radio-inline">
                                       <input type="checkbox" id="check3" name="question3" value="Online Course"><span class="label-text">Online Course</span>
                                       </label>
                                       <div class="help-block with-errors"></div>
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <div class="form-group">
                                       <label for="Level-Offered">2. Level of Programs Offered</label>

                                       <div class="col-md-12">
										   <label class="radio-inline col-md-3">
										   <input type="checkbox" id="q2a" name="question4" value="Masters"><span class="label-text">Masters</span>
										   </label>
										   <div class="col-md-9 q2a" style="display:none">
										       <div class="col-md-4">
										      	<label class="">Program Offered Since?</label> 
										       </div>
										      <div class="col-md-3">
                                                <select name="year1" class="form-control" id="yearida">
                                                   <option value="" selected disabled>YYYY</option>
                                                   <c:forEach begin="1978" end="2018" step="1" var="d">
                                                      <option value="${d}">${d}</option>
                                                   </c:forEach>
                                                </select>
                                             </div>
                                             <div class="col-md-3">
                                                <select name="month1" class="form-control" id="monthida">
                                                   <option value="" selected disabled>MM</option>
                                                   <option value="01">Jan</option>
                                                   <option value="02">Feb</option>
                                                   <option value="03">Mar</option>
                                                   <option value="04">Apr</option>
                                                   <option value="05">May</option>
                                                   <option value="06">Jun</option>
                                                   <option value="07">Jul</option>
                                                   <option value="08">Aug</option>
                                                   <option value="09">Sep</option>
                                                   <option value="10">Oct</option>
                                                   <option value="11">Nov</option>
                                                   <option value="12">Dec</option>
                                                </select>
                                             </div>
                                             <div class="col-md-2">
                                                <select name="day1" class="form-control" id="dayida">
                                                   <option value="" selected disabled>DD</option>
                                                   <option value="01">01</option>
                                                   <option value="02">02</option>
                                                   <option value="03">03</option>
                                                   <option value="04">04</option>
                                                   <option value="05">05</option>
                                                   <option value="06">06</option>
                                                   <option value="07">07</option>
                                                   <option value="08">08</option>
                                                   <option value="09">09</option>
                                                   <c:forEach begin="10" end="28" step="1" var="d">
                                                      <option value="${d}">${d}</option>
                                                   </c:forEach>
                                                   <option id="d29" style="display: none;" value="29">29</option>
                                                   <option id="d30" style="display: none;" value="30">30</option>
                                                   <option id="d31" style="display: none;" value="31">31</option>
                                                </select>
                                             </div>
                                             <div class="help-block with-errors"></div>
                                             <input type="hidden" class="form-control" name="Date-Offered1" id="dateeida">
                                          </div>
                                       </div>
                                       <div class="col-md-12">
                                          <label class="radio-inline col-md-3">
                                          <input type="checkbox" id="q2b" name="question5" value="Bachelors"><span class="label-text">Bachelors</span>
                                          </label>
                                          
                                          <div class="col-md-9 q2b" style="display:none">
                                          	   <div class="col-md-4">
										      	<label class="">Program Offered Since?</label> 
										       </div>
                                             <div class="col-md-3">
                                                <select name="year2" class="form-control" id="yearidb">
                                                   <option value="" selected disabled>YYYY</option>
                                                   <c:forEach begin="1978" end="2018" step="1" var="d">
                                                      <option value="${d}">${d}</option>
                                                   </c:forEach>
                                                </select>
                                             </div>
                                             <div class="col-md-3">
                                                <select name="month2" class="form-control" id="monthidb">
                                                   <option value="" selected disabled>MM</option>
                                                   <option value="01">Jan</option>
                                                   <option value="02">Feb</option>
                                                   <option value="03">Mar</option>
                                                   <option value="04">Apr</option>
                                                   <option value="05">May</option>
                                                   <option value="06">Jun</option>
                                                   <option value="07">Jul</option>
                                                   <option value="08">Aug</option>
                                                   <option value="09">Sep</option>
                                                   <option value="10">Oct</option>
                                                   <option value="11">Nov</option>
                                                   <option value="12">Dec</option>
                                                </select>
                                             </div>
                                             <div class="col-md-2">
                                                <select name="day2" class="form-control" id="dayidb">
                                                   <option value="" selected disabled>DD</option>
                                                   <option value="01">01</option>
                                                   <option value="02">02</option>
                                                   <option value="03">03</option>
                                                   <option value="04">04</option>
                                                   <option value="05">05</option>
                                                   <option value="06">06</option>
                                                   <option value="07">07</option>
                                                   <option value="08">08</option>
                                                   <option value="09">09</option>
                                                   <c:forEach begin="10" end="28" step="1" var="d">
                                                      <option value="${d}">${d}</option>
                                                   </c:forEach>
                                                   <option id="d29" style="display: none;" value="29">29</option>
                                                   <option id="d30" style="display: none;" value="30">30</option>
                                                   <option id="d31" style="display: none;" value="31">31</option>
                                                </select>
                                             </div>
                                             <div class="help-block with-errors"></div>
                                             <input type="hidden" class="form-control" name="Date-Offered2" id="dateeidb">
                                          </div>
                                       </div>
                                       <div class="col-md-12">
                                          <label class="radio-inline col-md-3">
                                          <input type="checkbox" id="q2c" name="question6" value="Diploma"><span class="label-text">Diploma</span>
                                          </label>
                                          
                                          <div class="col-md-9 q2c" style="display:none">
                                               <div class="col-md-4">
										      	<label class="">Program Offered Since?</label> 
										       </div>
                                             <div class="col-md-3">
                                                <select name="year3" class="form-control" id="yearidc">
                                                   <option value="" selected disabled>YYYY</option>
                                                   <c:forEach begin="1978" end="2018" step="1" var="d">
                                                      <option value="${d}">${d}</option>
                                                   </c:forEach>
                                                </select>
                                             </div>
                                             <div class="col-md-3">
                                                <select name="month3" class="form-control" id="monthidc">
                                                   <option value="" selected disabled>MM</option>
                                                   <option value="01">Jan</option>
                                                   <option value="02">Feb</option>
                                                   <option value="03">Mar</option>
                                                   <option value="04">Apr</option>
                                                   <option value="05">May</option>
                                                   <option value="06">Jun</option>
                                                   <option value="07">Jul</option>
                                                   <option value="08">Aug</option>
                                                   <option value="09">Sep</option>
                                                   <option value="10">Oct</option>
                                                   <option value="11">Nov</option>
                                                   <option value="12">Dec</option>
                                                </select>
                                             </div>
                                             <div class="col-md-2">
                                                <select name="day3" class="form-control" id="dayidc">
                                                   <option value="" selected disabled>DD</option>
                                                   <option value="01">01</option>
                                                   <option value="02">02</option>
                                                   <option value="03">03</option>
                                                   <option value="04">04</option>
                                                   <option value="05">05</option>
                                                   <option value="06">06</option>
                                                   <option value="07">07</option>
                                                   <option value="08">08</option>
                                                   <option value="09">09</option>
                                                   <c:forEach begin="10" end="28" step="1" var="d">
                                                      <option value="${d}">${d}</option>
                                                   </c:forEach>
                                                   <option id="d29" style="display: none;" value="29">29</option>
                                                   <option id="d30" style="display: none;" value="30">30</option>
                                                   <option id="d31" style="display: none;" value="31">31</option>
                                                </select>
                                             </div>
                                             <div class="help-block with-errors"></div>
                                             <input type="hidden" class="form-control" name="Date-Offered3" id="dateeidc">
                                          </div>
                                       </div>
                                       <div class="col-md-12">
                                          <label class="radio-inline col-md-3">
                                          <input type="checkbox" id="q2d" name="question7" value="Certificate"><span class="label-text">Certificate</span>
                                          </label>
                                          <div class="col-md-9 q2d" style="display:none">
                                          	<div class="col-md-4">
										      	<label class="">Program Offered Since?</label> 
										      </div>
                                             <div class="col-md-3">
                                                <select name="year4" class="form-control" id="yearidd">
                                                   <option value="" selected disabled>YYYY</option>
                                                   <c:forEach begin="1978" end="2018" step="1" var="d">
                                                      <option value="${d}">${d}</option>
                                                   </c:forEach>
                                                </select>
                                             </div>
                                             <div class="col-md-3">
                                                <select name="month4" class="form-control" id="monthidd">
                                                   <option value="" selected disabled>MM</option>
                                                   <option value="01">Jan</option>
                                                   <option value="02">Feb</option>
                                                   <option value="03">Mar</option>
                                                   <option value="04">Apr</option>
                                                   <option value="05">May</option>
                                                   <option value="06">Jun</option>
                                                   <option value="07">Jul</option>
                                                   <option value="08">Aug</option>
                                                   <option value="09">Sep</option>
                                                   <option value="10">Oct</option>
                                                   <option value="11">Nov</option>
                                                   <option value="12">Dec</option>
                                                </select>
                                             </div>
                                             <div class="col-md-2">
                                                <select name="day4" class="form-control" id="dayidd">
                                                   <option value="" selected disabled>DD</option>
                                                   <option value="01">01</option>
                                                   <option value="02">02</option>
                                                   <option value="03">03</option>
                                                   <option value="04">04</option>
                                                   <option value="05">05</option>
                                                   <option value="06">06</option>
                                                   <option value="07">07</option>
                                                   <option value="08">08</option>
                                                   <option value="09">09</option>
                                                   <c:forEach begin="10" end="28" step="1" var="d">
                                                      <option value="${d}">${d}</option>
                                                   </c:forEach>
                                                   <option id="d29" style="display: none;" value="29">29</option>
                                                   <option id="d30" style="display: none;" value="30">30</option>
                                                   <option id="d31" style="display: none;" value="31">31</option>
                                                </select>
                                             </div>
                                             <div class="help-block with-errors"></div>
                                             <input type="hidden" class="form-control" name="Date-Offered4" id="dateeidd">
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="Pro-Resource">4. Number of Professional Resource Persons (Faculty, Guest Faculty, etc.)</label>
                                       <input type="text" class="form-control onlyDigit" name="Pro-Resource" id="Pro-Resource" class="form-control" placeholder="Number of Professional Resource Persons" pattern="[0-9]{1,10}" data-pattern-error="Only 10 numeric Digits allowed" required>
                                       <div class="help-block with-errors"></div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="Student-Graduated">5. Number of Students Graduated (in past 3 years)</label>
                                       <input type="text" class="form-control onlyDigit" name="Student-Graduated" id="Student-Graduated" class="form-control" placeholder="Number of Students Graduated (in past 3 years)" pattern="[0-9]{1,10}" data-pattern-error="Only 10 numeric Digits allowed" required>
                                       <div class="help-block with-errors"></div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="Student-Entrepreneurs">6. Number of Students turned Entrepreneurs (in past 3 years)</label>
                                       <input type="text" class="form-control onlyDigit" name="Student-Entrepreneurs" id="Student-Entrepreneurs" class="form-control" placeholder="Number of Students turned Entrepreneurs (in past 3 years)" pattern="[0-9]{1,10}" data-pattern-error="Only 10 numeric Digits allowed" required>
                                       <div class="help-block with-errors"></div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="Student-IPR">7. Number of IPRs/Trademarks filed by Students (in past 3 years)</label>
                                       <input type="text" class="form-control onlyDigit" name="Student-IPR" id="Student-IPR" class="form-control" placeholder="Number of IPRs/Trademarks filed by Students (in past 3 years)" pattern="[0-9]{1,10}" data-pattern-error="Only 10 numeric Digits allowed" required>
                                       <div class="help-block with-errors"></div>
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <label for="Enterpreneur-References">8. Number of Entrepreneur References being provided</label>
                                    <div class="table-responsive">
                                       <table class="table table-bordered order-list Enterpreneur-References" id="Enterpreneur-References1">
                                          <thead>
                                             <tr>
                                                <td>Individual Name</td>
                                                <td>Enterprise Name</td>
                                                <td>Industry</td>
                                                <td>Years in Operation</td>
                                                <td>Mobile Number</td>
                                                <td>Email Address(if any)</td>
                                                <td>Website(if any)</td>
                                                <td>Quantum of funding received</td>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="individual-name1" class="form-control" placeholder="Individual Name" required/>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="enterprise-name1" class="form-control" placeholder="Enterprise Name" required/>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="industry1" class="form-control" placeholder="Industry" required/>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="years-in-operation1" class="form-control onlyDigit" pattern="[0-9]{1,10}" placeholder="Years in Operation" required/>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="mobile1" class="form-control onlyDigit" pattern="[0-9]{10}" placeholder="10 Digit Mobile Number" required/>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="email" name="email-address1" class="form-control" placeholder="Email"  pattern="^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$" data-pattern-error="Please provide a valid email id."/>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="website1" class="form-control" placeholder="Website" />
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="quantum-of-funding-received1" class="form-control onlyDigit" placeholder="Qunatum of funding received" required onkeyup="qunt1_word.innerHTML=convertNumberToWords(this.value)"><div id="qunt1_word"></div>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <a class="deleteRow"></a>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="individual-name2" class="form-control" placeholder="Individual Name" required/>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="enterprise-name2" class="form-control" placeholder="Enterprise Name" required/>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="industry2" class="form-control" placeholder="Industry" required/>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="years-in-operation2" class="form-control onlyDigit" pattern="[0-9]{1,10}" placeholder="Years in Operation" required/>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="mobile2" class="form-control onlyDigit" pattern="[0-9]{10}" placeholder="10 Digit Mobile Number" required/>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="email" name="email-address2" class="form-control" placeholder="Email"  pattern="^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$" data-pattern-error="Please provide a valid email id."/>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="website2" class="form-control" placeholder="Website" />
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="quantum-of-funding-received2" class="form-control onlyDigit" placeholder="Qunatum of funding received" required onkeyup="qunt2_word.innerHTML=convertNumberToWords(this.value)"><div id="qunt2_word"></div>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <a class="deleteRow"></a>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="individual-name3" class="form-control" placeholder="Individual Name" required/>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="enterprise-name3" class="form-control" placeholder="Enterprise Name" required/>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="industry3" class="form-control" placeholder="Industry" required/>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="years-in-operation3" class="form-control onlyDigit" pattern="[0-9]{1,10}" placeholder="Years in Operation" required/>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="mobile3" class="form-control onlyDigit" pattern="[0-9]{10}" placeholder="10 Digit Mobile Number" required/>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="email" name="email-address3" class="form-control" placeholder="Email"  pattern="^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$" data-pattern-error="Please provide a valid email id."/>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="website3" class="form-control" placeholder="Website" />
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="quantum-of-funding-received3" class="form-control" placeholder="Qunatum of funding received" required onkeyup="qunt3_word.innerHTML=convertNumberToWords(this.value)"><div id="qunt3_word"></div>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <a class="deleteRow"></a>
                                                </td>
                                             </tr>
                                          </tbody>
                                          <tfoot>
                                             <tr>
                                                <td colspan="9" style="text-align: left;">
                                                   <input type="button" class="btn btn-lg btn-block " id="addrow1" value="Add Row" />
                                                </td>
                                             </tr>
                                             <tr>
                                             </tr>
                                          </tfoot>
                                       </table>
                                    </div>
                                    <input type="hidden" name="Enterpreneur-References" value="" />
                                 </div>
                                 <input type="hidden" name="nomineeEmail" value="${nominee.emailId}" />
                                 <input type="hidden" name="category" value="ecosystem" />
                                 <input type="hidden" name="currentpath" value=""/>
                                 <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                                 <div class="col-md-12">
                                    <div class="row align-center">
                                       <div class="submit-btn">
                                          <a class="btn btn-warning" href="/basicdetails?p1=true&p2=${encryptedEmail}&p3=ecosystembaseform">Back</a>
                                          <span class="btn btn-info review-form" data-toggle="modal" data-target="#myModal">Review Form</span>
                                          <input class="btn btn-warning" id="ecosystemsave1" type="button" value="Save As Draft">
                                          <input class="btn btn-primary" id="submitNomination1" type="button" name="Submit" value="Submit Nomination">
                                       </div>
                                    </div>
                                 </div>
                              </form>
                           </div>
                           <div class="row mentorform">
                              <form id="ecosystembaseform2" method="POST" role="form" data-toggle="validator" >
                                 <input type="hidden" name="registrationNumber" value="${registrationNumber}" />
                                 <input type="hidden" name="builder-category" value="Mentor" />
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="Mentees-Mentored">1. Number of Mentees Mentored (in past 3 years)</label>
                                       <input type="text" class="form-control onlyDigit" name="Mentees-Mentored" id="Mentees-Mentored" class="form-control" placeholder="Number of Mentees Mentored (in past 3 years)" pattern="[0-9]{1,10}" data-pattern-error="Only 10 numeric Digits allowed" required>
                                       <div class="help-block with-errors"></div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="Mentees-Entrepreneurs">2. How many Mentees turned Entrepreneurs (in past 3 years)</label>
                                       <input type="text" class="form-control onlyDigit" name="Mentees-Entrepreneurs" id="Mentees-Entrepreneurs" class="form-control" placeholder="How many Mentees turned Entrepreneurs (in past 3 years)" pattern="[0-9]{1,10}" data-pattern-error="Only 10 numeric Digits allowed" required>
                                       <div class="help-block with-errors"></div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="Mentees-IPR">3. Number of IPRs/Trademarks filed by Mentees (in past 3 years)</label>
                                       <input type="text" class="form-control onlyDigit" name="Mentees-IPR" id="Mentees-IPR" class="form-control" placeholder="Number of IPRs/Trademarks filed by Mentees (in past 3 years)" pattern="[0-9]{1,10}" data-pattern-error="Only 10 numeric Digits allowed" required>
                                       <div class="help-block with-errors"></div>
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <div class="form-group">
                                       <label for="Enterpreneur-References">4. Number of Entrepreneur References being provided</label>
                                       <table class="table table-responsive order-list Enterpreneur-References" id="Enterpreneur-References2">
                                          <thead>
                                             <tr>
                                                <td>Individual Name</td>
                                                <td>Enterprise Name</td>
                                                <td>Industry</td>
                                                <td>Years in Operation</td>
                                                <td>Mobile Number</td>
                                                <td>Email Address(if any)</td>
                                                <td>Website(if any)</td>
                                                <td>Quantum of funding received</td>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="individual-name1" class="form-control" placeholder="Individual Name" required/>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="enterprise-name1" class="form-control" placeholder="Enterprise Name" required/>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="industry1" class="form-control" placeholder="Industry" required/>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="years-in-operation1" class="form-control onlyDigit" pattern="[0-9]{1,10}" placeholder="Years in Operation" required/>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="mobile1" class="form-control onlyDigit" pattern="[0-9]{10}" placeholder="10 Digit Mobile Number" required/>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="email" name="email-address1" class="form-control" placeholder="Email"  pattern="^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$" data-pattern-error="Please provide a valid email id."/>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="website1" class="form-control" placeholder="Website" />
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="quantum-of-funding-received1" class="form-control onlyDigit" placeholder="Qunatum of funding received" required onkeyup="qunt4_word.innerHTML=convertNumberToWords(this.value)"><div id="qunt4_word"></div>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <a class="deleteRow"></a>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="individual-name2" class="form-control" placeholder="Individual Name" required/>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="enterprise-name2" class="form-control" placeholder="Enterprise Name" required/>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="industry2" class="form-control" placeholder="Industry" required/>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="years-in-operation2" class="form-control onlyDigit" pattern="[0-9]{1,10}" placeholder="Years in Operation" required/>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="mobile2" class="form-control onlyDigit" pattern="[0-9]{10}" placeholder="10 Digit Mobile Number" required/>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="email" name="email-address2" class="form-control" placeholder="Email"  pattern="^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$" data-pattern-error="Please provide a valid email id."/>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="website2" class="form-control" placeholder="Website" />
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="quantum-of-funding-received2" class="form-control onlyDigit" placeholder="Qunatum of funding received" required  onkeyup="qunt5_word.innerHTML=convertNumberToWords(this.value)"><div id="qunt5_word"></div>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <a class="deleteRow"></a>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="individual-name3" class="form-control" placeholder="Individual Name" required/>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="enterprise-name3" class="form-control" placeholder="Enterprise Name" required/>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="industry3" class="form-control" placeholder="Industry" required/>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="years-in-operation3" class="form-control onlyDigit" pattern="[0-9]{1,10}" placeholder="Years in Operation" required/>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="mobile3" class="form-control onlyDigit" pattern="[0-9]{10}" placeholder="10 Digit Mobile Number" required/>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="email" name="email-address3" class="form-control" placeholder="Email"  pattern="^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$" data-pattern-error="Please provide a valid email id."/>
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="website3" class="form-control" placeholder="Website" />
                                                   </div>
                                                </td>
                                                <td>
                                                   <div class="form-group">
                                                      <input type="text" name="quantum-of-funding-received3" class="form-control onlyDigit" placeholder="Qunatum of funding received" required  onkeyup="qunt6_word.innerHTML=convertNumberToWords(this.value)"><div id="qunt6_word"></div>
                                                      <div class="help-block with-errors"></div>
                                                   </div>
                                                </td>
                                                <td>
                                                   <a class="deleteRow"></a>
                                                </td>
                                             </tr>
                                          </tbody>
                                          <tfoot>
                                             <tr>
                                                <td colspan="7" style="text-align: left;">
                                                   <input type="button" class="btn btn-lg btn-block " id="addrow2" value="Add Row" />
                                                </td>
                                             </tr>
                                             <tr>
                                             </tr>
                                          </tfoot>
                                       </table>
                                       <div class="help-block with-errors"></div>
                                       <input type="hidden" name="Enterpreneur-References" value="" />
                                    </div>
                                 </div>
                                 <input type="hidden" name="nomineeEmail" value="${nominee.emailId}" />
                                 <input type="hidden" name="category" value="ecosystem" />
                                 <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                                 <div class="col-md-12">
                                    <div class="row align-center">
                                       <div class="submit-btn">
                                          <input type="hidden" name="currentpath" value=""/>
                                          <a class="btn btn-warning" href="/basicdetails?p1=true&p2=${encryptedEmail}&p3=ecosystembaseform">Back</a>
                                          <span class="btn btn-info review-form" data-toggle="modal" data-target="#myModal">Review Form</span>
                                          <input class="btn btn-warning" id="ecosystemsave2" type="button" value="Save As Draft">
                                          <input class="btn btn-primary" id="submitNomination2" type="button" name="Submit" value="Submit Nomination">
                                       </div>
                                    </div>
                                 </div>
                              </form>
                           </div>
                           <div class="row incubatorform">
                              <form id="ecosystembaseform3" method="POST" role="form" data-toggle="validator" >
                                 <input type="hidden" name="registrationNumber" value="${registrationNumber}" />
                                 <input type="hidden" name="builder-category" value="Incubator" />
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="Incubatees">1. Number of Incubatees (in past 3 years)</label>
                                       <input type="text" class="form-control onlyDigit" name="Incubatees" id="Incubatees" class="form-control" placeholder="Number of Incubatees (in past 3 years)" pattern="[0-9]{1,10}" data-pattern-error="Only 10 numeric Digits allowed" required>
                                       <div class="help-block with-errors"></div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="Professional-Resource">2. Number of Professional Resource Persons (Faculty, Guest Faculty, etc.)</label>
                                       <input type="text" class="form-control onlyDigit" name="Professional-Resource" id="Professional-Resource" class="form-control" placeholder="Number of Professional Resource Persons (Faculty, Guest Faculty, etc.)" pattern="[0-9]{1,10}" data-pattern-error="Only 10 numeric Digits allowed" required>
                                       <div class="help-block with-errors"></div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="Quantum-Funds">3. Quantum of Funds Invested/Granted to Incubatee Enterprises</label>
                                       <div class="input-group">
                                          <input type="text" class="form-control" name="Quantum-Funds" id="Quantum-Funds" class="form-control onlyDigit" placeholder="Quantum of Funds Invested/Granted to Incubatee Enterprises" required onkeyup="qunt7_word.innerHTML=convertNumberToWords(this.value)"><div id="qunt7_word"></div>
                                          <div class="help-block with-errors"></div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="Services-Provided">4. Services Provided?</label>
                                       <select class="form-control" multiple name="Services-Provided" id="Services-Provided" data-error="Please fill in this field." required>
                                          <option value="" selected disabled>Select</option>
                                          <option value="Capital">1. Capital</option>
                                          <option value="Space">2. Space</option>
                                          <option value="Mentorship">3. Mentorship</option>
                                          <option value="Refining Ideas">4. Refining Ideas</option>
                                       </select>
                                       <div class="help-block with-errors"></div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="Monetized-Equity">5. If Capital provided, Total Monetized Equity Value held by Incubator (as on 31 Mar 2018)</label>
                                       <div class="input-group">
                                          <input type="text" name="Monetized-Equity" id="Monetized-Equity" class="form-control" placeholder="If Capital provided, Total Monetized Equity Value held by Incubator (as on 31 Mar 2018)">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="Trademarks-IPR">6. Number of IPRs/Trademarks filed by Incubatees (in past 3 years)</label>
                                       <input type="text" class="form-control onlyDigit" name="Trademarks-IPR" id="Trademarks-IPR" class="form-control" placeholder="Number of IPRs/Trademarks filed by Incubatees (in past 3 years)" pattern="[0-9]{1,10}" data-pattern-error="Only 10 numeric Digits allowed" required>
                                       <div class="help-block with-errors"></div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="Incubatees-Entrepreneurs">7. How many Incubatees turned Entrepreneurs (in past 3 years)</label>
                                       <input type="text" class="form-control onlyDigit" name="Incubatees-Entrepreneurs" id="Incubatees-Entrepreneurs" class="form-control" placeholder="How many Incubatees turned Entrepreneurs (in past 3 years)" pattern="[0-9]{1,10}" data-pattern-error="Only 10 numeric Digits allowed" required>
                                       <div class="help-block with-errors"></div>
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <label for="Enterpreneur-References">8. Number of Entrepreneur References being provided</label>
                                    <table class="table table-responsive order-list Enterpreneur-References" id="Enterpreneur-References3">
                                       <thead>
                                          <tr>
                                             <td>Individual Name</td>
                                             <td>Enterprise Name</td>
                                             <td>Industry</td>
                                             <td>Years in Operation</td>
                                             <td>Mobile Number</td>
                                             <td>Email Address(if any)</td>
                                             <td>Website(if any)</td>
                                             <td>Quantum of funding received</td>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <tr>
                                             <td>
                                                <div class="form-group">
                                                   <input type="text" name="individual-name1" class="form-control" placeholder="Individual Name" required/>
                                                   <div class="help-block with-errors"></div>
                                                </div>
                                             </td>
                                             <td>
                                                <div class="form-group">
                                                   <input type="text" name="enterprise-name1" class="form-control" placeholder="Enterprise Name" required/>
                                                   <div class="help-block with-errors"></div>
                                                </div>
                                             </td>
                                             <td>
                                                <div class="form-group">
                                                   <input type="text" name="industry1" class="form-control" placeholder="Industry" required/>
                                                   <div class="help-block with-errors"></div>
                                                </div>
                                             </td>
                                             <td>
                                                <div class="form-group">
                                                   <input type="text" name="years-in-operation1" class="form-control onlyDigit" pattern="[0-9]{1,10}" placeholder="Years in Operation" required/>
                                                   <div class="help-block with-errors"></div>
                                                </div>
                                             </td>
                                             <td>
                                                <div class="form-group">
                                                   <input type="text" name="mobile1" class="form-control onlyDigit" pattern="[0-9]{10}" placeholder="10 Digit Mobile Number" required/>
                                                   <div class="help-block with-errors"></div>
                                                </div>
                                             </td>
                                             <td>
                                                <div class="form-group">
                                                   <input type="email" name="email-address1" class="form-control" placeholder="Email"  pattern="^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$" data-pattern-error="Please provide a valid email id."/>
                                                </div>
                                             </td>
                                             <td>
                                                <div class="form-group">
                                                   <input type="text" name="website1" class="form-control" placeholder="Website" />
                                                </div>
                                             </td>
                                             <td>
                                                <div class="form-group">
                                                   <input type="text" name="quantum-of-funding-received1" class="form-control onlyDigit" placeholder="Qunatum of funding received" required onkeyup="qunt8_word.innerHTML=convertNumberToWords(this.value)"><div id="qunt8_word"></div>
                                                   <div class="help-block with-errors"></div>
                                                </div>
                                             </td>
                                             <td>
                                                <a class="deleteRow"></a>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td>
                                                <div class="form-group">
                                                   <input type="text" name="individual-name2" class="form-control" placeholder="Individual Name" required/>
                                                   <div class="help-block with-errors"></div>
                                                </div>
                                             </td>
                                             <td>
                                                <div class="form-group">
                                                   <input type="text" name="enterprise-name2" class="form-control" placeholder="Enterprise Name" required/>
                                                   <div class="help-block with-errors"></div>
                                                </div>
                                             </td>
                                             <td>
                                                <div class="form-group">
                                                   <input type="text" name="industry2" class="form-control" placeholder="Industry" required/>
                                                   <div class="help-block with-errors"></div>
                                                </div>
                                             </td>
                                             <td>
                                                <div class="form-group">
                                                   <input type="text" name="years-in-operation2" class="form-control onlyDigit" pattern="[0-9]{1,10}" placeholder="Years in Operation" required/>
                                                   <div class="help-block with-errors"></div>
                                                </div>
                                             </td>
                                             <td>
                                                <div class="form-group">
                                                   <input type="text" name="mobile2" class="form-control" pattern="[0-9]{10}" placeholder="10 Digit Mobile Number" required/>
                                                   <div class="help-block with-errors"></div>
                                                </div>
                                             </td>
                                             <td>
                                                <div class="form-group">
                                                   <input type="email" name="email-address2" class="form-control" placeholder="Email"  pattern="^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$" data-pattern-error="Please provide a valid email id."/>
                                                </div>
                                             </td>
                                             <td>
                                                <div class="form-group">
                                                   <input type="text" name="website2" class="form-control" placeholder="Website" />
                                                </div>
                                             </td>
                                             <td>
                                                <div class="form-group">
                                                   <input type="text" name="quantum-of-funding-received2" class="form-control onlyDigite ap" placeholder="Qunatum of funding received" required onkeyup="qunt9_word.innerHTML=convertNumberToWords(this.value)"><div id="qunt9_word"></div>
                                                   <div class="help-block with-errors"></div>
                                                </div>
                                             </td>
                                             <td>
                                                <a class="deleteRow"></a>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td>
                                                <div class="form-group">
                                                   <input type="text" name="individual-name3" class="form-control" placeholder="Individual Name" required/>
                                                   <div class="help-block with-errors"></div>
                                                </div>
                                             </td>
                                             <td>
                                                <div class="form-group">
                                                   <input type="text" name="enterprise-name3" class="form-control" placeholder="Enterprise Name" required/>
                                                   <div class="help-block with-errors"></div>
                                                </div>
                                             </td>
                                             <td>
                                                <div class="form-group">
                                                   <input type="text" name="industry3" class="form-control" placeholder="Industry" required/>
                                                   <div class="help-block with-errors"></div>
                                                </div>
                                             </td>
                                             <td>
                                                <div class="form-group">
                                                   <input type="text" name="years-in-operation3" class="form-control" pattern="[0-9]{1,10}" placeholder="Years in Operation" required/>
                                                   <div class="help-block with-errors"></div>
                                                </div>
                                             </td>
                                             <td>
                                                <div class="form-group">
                                                   <input type="text" name="mobile3" class="form-control" pattern="[0-9]{10}" placeholder="10 Digit Mobile Number" required/>
                                                   <div class="help-block with-errors"></div>
                                                </div>
                                             </td>
                                             <td>
                                                <div class="form-group">
                                                   <input type="email" name="email-address3" class="form-control" placeholder="Email"  pattern="^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$" data-pattern-error="Please provide a valid email id."/>
                                                </div>
                                             </td>
                                             <td>
                                                <div class="form-group">
                                                   <input type="text" name="website3" class="form-control" placeholder="Website" />
                                                </div>
                                             </td>
                                             <td>
                                                <div class="form-group">
                                                   <input type="text" name="quantum-of-funding-received3" class="form-control onlyDigit" placeholder="Qunatum of funding received" required onkeyup="qunt10_word.innerHTML=convertNumberToWords(this.value)"><div id="qunt10_word"></div>
                                                   <div class="help-block with-errors"></div>
                                                </div>
                                             </td>
                                             <td>
                                                <a class="deleteRow"></a>
                                             </td>
                                          </tr>
                                       </tbody>
                                       <tfoot>
                                          <tr>
                                             <td colspan="7" style="text-align: left;">
                                                <input type="button" class="btn btn-lg btn-block " id="addrow3" value="Add Row" />
                                             </td>
                                          </tr>
                                          <tr>
                                          </tr>
                                       </tfoot>
                                    </table>
                                    <div class="help-block with-errors"></div>
                                    <input type="hidden" name="Enterpreneur-References" value="" />
                                 </div>
                                 <input type="hidden" name="nomineeEmail" value="${nominee.emailId}" />
                                 <input type="hidden" name="category" value="ecosystem" />
                                 <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                                 <div class="col-md-12">
                                    <div class="row align-center">
                                       <div class="submit-btn">
                                          <input type="hidden" name="currentpath" value=""/>
                                          <a class="btn btn-warning" href="/basicdetails?p1=true&p2=${encryptedEmail}&p3=ecosystembaseform">Back</a>
                                          <span class="btn btn-info review-form" data-toggle="modal" data-target="#myModal">Review Form</span>
                                          <input class="btn btn-warning" id="ecosystemsave3" type="button" value="Save As Draft">
                                          <input class="btn btn-primary" id="submitNomination3" type="button" name="Submit" value="Submit Nomination">
                                       </div>
                                    </div>
                                 </div>
                              </form>
                           </div>
                           <div class="row ruralform">
                              <form id="ecosystembaseform4"  method="POST" role="form" data-toggle="validator">
                                 <input type="hidden" name="registrationNumber" value="${registrationNumber}" />
                                 <input type="hidden" name="builder-category" value="Promoters of Rural Producer Enterprises" />
                                 <div class="col-md-6">
                                       <label for="Area-Covered">1. Number of States, Districts, Villages covered?</label>
                                       <div class="row">
										    <div class="col-md-4">
										        <div class="form-group">
										            <label>State</label>
										            <input type="text" class="state" name="rural-state" required class="form-control onlyDigit">
										            <div class="help-block with-errors"></div>
										        </div>
										    </div>
										
										    <div class="col-md-4">
										        <div class="form-group">
										            <label>District</label>
										            <input type="text" class="city" name="rural-city" required class="form-control onlyDigit">
										            <div class="help-block with-errors"></div>
										        </div>
										    </div>
										
										    <div class="col-md-4">
										        <div class="form-group">
										            <label>Village</label>
										            <input type="text" class="village" name="rural-village" required class="form-control onlyDigit">
										            <div class="help-block with-errors"></div>
										        </div>										
										    </div>
										
										    <input type="hidden" name="Area-Covered" id="Area-Covered" class="address_add">
										</div>
                                       
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="Total-Population">2. Total Population covered?</label>
                                       <input type="text" class="form-control" name="Total-Population" id="Total-Population" class="form-control" placeholder="Total Population covered?" pattern="[0-9]{1,10}" data-pattern-error="Only 10 numeric Digits allowed" required>
                                       <div class="help-block with-errors"></div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="Cooperative-Societies">3. Number of Cooperative Societies/Self-Help Groups?</label>
                                       <input type="text" class="form-control" name="Cooperative-Societies" id="Cooperative-Societies" class="form-control" placeholder="Number of Cooperative Societies/Self-Help Groups?" pattern="[0-9]{1,10}" data-pattern-error="Only 10 numeric Digits allowed" required>
                                       <div class="help-block with-errors"></div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="Rural-Producers">4. Number of Rural Producers Associated (including through Societies & SHGs)</label>
                                       <input type="text" class="form-control" name="Rural-Producers" id="Rural-Producers" class="form-control" placeholder="Number of Rural Producers Associated (including through Societies & SHGs)" pattern="[0-9]{1,10}" data-pattern-error="Only 10 numeric Digits allowed" required>
                                       <div class="help-block with-errors"></div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="Working-Industries">5. What all Industry/Industries are you working with? (eg: Dairy, Handicrafts, etc.)</label>
                                       <textarea class="form-control" name="Working-Industries" id="Working-Industries" class="form-control" placeholder="What all Industry/Industries are you working with? (eg: Dairy, Handicrafts, etc.)" maxlength="255" onblur="check(this);" required></textarea>
                                       <div class="help-block with-errors"></div>
                                    </div>
                                 </div>
                                 <input type="hidden" name="nomineeEmail" value="${nominee.emailId}" />
                                 <input type="hidden" name="category" value="ecosystem" />
                                 <input type="hidden" name="currentpath" value=""/>
                                 <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                                 <div class="col-md-12">
                                    <div class="row align-center">
                                       <div class="submit-btn">
                                          <a class="btn btn-warning" href="/basicdetails?p1=true&p2=${encryptedEmail}&p3=ecosystembaseform">Back</a>
                                          <span class="btn btn-info review-form" data-toggle="modal" data-target="#myModal">Review Form</span>
                                          <input class="btn btn-warning" id="ecosystemsave4" type="button" value="Save As Draft">
                                          <input class="btn btn-primary" id="submitNomination4" type="button" name="Submit" value="Submit Nomination">
                                       </div>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </main>
      <!--review popup -->
      <div id="myModal" class="modal fade" role="dialog">
         <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Review Form</h4>
               </div>
               <div class="modal-body table-responsive">
                  <table class="table table-bordered" id="review-form-table">
                     <tbody>
                        <tr class="align-center">
                           <th colspan="2">PART A: Nominee's Basic Details</th>
                        </tr>
                        <tr>
                           <td>Award Track</td>
                           <td class="field1">
                              Ecosystem Builders Track
                           </td>
                        </tr>
                        <tr>
                           <td>Award Category</td>
                           <td class="field2" data-id="awardCategory">
                              <c:if test="${nominee.enterpriseCategory == 'onelac'}">
                                 <c:out value="Initial Investment upto 1 Lakh" />
                              </c:if>
                              <c:if test="${nominee.enterpriseCategory == 'uptoten'}">
                                 <c:out value="Initial Investment between 1 Lakh to 10 Lakh" />
                              </c:if>
                              <c:if test="${nominee.enterpriseCategory == 'uptocrore'}">
                                 <c:out value="Initial Investment between 10 Lakh to 1 Crore" />
                              </c:if>
                              <c:if test="${nominee.awardCategory == 'ecosystem'}">
                                 ${nominee.enterpriseCategory}
                              </c:if>
                           </td>
                        </tr>
                        <c:if test="${nominee.awardCategory == 'enterprise'}">
                           <tr>
                              <td>Award Sub Category</td>
                              <td class="field3">${nominee.awardSub}</td>
                           </tr>
                           <tr>
                              <td>Please list about the product/service you offer</td>
                              <td class="field4">${nominee.specifyproduct}</td>
                           </tr>
                           <tr>
                              <td>Nominee/Entrepreneur’s Name</td>
                              <td class="field4">${nominee.nomineeName}</td>
                           </tr>
                           <tr>
                              <td>Date of Birth of the Nominee/Entrepreneur</td>
                              <td class="field4">${nominee.dob}</td>
                           </tr>
                           <tr>
                              <td>Gender</td>
                              <td class="field4">${nominee.gender}</td>
                           </tr>
                           <tr>
                              <td>Social Category</td>
                              <td class="field4">${nominee.socialCategory}</td>
                           </tr>
                           <tr>
                              <td>Person with Disability (PwD)</td>
                              <td class="field4">${nominee.specifyproduct}</td>
                           </tr>
                           <tr>
                              <td>Do you wish to nominate the candidate in the 'Special Category' awards?</td>
                              <td class="field4">
                                 <c:choose>
                                    <c:when test="${nominee.speccatopt}">
                                       Yes
                                    </c:when>
                                    <c:otherwise>
                                       No
                                    </c:otherwise>
                                 </c:choose>
                              </td>
                           </tr>
                           <tr>
                              <td>If yes, please select the appropriate category from the list of following 'Special Category' awards.</td>
                              <td class="field4">${nominee.speccatval}</td>
                           <tr>
                              <td>Name of the Enterprise</td>
                              <td class="field4">${nominee.enterpriseName}</td>
                           </tr>
                        </c:if>
                        <tr>
                           <td>Nominee's Name</td>
                           <td class="field4" data-id="nomineeName">${nominee.nomineeName}</td>
                        </tr>
                        <tr>
                           <td>Gender</td>
                           <td class="field4" data-id="gender">${nominee.gender}</td>
                        </tr>
                        <tr>
                           <td>Nominee's Institution's Name</td>
                           <td class="field4" data-id="enterpriseName">${nominee.enterpriseName}</td>
                        </tr>
                        <tr>
                           <td>House No.</td>
                           <td class="field4" data-id="address1">${nominee.address1}</td>
                        </tr>
                        <tr>
                           <td>Street Name</td>
                           <td class="field4" data-id="address2">${nominee.address2}</td>
                        </tr>
                        <tr>
                           <td>Mohalla/Village</td>
                           <td class="field4" data-id="street">${nominee.street}</td>
                        </tr>
                        <tr>
                           <td>City</td>
                           <td class="field4" data-id="city">${nominee.city}</td>
                        </tr>
                        <tr>
                           <td>State</td>
                           <td class="field4" data-id="state">${nominee.state}</td>
                        </tr>
                        <tr>
                           <td>Landmark</td>
                           <td class="field4" data-id="landmark">${nominee.landmark}</td>
                        </tr>
                        <tr>
                           <td>Pin Code</td>
                           <td class="field4" data-id="pinCode">${nominee.pinCode}</td>
                        </tr>
                        <tr>
                           <td>Website(If available)</td>
                           <td class="field4" data-id="website">${nominee.website}</td>
                        </tr>
                        <tr>
                           <td>Email ID</td>
                           <td class="field4" data-id="emailId">${nominee.emailId}</td>
                        </tr>
                        <tr>
                           <td>Contact Number</td>
                           <td class="field4" data-id="contactNumber">${nominee.contact}</td>
                        </tr>
                        <tr>
                           <td>Please describe the role the Nominee is playing as an Eco-System Contributor.</td>
                           <td class="field1" data-id="roleNominee">${nominee.roleNominee}</td>
                        </tr>
                        <tr>
                           <td>Please describe the impact the Nominee has created by playing their role as Mentor/Incubation Center/Entrepreneurship Development Institutes/.</td>
                           <td class="field2" data-id="impactNominee">${nominee.impactNominee}</td>
                        </tr>
                        <tr>
                           <td>Please mention about the recognitions/awards the Nominee has received in the area of Entrepreneurship development.</td>
                           <td class="field4" data-id="recognitionNominee">${nominee.recognitionNominee}</td>
                        </tr>
                        <tr>
                           <td>Please mention about industry collaborations, Road shows/Bussiness Plan Competitions/Funding Events organised.</td>
                           <td class="field4" data-id="collaborationNominee">${nominee.collaborationNominee}</td>
                        </tr>
                        <tr>
                           <td>Any other special mentions?</td>
                           <td class="field4" data-id="specMentionNominee">${nominee.specMentionNominee}</td>
                        </tr>
                        <tr id="review-row">
                           <th colspan="2">PART B: Nominee's Basic Details</th>
                        </tr>
                     </tbody>
                  </table>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </div>
         </div>
      </div>
      <!--review popup -->
      <!-- Body Ends Here -->
      <%@include file="fragments/footer.jsp" %>
      <script>
         $(document).ready(function() {
         	 $('#ecosystemsave1').on('click',function() {
              	if(localStorage.getItem("ecosystembaseformdata")){
              		var data = JSON.parse(localStorage.getItem("ecosystembaseformdata"))
              		data["partB"] = $('#ecosystembaseform1').serialize()
              		data["tableData"] = $('#Enterpreneur-References1').html()
              		data["formName"] = "ecosystembaseform1";
              		localStorage.setItem("ecosystembaseformdata",JSON.stringify(data))
              	}
                  alert('Saved...');
              });
              $('#ecosystemsave2').on('click',function() {
              	if(localStorage.getItem("ecosystembaseformdata")){
              		var data = JSON.parse(localStorage.getItem("ecosystembaseformdata"))
              		data["partB"] = $('#ecosystembaseform2').serialize()
              		data["tableData"] = $('#Enterpreneur-References2').html()
              		data["formName"] = "ecosystembaseform2";
              		localStorage.setItem("ecosystembaseformdata",JSON.stringify(data))
              	}
                  alert('Saved...');
              });
              $('#ecosystemsave3').on('click',function() {
              	if(localStorage.getItem("ecosystembaseformdata")){
              		var data = JSON.parse(localStorage.getItem("ecosystembaseformdata"))
              		data["partB"] = $('#ecosystembaseform3').serialize()
              		data["tableData"] = $('#Enterpreneur-References3').html()
              		data["formName"] = "ecosystembaseform3";
              		localStorage.setItem("ecosystembaseformdata",JSON.stringify(data))
              	}
                  alert('Saved...');
              });
              $('#ecosystemsave4').on('click',function() {
              	if(localStorage.getItem("ecosystembaseformdata")){
              		var data = JSON.parse(localStorage.getItem("ecosystembaseformdata"))
              		data["partB"] = $('#ecosystembaseform4').serialize()
              		data["tableData"] = '';
              		data["formName"] = "ecosystembaseform4";
              		localStorage.setItem("ecosystembaseformdata",JSON.stringify(data))
              	}
                  alert('Saved...');
              });
         
             
              $("#submitNomination1").on("click",function(e) {
                  
                  var jsonTableData = [];
                  $('#Enterpreneur-References1').find('tbody tr').each(function(index) {
                      var $td = $(this).find('input');
                      var newObj = {};
                      $td.each(function(i) {
         
                          var key, val;
         
                          key = $td.eq(i).attr("placeholder");
                          val = $td.eq(i).val();
         
                          newObj[key] = val;
         
                      });
                      jsonTableData.push(newObj);
                  });
         
                  $('input[name=Enterpreneur-References]').val(JSON.stringify(jsonTableData));
                  console.log(JSON.stringify(jsonTableData));
               var validator = $("#ecosystembaseform1").data("bs.validator");
               validator.validate();
         
               if (!validator.hasErrors()) {
             	  if (confirm('Have you reviewed your data and want to do final submission?')) {
            		console.log("form halt");
                   	var userData = JSON.parse(localStorage.getItem("ecosystembaseformdata"))
               		userData["status"] = "submit";
                   	userData["partB"] = $('#ecosystembaseform1').serialize()
                   	localStorage.setItem("ecosystembaseformdata",JSON.stringify(userData));
                   	
                   	$.ajax({
                   		url:'/anotherbasicdetail',
                   		type:'POST',
                   		data:userData["partA"]+'&'+userData["partB"]+'&status='+userData["status"],
                   		success:function(data){
                   			window.location.href = "/nominatorlanding";
                   		},
                   		error:function(a,b,c){
                   			alert("something went wrong. data not saved.");
                   			console.log(a,b,c)
                   		}
                   	})
                     var myItem = localStorage.getItem('popup');
                     localStorage.clear();
                     localStorage.setItem('popup', myItem);
               }
               }
         
              });
              
              $("#submitNomination2").on("click",function(e) {
                  
                  var jsonTableData = [];
                  $('#Enterpreneur-References2').find('tbody tr').each(function(index) {
                      var $td = $(this).find('input');
                      var newObj = {};
                      $td.each(function(i) {
         
                          var key, val;
         
                          key = $td.eq(i).attr("placeholder");
                          val = $td.eq(i).val();
         
                          newObj[key] = val;
         
                      });
                      jsonTableData.push(newObj);
                  });
         
                  $('input[name=Enterpreneur-References]').val(JSON.stringify(jsonTableData));
                  console.log(JSON.stringify(jsonTableData));
             var validator = $("#ecosystembaseform2").data("bs.validator");
             validator.validate();
         
             if (!validator.hasErrors()) {
             	if (confirm('Have you reviewed your data and want to do final submission?')) {
          		console.log("form halt");
                 	var userData = JSON.parse(localStorage.getItem("ecosystembaseformdata"))
             		userData["status"] = "submit";
                 	userData["partB"] = $('#ecosystembaseform2').serialize()
                 	localStorage.setItem("ecosystembaseformdata",JSON.stringify(userData));
                 	
                 	$.ajax({
                 		url:'/anotherbasicdetail',
                 		type:'POST',
                 		data:userData["partA"]+'&'+userData["partB"]+'&status='+userData["status"],
                 		success:function(data){
                 			window.location.href = "/nominatorlanding";
                 		},
                 		error:function(a,b,c){
                 			alert("something went wrong. data not saved.");
                 			console.log(a,b,c)
                 		}
                 	})
                 var myItem = localStorage.getItem('popup');
                  localStorage.clear();
                  localStorage.setItem('popup', myItem);
             }
             }
              });
              $("#submitNomination3").on("click",function(e) {
                  
                  var jsonTableData = [];
                  $('#Enterpreneur-References3').find('tbody tr').each(function(index) {
                      var $td = $(this).find('input');
                      var newObj = {};
                      $td.each(function(i) {
         
                          var key, val;
         
                          key = $td.eq(i).attr("placeholder");
                          val = $td.eq(i).val();
         
                          newObj[key] = val;
         
                      });
                      jsonTableData.push(newObj);
                  });
         
                  $('input[name=Enterpreneur-References]').val(JSON.stringify(jsonTableData));
                  console.log(JSON.stringify(jsonTableData));
                  
                //e.preventDefault();
               var validator = $("#ecosystembaseform3").data("bs.validator");
               validator.validate();
         
               if (!validator.hasErrors()) {
             	  if (confirm('Have you reviewed your data and want to do final submission?')) {
            		console.log("form halt");
                   	var userData = JSON.parse(localStorage.getItem("ecosystembaseformdata"))
               		userData["status"] = "submit";
                   	userData["partB"] = $('#ecosystembaseform3').serialize()
                   	localStorage.setItem("ecosystembaseformdata",JSON.stringify(userData));
                   	
                   	$.ajax({
                   		url:'/anotherbasicdetail',
                   		type:'POST',
                   		data:userData["partA"]+'&'+userData["partB"]+'&status='+userData["status"],
                   		success:function(data){
                   			window.location.href = "/nominatorlanding";
                   		},
                   		error:function(a,b,c){
                   			alert("something went wrong. data not saved.");
                   			console.log(a,b,c)
                   		}
                   	})
                     var myItem = localStorage.getItem('popup');
                     localStorage.clear();
                     localStorage.setItem('popup', myItem);
               }
               }
              });
              $("#submitNomination4").on("click",function(e) {
               var validator = $("#ecosystembaseform4").data("bs.validator");
               validator.validate();
         
               if (!validator.hasErrors()) {
             	  if (confirm('Have you reviewed your data and want to do final submission?')) {
            		console.log("form halt");
                   	var userData = JSON.parse(localStorage.getItem("ecosystembaseformdata"))
               		userData["status"] = "submit";
                   	userData["partB"] = $('#ecosystembaseform4').serialize()
                   	localStorage.setItem("ecosystembaseformdata",JSON.stringify(userData));
                   	
                   	$.ajax({
                   		url:'/anotherbasicdetail',
                   		type:'POST',
                   		data:userData["partA"]+'&'+userData["partB"]+'&status='+userData["status"],
                   		success:function(data){
                   			window.location.href = "/nominatorlanding";
                   			return true;
                   		},
                   		error:function(a,b,c){
                   			alert("something went wrong. data not saved.");
                   			console.log(a,b,c)
                   			return false;
                   		}
                   	})
                     var myItem = localStorage.getItem('popup');
                     localStorage.clear();
                     localStorage.setItem('popup', myItem);
               }
               }
              });
              
         
             $('.edioform,.mentorform,.incubatorform,.ruralform').hide();
             $('#builder-category').change(function() {
                 if ($('#builder-category').val() === 'Entrepreneurship Development Institutes/Organisations') {
                     $('.edioform').show();
                     $('.mentorform,.incubatorform,.ruralform').hide();
                 }
                 if ($('#builder-category').val() === 'Mentor') {
                     $('.mentorform').show();
                     $('.edioform,.incubatorform,.ruralform').hide();
                 }
                 if ($('#builder-category').val() === 'Incubator') {
                     $('.incubatorform').show();
                     $('.mentorform,.edioform,.ruralform').hide();
                 }
                 if ($('#builder-category').val() === 'Promoters of Rural Producer Enterprises') {
                     $('.ruralform').show();
                     $('.mentorform,.incubatorform,.edioform').hide();
                 }
             });

         
             if (typeof localStorage !== 'undefined' && typeof localStorage.getItem("ecosystembaseformdata") !==null){
             	var data = JSON.parse(localStorage.getItem("ecosystembaseformdata"));
             	switch(data["formName"]){
             	  case "ecosystembaseform1":{
                    	$('#Enterpreneur-References1').html(data["tableData"])
             		  var pairs, i, keyValuePair, key, value, map = {};
                       var query = data["partB"];
                       pairs = query.split('&');
                       for (i = 0; i < pairs.length; i += 1) {
                           keyValuePair = pairs[i].split('=');
                           key = decodeURIComponent(keyValuePair[0]);
                           value = (keyValuePair.length > 1) ? decodeURIComponent(keyValuePair[1]) : '';
                           if (key !== '_csrf') {
                               if (key !== '') {
                                   $('#ecosystembaseform1 [name="' + key + '"]').val(value);
                               }
                               if (key === 'builder-category' || key === 'Programs-Offered' || key === 'Level-Offered' || key === 'Services-Provided') {
                                   $('select[name=' + key + ']').val(value);
                                   $('.edioform').show();
                                   $('.mentorform').hide();
                                   $('.incubatorform').hide();
                                   $('.ruralform').hide();
                               }
                               if(key==='question1'){
                              	 $('[name="question1"]').prop('checked',true);                                                 
                               }
                               if(key==='question2'){
                              	 $('[name="question2"]').prop('checked',true);                                               
                            }
                               if(key==='question3'){
                              	 $('[name="question3"]').prop('checked',true);                                             
                            }
                               if(key==='question4'){
                              	 $('[name="question4"]').prop('checked',true);
                              	 $(".q2a").show();
                            }
                               if(key==='question5'){
                              	 $('[name="question5"]').prop('checked',true);
                              	 $(".q2b").show();
                            }
                               if(key==='question6'){
                              	 $('[name="question6"]').prop('checked',true);
                              	 $(".q2c").show();
                            }
                               if(key==='question7'){
                              	 $('[name="question7"]').prop('checked',true);
                              	 $(".q2d").show();
                            }
                          }
                       }
                       
                      
                       
             		  break;
             	  }
             	  case "ecosystembaseform2":{
                       $('#Enterpreneur-References2').html(data["tableData"])
             		  var pairs, i, keyValuePair, key, value, map = {};
                       var query = data["partB"];
                       pairs = query.split('&');
                       for (i = 0; i < pairs.length; i += 1) {
                           keyValuePair = pairs[i].split('=');
                           key = decodeURIComponent(keyValuePair[0]);
                           value = (keyValuePair.length > 1) ? decodeURIComponent(keyValuePair[1]) : '';
                           if (key !== '_csrf') {
                               if (key !== '') {
                                   $('#ecosystembaseform2 [name="' + key + '"]').val(value);
                               }
                               if (key === 'builder-category') {
                                   $('select[name=' + key + ']').val(value);
                                   $('.mentorform').show();
                                   $('.incubatorform').hide();
                                   $('.edioform').hide();
                                   $('.ruralform').hide();
                               }
                           }
                       }
                       
             		  break;
             	  }
             	  case "ecosystembaseform3":{
             		 	var checkedArr =[];
                       $('#Enterpreneur-References3').html(data["tableData"])
             		  var pairs, i, keyValuePair, key, value, map = {};
                       var query = data["partB"];
                       pairs = query.split('&');
                       for (i = 0; i < pairs.length; i += 1) {
                           keyValuePair = pairs[i].split('=');
                           key = decodeURIComponent(keyValuePair[0]);
                           value = (keyValuePair.length > 1) ? decodeURIComponent(keyValuePair[1]) : '';
                           if (key !== '_csrf') {
                               if (key !== '') {
                                   $('#ecosystembaseform3 [name="' + key + '"]').val(value);
                               }
                               if (key === 'builder-category') {
                                   $('select[name=' + key + ']').val(value);
                                   $('.incubatorform').show();
                                   $('.mentorform').hide();
                                   $('.edioform').hide();
                                   $('.ruralform').hide();
         
                               }
                               if(key === 'Services-Provided'){
                 	            	checkedArr.push(value)
                               }
                           }
                       }
                       $('#ecosystembaseform3 select[name="Services-Provided"]').val(checkedArr);
             		  break;
             	  }
             	  case "ecosystembaseform4":{
             		     var pairs, i, keyValuePair, key, value, map = {};
                          var query = data["partB"];
                          pairs = query.split('&');
                          for (i = 0; i < pairs.length; i += 1) {
                              keyValuePair = pairs[i].split('=');
                              key = decodeURIComponent(keyValuePair[0]);
                              value = (keyValuePair.length > 1) ? decodeURIComponent(keyValuePair[1]) : '';
                              if (key !== '_csrf') {
                                  if (key !== '') {
                                      $('#ecosystembaseform4 [name="' + key + '"]').val(value);
                                  }
                                  if (key === 'builder-category') {
                                      $('select[name="' + key + '"]').val(value);
                                      $('.ruralform').show();
                                      $('.edioform').hide();
                                      $('.mentorform').hide();
                                      $('.incubatorform').hide();
                                  }
                              }
                          }
             		  break;
             	  }
             	  default:{
             		  break;
             	  }
             	}
             }
             
            
         });
         
         $(document).ready(function() {
             var count = 4;
             $("#addrow1,#addrow2,#addrow3").on("click", function() {
            	 if(count<=10){
                 var newRow = "";
                 var cols = "";
                 cols += '<td><div class="form-group"><input type="text" name="individual-name' + count + '" class="form-control" placeholder="Individual Name"/><div class="help-block with-errors"></div></div></td>';
                 cols += '<td><div class="form-group"><input type="text" name="enterprise-name' + count + '"  class="form-control" placeholder="Enterprise Name"/><div class="help-block with-errors"></div></div></td>';
                 cols += '<td><div class="form-group"><input type="text" name="industry' + count + '"  class="form-control" placeholder="Industry"/><div class="help-block with-errors"></div></div></td>';
                 cols += '<td><div class="form-group"><input type="text" name="years-in-operation' + count + '"  class="form-control" pattern="[0-9]{1,10}" placeholder="Years in Operation"/><div class="help-block with-errors"></div></div></td>';
                 cols += '<td><div class="form-group"><input type="text" name="mobile' + count + '"  class="form-control" pattern="[0-9]{10}" placeholder="10 Digit Mobile Number" /><div class="help-block with-errors"></div></div></td>';
                 cols += '<td><div class="form-group"><input type="email" name="email-address' + count + '"  class="form-control" placeholder="Email"/></td>';
                 cols += '<td><div class="form-group"><input type="text" name="website' + count + '"  class="form-control" placeholder="Website"/></td>';
                 cols += '<td><div class="form-group"><input type="text" name="quantum-of-funding-received' + count + '"  class="form-control" placeholder="Quantum of funding received" onkeyup="qunt' + (count+100) + '_word.innerHTML=convertNumberToWords(this.value)"><div id="qunt' + (count+100) + '_word"></div><div class="help-block with-errors"></div></div></td>';
                 cols += '<td><a class="ibtnDel btn btn-md btn-danger">Delete</a></td>';
                 newRow = "<tr>" + cols + "</tr>";
                 $("table.order-list tbody").append(newRow);
                 count++;
                 $("form").validator('update');
            	 }
             });
             
             $("table.order-list").on("click", ".ibtnDel", function(event) {
                 $(this).closest("tr").remove();
                 count -= 1;
             });
         });		
         
         $(document).ready(function() {
             $(".onlyDigit").keypress(function(e) {
                 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                     return false;
                 }
             });
         });
         var reviewTable = "";
         $('.review-form').click(function() {
         	
         	var data = JSON.parse(localStorage.getItem('ecosystembaseformdata'))["partA"]
         	var dataArr = data.split('&');
         	for(var i=0; i<dataArr.length; i++){
         		var key = dataArr[i].split('=')
         		key[0] = decodeURIComponent(key[0]);
         		key[1] = (key[1].length > 0) ? decodeURIComponent(key[1]) : '';
         		if($('#review-form-table').find('[data-id="'+key[0]+'"]').length){
         			if(key[1] == 'm'){key[1] = 'Male';}if(key[1] == 'f'){key[1] = 'Female';}if(key[1] == 't'){key[1] = 'Third Gender';}
         			$('#review-form-table').find('[data-id="'+key[0]+'"]').text(key[1]);
         		}
         	}
         	
         	$('#review-row').nextAll().remove();
             var catval = $('#builder-category').val();
             var reviewTable = "";
             if (catval === 'Entrepreneurship Development Institutes/Organisations') {
             	
             	var checkedArr =[];
                $('[name=question1],[name=question2],[name=question3]').each(function(){
              	  if($(this).prop('checked')){
              		  checkedArr.push($(this).val())
              	  }
              	  
                });
               	
                   reviewTable += "<tr><td>Nature of Programs Offered</td><td>" + /* $('#Programs-Offered').val() */ checkedArr.join(', ') + "</td></tr>";
                   
                   var checkedLvlArr =[];
                  $('[name=question4],[name=question5],[name=question6],[name=question7]').each(function(){
                	  if($(this).prop('checked')){
                		  checkedLvlArr.push($(this).val())
                	  }
                	  
                  });
                   reviewTable += "<tr><td>Level of Programs Offered</td><td>" + /* $('#Level-Offered').val() */ checkedLvlArr.join(', ') + "</td></tr>";
                   
                   var checkedDtArr =[];
                $('[name=Date-Offered1],[name=Date-Offered2],[name=Date-Offered3],[name=Date-Offered4]').each(function(){
              	  if($(this).val() != ""){
              		checkedDtArr.push($(this).val())
              	  }
              	  
                });
                 reviewTable += "<tr><td>Programs offered since?</td><td>" + /* $('#Date-Offered').val() */checkedDtArr.join(', ') + "</td></tr>";
                 reviewTable += "<tr><td>Number of Professional Resource Persons (Faculty, Guest Faculty, etc.)</td><td>" + $('#Pro-Resource').val() + "</td></tr>";
                 reviewTable += "<tr><td>Number of Students Graduated (in past 3 years)</td><td>" + $('#Student-Graduated').val() + "</td></tr>";
                 reviewTable += "<tr><td>Number of Students turned Entrepreneurs (in past 3 years)</td><td>" + $('#Student-Entrepreneurs').val() + "</td></tr>";
                 reviewTable += "<tr><td>Number of IPRs/Trademarks filed by Students (in past 3 years)</td><td>" + $('#Student-IPR').val() + "</td></tr>";
                 reviewTable += "<tr><td colspan=2>Number of Enterpreneur References being provided</td></tr>";
                 var tableData = "<table class='table table-bordered'><thead><tr><td>Individual Name</td><td>Enterprise Name</td><td>Industry</td><td>Years in Operation</td><td>Mobile</td><td>Email Address(if any)</td>";
                 tableData += "<td>Website(if any)</td><td>Quantum of funding received</td></tr></thead><tbody>";
                 $("#Enterpreneur-References1").find("tbody tr").each(function(x){
                 	tableData += "<tr>";
                 	$(this).find('input').each(function(i){
                 		tableData += "<td>"+$(this).val()+"</td>";
                 	});
                 	tableData += "</tr>";
                 });
                 tableData += "</tbody></table>";
                 reviewTable += "<tr><td colspan=2>"+tableData+"</td></tr>";
             } else if (catval === 'Mentor') {
                 reviewTable += "<tr><td>Number of Mentees Mentored (in past 3 years)</td><td>" + $('#Mentees-Mentored').val() + "</td></tr>";
                 reviewTable += "<tr><td>How many Mentees turned Entrepreneurs (in past 3 years)</td><td>" + $('#Mentees-Entrepreneurs').val() + "</td></tr>";
                 reviewTable += "<tr><td>Number of IPRs/Trademarks filed by Mentees (in past 3 years)</td><td>" + $('#Mentees-IPR').val() + "</td></tr>";
                 reviewTable += "<tr><td colspan=2>Number of Enterpreneur References being provided</td></tr>";
                 var tableData = "<table class='table table-bordered'><thead><tr><td>Individual Name</td><td>Enterprise Name</td><td>Industry</td><td>Years in Operation</td><td>Mobile</td><td>Email Address(if any)</td>";
                 tableData += "<td>Website(if any)</td><td>Quantum of funding received</td></tr></thead><tbody>";
                 $("#Enterpreneur-References2").find("tbody tr").each(function(x){
                 	tableData += "<tr>";
                 	$(this).find('input').each(function(i){
                 		tableData += "<td>"+$(this).val()+"</td>";
                 	});
                 	tableData += "</tr>";
                 });
                 tableData += "</tbody></table>";
                 reviewTable += "<tr><td colspan=2>"+tableData+"</td></tr>";
             } else if (catval === 'Incubator') {
                 reviewTable += "<tr><td>Number of Incubatees (in past 3 years)</td><td>" + $('#Incubatees').val() + "</td></tr>";
                 reviewTable += "<tr><td>Number of Professional Resource Persons (Faculty, Guest Faculty, etc.)</td><td>" + $('#Professional-Resource').val() + "</td></tr>";
                 reviewTable += "<tr><td>Quantum of Funds Invested/Granted to Incubatee Enterprises</td><td>" + $('#Quantum-Funds').val() + "</td></tr>";
                 reviewTable += "<tr><td>Services Provided?</td><td>" + $('#Services-Provided').val() + "</td></tr>";
                 reviewTable += "<tr><td>If Capital provided, Total Monetized Equity Value held by Incubator (as on 31 Mar 2018)</td><td>" + $('#Monetized-Equity').val() + "</td></tr>";
                 reviewTable += "<tr><td>Number of IPRs/Trademarks filed by Incubatees (in past 3 years)</td><td>" + $('#Trademarks-IPR').val() + "</td></tr>";
                 reviewTable += "<tr><td>How many Incubatees turned Entrepreneurs (in past 3 years)</td><td>" + $('#Incubatees-Entrepreneurs').val() + "</td></tr>";
                 var tableData = "<table class='table table-bordered'><thead><tr><td>Individual Name</td><td>Enterprise Name</td><td>Industry</td><td>Years in Operation</td><td>Mobile</td><td>Email Address(if any)</td>";
                 tableData += "<td>Website(if any)</td><td>Quantum of funding received</td></tr></thead><tbody>";
                 $("#Enterpreneur-References3").find("tbody tr").each(function(x){
                 	tableData += "<tr>";
                 	$(this).find('input').each(function(i){
                 		tableData += "<td>"+$(this).val()+"</td>";
                 	});
                 	tableData += "</tr>";
                 });
                 tableData += "</tbody></table>";
                 reviewTable += "<tr><td colspan=2>"+tableData+"</td></tr>";
             } else if (catval === 'Promoters of Rural Producer Enterprises') {
                 reviewTable += "<tr><td>Number of States, Districts, Villages covered?</td><td>" + $('#Area-Covered').val() + "</td></tr>";
                 reviewTable += "<tr><td>Total Population covered?</td><td>" + $('#Total-Population').val() + "</td></tr>";
                 reviewTable += "<tr><td>Number of Cooperative Societies/Self-Help Groups?</td><td>" + $('#Cooperative-Societies').val() + "</td></tr>";
                 reviewTable += "<tr><td>Number of Rural Producers Associated (including through Societies & SHGs)</td><td>" + $('#Rural-Producers').val() + "</td></tr>";
                 reviewTable += "<tr><td>What all Industry/Industries are you working with? (eg: Dairy, Handicrafts, etc.)</td><td>" + $('#Working-Industries').val() + "</td></tr>";
             } else {
         
             }
             $('#review-form-table').find("tbody").append(reviewTable);
         });
         
         /* date dropdowns checker START */
         $("#monthida, #yearida, #dayida").change(function(){
           $("#d29").hide(); $("#d30").hide(); $("#d31").hide();
           var m = document.getElementById("monthida");
           var y = document.getElementById("yearida");
           var d = document.getElementById("dayida");
           var iMonth = m.options[m.selectedIndex].value;
           var iYear = y.options[y.selectedIndex].value;
           var iday = d.options[d.selectedIndex].value;
           console.log("month-- "+ iMonth);
           console.log("year-- "+ iYear);
           console.log("result-- "+daysInMonth(iMonth -1, iYear));
           var dayVal = daysInMonth(iMonth -1, iYear);
         
           if(dayVal == 29){$("#d29").show();}
           if(dayVal == 30){$("#d29").show(); $("#d30").show();}
           if(dayVal == 31){$("#d29").show(); $("#d30").show(); $("#d31").show();}
         
           if(iMonth != "MM" && iYear != "yyyy" && iday != "dd"){
            document.getElementById('dateeida').value = iday+"-"+iMonth+"-"+iYear;
           }
         
           });
         
         $("#monthidb, #yearidb, #dayidb").change(function(){
            $("#d29").hide(); $("#d30").hide(); $("#d31").hide();
            var m = document.getElementById("monthidb");
            var y = document.getElementById("yearidb");
            var d = document.getElementById("dayidb");
            var iMonth = m.options[m.selectedIndex].value;
            var iYear = y.options[y.selectedIndex].value;
            var iday = d.options[d.selectedIndex].value;
            console.log("month-- "+ iMonth);
            console.log("year-- "+ iYear);
            console.log("result-- "+daysInMonth(iMonth -1, iYear));
            var dayVal = daysInMonth(iMonth -1, iYear);
         
            if(dayVal == 29){$("#d29").show();}
            if(dayVal == 30){$("#d29").show(); $("#d30").show();}
            if(dayVal == 31){$("#d29").show(); $("#d30").show(); $("#d31").show();}
         
            if(iMonth != "MM" && iYear != "yyyy" && iday != "dd"){
             document.getElementById('dateeidb').value = iday+"-"+iMonth+"-"+iYear;
            }
         
            });
         
         $("#monthidc, #yearidc, #dayidc").change(function(){
            $("#d29").hide(); $("#d30").hide(); $("#d31").hide();
            var m = document.getElementById("monthidc");
            var y = document.getElementById("yearidc");
            var d = document.getElementById("dayidc");
            var iMonth = m.options[m.selectedIndex].value;
            var iYear = y.options[y.selectedIndex].value;
            var iday = d.options[d.selectedIndex].value;
            console.log("month-- "+ iMonth);
            console.log("year-- "+ iYear);
            console.log("result-- "+daysInMonth(iMonth -1, iYear));
            var dayVal = daysInMonth(iMonth -1, iYear);
         
            if(dayVal == 29){$("#d29").show();}
            if(dayVal == 30){$("#d29").show(); $("#d30").show();}
            if(dayVal == 31){$("#d29").show(); $("#d30").show(); $("#d31").show();}
         
            if(iMonth != "MM" && iYear != "yyyy" && iday != "dd"){
             document.getElementById('dateeidc').value = iday+"-"+iMonth+"-"+iYear;
            }
         
            });
         
         $("#monthidd, #yearidd, #dayidd").change(function(){
            $("#d29").hide(); $("#d30").hide(); $("#d31").hide();
            var m = document.getElementById("monthidd");
            var y = document.getElementById("yearidd");
            var d = document.getElementById("dayidd");
            var iMonth = m.options[m.selectedIndex].value;
            var iYear = y.options[y.selectedIndex].value;
            var iday = d.options[d.selectedIndex].value;
            console.log("month-- "+ iMonth);
            console.log("year-- "+ iYear);
            console.log("result-- "+daysInMonth(iMonth -1, iYear));
            var dayVal = daysInMonth(iMonth -1, iYear);
         
            if(dayVal == 29){$("#d29").show();}
            if(dayVal == 30){$("#d29").show(); $("#d30").show();}
            if(dayVal == 31){$("#d29").show(); $("#d30").show(); $("#d31").show();}
         
            if(iMonth != "MM" && iYear != "yyyy" && iday != "dd"){
             document.getElementById('dateeidd').value = iday+"-"+iMonth+"-"+iYear;
            }
         
            });
         
            function daysInMonth(iMonth, iYear)
             {
              return 32 - new Date(iYear, iMonth, 32).getDate();
             }
         /* date dropdowns checker END */
         
         /* change for checkboxes of question 2 part B start */
         $("#q2a").change(function(){
         if($(this).prop('checked')){
         $(".q2a").show();
         }
         else{
         $(".q2a").hide();
         $("#monthida").val("");
         $("#yearida").val("");
         $("#dayida").val("");
         $("#dateeida").val("");
         }
         });
         $("#q2b").change(function(){
         if($(this).prop('checked')){
         $(".q2b").show();
         }
         else{
         $(".q2b").hide();
         $("#monthidb").val("");
         $("#yearidb").val("");
         $("#dayidb").val("");
         $("#dateeidb").val("");
         }
         });
         $("#q2c").change(function(){
         if($(this).prop('checked')){
         $(".q2c").show();
         }
         else{
         $(".q2c").hide();
         $("#monthidc").val("");
         $("#yearidc").val("");
         $("#dayidc").val("");
         $("#dateeidc").val("");
         }
         });
         $("#q2d").change(function(){
         if($(this).prop('checked')){
         $(".q2d").show();
         }
         else{
         $(".q2d").hide();
         $("#monthidd").val("");
         $("#yearidd").val("");
         $("#dayidd").val("");
         $("#dateeidd").val("");
         }
         });
         
         $(document).on('change', '.city,.state,.village', function() {
             var city = $(".city").val();
             var state = $(".state").val();
             var village = $(".village").val();
             var address_add = state.toString() +" States, "+ city.toString() +" Cities, "+ village.toString() +" Villages" ;
             $(".address_add").val(address_add);
         });
         
         function convertNumberToWords(amount) {
     		var words = new Array();
     		words[0] = '';
     		words[1] = 'One';
     		words[2] = 'Two';
     		words[3] = 'Three';
     		words[4] = 'Four';
     		words[5] = 'Five';
     		words[6] = 'Six';
     		words[7] = 'Seven';
     		words[8] = 'Eight';
     		words[9] = 'Nine';
     		words[10] = 'Ten';
     		words[11] = 'Eleven';
     		words[12] = 'Twelve';
     		words[13] = 'Thirteen';
     		words[14] = 'Fourteen';
     		words[15] = 'Fifteen';
     		words[16] = 'Sixteen';
     		words[17] = 'Seventeen';
     		words[18] = 'Eighteen';
     		words[19] = 'Nineteen';
     		words[20] = 'Twenty';
     		words[30] = 'Thirty';
     		words[40] = 'Forty';
     		words[50] = 'Fifty';
     		words[60] = 'Sixty';
     		words[70] = 'Seventy';
     		words[80] = 'Eighty';
     		words[90] = 'Ninety';
     		amount = amount.toString();
     		var atemp = amount.split(".");
     		var number = atemp[0].split(",").join("");
     		var n_length = number.length;
     		var words_string = "";
     		if (n_length <= 9) {
     			var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
     			var received_n_array = new Array();
     			for (var i = 0; i < n_length; i++) {
     				received_n_array[i] = number.substr(i, 1);
     			}
     			for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
     				n_array[i] = received_n_array[j];
     			}
     			for (var i = 0, j = 1; i < 9; i++, j++) {
     				if (i == 0 || i == 2 || i == 4 || i == 7) {
     					if (n_array[i] == 1) {
     						n_array[j] = 10 + parseInt(n_array[j]);
     						n_array[i] = 0;
     					}
     				}
     			}
     			value = "";
     			for (var i = 0; i < 9; i++) {
     				if (i == 0 || i == 2 || i == 4 || i == 7) {
     					value = n_array[i] * 10;
     				} else {
     					value = n_array[i];
     				}
     				if (value != 0) {
     					words_string += words[value] + " ";
     				}
     				if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
     					words_string += "Crores ";
     				}
     				if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
     					words_string += "Lakhs ";
     				}
     				if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
     					words_string += "Thousand ";
     				}
     				if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
     					words_string += "Hundred and ";
     				} else if (i == 6 && value != 0) {
     					words_string += "Hundred ";
     				}
     			}
     			words_string = words_string.split("  ").join(" ");
     		}
     		return words_string;
     	}
         /* change for checkboxes of question 2 part B END */
      </script>
   </body>
</html>